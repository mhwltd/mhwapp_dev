<?php

function createThumbs( $pathToImages, $pathToThumbs, $fname, $thumbWidth ) 
{
  // open the directory
  //$dir = opendir( $pathToImages );

  // loop through it, looking for any/all JPG files:
   //while (false !== ($fname = readdir( $dir ))) {
    // parse path for the extension
    $info = pathinfo($pathToImages . $fname);
    // continue only if this is a JPEG image
    if ( strtolower($info['extension']) == 'jpg' || strtolower($info['extension']) == 'jpeg' || strtolower($info['extension']) == 'jpe' ) 
    {
      if($debug=="on"){ echo "Creating thumbnail for {$pathToImages}{$fname} in {$pathToThumbs}<br />"; }

      // load image and get image size

	  $img = imagecreatefromjpeg( "{$pathToImages}{$fname}" );
		//echo '1';
      $width = imagesx( $img );
      $height = imagesy( $img );

      // calculate thumbnail size
      $new_width = $thumbWidth;
      $new_height = floor( $height * ( $thumbWidth / $width ) );
		//echo '2';
      // create a new tempopary image
      $tmp_img = imagecreatetruecolor( $new_width, $new_height );
		//echo '3';
      // copy and resize old image into new image 
      imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
		//echo '4';
      // save thumbnail into a file
	  imagejpeg( $tmp_img, "{$pathToThumbs}{$fname}" );
		//echo '5';
    }
	elseif ( strtolower($info['extension']) == 'gif' ) 
    {
      if($debug=="on"){ echo "Creating thumbnail for {$fname} <br />"; }

      // load image and get image size

	  $img = imagecreatefromgif( "{$pathToImages}{$fname}" );

      $width = imagesx( $img );
      $height = imagesy( $img );

      // calculate thumbnail size
      $new_width = $thumbWidth;
      $new_height = floor( $height * ( $thumbWidth / $width ) );

      // create a new tempopary image
      $tmp_img = imagecreatetruecolor( $new_width, $new_height );

      // copy and resize old image into new image 
      imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );

      // save thumbnail into a file
	  imagegif( $tmp_img, "{$pathToThumbs}{$fname}" );

    }
	elseif ( strtolower($info['extension']) == 'png' ) 
    {
      if($debug=="on"){ echo "Creating thumbnail for {$fname} <br />"; }

      // load image and get image size

	  $img = imagecreatefrompng( "{$pathToImages}{$fname}" );

      $width = imagesx( $img );
      $height = imagesy( $img );

      // calculate thumbnail size
      $new_width = $thumbWidth;
      $new_height = floor( $height * ( $thumbWidth / $width ) );

      // create a new tempopary image
      $tmp_img = imagecreatetruecolor( $new_width, $new_height );

      // copy and resize old image into new image 
      imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );

      // save thumbnail into a file
	  imagepng( $tmp_img, "{$pathToThumbs}{$fname}" );

    }
	/* elseif ( strtolower($info['extension']) == 'tif' || strtolower($info['extension']) == 'tiff'  ) 
    {
      if($debug=="on"){ echo "Creating thumbnail for {$fname} <br />"; }

      // load image and get image size
	  $img = imagecreatefromstring( file_get_contents("{$pathToImages}{$fname}") );

      $width = imagesx( $img );
      $height = imagesy( $img );

      // calculate thumbnail size
      $new_width = $thumbWidth;
      $new_height = floor( $height * ( $thumbWidth / $width ) );

      // create a new tempopary image
      $tmp_img = imagecreatetruecolor( $new_width, $new_height );

      // copy and resize old image into new image 
      imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );

      // save thumbnail into a file
	  imagepng( $tmp_img, "{$pathToThumbs}{$fname}" );

    } */
  //}
  // close the directory
  //closedir( $dir );
}
function xmlObjToArr($obj) { 
	$namespace = $obj->getDocNamespaces(true); 
	$namespace[NULL] = NULL; 

	$children = array(); 
	$attributes = array(); 
	$name = strtolower((string)$obj->getName()); 
	
	$text = trim((string)$obj); 
	if( strlen($text) <= 0 ) { 
		$text = NULL; 
	} 
	
	// get info for all namespaces 
	if(is_object($obj)) { 
		foreach( $namespace as $ns=>$nsUrl ) { 
			// atributes 
			$objAttributes = $obj->attributes($ns, true); 
			foreach( $objAttributes as $attributeName => $attributeValue ) { 
				$attribName = strtolower(trim((string)$attributeName)); 
				$attribVal = trim((string)$attributeValue); 
				if (!empty($ns)) { 
					$attribName = $ns . ':' . $attribName; 
				} 
				$attributes[$attribName] = $attribVal; 
			} 
			
			// children 
			$objChildren = $obj->children($ns, true); 
			foreach( $objChildren as $childName=>$child ) { 
				$childName = strtolower((string)$childName); 
				if( !empty($ns) ) { 
					$childName = $ns.':'.$childName; 
				} 
				$children[$childName][] = xmlObjToArr($child); 
			} 
		} 
	} 
	
	return array( 
		'name'=>$name, 
		'text'=>$text, 
		'attributes'=>$attributes, 
		'children'=>$children 
	); 
} 
function hash_gen($start = null, $end = 0, $hash = FALSE, $prefix = FALSE){

    // start IS set NO hash
    if( isset($start, $end) && ($hash == FALSE) ){

        $md_hash = substr(md5(uniqid(rand(), true)), $start, $end);
        $new_hash = $md_hash;

    }else //start IS set WITH hash NOT prefixing
    if( isset($start, $end) && ($hash != FALSE) && ($prefix == FALSE) ){

        $md_hash = substr(md5(uniqid(rand(), true)), $start, $end);
        $new_hash = $md_hash.$hash;

    }else //start NOT set WITH hash NOT prefixing 
    if( !isset($start, $end) && ($hash != FALSE) && ($prefix == FALSE) ){

        $md_hash = md5(uniqid(rand(), true));
        $new_hash = $md_hash.$hash;

    }else //start IS set WITH hash IS prefixing 
    if( isset($start, $end) && ($hash != FALSE) && ($prefix == TRUE) ){

        $md_hash = substr(md5(uniqid(rand(), true)), $start, $end);
        $new_hash = $hash.$md_hash;

    }else //start NOT set WITH hash IS prefixing
    if( !isset($start, $end) && ($hash != FALSE) && ($prefix == TRUE) ){

        $md_hash = md5(uniqid(rand(), true));
        $new_hash = $hash.$md_hash;

    }else{

        $new_hash = md5(uniqid(rand(), true));

    }

    return $new_hash;
}
function mb_pathinfo($filepath) {
    preg_match('%^(.*?)[\\\\/]*(([^/\\\\]*?)(\.([^\.\\\\/]+?)|))[\\\\/\.]*$%im',$filepath,$m);
    if($m[1]) $ret['dirname']=$m[1];
    if($m[2]) $ret['basename']=$m[2];
    if($m[5]) $ret['extension']=$m[5];
    if($m[3]) $ret['filename']=$m[3];
    return $ret;
}
function convert_ascii($string) 
{ 
  // Replace Single Curly Quotes
  $search[]  = chr(226).chr(128).chr(152);
  $replace[] = "'";
  $search[]  = chr(226).chr(128).chr(153);
  $replace[] = "'";
  // Replace Smart Double Curly Quotes
  $search[]  = chr(226).chr(128).chr(156);
  $replace[] = '"';
  $search[]  = chr(226).chr(128).chr(157);
  $replace[] = '"';
  // Replace En Dash
  $search[]  = chr(226).chr(128).chr(147);
  $replace[] = '--';
  // Replace Em Dash
  $search[]  = chr(226).chr(128).chr(148);
  $replace[] = '---';
  // Replace Bullet
  $search[]  = chr(226).chr(128).chr(162);
  $replace[] = '*';
  // Replace Middle Dot
  $search[]  = chr(194).chr(183);
  $replace[] = '*';
  // Replace Ellipsis with three consecutive dots
  $search[]  = chr(226).chr(128).chr(166);
  $replace[] = '...';
  // Apply Replacements
  $string = str_replace($search, $replace, $string);
  // Remove any non-ASCII Characters
  $string = preg_replace("/[^\x01-\x7F]/","", $string);
  return $string; 
}


function generated_validation_query($table_name,$product_info)
{
	
$generated_str="";

	foreach($product_info as $each_product_name=>$val)
	{
		if($val==1)
		{
		  $generated_str=$generated_str.$table_name.".".$each_product_name. " is not null and ".$table_name.".".$each_product_name."!='' and ";
		}
		
	}
	
	return substr($generated_str, 0, -4);
}


function generated_validation_query_v2($table_name,$product_info,$federal_type,$compliance_type,$product_class,$mktg_prod_type,$bev_type)
{
	
$generated_str="";

	foreach($product_info as $each_product_name=>$val)
	{
		if($val==1)
		{	
		  $implode_arr=join("','",$$each_product_name); 
		  $generated_str=$generated_str.$table_name.".".$each_product_name." in  ('".$implode_arr."') and ";
		}
		
	}
	
	return substr($generated_str, 0, -4);
}


function generated_validation_query_v3($table_name,$checked_product_is_linked_to_supplier)
{
$generated_str="";
	if($checked_product_is_linked_to_supplier==1)
	{	
	$generated_str=" $table_name.supplier_id in (select mhw_app_prod_supplier.supplier_id from mhw_app_prod_supplier where mhw_app_prod_supplier.active=1 and mhw_app_prod_supplier.supplier_id>0) ";
	}
return $generated_str;	
}


function generated_validation_query_v51($table_name,$product_info)
{	
    $generated_str="";
    $implode_arr="'".join("','",$product_info)."'";	
	$generated_str=" and i4.requirement_met=1 and reverse(left(reverse(i4.image_name), charindex('.', reverse(i4.image_name)))) in ($implode_arr) ";

	return $generated_str;
}


function generated_validation_query_6($table_name,$product_info)
{
unset($product_info["vintage"]);
$generated_str="";

	foreach($product_info as $each_product_name=>$val)
	{
		if($val==1)
		{
		  $generated_str=$generated_str.$table_name.".".$each_product_name. " is not null and ".$table_name.".".$each_product_name."!='' and ";
		}
		
	}
	
	return substr($generated_str, 0, -4);
}


?>