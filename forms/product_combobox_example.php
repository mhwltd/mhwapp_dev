<?php
session_start();
include("prepend.php");
include("settings.php");

if(!isset($_SESSION['mhwltdphp_user'])){

	header("Location: login.php");
}
else{
	include('head.php');

	if(isset($_POST['prodID'])){
		$prodID = $_POST['prodID'];
		$product_mhw_code = $_POST['product_mhw_code'];
		$product_mhw_code_search = $_POST['product_mhw_code_search'];
		$pf_client_name = $_POST['tfa_client_name'];
		$pf_brand_name = $_POST['tfa_brand_name'];
		$pf_product_desc = $_POST['tfa_product_desc'];

		echo "<pre>";
		var_dump($_REQUEST); 
		echo "</pre>";

	}

?>

		<script>
		$(document).ready(function(){
			/*======BEGIN INITIAL PAGE LOAD======*/
			
			//if product id is passed to page, preload with data
			var prodIDinit = $("#prodID").val();
			if(parseInt(prodIDinit) > 0){
				prefill_prod(prodIDinit);

				cbb_formatter();
			}

			/*======END INITIAL PAGE LOAD======*/

			/*======BEGIN comboboxes==========*/
			function cbb_formatter(){ 
				//$('#tfa_brand_name').addClass('combobox_restyle');
				//$('#tfa_brand_name').css( "border-radius", "0px" );
				$('ul.ac_results').css("margin-left","0em");
				$('ul.ac_results').css("margin","0em");
				$('.ac_button').css("height","28px");
				$('.ac_button').css("background","transparent");
				$('.ac_button').css("margin-left","-35px");
				$('.ac_button').css("border","0px");
				$('.ac_subinfo dl').css("margin-left","0em");
				$('.ac_subinfo dl').css("margin","0em");
			}
			//load form fields - as a result of selecting product_desc combobox
			function prefill_prod(prodID){
				var qrytype = "prodByID";
				var client = $('#tfa_client_name').val(); //get value of the client combobox
				$.post('query-request.php', {qrytype:qrytype,prodID:prodID,client:client}, function(dataPx){ // ajax request query-request.php, send brand as post variable, return dataX variable, parse into JSON

					var t = JSON.parse(dataPx);

					$.each( t, function( key, value ) {
						//product

						$('#product_mhw_code').val(value.product_mhw_code);
						$('#product_mhw_code_search').val(value.product_mhw_code_search);	
	
					});
				});
			}
			function isProdValidSelection(){
				var isProdID = $('#tfa_product_desc')[0].hasAttribute("sub_info"); //free-type data will not have sub_info attribute
				if(isProdID){
					var json = $('#tfa_product_desc').attr('sub_info');
					//if additional sub_info elements are added, additional apostrophe replacements need to be added
					var json2 = json.replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"');
					var jsonObj = JSON.parse(json2);

					$("#prodID").val(jsonObj.id);  //set field for processing update
					$("#tfa_399-L").html("Product: editing product_id "+jsonObj.id); //display message for update
					prefill_prod(jsonObj.id); //prefill form fields
				}
				else{
					$("#tfa_399-L").html("Product: new entry"); //display message for insert
					$("#prodID").val("");  //clear field for insert
				}
			}

			//initialize brand combobox
			function initialize_brand(){
				$('#tfa_brand_name').parent().find(".ac_button").remove();  //clear elements of previous instance
				$('#tfa_brand_name').parent().find(".ac_result_area").remove();  //clear elements of previous instance
				var client = $('#tfa_client_name').val(); //get value of the client combobox
				var cbbtype = 'brand';
				$.post('select-request.php', {cbbtype:cbbtype,client:client}, function(dataX){ // ajax request select-request.php, send the client POST variable, return dataX variable, parse as JSON
					var data = JSON.parse(dataX);
					$(function() {
						$('#tfa_brand_name').ajaxComboBox(
							data
						);
						cbb_formatter(); //apply css and classname changes to combobox elements
					});
				});
				$('#tfa_brand_name').bind("change", function() {
					initialize_product();

					isProdValidSelection();

					cbb_formatter(); //apply css and classname changes to combobox elements
				});
			}
			initialize_brand();
			
			//initialize product combobox
			function initialize_product(){
				$('#tfa_product_desc').parent().find(".ac_button").remove();  //clear elements of previous instance
				$('#tfa_product_desc').parent().find(".ac_result_area").remove();  //clear elements of previous instance
				var client = $('#tfa_client_name').val(); //get value of the client combobox
				var brand = $('#tfa_brand_name').val(); //get value of the brand combobox
				var cbbtype = 'product';
				$.post('select-request.php', {cbbtype:cbbtype,client:client,brand:brand}, function(dataX){ // ajax request select-request.php, send brand as post variable, return dataX variable, parse into JSON
					var data = JSON.parse(dataX);
					$(function() {
						$('#tfa_product_desc').ajaxComboBox(
							data,
							{
							  bind_to: 'tfa_product_desc',
							  sub_info: true,
							  sub_as: {
								id: 'Product ID',
								code: 'MHW Product Code'
							  },
							}
						).bind('tfa_product_desc', function() {
						  var json = $(this).attr('sub_info');
						  //if additional sub_info elements are added, additional apostrophe replacements need to be added
						  var json2 = json.replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"');
						  var jsonObj = JSON.parse(json2);

						  if(parseInt(jsonObj.id) > 0){
							$("#prodID").val(jsonObj.id); //set field for processing update
							$("#tfa_399-L").html("Product: editing product_id "+jsonObj.id); //display message for update
							prefill_prod(jsonObj.id); //prefill form fields
						  }
						  isProdValidSelection();

						});
						cbb_formatter(); //apply css and classname changes to combobox elements
					});
				});
				$('#tfa_product_desc').bind("change", function(){ 
					isProdValidSelection();
				});
			}
			initialize_product();

			//client combobox >> brand combobox
			$('#tfa_client_name').change(function(){ //if client value changes
				//replace combobox elements with original input
				$('#tfa_brand_name').parent().parent().html("<input type=\"text\" id=\"tfa_brand_name\" name=\"tfa_brand_name\" value=\"\" aria-required=\"true\" title=\"Brand Name\" data-dataset-allow-free-responses=\"\" class=\"required\">");
				//replace combobox elements with original input
				$('#tfa_product_desc').parent().parent().html("<input type=\"text\" id=\"tfa_product_desc\" name=\"tfa_product_desc\" value=\"\" aria-required=\"true\" title=\"Product Name\" data-dataset-allow-free-responses=\"\" class=\"required\">");

				initialize_brand();
				
				//re-initialize product combobox
				initialize_product();

				isProdValidSelection();

				cbb_formatter(); //apply css and classname changes to combobox elements
			});
			/*======END comboboxes==========*/
			//clear form
			$("#clearform").click(function() {		
				$('#prodID').val('');
				$('#product_mhw_code').val('');
				$('#product_mhw_code_search').val('');
	
				$("#tfa_399-L").html("Product: new entry"); //display message for insert

				//replace combobox elements with original input
				$('#tfa_brand_name').parent().parent().html("<input type=\"text\" id=\"tfa_brand_name\" name=\"tfa_brand_name\" value=\"\" aria-required=\"true\" title=\"Brand Name\" data-dataset-allow-free-responses=\"\" class=\"required\">");
				//replace combobox elements with original input
				$('#tfa_product_desc').parent().parent().html("<input type=\"text\" id=\"tfa_product_desc\" name=\"tfa_product_desc\" value=\"\" aria-required=\"true\" title=\"Product Name\" data-dataset-allow-free-responses=\"\" class=\"required\">");

				initialize_brand();

				initialize_product();

				cbb_formatter();

			});
		});
    </script>

    <link href="css/wforms-layout_v530-14.css" rel="stylesheet" type="text/css" />
    <link href="css/theme-52661.css" rel="stylesheet" type="text/css" />
    <link href="css/wforms-jsonly_v530-14.css" rel="alternate stylesheet" title="This stylesheet activated by javascript" type="text/css" />
	<link href="css/wforms-inline.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/wforms_v530-14.js"></script>
    <script type="text/javascript" src="js/localization-en_US_v530-14.js"></script>         
    
</head>
<body class="default wFormWebPage">
<div class="container-fluid">
	<div id="tfaContent">
		<div class="wFormContainer" style="max-width: 85%; width:auto;" >
			<div class="row">
				<div class="col-sm-12">

    				<div class="wFormHeader"></div>
    		
					<h3 class="wFormTitle" id="4690986-T">Product Combobox Example</h3>
					<form method="post" action="product_combobox_example.php" class="hintsBelow labelsAbove" id="4690986" role="form" enctype="multipart/form-data">

					<div class="float-right"><button type="reset" value="Reset" id="clearform" class="btn btn-sm btn-outline-danger">Clear Form</button></div>

					<div class="oneField field-container-D    " id="tfa_client_name-D">
						<label id="tfa_client_name-L" class="label preField " for="tfa_client_name"><b>Client Name</b></label><br>
						<div class="inputWrapper">
							<select id="tfa_client_name" name="tfa_client_name" title="Client Name" aria-required="true" class="required">
							<?php
							$clients = explode(";",$_SESSION['mhwltdphp_userclients']);
							
							foreach ($clients as &$clientvalue) {
								if(isset($pf_client_name) && $pf_client_name==$clientvalue){
									echo "<option value=\"".$clientvalue."\" class=\"\" selected>".$clientvalue."</option>";
								}
								else{
										echo "<option value=\"".$clientvalue."\" class=\"\">".$clientvalue."</option>";
								}
							}
							?>					
							</select>
						</div>
						
					</div>
					
	
					<!--BEGIN PRODUCT-->				
					<fieldset id="tfa_399" class="section" >
					<?php
						if($prodID > 0){ echo "<legend id=\"tfa_399-L\">Product: editing product_id ".$prodID."</legend>"; }
						else{  echo "<legend id=\"tfa_399-L\">Product: new entry</legend>"; }
					?>
					<div id="tfa_856" class="section inline group">
						<div class="oneField field-container-D    " id="tfa_brand_name-D">
							<label id="tfa_brand_name-L" class="label preField reqMark" for="tfa_brand_name">Brand Name</label><br>
							<div class="inputWrapper"><input type="text" id="tfa_brand_name" name="tfa_brand_name" value="<?php echo $pf_brand_name; ?>" aria-required="true" title="Brand Name" data-dataset-allow-free-responses="" class="required"></div>
						</div>
						<div class="oneField field-container-D    " id="tfa_product_desc-D">
							<label id="tfa_product_desc-L" class="label preField reqMark" for="tfa_product_desc">Product Name</label><br>
							<div class="inputWrapper"><input type="text" id="tfa_product_desc" name="tfa_product_desc" value="<?php echo $pf_product_desc; ?>" aria-required="true" title="Product Name" data-dataset-allow-free-responses="" class="required"></div>
						</div>
						
					</div>
					</fieldset>
					<!---END PRODUCT-->	
					
					<div class="actions" id="4690986-A"><input type="submit" data-label="Submit" class="primaryAction" id="submit_button" value="Submit"></div>
					<div style="clear:both"></div>
					
					<input type="hidden" name="prodID" id="prodID" value="<?php echo $prodID; ?>">
					<input type="hidden" name="product_mhw_code" id="product_mhw_code" value="<?php echo $product_mhw_code; ?>">
					<input type="hidden" name="product_mhw_code_search" id="product_mhw_code_search" value="<?php echo $product_mhw_code_search; ?>">
					<input type="hidden" name="userID" id="userID" value="<?php echo $_SESSION['mhwltdphp_user']; ?>">

					</form>
				</div>
			</div>
		</div>    
	</div>

	<script src='js/iframe_resize_helper_internal.js'></script>
</div>
</body>
</html>
<?php
}
?>