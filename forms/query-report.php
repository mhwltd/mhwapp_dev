<?php
include('settings.php');
include('functions.php');
error_reporting(E_ALL); //displays an error

session_start();
if(!isset($_SESSION['mhwltdphp_user'])){
	die( "Not authenticated !" );  
}
$isAdmin = in_array($_SESSION['mhwltdphp_usertype'], ['ADMIN','SUPERUSER']);
include("dbconnect.php");

//Establishes the connection
$conn = sqlsrv_connect($serverName, $connectionOptions);
if( $conn === false) {
		die( print_r( sqlsrv_errors(), true));
}

if($_SESSION['mhwltdphp_user']!='' && $_POST['qrytype']!=''){
	$trksql= "INSERT INTO [mhw_app_workflow] VALUES ('".$_POST['qrytype']."', 0, GETDATE(), GETDATE(), '".$_SESSION['mhwltdphp_user']."', 1, 0)";
	$trkResults= sqlsrv_query($conn, $trksql);
}

if($_POST['qrytype']=='prodByID')
{
	if(isset($_POST['prodID']) && $_POST['prodID']!=='' && isset($_POST['client']) && $_POST['client']!=='')
	{
		//$tsql= "SELECT p.[product_id],p.[client_name],p.[brand_name],p.[product_desc],p.[product_mhw_code],p.[product_mhw_code_search],p.[TTB_ID] as [TTB],p.[federal_type],p.[compliance_type],CASE WHEN p.[product_class] = 'Foreign' THEN 'Imported' ELSE p.[product_class] END AS [product_class],p.[mktg_prod_type],p.[bev_type],p.[fanciful],p.[country],p.[appellation],p.[lot_item],p.[bottle_material],p.[alcohol_pct],s.[supplier_id],s.[supplier_name],s.[supplier_contact],s.[supplier_fda_number],s.[tax_reduction_allocation],s.[supplier_address_1],s.[supplier_address_2],s.[supplier_address_3],s.[supplier_city],s.[supplier_state],s.[supplier_country],s.[supplier_zip],s.[supplier_phone],s.[supplier_email],s.[federal_basic_permit],(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[requirement_met] = 1 AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount' FROM [dbo].[mhw_app_prod] p left outer join [mhw_app_prod_supplier] s on s.[supplier_id] = p.[supplier_id] WHERE p.[product_id] = ".$_POST['prodID']." and p.[client_name] IN ('".$_POST['client']."') AND p.[active] = 1 and p.[deleted] = 0";
		
		$tsql= "EXEC [dbo].[usp_query_request] @querytype = '".$_POST['qrytype']."', @client = '".$_POST['client']."', @prodID = ".$_POST['prodID'].", @suppID = 0";
		//echo $tsql;
	}
}
if($_POST['qrytype']=='suppByID')
{
	if(isset($_POST['suppID']) && $_POST['suppID']!=='')
	{
		$tsql= "SELECT s.[supplier_name],s.[supplier_contact],s.[supplier_fda_number],s.[tax_reduction_allocation],s.[supplier_address_1],s.[supplier_address_2],s.[supplier_address_3],s.[supplier_city],s.[supplier_state],s.[supplier_country],s.[supplier_zip],s.[supplier_phone],s.[supplier_email],s.[federal_basic_permit] FROM [mhw_app_prod_supplier] s WHERE s.[supplier_id] = ".$_POST['suppID']." AND s.[active] = 1 and s.[deleted] = 0";
		//echo $tsql;
	}
}
if($_POST['qrytype']=='VPdefault'){
	if(isset($_POST['clientlist']) && $_POST['clientlist']!=='')
	{
		$tsql= "EXEC [dbo].[usp_query_request] @querytype = '".$_POST['qrytype']."', @client = ".$_POST['clientlist'].", @prodID = 0, @suppID = 0";

		//$tsql= "SELECT p.[product_id],p.[client_code],p.[client_name],p.[brand_name],p.[product_desc],p.[product_mhw_code],p.[product_mhw_code_search],p.[TTB_ID],p.[federal_type],p.[compliance_type],p.[product_class],p.[mktg_prod_type],p.[bev_type],p.[fanciful],p.[country],p.[appellation],p.[lot_item],p.[bottle_material],p.[alcohol_pct],p.[create_date],s.[supplier_id],s.[supplier_name],s.[supplier_contact],s.[supplier_fda_number],s.[tax_reduction_allocation],s.[supplier_address_1],s.[supplier_address_2],s.[supplier_address_3],s.[supplier_city],s.[supplier_state],s.[supplier_country]  ,s.[supplier_zip]  ,s.[supplier_phone] ,s.[supplier_email], s.[federal_basic_permit] ,s.[create_via] ,s.[create_date] ,s.[edit_date] ,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_item] i WHERE i.[product_id] = p.[product_id] AND i.[active] = 1 AND i.[deleted] = 0) as 'itemcount' ,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount' FROM [dbo].[mhw_app_prod] p WITH (NOLOCK) LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] s WITH (NOLOCK) on s.[supplier_id] = p.[supplier_id] WHERE p.[client_name] IN (".$_POST['clientlist'].") AND p.[active] = 1 AND p.[deleted] = 0 /* AND s.[active] = 1 AND s.[deleted] = 0 */ ORDER BY p.[create_date] desc";
	}
}
if($_POST['qrytype']=='VPitems'){
	if(isset($_POST['clientlist']) && $_POST['clientlist']!=='')
	{
		$tsql= "EXEC [dbo].[usp_query_request] @querytype = '".$_POST['qrytype']."', @client = ".$_POST['clientlist'].", @prodID = 0, @suppID = 0";
		
		//$tsql= "SELECT i.[item_id],i.[item_client_code],i.[item_mhw_code],i.[item_description],i.[container_type],i.[container_size],i.[stock_uom],i.[bottles_per_case],i.[upc],i.[scc],i.[vintage],i.[various_vintages],i.[height],i.[length],i.[width],i.[weight],i.[item_status],i.[create_via],i.[create_date],i.[edit_date],p.[product_id],p.[client_code],p.[client_name],p.[brand_name],p.[product_desc],p.[product_mhw_code] FROM [dbo].[mhw_app_prod_item] i WITH (NOLOCK) left outer join [dbo].[mhw_app_prod] p WITH (NOLOCK) on p.[product_id] = i.[product_id] WHERE p.[client_name] IN (".$_POST['clientlist'].") AND p.[active] = 1 AND p.[deleted] = 0 AND i.[active] = 1 AND i.[deleted] = 0 ORDER BY p.[product_id], i.[create_date] desc";
	}
}
if($_POST['qrytype']=='VPproditems'){
	if(isset($_POST['prodID']) && $_POST['prodID']!=='')
	{
		$tsql= "EXEC [dbo].[usp_query_request] @querytype = '".$_POST['qrytype']."', @client = '', @prodID = ".$_POST['prodID'].", @suppID = 0";

		//$tsql= "SELECT i.[item_id],i.[item_client_code],i.[item_mhw_code],i.[item_description]	      ,i.[container_type],i.[container_size],i.[stock_uom],i.[bottles_per_case],i.[upc],i.[scc],i.[vintage],i.[various_vintages],i.[height],i.[length],i.[width],i.[weight],i.[item_status],i.[create_via],i.[create_date],i.[edit_date],p.[product_id],p.[client_code],p.[client_name],p.[brand_name],p.[product_desc],p.[product_mhw_code] FROM [dbo].[mhw_app_prod_item] i WITH (NOLOCK) left outer join [dbo].[mhw_app_prod] p WITH (NOLOCK) on p.[product_id] = i.[product_id] WHERE i.[product_id] = '".$_POST['prodID']."' AND p.[active] = 1 AND p.[deleted] = 0 AND i.[active] = 1 AND i.[deleted] = 0 ORDER BY p.[product_id], i.[create_date] desc";
	}
}
if($_POST['qrytype']=='VPunfinalized'){
	if(isset($_POST['clientlist']) && $_POST['clientlist']!=='')
	{
		
		$tsql= "EXEC [dbo].[usp_query_request] @querytype = '".$_POST['qrytype']."', @client = ".$_POST['clientlist'].", @prodID = 0, @suppID = 0";

		//$tsql= "SELECT p.[product_id],p.[client_code],p.[client_name],p.[brand_name],p.[product_desc],p.[product_mhw_code],p.[product_mhw_code_search],p.[TTB_ID],p.[federal_type],p.[compliance_type],p.[product_class],p.[mktg_prod_type],p.[bev_type],p.[fanciful],p.[country],p.[appellation],p.[lot_item],p.[bottle_material],p.[alcohol_pct],p.[create_date],s.[supplier_id],s.[supplier_name],s.[supplier_contact],s.[supplier_fda_number],s.[tax_reduction_allocation],s.[supplier_address_1],s.[supplier_address_2],s.[supplier_address_3],s.[supplier_city],s.[supplier_state],s.[supplier_country]  ,s.[supplier_zip]  ,s.[supplier_phone] ,s.[supplier_email] ,s.[federal_basic_permit] ,s.[create_via] ,s.[create_date] ,s.[edit_date] ,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_item] i WHERE i.[product_id] = p.[product_id] AND i.[active] = 1 AND i.[deleted] = 0) as 'itemcount' ,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount' FROM [dbo].[mhw_app_prod] p WITH (NOLOCK) LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] s WITH (NOLOCK) on s.[supplier_id] = p.[supplier_id] WHERE p.[client_name] IN (".$_POST['clientlist'].") AND p.[finalized] = 0 AND p.[processed] = 0 AND p.[active] = 1 AND p.[deleted] = 0 AND s.[active] = 1 AND s.[deleted] = 0 ORDER BY p.[create_date] desc";
	}
}
if($_POST['qrytype']=='VPpending'){
	if(isset($_POST['clientlist']) && $_POST['clientlist']!=='')
	{
		$tsql= "EXEC [dbo].[usp_query_request] @querytype = '".$_POST['qrytype']."', @client = ".$_POST['clientlist'].", @prodID = 0, @suppID = 0";
		
		//$tsql= "SELECT p.[product_id],p.[client_code],p.[client_name],p.[brand_name],p.[product_desc],p.[product_mhw_code],p.[product_mhw_code_search],p.[TTB_ID],p.[federal_type],p.[compliance_type],p.[product_class],p.[mktg_prod_type],p.[bev_type],p.[fanciful],p.[country],p.[appellation],p.[lot_item],p.[bottle_material],p.[alcohol_pct],p.[create_date],s.[supplier_id],s.[supplier_name],s.[supplier_contact],s.[supplier_fda_number],s.[tax_reduction_allocation],s.[supplier_address_1],s.[supplier_address_2],s.[supplier_address_3],s.[supplier_city],s.[supplier_state],s.[supplier_country]  ,s.[supplier_zip]  ,s.[supplier_phone] ,s.[supplier_email] ,s.[federal_basic_permit] ,s.[create_via] ,s.[create_date] ,s.[edit_date] ,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_item] i WHERE i.[product_id] = p.[product_id] AND i.[active] = 1 AND i.[deleted] = 0) as 'itemcount' ,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount' FROM [dbo].[mhw_app_prod] p WITH (NOLOCK) LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] s WITH (NOLOCK) on s.[supplier_id] = p.[supplier_id] WHERE p.[client_name] IN (".$_POST['clientlist'].") AND p.[finalized] = 1 AND p.[processed] = 0 AND p.[active] = 1 AND p.[deleted] = 0 /* AND s.[active] = 1 AND s.[deleted] = 0 */ ORDER BY p.[create_date] desc";
	}
}
if($_POST['qrytype']=='VPfinalized'){
	if(isset($_POST['clientlist']) && $_POST['clientlist']!=='')
	{
		$tsql= "EXEC [dbo].[usp_query_request] @querytype = '".$_POST['qrytype']."', @client = ".$_POST['clientlist'].", @prodID = 0, @suppID = 0";
		
		//$tsql= "SELECT p.[product_id],p.[client_code],p.[client_name],p.[brand_name],p.[product_desc],p.[product_mhw_code],p.[product_mhw_code_search],p.[TTB_ID],p.[federal_type],p.[compliance_type],p.[product_class],p.[mktg_prod_type],p.[bev_type],p.[fanciful],p.[country],p.[appellation],p.[lot_item],p.[bottle_material],p.[alcohol_pct],p.[create_date],s.[supplier_id],s.[supplier_name],s.[supplier_contact],s.[supplier_fda_number],s.[tax_reduction_allocation],s.[supplier_address_1],s.[supplier_address_2],s.[supplier_address_3],s.[supplier_city],s.[supplier_state],s.[supplier_country]  ,s.[supplier_zip]  ,s.[supplier_phone] ,s.[supplier_email] ,s.[federal_basic_permit] ,s.[create_via] ,s.[create_date] ,s.[edit_date] ,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_item] i WHERE i.[product_id] = p.[product_id] AND i.[active] = 1 AND i.[deleted] = 0) as 'itemcount' ,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount' FROM [dbo].[mhw_app_prod] p WITH (NOLOCK) LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] s WITH (NOLOCK) on s.[supplier_id] = p.[supplier_id] WHERE p.[client_name] IN (".$_POST['clientlist'].") AND p.[finalized] = 1 AND p.[processed] = 1 AND p.[active] = 1 AND p.[deleted] = 0 /* AND s.[active] = 1 AND s.[deleted] = 0 */ ORDER BY p.[create_date] desc";
	}
}
if($_POST['qrytype']=='STAT_COUNT_unfinalized'){
	if(isset($_POST['clientlist']) && $_POST['clientlist']!=='')
	{
		$tsql= "SELECT COUNT(*) as 'unfinalizedCount' FROM [dbo].[mhw_app_prod] p WITH (NOLOCK) LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] s WITH (NOLOCK) on s.[supplier_id] = p.[supplier_id] WHERE p.[client_name] IN ('".str_replace(";","','",$_POST['clientlist'])."') AND p.[finalized] = 0 AND p.[processed] = 0 AND p.[active] = 1 AND p.[deleted] = 0 /* AND s.[active] = 1 AND s.[deleted] = 0 */";
	}
}
if($_POST['qrytype']=='processFinalize'){
	if(isset($_POST['prodlist']) && $_POST['prodlist']!=='')
	{
		$ProdIDString =  str_replace("]","",str_replace("[","",$_POST['prodlist']));
		
		$tsql= "UPDATE [dbo].[mhw_app_prod] SET [finalized] = 1 WHERE [product_id] IN (".$ProdIDString.")";
		//echo  $tsql;
	}
}
if($_POST['qrytype']=='alertFinalize'){
	if(isset($_POST['prodlist']) && $_POST['prodlist']!=='')
	{
		$ProdIDString =  str_replace("]","",str_replace("[","",$_POST['prodlist']));
		$ProdCodeString =  str_replace("]","",str_replace("[","",$_POST['codelist']));
		
		$tsql= "EXEC [dbo].[usp_product_finalize] '".$_POST['qrytype']."','".$ProdIDString."',".$_POST['clientlist'].",'".$ProdCodeString."','".$siteroot."','".$_SESSION['mhwltdphp_user']."','".$_SESSION['mhwltdphp_useremail']."'";
		//echo  $tsql;
	}
}
if($_POST['qrytype']=='itemByID')
	{
	if(isset($_POST['itemID']) && $_POST['itemID']!=='')
	{
		$itemID = intval($_POST['itemID']);
		$tsql= "
		SELECT TOP 1 [item_id]
			,[product_id]
			,[container_type]
			,[container_size]
			,[stock_uom]
			,[bottles_per_case]
			,[upc]
			,[scc]
			,[vintage]
			,[various_vintages]
			,[item_status]
			,[active]
			,[deleted]
			,[height]
			,[length]
			,[width]
			,[weight]
		FROM [dbo].[mhw_app_prod_item]
		WHERE [item_id] = $itemID";
	}
}
if ($_POST['qry_type']=='pricing'){
	if(isset($_POST['view_type']) && $_POST['view_type']!=='' && isset($_POST['view_state']) && $_POST['view_state']!=='' && isset($_POST['client']) && $_POST['client']!=='')
	{
		$limitRecords = 1000; // for every item
		//$whereItem = "";
		$whereArr = [];
		$whereSubArr = [];

		//$whereMonth = "";
		if(isset($_POST['row_id']) && $_POST['row_id']!=='') {
			$whereSubArr[] = "ID = ".intval($_POST['row_id']);
		}
		if(isset($_POST['itemID']) && $_POST['itemID']!=='') {
			$whereSubArr[] = "itemID = ".intval($_POST['itemID']);
			//$whereItem = "WHERE itemID = ".intval($_POST['itemID']);
			$limitRecords = 1;
		}
		if (isset($_POST['effective_month'])) {
			$effective_month = str_replace("($300 fee) ","",addslashes($_POST['effective_month']));
			$whereSubArr[] = "tfa_effective_month LIKE '$effective_month'";
			//$whereMonth = "WHERE tfa_effective_month LIKE '$effective_month' ";
		}
		if ($_POST['view_type']=='Open States' || $_POST['view_state']=='tfa_os') {
			$tableName = 'tfa_os';
			//$state = str_replace("'","\'",$_REQUEST['view_state']);
			$state = addslashes($_REQUEST['view_state']);
			if ($state == "Deleted" || $_POST['view_type']=='Deleted Items') {
				$whereArr[] = "l.deleted = 1";
			} else {
				$whereArr[] = "ISNULL(l.deleted, 0) <> 1";
			}
			if ($state != "All" && $state != "Deleted" && $state != "tfa_os") {
				$whereArr[] = "tfa_os_price_state = '".addslashes($state)."'";
			}
			
			$where = implode(" AND ", $whereArr);
			$whereSub = count($whereSubArr)>0 ? "WHERE ".implode(" AND ", $whereSubArr) : "";

			$tsql = "SELECT l.*,  convert(varchar, l.created_at, 120) AS created,
							i.item_description, i.container_size, i.bottles_per_case, i.vintage
						FROM [dbo].[$tableName] AS l
						LEFT JOIN mhw_app_prod_item i
						ON i.item_id = l.itemID
						LEFT JOIN [dbo].[mhw_app_prod] p WITH (NOLOCK) 
						ON p.[product_id] = i.[product_id]
						WHERE l.ID IN (
							SELECT TOP $limitRecords ID FROM (
								SELECT itemID, tfa_effective_month, tfa_os_price_state, MAX(ID) AS ID
								FROM [dbo].[$tableName] AS l
								$whereSub
								GROUP BY itemID, tfa_effective_month, tfa_os_price_state
							) subtbl
							ORDER BY ID DESC
						)
						AND p.[client_name] = '".addslashes($_POST['client'])."'
						AND $where
						ORDER BY i.item_description, tfa_os_price_state DESC
						";
		}else{

			$tableName = addslashes($_POST['view_state']);
			$fobField = $tableName."_fob";
			//$where = "AND l.".$tableName."_change_type <> 'Delete' ";
			if ($_POST['view_type']=='Deleted Items' ) {
				$whereArr[] = "l.deleted = 1";
			} else{
				$whereArr[] = "ISNULL(l.deleted, 0) <> 1 ";
	}

			$whereSub = count($whereSubArr)>0 ? "WHERE ".implode(" AND ", $whereSubArr) : "";
			$where = implode(" AND ", $whereArr);

			$tsql = "SELECT *,  convert(varchar, created_at, 120) AS created, 
							i.item_description, i.container_size, i.bottles_per_case, i.vintage
						FROM [dbo].[$tableName] AS l
						LEFT JOIN mhw_app_prod_item i
						ON i.item_id = l.itemID
						LEFT JOIN [dbo].[mhw_app_prod] p WITH (NOLOCK) 
						ON p.[product_id] = i.[product_id]
						WHERE l.ID IN (
							SELECT TOP $limitRecords ID FROM (
								SELECT [itemID], [tfa_effective_month], [$fobField], MAX(ID) AS ID
								FROM [dbo].[$tableName] AS l
								$whereSub
								GROUP BY [itemID], [tfa_effective_month], [$fobField] 
							) subtbl
							ORDER BY ID DESC
						)
						AND p.[client_name] = '".$_POST['client']."'
						AND $where
						ORDER BY i.item_description DESC
						";
		};
	}
	//echo( $tsql);exit;
}


if ($_POST['qry_type']=='blr-refresh') {

	$refreshStatusUrl = "https://mhwltd.app/sc_api/curl_sc_GetFederalRegistrationStatusByCola.php";
	$refreshStatusUrl .= "?output=responseonly";
	$refreshStatusUrl .= "&lkID=".urlencode($_POST['lkID']);
	$refreshStatusUrl .= "&ccID=".urlencode($_POST['ccID']);
	$ch = curl_init(); 
	curl_setopt($ch, CURLOPT_URL, $refreshStatusUrl); 
	//curl_setopt($ch, CURLOPT_POST, count($fields));
	//curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
	//return the transfer as a string 
	//curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	// $output contains the output string 
	$output = curl_exec($ch); 
	// close curl resource to free up system resources 
	curl_close($ch);      
	echo $refreshStatusUrl." - ".$output; exit;
}

if ($_POST['qry_type']=='distributors-list') {
	
	if (empty($_POST['state_abbrev'])) {
		echo("Wrong parameters !"); exit;
	}

	$state_abbrev = addslashes($_POST['state_abbrev']);

    $tsql = "SELECT [DistKey], [Name] FROM [dbo].[sc_distributor-list]
			WHERE [State] = '$state_abbrev'
			ORDER BY [Name]
	";
	//echo $tsql;exit;
	//$stmt = sqlsrv_prepare( $conn, $tsql, $params);
}

$product_id = intval($_POST['product_id']);
$item_id = intval($_POST['item_id']);
$supplier_id = intval($_POST['supplier_id']);

if ($_POST['qry_type']=='productArchive') {

	$where = "";
	$whereArr = array();
	
	if ($product_id > 0) {
		$whereArr[] = "a.product_id = ".$product_id;
	}

	if ($item_id > 0) {
		//$whereArr[] = "i.item_id = ".$item_id;
		$whereArr[] = "1=0";
	}

	if ($supplier_id > 0) {
		//$whereArr[] = "ps.supplier_id = ".$supplier_id;
		$whereArr[] = "1=0";
	} 

	if (count($whereArr)>0) {
		$where = "WHERE ".implode(" AND ", $whereArr);
	}

    $tsql = "SELECT DISTINCT a.[product_id]
				,p.*
				,a.[field]
				,a.[previous_value]
				,convert(varchar, DATEADD(HOUR,-4,a.[edit_date]), 120) as edit_date
				,a.[edit_user]
			FROM [dbo].[mhw_app_prod_archive] a
			LEFT JOIN [dbo].[mhw_app_prod] p
			ON p.product_id = a.product_id
			LEFT OUTER JOIN [dbo].[mhw_app_prod_item] i 
			ON i.product_id = p.product_id 
			LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] ps 
			ON ps.product_id = a.product_id 
			$where
			ORDER BY convert(varchar, DATEADD(HOUR,-4,a.[edit_date]), 120) DESC
	";
	//echo $tsql;exit;
}

if ($_POST['qry_type']=='itemArchive') {

	$where = "";
	$whereArr = array();
	
	if ($product_id > 0) {
		// $whereArr[] = "i.product_id = ".$product_id;
		// if calling item then show only items
		$whereArr[] = "1=0";
	}

	if ($item_id > 0) {
		$whereArr[] = "a.item_id = ".$item_id;
	}

	if ($supplier_id > 0) {
		//$whereArr[] = "ps.supplier_id = ".$supplier_id;
		$whereArr[] = "1=0";
	} 

	if (count($whereArr)>0) {
		$where = "WHERE ".implode(" AND ", $whereArr);
	}

    $tsql = "SELECT a.[item_id]
				,i.*
				,a.[field]
				,a.[previous_value]
				,convert(varchar, a.[edit_date], 120) as edit_date
				,a.[edit_user]
			FROM [dbo].[mhw_app_prod_item_archive] a
			LEFT JOIN [dbo].[mhw_app_prod_item] i
			ON i.item_id = a.item_id
			LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] ps
			ON ps.product_id = i.product_id
			$where
			ORDER BY a.[edit_date] DESC
	";
	//echo $tsql;exit;
}

if ($_POST['qry_type']=='supplierArchive') {

	$where = "";
	$whereArr = array();

	if ($supplier_id > 0) {
		$whereArr[] = "a.supplier_id = ".$supplier_id;
	} 

	if ($product_id > 0) {
		$whereArr[] = "ps.product_id = ".$product_id;
	}

	if ($item_id > 0) {
		$whereArr[] = "ip.item_id = ".$item_id;
	}

	if (count($whereArr)>0) {
		$where = "WHERE ".implode(" AND ", $whereArr);
	}

    $tsql = "SELECT a.[supplier_id]
				,s.[supplier_name]
				,a.[field]
				,a.[previous_value]
				,convert(varchar, a.[edit_date], 120) as edit_date
				,a.[edit_user]
			FROM [dbo].[mhw_app_prod_supplier_archive] a
			LEFT JOIN [dbo].[mhw_suppliers] s
			ON s.id = a.supplier_id
			LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] ps
			ON ps.supplier_id = a.supplier_id
			LEFT OUTER JOIN [dbo].[mhw_app_prod_item] ip
			ON ps.product_id = ip.product_id
			$where
			GROUP BY a.[edit_date]
				,a.[supplier_id]
				,s.[supplier_name]
				,a.[field]
				,a.[previous_value]
				,a.[edit_user]	
			ORDER BY a.[edit_date] DESC
	";
	//echo $tsql;exit;
}

if ($_POST['qry_type']=='workflow') {

	$whereArray = array();
	if ($product_id) {
		$whereArray[] = "workflow_type IN ('product_delete','product','product edit','product_image') AND record_id = $product_id";
	};
	if ($item_id) {
		$whereArray[] = "workflow_type IN ('product_item','product_item edit','item_delete') AND record_id = $item_id";
	};
	if ($supplier_id) {
		$whereArray[] = "workflow_type IN ('product_supplier','product_supplier edit','supplier_delete') AND record_id = $supplier_id";
	};

	$where = count($whereArray)>0 ? "WHERE ".implode(" AND ", $whereArray) : "";

    $tsql = "SELECT [workflow_type]
				,[record_id]
				,[created_date]
				,convert(varchar, [edit_date], 120) as edit_date
				,[username]
			FROM [dbo].[mhw_app_workflow]
			$where
			ORDER BY [edit_date] DESC
	";
}



/* ********************* added by Jobaidur :start ************************ */

if($_POST['qrytype']=='getItemDetails'){
	if(isset($_POST['prodID']) && $_POST['prodID']!=='')
	{		
		$tsql= "SELECT i.* FROM [dbo].[mhw_app_prod_item] i WITH (NOLOCK) left outer join [dbo].[mhw_app_prod] p WITH (NOLOCK) on p.[product_id] = i.[product_id]  WHERE i.[product_id] = '".$_POST['prodID']."' AND p.[active] = 1 AND p.[deleted] = 0  AND i.[active] = 1 AND i.[deleted] = 0 order by i.[create_date] desc";
	}
}

if($_POST['qrytype']=='getFileDetails'){
	if(isset($_POST['prodID']) && $_POST['prodID']!=='')
	{		
        $implode_arr="'".join("','",$limitedext)."'";	
		$tsql= "SELECT i4.*,(select count(*) as valid_file_count from [dbo].[mhw_app_prod_image] i WHERE i.[image_id] = i4.[image_id] and i.requirement_met=1 and reverse(left(reverse(i.image_name), charindex('.', reverse(i.image_name)))) in ($implode_arr)) as valid_file_ext_count FROM [dbo].[mhw_app_prod_image] i4 WITH (NOLOCK) left outer join [dbo].[mhw_app_prod] p WITH (NOLOCK) on p.[product_id] = i4.[product_id]  WHERE i4.[product_id] = '".$_POST['prodID']."' AND i4.[active] = 1 AND i4.[deleted] = 0";
	}
}

if($_POST['qrytype']=='getTtbIdInfo'){
	if(isset($_POST['prodID']) && $_POST['prodID']!=='')
	{		
		$tsql= "select p.[TTB_ID],p.[PRODUCT_ID],(select count(*) from [sc_cola_federal_sts] f where f.[cola] =SUBSTRING(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(p.TTB_ID,'-',''),'a',''),'b',''),'A',''),'B',''),0,15) and f.[status] = 'Approved') as is_valid_cola,case when (SELECT len(SUBSTRING(p2.[TTB_ID],1,14)) FROM mhw_app_prod p2 WHERE p2.[product_id]=p.[product_id] and SUBSTRING(p2.[TTB_ID],1,14) not like '%[^0-9]%' and (p2.TTB_ID != '' and p2.TTB_ID is not null))=14 then 1 else 0 end as is_valid_min_len from mhw_app_prod p where p.[product_id] = '".$_POST['prodID']."'";
	}
}


if ($_POST['qry_type']=='blr-status') {

	/*
	$tsql = "SELECT s.[blr_state_abbrev], s.[blr_state_name], s.[blr_state_note],
				p.[product_id], p.[product_desc],
				p.[brand_name], p.[product_mhw_code], p.[product_desc],
				p.[TTB_ID], p.[federal_type],
				b.[blr_id], b.[client_name],
				b.[blr_contact], b.[blr_email], b.[blr_phone],
				b.[blr_reg_type], b.[blr_reg_sub_type], b.[blr_phone],
				c.[Status]
			FROM [dbo].[mhw_app_blr] b
			LEFT JOIN [dbo].[mhw_app_blr_states] s
				ON s.[blr_id] = b.[blr_id]
			LEFT JOIN [dbo].[mhw_app_prod] p
				ON p.[product_id] = b.[product_id]
			LEFT JOIN [dbo].[sc_cola_state_sts] c
				ON c.[cola] = p.[TTB_ID]
				AND c.[client] = p.[client_code]
				AND c.[State] = s.[blr_state_name]
			WHERE p.[product_id] IS NOT NULL
				AND b.[client_name] = '".$_POST['client']."'
	";
	*/

	if (empty($_POST['view_type']) || empty($_POST['client']) ) {
		echo("Wrong parameters !"); exit;
	}

	if ($_POST['view_type'] === 'State Requests') {

		$tsql = "SELECT DISTINCT
				s.[blr_state_abbrev], s.[blr_state_name]
			FROM [dbo].[mhw_app_blr_states] s
			LEFT JOIN [dbo].[mhw_app_blr] b
				ON s.[blr_id] = b.[blr_id]
			LEFT JOIN [dbo].[mhw_app_prod] p
				ON p.[product_id] = b.[product_id]
			WHERE p.[product_id] IS NOT NULL
				AND b.[client_name] = '".addslashes($_POST['client'])."'
		";

	} else if ($_POST['view_type'] === 'Product Requests') {

		$tsql = "SELECT DISTINCT
				p.[product_id], p.[product_desc],
				p.[brand_name], p.[product_mhw_code],
				p.[TTB_ID], p.[federal_type], p.[client_code],
				f.[status] as federal_status
			FROM [dbo].[mhw_app_blr] b
			LEFT JOIN [dbo].[mhw_app_prod] p
				ON p.[product_id] = b.[product_id]
			LEFT JOIN [dbo].[sc_cola_federal_sts] f
				ON f.[cola] = p.[TTB_ID]
				AND f.[client] = b.[client_code]
			WHERE p.[product_id] IS NOT NULL
				AND b.[client_name] = '".addslashes($_POST['client'])."'
			ORDER BY p.[product_desc]
		";

	} else if ($_POST['view_type'] === 'All Products') {

		$tsql = "SELECT DISTINCT
				p.[product_id], p.[product_desc],
				p.[brand_name], p.[product_mhw_code],
				p.[TTB_ID], p.[federal_type], p.[client_code],
				f.[status] as federal_status
			FROM  [dbo].[mhw_app_prod] p
			LEFT JOIN [dbo].[sc_cola_federal_sts] f
				ON f.[cola] = p.[TTB_ID]
				AND f.[client] = p.[client_code]
			WHERE p.[product_id] IS NOT NULL
				AND p.[client_name] = '".addslashes($_POST['client'])."'
			ORDER BY p.[product_desc]
		";
	} else if ($_POST['view_type'] === 'All States') {

		$tsql = "SELECT DISTINCT
				[state_abbrev], [state_name]
			FROM [view_states]
			ORDER BY [state_name]
		";
	}
    else if ($_POST['view_type'] === 'Federal product approvals') {
		
		$client_name_qry=" AND p.[client_name] = '".addslashes($_POST['client'])."'";	
		$prod_ids_qry_part="";
		if($_POST['prodlistids']!==''&& $_POST['prodlistids']!=='[]')
		{
			$prodids =  str_replace("]","",str_replace("[","",$_POST['prodlistids']));
			$prod_ids_qry_part=" AND p.[product_id] IN (".$prodids.") ";			
		}
		
		$advance_searching_qry_part="";
		if($_POST['searching_data']!==''&& $_POST['searching_data']!=='{}')
		{
			$searching_data_val =  json_decode($_POST['searching_data']);
		    foreach($searching_data_val as $key=>$val)
			{
				if(trim($val)!='' && (substr($key, 0, 2)=='p.' || substr($key, 0, 2)=='f.'))
				{					
						if($key=='p.mhw_team'){
							$advance_searching_qry_part=$advance_searching_qry_part. " AND ISNULL((select t.[Current Team Assignment ID] from tray_email_routing t with (nolock) where t.[Client ID] = p.client_code),'') like '%".$val."%'";
						}
						else if($key=='p.compid'){
							$advance_searching_qry_part=$advance_searching_qry_part. " AND ISNULL((select c.COMPID from v_arclnt c with (nolock) where c.[Client] = p.client_code),'') like '%".$val."%'";
						}
						else{
							$advance_searching_qry_part=$advance_searching_qry_part. " AND ". $key ." like '%".$val."%'";
						}
				}
			}
			
			if(isset($searching_data_val->all_clients) && $searching_data_val->all_clients=='all_clients')
				{
					if($advance_searching_qry_part)
					{
					$client_name_qry="";
                    }					
				}			
		}
		
		
		/* REPORT FED QUERY - ONLY DISPLAY PRODUCTS WITH APPROVALS */

		$tsql = "SELECT DISTINCT
				p.[product_id]
				,CASE
				 WHEN ISNUMERIC(SUBSTRING(p.[TTB_ID],1,14))=1 AND len(SUBSTRING(p.[TTB_ID],1,14))=14 THEN SUBSTRING(p.[TTB_ID],1,14)
				 ELSE ''
				END AS prefix_ttb_id
				,p.[product_desc]
				,p.[brand_name]
				,p.[product_mhw_code]
				,p.[TTB_ID]
				,p.[federal_type]
				,p.[client_code]
				,f.[status] as federal_status
				,ISNULL(f.[status],'No Status') as status
				,convert(varchar, f.[dateapproved], 120) as dateapproved
				,p.[product_class]
				,f.[primaryfederalbasicpermitnumber]
				,p.[country]
				,f.[legalnameusedonlabel]
				,f.[applicantname]
				,convert(varchar, f.[datecompleted], 120) as datecompleted
				,convert(varchar,DATEADD(day,CAST(f.approvaleta AS int)+3, convert(smalldatetime, f.[dateofapplication])),101) as est_dt_approval
				,'' as user_notes
				,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount'
				,REPLACE(REPLACE(ISNULL(convert(varchar, f.[dateofapplication], 120),''),'NULL',''),' 12:00:00 AM','') as dateofapplication
				,(select t.[Current Team Assignment ID] from tray_email_routing t with (nolock) where t.[Client ID] = p.client_code) as 'MHWTeam'
				,(select c.COMPID from v_arclnt c with (nolock) where c.[Client] = p.client_code) as 'CompID'
			FROM  [dbo].[mhw_app_prod] p
			LEFT JOIN [dbo].[__fact_federal_cola_status_recent] f ON f.[cola] = LTRIM(RTRIM(CAST(SUBSTRING(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(p.[TTB_ID],'-',''),'a',''),'b',''),'A',''),'B',''),0,15) as varchar)))
				AND f.[client] = p.[client_code]
				AND f.active = 1
			WHERE p.[product_id] IS NOT NULL  
				AND p.active = 1
			   	$client_name_qry     
				AND f.[status] is not NULL and f.[status]!=''
				$prod_ids_qry_part $advance_searching_qry_part
			ORDER BY p.[product_desc]";
		// echo $tsql; exit;
	}
	else if ($_POST['view_type'] === 'State product approvals') {
		
		 $client_name_qry=" AND p.[client_name] = '".addslashes($_POST['client'])."'";
		 $prod_ids_qry_part="";
		if($_POST['prodlistids']!==''&& $_POST['prodlistids']!=='[]')
		{
			$prodids =  str_replace("]","",str_replace("[","",$_POST['prodlistids']));
			$prod_ids_qry_part=" AND p.[product_id] IN (".$prodids.") ";			
		}
		
		$advance_searching_qry_part="";
		if($_POST['searching_data']!==''&& $_POST['searching_data']!=='{}')
		{
			$searching_data_val =  json_decode($_POST['searching_data']);
		    foreach($searching_data_val as $key=>$val)
			{ 
				if(trim($val)!='' && (substr($key, 0, 2)=='p.' || substr($key, 0, 2)=='s.'))
				{					
						if($key=='p.mhw_team'){
							$advance_searching_qry_part=$advance_searching_qry_part. " AND ISNULL((select t.[Current Team Assignment ID] from tray_email_routing t with (nolock) where t.[Client ID] = p.client_code),'') like '%".$val."%'";
						}
						else if($key=='p.compid'){
							$advance_searching_qry_part=$advance_searching_qry_part. " AND ISNULL((select c.COMPID from v_arclnt c with (nolock) where c.[Client] = p.client_code),'') like '%".$val."%'";
						}
						else{
							$advance_searching_qry_part=$advance_searching_qry_part. " AND ". $key ." like '%".$val."%'";
						}
				}
			}
						
			if(isset($searching_data_val->all_clients) && $searching_data_val->all_clients=='all_clients')
				{
					if($advance_searching_qry_part)
					{
						$client_name_qry="";	
                    }						
				}
		}
	
		/* REPORT STATE QUERY - ONLY DISPLAY PRODUCTS WITH STATE REGISTRATIONS */
		
		$tsql = "SELECT DISTINCT
				p.[product_id]
				,p.[product_desc]
				,CASE
					WHEN ISNUMERIC(SUBSTRING(p.[TTB_ID],1,14))=1 AND len(SUBSTRING(p.[TTB_ID],1,14))=14 THEN SUBSTRING(p.[TTB_ID],1,14)
					ELSE ''
				END AS prefix_ttb_id
				,p.[brand_name]
				,p.[product_mhw_code]
				,p.[TTB_ID]
				,p.[federal_type]
				,p.[client_code]
				,r.state_name as State
				,CASE 
				  WHEN ISNULL(s.Status,'No Status') = 'No Status' AND ISNULL(s2.Status,'No Status') <> 'No Status' THEN s2.StateApprovalNumber 
				  ELSE s.StateApprovalNumber 
				 END as StateApprovalNumber
				,CASE 
				  WHEN ISNULL(s.Status,'No Status') = 'No Status' AND ISNULL(s2.Status,'No Status') <> 'No Status' THEN ISNULL(s2.Status,'No Status')
				   ELSE ISNULL(s.Status,'No Status')
				 END as Status
				,CASE 
				  WHEN ISNULL(s.Status,'No Status') = 'No Status' AND ISNULL(s2.Status,'No Status') <> 'No Status' THEN s2.EffectiveDate
				  ELSE s.EffectiveDate
				 END as EffectiveDate
				,CASE 
				  WHEN ISNULL(s.Status,'No Status') = 'No Status' AND ISNULL(s2.Status,'No Status') <> 'No Status' THEN s2.Preparer
				  ELSE s.Preparer
				 END as Preparer
				,CASE 
				  WHEN ISNULL(s.Status,'No Status') = 'No Status' AND ISNULL(s2.Status,'No Status') <> 'No Status' THEN REPLACE(REPLACE(ISNULL(convert(varchar, s2.[DateSubmitted], 120),''),'NULL',''),' 12:00:00 AM','')
				  ELSE REPLACE(REPLACE(ISNULL(convert(varchar, s.[DateSubmitted], 120),''),'NULL',''),' 12:00:00 AM','')
				  END as DateSubmitted
				,CASE 
				  WHEN ISNULL(s.Status,'No Status') = 'No Status' AND ISNULL(s2.Status,'No Status') <> 'No Status' THEN convert(varchar, s2.[ExpirationDate], 120)
				  ELSE convert(varchar, s.[ExpirationDate], 120)
				  END as ExpirationDate
				,CASE 
				  WHEN ISNULL(s.Status,'No Status') = 'No Status' AND ISNULL(s2.Status,'No Status') <> 'No Status' THEN 
					   (CASE WHEN REPLACE(s2.[DateSubmitted],'NULL','') = '' THEN '' 
							 WHEN REPLACE(s2.[DateSubmitted],'NULL','') = '1/1/1753 12:00:00 AM' THEN '' 
							 WHEN REPLACE(REPLACE(ISNULL(convert(varchar, s2.[DateSubmitted], 101),''),'NULL',''),' 12:00:00 AM','') = '1900-01-01 00:00:00' THEN ''
							 WHEN convert(smalldatetime, REPLACE(s2.[DateSubmitted],'NULL','')) = '1900-01-01 00:00:00' THEN ''
							 ELSE convert(varchar,DATEADD(day,CAST(s2.StateRegistrationApprovalEta AS int)+3, convert(smalldatetime, REPLACE(s2.[DateSubmitted],'NULL',''))), 101) END)
				  ELSE (CASE WHEN REPLACE(s.[DateSubmitted],'NULL','') = '' THEN '' 
							 WHEN REPLACE(s.[DateSubmitted],'NULL','') = '1/1/1753 12:00:00 AM' THEN '' 
							 WHEN REPLACE(REPLACE(ISNULL(convert(varchar, s.[DateSubmitted], 101),''),'NULL',''),' 12:00:00 AM','') = '1900-01-01 00:00:00' THEN ''
							 WHEN convert(smalldatetime, REPLACE(s.[DateSubmitted],'NULL','')) = '1900-01-01 00:00:00' THEN ''
							 ELSE convert(varchar,DATEADD(day,CAST(s.StateRegistrationApprovalEta AS int)+3, convert(smalldatetime, REPLACE(s.[DateSubmitted],'NULL',''))), 101) END)
				 END  as est_dt_approval
				,'' as mhw_user_notes
				,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount'
				,(select t.[Current Team Assignment ID] from tray_email_routing t with (nolock) where t.[Client ID] = p.client_code) as 'MHWTeam'
				,(select c.COMPID from v_arclnt c with (nolock) where c.[Client] = p.client_code) as 'CompID'
			FROM  [dbo].[mhw_app_prod] p
			cross join [dbo].[__ref_states] r with (nolock)
			left outer join [dbo].[__fact_state_reg_status_cola_recent] s on s.[cola] = LTRIM(RTRIM(CAST(SUBSTRING(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(p.[TTB_ID],'-',''),'a',''),'b',''),'A',''),'B',''),0,15) as varchar)))
			  and s.client=p.client_code
			  and s.State = r.state_name
			  and s.active = 1	
			left outer join [dbo].[__fact_state_reg_status_prod_recent] s2 on s2.[Prod] = p.[product_mhw_code]
			 and s2.client=p.client_code
			 and s2.State = r.state_name
			 and s2.active = 1				
            WHERE p.[product_id] IS NOT NULL
			AND p.active = 1
			AND ((s.[State] IS NOT NULL) OR (s2.[State] IS NOT NULL))
				  $client_name_qry  
			$prod_ids_qry_part  $advance_searching_qry_part
			ORDER BY p.[product_desc]";	
			//echo $tsql;exit;
	}
	else {
		echo("Wrong parameters !"); exit;
	}

}

if ($_POST['qry_type']=='blr-status-expand') {

	if (empty($_POST['view_type']) || empty($_POST['client']) || empty($_POST['key'])) {
		echo("Wrong parameters !"); exit;
	}

	if ($_POST['view_type'] === 'State Requests') {

		$tsql = "SELECT DISTINCT s.[blr_state_note],
				(STUFF((SELECT CAST(', ' + dl.Name AS VARCHAR(MAX)) 
					FROM [mhw_app_blr_state_dist] sd
					LEFT JOIN [dbo].[sc_distributor-list] dl
					ON dl.[DistKey] = sd.[blr_distributor_key]

					WHERE (s.[blr_state_id] = sd.[blr_state_id]) 
				FOR XML PATH ('')), 1, 2, '')) AS blr_distributors,
				s.[blr_state_id], s.[blr_state_abbrev],
				p.[product_id], p.[product_desc],
				p.[brand_name], p.[product_mhw_code],
				p.[TTB_ID], p.[federal_type], p.[client_code],
				b.[blr_id], b.[blr_confirmation], b.[client_name],
				b.[blr_contact], b.[blr_email], b.[blr_phone],
				b.[blr_reg_type], b.[blr_reg_sub_type],
				convert(varchar, b.[create_date], 120) as create_date,
				c.[Status] as state_status, f.[status] as federal_status,
				c.[ExpirationDate], c.[StateApprovalNumber], c.[State]
			FROM [dbo].[mhw_app_blr] b
			LEFT JOIN [dbo].[mhw_app_prod] p
				ON p.[product_id] = b.[product_id]
			LEFT JOIN [dbo].[mhw_app_blr_states] s
				ON s.[blr_id] = b.[blr_id]
			LEFT JOIN [dbo].[sc_distributor-list] d
				ON d.[DistKey] = s.[blr_distributor_key]
			LEFT JOIN [dbo].[sc_cola_state_sts] c
				ON c.[cola] = p.[TTB_ID]
				AND c.[client] = p.[client_code]
				AND c.[State] = s.[blr_state_name]
			LEFT JOIN [dbo].[sc_cola_federal_sts] f
				ON f.[cola] = p.[TTB_ID]
				AND f.[client] = b.[client_code]
			WHERE p.[product_id] IS NOT NULL
				AND s.[blr_state_abbrev] = '".addslashes($_POST['key'])."'
				AND b.[client_name] = '".addslashes($_POST['client'])."'
		";
		//echo( $tsql);exit;
	} else if ($_POST['view_type'] === 'Product Requests') {

		$tsql = "SELECT DISTINCT
				s.[blr_state_abbrev], s.[blr_state_name], s.[blr_state_note],
				(STUFF((SELECT CAST(', ' + dl.Name AS VARCHAR(MAX)) 
					FROM [mhw_app_blr_state_dist] sd
					LEFT JOIN [dbo].[sc_distributor-list] dl
					ON dl.[DistKey] = sd.[blr_distributor_key]

					WHERE (s.[blr_state_id] = sd.[blr_state_id]) 
				FOR XML PATH ('')), 1, 2, '')) AS blr_distributors,
				s.[blr_state_id],
				b.[blr_id], b.[blr_confirmation], b.[client_name],
				b.[blr_contact], b.[blr_email], b.[blr_phone],
				b.[blr_reg_type], b.[blr_reg_sub_type],
				convert(varchar, b.[create_date], 120) as create_date,
				c.[Status] as state_status, f.[status] as federal_status,
				c.[ExpirationDate], c.[StateApprovalNumber], s.[blr_state_name] as [State]
			FROM [dbo].[mhw_app_blr_states] s
			LEFT JOIN [dbo].[mhw_app_blr] b
				ON s.[blr_id] = b.[blr_id]
			INNER JOIN [dbo].[mhw_app_prod] p
				ON p.[product_id] = b.[product_id]
			LEFT JOIN [dbo].[sc_distributor-list] d
				ON d.[DistKey] = s.[blr_distributor_key]
			LEFT JOIN [dbo].[sc_cola_state_sts] c
				ON c.[cola] = p.[TTB_ID]
				AND c.[client] = b.[client_code]
				AND c.[State] = s.[blr_state_name]
			LEFT JOIN [dbo].[sc_cola_federal_sts] f
				ON f.[cola] = p.[TTB_ID]
				AND f.[client] = b.[client_code]
			WHERE b.[product_id] = '".addslashes($_POST['key'])."'
				AND b.[client_name] = '".addslashes($_POST['client'])."'
		";

	} else if ($_POST['view_type'] === 'All States') {

		$state_name = addslashes($_POST['key']);
		$client_name = addslashes($_POST['client']);

		$tsql = "SELECT DISTINCT
				p.[product_id]
				,p.[product_desc]
				,p.[brand_name]
				,p.[product_mhw_code]
				,p.[TTB_ID]
				,p.[federal_type]
				,p.[client_code]
				,s.[blr_state_abbrev]
				,(STUFF((SELECT CAST(', ' + dl.Name AS VARCHAR(MAX))
					FROM (
						SELECT DISTINCT
						  [Region]
						  ,[DistributorKey]
						FROM [sc_product_avail]
					) sd
					LEFT JOIN [dbo].[sc_distributor-list] dl
					ON dl.[DistKey] = sd.[DistributorKey]
					WHERE (s.[blr_state_abbrev] = sd.[Region])
				FOR XML PATH ('')), 1, 2, '')) AS blr_distributors
				,b.[blr_reg_type]
				,b.[blr_reg_sub_type]
				,b.[blr_confirmation]
				,convert(varchar, b.[create_date], 120) as create_date
				,c.[Status] as state_status
				,f.[status] as federal_status
				,c.[ExpirationDate]
				,c.[StateApprovalNumber]
				,c.[State]
			FROM [dbo].[mhw_app_prod] p
			LEFT OUTER JOIN [dbo].[sc_cola_state_sts] c
				ON c.[cola] = p.[TTB_ID]
				AND c.[client] = p.[client_code]
				AND c.[State] = '$state_name'
			LEFT OUTER JOIN [dbo].[sc_cola_federal_sts] f
				ON f.[cola] = p.[TTB_ID]
				AND f.[client] = p.[client_code]
			LEFT OUTER JOIN [dbo].[mhw_app_blr] b
				ON p.[product_id] = b.[product_id]
				AND b.[client_code] = p.[client_code]
			LEFT OUTER JOIN [dbo].[mhw_app_blr_states] s
				ON s.[blr_id] = b.[blr_id]
				AND s.[blr_state_name] = '$state_name'
			WHERE p.[product_id] IS NOT NULL
				AND ( c.[State] IS NOT NULL OR s.[blr_state_name] IS NOT NULL)
				AND ( 
					b.[blr_confirmation] IS NOT NULL 
					OR c.[Status]  IS NOT NULL 
					OR f.[Status]  IS NOT NULL 
				)
				AND p.[client_name] = '$client_name'
		";

	} else if ($_POST['view_type'] === 'All Products') {

		$tsql = "SELECT DISTINCT
				CASE WHEN c.[State] IS NOT NULL THEN c.[State] ELSE s.[blr_state_name] END AS [State]
				,b.[blr_reg_type]
				,b.[blr_reg_sub_type]
				,b.[blr_confirmation]
				,convert(varchar, b.[create_date], 120) as create_date
				,s.[blr_state_abbrev]
				,(STUFF((SELECT CAST(', ' + dl.Name AS VARCHAR(MAX))
					FROM (
						SELECT DISTINCT
						  [Region]
						  ,[DistributorKey]
						FROM [sc_product_avail] pa
						WHERE p.[TTB_ID] = pa.[Cola]
							AND pa.[client] = '".addslashes($_POST['client'])."'
					) sd
					LEFT JOIN [dbo].[sc_distributor-list] dl
					ON dl.[DistKey] = sd.[DistributorKey]
					WHERE (
						s.[blr_state_abbrev] = sd.[Region]
					)
				FOR XML PATH ('')), 1, 2, '')) AS blr_distributors
				,f.[status] as federal_status
				,c.[Status] as state_status
				,c.[ExpirationDate]
				,c.[StateApprovalNumber]
			FROM [dbo].[mhw_app_prod] p
			LEFT OUTER JOIN [dbo].[sc_cola_state_sts] c
				ON c.[cola] = p.[TTB_ID]
				AND c.[client] = p.[client_code]
			LEFT OUTER JOIN [dbo].[sc_cola_federal_sts] f
				ON f.[cola] = p.[TTB_ID]
				AND f.[client] = p.[client_code]
			LEFT OUTER JOIN [dbo].[mhw_app_blr] b
				ON p.[product_id] = b.[product_id]
				AND b.[client_code] = p.[client_code]
			LEFT OUTER JOIN [dbo].[mhw_app_blr_states] s
				ON s.[blr_id] = b.[blr_id]
			WHERE  ( c.[State] IS NOT NULL OR s.[blr_state_name] IS NOT NULL)
				AND ( 
					b.[blr_confirmation] IS NOT NULL 
					OR c.[Status]  IS NOT NULL 
					OR f.[Status]  IS NOT NULL 
				)
				AND p.[product_id] = '".addslashes($_POST['key'])."'
				AND p.[client_name] = '".addslashes($_POST['client'])."'
		";

	}
    else if ($_POST['view_type'] === 'State product approvals') {

		$tsql = "SELECT DISTINCT
				p.[product_id], p.[product_desc],
				p.[brand_name], p.[product_mhw_code],
				p.[TTB_ID], p.[federal_type], p.[client_code],
				f.[status] as federal_status,f.[status] as status,
				convert(varchar, f.[dateapproved], 120) as dateapproved,p.[product_class],f.[primaryfederalbasicpermitnumber],
				p.[country],f.[legalnameusedonlabel],f.[applicantname],convert(varchar, f.[datecompleted], 120) as datecompleted
				,'' as user_notes
			FROM  [dbo].[mhw_app_prod] p
			LEFT JOIN [dbo].[sc_cola_federal_sts] f
				ON f.[cola] = p.[TTB_ID]
				AND f.[client] = p.[client_code]
			WHERE p.[product_id] IS NOT NULL
				AND p.active = 1
				AND p.[client_name] = '".addslashes($_POST['client'])."'
				AND p.[product_id] = '".addslashes($_POST['key'])."'
			ORDER BY p.[product_desc]
		";
		
	}
	else if ($_POST['view_type'] === 'Federal product approvals') {
		$tsql = "SELECT DISTINCT
				p.[product_id], p.[product_desc],
				p.[brand_name], p.[product_mhw_code],
				p.[TTB_ID], p.[federal_type], p.[client_code],
				'' as StateApprovalNumber,'' as Status,'' as EffectiveDate,'' as State,'' as Preparer,'' as mhw_user_notes
			FROM  [dbo].[mhw_app_prod] p			
            WHERE p.[product_id] IS NOT NULL
			AND p.active = 1
			AND p.[client_name] = '".addslashes($_POST['client'])."'
			AND p.[product_id] = '".addslashes($_POST['key'])."'
			ORDER BY p.[product_desc]"; 	
	}
	else {
		echo("Wrong parameters !"); exit;
	}
}


/* ********************* added by Jobaidur :End ************************ */

function utf8ize($d) {
	if (is_array($d)) {
		foreach ($d as $k => $v) {
			$d[$k] = utf8ize($v);
		}
	} else if (is_string ($d)) {
		return utf8_encode($d);
	}
	return $d;
}

//echo( $tsql);exit;
$getResults= sqlsrv_query($conn, $tsql);
//echo ("Reading data from table" . PHP_EOL);
if ($getResults == FALSE)
	echo (sqlsrv_errors());

/* Processing query results */

/* Setup an empty array */
$json = array();
/* Iterate through the table rows populating the array */
do {
     while ($row = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
     $json[] = utf8ize($row);
     }
} while ( sqlsrv_next_result($getResults) );
 

//$json = utf8ize($json);

/* Run the tabular results through json_encode() */
/* And ensure numbers don't get cast to strings */
echo json_encode($json);

/* Free statement and connection resources. */
sqlsrv_free_stmt( $getResults);
sqlsrv_free_stmt( $trkResults);
sqlsrv_close( $conn);




?>