<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

//session_start();
include("sessionhandler.php");
include("prepend.php");
include("settings.php");

if(!isset($_SESSION['mhwltdphp_user'])){

	header("Location: login.php");
}
else{
	include('head.php');

	$statesArray = array();
	$blrRulesArray = array();
	$distributorsArray = array();
	$submitsArray = array();
	
	if($_POST['referringform']=='blr.php'){
	
		include('blr_processor.php');
	}

	$prodID = '';
	$blrID = '';
	$product_mhw_code = '';
	$product_mhw_code_search = '';
	$pf_client_name = '';
	$pf_client_code = '';
	$pf_brand_name = '';
	$pf_product_desc = '';

	if(isset($_POST['prodID'])){
		$prodID = $_POST['prodID'];
		$blrID = $_POST['blrID'];
		$product_mhw_code = $_POST['product_mhw_code'];
		$product_mhw_code_search = $_POST['product_mhw_code_search'];
		$pf_client_name = $_POST['tfa_client_name'];
		$pf_client_code = $_POST['tfa_client_code'];
		$pf_brand_name = $_POST['tfa_brand_name'];
		$pf_product_desc = $_POST['tfa_product_desc'];

		//echo "<pre>";
		//var_dump($_REQUEST); 
		//echo "</pre>";
	}

	/* get rules ans states */
	include("dbconnect.php");
	$conn = sqlsrv_connect($serverName, $connectionOptions);
	if( $conn === false) {
		die( print_r( sqlsrv_errors(), true));
	}
	$tsql= "SELECT * FROM [dbo].[mhw_app_blr_rules]
		ORDER BY [blr_rule_state_name] ASC
	";
	$stmt = sqlsrv_query($conn, $tsql);
	if( $stmt ) {
		while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
			$blrRulesArray[] = $row;
			$statesArray[$row['blr_rule_state_abbrev']] = $row['blr_rule_state_name'];
		}
	}

	//get submitted products/states
	$tsql= "SELECT DISTINCT s.[blr_state_abbrev], b.[product_id], 1 AS submitted
			FROM [dbo].[mhw_app_blr] b
			LEFT JOIN [dbo].[mhw_app_blr_states] s
				ON s.[blr_id] = b.[blr_id]
			WHERE b.[product_id] IS NOT NULL
			AND s.[blr_state_abbrev] IS NOT NULL
			AND b.[product_id] <> 0
	";
	$stmt = sqlsrv_query($conn, $tsql);
	if( $stmt ) {
		while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
			array_push($submitsArray, $row);
		}
	}

	//get distributors
    $tsql= "SELECT * FROM [dbo].[sc_distributor-list] WHERE [DistKey] IS NOT NULL ORDER BY [Name]";
	$stmt = sqlsrv_query($conn, $tsql);
	if( $stmt ) {
		while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
			array_push($distributorsArray, $row);
		}
	}

	sqlsrv_free_stmt($stmt);
	
	/*
	// get states list 
	$tsql= "SELECT DISTINCT [blr_rule_state_abbrev], [blr_rule_state_name] FROM [dbo].[mhw_app_blr_rules]";
	$stmt = sqlsrv_query($conn, $tsql);
	if( $stmt ) {
		while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
			$statesArray[$row['blr_rule_state_abbrev']] = $row['blr_rule_state_name'];
		}
	}
	*/
?>

	<style>
	.distributor_select {
		min-width: 200px;
	}
	</style>

	<script>

	// selected product
	var product = null;
	
	// BLR rules (requirements)
	var blrRules = <?=json_encode($blrRulesArray)?>;

	// distributors list
	var distributors = [<?=join(",\n", array_map(function($d) {
		return "{DistKey:'".$d['DistKey']."',Name:'".addslashes(utf8_encode($d['Name']))."',State:'".$d['State']."'}";
	}, $distributorsArray))?>];

	// submits list
	var submits = <?=json_encode($submitsArray)?>;
	
	// brands from ajax request
	var brands = [];

	// on adding state callback
	var showState = function(state) {

		$("#distributor_select_"+state).html('');
		//$("#distributor_select_"+state).append('<option value="">Please select...</option>');
		//$("#distributor_select_"+state).append('<option value="0">Add New...</option>');
		$("#distributor_select_"+state).append(
			distributors.filter(d=>d.State===state).map(d=>`<option value="${d.DistKey}">${d.Name}</option>`).join('\n')
		);

		const rule = blrRules.find(r=> product && r.blr_rule_state_abbrev==state && r.blr_rule_federal_type.toUpperCase()==product.federal_type.toUpperCase());
		const submitted = submits.find(s => product && s.blr_state_abbrev==state && s.product_id==product.product_id);

		var submittedWarning = "";
		if (submitted!==undefined) {
			submittedWarning = '<span style="color:red">Submitted already !</span><br/>';
		}
		if (rule!==undefined) {
			if (rule.blr_rule_key.toUpperCase().includes('DISTRIBUTOR SPECIFIC')) {
				//console.log(state, rule.blr_rule_key);
				$("#distributor_select_"+state).attr("required", true);
				$("#distributor_select_"+state).attr("aria-required", "true");
	 			//$("#distributor_select_"+state).addClass("required");
			}
			$("#requirements_"+state).html(
				"<small>" + submittedWarning +
				(rule.blr_rule_franchise ? "Franchise: " + rule.blr_rule_franchise : "") +
				(rule.blr_rule_tax ? "<br/>Tax: " + rule.blr_rule_tax : "") +
				(rule.blr_rule_additional_licensing ? "<br/>Additional Licensing: " + rule.blr_rule_additional_licensing : "") +
				(rule.blr_rule_approval_time ? "<br/>Approval Time: " + rule.blr_rule_approval_time : "") +
				(rule.blr_rule_renewal ? "<br/>Renewal: " + rule.blr_rule_renewal : "") +
				(rule.blr_rule_renew_dates ? "<br/>Renew Dates: " + rule.blr_rule_renew_dates : "") +
				(rule.blr_rule_key ? "<br/>KEY: " + rule.blr_rule_key : "") +
				"</small>"
			);
		}else{
			$("#requirements_"+state).text(submittedWarning);
		}
	};

	$(document).ready(function(){
		/*======BEGIN INITIAL PAGE LOAD======*/

		///////////BLR-specific (additional scripts at the bottom)///////////////////////////////////////////////
		$('#registration_info_NewCOLA1').hide();
		$('#registration_info_NewCOLA2').hide();
		$('#registration_info_NewCOLA3').hide();
		$('.registration_info').hide();
		$('#continue1').hide();
		$('#submit_button').hide();

		$('#blr_registration_type').on("change", function () {
			//alert($("#blr_registration_type").prop('selectedIndex'));
			var thisindex = $(this).prop('selectedIndex');
			var subVal = false;
			$('.registration_info').hide();

			$('#registration_info_NewCOLA'+thisindex).show();
			
			//if(thisindex > 0){
			//	$('#continue1').show();
			//}

			if(thisindex==0){
				$('#StateRow1').hide();
				$('#StateRow2').hide();
				$('#continue1').hide();
				$('#submit_button').hide();
			}
			else if(thisindex==1){
				$('.NewCOLA1_sub').each(function() {
					if ( $(this).checked || $(this).prop( "checked" ) || $(this).is( ":checked" ) ){
						subVal = true;
					}
				});

				if(subVal){ 
					$('#continue1').show(); 
					$('#StateRow1').hide();
					$('#StateRow2').hide();
					$('#submit_button').hide();
				}
				else{
					$('#continue1').hide();
					$('#StateRow1').hide();
					$('#StateRow2').hide();
					$('#submit_button').hide();
				}
			}
			else if(thisindex==2){
				$('.NewCOLA2_sub').each(function() {
					if ( $(this).checked || $(this).prop( "checked" ) || $(this).is( ":checked" ) ){
						subVal = true;
					}
				});

				if(subVal){ 
					$('#continue1').show();
					$('#StateRow1').hide();
					$('#StateRow2').hide();
					$('#submit_button').hide();
				}
				else{
					$('#continue1').hide();
					$('#StateRow1').hide();
					$('#StateRow2').hide();
					$('#submit_button').hide();
				}
			}
			else if(thisindex==3){
				$('#StateRow1').hide();
				$('#StateRow2').hide();
				$('#continue1').hide();
				$('#submit_button').show();
			}
		});

		$('.NewCOLA1_sub').on("change", function () {
			//var subVal = $(this).val();
			//if(subVal.length > 0){ 
			if (  $(this).checked ||  $(this).prop( "checked" ) ||  $(this).is( ":checked" ) ){
				$('#continue1').show();
				$('#StateRow1').hide();
				$('#StateRow2').hide();
				$('#submit_button').hide();
			}
			else{
				$('#continue1').hide();
				$('#StateRow1').hide();
				$('#StateRow2').hide();
				$('#submit_button').hide();
			}
		});

		$('.NewCOLA2_sub').on("change", function () {
			//var subVal = $(this).val();
			//if(subVal.length > 0){ 
			if (  $(this).checked ||  $(this).prop( "checked" ) ||  $(this).is( ":checked" ) ){
				$('#continue1').show();
				$('#StateRow1').hide();
				$('#StateRow2').hide();
				$('#submit_button').hide();
			}
			else{
				$('#continue1').hide();
				$('#StateRow1').hide();
				$('#StateRow2').hide();
				$('#submit_button').hide();
			}
		});

		$('#continue1').on('click', function () {
			var thisindex = $('#blr_registration_type').prop('selectedIndex');

			if(thisindex==1){
				//var subVal = $('#NewCOLA1_sub').val();
				//if(subVal.length > 0){ 
				$('.NewCOLA1_sub').each(function() {
					if ( $(this).checked || $(this).prop( "checked" ) || $(this).is( ":checked" ) ){
						$('#StateRow1').show();
						$('#StateRow2').show(); 
						$('#submit_button').show();
					}
				});
			}
			else if(thisindex==2){
				//var subVal = $('#NewCOLA2_sub').val();
				//if(subVal.length > 0){ 
				$('.NewCOLA2_sub').each(function() {
					if ( $(this).checked || $(this).prop( "checked" ) || $(this).is( ":checked" ) ){
						$('#StateRow1').show();
						$('#StateRow2').show();
						$('#submit_button').show();
					}
				});
			}
		});

		/*
		$('.distributor_select').on("change", function () {
			var thisindex = $(this).prop('selectedIndex');
			var thisstate = $(this).data('state');
			if(thisindex == 1){
				var $tr =  $(this).parents('tr');

				if($('#distributor_row_'+thisstate).length == 0){
					$tr.after("<tr id='distributor_row_"+thisstate+"'><td>&nbsp;</td>\
					<td colspan='3'><table><tr><th>Distributor Name</th><th>Contact</th><th>Email</th><th>Phone</th><th>Address</th></tr>\
					<tr><td><input type='text' id='distributor_name_"+thisstate+"' aria-required=\"true\" class=\"required\" name='distributor_name_"+thisstate+"'> </td>\
					<td><input type='text' id='distributor_contact_"+thisstate+"' aria-required=\"true\" class=\"required\" name='distributor_contact_"+thisstate+"'> </td>\
					<td><input type='text' id='distributor_phone_"+thisstate+"' aria-required=\"true\" class=\"required\" name='distributor_phone_"+thisstate+"'> </td>\
					<td><input type='text' id='distributor_email_"+thisstate+"' aria-required=\"true\" class=\"required\" name='distributor_email_"+thisstate+"'> </td>\
					<td><textarea id='distributor_address_"+thisstate+"' aria-required=\"true\" class=\"required\" name='distributor_address_"+thisstate+"' cols=20 rows=5></textarea></td>\
					</tr></table></td></tr>");
				}
				else{
					$('#distributor_row_'+thisstate).show();
				}
			}
			else{
				$('#distributor_row_'+thisstate).remove();
			}
		});
		*/
		
		$('.STrules').hide();

		//TBD - SHOULD BE DYNAMIC
		/*
		$('#STrule_AK').show();
		$('#STrule_CO').show();
		$('#STrule_IN').show();
		$('#STrule_NV').show();
		$('#STrule_OR').show();
		*/
		/*
		$('#StateRow1').hide();
		$('#StateRow2').hide();
		*/
		
		$('#tfa_email').on('input', function() {
			var input=$(this);
			var re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
			var is_email=re.test(input.val());
			if(is_email){input.removeClass("invalid").addClass("valid");}
			else{input.removeClass("valid").addClass("invalid");}
		});

		$("#submit button").click(function(event){
			var form_data=$("#blrform").serializeArray();
			var error_free=true;
			for (var input in form_data){
				var element=$("#"+form_data[input]['name']);
				var valid=element.hasClass("valid");
				var error_element=$("span", element.parent());
				if (!valid){error_element.removeClass("error").addClass("error_show"); error_free=false;}
				else{error_element.removeClass("error_show").addClass("error");}
			}
			if (!error_free){
				event.preventDefault(); 
			}
			else{
				alert('No errors: Form will be submitted');
			}
		});
		
		////////BLR-specific (additional scripts at the bottom)///////////////////////////////////////

		//if product id is passed to page, preload with data
		var prodIDinit = $("#prodID").val();
		if(parseInt(prodIDinit) > 0){
			prefill_prod(prodIDinit);

			cbb_formatter();
		}
		//function to toggle button in Image section
		function TTBlink(ttbVal){
			var ttbUrl = '<?php echo $ttblink; ?>';  //defined in settings.php
			//var ttbVal = $("#tfa_TTB").val();
			var ttbInt = parseInt(ttbVal);  //TTB value must be integer to be valid, but may begin with leading zeros so use text value for url
			if(ttbVal!="" && ttbVal!="P" && ttbVal!="p" && ttbInt){
				$(".ttbLinkDiv").html("<label id='tfa_ttbLink[0]-L' class='label preField ' for='tfa_ttbLink[0]'>View COLA Details:</label> <div class='inputWrapper'> <a href='"+ttbUrl+ttbVal+"' class='btn btn-outline-primary btn-sm ttbLink' target='_blank'><i class='fas fa-file-contract'></i> TTB Site <i class='fas fa-external-link-square-alt'></i><a/></div>");
			}
			else{
				$(".ttbLinkDiv").html(""); //clear button for invalid or 'P' (pending) TTB ID
			}
		}
		/*======END INITIAL PAGE LOAD======*/

		/*======BEGIN comboboxes==========*/
		function cbb_formatter(){ 
			//$('#tfa_brand_name').addClass('combobox_restyle');
			//$('#tfa_brand_name').css( "border-radius", "0px" );
			$('ul.ac_results').css("margin-left","0em");
			$('ul.ac_results').css("margin","0em");
			$('.ac_button').css("height","28px");
			$('.ac_button').css("background","transparent");
			$('.ac_button').css("margin-left","-35px");
			$('.ac_button').css("border","0px");
			$('.ac_subinfo dl').css("margin-left","0em");
			$('.ac_subinfo dl').css("margin","0em");
		}
		//load form fields - as a result of selecting product_desc combobox
		function prefill_prod(prodID){
			var qrytype = "prodByID";
			var client = $('#tfa_client_name').val(); //get value of the client combobox
			$.post('query-request.php', {qrytype:qrytype,prodID:prodID,client:client}, function(dataPx){ // ajax request query-request.php, send brand as post variable, return dataX variable, parse into JSON

				var t = [];
				try {
					t = JSON.parse(dataPx);
				} catch (error) {
					//location.reload();
					console.log(error, dataPx);
				}

				$.each( t, function( key, value ) {
					//product
					product = value;
					$('#product_mhw_code').val(value.product_mhw_code);
					$('#product_mhw_code_search').val(value.product_mhw_code_search);	
					$('#federal_type').html(value.federal_type);	
					$("#blr_states").val().forEach(showState);			

				});
			});
		}
		function isProdValidSelection(){
			var isProdID = $('#tfa_product_desc')[0].hasAttribute("sub_info"); //free-type data will not have sub_info attribute
			if(isProdID){
				var json = $('#tfa_product_desc').attr('sub_info');
				//if additional sub_info elements are added, additional apostrophe replacements need to be added
				//var json2 = json.replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"');
				var json2 = json.replace( /\'/g, '"' );
				var jsonObj = JSON.parse(json2);

				$("#prodID").val(jsonObj.id);  //set field for processing update
				$("#tfa_399-L").html("Product: "+jsonObj.code); //display message for update
				prefill_prod(jsonObj.id); //prefill form fields
                $(".item-not-found").remove();
			}
			else{
				$("#tfa_399-L").html("Product: new entry"); //display message for insert
				$("#prodID").val("");  //clear field for insert
                $(".item-not-found").remove();
                if ($("#tfa_product_desc").val().length > 0) {
                    $("#tfa_product_desc").parent().parent().parent().prepend($("<div class='item-not-found'>The product is not found, <a href='/forms/productsetup.php'>add a new one here</a></div>" ));
                }
			}
		}

		//initialize brand combobox
		function initialize_brand(){
			$('#tfa_brand_name').parent().find(".ac_button").remove();  //clear elements of previous instance
			$('#tfa_brand_name').parent().find(".ac_result_area").remove();  //clear elements of previous instance
			var client = $('#tfa_client_name').val(); //get value of the client combobox
			var cbbtype = 'brand';
			$.post('select-request.php', {cbbtype:cbbtype,client:client}, function(dataX){ // ajax request select-request.php, send the client POST variable, return dataX variable, parse as JSON
				var data = [];
				try {
					data = JSON.parse(dataX);
				} catch (error) {
					//location.reload();
					console.log(error, dataX);
				}
				brands = data;
				$(function() {
					$('#tfa_brand_name').ajaxComboBox(
						data
					);
					cbb_formatter(); //apply css and classname changes to combobox elements
				});
			});
			$('#tfa_brand_name').bind("change", function() {

				var newBrand = $('#tfa_brand_name').val();
				$(".brand-not-found").remove();
				if (newBrand.length > 0 && undefined===brands.find(b=>b.id===newBrand)) {
					$("#tfa_brand_name").parent().parent().parent().prepend($("<div class='brand-not-found'>The brand is not found, <a href='/forms/productsetup.php'>add a new one here</a></div>" ));
				}

				$('#tfa_product_desc').parent().parent().html("<input type=\"text\" id=\"tfa_product_desc\" name=\"tfa_product_desc\" value=\"\" aria-required=\"true\" title=\"Product Name\" data-dataset-allow-free-responses=\"\" class=\"required\">");

				initialize_product();

				isProdValidSelection();

				cbb_formatter(); //apply css and classname changes to combobox elements
			});
		}
		initialize_brand();
		
		//initialize product combobox
		function initialize_product(){
			$('#tfa_product_desc').parent().find(".ac_button").remove();  //clear elements of previous instance
			$('#tfa_product_desc').parent().find(".ac_result_area").remove();  //clear elements of previous instance
			var client = $('#tfa_client_name').val(); //get value of the client combobox
			var brand = $('#tfa_brand_name').val(); //get value of the brand combobox
			var cbbtype = 'product';
			$.post('select-request.php', {cbbtype:cbbtype,client:client,brand:brand}, function(dataX){ // ajax request select-request.php, send brand as post variable, return dataX variable, parse into JSON
				var data = [];

				try {
					data = JSON.parse(dataX);
				} catch (error) {
					//location.reload();
					console.log(error, dataX);
				}

				$(function() {
					$('#tfa_product_desc').ajaxComboBox(
						data,
						{
						  bind_to: 'tfa_product_desc',
						  sub_info: true,
						  sub_as: {
							id: 'Product ID',
							code: 'MHW Product Code',
							ttb: 'TTB ID'
						  },
						}
					).bind('tfa_product_desc', function() {
					  var json = $(this).attr('sub_info');
					  //if additional sub_info elements are added, additional apostrophe replacements need to be added
						//var json2 = json.replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"');
						var json2 = json.replace( /\'/g, '"' );
					  var jsonObj = JSON.parse(json2);

					  if(parseInt(jsonObj.id) > 0){
						$("#prodID").val(jsonObj.id); //set field for processing update
						$("#tfa_399-L").html("Product: "+jsonObj.code); //display message for update
						prefill_prod(jsonObj.id); //prefill form fields
					  }
					  if(parseInt(jsonObj.ttb) > 0){
						TTBlink(jsonObj.ttb); //ttb link
					  }
					  isProdValidSelection();

					});
					cbb_formatter(); //apply css and classname changes to combobox elements
				});
			});
			$('#tfa_product_desc').bind("change", function(){ 
				isProdValidSelection();
			});
		}
		initialize_product();

		//client combobox >> brand combobox
		$('#tfa_client_name').change(function(){ //if client value changes
			//replace combobox elements with original input
			$('#tfa_brand_name').parent().parent().html("<input type=\"text\" id=\"tfa_brand_name\" name=\"tfa_brand_name\" value=\"\" aria-required=\"true\" title=\"Brand Name\" data-dataset-allow-free-responses=\"\" class=\"required\">");
			//replace combobox elements with original input
			$('#tfa_product_desc').parent().parent().html("<input type=\"text\" id=\"tfa_product_desc\" name=\"tfa_product_desc\" value=\"\" aria-required=\"true\" title=\"Product Name\" data-dataset-allow-free-responses=\"\" class=\"required\">");

			initialize_brand();
			
			//re-initialize product combobox
			initialize_product();

			isProdValidSelection();

			cbb_formatter(); //apply css and classname changes to combobox elements
		});
		/*======END comboboxes==========*/
		//clear form
		$("#clearform").click(function() {		

			window.location.reload();
			/*
			$('#prodID').val('');
			$('#product_mhw_code').val('');
			$('#product_mhw_code_search').val('');
			$('#federal_type').html('');

			$("#tfa_399-L").html("Product: new entry"); //display message for insert

			//replace combobox elements with original input
			$('#tfa_brand_name').parent().parent().html("<input type=\"text\" id=\"tfa_brand_name\" name=\"tfa_brand_name\" value=\"\" aria-required=\"true\" title=\"Brand Name\" data-dataset-allow-free-responses=\"\" class=\"required\">");
			//replace combobox elements with original input
			$('#tfa_product_desc').parent().parent().html("<input type=\"text\" id=\"tfa_product_desc\" name=\"tfa_product_desc\" value=\"\" aria-required=\"true\" title=\"Product Name\" data-dataset-allow-free-responses=\"\" class=\"required\">");

			initialize_brand();

			initialize_product();

			cbb_formatter();
			*/
		});
	});
    </script>

    <link href="css/wforms-layout_v530-14.css" rel="stylesheet" type="text/css" />
    <link href="css/theme-52661.css" rel="stylesheet" type="text/css" />
    <link href="css/wforms-jsonly_v530-14.css" rel="alternate stylesheet" title="This stylesheet activated by javascript" type="text/css" />
	<link href="css/wforms-inline.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/wforms_v530-14.js"></script>
    <script type="text/javascript" src="js/localization-en_US_v530-14.js"></script>         

</head>
<body class="default wFormWebPage">
<form method="post" action="blr.php" id="blrform" class="hintsBelow labelsAbove" enctype="multipart/form-data">
<div class="container-fluid">
	<div id="tfaContent">
		<div class="wFormContainer" style="max-width: 85%; width:auto;" >

			<div class="row">

				<div class="col-lg-12">

					<div class="row">

						<div class="col-lg-6">
							<div class="wFormHeader"></div>
							<h3 class="wFormTitle" id="4690986-T">Brand Label Registration</h3>
						</div> <!--END col-->

						<div class="col-lg-6 align-middle">
							<b class="text-danger">This form is not for distributor or authorized sales territory changes.</b>
						</div> <!--END col-->

					</div> <!--END row-->

					<div class="row">

						<div class="col-lg-12">
							
							 <!--<div class="card">
							  <div class="card-body">-->

								<div class="row">

									<div class="col-lg-2">
										<div class="oneField field-container-D    " id="tfa_client_name-D">
											<label id="tfa_client_name-L" class="label preField " for="tfa_client_name"><b>Client Name</b></label><br>
											<div class="inputWrapper">
												<select id="tfa_client_name" name="tfa_client_name" title="Client Name" aria-required="true" class="required">
												<?php
												$clients = explode(";",$_SESSION['mhwltdphp_userclients']);
												
												foreach ($clients as &$clientvalue) {
													if(isset($pf_client_name) && $pf_client_name==$clientvalue){
														echo "<option value=\"".$clientvalue."\" class=\"\" selected>".$clientvalue."</option>";
													}
													else{
															echo "<option value=\"".$clientvalue."\" class=\"\">".$clientvalue."</option>";
													}
												}
												?>					
												</select>
											</div>
											
										</div>
									</div> <!--END col-->

									<div class="col-lg-3">
										<div class="oneField field-container-D    " id="tfa_contact-D">
											<label id="tfa_contact-L" class="label preField reqMark" for="tfa_contact"><b>Requestor/Contact</b></label><br>
											<div class="inputWrapper">
												<input type="text" id="tfa_contact" name="tfa_contact" class="required" value="<?php echo $_SESSION['mhwltdphp_userlastname']; ?>" />
											</div>
										</div>
									</div> <!--END col-->

									<div class="col-lg-3">
										<div class="oneField field-container-D    " id="tfa_email-D">
											<label id="tfa_email-L" class="label preField reqMark" for="tfa_email"><b>Email</b></label><br>
											<div class="inputWrapper">
												<input type="text" id="tfa_email" name="tfa_email" class="required" value="<?php echo $_SESSION['mhwltdphp_useremail']; ?>" />
											</div>
										</div>
									</div> <!--END col-->

									<div class="col-lg-3">
										<div class="oneField field-container-D    " id="tfa_phone-D">
											<label id="tfa_phone-L" class="label preField reqMark" for="tfa_phone"><b>Phone</b></label><br>
											<div class="inputWrapper">
												<input type='text' id="tfa_phone" name="tfa_phone" class="required" />
											</div>
										</div>
									</div> <!--END col-->

									<div class="col-lg-1">
										<div class="float-right"><button type="reset" value="Reset" id="clearform" class="btn btn-sm btn-outline-danger">Clear Form</button></div>
									</div> <!--END col-->

								</div> <!--END row-->
							<!--</div>
						</div>-->
						</div> <!--END col-->

					</div> <!--END row-->

					<br />

					<div class="row">

						<div class="col-lg-12">		
							<fieldset id="tfa_399" class="section" >
							<?php
								if($prodID > 0 && $product_mhw_code!=''){ 
									echo "<legend id=\"tfa_399-L\">Product: editing ".$product_mhw_code."</legend>"; }
								else{  
									echo "<legend id=\"tfa_399-L\">Product: new entry</legend>"; 
								}
							?>
							
							<div class="row">
								<div class="col-lg-4">
									<!--BEGIN BRAND/PRODUCT-->	
									<div id="tfa_856" class="section inline group">
										<div class="oneField field-container-D    " id="tfa_brand_name-D">
											<label id="tfa_brand_name-L" class="label preField reqMark" for="tfa_brand_name">Brand Name</label><br>
											<div class="inputWrapper"><input type="text" id="tfa_brand_name" name="tfa_brand_name" value="<?php echo $pf_brand_name; ?>" aria-required="true" title="Brand Name" data-dataset-allow-free-responses="" class="required"></div>
										</div>
										<div class="oneField field-container-D    " id="tfa_product_desc-D">
											<label id="tfa_product_desc-L" class="label preField reqMark" for="tfa_product_desc">Product Name</label><br>
											<div class="inputWrapper"><input type="text" id="tfa_product_desc" name="tfa_product_desc" value="<?php echo $pf_product_desc; ?>" aria-required="true" title="Product Name" data-dataset-allow-free-responses="" class="required"></div>
										</div>
										
									</div>
									<!---END PRODUCT-->	
									<!--BEGIN BLR TYPE-->
									<div class="oneField field-container-D    " id="blr_registration_type-D">
										<label id="blr_registration_type-L" class="label preField reqMark" for="blr_registration_type">Registration Type</label><br>
										<div class="inputWrapper">
											<select id="blr_registration_type" name="blr_registration_type" title="Registration Type" aria-required="true" class="required">
											<option value="">Please select...</option>
											
											<?php
											$registration_type_opts = array('New COLA - Product Never Registered','New COLA - Existing Product Already Registered','New COLA - No Action');
											$registration_type_opts_info = array('registration_info_NewCOLA1','registration_info_NewCOLA2','registration_info_NewCOLA3');
											foreach ($registration_type_opts as &$registration_type_optsVALUE) {
												if(isset($pf_registration_type) && $pf_registration_type==$registration_type_optsVALUE){
													echo "<option value=\"".$registration_type_optsVALUE."\" class=\"\" selected>".$registration_type_optsVALUE."</option>";
														
												}
												else{
													echo "<option value=\"".$registration_type_optsVALUE."\" class=\"\">".$registration_type_optsVALUE."</option>";
												}
											}
											?>							
											</select>
										</div>
									</div>
								</div> <!--END col-->
								<div class="col-lg-1">
									<div class="ttbLinkDiv" id="tfa_ttbLink-D"></div>
								</div> <!--END col-->
								<div class="col-lg-5" style="min-width:20em">
									<div id="registration_info_NewCOLA1" class="registration_info" style="display:none">
										<b>New COLA, Product Never Registered (ability to register in any state as new)</b><br />
										<i>Select this option if this new COLA meets one of the following criteria (choose one secondary request type):</i><br />
										&nbsp;<input type="radio" name="NewCOLA1_sub" id="NewCOLA1_sub1" class="NewCOLA1_sub" value="1">1. This is the 1st COLA of a new Product that has never been registered <br />
										&nbsp;<input type="radio" name="NewCOLA1_sub" id="NewCOLA1_sub2" class="NewCOLA1_sub" value="2">2. This is an additional COLA of a new product that has never been registered <br />
									</div>
									<div id="registration_info_NewCOLA2" class="registration_info" style="display:none">
										<b>New COLA -Existing product that is already registered (update COLA in states that require COLA revision and price posting)</b><br />
										<i>Select this option if this new COLA meets one of the following criteria (choose one secondary request type):</i><br />
										&nbsp;<input type="radio" name="NewCOLA2_sub" id="NewCOLA2_sub1" class="NewCOLA2_sub" value="1">1. This is an updated/additional COLA of an existing product that has already been registered; I only want to update the COLA and Price Posting in applicable states.<br />
										&nbsp;<input type="radio" name="NewCOLA2_sub" id="NewCOLA2_sub2" class="NewCOLA2_sub" value="2">2. This is an updated/additional COLA of an existing product that has already been registered; I would like to register in new states.<br />
									</div>
									<div id="registration_info_NewCOLA3" class="registration_info" style="display:none">
										<b>New COLA - I do not want to register this COLA at this time.  I understand that I cannot sell my product until Brand Label Registration AND Price posting (in applicable states) have been approved</b><br />
									</div>
								</div> <!--END col-->
								<div class="col-lg-1">
									<button class="btn btn-primary" id="continue1" type="button" style="display:none">Continue</button>
								</div> <!--END col-->
								<!--END BLR TYPE-->
							</div> <!--END row-->
							</fieldset>
						</div> <!--END col-->

					</div> <!--END row-->

					<!-- BEGIN STATES-->
					<br />

					<div class="row" id="StateRow1">

						 <div class="col-lg-1">&nbsp;</div> <!--END col-->
						 <div class="col-lg-2">
							<!--<fieldset id="tfa_1827" class="section">-->
							<label id="blr_states-L" class="label preField " for="blr_states"><b>Select States</b>&nbsp;&nbsp;<span id="state_count" class="badge badge-primary pull-right">0</span></label>
							<!--<span class="field-hint-inactive" id="blr_states-H"><span id="blr_states-HH" class="hint"> (Hold down Ctrl to select multiple states)</span></span></label><br>-->
							<div class="inputWrapper">
								<select id="blr_states" multiple name="blr_states[]" title="Applicable States" aria-describedby="blr_states-HH" class="" height="350" style="height:350px;">
									<?php foreach ($statesArray as $stateAbbrev => $stateName) { ?>
										<option value="<?=$stateAbbrev?>" id="blr_states_<?=$stateAbbrev?>" class=""><?=$stateName?></option>
									<?php } ?>
								</select>
							</div>
							<!--</fieldset>-->
						</div> <!--END col-->

						<div class="col-lg-9" style="min-width: 550px;">
							<div id="map2" style="width: 550px; height: 450px; margin:auto"></div>
							<div id="alert"></div>
						</div> <!--END col-->

					</div> <!--END row-->
	 
					<div class="row" id="StateRow2" style="display: none;">

					  <div class="col-lg-1">&nbsp;</div> <!--END col-->
					  <div class="col-lg-10">
						<p><b>States-specific Requirements/Notes</b> for Federal Type: <b><span id="federal_type"></span></b></p><br />
						<table class="table table-hover" data-toggle="table" data-pagination="false">
						<!--<table id="prodTable" class="table table-hover" data-toggle="table" data-pagination="false" data-show-pagination-switch="true"  data-show-columns="true" data-show-toggle="true" data-search="true" data-show-export="true">-->
						<thead>
							<tr><th>State</th><th>Requirements</th><th>Notes (example: sales territory)</th><th>Distributor</th></tr>
						</thead>
						<tbody>
							<?php foreach ($statesArray as $stateAbbrev => $stateName) { ?>
							<tr id="STrule_<?=$stateAbbrev?>" class="STrules">
								<td><?=$stateName?> <input type="hidden" id="state_name_<?=$stateAbbrev?>" name="state_name_<?=$stateAbbrev?>" value="<?=$stateName?>"></td>
								<td class="requirements" id="requirements_<?=$stateAbbrev?>"></td>
								<td><textarea id="state_note_<?=$stateAbbrev?>" name="state_note_<?=$stateAbbrev?>" class="state_note" cols=25 rows=2></textarea></td>
								<td>
									<select id="distributor_select_<?=$stateAbbrev?>" name="distributor_select_<?=$stateAbbrev?>[]" multiple="multiple" class="distributor_select" data-state="<?=$stateAbbrev?>">
										<!--
										<option value="">Please select...</option>
										<option value="0">Add New...</option>
										-->
									</select>
									<button type="button" class="btn btn-primary btn-sm align-bottom bnt-add-distributor" data-state="<?=$stateAbbrev?>" data-toggle="modal" data-target-2="#add-distributor-modal">
										Add New
									</button>
								</td>
							</tr>
							<?php } ?>
						</tbody>
						</table>
					  </div> <!--END col-->
					  
					</div> <!--END row-->
					<!-- END STATES-->

					<br />

					<div class="row">

						<div class="col-lg-8">&nbsp;</div> <!--END col-->
						<div class="col-lg-1">
							<div class="actions" style="float:right;"><button type="submit" data-label="Submit" id="submit_button" class="btn btn-primary">Submit</button></div>
						</div> <!--END col-->

					</div> <!--END row-->

					<div style="clear:both"></div>
					<input type="hidden" name="referringform" id="referringform" value="blr.php">
					<input type="hidden" name="blrID" id="blrID" value="<?php echo $blrID; ?>">
					<input type="hidden" name="blr_state_count" id="blr_state_count" value="<?php echo $blr_state_count; ?>">
					<input type="hidden" name="blr_state_count_SF" id="blr_state_count_SF" value="<?php echo $blr_state_count_SF; ?>">
					<input type="hidden" name="prodID" id="prodID" value="<?php echo $prodID; ?>">
					<input type="hidden" name="tfa_client_code" id="tfa_client_code" value="<?php echo $pf_client_code; ?>">
					<input type="hidden" name="product_mhw_code" id="product_mhw_code" value="<?php echo $product_mhw_code; ?>">
					<input type="hidden" name="product_mhw_code_search" id="product_mhw_code_search" value="<?php echo $product_mhw_code_search; ?>">
					<input type="hidden" name="userID" id="userID" value="<?php echo $_SESSION['mhwltdphp_user']; ?>">

				</div> <!--END col-->

			</div> <!--END row-->

		</div>    <!--END wFormContainer-->
	</div> <!--END tfaContent-->
</div> <!--END container-fluid-->

</form>


<!-- Modal -->
<div class="modal fade" id="add-distributor-modal" tabindex="-1" role="dialog" aria-labelledby="add-distributor-modal-label" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="add-distributor-modal-label">Add New Distributor</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
		<form id="add-distributor-form">
			<div class="form-row">
				<div class="form-group col-md-12">
					<label>Distributor Name <span>*</span></label>
					<input type="text" class="form-control" name="distributor_name" maxlength="150" required>
				</div>
				<div class="form-group col-md-12">
					<label>Distributor Address <span>*</span></label>
					<input type="phone" class="form-control" name="distributor_address" maxlength="150" required>
				</div>				
				<div class="form-group col-md-12">
					<label>Distributor Contact Person <span>*</span></label>
					<input type="text" class="form-control" name="distributor_contact" maxlength="150" required>
				</div>
				<div class="form-group col-md-12">
					<label>Distributor Phone <span>*</span></label>
					<input type="phone" class="form-control" name="distributor_phone" maxlength="25" required>
				</div>				
				<div class="form-group col-md-12">
					<label>Distributor Email <span>*</span></label>
					<input type="email" class="form-control" name="distributor_email" maxlength="150" required>
				</div>									
			</div>	
			<input type="hidden" id="distributor_state" name="distributor_state">
		</form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-primary" id="btn-do-add-distributor">Add Distributor</button>
		</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="add-distributor-message-modal" tabindex="-1" role="dialog" aria-labelledby="add-distributor-message-modal-label" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="add-distributor-message-modal-label">Add New Distributor</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<h5>Contact customer service to set-up up a new wholesale customer</h5>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		</div>
		</div>
	</div>
</div>

<script src='js/iframe_resize_helper_internal.js'></script>
<script src="lib/raphael.js"></script>
<!-- <script src="scale.raphael.js"></script> -->
<!--<script src="js/jquery-3.3.1.min.js"></script>-->
<script src="example/color.jquery.js"></script>
<script src="jquery.usmap.js"></script>

<script>
$(document).ready(function() {

  $('.distributor_select').select2();

  $('.bnt-add-distributor').click(e => {
	$('#distributor_state').val($(e.target).data('state'));
	<?php if ($allowToAddNewDistributor) { ?>
	$('#add-distributor-modal').modal();
	<?php } else { ?>
	$('#add-distributor-message-modal').modal();
	<?php } ?>
  })

  $('#btn-do-add-distributor').click(e => {
	$("#add-distributor-modal").modal('hide');
    $.post('blr-add-distributor.php', $("#add-distributor-form").serialize(), function(jsonObj) { 
	  try {
		$("#distributor_select_"+jsonObj.state).append(
			`<option value=${jsonObj.key}>${jsonObj.name}</option>`
		);
		$("#distributor_select_"+jsonObj.state).val([jsonObj.key]);
		$("#distributor_select_"+jsonObj.state).trigger('change');

	  }catch (e) {
		console.log(e);
	  }
    });
  })

  $('#map2').usmap({
	showLabels: true,
	'stateStyles': {
	  fill: '#025', 
	  "stroke-width": 1,
	  'stroke' : '#036'
	},
	'stateHoverStyles': {
	  fill: 'teal'
	},
	//TBD - SHOULD BE DYNAMIC
	/*
	'stateSpecificStyles': {
	  'AK' : {fill: 'teal'},
	  'CO' : {fill: 'teal'},
	  'IN' : {fill: 'teal'},
	  'NV' : {fill: 'teal'},
	  'OR' : {fill: 'teal'}
	},
	*/
	'click' : function(event, data) {

	  var stateCount = $("#state_count").text();
	  var stateCountInt = parseInt(stateCount);

	  if($("#STrule_"+data.name).is(":visible")){ 
		  $('#STrule_'+data.name).hide(); 
		  $('#blr_states option[value=' + data.name + ']').removeAttr('selected');
		  $('#blr_states option[value=' + data.name + ']').toggleClass('selected');
		  $('#blr_states option[value=' + data.name + ']').prop('selected', false);
		  $('#' + data.name).css('fill', '#025');
		  stateCountInt = stateCountInt-1;
		  $("#state_count").text(stateCountInt);
		  $("#blr_state_count").val(stateCountInt);
		  if ($('#distributor_row_'+data.name).length>0) {
			$('#distributor_row_'+data.name).remove();
		  }
	  }
	  else{ 
		  $('#STrule_'+data.name).show(); 
		  $('#blr_states option[value=' + data.name + ']').attr('selected', true);
		  $('#blr_states option[value=' + data.name + ']').toggleClass('selected');
		  $('#blr_states option[value=' + data.name + ']').prop('selected', true);
		  $('#' + data.name).css('fill', 'teal');
		  showState(data.name);
		  stateCountInt = stateCountInt+1;
		  $("#state_count").text(stateCountInt);
		  $("#blr_state_count").val(stateCountInt);
	  }
	}
  });

  //multiple selection without requiring ctrl key
  $('#blr_states option').on("mousedown", function (e) {
	 e.preventDefault();
		jQuery(this).toggleClass('selected');

		var stateVal = $(this).val(); 
		var stateCount = $("#state_count").text();
		var stateCountInt = parseInt(stateCount);

		if($("#STrule_"+stateVal).is(":visible")){ 
			$('#STrule_'+stateVal).hide(); 
			stateCountInt = stateCountInt-1;
			$("#state_count").text(stateCountInt);
			$("#blr_state_count").val(stateCountInt);
			$('#' + stateVal).css('fill', '#025');
			if ($('#distributor_row_'+stateVal).length>0) {
				$('#distributor_row_'+stateVal).remove();
		  	}
		}
		else{ 
			$('#STrule_'+stateVal).show(); 
			 stateCountInt = stateCountInt+1;
			 showState(stateVal);
			 $("#state_count").text(stateCountInt);
			 $("#blr_state_count").val(stateCountInt);
			 $('#' + stateVal).css('fill', 'teal');
		}
	  
		jQuery(this).prop('selected', !jQuery(this).prop('selected'));
		return false;
  });
  
  $('#StateRow1').hide();
  $('#StateRow2').hide();
});
</script>

</body>
</html>
<?php
}
?>