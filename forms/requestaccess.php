<?php
include('head.php');

//no authentication required

?>

<!--Main layout-->
<main>
<div class="container-fluid">
	<!-- FORM: HEAD SECTION -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function(){
            const FORM_TIME_START = Math.floor((new Date).getTime()/1000);
            let formElement = document.getElementById("tfa_0");
            if (null === formElement) {
                formElement = document.getElementById("0");
            }
            let appendJsTimerElement = function(){
                let formTimeDiff = Math.floor((new Date).getTime()/1000) - FORM_TIME_START;
                let cumulatedTimeElement = document.getElementById("tfa_dbCumulatedTime");
                if (null !== cumulatedTimeElement) {
                    let cumulatedTime = parseInt(cumulatedTimeElement.value);
                    if (null !== cumulatedTime && cumulatedTime > 0) {
                        formTimeDiff += cumulatedTime;
                    }
                }
                let jsTimeInput = document.createElement("input");
                jsTimeInput.setAttribute("type", "hidden");
                jsTimeInput.setAttribute("value", formTimeDiff.toString());
                jsTimeInput.setAttribute("name", "tfa_dbElapsedJsTime");
                jsTimeInput.setAttribute("id", "tfa_dbElapsedJsTime");
                jsTimeInput.setAttribute("autocomplete", "off");
                if (null !== formElement) {
                    formElement.appendChild(jsTimeInput);
                }
            };
            if (null !== formElement) {
                if(formElement.addEventListener){
                    formElement.addEventListener('submit', appendJsTimerElement, false);
                } else if(formElement.attachEvent){
                    formElement.attachEvent('onsubmit', appendJsTimerElement);
                }
            }
        });
		
	</script>
	<script>
	$( document ).ready(function() {
		$('input.preventspace').keydown(function(e) {
		    if (e.keyCode == 32) {
		        return false;
		    }
		});	
	});
    </script>

    <link href="css/wforms-layout_v530-14.css" rel="stylesheet" type="text/css" />
    <link href="css/theme-52661.css" rel="stylesheet" type="text/css" />
    <link href="css/wforms-jsonly_v530-14.css" rel="alternate stylesheet" title="This stylesheet activated by javascript" type="text/css" />
    <script type="text/javascript" src="js/wforms_v530-14.js"></script>
    <script type="text/javascript" src="js/localization-en_US_v530-14.js"></script>

	<!-- FORM: BODY SECTION -->
	<div class="wFormContainer"  >
		<div class="wFormHeader"></div>
		<style type="text/css">
		#tfa_16,
		*[id^="tfa_16["] {
		    width: 280px !important;
		}


		#tfa_16-D,
		*[id^="tfa_16["][class~="field-container-D"] {
		    width: auto !important;
		}


		#tfa_37,
		*[id^="tfa_37["] {
		    width: 197px !important;
		}
		#tfa_37-D,
		*[id^="tfa_37["][class~="field-container-D"] {
		    width: auto !important;
		}
		.removeSpan {
			clear:both;
			top:-20px;
		}
		</style>
		<div class="">
			<div class="wForm" id="4689948-WRPR" dir="ltr">
				<div class="codesection" id="code-4689948"></div>
				<h3 class="wFormTitle" id="4689948-T">Web Access Form</h3>
				<form method="post" action="requestprocessor.php" class="hintsBelow labelsAbove" id="4689948" role="form">
				<div class="oneField field-container-D    " id="business_name-D">
					<label id="business_name-L" class="label preField " for="business_name">Business Name</label><br>
					<div class="inputWrapper"><input type="text" id="business_name" name="business_name" value="<?php echo urldecode($_GET['acctname']); ?>" title="Business Name" data-dataset-allow-free-responses=""></div>
				</div>
				<fieldset id="tfa_6" class="section">
				<legend id="tfa_6-L">Instructions</legend>
				<div class="htmlSection" id="tfa_7">
					<div class="htmlContent" id="tfa_7-HTML">
						<p class="MsoNormal"><span style="font-size:10.0pt;mso-bidi-font-size:11.0pt">MHW provides clients with various reports on our Client Reporting Dashboard (CRD). You will be able to obtain up-to-date information such as account receivable balances, sales and cash receipts, inventory on hand, historical data, etc. You will also be able to print copies of your Invoices, Sales Orders, and Purchase Orders, and download/email data in PDF format and excel spreadsheets. There are many “Quick Graphs” available to provide a more visual look at your data, as well as a customizable “Dashboard” that can give you a look into your business on one webpage.</span></p>

						<p class="MsoNormal"><span style="font-size:10.0pt;mso-bidi-font-size:11.0pt">&nbsp;</span></p>

						<p class="MsoNormal"><span style="font-size:10.0pt;mso-bidi-font-size:11.0pt">MHW provides an interface for our clients to manage access to their own information. Upon set-up, you must provide us with the name of your client “Administrator” that will be able to add and remove users, as well as limit the reports available to each user. There are pre-defined roles available for office staff, as well as salespeople (salespeople can be limited to data based on their sales if needed).</span></p>
						<p class="MsoNormal"><span style="font-size:10.0pt;mso-bidi-font-size:11.0pt"><br></span></p>
						<p class="MsoNormal"><span style="font-size: 13.3333px;">Completing this form to obtain access to our CRD is also acknowledgement that you have received, reviewed, and approved of the rates in our MHW Rate Card. The rate card was emailed to you and is available for you to download as a PDF on the CRD.</span></p>

						<p class="MsoNormal"><span style="font-size:10.0pt;mso-bidi-font-size:11.0pt">&nbsp;</span></p>

						<p class="MsoNormal"><span style="font-size:10.0pt;mso-bidi-font-size:11.0pt">We provide an additional website, our Integrated Compliance System (ICS), which will allow you to view all the compliance information for your products. You can see all Federal and State Compliance status, as well as the Distributor Appointments in applicable states. You can also use the reporting features to easily determine where your products are registered and where they can be sold. You can view COLA’s for each product, as well as request additional state registrations and view expected registration approvals through this portal.</span></p>

						<p class="MsoNormal"><span style="font-size:10.0pt;mso-bidi-font-size:11.0pt">&nbsp;</span></p>

						<p class="MsoNormal"><span style="font-size:10.0pt;mso-bidi-font-size:11.0pt">Please provide the following information and check the appropriate boxes to indicate the access to be given to each user.</span></p>
						<p class="MsoNormal"><span style="font-size:10.0pt;mso-bidi-font-size:11.0pt"><br></span></p>
						<p class="MsoNormal"><span style="font-size:10.0pt;mso-bidi-font-size:11.0pt"><i><u><b>NOTE: Users must be registered through this form to update product and item specifications as well as price posting information. At least 1 Account must mark "Receive Onboarding Emails" as "Yes".</b></u></i></span></p>
					</div>
				</div>
				</fieldset>
				<fieldset id="tfa_1[0]" class="repeat section" data-repeatlabel="Add another user">
				<legend id="tfa_1[0]-L">Account</legend>
				<div id="tfa_5[0]" class="section inline group">
					<div class="oneField field-container-D    " id="first_name[0]-D">
						<label id="first_name[0]-L" class="label preField reqMark" for="first_name[0]">First Name</label><br>
						<div class="inputWrapper">
							<input type="text" id="first_name[0]" name="first_name[0]" value="" aria-required="true" title="First Name" data-dataset-allow-free-responses="" class="required">
						</div>
					</div>
					<div class="oneField field-container-D    " id="last_name[0]-D">
						<label id="last_name[0]-L" class="label preField reqMark" for="last_name[0]">Last Name</label><br>
						<div class="inputWrapper">
							<input type="text" id="last_name[0]" name="last_name[0]" value="" aria-required="true" title="Last Name" data-dataset-allow-free-responses="" class="required">
						</div>
					</div>
				</div>
				<div id="tfa_28[0]" class="section inline group">
					<div class="oneField field-container-D    " id="tfa_user_id[0]-D">
						<label id="tfa_user_id[0]-L" class="label preField reqMark" for="tfa_user_id[0]">Requested User ID</label><br>
						<div class="inputWrapper">
							<input type="text" id="tfa_user_id[0]" name="tfa_user_id[0]" value="" maxlength="20" aria-required="true" aria-describedby="tfa_user_id[0]-HH" title="User ID" data-dataset-allow-free-responses="" class="required preventspace"><span class="field-hint-inactive" id="tfa_user_id[0]-H"><span id="tfa_user_id[0]-HH" class="hint">Up to 20 characters (NO spaces)</span></span>
						</div>
					</div>			
					<div class="oneField field-container-D    " id="tfa_password[0]-D">
						<label id="tfa_password[0]-L" class="label preField reqMark" for="tfa_password[0]">Requested Password</label><br>
						<div class="inputWrapper">
							<input type="text" id="tfa_password[0]" name="tfa_password[0]" value="" size="" aria-required="true" aria-describedby="tfa_password[0]-HH" maxlength="20" title="Password" class="required preventspace"><span class="field-hint-inactive" id="tfa_password[0]-H"><span id="tfa_password[0]-HH" class="hint">Up to 20 characters (NO spaces)</span></span>
						</div>
					</div>
					<div class="oneField field-container-D    " id="email[0]-D">
						<label id="email[0]-L" class="label preField reqMark" for="email[0]">Email</label><br>
						<div class="inputWrapper">
							<input type="text" id="email[0]" name="email[0]" value="" aria-required="true" title="Email" data-dataset-allow-free-responses="" class="validate-email required">
						</div>
					</div>
					<div class="oneField field-container-D    " id="role[0]-D">
						<label id="role[0]-L" class="label preField reqMark" for="role[0]">Role</label><br>
						<div class="inputWrapper">
							<select id="role[0]" name="role[0]" title="Role" aria-required="true" class="required">
							<option value="">Please select...</option>
							<option value="Administrator" id="tfa_17[0]" class="">Administrator</option>
							<option value="Manager" id="tfa_18[0]" class="">Manager</option>
							<option value="Salesperson" id="tfa_19[0]" class="">Salesperson</option></select>
						</div>
					</div>
				</div>
				<div id="tfa_36[0]" class="section inline group">
					<div class="oneField field-container-D    " id="ics[0]-D">
						<label id="ics[0]-L" class="label preField reqMark" for="ics[0]">ICS Compliance Reports?</label><br>
						<div class="inputWrapper">
							<select id="ics[0]" name="ics[0]" title="ICS Compliance Reports?" aria-required="true" class="required">
							<option value="">Please select...</option>
							<option value="Yes" id="tfa_21[0]" class="">Yes</option>
							<option value="No" id="tfa_22[0]" class="">No</option></select>
						</div>
					</div>
					<div class="oneField field-container-D    " id="onboarding[0]-D">
						<label id="onboarding[0]-L" class="label preField reqMark" for="onboarding[0]">Receive Onboarding Emails</label><br>
						<div class="inputWrapper">
							<select id="onboarding[0]" name="onboarding[0]" title="Receive Onboarding Emails" aria-required="true" aria-describedby="onboarding[0]-HH" class="calc-OnboardingEmails required">
							<option value="">Please select...</option>
							<option value="Yes" id="tfa_34[0]" class="calcval-1" data-default-value="true">Yes</option>
							<option value="No" id="tfa_35[0]" class="calcval-0">No</option></select>
							<span class="field-hint-inactive" id="onboarding[0]-H"><span id="onboard[0]-HH" class="hint">(At least 1 contact must be marked Yes)</span></span>
						</div>
					</div>
				</div>

				</fieldset>

				<div class="oneField field-container-D    " id="tfa_37-D">
					<label id="tfa_37-L" class="label preField " for="tfa_37">Onboarding Email Recipients</label><br>
					<div class="inputWrapper">
						<input type="text" id="tfa_37" name="tfa_37" value="" readonly title="Onboarding Email Recipients" data-dataset-allow-free-responses="" class="validate-custom /[^0]+/ formula=OnboardingEmails readonly">
					</div>
					
					<script type="text/javascript">
	                if(typeof wFORMS != 'undefined') {
	                    if(wFORMS.behaviors.validation) {
	                        wFORMS.behaviors.validation.rules['customtfa_37'] =  { selector: '*[id="tfa_37"]', check: 'validateCustom'};
	                        wFORMS.behaviors.validation.messages['customtfa_37'] = "At least one contact must receive onboarding emails";
	                    }
	                }
					</script>
				</div>
				<input type="hidden" id="tfa_1[0]-RC" name="tfa_1[0]-RC" value="0" class="">
				<div class="actions" id="4689948-A">
					<input type="submit" data-label="Submit" class="primaryAction" id="submit_button" value="Submit">
				</div>
				<div style="clear:both"></div>
				<input type="hidden" value="4689948" name="tfa_dbFormId" id="tfa_dbFormId">
				<input type="hidden" value="" name="tfa_dbResponseId" id="tfa_dbResponseId">
				<input type="hidden" value="d33e96fe3b7c7bb06adc03c2d485aa35" name="tfa_dbControl" id="tfa_dbControl">
				<input type="hidden" value="28" name="tfa_dbVersionId" id="tfa_dbVersionId">
				<input type="hidden" value="" name="tfa_switchedoff" id="tfa_switchedoff">
				</form>
			</div>
		</div>
		<div class="wFormFooter">
			<p class="supportInfo"><br></p>
		</div>
		<p class="supportInfo" >
		</p>
	</div>

</div>
</main>
<!--Main layout-->
  
</html>