$(document).ready(function(){1
    var allowedStates = ["NY", "NJ", "CA", "FL"];
    var allowsubmit = "false";

    $('input[name=requesttype]').change(function(){
        var value = $( 'input[name=requesttype]:checked' ).val();
        if (value == 'requestdistributor') {
            
            $("[name='samplerequestdate']").prop("required", true);
            $("[name='samplesolicitordate']").prop("required", false);
            $("[name='eventdate']").prop("required", false);
            $("[name='eventdate2']").prop("required", false);
            $("[name='sampledate2']").prop("required", false);
            $("[name='eventstateeventonly']").prop("required", false);
            $("[name='eventstatesampleevent']").prop("required", false);
            $("[name='othertxt']").prop("required", false);

            $("[name='samplerequestdate']").val("");
            $("[name='samplesolicitordate']").val("");
            $("[name='eventdate']").val("");
            $("[name='eventdate2']").val("");
            $("[name='sampledate2']").val("");
            $("[name='eventstateeventonly']").val("");
            $("[name='eventstatesampleevent']").val("");
            $("[name='othertxt']").val("");

            Section3Hide();
            Section5Hide();
            RenumberRemoveSections();

            SameAddressSectionHide(); 
            SpecificAddressSectionShow();
  
            $('#alerts').hide();
        
        } else if (value === 'solicitoraccountvisits') {
            $("[name='samplerequestdate']").prop("required", false);
            $("[name='samplesolicitordate']").prop("required", true);
            $("[name='eventdate']").prop("required", false);
            $("[name='eventdate2']").prop("required", false);
            $("[name='sampledate2']").prop("required", false);
            $("[name='eventstateeventonly']").prop("required", false);
            $("[name='eventstatesampleevent']").prop("required", false);
            $("[name='othertxt']").prop("required", false); 

            $("[name='samplerequestdate']").val("");
            $("[name='samplesolicitordate']").val("");
            $("[name='eventdate']").val("");
            $("[name='eventdate2']").val("");
            $("[name='sampledate2']").val("");
            $("[name='eventstateeventonly']").val("");
            $("[name='eventstatesampleevent']").val("");
            $("[name='othertxt']").val("");
   
            Section3Hide();
            Section5Hide();
            RenumberRemoveSections();
            
            SameAddressSectionHide(); 
            SpecificAddressSectionShow();

            $('#alerts').hide();

        } else if (value === 'eventandsample') {
            $("[name='samplerequestdate']").prop("required", false);
            $("[name='samplesolicitordate']").prop("required", false);
            $("[name='eventdate']").prop("required", false);
            $("[name='eventdate2']").prop("required", true);
            $("[name='sampledate2']").prop("required", true);
            $("[name='eventstatesampleevent']").prop("required", true);
            $("[name='eventstateeventonly']").prop("required", false); 
            $("[name='othertxt']").prop("required", false);

            $("[name='samplerequestdate']").val("");
            $("[name='samplesolicitordate']").val("");
            $("[name='eventdate']").val("");
            $("[name='eventdate2']").val("");
            $("[name='sampledate2']").val("");
            $("[name='eventstateeventonly']").val("");
            $("[name='eventstatesampleevent']").val("");
            $("[name='othertxt']").val("");

            Section3Show();
            Section5Show();
            RenumberAddSections();

            SameAddressSectionShow(); 
            SpecificAddressSectionHide();

            $('#alerts').hide();

        } else if (value === 'eventonly') {
            $("[name='samplerequestdate']").prop("required", false);
            $("[name='samplesolicitordate']").prop("required", false);
            $("[name='eventdate']").prop("required", true);
            $("[name='eventdate2']").prop("required", false);
            $("[name='sampledate2']").prop("required", false);
            $("[name='eventstateeventonly']").prop("required", true);
            $("[name='eventstatesampleevent']").prop("required", false);
            $("[name='othertxt']").prop("required", false);

            $("[name='samplerequestdate']").val("");
            $("[name='samplesolicitordate']").val("");
            $("[name='eventdate']").val("");
            $("[name='eventdate2']").val("");
            $("[name='sampledate2']").val("");
            $("[name='eventstateeventonly']").val("");
            $("[name='eventstatesampleevent']").val("");
            $("[name='othertxt']").val("");

            Section3Show();
            Section5Show();
            RenumberAddSections(); 

            SameAddressSectionShow(); 
            SpecificAddressSectionHide();
            
        } else if (value === 'other') {
            $("[name='samplerequestdate']").prop("required", false);
            $("[name='samplesolicitordate']").prop("required", false);
            $("[name='othertxt']").prop("required", true);
            $("[name='eventdate']").prop("required", false);
            $("[name='eventdate2']").prop("required", false);
            $("[name='sampledate2']").prop("required", false);
            $("[name='eventstateeventonly']").prop("required", false);
            $("[name='eventstatesampleevent']").prop("required", false); 
            
            $("[name='samplerequestdate']").val("");
            $("[name='samplesolicitordate']").val("");
            $("[name='eventdate']").val("");
            $("[name='eventdate2']").val("");
            $("[name='sampledate2']").val("");
            $("[name='eventstateeventonly']").val("");
            $("[name='eventstatesampleevent']").val("");
            $("[name='othertxt']").val("");

            Section3Hide();  
            Section5Hide();
            RenumberRemoveSections();

            SameAddressSectionHide(); 
            SpecificAddressSectionShow();
            $('#alerts').hide();
        }
    });

    $('[name=country]').change(function(){
        var countryStr = $('[name=country]').val();
        if ((countryStr.toLowerCase() !== 'United States'.toLowerCase()) && (countryStr.toLowerCase() !== 'US'.toLowerCase())) { 
            $('[name=state]').val("XX");
        } 
      });

      $('[name=deliverycountry]').change(function(){
        var countryStr = $('[name=deliverycountry]').val();
        if ((countryStr.toLowerCase() !== 'United States'.toLowerCase()) && (countryStr.toLowerCase() !== 'US'.toLowerCase())) { 
            $('[name=deliverystate]').val("XX");
        } 
      });

      
    $('[name=state]').change(function(){
        var countryStr = $('[name=country]').val();
        if ((countryStr.toLowerCase() !== 'United States'.toLowerCase()) && (countryStr.toLowerCase() !== 'US'.toLowerCase()) && (countryStr !== '')) { 
            $('[name=state]').val("XX");
        } 
      });

      $('[name=deliverystate]').change(function(){
        var countryStr = $('[name=deliverycountry]').val();
        if ((countryStr.toLowerCase() !== 'United States'.toLowerCase()) && (countryStr.toLowerCase() !== 'US'.toLowerCase()) && (countryStr !== '')) { 
            $('[name=deliverystate]').val("XX");
        } 
      });

    $("#tfa_product_desc").change(function(){
        var eventNotificationType = $( 'input[name=requesttype]:checked' ).val();
        var prodID = $('#prodID').val();

           if ($(this).val() === "") {
                $("#AddNewProduct").prop('disabled', true);
           } else if ($(this).val() !== "" &&  (typeof eventNotificationType == "undefined")) {
                $("#AddNewProduct").prop('disabled', true);
           } else if (prodID == '') { 
                $("#AddNewProduct").prop('disabled', true);
           } else {
             $("#AddNewProduct").prop('disabled', false);
        }
      });


      $("#AddNewProduct").click(function(){
        var eventNotificationType = $( 'input[name=requesttype]:checked' ).val();
        if (eventNotificationType === 'eventonly') {
            $('#alerts').show();
        } else {
            $('#alerts').hide();
        }

        var currentRow= $('.items').last();
            var bottlequan = currentRow.find('[id^=bottlequan]');
            var casequan = currentRow.find('[id^=casequan]');

            bottlequan.change(function (event) {
                if ($(this).val() > 0 ) {
                   $(this).prop("required", true);
                   $('#casequan').prop("required", false);
                   $('#casequan').val("");
                } else {
                    $(this).prop("required", true);
                    $('#casequan').prop("required", false);
                    $('#casequan').val("");
                }
            });

            casequan.change(function (event) {
                if ($(this).val() > 0 ) {
                    $(this).prop("required", true);
                    $('#bottlequan').prop("required", false);
                    $('#bottlequan').val("");
                 } else {
                     $(this).prop("required", false);
                     $('#bottlequan').prop("required", true);
                     $('#bottlequan').val("");
                 }
            });
      });

      $('input[name=deliverytype]').change(function(){
        $('input[name="delivery"]').prop('checked', false);
        $('input[name="pickuptype"]').prop('checked', false);
        ClearSection3Delivery();
        SpecificAddressSectionHide();
        var value = $( 'input[name=deliverytype]:checked' ).val();
        if (value === "delivery") {
            $("[name='delivery']").prop("required", true);
            $("[name='pickuptype']").prop("required", false);
        } else if (value === "pickup") {
            $("[name='delivery']").prop("required", false);
            $("[name='pickuptype']").prop("required", true);
        }
      });

    $('input[name=delivery]').change(function(){
        var value = $( 'input[name=delivery]:checked' ).val();
        if (value === 'distributor') {
            $("[name='pickupdisttxt']").prop("required", true);
            $("[name='deliveryaddress']").prop("required", false);
            $("[name='deliveryaddress2']").prop("required", false);
            $("[name='deliverycity']").prop("required", false);
            $("[name='deliverystate']").prop("required", false);
            $("[name='deliveryzip']").prop("required", false);
            $("[name='deliverycountry']").prop("required", false);

            $("[name='pickupdisttxt']").val();
            $("[name='deliveryaddress']").val();
            $("[name='deliveryaddress2']").val();
            $("[name='deliverycity']").val();
            $("[name='deliverystate']").val();
            $("[name='deliveryzip']").val();
            $("[name='deliverycountry']").val();

        } else if (value === 'specificAddress') {
            $("[name='pickupdisttxt']").prop("required", false);
            $("[name='deliveryaddress']").prop("required", true);
            $("[name='deliveryaddress2']").prop("required", false);
            $("[name='deliverycity']").prop("required", true);
            $("[name='deliverystate']").prop("required", true);
            $("[name='deliveryzip']").prop("required", true);
            $("[name='deliverycountry']").prop("required", true);

            $("[name='pickupdisttxt']").val("");
            $("[name='deliveryaddress']").val("");
            $("[name='deliveryaddress2']").val("");
            $("[name='deliverycity']").val("");
            $("[name='deliverystate']").val("");
            $("[name='deliveryzip']").val("");
            $("[name='deliverycountry']").val();
        } else if (value === 'sameAddress') {
            $("[name='pickupdisttxt']").prop("required", false);
            $("[name='deliveryaddress']").prop("required", false);
            $("[name='deliveryaddress2']").prop("required", false);
            $("[name='deliverycity']").prop("required", false);
            $("[name='deliverystate']").prop("required", false);
            $("[name='deliveryzip']").prop("required", false);
            $("[name='deliverycountry']").prop("required", false);

            $("[name='pickupdisttxt']").val("");
            $("[name='deliveryaddress']").val("");
            $("[name='deliveryaddress2']").val("");
            $("[name='deliverycity']").val("");
            $("[name='deliverystate']").val("");
            $("[name='deliveryzip']").val("");
            $("[name='deliverycountry']").val();
        }      
    });

    $('input[name=pickuptype]').change(function(){
        var value = $( 'input[name=pickuptype]:checked' ).val();
        ClearSection3Delivery();
        
        if (value === 'westernwine') {
            $("[name='pickupothertxt']").prop("required", false);
            $("[name='pickupothertxt']").val("");
        } else if (value === 'westerncarriers') {
            $("[name='pickupothertxt']").prop("required", false);
            $("[name='pickupothertxt']").val("");
        } else if (value === 'pwd') {
            $("[name='pickupothertxt']").prop("required", false);
            $("[name='pickupothertxt']").val("");            
        } else if (value === 'greystone') {
            $("[name='pickupothertxt']").prop("required", false);
            $("[name='pickupothertxt']").val("");
        }  else if (value === 'other') {
            $("[name='pickupothertxt']").prop("required", true);
            $("[name='pickupothertxt']").val("");
        } 
    });


    $('input[name=sampleuser]').change(function(){
        var value = $( 'input[name=sampleuser]:checked' ).val();
        if (value === 'MHW') {
            $("[name='sampleusermhw']").prop("required", true);
            $("[name='sampleuserrep']").prop("required", false);
            $("[name='sampleuserbroker']").prop("required", false);

            $("[name='sampleuserbroker']").val("");
            $("[name='sampleuserrep']").val("");
        } else if (value === 'broker') {
            $("[name='sampleuserbroker']").prop("required", true);
            $("[name='sampleuserrep']").prop("required", false);
            $("[name='sampleusermhw']").prop("required", false);

            $("[name='sampleusermhw']").val("");
            $("[name='sampleuserrep']").val("");

        } else if (value === 'rep') {
            $("[name='sampleuserrep']").prop("required", true);
            $("[name='sampleuserbroker']").prop("required", false);
            $("[name='sampleusermhw']").prop("required", false);

            $("[name='sampleusermhw']").val("");
            $("[name='sampleuserbroker']").val("");
        } 
    });

    IsNotValidStateEventOnly();
    IsNotValidStateSampleReqEvent();

    function IsNotValidStateEventOnly() {   
        $("#eventstateeventonly").change(function () 
        {
            DisplayErrorMsgIsNotValidStateEventOnly();
        });
    }

    function IsNotValidStateSampleReqEvent() {
        $("#eventstatesampleevent").change(function () 
        {
            DisplayErrorMsgStateSampleReqEvent()
        });
    }

    function DisplayErrorMsgIsNotValidStateEventOnly() {
        if (!IsValidStateEventOnly()) {
            $('#eventTypeWarningMesg').modal('show');  
        } 
    }

    function DisplayErrorMsgStateSampleReqEvent() {
            if (!IsValidStateSampleReqEvent()) {  
                $('#eventTypeWarningMesg').modal('show'); 
            } 
    }

    function IsValidStateEventOnly() {
        var selValue = $("#eventstateeventonly").val();
        if (allowedStates.indexOf(selValue) >= 0) {
            return true; 
        } else {
            return false; 
        }
    }

    function IsValidStateSampleReqEvent() {
        var selValue = $("#eventstatesampleevent").val();
        if (allowedStates.indexOf(selValue) >= 0) {  
            return true; 
        } else {
            return false; 
        }
    }


    function Section3Hide() {
        $("#eventDetails").hide();
        $("[name='eventdesc']").prop("required", false);
        $("[name='eventname']").prop("required", false);
        $("[name='eventtype']").prop("required", false);
        ClearSection3Delivery();
    }

    function Section3Show() {
        $("#eventDetails").show();
        $("[name='eventdesc']").prop("required", true);
        $("[name='eventname']").prop("required", true);
        $("[name='eventtype']").prop("required", true);
        $("[name='address']").prop("required", true);
        $("[name='address2']").prop("required", false);
        $("[name='city']").prop("required", true);
        $("[name='state']").prop("required", true);
        $("[name='zip']").prop("required", true);
        $("[name='country']").prop("required", true);
    }

    function ClearSection3Delivery() {     
        $("[name='pickupdisttxt']").prop("required", false);
        $("[name='address']").prop("required", false);
        $("[name='address2']").prop("required", false);
        $("[name='city']").prop("required", false);
        $("[name='state']").prop("required", false);
        $("[name='zip']").prop("required", false);
        $("[name='country']").prop("required", false);
        $("[name='pickupothertxt']").prop("required", false);

        $("[name='pickupdisttxt']").val("")
        $("[name='address']").val("");
        $("[name='address2']").val("");
        $("[name='city']").val("");
        $("[name='state']").val("");
        $("[name='zip']").val("");
        $("[name='country']").val("");
        $("[name='pickupothertxt']").val("");
    }

    function Section5Hide() {
        $("#whosection").hide();
        $("[name='sampleuser']").prop("required", false);

        $("[name='sampleusermhw']").prop("required", false);
        $("[name='sampleuserbroker']").prop("required", false);
        $("[name='sampleuserrep']").prop("required", false);
  
        $('input[name="sampleuser"]').prop('checked', false);
        $("[name='sampleusermhw']").val("");
        $("[name='sampleuserbroker']").val("");
        $("[name='sampleuserrep']").val("");
    }

    function Section5Show() {
        $("#whosection").show();

        $("input[name='sampleuser']").attr("checked", false);
        $("[name='sampleuser']").prop("required", true);

        $("[name='sampleusermhw']").prop("required", false);
        $("[name='sampleuserbroker']").prop("required", false);
        $("[name='sampleuserrep']").prop("required", false);

        $("[name='sampleusermhw']").val("");
        $("[name='sampleuserbroker']").val("");
        $("[name='sampleuserrep']").val("");

    }

    function SpecificAddressSectionShow() {
        $("#specificAddressSection").show();
        $("#showSpecificAddressFieldSection").removeClass('d-none');

        $("[name='sampleusermhw']").prop("required", false);
        $("[name='sampleuserbroker']").prop("required", false);
        $("[name='sampleuserrep']").prop("required", false);

        $("[name='sampleusermhw']").val("");
        $("[name='sampleuserbroker']").val("");
        $("[name='sampleuserrep']").val("");
    }

    function SameAddressSectionShow() {
        $("#sameAddressSection").show();

        $("[name='deliveryaddress']").prop("required", true);
        $("[name='deliveryaddress2']").prop("required", false);
        $("[name='deliverycity']").prop("required", true);
        $("[name='deliverystate']").prop("required", true);
        $("[name='deliveryzip']").prop("required", true);
        $("[name='deliverycountry']").prop("required", true);
    }

    function SpecificAddressSectionHide() {
        $("#specificAddressSection").hide();
        $("#showSpecificAddressFieldSection").addClass('d-none');
        
        $("[name='deliveryaddress']").prop("required", false);
        $("[name='deliveryaddress2']").prop("required", false);
        $("[name='deliverycity']").prop("required", false);
        $("[name='deliverystate']").prop("required", false);
        $("[name='deliveryzip']").prop("required", false);
        $("[name='deliverycountry']").prop("required", false);

        $("[name='deliveryaddress']").val("");
        $("[name='deliveryaddress2']").val("");
        $("[name='deliverycity']").val("");
        $("[name='deliverystate']").val("");
        $("[name='deliveryzip']").val("");
        $("[name='deliverycountry']").val("");
    }

    function SameAddressSectionHide() {
        $("#sameAddressSection").hide();
    }

    function RenumberAddSections() 
    {
        $("#section3").text("3.");
        $("#section4").text("4.");
        $("#section5").text("5.");
    }

    function RenumberRemoveSections() 
    {
        $("#section4").text("3.");
    }

    $('form:first').submit(function(e) {
        if (allowsubmit == "true") {
            allowsubmit = "false";
            return true;
        } 
        
        if (validForm()) {
            return true;
        } else {
            return false;
        }
   }); 

    $("#okbutton").click(function(){
        allowsubmit = "true";
        $("form:first").submit();
        $("#okbutton").prop('disabled', true);
    });

    $("#cancelButton").click(function(){
        allowsubmit = "false";
        $("#submit_button").prop('disabled', false);
        $("#submit_button").prop("value", "Submit"); 
        $("#submit_button").css("color", "#ffffff");
    });
    

      $(document.body).on('change', '.attachmentButton', function (e) {
        var docRowId = $(this).attr('data-doc-row');

        //hide the div we're in
        var theDiv = $('#' + docRowId).hide();

        //append a new <li> to the docsListing <ul>
        $('#docsListing').append('<div id="span-' + docRowId + '" class="pad-right-15">' + this.files[0].name + '<i class="fa fa-times-circle-o arm-font-16 text-danger pad-left-5 removeUpload arm-pointer" aria-label="Remove" title="Remove" data-doc-row="' + docRowId + '"></i></div>');

        //add another file and button to the page

        //in the first div of the attachmentTemplate add in a guid to uniquely identify the row (so we can delete if need be)
        var uniqueId = guid();
        $('#attachmentTemplate').find('.row').attr('id', uniqueId);
        $('#attachmentTemplate').find('.attachmentButton').attr('data-doc-row', uniqueId);

        $('#docsDiv').append($('#attachmentTemplate').html());
    });

    $(document.body).on('click', '.removeUpload', function (e) {
        //nuke it from the form
        var docRowId = $(this).attr('data-doc-row');
        $('#span-' + docRowId).remove();
        $('#' + docRowId).remove();
    });

function validForm() {
    var firstSentence = "Less than 20 days' notice for events in CA, NJ, & NY are subject to a rushed vetting fee of $150.00 per request.";
    var secondSentence = "Less than 10 days notice is subject to rejection."

    var value = $('input[name=requesttype]:checked').val();   
    if (value == 'requestdistributor') {
        var today = moment();
        var samplerequestdateValue = $('input[name=samplerequestdate]').val().replace(/-/, '/').replace(/-/, '/');
        var samplerequestdateValueDT = moment(samplerequestdateValue);

        if (samplerequestdateValueDT.isBefore(today, 'day')) {
            $('#okMesg #ModalText').text("Dates in the past are not allowed!"); 
            $('#okMesg').modal('show'); 
            return false;
        } else if (samplerequestdateValueDT.isSame(today, 'day')) {
            $('#warningMesg #ModalText').text("For immediate pickup fees may apply, contact MHW directly, or select tomorrow or a future date"); 
            $('#warningMesg').modal('show'); 
           return false;           
        } 

    } else if (value === 'solicitoraccountvisits') {
        var today = moment();
        var samplesolicitordateValue = $('input[name=samplesolicitordate]').val().replace(/-/, '/').replace(/-/, '/');
        var samplesolicitordateDT = moment(samplesolicitordateValue);

        if (samplesolicitordateDT.isBefore(today, 'day')) {
            $('#okMesg #ModalText').text("Dates in the past are not allowed!"); 
            $('#okMesg').modal('show'); 
            return false;
            
        } else if (samplesolicitordateDT.isSame(today, 'day')) {
            $('#warningMesg #ModalText').text("For immediate pickup fees may apply, contact MHW directly, or select tomorrow or a future date"); 
            $('#warningMesg').modal('show'); 
            return false;           
        }

    } else if (value === 'eventandsample') {   
        if (!IsValidStateSampleReqEvent()) {
           DisplayErrorMsgStateSampleReqEvent();
           return false;
        } 
        var eventdateValue = $('input[name=eventdate2]').val().replace(/-/, '/').replace(/-/, '/');
        var sampledate2Value = $('input[name=sampledate2]').val().replace(/-/, '/').replace(/-/, '/');
        var eventdateValueDT = moment(eventdateValue);
        var sampledate2ValueDT = moment(sampledate2Value);
        var today = moment();
        
        var todaydateValue10DaysDT = moment(moment(), "DD-MM-YYYY").add(10, 'days');
        var todaydateValue20DaysDT = moment(moment(), "DD-MM-YYYY").add(20, 'days');
        
        if (eventdateValueDT.isBefore(today, 'day')) {
            $('#okMesg #ModalText').text("Dates in the past are not allowed!"); 
            $('#okMesg').modal('show'); 
            return false;
        }

        if (sampledate2ValueDT.isBefore(today, 'day')) {
            $('#okMesg #ModalText').text("Dates in the past are not allowed!"); 
            $('#okMesg').modal('show'); 
            return false;
        } else if (sampledate2ValueDT.isSame(today, 'day')) {
            $('#warningMesg #ModalText').text("For immediate pickup fees may apply, contact MHW directly, or select tomorrow or a future date"); 
            $('#warningMesg').modal('show'); 
            return false;           
        } else if (sampledate2ValueDT.isBefore(todaydateValue10DaysDT, 'day')) {
            $('#warningMesg #ModalText').text(firstSentence + '\n\n' + secondSentence); 
            $('#warningMesg').modal('show');  
            return false;
        } else if (sampledate2ValueDT.isBefore(todaydateValue20DaysDT, 'day')) {
            $('#warningMesg #ModalText').text(firstSentence); 
            $('#warningMesg').modal('show');  
            return false;
        }
    } else if (value === 'eventonly') {
        if (!IsValidStateEventOnly()) {
           DisplayErrorMsgIsNotValidStateEventOnly();
           return false;
        }

        var eventdateValue = $('input[name=eventdate]').val().replace(/-/, '/').replace(/-/, '/');
        var eventdateValueDT = moment(eventdateValue);
        var today = moment();
        
        var todaydateValue10DaysDT = moment(moment(), "DD-MM-YYYY").add(10, 'days');
        var todaydateValue20DaysDT = moment(moment(), "DD-MM-YYYY").add(20, 'days');
       
        if (eventdateValueDT.isBefore(today, 'day')) {
            $('#okMesg #ModalText').text("Dates in the past are not allowed!"); 
            $('#okMesg').modal('show'); 
            return false;
        }
        
        if (eventdateValueDT.isBefore(todaydateValue10DaysDT, 'day')) {
            $('#warningMesg #ModalText').text(firstSentence + '\n\n' + secondSentence); 
            $('#warningMesg').modal('show');  
            return false;
        }

        if (eventdateValueDT.isBefore(todaydateValue20DaysDT, 'day')) {
            $('#warningMesg #ModalText').text(firstSentence); 
            $('#warningMesg').modal('show');  
            return false;
        }       
    }
    return true;
}
    /**
     * Generates a GUID string.
     * @returns {String} The generated GUID.
     * @example af8a8416-6e18-a307-bd9c-f2c947bbb3aa
     * @author Slavik Meltser (slavik@meltser.info).
     * @link http://slavik.meltser.info/?p=142
     */
    function guid() {
        function _p8(s) {
            var p = (Math.random().toString(16)+"000000000").substr(2,8);
            return s ? "-" + p.substr(0,4) + "-" + p.substr(4,4) : p ;
        }
        return _p8() + _p8(true) + _p8(true) + _p8();
    }
});