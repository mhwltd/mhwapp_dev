<?php
session_start();
include("prepend.php");
include("settings.php");

if (!isset($_SESSION['mhwltdphp_user'])) {
    header("Location: login.php");
} else {
    include('head.php');
    include('dbconnect.php');
    $uniqueId = uniqid();
    
    //if(isset($_POST["submit"])) {
    if (isset($_POST["requesttype"]) !== false) {
        //var_dump($_FILES["docs"]);

        $requesttype = (empty($_POST['requesttype'])) ? null : $_POST['requesttype'];

        $samplerequestdate = (empty($_POST['samplerequestdate'])) ? null : $_POST['samplerequestdate'];
        $samplesolicitordate = (empty($_POST['samplesolicitordate'])) ? null : $_POST['samplesolicitordate'];
        $eventdate = (empty($_POST['eventdate'])) ? null : $_POST['eventdate'];
        $sampledate = (empty($_POST['sampledate'])) ? null : $_POST['sampledate'];
        $eventdate2 = (empty($_POST['eventdate2'])) ? null : $_POST['eventdate2'];
        $sampledate2 = (empty($_POST['sampledate2'])) ? null : $_POST['sampledate2'];

        $eventstatesampleevent = (empty($_POST['eventstatesampleevent'])) ? null : $_POST['eventstatesampleevent'];
        $eventstateeventonly = (empty($_POST['eventstateeventonly'])) ? null : $_POST['eventstateeventonly'];
        $othertxt = (empty($_POST['othertxt'])) ? null : $_POST['othertxt'];
        
        $deliverytype = (empty($_POST['deliverytype'])) ? null : $_POST['deliverytype'];
        $delivery = (empty($_POST['delivery'])) ? null : $_POST['delivery'];
        
        $pickuptype = (empty($_POST['pickuptype'])) ? null : $_POST['pickuptype'];
        $pickupdisttxt = (empty($_POST['pickupdisttxt'])) ? null : $_POST['pickupdisttxt'];
        $deliveryaddress = (empty($_POST['deliveryaddress'])) ? null : $_POST['deliveryaddress'];
        $deliveryaddress2 = (empty($_POST['deliveryaddress2'])) ? null : $_POST['deliveryaddress2'];
        $deliverycity = (empty($_POST['deliverycity'])) ? null : $_POST['deliverycity'];
        $deliverystate = (empty($_POST['deliverystate'])) ? null : $_POST['deliverystate'];
        $deliveryzip = (empty($_POST['deliveryzip'])) ? null : $_POST['deliveryzip'];
        $deliverycountry = (empty($_POST['deliverycountry'])) ? null : $_POST['deliverycountry'];
        $pickupothertxt = (empty($_POST['pickupothertxt'])) ? null : $_POST['pickupothertxt'];

        $eventdesc = (empty($_POST['eventdesc'])) ? null : $_POST['eventdesc'];
        $eventname = (empty($_POST['eventname'])) ? null : $_POST['eventname'];
        $eventtype = (empty($_POST['eventtype'])) ? null : $_POST['eventtype'];

        $address = (empty($_POST['address'])) ? null : $_POST['address'];
        $address2 = (empty($_POST['address2'])) ? null : $_POST['address2'];
        $city = (empty($_POST['city'])) ? null : $_POST['city'];
        $state = (empty($_POST['state'])) ? null : $_POST['state'];
        $zip = (empty($_POST['zip'])) ? null : $_POST['zip'];
        $country = (empty($_POST['country'])) ? null : $_POST['country'];
      
        $sampleuser = (empty($_POST['sampleuser'])) ? null : $_POST['sampleuser'];
        $sampleusermhw = (empty($_POST['sampleusermhw'])) ? null : $_POST['sampleusermhw'];
        $sampleuserbroker = (empty($_POST['sampleuserbroker'])) ? null : $_POST['sampleuserbroker'];
        $sampleuserrep = (empty($_POST['sampleuserrep'])) ? null : $_POST['sampleuserrep'];

        $userID = $_POST['userID'];
        
        //echo "<pre>";
        //var_dump($_REQUEST);
        //echo "</pre>";

        //Establishes the connection
        $conn = sqlsrv_connect($serverName, $connectionOptions);

        if ($conn === false) {
            //echo "Connection could not be established.<br />";
            die(print_r(sqlsrv_errors(), true));
        }

        $mainsql = "INSERT INTO mhw_app_event_form (requesttype, samplerequestdate,  samplesolicitordate, eventdate, sampledate, eventdate2, sampledate2,
		                        eventstatesampleevent, eventstateeventonly, othertxt, deliverytype, delivery, pickupdisttxt,
								eventdesc, eventname, eventtype, address, address2, city, state, zip, country,
								deliveryaddress, deliveryaddress2, deliverycity, deliverystate, deliveryzip, deliverycountry,
								pickupothertxt, sampleuser, sampleusermhw, sampleuserbroker, sampleuserrep, created_by) 
					VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?); SELECT SCOPE_IDENTITY() AS IDENTITY_COLUMN_NAME";
        
        // prepare and bind
        $mainparams = array($requesttype, $samplerequestdate, $samplesolicitordate, $eventdate,  $sampledate, $eventdate2, $sampledate2,
                            $eventstatesampleevent, $eventstateeventonly, $othertxt, $deliverytype, $delivery, $pickupdisttxt,
                            $eventdesc, $eventname, $eventtype, $address, $address2, $city, $state, $zip, $country,
                            $deliveryaddress, $deliveryaddress2, $deliverycity, $deliverystate, $deliveryzip, $deliverycountry,
                            $pickupothertxt, $sampleuser, $sampleusermhw, $sampleuserbroker, $sampleuserrep, $userID);
        $stmt = sqlsrv_query($conn, $mainsql, $mainparams);

        if ($stmt === false) {
            die(print_r(sqlsrv_errors(), true));
        }

        sqlsrv_next_result($stmt);  //note this line!!
        sqlsrv_fetch($stmt);
        $eventId = sqlsrv_get_field($stmt, 0);


        //****** Event Product ***********
        $eventproductsql = "INSERT INTO mhw_event_product (event_id, product_name, bottle_quantity, case_quantity, created_by) 
		VALUES (?, ?, ?, ?, ?)";

        $productarray = (empty($_POST['product'])) ? null : $_POST['product'];
        $bottlequanarray = (empty($_POST['bottlequan'])) ? null : $_POST['bottlequan'];
        $casequanarray = (empty($_POST['casequan'])) ? null : $_POST['casequan'];

        for ($x = 0; $x < sizeof($productarray); $x++) {
            $prod = (empty($productarray[$x])) ? null : $productarray[$x];
            $bottle = (empty($bottlequanarray[$x])) ? null : $bottlequanarray[$x];
            $case = (empty($casequanarray[$x])) ? null : $casequanarray[$x];
            
            $eventproductparams = array($eventId, $prod, $bottle, $case, $userID);
            $eventproductstmt = sqlsrv_query($conn, $eventproductsql, $eventproductparams);

            if ($eventproductstmt === false) {
                die(print_r(sqlsrv_errors(), true));
            }
        }
        


        //****** Upload File ***********
        $uploadFileSql = "INSERT INTO mhw_event_file (event_id, filename, filetype, attachment, created_by) VALUES (?, ?, ?, cast (? as varbinary(max)), ?)";
        
        $countfiles = count($_FILES['file']['name']);

        // Looping all files
        for ($i=0;$i<$countfiles;$i++) {
            $tempName = trim($_FILES["file"]["name"][$i]);
            $baseFileName = trim(basename($_FILES["file"]["name"][$i]));

            if (!empty($baseFileName)) {
                $file_name = trim($_FILES["file"]["name"][$i]);
                $file_type = trim($_FILES["file"]["type"][$i]);
                $fileSize  = trim($_FILES["file"]["size"][$i]);
                $tmp_name  = trim($_FILES["file"]["tmp_name"][$i]);

                // Convert to base64
                //$image_base64 = base64_encode(file_get_contents($_FILES['file']['tmp_name']) );
                $filePointer = fopen($tmp_name, "rb");
                $file_content = fread($filePointer, filesize(trim($_FILES["file"]["tmp_name"][$i])));
                fclose($filePointer);

                $uploadFileparams = array($eventId, $file_name, $file_type, $file_content, $userID);
                $uploadFileStmt = sqlsrv_query($conn, $uploadFileSql, $uploadFileparams);
    
                if ($uploadFileStmt === false) {
                    die(print_r(sqlsrv_errors(), true));
                }
            }
		}
		echo '<script>document.location="mhw_form_submitted.php"</script>';
    } ?>

<script src="main.js"></script>	
<script src="eventform.js"></script>	
<script src="js/repeater.js"></script>


<style type="text/css">
	.warning {display:block; margin:15px 0;}
	.warning span {color:red;}
	input[type='number'] {width:18rem; display:inline;}
	#productvalid {display:none;}

	a.errorlink:link {color:red;text-decoration:underline;}
	a.errorlink:visited {color:red;text-decoration:underline;}
	a.errorlink:hover {color:red;text-decoration:underline;}
</style>	

    <link href="css/wforms-layout_v530-14.css" rel="stylesheet" type="text/css" />
    <link href="css/theme-52661.css" rel="stylesheet" type="text/css" />
    <link href="css/wforms-jsonly_v530-14.css" rel="alternate stylesheet" title="This stylesheet activated by javascript" type="text/css" />
	<link href="css/wforms-inline.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/wforms_v530-14.js"></script>
    <script type="text/javascript" src="js/localization-en_US_v530-14.js"></script>         

</head>
<body class="default wFormWebPage">
<div class="container-fluid">
	<div id="tfaContent">
		<div class="wFormContainer" style="max-width: 85%; width:auto;" >
			<div class="row">
				<div class="col-sm-12">

    				<div class="wFormHeader"></div>
    		
					<h3 class="wFormTitle" id="4690986-T">Event Notification & Sample Request Form</h3>
					<p> Please answer the following questions regarding your event notification and/or sample request. Your responses will then be forwarded to our compliance department for review. Answering each question in its entirety will prevent delays in the compliance vetting process. </p>
					
					<form method="post" action="mhw_event_form.php" class="hintsBelow labelsAbove" id="eventform" role="form" enctype="multipart/form-data" >
						<div class="float-right"><button type="reset" value="Reset" id="clearform" class="btn btn-sm btn-outline-danger" onClick="window.location.reload()">Clear Form</button></div>					

						<div class="form-group">
							<h5>1. Select the appropriate option:</h5>

							<div class="form-check">
							  <input class="form-check-input hiddeninputs" data-div="showsrequestdate" type="radio" name="requesttype"  value="requestdistributor" required>
							  <label class="form-check-label">
							    This is a sample request for a distributor
							  </label>						  
							</div>
							<div class="form-check" id="showsrequestdate">
								Provide requested date of sample delivery: <input type="date" name="samplerequestdate" min="<?php echo date("Y-m-d"); ?>" value="">
							</div>	

							<div class="form-check">
							  <input class="form-check-input hiddeninputs" data-div="showssolicitordate" type="radio" name="requesttype"  value="solicitoraccountvisits" required>
							  <label class="form-check-label">
							  This is a sample request for a licensed solicitor to use for account visits
							  </label>						  
							</div>	
							<div class="form-check" id="showssolicitordate">
								Provide requested date of sample delivery: <input type="date" name="samplesolicitordate" min="<?php echo date("Y-m-d"); ?>" value="">
							</div>

							<div class="form-check">
							  <input class="form-check-input hiddeninputs" data-div="showrequesteventdate" type="radio" name="requesttype" value="eventandsample" required>
							  <label class="form-check-label">
								This is a SAMPLE request and EVENT notification
							  </label>
							</div>
							
							<div class="form-check" id="showrequesteventdate">
								Provide date of event: <input type="date" name="eventdate2" min="<?php echo date("Y-m-d"); ?>">
								Provide requested date of sample delivery: <input type="date" name="sampledate2" min="<?php echo date("Y-m-d"); ?>" value="">
								Event State: <select id="eventstatesampleevent" style="width: 200px;" name="eventstatesampleevent" class="form-control custom-select">
								        <option value=""></option>
										<option value="AL">Alabama</option>
										<option value="AK">Alaska</option>
										<option value="AZ">Arizona</option>
										<option value="AR">Arkansas</option>
										<option value="CA">California</option>
										<option value="CO">Colorado</option>
										<option value="CT">Connecticut</option>
										<option value="DE">Delaware</option>
										<option value="DC">District Of Columbia</option>
										<option value="FL">Florida</option>
										<option value="GA">Georgia</option>
										<option value="HI">Hawaii</option>
										<option value="ID">Idaho</option>
										<option value="IL">Illinois</option>
										<option value="IN">Indiana</option>
										<option value="IA">Iowa</option>
										<option value="KS">Kansas</option>
										<option value="KY">Kentucky</option>
										<option value="LA">Louisiana</option>
										<option value="ME">Maine</option>
										<option value="MD">Maryland</option>
										<option value="MA">Massachusetts</option>
										<option value="MI">Michigan</option>
										<option value="MN">Minnesota</option>
										<option value="MS">Mississippi</option>
										<option value="MO">Missouri</option>
										<option value="MT">Montana</option>
										<option value="NE">Nebraska</option>
										<option value="NV">Nevada</option>
										<option value="NH">New Hampshire</option>
										<option value="NJ">New Jersey</option>
										<option value="NM">New Mexico</option>
										<option value="NY">New York</option>
										<option value="NC">North Carolina</option>
										<option value="ND">North Dakota</option>
										<option value="OH">Ohio</option>
										<option value="OK">Oklahoma</option>
										<option value="OR">Oregon</option>
										<option value="PA">Pennsylvania</option>
										<option value="RI">Rhode Island</option>
										<option value="SC">South Carolina</option>
										<option value="SD">South Dakota</option>
										<option value="TN">Tennessee</option>
										<option value="TX">Texas</option>
										<option value="UT">Utah</option>
										<option value="VT">Vermont</option>
										<option value="VA">Virginia</option>
										<option value="WA">Washington</option>
										<option value="WV">West Virginia</option>
										<option value="WI">Wisconsin</option>
										<option value="WY">Wyoming</option>
									</select>
							</div>
							
						
							<div class="form-check">
							  <input class="form-check-input hiddeninputs" data-div="showeventdate" type="radio" name="requesttype"  value="eventonly" required>
							  <label class="form-check-label">
								This is an EVENT notification only
							  </label>						  
							</div>						
							<div class="form-check" id="showeventdate">
								Provide date of event: <input type="date" name="eventdate" min="<?php echo date("Y-m-d"); ?>" value="">
								Event State: <select id="eventstateeventonly" style="width: 200px;" name="eventstateeventonly" class="form-control custom-select">
								        <option value=""></option>
										<option value="AL">Alabama</option>
										<option value="AK">Alaska</option>
										<option value="AZ">Arizona</option>
										<option value="AR">Arkansas</option>
										<option value="CA">California</option>
										<option value="CO">Colorado</option>
										<option value="CT">Connecticut</option>
										<option value="DE">Delaware</option>
										<option value="DC">District Of Columbia</option>
										<option value="FL">Florida</option>
										<option value="GA">Georgia</option>
										<option value="HI">Hawaii</option>
										<option value="ID">Idaho</option>
										<option value="IL">Illinois</option>
										<option value="IN">Indiana</option>
										<option value="IA">Iowa</option>
										<option value="KS">Kansas</option>
										<option value="KY">Kentucky</option>
										<option value="LA">Louisiana</option>
										<option value="ME">Maine</option>
										<option value="MD">Maryland</option>
										<option value="MA">Massachusetts</option>
										<option value="MI">Michigan</option>
										<option value="MN">Minnesota</option>
										<option value="MS">Mississippi</option>
										<option value="MO">Missouri</option>
										<option value="MT">Montana</option>
										<option value="NE">Nebraska</option>
										<option value="NV">Nevada</option>
										<option value="NH">New Hampshire</option>
										<option value="NJ">New Jersey</option>
										<option value="NM">New Mexico</option>
										<option value="NY">New York</option>
										<option value="NC">North Carolina</option>
										<option value="ND">North Dakota</option>
										<option value="OH">Ohio</option>
										<option value="OK">Oklahoma</option>
										<option value="OR">Oregon</option>
										<option value="PA">Pennsylvania</option>
										<option value="RI">Rhode Island</option>
										<option value="SC">South Carolina</option>
										<option value="SD">South Dakota</option>
										<option value="TN">Tennessee</option>
										<option value="TX">Texas</option>
										<option value="UT">Utah</option>
										<option value="VT">Vermont</option>
										<option value="VA">Virginia</option>
										<option value="WA">Washington</option>
										<option value="WV">West Virginia</option>
										<option value="WI">Wisconsin</option>
										<option value="WY">Wyoming</option>
									</select>
							</div>
							
							<div class="form-check">
							  <input class="form-check-input hiddeninputs" type="radio" data-div="other" name="requesttype" value="other" required>
							  <label class="form-check-label">
								Other
							  </label>
							</div>

							<div class="form-check" id="other">
								<textarea class="form-control" name="othertxt" maxlength="150"></textarea>
							</div>

							<div class="warning pb-2"><span>Important Note:</span> <u>Less than 20 days' notice for events in CA, NJ, & NY are subject to a rushed vetting fee of $150.00 per request. Less than 10 days notice is subject to rejection.</u>	
							</div>												
						</div>
						
						<div class="form-group pb-3">
							<h5>2. QUANTITY: How many bottles or cases of samples are you requesting? (List quantity per item)</h5>
							<div id="alerts" class="alert alert-warning collapse" role="alert">
								<button type="button" class="close" data-toggle="collapse" href="#my-alert-box">
									<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
								</button>
								<p>This is for informational purposes only and a sample order will not be processed</p>
							</div>

							<div class="repeattest">
								<div class="oneField field-container-D" id="tfa_product_desc-D">
									<label id="tfa_product_desc-L" class="label preField reqMark" for="tfa_product_desc">Item Description</label><br>
									<div class="inputWrapper"><input type="text" id="tfa_product_desc" name="tfa_product_desc" value="<?php echo $pf_product_desc; ?>" aria-required="false" title="Product Name" data-dataset-allow-free-responses="" class="tfa_product_desc"><small id="productvalid" class="warning"><span>No product match found, you can <a class="errorlink" href="productsetup.php">add a new one</a>  </span></small></div>
									
								</div>													
							</div>
													
							<div id="repeater">
							  <!-- Repeater Heading -->
							  <div class="repeater-heading">
								  <button class="btn btn-primary repeater-add-btn" style="margin:10px 0;" id="AddNewProduct" disabled>
									  Add New Item
								  </button>
							  </div>
							  
							  <!-- Repeater Items -->
							  <div class="items" data-group="test">
								<!-- Repeater Items Here -->		
								
								<div class="oneField field-container-D" >
								    <div class="row">
										<div class="col-3">
											<label class="label preField reqMark">Product Name</label>
										</div>
										<div class="col-3">
											<label class="label preField reqMark">Bottle Quantity</label>
										</div>
										<div class="col-3">
											<label class="label preField reqMark">Case Quantity</label>
										</div>
									</div>
									<div class="row" id="products">
									    <div class="col-3">
											<input type="text" style="width: 20em" name="product[]" value="" aria-required="true" title="Product Name" data-dataset-allow-free-responses="" data-skip-name="true" class="tfa_product_desc" readonly >
										</div>
										<div class="col-3">
										     <input type="number" class="form-control" style="width: 20em" data-skip-name="true" id="bottlequan" name="bottlequan[]" placeholder="Bottle Quantity" min=1 required>	
										</div>
										<div class="col-3">
										 <input type="number" class="form-control" style="width: 20em" data-skip-name="true" id="casequan" name="casequan[]" placeholder="Case Quantity" min=1>	
									</div>
								</div>							
								
								<!-- Repeater Remove Btn -->
								<div class="repeater-remove-btn">
									  <button id="remove-btn" class="btn btn-danger" style="margin:10px 0;" onclick="$(this).parents('.items').remove()">
										  Remove
									  </button>
								</div>		
							  </div>															
							</div>			</div>					  	
								  
						 
						  <div id="eventDetails" class="form-group collapse pt-5">
								<h5><span id="section3">3.</span> PROVIDE EVENT DETAILS:</h5>
								<div class="form-group">						  
									<label>Describe the Event:(Common events: Consumer tasting, tradeshow, sponsorship, charitable donation, concert/festival/fairs, media, parties)</label>							
									<textarea class="form-control" name="eventdesc" placeholder="Event Description" maxlength="250"></textarea>							
								</div>
					
								<div class="form-group">
									<label> Name of Event:</label>
									<input type="text" class="form-control" name="eventname" placeholder="Event Name" maxlength="250">
								</div>
					
								<div class="form-group">
									<label> Is the event Public or Invitation Only:</label>
										<select class="custom-select" name="eventtype" required>								  
										<option value="1">Public</option>
										<option value="2">Invitation Only</option>								  
										</select>
								</div>
					
								<div class="form-group">
									<label>Address</label>
									<input type="text" class="form-control" name="address" placeholder="1234 Main St" maxlength="150">
								</div>
								<div class="form-group">
									<label>Address 2</label>
									<input type="text" class="form-control" name="address2" placeholder="Apartment, studio, or floor" maxlength="150">
								</div>
								<div class="form-row">
									<div class="form-group col-md-5">
										<label>City</label>
										<input type="text" class="form-control" name="city" maxlength="50">
									</div>
									<div class="form-group col-md-3">
										<label>State</label>
										<input type="text" class="form-control" name="state" maxlength="50">
									</div>
										<div class="form-group col-md-2">
										<label>Zip</label>
										<input type="text" class="form-control" name="zip" maxlength="20">
									</div>
									<div class="form-group col-md-2">
										<label>Country</label>
										<input type="text" class="form-control" name="country" maxlength="100">
									</div>
								</div>

								<div id="docsDiv">
									<div class="row col-lg-8 form-group" id="1">
										<label class="btn btn-default">
											<i class="fa fa-plus" aria-hidden="true"></i> Attachment
											<input id="docs" name="file[]" class="attachmentButton" data-doc-row="1" type="file" style="display: none" accept="image/*,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation" />
										</label>
									</div>
								</div>
								<div class="row pb-3">
									<div class="col-10">
										<div class="panel panel-default">
											<div class="panel-body pl-4" id="docsListing">

											</div>
										</div>
									</div>
								</div>

							</div>
						
							<div class="form-group pt-3">
								    <h5><span id="section4">3. </span> SAMPLE DELIVERY/PICK-UP PREFERENCE: Select the appropriate option: </h5>
									<div class="form-check">
										<input class="form-check-input hiddeninputs" type="radio" data-div="showdelivery" id="deliverytype" name="deliverytype" value="delivery" required>
										<label class="form-check-label">
											Delivery
										</label>
									</div>

									<div class="form-check" id="showdelivery">
										<div class="form-check">
											<input class="form-check-input hiddeninputs" type="radio" data-div="showdistributor" name="delivery" value="distributor">
											<label class="form-check-label">
											Licensed distributor (Provide name and address):
											</label>
										</div>

										<div id="showdistributor" class="form-check" >
											<textarea class="form-control" name="pickupdisttxt" maxlength="250"></textarea>
										</div>

										<div id="specificAddressSection" class="form-check">
											<input class="form-check-input hiddeninputs" type="radio" data-div="showSpecificAddressFieldSection" name="delivery" value="specificAddress">
											<label class="form-check-label">
												Specific Address
											</label>
										</div>
 
										<div id="showSpecificAddressFieldSection" class="form-check collapse">
										    <div class="form-group">
												<label>Address</label>
												<input type="text" class="form-control" name="deliveryaddress" placeholder="1234 Main St" maxlength="150">
											</div>
											<div class="form-group">
												<label>Address 2</label>
												<input type="text" class="form-control" name="deliveryaddress2" placeholder="Apartment, studio, or floor" maxlength="150">
											</div>
											<div class="form-row">
												<div class="form-group col-md-5">
													<label>City</label>
													<input type="text" class="form-control" name="deliverycity" maxlength="50">
												</div>
												<div class="form-group col-md-3">
													<label>State</label>
													<input type="text" class="form-control" name="deliverystate" maxlength="50">
												</div>
												<div class="form-group col-md-2">
													<label>Zip</label>
													<input type="text" class="form-control" name="deliveryzip" maxlength="20">
												</div>
												<div class="form-group col-md-2">
													<label>Country</label>
													<input type="text" class="form-control" name="deliverycountry" maxlength="100">
												</div>
											</div>
											</div>
								       </div>


										<div id="sameAddressSection" class="form-check collapse">
											<input class="form-check-input" type="radio"  name="delivery" value="sameAddress">
											<label class="form-check-label">
												Same address of event listed in #3
											</label>
										</div>

										<div class="form-check">
											<input class="form-check-input hiddeninputs" type="radio"  data-div="showpickup" id="deliverytype" name="deliverytype" value="pickup" required>
											<label class="form-check-label">
												Pickup
											</label>
										</div>
									</div>							

									<div class="form-check" id="showpickup">
										<div class="form-check">
											<input class="form-check-input hiddeninputs" type="radio" name="pickuptype" value="westernwine">
											<label class="form-check-label">
												Western Wine Services, CA
											</label>
										</div>
										
										<div class="form-check">
											<input class="form-check-input hiddeninputs" data-div="pickupwtxt" type="radio" name="pickuptype" value="westerncarriers">
											<label class="form-check-label">
											   Western Carriers, NJ
											</label>	
										</div>

										<div id="pickupwtxt" class="form-check-label">
											<small class="warning"><span> Pick-up at Western Carriers, NJ requires: <br />
											(1) NJ solicitor's permit or the equivalent for out-of-state companies.  <br />
											(2) NJ Transit Insignia for your  vehicle or a temporary permit for your rental vehicle. </span></small>
										</div>

										<div class="form-check">
											<input class="form-check-input hiddeninputs" type="radio" name="pickuptype" value="pwd">
											<label class="form-check-label">
											    PWD
											</label>	
										</div>

										<div class="form-check">
											<input class="form-check-input hiddeninputs" type="radio" name="pickuptype" value="greystone">
											<label class="form-check-label">
											   Greystone
											</label>	
										</div>

										<div class="form-check">
											<input class="form-check-input hiddeninputs" data-div="pickupothertxt" type="radio" name="pickuptype" value="other">
											<label class="form-check-label">
												Other - explain below (Retail sample delivery is prohibited) :
											</label>
										</div>
										<div id="pickupothertxt">
											<textarea class="form-control" name="pickupothertxt" maxlength="250"></textarea>
										</div>	
									</div>
						     </div>
						
						
						<div id="whosection" class="form-group collapse pb-3">
							<h5><span id="section5">6.</span> WHO is using the samples or participating in the event? Select the appropriate option: </h5>
							
							 <div class="form-check">
							  <input class="form-check-input hiddeninputs" data-div="sampleusermhw" type="radio" name="sampleuser" value="MHW">
							  <label class="form-check-label">
							  MHW licensed solicitor/rep (provide name and license number):
							  </label>
							</div>
							
							<div id="sampleusermhw">
								<textarea class="form-control" name="sampleusermhw" maxlength="150"></textarea>
							</div>
							
							<div class="form-check">
							  <input class="form-check-input hiddeninputs" data-div="sampleuserbroker" type="radio" name="sampleuser" value="broker">
							  <label class="form-check-label">
								Broker, PR/Marketing Firm (provide name and licenses/permits, if any):
							  </label>
							</div>
							
							<div id="sampleuserbroker">
								<textarea class="form-control" name="sampleuserbroker" maxlength="150"></textarea>
							</div>
							
							<div class="form-check">
							  <input class="form-check-input hiddeninputs" data-div="sampleuserrep" type="radio" name="sampleuser" value="rep">
							  <label class="form-check-label">
								An authorized representative (provide name and licenses/permits, if any):
							  </label>
							</div>
							
							<div id="sampleuserrep">
								<textarea class="form-control" name="sampleuserrep" maxlength="150"></textarea>
							</div>							
						</div>	
					
					<div class="actions" id="4690986-A">
					<input type="submit" data-label="Submit" class="primaryAction btn btn-primary" id="submit_button" name="submit_button" value="Submit"></div>
					<div style="clear:both"></div>
					
					<input type="hidden" name="prodID" id="prodID" value="<?php echo $prodID; ?>">
					<input type="hidden" name="product_mhw_code" id="product_mhw_code" value="<?php echo $product_mhw_code; ?>">
					<input type="hidden" name="product_mhw_code_search" id="product_mhw_code_search" value="<?php echo $product_mhw_code_search; ?>">
					<input type="hidden" name="userID" id="userID" value="<?php echo $_SESSION['mhwltdphp_user']; ?>">

					</form>
				</div>
			</div>
		</div>    
	</div>

	<script src='js/iframe_resize_helper_internal.js'></script>
</div>


<!-- Modal -->
<div class="modal fade" id="eventTypeWarningMesg" tabindex="-1" role="dialog" aria-labelledby="eventTypeWarningLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="eventTypeWarningLabel">Warning</h5>
      </div>
      <div id="ModalText" class="modal-body"> You are required to select 'This is a sample request for a distributor' option.  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="warningMesg" tabindex="-1" role="dialog" aria-labelledby="warningLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="warningLabel">Warning</h5>
      </div>
      <div id="ModalText" class="modal-body"> </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="okbutton">Ok</button>
		<button type="button" class="btn btn-primary" id="cancelButton" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="okMesg" tabindex="-1" role="dialog" aria-labelledby="warningLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="warningLabel"></h5>
      </div>
      <div id="ModalText" class="modal-body"> </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="okbutton" data-dismiss="modal">Ok</button>
      </div>
    </div>
  </div>
</div>

<div id="attachmentTemplate" class="d-none">
    <div class="row">
        <div class="col-lg-5">
            <div class="form-group">
                <label class="btn btn-default">
                    <input id="docs" name="file[]" class="attachmentButton" type="file" style="display:none" accept="image/*,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation" />
                    <i class="fa fa-plus" aria-hidden="true"></i> Attachment
                </label>
            </div>
        </div>
    </div>
</div>

</body>
</html>
<?php
}
?>