<?php
session_start();
include("prepend.php");
include("settings.php");

if(!isset($_SESSION['mhwltdphp_user'])){

	header("Location: login.php");
}
else{
	include('head.php');
	include('dbconnect.php');

	if(isset($_POST['prodID'])){

		$fob_point = $_POST['fob_point'];

		$door_port = $_POST['door_port'];
		$final_dest = $_POST['final_dest'];
		$ponotes = $_POST['po_notes'];

		$allcontainer = $_POST['allcontainer'];
		$emailsupplier = $_POST['emailsupplier'];
		$emailpo = $_POST['emailpo'];

		$supplier_name = $_POST['supplier_name'];

		$supplier_contact = $_POST['supplier_contact'];
		$supplier_fda = $_POST['supplier_fda'];
		$supplier_tax_red = $_POST['supplier_tax_red'];
		$supplier_address = $_POST['supplier_address'];

		$supplier_address2 = $_POST['supplier_address2'];
		$supplier_address3 = $_POST['supplier_address3'];
		$supplier_city = $_POST['supplier_city'];
		$supplier_state = $_POST['supplier_state'];

		$supplier_country = $_POST['supplier_country'];
		$supplier_zip = $_POST['supplier_zip'];
		$supplier_phone = $_POST['supplier_phone'];
		$supplier_email = $_POST['supplier_email'];

		$userID = $_POST['userID'];


		//Establishes the connection
		$conn = sqlsrv_connect($serverName, $connectionOptions);

		if( $conn === false) {
			//echo "Connection could not be established.<br />";
			die( print_r( sqlsrv_errors(), true));
		} 

		$mainsql = "INSERT INTO mhw_app_po_form (fob_point, door_port, final_dest, ponotes, allcontainer,
												 emailsupplier, emailpo, supplier_name, supplier_contact, supplier_fda, 
												 supplier_tax_red, supplier_address, supplier_address2, supplier_address3, 
												 supplier_city, supplier_state, supplier_country, supplier_zip, 
												 supplier_phone, supplier_email, created_by) 
					VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?); SELECT SCOPE_IDENTITY() AS IDENTITY_COLUMN_NAME";
		
		// prepare and bind
		$mainparams = array($fob_point, $door_port, $final_dest, $ponotes, $allcontainer, 
							$emailsupplier, $emailpo, $supplier_name, $supplier_contact, $supplier_fda, $supplier_tax_red, $supplier_address, $supplier_address2, $supplier_address3, 
							$supplier_city, $supplier_state, $supplier_country, $supplier_zip, $supplier_phone, $supplier_email, $userID);		
		$stmt = sqlsrv_query( $conn, $mainsql, $mainparams);

		if( $stmt === false ) {
			die( print_r( sqlsrv_errors(), true));
		}

		sqlsrv_next_result($stmt);  //note this line!!
		sqlsrv_fetch($stmt); 	
		$poId = sqlsrv_get_field($stmt, 0);



		//****** PO Product ***********
		$poproductsql = "INSERT INTO mhw_po_product (po_id, product_name, product_quantity, product_price, created_by) 
		VALUES (?, ?, ?, ?, ?)";

		$productarray = $_POST['product'];
		$prodquanarray = $_POST['prodquan'];
		$prodpricearray = $_POST['prodprice'];

		for ($x = 0; $x < sizeof($productarray); $x++) {
			$prod = $productarray[$x];
			$quan = $prodquanarray[$x]; 
			$price = $prodpricearray[$x];

			$poproductparams = array($poId, $prod, $quan, $price, $userID);		
			$poproductstmt = sqlsrv_query( $conn, $poproductsql, $poproductparams);

			if( $poproductstmt === false ) {
				die( print_r( sqlsrv_errors(), true));
			}
		}

		//echo "<pre>";
		//var_dump($_REQUEST); 
		//echo "</pre>";
	}

?>

<script src="poform.js"></script>	
<script src="js/repeater.js"></script>
   


<style type="text/css">
.warning {display:block; margin:15px 0;}
.warning span {color:red;}
input[type='number'] {width:18rem; display:inline;}
#productvalid {display:none;}
#nsupplier_fields {display:none;}
label span {color:red;}
.items{clear:both;}
.repeater-remove-btn {width:100%;}
</style>	

    <link href="css/wforms-layout_v530-14.css" rel="stylesheet" type="text/css" />
    <link href="css/theme-52661.css" rel="stylesheet" type="text/css" />
    <link href="css/wforms-jsonly_v530-14.css" rel="alternate stylesheet" title="This stylesheet activated by javascript" type="text/css" />
	<link href="css/wforms-inline.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/wforms_v530-14.js"></script>
    <script type="text/javascript" src="js/localization-en_US_v530-14.js"></script>         
    
</head>
<body class="default wFormWebPage">
<div class="container-fluid">
	<div id="tfaContent">
		<div class="wFormContainer" style="max-width: 85%; width:auto;" >
			<div class="row">
				<div class="col-sm-12">

    				<div class="wFormHeader"></div>
    		
					<h3 class="wFormTitle" id="4690986-T">PO Request Form</h3>
					<p> Please answer the following questions regarding your PO request. </p>
					
					<form method="post" action="mhw_po_form.php" class="hintsBelow labelsAbove" id="4690986" role="form" enctype="multipart/form-data">
						<div class="float-right"><button type="reset" value="Reset" id="clearform" class="btn btn-sm btn-outline-danger" onClick="window.location.reload()">Clear Form</button></div>	

						<div class="form-group" style="clear:both;">
							<div class="form-row">
								<div class="form-group col-md-4">
									<label>FOB Point</label>
									<input type="text" class="form-control" name="fob_point" placeholder="FOB Point" maxlength="150" required> 
								</div>
								<div class="form-group col-md-4">
									  <label>Door/Port</label>
									<input type="text" class="form-control" name="door_port" placeholder="Door/Port" maxlength="150" required>
								</div>
								<div class="form-group col-md-4">
									<label>Final Destination</label>
									<input type="text" class="form-control" name="final_dest" placeholder="Final Destination" maxlength="150" required>
								</div>
							</div>
							<div class="form-group">
								<label> Notes </label>
								<textarea class="form-control" name="po_notes" placeholder="Notes" maxlength="250" required></textarea>
							</div>
							
							<div class="form-check">
								<input type="checkbox" class="form-check-input" name="allcontainer" value="Request All On One Container" /> 
								<label class="form-check-label">Request All On One Container </label>
							</div>
							<div class="form-check">
								<input type="checkbox" class="form-check-input hiddeninputs" name="emailsupplier" value="Request MHW to email supplier PO" data-div="sup-email" maxlength="50" /> 
								<label class="form-check-label">Request MHW to email supplier PO</label>
								<div id="sup-email"> <input type="email" class="form-control" name="emailpo" placeholder="Supplier Email Address" maxlength="250"/> </div>
							</div>
						</div>
						
						<div class="form-group">
						
						  <h5>Supplier Info:</h5>
						  
						  <div class="form-row">
							<div class="form-group col-md-3">
							  <label>Supplier Name <span>*</span></label>
							  <select id="supplier_name" name="supplier_name" class="form-control custom-select">
								<option> Default </option>
								<option value="new"> New Supplier</option>
							  </select>
							</div>	
						  </div>
						  <div id="nsupplier_fields">
							  <div class="form-row">
									<div class="form-group col-md-3">
									  <label>Supplier Contact Person <span>*</span></label>
									  <input type="text" class="form-control supplierreq" name="supplier_contact" maxlength="150" required>
									</div>
									<div class="form-group col-md-3">
									  <label>Supplier FDA Registration Number <span>*</span></label>
									  <input type="text" class="form-control supplierreq" name="supplier_fda" maxlength="100" required>
									</div>
									<div class="form-group col-md-3">
									  <label>Tax Reduction Allocation</label>
									  <input type="text" class="form-control" name="supplier_tax_red" maxlength="150">
									</div>			
								  <div class="form-group col-md-3">
									<label>Supplier Address Line 1 <span>*</span></label>
									<input type="text" class="form-control supplierreq" name="supplier_address" maxlength="100" required> 
								  </div>								
							  </div>
							 <div class="form-row">
								  <div class="form-group col-md-3">
									<label>Supplier Address Line 2</label>
									<input type="text" class="form-control" name="supplier_address2" maxlength="100">
								  </div>
								  <div class="form-group col-md-3">
									<label>Supplier Address Line 3</label>
									<input type="text" class="form-control" name="supplier_address3" maxlength="100">
								  </div>							  
									<div class="form-group col-md-3">
									  <label>Supplier City <span>*</span></label>
									  <input type="text" class="form-control supplierreq" name="supplier_city" maxlength="50" required>
									</div>
									<div class="form-group col-md-3">
									  <label>Supplier State</label>
									  <input type="text" class="form-control" name="supplier_state" maxlength="50" required> 
									</div>								
								</div>
								<div class="form-row">
									<div class="form-group col-md-3">
									  <label>Supplier Country <span>*</span></label>
									  <input type="text" class="form-control supplierreq" name="supplier_country" maxlength="50" required> 
									</div>								
									<div class="form-group col-md-3">
									  <label>Supplier Zip Code</label>
									  <input type="text" class="form-control" name="supplier_zip" maxlength="50">
									</div>
									<div class="form-group col-md-3">
									  <label>Supplier Phone <span>*</span></label>
									  <input type="phone" class="form-control supplierreq" name="supplier_phone" maxlength="25" required>
									</div>				
									<div class="form-group col-md-3">
									  <label>Supplier Email <span>*</span></label>
									  <input type="email" class="form-control supplierreq" name="supplier_email" maxlength="150" required>
									</div>									
							  </div>	
							</div>
						</div>
						
						<div class="form-group">
							<h5>Items: </h5>
							
						<div class="repeattest">
							<div class="oneField field-container-D" id="tfa_product_desc-D">
								<label id="tfa_product_desc-L" class="label preField reqMark" for="tfa_product_desc">Item Name</label><br>
								<div class="inputWrapper"><input type="text" id="tfa_product_desc" name="tfa_product_desc" value="<?php echo $pf_product_desc; ?>" title="Product Name" data-dataset-allow-free-responses="" class="tfa_product_desc"><small id="productvalid" class="warning"><span>No product match found. </span></small></div>
								
							</div>													
						</div>
							
							
							<div id="repeater">
							  <!-- Repeater Heading -->
							  <div class="repeater-heading">
								  <button class="btn btn-primary repeater-add-btn" style="margin:10px 0;">
									  Add New Item
								  </button>
							  </div>
							  
							  <!-- Repeater Items -->
							  <div class="items" data-group="test">
								<!-- Repeater Items Here -->		
								
								<div class="oneField field-container-D" >
									<label class="label preField reqMark">Item Name</label><br>
									<div class="inputWrapper">
										<input type="text" name="product[]" value="" aria-required="true" title="Item Name" data-dataset-allow-free-responses="" data-skip-name="true" class="tfa_product_desc" readonly required>
										<input type="number" data-skip-name="true" name="prodquan[]" placeholder="Quantity" min=1 value="1" maxlength="10" required>	
										<input type="text" name="prodprice[]" placeholder="FOB Price" data-dataset-allow-free-responses="" data-skip-name="true" value=""  maxlength="10" required>
									</div>
								</div>							
								
								<!-- Repeater Remove Btn -->
								<div class="repeater-remove-btn">
									  <button id="remove-btn" class="btn btn-danger" style="margin:10px 0;" onclick="$(this).parents('.items').remove()">
										  Remove
									  </button>
								</div>		
							  </div>															
							</div>								  	
						  </div>							  						
					
					<div class="actions" id="4690986-A"><input type="submit" data-label="Submit" class="primaryAction btn btn-primary" id="submit_button" value="Submit"></div>
					<div style="clear:both"></div>
					
					<input type="hidden" name="prodID" id="prodID" value="<?php echo $prodID; ?>">
					<input type="hidden" name="product_mhw_code" id="product_mhw_code" value="<?php echo $product_mhw_code; ?>">
					<input type="hidden" name="product_mhw_code_search" id="product_mhw_code_search" value="<?php echo $product_mhw_code_search; ?>">
					<input type="hidden" name="userID" id="userID" value="<?php echo $_SESSION['mhwltdphp_user']; ?>">

					</form>
				</div>
			</div>
		</div>    
	</div>

	<script src='js/iframe_resize_helper_internal.js'></script>
</div>
</body>
</html>
<?php
}
?>