<?php
session_start();
include("prepend.php");
include("settings.php");
include('head.php');
?>

<script src="main.js"></script>	
<script src="eventform.js"></script>	
<script src="js/repeater.js"></script>


<style type="text/css">
	.warning {display:block; margin:15px 0;}
	.warning span {color:red;}
	input[type='number'] {width:18rem; display:inline;}
	#productvalid {display:none;}

	a.errorlink:link {color:red;text-decoration:underline;}
	a.errorlink:visited {color:red;text-decoration:underline;}
	a.errorlink:hover {color:red;text-decoration:underline;}
</style>	

    <link href="css/wforms-layout_v530-14.css" rel="stylesheet" type="text/css" />
    <link href="css/theme-52661.css" rel="stylesheet" type="text/css" />
    <link href="css/wforms-jsonly_v530-14.css" rel="alternate stylesheet" title="This stylesheet activated by javascript" type="text/css" />
	<link href="css/wforms-inline.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/wforms_v530-14.js"></script>
    <script type="text/javascript" src="js/localization-en_US_v530-14.js"></script>         
    
</head>
<body class="default wFormWebPage">
<div class="container-fluid">
	<div id="tfaContent">
		<div class="wFormContainer" style="max-width: 85%; width:auto;" >
			<div class="row">
				<div class="col-sm-12">

    				<div class="wFormHeader"></div>
    		
					<h3 class="wFormTitle" id="4690986-T">Event Notification & Sample Request Form</h3>
					<h4 class="wFormTitle" id="4690986-T"><p> Your form was successfully submitted! </p></h4>
				</div>
			</div>
		</div>    
	</div>
</div>
</body>
</html>
