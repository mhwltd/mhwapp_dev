<?php
//=============settings============
include("settings.php");
//=============settings============

//=============functions============
include("functions.php");
//=============functions============
?>

<!--Main layout-->
<main>
<div class="container-fluid">
<!--<span id="processor_include">PROCESSOR LISTENING...</span>-->

<?php
	//echo "<pre>";
	//var_dump($_REQUEST); 
	//echo "</post>";
	//echo "<pre>";
	//var_dump($_POST); exit;

    include("dbconnect.php");
   
    //Establishes the connection
    $conn = sqlsrv_connect($serverName, $connectionOptions);
	if( $conn === false) {
    	die( print_r( sqlsrv_errors(), true));
	}

	$errorlog="\n";
	$reqlog="\n";
	$errorFree=1;

	$client_code = $_POST['tfa_client_code'];
	$client_name = str_replace("'","''",$_POST['tfa_client_name']);
	$brand_name = str_replace("'","''",$_POST['tfa_brand_name']);
	$product_desc = $_POST['tfa_product_desc'];
	$product_mhw_code = $_POST['product_mhw_code'];
	$product_mhw_code_search = $_POST['product_mhw_code_search'];
	$product_id = intval($_POST['prodID']);
	$user_id = $_POST['userID'];
	$blr_id = intval($_POST['blrID']);

	$confirmationNumber = hash_gen();

	$blr_reg_sub_type='';
	if($_POST['NewCOLA1_sub']!=''){ $blr_reg_sub_type = $_POST['NewCOLA1_sub']; }
	if($_POST['NewCOLA2_sub']!=''){ $blr_reg_sub_type = $_POST['NewCOLA2_sub']; }

	/* ============BEGIN BLR HEAD ======================== */ 
	//blr insert stored procedure

	$tsql= "EXEC [dbo].[usp_blr_hdr] 
	@blr_id = ?
	,@client_code = ?
	,@client_name = ?
	,@brand_name = ?
	,@product_id = ?
	,@product_desc = ?
	,@blr_contact = ?
	,@blr_email = ?
	,@blr_phone = ?
	,@blr_reg_type = ?
	,@blr_reg_sub_type  = ?
	,@blr_confirmation = ?
	,@blr_state_count = ?
	,@blr_state_count_SF = ?
	,@create_via  = ?
	,@username  = ?";

	$blrID = 0;
	$prodID = $product_id;
	
	//prep variables for re-filling the fields for resubmitting
	$pf_client_code = $client_code;
	$pf_client_name = $client_name;
	$pf_brand_name = $brand_name;
	$pf_product_desc = $product_desc;
	
	$stmt = sqlsrv_query($conn, $tsql, array ( 
		$blr_id,
		$client_code,
		$client_name,
		$brand_name,
		$product_id,
		$product_desc,
		$_POST['tfa_contact'],
		$_POST['tfa_email'],
		$_POST['tfa_phone'],
		$_POST['blr_registration_type'],
		$blr_reg_sub_type,
		$confirmationNumber,
		$_POST['blr_state_count'],
		$_POST['blr_state_count_SF'],
		'blr',
		$user_id
	));
	
	while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
		$prodID = $row['prodID']; 
		$blrID = $row['blrID']; 
	}

	if( $stmt === false ) {
		if( ($errors = sqlsrv_errors() ) != null) {
			//log sql errors for writing to server later
			foreach( $errors as $error ) {
				$errorlog.= $tsql."\n";
				$errorlog.= "SQLSTATE: ".$error[ 'SQLSTATE']."\n";
				$errorlog.= "code: ".$error[ 'code']."\n";
				$errorlog.= "message: ".$error[ 'message']."\n";
			}
		}
		$errorFree=0;
	}
  
	//release sql statement
	sqlsrv_free_stmt($stmt);
	/* ============END BLR HEAD ======================== */ 
	
	if($blrID>0){
		//query client code - needed for tray call
		if(!$client_code || $client_code==''){
			$tsqlcc= "EXEC [dbo].[usp_qry_client_code] @client_name = ?";
			$stmtcc = sqlsrv_query($conn, $tsqlcc, [$client_name]);
		
			while ($rowcc = sqlsrv_fetch_array($stmtcc, SQLSRV_FETCH_ASSOC)) {
				$client_code = $rowcc['clientCode']; 
			}
			$pf_client_code = $client_code;
		}

		/* ============BEGIN STATES ======================== */ 
		$ii=0;
		//$blr_states[] = $_POST['blr_states'];
		$staterows=intval(count($_POST['blr_states']));
		if($staterows<1){ $staterows=0; }

//echo "staterows:".$staterows;
				
		while ($ii<=$staterows){

//echo $ii;

			$tsql_state = '';
			$distributor_name = '';
			$distributor_contact = '';
			$distributor_phone = '';
			$distributor_email = '';
			$distributor_address = '';
			$state_name = '';
			$state_note = '';
			$distributor_key = 'null';

			$state_code = $_POST['blr_states'][$ii];

//echo $state_code;

			if($state_code!=''){
				$state_name = $_POST['state_name_'.$state_code];

//echo $state_name;

				$state_note = $_POST['state_note_'.$state_code];


				/*
				$distributor_key = $_POST['distributor_select_'.$state_code];
				
				if ($distributor_key=='0') {
					$distributor_name = $_POST['distributor_name_'.$state_code];
					$distributor_contact = $_POST['distributor_contact_'.$state_code];
					$distributor_phone = $_POST['distributor_phone_'.$state_code];
					$distributor_email = $_POST['distributor_email_'.$state_code];
					$distributor_address = $_POST['distributor_address_'.$state_code];
				} else if (empty($distributor_key)) {
					$distributor_key = NULL;
				}
				*/


				$tsql_state = "EXEC [dbo].[usp_blr_dtl]
				@blr_id = ?
				,@blr_state_name = ?
				,@blr_state_abbrev = ?
				,@blr_state_note = ?
				,@blr_distributor_key = ?
				,@blr_distributor_name = ?
				,@blr_distributor_contact = ?
				,@blr_distributor_phone = ?
				,@blr_distributor_email = ?
				,@blr_distributor_address = ?
				,@create_via = ?
				,@username = ?";
				
				$current_date = date("Y-m-d H:i:s");

				$tsql_distributors = "
					INSERT INTO [dbo].[mhw_app_blr_state_dist] (
						[blr_state_id], [blr_distributor_key], [create_date], [edit_date], [active], [deleted]
					) VALUES (
						?, ?, '$current_date', '$current_date', 1, 0
					)
				";

				$stateID = 0;
				
				$stmt_state = sqlsrv_query($conn, $tsql_state, array(
					$blrID,
					$state_name,
					$state_code,
					$state_note,
					$distributor_key,
					$distributor_name,
					$distributor_contact,
					$distributor_phone,
					$distributor_email,
					$distributor_address,
					'blr',
					$user_id
				));
			
				while ($row_state = sqlsrv_fetch_array($stmt_state, SQLSRV_FETCH_ASSOC)) {
					$stateID = $row_state['blr_stateID']; 

					
					if ($stateID) {
						$distributor_keys = $_POST['distributor_select_'.$state_code];
						foreach ($distributor_keys as $key) {
							$stmt_distributors = sqlsrv_prepare( $conn, $tsql_distributors, array($stateID, $key));  

							$fp1 = fopen('../logs/sqllog.txt', 'a');
							$sqllog="\n\r".date("Ymd H:i:s")."\t".print_r( sqlsrv_errors(), true)."\n\r";
							if( sqlsrv_execute($stmt_distributors) === false )  
							{  
								$sqllog="\n\r".date("Ymd H:i:s")."\t".print_r( sqlsrv_errors(), true)."\n\r";
								fwrite($fp1, $sqlog);
								print_r( sqlsrv_errors());
							}
							fclose($fp1);
						}
				}
				}



//CALL TRAY WORKFLOW
				$fields = array();
				$fields = array(
					'clientcode' => urlencode($client_code),
					'clientname' => urlencode($client_name),
					'product' => urlencode($product_desc),
					'state' => urlencode($state_code)
				);
				$fields_string ="";
				foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
				rtrim($fields_string, '&');

				// create curl resource 
				$ch = curl_init(); 
				// set url 
				curl_setopt($ch, CURLOPT_URL, $trayBLRWorkflow); 
				curl_setopt($ch,CURLOPT_POST, count($fields));
				curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
				//return the transfer as a string 
				//curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
				// $output contains the output string 
				$output = curl_exec($ch); 
				// close curl resource to free up system resources 
				curl_close($ch);      

//echo  "stateID:".$stateID;
				
				if( $stmt_state === false ) {
					if( ($errors = sqlsrv_errors() ) != null) {
						//log sql errors for writing to server later
						foreach( $errors as $error ) {
							$errorlog.= $tsql_state."\n";
							$errorlog.= "SQLSTATE: ".$error[ 'SQLSTATE']."\n";
							$errorlog.= "code: ".$error[ 'code']."\n";
							$errorlog.= "message: ".$error[ 'message']."\n";
						}
					}
					$STerrorFree=0;
				} //end error handling
			  
				//release sql statement
				sqlsrv_free_stmt($stmt_state);
			}
			
			//increment state counter
			$ii++;
		} //end state loop
		/* ============END STATES ======================== */ 
		
	} //end blrID dependent 

	sqlsrv_close($conn);  

	if($errorFree==1){
		echo "<div class=\"card\"><div class=\"card-body\">";
		echo "<h5 class=\"card-title\">Thank you</h5>";
		echo "<p class=\"card-text\">Your Brand Label Registration Form has been submitted. <br />Your confirmation number is: <b>".$confirmationNumber."</b></p>";
		echo "</div></div>";
		$fp1 = fopen('../logs/blrlog.txt', 'a');
		$reqlog="\n\r\n\r".date("Ymd H:i:s")."\t".$reqlog."\n\r";
		fwrite($fp1, $reqlog);
		fclose($fp1);
	}
	else{
		$errorstamp = date("Ymd H:i:s");
		echo "<div class=\"row\"><div class=\"col-sm-8\"><div class=\"card\"><div class=\"card-body\"><h5 class=\"card-title\">Error</h5><p class=\"card-text\">There was an error saving your Brand Label Registration Form. ".$pageerr." Additional error details have been logged. (ERROR-".$errorstamp.")</p></div></div></div>";
		$fp2 = fopen('../logs/blr3rr0rlog.txt', 'a');
		$errorlog="\n\r\n\r".$errorstamp."\t".$errorlog."\n\r";
		fwrite($fp2, $errorlog);
		fclose($fp2);
	}
?>
</div>
</main>
<!--Main layout-->

</html>