<?php
session_start();
include("prepend.php");
include("settings.php");

if(!isset($_SESSION['mhwltdphp_user'])){

	header("Location: login.php");
}
else{
	include('head.php');
	include('dbconnect.php');

	if(isset($_POST['certificaton'])) {

		//Establishes the connection
		$conn = sqlsrv_connect($serverName, $connectionOptions);

		if( $conn === false) {
			//echo "Connection could not be established.<br />";
			die( print_r( sqlsrv_errors(), true));
		} 

		$userID = $_SESSION['mhwltdphp_user'];

		$mainsql = "INSERT INTO mhw_po_request (
				is_mhw_arranging_freight, 
				container_type,
				number_of_pallets,
				total_weight,
				container_temperature,
				[statement],
				freight_forwarder_name,
				eta_to_port,
				booked_to,
				tax_payment_status,
				final_destination,
				client_reference_number,
				create_user,
				create_date,
				deleted,
				processed
			) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?); 
			SELECT SCOPE_IDENTITY() AS IDENTITY_COLUMN_NAME";
		
		// prepare and bind
		$mainparams = array(
			$_POST['is_mhw_arranging_freight']=="Yes" ? 1 : 0,
			$_POST['container_type'],
			$_POST['number_of_pallets'],
			$_POST['total_weight'],
			$_POST['container_temperature'],
			$_POST['statement'],
			$_POST['freight_forwarder_name'],
			$_POST['eta_to_port'],
			$_POST['booked_to'],
			$_POST['tax_payment_status'],
			$_POST['final_destination'],
			$_POST['client_reference_number'],
			$userID,
			date('Y-m-d H:i:s'),
			0,
			0
		);

		$stmt = sqlsrv_query( $conn, $mainsql, $mainparams);

		if( $stmt === false ) {
			die( print_r( sqlsrv_errors(), true));
		}

		sqlsrv_next_result($stmt);  //note this line!!
		sqlsrv_fetch($stmt); 	
		$poId = sqlsrv_get_field($stmt, 0);



		//****** PO Product ***********
		
		$poproductsql = "INSERT INTO mhw_po_product (po_id, product_name, product_quantity, product_price, created_by) 
		VALUES (?, ?, ?, ?, ?)";

		$productarray = $_POST['product_id'];
		$prodquanarray = $_POST['product_quantity'];
		$prodpricearray = $_POST['product_price'];

		for ($x = 0; $x < sizeof($productarray); $x++) {
			$prod = $productarray[$x];
			$quan = $prodquanarray[$x]; 
			$price = $prodpricearray[$x];

			$poproductparams = array($poId, $prod, $quan, $price, $userID);		
			$poproductstmt = sqlsrv_query( $conn, $poproductsql, $poproductparams);

			if( $poproductstmt === false ) {
				die( print_r( sqlsrv_errors(), true));
			}
		}


		//****** PO Suppliers ***********
		
		$suppliers_sql = "INSERT INTO [mhw_po_request_suppliers] (request_id, supplier_id, supplier_name, supplier_contact, supplier_phone, supplier_email, cases_num, product_ready_date) 
		VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

		$supplier_ids = $_POST['supplier_id'];
		$supplier_names = $_POST['supplier_name'];
		$supplier_phones = $_POST['supplier_phone'];
		$supplier_contacts = $_POST['supplier_contact'];
		$cases_nums = $_POST['cases_num'];
		$product_ready_dates = $_POST['product_ready_date'];

		for ($i = 0; $i < sizeof($supplier_ids); $i++) {
			$supplier_id = $supplier_ids[$i];
			$supplier_name = $supplier_names[$i];
			$supplier_phone = $supplier_phones[$i];
			$supplier_contact = $supplier_contacts[$i];
			$cases_num = $cases_nums[$i];
			$product_ready_date = $product_ready_dates[$i];

			$stmt = sqlsrv_query( $conn, $suppliers_sql, [
				$poId, $supplier_id, $supplier_name, $supplier_contact, $supplier_phone, $supplier_email, $cases_num, $product_ready_date
			]);

			if( $stmt === false ) {
				die( print_r( sqlsrv_errors(), true));
			}
		}

		/*
		echo "<pre>";
		var_dump($_POST); 
		echo "</pre>";
		*/
	}

?>


	<style type="text/css">
		.warning {display:block; margin:15px 0;}
		.warning span {color:red;}
		input[type='number'] {width:18rem; display:inline;}
		#productvalid {display:none;}
		#nsupplier_fields {display:none;}
		label span {color:red;}
		.items{clear:both;}
		.repeater-remove-btn {width:100%;}
	</style>	

    <link href="css/wforms-layout_v530-14.css" rel="stylesheet" type="text/css" />
    <link href="css/theme-52661.css" rel="stylesheet" type="text/css" />
    <link href="css/wforms-jsonly_v530-14.css" rel="alternate stylesheet" title="This stylesheet activated by javascript" type="text/css" />
	<link href="css/wforms-inline.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/wforms_v530-14.js"></script>
    <script type="text/javascript" src="js/localization-en_US_v530-14.js"></script>         

</head>
<body class="default wFormWebPage">
<div class="container-fluid">
	<div id="tfaContent">
		<div class="wFormContainer" style="max-width: 85%; width:auto;" >
			<div class="row">
				<div class="col-sm-12">

    				<div class="wFormHeader"></div>
    		
					<h3 class="wFormTitle" id="4690986-T">INTERNATIONAL PURCHASE ORDER REQUEST FORM </h3>
					
					<p> <b class="text-danger">Instructions</b> – Utilize this form to request an international freight quote and / or notify MHW of an incoming international shipment.  Please fill out (1) form for each separate container request.  </p>

					<hr/>
					<h4> GENERAL CONTAINER INFORMATION </h4>

					<b class="text-danger">• Important Notes:</b>
					<br/>
					<ul>
						<li> <b>20 FT Container</b> – Holds 10 pallets with a maximum weight of . . . . </li>
						<li> <b>40 FT Container</b> – Holds 20 pallets with a maximum weight of . . . . </li>
						<li> <b>LCL</b> – Less than Container Load.  This freight is often more expensive on a per case basis than a full container </li>
						<li> <b>Dry Container</b> – This indicates there is no insulation / temperature control within the container </li>
						<li> <b>Insulated Container</b> – The freight forwarder will cover the product with a thermal blanket to help protect against heat / cold (*Additional Costs) </li>
						<li> <b>Refrigerated Container</b> – The container will maintain a set temperature throughout the sailing.  (*Additional Cost) </li>
						<li> <b>Door vs. Port Bookings</b> – Containers booked to door mean the freight forwarder arranges the shipment all the way to the door of the final destination.  Containers booked to port mean that the freight forwarder arranges shipment to the port and MHW arranges the movement from the port to the final destination. </li>
					</ul>

					<form method="post" action="mhw_po_request.php" class="hintsBelow labelsAbove" id="4690986" role="form" enctype="multipart/form-data">

						<div class="float-right"><button type="reset" value="Reset" id="clearform" class="btn btn-sm btn-outline-danger" onClick="window.location.reload()">Clear Form</button></div>	

						<div class="form-row">
							<div class="form-group col-md-12">
								<label><b>1. Is MHW Arranging Freight </b> <span>*</span><label>
								<select id="is_mhw_arranging_freight" name="is_mhw_arranging_freight" class="form-control" style="max-width:100px;margin-left:10px" required>
									<option>Yes</option>
									<option>No</option>
								</select>
							</div>	
						</div>
					
						<div class="freight_forwarder_block" style="display:none">

							<div class="form-group col-md-6">
								<label>Freight Forwarder Name <span>*</span></label>
								<input type="text" class="form-control" name="freight_forwarder_name" placeholder="Freight Forwarder Name (required)" maxlength="150"> 
							</div>

							<div class="form-group col-md-6">
								<label>ETA to Port <span>*</span> </label>
								<input type="date" class="form-control" name="eta_to_port" style="max-width:200px;"> 
							</div>

							<div class="form-group col-md-6">
							  	<label>Booked to port or to door <span>*</span></label>
								<select id="booked_to" name="booked_to" class="form-control" style="max-width:200px">
									<option value="">Select...</option>
									<option>DOOR</option>
									<option>PORT</option>
								</select>
							</div>	

						</div>

						<div class="common_block" style="display:block">

							<div class="form-group col-md-6">
							  	<label>Container Type <span class="mhw_arranging_freight_required"><span>*</span></span></label>
								<select id="container_type" name="container_type" class="form-control" style="max-width:200px" required>
									<option value="">Select...</option>
									<option value="20 FT">20 FT</option>
									<option value="40 FT">40 FT</option>
									<option value="LCL">LCL</option>
								</select>
							</div>	

							<div class="lcl_block" style="display:none">
								
									<div class="form-group col-md-6">
										<label>Number of Pallets </label><br/>
										<input type="number" class="form-control" name="number_of_pallets"> 
									</div>	

									<div class="form-group col-md-6">
										<label>Total Weight of entire shipment</label><br/>
										<input type="number" class="form-control" name="total_weight"> 
									</div>	

							</div>	

							<div class="form-group col-md-6">
							  	<label>Container Temperature  <span class="mhw_arranging_freight_required"><span>*</span></span></label>
								<select id="container_temperature" name="container_temperature" class="form-control" style="max-width:200px" required>
									<option value="">Select...</option>
									<option value="DRY">DRY</option>
									<option value="INSULATED">INSULATED</option>
									<option value="REFIGERATED">REFIGERATED</option>
								</select>
							</div>	

							<div class="form-group col-md-6 mhw_arranging_freight_required" >
								<label>MHW will get quotes for your approval prior to proceeding</label>
								<input type="hidden" class="form-control" name="statement" placeholder="Statement" maxlength="150">
							</div>


						</div>


						<div class="form-row">
							<div class="form-group col-md-12">
							<label><b>2. What is the tax payment status of the shipment <span>*</span></b></label>
							  <select id="tax_payment_status" name="tax_payment_status" class="form-control" style="max-width:200px;margin-left:10px" required>
									<option value="">Select...</option>
									<option>BOND</option>
									<option>TAX PAID</option>
							  </select>
							</div>	
						</div>

						<div class="form-row">
							<div class="form-group col-md-12">
								<label><b>3. What is the final destination of the shipment <span>*</span></b></label>
								<input type="text" class="form-control" name="final_destination" placeholder="The final destination of the shipment (EX. Western Carriers, NJ) " maxlength="150" required> 
							</div>	
						</div>

						<div class="form-row">
							<div class="form-group col-md-12">
								<label><b>4. Client Reference Number for the shipment </b></label>
								<input type="text" class="form-control" name="client_reference_number" placeholder="Client Reference Number for the shipment" maxlength="50"> 
							</div>	
						</div>


						<div class="freight_forwarder_block_reverse">

						<hr/>
						<h4> PICK UP POINTS </h4>
						<p> <b class="text-danger">Instructions:</b>  Please complete the below information to identify where the product will be picked up.  If there are multiple supplier pick ups please select the supplier and click "ADD SUPPLIER".  All suppliers must be listed to receive an accurate quote.  If a supplier is not selectable, please click "Enter New Supplier".  Please note that once you complete the information for a supplier the first time it will be saved as a selectable drop-down option for future use. </p>

						<div class="form-group">
						
							<div id="suppliers-repeater">
								<!-- Repeater Heading -->
								<div class="repeater-heading">
									<div class="form-row">
										
										<div class="form-group col-md-4">
											<select id="supplier_selected" name="supplier_selected" class="form-control custom-select">
												<option value=''>Select Supplier ...</option>
											</select>

										<!--
										<div class="oneField field-container-D" id="suppliers_combo-D">
											<div class="inputWrapper">
												<input type="text" id="suppliers_combo" name="suppliers_combo" value="" title="Supplier" data-dataset-allow-free-responses="" class="suppliers_combo">
											</div>
										</div>
										-->

										</div>
										<div class="form-group col-md-5">
											<button class="btn btn-primary repeater-add-btn btn-sm align-top" >
												Add Supplier
											</button>
											&nbsp;
											<button type="button" class="btn btn-primary btn-sm align-top" data-toggle="modal" data-target="#addSupplierModal">
												Enter New Supplier
											</button>
										</div>

									</div>

									<br/>
									<label><b>Supplier(s)</b></label>
									<br/>
									<div class="row" >
										<div class="col-2">
											<label>Name</label>
										</div>
										<div class="col-2">
											<label>Contact</label>
										</div>
										<div class="col-2">
											<label>Phone</label>
										</div>
										<div class="col-2">
											<label>Email</label>
										</div>
										<div class="col-1">
											<label># of cases to be picked</label>
										</div>
										<div class="col-2">
											<label>Product ready date</label>
										</div>
									</div>							

								</div>
							  
							  	<div class="items" data-group="items">
								<!-- Repeater Items Here -->		
								
									<div class="row" >
										<div class="col-2">
											<input type="hidden" name="supplier_id[]" value="" data-skip-name="true" class="supplier_id" required>
											<input type="text" name="supplier_name[]" value="" aria-required="true" title="Name" data-dataset-allow-free-responses="" data-skip-name="true" class="supplier_name" style="width:100%" readonly required>
										</div>
										<div class="col-2">
											<input type="text" name="supplier_contact[]" value="" aria-required="true" title="Contact" data-dataset-allow-free-responses="" data-skip-name="true" class="supplier_contact" style="width:100%" readonly>
										</div>
										<div class="col-2">
											<input type="text" name="supplier_phone[]" value="" aria-required="true" title="Phone" data-dataset-allow-free-responses="" data-skip-name="true" class="supplier_phone" style="width:100%" readonly>
										</div>
										<div class="col-2">
											<input type="email" name="supplier_email[]" value="" aria-required="true" title="Email" data-dataset-allow-free-responses="" data-skip-name="true" class="supplier_email" style="width:100%" readonly>
										</div>
										<div class="col-1">
											<input type="number" name="cases_num[]" min=1 value="1" aria-required="true" title="Cases" data-dataset-allow-free-responses="" data-skip-name="true" class="cases_num" style="width:100%" required>
										</div>
										<div class="col-2">
											<input type="date" name="product_ready_date[]" value="" aria-required="true" title="Phone" data-dataset-allow-free-responses="" data-skip-name="true" class="product_ready_date" style="width:100%" required>
										</div>
										<div class="col-1">
											<button id="remove-btn" class="btn btn-danger btn-sm" style="margin-bottom: 0.2rem;" onclick="$(this).parents('.items').remove()">
												&times;
											</button>
										</div>
									</div>							

								</div>

							</div> <!-- repeater -->

						</div>
						</div>



						<br/>
						<hr/>
						<h4> ITEMS </h4>

						<p> <b class="text-danger">Instructions:</b> Please select the items and quantity for all SKU's that will be on this shipment.  If desired, you may also enter that item's purchasing price.  If an item is not available to select, please return to the item / product set up and create the item.  Once the item is established in MHW’s system you can create the purchase order. </p>
						<p> <b>Note:</b>  All items shipping must be listed below with the correct bottle size, case configuration and quantity.  Missing items could result in delays with US Customs Clearance and potentially additional fees. </p>
  
						<div class="form-group container-fluid">
							
							<div id="items-repeater">
								<!-- Repeater Heading -->
								<div class="repeater-heading">

									<div class="oneField field-container-D" id="tfa_product_desc-D">
										<div class="inputWrapper">
											<input type="text" id="tfa_product_desc" name="tfa_product_desc" value="<?php echo $pf_product_desc; ?>" title="Product Name" data-dataset-allow-free-responses="" class="tfa_product_desc">
											<button class="btn btn-primary repeater-add-btn repeater-add-item-btn btn-sm align-top" >
												Add Item
											</button>
											<br/>
											<small id="productvalid" class="warning"><span>No item match found. </span></small>
										</div>
									</div>
									<br/>
									<label><b>Item(s)</b></label>
									<br/>
									<div class="row" >
										<div class="col-5">
											<label>Item Name</label>
										</div>
										<div class="col-3">
											<label>Quantity</label>
										</div>
										<div class="col-3">
											<label>Purchase Price</label>
										</div>
									</div>							

								</div>
							  
							  	<div class="items" data-group="items">
								<!-- Repeater Items Here -->		
								
									<div class="row" >
										<div class="col-5">
											<input type="hidden" name="product_id[]" value="" data-skip-name="true" class="tfa_product_id" required>
											<input type="text" name="product_name[]" value="" aria-required="true" title="Item Name" data-dataset-allow-free-responses="" data-skip-name="true" class="tfa_product_desc" style="width:100%" readonly required>
										</div>
										<div class="col-3">
											<input type="number" data-skip-name="true" name="product_quantity[]" placeholder="Quantity" min=1 value="1" maxlength="10" style="text-align:right;width:100%" required>	
										</div>
										<div class="col-3">
											<input type="number" min="0" step=".01" name="product_price[]" placeholder="Purchase Price" data-dataset-allow-free-responses="" data-skip-name="true" value=""  maxlength="10" style="text-align:right;width:100%">
										</div>
										<div class="col-1">
											<button id="remove-btn" class="btn btn-danger btn-sm" style="margin-bottom: 0.2rem;" onclick="$(this).parents('.items').remove()">
												&times;
											</button>
										</div>
									</div>							

								</div>

							</div> <!-- repeater -->

						</div> <!-- form-group -->


						<br/>
						<br/>
						<hr/>
						<h4> SPECIAL INSTRUCTIONS </h4>

						<p> <b class="text-danger">Instructions:</b> Please let us know if there are any special instructions or notes for this shipment request. </p>

						<div class="form-row">
							<div class="form-group col-md-12">
								<textarea class="form-control" name="special" ></textarea>
							</div>	
						</div>
						


						<hr/>
						<h4> CERTIFICATION </h4>

						<p> By clicking here I acknowledge that all funds for the shipment (freight, tax, duty, broker fees, etc) must be in place in your MHW bank account prior to the freight being picked up from the foreign supplier.  I also acknowledge that incorrect item / quantity information can result in delays with the US Customs Clearance process, inbounding at the final destination and potentially additional fees. </p>
						<input type="checkbox" class="form-control" name="certificaton" required> 


						<div class="actions" id="4690986-A"><input type="submit" data-label="Submit" class="primaryAction btn btn-primary" id="submit_button" value="Submit"></div>
						<div style="clear:both"></div>
						
						<input type="hidden" name="prodID" id="prodID" value="<?php echo $prodID; ?>">
						<input type="hidden" name="product_mhw_code" id="product_mhw_code" value="<?php echo $product_mhw_code; ?>">
						<input type="hidden" name="product_mhw_code_search" id="product_mhw_code_search" value="<?php echo $product_mhw_code_search; ?>">
						<input type="hidden" name="userID" id="userID" value="<?php echo $_SESSION['mhwltdphp_user']; ?>">

					</form>
				</div>
			</div>
		</div>    
	</div>

	<!-- Modal -->
	<div class="modal fade" id="addSupplierModal" tabindex="-1" role="dialog" aria-labelledby="addSupplierModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="addSupplierModalLabel">Add New Supplier</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
		<form id="supplier-form">
			<div class="form-row">
				<div class="form-group col-md-12">
					<label>Supplier Name <span>*</span></label>
					<input type="text" class="form-control supplierreq" name="supplier_name" maxlength="150" required>
				</div>
				<div class="form-group col-md-12">
					<label>Supplier Contact Person <span>*</span></label>
					<input type="text" class="form-control supplierreq" name="supplier_contact" maxlength="150" required>
				</div>
				<div class="form-group col-md-12">
					<label>Supplier Phone <span>*</span></label>
					<input type="phone" class="form-control supplierreq" name="supplier_phone" maxlength="25" required>
				</div>				
				<div class="form-group col-md-12">
					<label>Supplier Email <span>*</span></label>
					<input type="email" class="form-control supplierreq" name="supplier_email" maxlength="150" required>
				</div>									
			</div>	
		</form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-primary" id="add_supplier">Add Supplier</button>
		</div>
		</div>
	</div>
	</div>

	<script src="mhw_po_request.js?v=<?=rand()?>"></script>
	<script src="js/repeater.js?v=<?=rand()?>"></script>
	<script src='js/iframe_resize_helper_internal.js'></script>
</div>
</body>
</html>
<?php
}
?>