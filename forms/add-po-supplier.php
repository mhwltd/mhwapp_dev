<?php

	error_reporting(E_ALL); //displays an error

	session_start();
	if(!isset($_SESSION['mhwltdphp_user'])){
		die( "Not authenticated !" );  
	}

	include("dbconnect.php");

	//Establishes the connection
	$conn = sqlsrv_connect($serverName, $connectionOptions);
	if( $conn === false) {
			die( print_r( sqlsrv_errors(), true));
	}

	$tsql = "
		INSERT INTO mhw_suppliers (
			supplier_name,
			supplier_contact, 
			supplier_phone,
			supplier_email,
			create_user,
			create_date,
			client_code,
			deleted
		)
		VALUES (
			?,?,?,?,?,?,?,?
		);
	";

	$params = array ( 
		$_POST['supplier_name'],
		$_POST['supplier_contact'],
		$_POST['supplier_phone'],
		$_POST['supplier_email'],
		$_SESSION['mhwltdphp_user'],
		date('Y-m-d H:i:s'),
		$_POST['client_code'],
		0
	);

	$stmt = sqlsrv_prepare( $conn, $tsql, $params);  

	if( $stmt === false )  
	{  
		 echo "Statement could not be prepared.\n";  
		 die( print_r( sqlsrv_errors(), true));  
	}  

	if( sqlsrv_execute($stmt) === false )  
	{  
	  echo "Statement could not be executed.\n";  
	  die( print_r( sqlsrv_errors(), true));  
	}  


	$json = array(
		'result' => sqlsrv_rows_affected ( $stmt )
	);
	header('Content-Type: application/json');
	echo json_encode($json);

	/* Free statement and connection resources. */  
	sqlsrv_free_stmt( $stmt);  
	sqlsrv_close( $conn); 

	//header("Location: result.php");

?>