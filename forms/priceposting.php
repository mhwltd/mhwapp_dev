<?php
include('head.php');
//if(!isset($_SESSION['mhwltdphp_user'])){

	//header("Location: login.php");
//}
//else{
include("sessionhandler.php");

/* get distributors */
include("dbconnect.php");

$distributorsArray = array();
$conn = sqlsrv_connect($serverName, $connectionOptions);
if( $conn === false) {
    die( print_r( sqlsrv_errors(), true));
}
$tsql= "SELECT * FROM [dbo].[sc_distributor-list]";
$stmt = sqlsrv_query($conn, $tsql);
if( $stmt ) {
    while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
        array_push($distributorsArray, $row);
    }
}
sqlsrv_free_stmt($stmt);

?>

<!DOCTYPE HTML>
<html lang="en-US">
<head>

    <title>Price Posting Form</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function(){
            const FORM_TIME_START = Math.floor((new Date).getTime()/1000);
            let formElement = document.getElementById("tfa_0");
            if (null === formElement) {
                formElement = document.getElementById("0");
            }
            let appendJsTimerElement = function(){
                let formTimeDiff = Math.floor((new Date).getTime()/1000) - FORM_TIME_START;
                let cumulatedTimeElement = document.getElementById("tfa_dbCumulatedTime");
                if (null !== cumulatedTimeElement) {
                    let cumulatedTime = parseInt(cumulatedTimeElement.value);
                    if (null !== cumulatedTime && cumulatedTime > 0) {
                        formTimeDiff += cumulatedTime;
                    }
                }
                let jsTimeInput = document.createElement("input");
                jsTimeInput.setAttribute("type", "hidden");
                jsTimeInput.setAttribute("value", formTimeDiff.toString());
                jsTimeInput.setAttribute("name", "tfa_dbElapsedJsTime");
                jsTimeInput.setAttribute("id", "tfa_dbElapsedJsTime");
                jsTimeInput.setAttribute("autocomplete", "off");
                if (null !== formElement) {
                    formElement.appendChild(jsTimeInput);
                }
            };
            if (null !== formElement) {
                if(formElement.addEventListener){
                    formElement.addEventListener('submit', appendJsTimerElement, false);
                } else if(formElement.attachEvent){
                    formElement.attachEvent('onsubmit', appendJsTimerElement);
                }
            }
        });


    </script>


	<script>
	
		$(document).ready(function(){
			/*======BEGIN comboboxes==========*/
			function cbb_formatter(){ 
				//$('#tfa_brand_name').addClass('combobox_restyle');
				//$('#tfa_brand_name').css( "border-radius", "0px" );
				$('ul.ac_results').css("margin-left","0em");
				$('ul.ac_results').css("margin","0em");
				$('.ac_button').css("height","28px");
				$('.ac_button').css("background","transparent");
				$('.ac_button').css("margin-left","-35px");
				$('.ac_button').css("border","0px");
				$('.ac_subinfo dl').css("margin-left","0em");
				$('.ac_subinfo dl').css("margin","0em");
			}
			//load form fields - as a result of selecting item_desc combobox
			function prefill_item(itemID){
				var qrytype = "itemByID";
				var client = $('#tfa_client_name').val(); //get value of the client combobox
				$.post('query-request.php', {qrytype:qrytype,itemID:itemID,client:client}, function(dataPx){ // ajax request query-request.php, send brand as post variable, return dataX variable, parse into JSON

					var t = JSON.parse(dataPx);

					$.each( t, function( key, value ) {
						//product
						$('#tfa_item_code').val(value.item_mhw_code);
						$('#tfa_stock_uom').val(value.stock_uom);
						$('#tfa_bottles_per_case').val(value.bottles_per_case);
	
					});
				});
			}
			function isItemValidSelection(){
				var isItemID = $('#tfa_item_desc')[0].hasAttribute("sub_info"); //free-type data will not have sub_info attribute
				if(isItemID){
					var json = $('#tfa_item_desc').attr('sub_info');
					//if additional sub_info elements are added, additional apostrophe replacements need to be added
					var json2 = json.replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"');
					var jsonObj = JSON.parse(json2);

					$("#itemID").val(jsonObj.id);  //set field for processing update
					$("#tfa_4-L").html("Item: editing item_id "+jsonObj.id); //display message for update
					prefill_item(jsonObj.id); //prefill form fields
				}
				else{
					$("#tfa_4-L").html("Item: not found"); //display message for insert
					$("#itemID").val("");  //clear field for insert

				}
			}

		//initialize product combobox
			function initialize_item(){
				$('#tfa_item_desc').parent().find(".ac_button").remove();  //clear elements of previous instance
				$('#tfa_item_desc').parent().find(".ac_result_area").remove();  //clear elements of previous instance
				var client = $('#tfa_client_name').val(); //get value of the client combobox
				var cbbtype = 'item';
				$.post('select-request.php', {cbbtype:cbbtype,client:client}, function(dataX){ // ajax request select-request.php, send brand as post variable, return dataX variable, parse into JSON
					var data = JSON.parse(dataX);
					$(function() {
						$('#tfa_item_desc').ajaxComboBox(
							data,
							{
							  bind_to: 'tfa_item_desc',
							  sub_info: true,
							  sub_as: {
								id: 'Item ID',
								code: 'MHW Item Code',
								code2: 'Client Item Code',
								parent: 'Product Code',
								parent2: 'Product Description',
								brand: 'Brand'
							  },
							}
						).bind('tfa_item_desc', function() {
						  var json = $(this).attr('sub_info');
						  //if additional sub_info elements are added, additional apostrophe replacements need to be added
						  var json2 = json.replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"');
						  var jsonObj = JSON.parse(json2);

						  if(parseInt(jsonObj.id) > 0){
							$("#itemID").val(jsonObj.id); //set field for processing update
							$("#tfa_4-L").html("Item: editing item_id "+jsonObj.id); //display message for update
							prefill_item(jsonObj.id); //prefill form fields
						  }
						  isItemValidSelection();

						});
						cbb_formatter(); //apply css and classname changes to combobox elements
					});
				});
				$('#tfa_product_desc').bind("change", function(){ 
					isItemValidSelection();
				});
			}
			initialize_item();

			//client combobox >> brand combobox
			$('#tfa_client_name').change(function(){ //if client value changes
				//replace combobox elements with original input
				$('#tfa_item_desc').parent().parent().html("<input type=\"text\" id=\"tfa_item_desc\" name=\"tfa_item_desc\" value=\"\" aria-required=\"true\" title=\"Item Description\" data-dataset-allow-free-responses=\"\" class=\"required validate-alphanum\">");
				
				//re-initialize item combobox
				initialize_item();

				isItemValidSelection();

				cbb_formatter(); //apply css and classname changes to combobox elements
			});
			/*======END comboboxes==========*/
			//clear form
			$("#clearform").click(function() {		
				$('#itemID').val('');
				$('#tfa_stock_uom').val('');
				$('#tfa_bottles_per_case').val('');
				$('#tfa_item_code').val('');

				$("#tfa_4-L").html("Item"); //display message for insert

				//replace combobox elements with original input
				$('#tfa_item_desc').parent().parent().html("<input type=\"text\" id=\"tfa_item_desc\" name=\"tfa_item_desc\" value=\"\" aria-required=\"true\" title=\"Item Description\" data-dataset-allow-free-responses=\"\" class=\"required validate-alphanum\">");

				initialize_item();

				cbb_formatter();

			});
			
			

		});
    </script>

	
    <link href="css/wforms-layout_v530-14.css" rel="stylesheet" type="text/css" />

    <link href="css/theme-52661.css" rel="stylesheet" type="text/css" />
    <link href="css/wforms-jsonly_v530-14.css" rel="alternate stylesheet" title="This stylesheet activated by javascript" type="text/css" />
    <script type="text/javascript" src="js/wforms_v530-14.js"></script>
    <script type="text/javascript" src="js/localization-en_US_v530-14.js"></script>

<style type="text/css">

.wForm fieldset.offstate , .offstate {display:none;}
</style>
            
    
</head>
<body class="default wFormWebPage">


    <div id="tfaContent">
        <div class="wFormContainer" style="max-width: 80%; width:auto;" >
    <div class="wFormHeader"></div>
    <style type="text/css">
                #tfa_6,
                *[id^="tfa_6["] {
                    width: 327px !important;
                }
                #tfa_6-D,
                *[id^="tfa_6["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_5,
                *[id^="tfa_5["] {
                    width: 207px !important;
                }
                #tfa_5-D,
                *[id^="tfa_5["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_1820,
                *[id^="tfa_1820["] {
                    width: 106px !important;
                }
                #tfa_1820-D,
                *[id^="tfa_1820["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_1822,
                *[id^="tfa_1822["] {
                    width: 113px !important;
                }
                #tfa_1822-D,
                *[id^="tfa_1822["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nat_price,
                *[id^="tfa_nat_price["] {
                    width: 101px !important;
                }
                #tfa_nat_price-D,
                *[id^="tfa_nat_price["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ca_state,
                *[id^="tfa_ca_state["] {
                    width: 102px !important;
                }
                #tfa_ca_state-D,
                *[id^="tfa_ca_state["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ct_state,
                *[id^="tfa_ct_state["] {
                    width: 102px !important;
                }
                #tfa_ct_state-D,
                *[id^="tfa_ct_state["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_state,
                *[id^="tfa_ma_state["] {
                    width: 102px !important;
                }
                #tfa_ma_state-D,
                *[id^="tfa_ma_state["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_state,
                *[id^="tfa_mn_state["] {
                    width: 102px !important;
                }
                #tfa_mn_state-D,
                *[id^="tfa_mn_state["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_state,
                *[id^="tfa_nj_state["] {
                    width: 102px !important;
                }
                #tfa_nj_state-D,
                *[id^="tfa_nj_state["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_state,
                *[id^="tfa_nyw_state["] {
                    width: 102px !important;
                }
                #tfa_nyw_state-D,
                *[id^="tfa_nyw_state["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_state,
                *[id^="tfa_nyr_state["] {
                    width: 102px !important;
                }
                #tfa_nyr_state-D,
                *[id^="tfa_nyr_state["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_other_state,
                *[id^="tfa_other_state["] {
                    width: 102px !important;
                }
                #tfa_other_state-D,
                *[id^="tfa_other_state["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ca_change_type,
                *[id^="tfa_ca_change_type["] {
                    width: 354px !important;
                }
                #tfa_ca_change_type-D,
                *[id^="tfa_ca_change_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ca_price_bottle,
                *[id^="tfa_ca_price_bottle["] {
                    width: 143px !important;
                }
                #tfa_ca_price_bottle-D,
                *[id^="tfa_ca_price_bottle["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ca_price_case,
                *[id^="tfa_ca_price_case["] {
                    width: 143px !important;
                }
                #tfa_ca_price_case-D,
                *[id^="tfa_ca_price_case["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ca_stand_cost,
                *[id^="tfa_ca_stand_cost["] {
                    width: 143px !important;
                }
                #tfa_ca_stand_cost-D,
                *[id^="tfa_ca_stand_cost["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ca_purch_cost,
                *[id^="tfa_ca_purch_cost["] {
                    width: 143px !important;
                }
                #tfa_ca_purch_cost-D,
                *[id^="tfa_ca_purch_cost["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ca_fob,
                *[id^="tfa_ca_fob["] {
                    width: 143px !important;
                }
                #tfa_ca_fob-D,
                *[id^="tfa_ca_fob["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ca_package,
                *[id^="tfa_ca_package["] {
                    width: 134px !important;
                }
                #tfa_ca_package-D,
                *[id^="tfa_ca_package["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ca_contents,
                *[id^="tfa_ca_contents["] {
                    width: 113px !important;
                }
                #tfa_ca_contents-D,
                *[id^="tfa_ca_contents["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ca_cont_charge,
                *[id^="tfa_ca_cont_charge["] {
                    width: 194px !important;
                }
                #tfa_ca_cont_charge-D,
                *[id^="tfa_ca_cont_charge["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ca_price_retail,
                *[id^="tfa_ca_price_retail["] {
                    width: 134px !important;
                }
                #tfa_ca_price_retail-D,
                *[id^="tfa_ca_price_retail["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ca_price_wholesale,
                *[id^="tfa_ca_price_wholesale["] {
                    width: 161px !important;
                }
                #tfa_ca_price_wholesale-D,
                *[id^="tfa_ca_price_wholesale["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ca_foe,
                *[id^="tfa_ca_foe["] {
                    width: 126px !important;
                }
                #tfa_ca_foe-D,
                *[id^="tfa_ca_foe["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_158,
                *[id^="tfa_158["] {
                    width: 168px !important;
                }
                #tfa_158-D,
                *[id^="tfa_158["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_158-L,
                label[id^="tfa_158["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_161,
                *[id^="tfa_161["] {
                    width: 168px !important;
                }
                #tfa_161-D,
                *[id^="tfa_161["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_161-L,
                label[id^="tfa_161["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_163,
                *[id^="tfa_163["] {
                    width: 160px !important;
                }
                #tfa_163-D,
                *[id^="tfa_163["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_163-L,
                label[id^="tfa_163["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_159,
                *[id^="tfa_159["] {
                    width: 168px !important;
                }
                #tfa_159-D,
                *[id^="tfa_159["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_159-L,
                label[id^="tfa_159["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_180,
                *[id^="tfa_180["] {
                    width: 168px !important;
                }
                #tfa_180-D,
                *[id^="tfa_180["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_180-L,
                label[id^="tfa_180["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_181,
                *[id^="tfa_181["] {
                    width: 168px !important;
                }
                #tfa_181-D,
                *[id^="tfa_181["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_181-L,
                label[id^="tfa_181["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_184,
                *[id^="tfa_184["] {
                    width: 160px !important;
                }
                #tfa_184-D,
                *[id^="tfa_184["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_184-L,
                label[id^="tfa_184["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_187,
                *[id^="tfa_187["] {
                    width: 168px !important;
                }
                #tfa_187-D,
                *[id^="tfa_187["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_187-L,
                label[id^="tfa_187["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_200,
                *[id^="tfa_200["] {
                    width: 168px !important;
                }
                #tfa_200-D,
                *[id^="tfa_200["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_200-L,
                label[id^="tfa_200["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_201,
                *[id^="tfa_201["] {
                    width: 168px !important;
                }
                #tfa_201-D,
                *[id^="tfa_201["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_201-L,
                label[id^="tfa_201["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_204,
                *[id^="tfa_204["] {
                    width: 160px !important;
                }
                #tfa_204-D,
                *[id^="tfa_204["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_204-L,
                label[id^="tfa_204["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_207,
                *[id^="tfa_207["] {
                    width: 168px !important;
                }
                #tfa_207-D,
                *[id^="tfa_207["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_207-L,
                label[id^="tfa_207["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_220,
                *[id^="tfa_220["] {
                    width: 168px !important;
                }
                #tfa_220-D,
                *[id^="tfa_220["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_220-L,
                label[id^="tfa_220["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_221,
                *[id^="tfa_221["] {
                    width: 168px !important;
                }
                #tfa_221-D,
                *[id^="tfa_221["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_221-L,
                label[id^="tfa_221["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_224,
                *[id^="tfa_224["] {
                    width: 160px !important;
                }
                #tfa_224-D,
                *[id^="tfa_224["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_224-L,
                label[id^="tfa_224["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_227,
                *[id^="tfa_227["] {
                    width: 168px !important;
                }
                #tfa_227-D,
                *[id^="tfa_227["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_227-L,
                label[id^="tfa_227["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_240,
                *[id^="tfa_240["] {
                    width: 168px !important;
                }
                #tfa_240-D,
                *[id^="tfa_240["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_240-L,
                label[id^="tfa_240["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_241,
                *[id^="tfa_241["] {
                    width: 168px !important;
                }
                #tfa_241-D,
                *[id^="tfa_241["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_241-L,
                label[id^="tfa_241["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_244,
                *[id^="tfa_244["] {
                    width: 160px !important;
                }
                #tfa_244-D,
                *[id^="tfa_244["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_244-L,
                label[id^="tfa_244["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_247,
                *[id^="tfa_247["] {
                    width: 168px !important;
                }
                #tfa_247-D,
                *[id^="tfa_247["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_247-L,
                label[id^="tfa_247["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_260,
                *[id^="tfa_260["] {
                    width: 168px !important;
                }
                #tfa_260-D,
                *[id^="tfa_260["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_260-L,
                label[id^="tfa_260["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_261,
                *[id^="tfa_261["] {
                    width: 168px !important;
                }
                #tfa_261-D,
                *[id^="tfa_261["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_261-L,
                label[id^="tfa_261["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_264,
                *[id^="tfa_264["] {
                    width: 160px !important;
                }
                #tfa_264-D,
                *[id^="tfa_264["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_264-L,
                label[id^="tfa_264["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_267,
                *[id^="tfa_267["] {
                    width: 168px !important;
                }
                #tfa_267-D,
                *[id^="tfa_267["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_267-L,
                label[id^="tfa_267["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_280,
                *[id^="tfa_280["] {
                    width: 168px !important;
                }
                #tfa_280-D,
                *[id^="tfa_280["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_280-L,
                label[id^="tfa_280["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_281,
                *[id^="tfa_281["] {
                    width: 168px !important;
                }
                #tfa_281-D,
                *[id^="tfa_281["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_281-L,
                label[id^="tfa_281["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_284,
                *[id^="tfa_284["] {
                    width: 160px !important;
                }
                #tfa_284-D,
                *[id^="tfa_284["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_284-L,
                label[id^="tfa_284["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_287,
                *[id^="tfa_287["] {
                    width: 168px !important;
                }
                #tfa_287-D,
                *[id^="tfa_287["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_287-L,
                label[id^="tfa_287["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_300,
                *[id^="tfa_300["] {
                    width: 168px !important;
                }
                #tfa_300-D,
                *[id^="tfa_300["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_300-L,
                label[id^="tfa_300["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_301,
                *[id^="tfa_301["] {
                    width: 168px !important;
                }
                #tfa_301-D,
                *[id^="tfa_301["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_301-L,
                label[id^="tfa_301["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_304,
                *[id^="tfa_304["] {
                    width: 160px !important;
                }
                #tfa_304-D,
                *[id^="tfa_304["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_304-L,
                label[id^="tfa_304["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_307,
                *[id^="tfa_307["] {
                    width: 168px !important;
                }
                #tfa_307-D,
                *[id^="tfa_307["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_307-L,
                label[id^="tfa_307["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_320,
                *[id^="tfa_320["] {
                    width: 168px !important;
                }
                #tfa_320-D,
                *[id^="tfa_320["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_320-L,
                label[id^="tfa_320["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_321,
                *[id^="tfa_321["] {
                    width: 168px !important;
                }
                #tfa_321-D,
                *[id^="tfa_321["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_321-L,
                label[id^="tfa_321["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_324,
                *[id^="tfa_324["] {
                    width: 168px !important;
                }
                #tfa_324-D,
                *[id^="tfa_324["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_324-L,
                label[id^="tfa_324["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_327,
                *[id^="tfa_327["] {
                    width: 168px !important;
                }
                #tfa_327-D,
                *[id^="tfa_327["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_327-L,
                label[id^="tfa_327["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_340,
                *[id^="tfa_340["] {
                    width: 168px !important;
                }
                #tfa_340-D,
                *[id^="tfa_340["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_340-L,
                label[id^="tfa_340["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_341,
                *[id^="tfa_341["] {
                    width: 168px !important;
                }
                #tfa_341-D,
                *[id^="tfa_341["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_341-L,
                label[id^="tfa_341["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_344,
                *[id^="tfa_344["] {
                    width: 168px !important;
                }
                #tfa_344-D,
                *[id^="tfa_344["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_344-L,
                label[id^="tfa_344["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_347,
                *[id^="tfa_347["] {
                    width: 168px !important;
                }
                #tfa_347-D,
                *[id^="tfa_347["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_347-L,
                label[id^="tfa_347["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_ct_change_type,
                *[id^="tfa_ct_change_type["] {
                    width: 354px !important;
                }
                #tfa_ct_change_type-D,
                *[id^="tfa_ct_change_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ct_price_bot,
                *[id^="tfa_ct_price_bot["] {
                    width: 143px !important;
                }
                #tfa_ct_price_bot-D,
                *[id^="tfa_ct_price_bot["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ct_price_case,
                *[id^="tfa_ct_price_case["] {
                    width: 143px !important;
                }
                #tfa_ct_price_case-D,
                *[id^="tfa_ct_price_case["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ct_stand_cost,
                *[id^="tfa_ct_stand_cost["] {
                    width: 143px !important;
                }
                #tfa_ct_stand_cost-D,
                *[id^="tfa_ct_stand_cost["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ct_purch_cost,
                *[id^="tfa_ct_purch_cost["] {
                    width: 143px !important;
                }
                #tfa_ct_purch_cost-D,
                *[id^="tfa_ct_purch_cost["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ct_fob,
                *[id^="tfa_ct_fob["] {
                    width: 143px !important;
                }
                #tfa_ct_fob-D,
                *[id^="tfa_ct_fob["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_change_type,
                *[id^="tfa_ma_change_type["] {
                    width: 354px !important;
                }
                #tfa_ma_change_type-D,
                *[id^="tfa_ma_change_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_price_bot,
                *[id^="tfa_ma_price_bot["] {
                    width: 143px !important;
                }
                #tfa_ma_price_bot-D,
                *[id^="tfa_ma_price_bot["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_price_case,
                *[id^="tfa_ma_price_case["] {
                    width: 143px !important;
                }
                #tfa_ma_price_case-D,
                *[id^="tfa_ma_price_case["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_stand_cost,
                *[id^="tfa_ma_stand_cost["] {
                    width: 143px !important;
                }
                #tfa_ma_stand_cost-D,
                *[id^="tfa_ma_stand_cost["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_purch_cost,
                *[id^="tfa_ma_purch_cost["] {
                    width: 143px !important;
                }
                #tfa_ma_purch_cost-D,
                *[id^="tfa_ma_purch_cost["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_fob,
                *[id^="tfa_ma_fob["] {
                    width: 143px !important;
                }
                #tfa_ma_fob-D,
                *[id^="tfa_ma_fob["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty1_quan,
                *[id^="tfa_ma_qty1_quan["] {
                    width: 168px !important;
                }
                #tfa_ma_qty1_quan-D,
                *[id^="tfa_ma_qty1_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty1_quan-L,
                label[id^="tfa_ma_qty1_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty1_on,
                *[id^="tfa_ma_qty1_on["] {
                    width: 168px !important;
                }
                #tfa_ma_qty1_on-D,
                *[id^="tfa_ma_qty1_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty1_on-L,
                label[id^="tfa_ma_qty1_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty1_type,
                *[id^="tfa_ma_qty1_type["] {
                    width: 160px !important;
                }
                #tfa_ma_qty1_type-D,
                *[id^="tfa_ma_qty1_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty1_type-L,
                label[id^="tfa_ma_qty1_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty1_amt,
                *[id^="tfa_ma_qty1_amt["] {
                    width: 168px !important;
                }
                #tfa_ma_qty1_amt-D,
                *[id^="tfa_ma_qty1_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty1_amt-L,
                label[id^="tfa_ma_qty1_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty2_quan,
                *[id^="tfa_ma_qty2_quan["] {
                    width: 168px !important;
                }
                #tfa_ma_qty2_quan-D,
                *[id^="tfa_ma_qty2_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty2_quan-L,
                label[id^="tfa_ma_qty2_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty2_on,
                *[id^="tfa_ma_qty2_on["] {
                    width: 168px !important;
                }
                #tfa_ma_qty2_on-D,
                *[id^="tfa_ma_qty2_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty2_on-L,
                label[id^="tfa_ma_qty2_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty2_type,
                *[id^="tfa_ma_qty2_type["] {
                    width: 160px !important;
                }
                #tfa_ma_qty2_type-D,
                *[id^="tfa_ma_qty2_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty2_type-L,
                label[id^="tfa_ma_qty2_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty2_amt,
                *[id^="tfa_ma_qty2_amt["] {
                    width: 168px !important;
                }
                #tfa_ma_qty2_amt-D,
                *[id^="tfa_ma_qty2_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty2_amt-L,
                label[id^="tfa_ma_qty2_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty3_quan,
                *[id^="tfa_ma_qty3_quan["] {
                    width: 168px !important;
                }
                #tfa_ma_qty3_quan-D,
                *[id^="tfa_ma_qty3_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty3_quan-L,
                label[id^="tfa_ma_qty3_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty3_on,
                *[id^="tfa_ma_qty3_on["] {
                    width: 168px !important;
                }
                #tfa_ma_qty3_on-D,
                *[id^="tfa_ma_qty3_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty3_on-L,
                label[id^="tfa_ma_qty3_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty3_type,
                *[id^="tfa_ma_qty3_type["] {
                    width: 160px !important;
                }
                #tfa_ma_qty3_type-D,
                *[id^="tfa_ma_qty3_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty3_type-L,
                label[id^="tfa_ma_qty3_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty3_amt,
                *[id^="tfa_ma_qty3_amt["] {
                    width: 168px !important;
                }
                #tfa_ma_qty3_amt-D,
                *[id^="tfa_ma_qty3_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty3_amt-L,
                label[id^="tfa_ma_qty3_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty4_quan,
                *[id^="tfa_ma_qty4_quan["] {
                    width: 168px !important;
                }
                #tfa_ma_qty4_quan-D,
                *[id^="tfa_ma_qty4_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty4_quan-L,
                label[id^="tfa_ma_qty4_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty4_on,
                *[id^="tfa_ma_qty4_on["] {
                    width: 168px !important;
                }
                #tfa_ma_qty4_on-D,
                *[id^="tfa_ma_qty4_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty4_on-L,
                label[id^="tfa_ma_qty4_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty4_type,
                *[id^="tfa_ma_qty4_type["] {
                    width: 160px !important;
                }
                #tfa_ma_qty4_type-D,
                *[id^="tfa_ma_qty4_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty4_type-L,
                label[id^="tfa_ma_qty4_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty4_amt,
                *[id^="tfa_ma_qty4_amt["] {
                    width: 168px !important;
                }
                #tfa_ma_qty4_amt-D,
                *[id^="tfa_ma_qty4_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty4_amt-L,
                label[id^="tfa_ma_qty4_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty5_quan,
                *[id^="tfa_ma_qty5_quan["] {
                    width: 168px !important;
                }
                #tfa_ma_qty5_quan-D,
                *[id^="tfa_ma_qty5_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty5_quan-L,
                label[id^="tfa_ma_qty5_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty5_on,
                *[id^="tfa_ma_qty5_on["] {
                    width: 168px !important;
                }
                #tfa_ma_qty5_on-D,
                *[id^="tfa_ma_qty5_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty5_on-L,
                label[id^="tfa_ma_qty5_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty5_type,
                *[id^="tfa_ma_qty5_type["] {
                    width: 160px !important;
                }
                #tfa_ma_qty5_type-D,
                *[id^="tfa_ma_qty5_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty5_type-L,
                label[id^="tfa_ma_qty5_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty5_amt,
                *[id^="tfa_ma_qty5_amt["] {
                    width: 168px !important;
                }
                #tfa_ma_qty5_amt-D,
                *[id^="tfa_ma_qty5_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty5_amt-L,
                label[id^="tfa_ma_qty5_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty6_quan,
                *[id^="tfa_ma_qty6_quan["] {
                    width: 168px !important;
                }
                #tfa_ma_qty6_quan-D,
                *[id^="tfa_ma_qty6_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty6_quan-L,
                label[id^="tfa_ma_qty6_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty6_on,
                *[id^="tfa_ma_qty6_on["] {
                    width: 168px !important;
                }
                #tfa_ma_qty6_on-D,
                *[id^="tfa_ma_qty6_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty6_on-L,
                label[id^="tfa_ma_qty6_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty6_type,
                *[id^="tfa_ma_qty6_type["] {
                    width: 160px !important;
                }
                #tfa_ma_qty6_type-D,
                *[id^="tfa_ma_qty6_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty6_type-L,
                label[id^="tfa_ma_qty6_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty6_amt,
                *[id^="tfa_ma_qty6_amt["] {
                    width: 168px !important;
                }
                #tfa_ma_qty6_amt-D,
                *[id^="tfa_ma_qty6_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty6_amt-L,
                label[id^="tfa_ma_qty6_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty7_quan,
                *[id^="tfa_ma_qty7_quan["] {
                    width: 168px !important;
                }
                #tfa_ma_qty7_quan-D,
                *[id^="tfa_ma_qty7_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty7_quan-L,
                label[id^="tfa_ma_qty7_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty7_on,
                *[id^="tfa_ma_qty7_on["] {
                    width: 168px !important;
                }
                #tfa_ma_qty7_on-D,
                *[id^="tfa_ma_qty7_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty7_on-L,
                label[id^="tfa_ma_qty7_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty7_type,
                *[id^="tfa_ma_qty7_type["] {
                    width: 160px !important;
                }
                #tfa_ma_qty7_type-D,
                *[id^="tfa_ma_qty7_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty7_type-L,
                label[id^="tfa_ma_qty7_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty7_amt,
                *[id^="tfa_ma_qty7_amt["] {
                    width: 168px !important;
                }
                #tfa_ma_qty7_amt-D,
                *[id^="tfa_ma_qty7_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty7_amt-L,
                label[id^="tfa_ma_qty7_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty8_quan,
                *[id^="tfa_ma_qty8_quan["] {
                    width: 168px !important;
                }
                #tfa_ma_qty8_quan-D,
                *[id^="tfa_ma_qty8_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty8_quan-L,
                label[id^="tfa_ma_qty8_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty8_on,
                *[id^="tfa_ma_qty8_on["] {
                    width: 168px !important;
                }
                #tfa_ma_qty8_on-D,
                *[id^="tfa_ma_qty8_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty8_on-L,
                label[id^="tfa_ma_qty8_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty8_type,
                *[id^="tfa_ma_qty8_type["] {
                    width: 160px !important;
                }
                #tfa_ma_qty8_type-D,
                *[id^="tfa_ma_qty8_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty8_type-L,
                label[id^="tfa_ma_qty8_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty8_amt,
                *[id^="tfa_ma_qty8_amt["] {
                    width: 168px !important;
                }
                #tfa_ma_qty8_amt-D,
                *[id^="tfa_ma_qty8_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty8_amt-L,
                label[id^="tfa_ma_qty8_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty9_quan,
                *[id^="tfa_ma_qty9_quan["] {
                    width: 168px !important;
                }
                #tfa_ma_qty9_quan-D,
                *[id^="tfa_ma_qty9_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty9_quan-L,
                label[id^="tfa_ma_qty9_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty9_on,
                *[id^="tfa_ma_qty9_on["] {
                    width: 168px !important;
                }
                #tfa_ma_qty9_on-D,
                *[id^="tfa_ma_qty9_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty9_on-L,
                label[id^="tfa_ma_qty9_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty9_type,
                *[id^="tfa_ma_qty9_type["] {
                    width: 160px !important;
                }
                #tfa_ma_qty9_type-D,
                *[id^="tfa_ma_qty9_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty9_type-L,
                label[id^="tfa_ma_qty9_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty9_amt,
                *[id^="tfa_ma_qty9_amt["] {
                    width: 168px !important;
                }
                #tfa_ma_qty9_amt-D,
                *[id^="tfa_ma_qty9_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty9_amt-L,
                label[id^="tfa_ma_qty9_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty10_quan,
                *[id^="tfa_ma_qty10_quan["] {
                    width: 168px !important;
                }
                #tfa_ma_qty10_quan-D,
                *[id^="tfa_ma_qty10_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty10_quan-L,
                label[id^="tfa_ma_qty10_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty10_on,
                *[id^="tfa_ma_qty10_on["] {
                    width: 168px !important;
                }
                #tfa_ma_qty10_on-D,
                *[id^="tfa_ma_qty10_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty10_on-L,
                label[id^="tfa_ma_qty10_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty10_type,
                *[id^="tfa_ma_qty10_type["] {
                    width: 160px !important;
                }
                #tfa_ma_qty10_type-D,
                *[id^="tfa_ma_qty10_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty10_type-L,
                label[id^="tfa_ma_qty10_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_ma_qty10_amt,
                *[id^="tfa_ma_qty10_amt["] {
                    width: 168px !important;
                }
                #tfa_ma_qty10_amt-D,
                *[id^="tfa_ma_qty10_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_ma_qty10_amt-L,
                label[id^="tfa_ma_qty10_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_change_type,
                *[id^="tfa_mn_change_type["] {
                    width: 354px !important;
                }
                #tfa_mn_change_type-D,
                *[id^="tfa_mn_change_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_price_bot,
                *[id^="tfa_mn_price_bot["] {
                    width: 143px !important;
                }
                #tfa_mn_price_bot-D,
                *[id^="tfa_mn_price_bot["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_price_case,
                *[id^="tfa_mn_price_case["] {
                    width: 143px !important;
                }
                #tfa_mn_price_case-D,
                *[id^="tfa_mn_price_case["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_std_cost,
                *[id^="tfa_mn_std_cost["] {
                    width: 143px !important;
                }
                #tfa_mn_std_cost-D,
                *[id^="tfa_mn_std_cost["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_purch_cost,
                *[id^="tfa_mn_purch_cost["] {
                    width: 143px !important;
                }
                #tfa_mn_purch_cost-D,
                *[id^="tfa_mn_purch_cost["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_fob,
                *[id^="tfa_mn_fob["] {
                    width: 143px !important;
                }
                #tfa_mn_fob-D,
                *[id^="tfa_mn_fob["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty1_quan,
                *[id^="tfa_mn_qty1_quan["] {
                    width: 168px !important;
                }
                #tfa_mn_qty1_quan-D,
                *[id^="tfa_mn_qty1_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty1_quan-L,
                label[id^="tfa_mn_qty1_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty1_on,
                *[id^="tfa_mn_qty1_on["] {
                    width: 168px !important;
                }
                #tfa_mn_qty1_on-D,
                *[id^="tfa_mn_qty1_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty1_on-L,
                label[id^="tfa_mn_qty1_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty1_type,
                *[id^="tfa_mn_qty1_type["] {
                    width: 160px !important;
                }
                #tfa_mn_qty1_type-D,
                *[id^="tfa_mn_qty1_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty1_type-L,
                label[id^="tfa_mn_qty1_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty1_amt,
                *[id^="tfa_mn_qty1_amt["] {
                    width: 168px !important;
                }
                #tfa_mn_qty1_amt-D,
                *[id^="tfa_mn_qty1_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty1_amt-L,
                label[id^="tfa_mn_qty1_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty2_quan,
                *[id^="tfa_mn_qty2_quan["] {
                    width: 168px !important;
                }
                #tfa_mn_qty2_quan-D,
                *[id^="tfa_mn_qty2_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty2_quan-L,
                label[id^="tfa_mn_qty2_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty2_on,
                *[id^="tfa_mn_qty2_on["] {
                    width: 168px !important;
                }
                #tfa_mn_qty2_on-D,
                *[id^="tfa_mn_qty2_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty2_on-L,
                label[id^="tfa_mn_qty2_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty2_type,
                *[id^="tfa_mn_qty2_type["] {
                    width: 160px !important;
                }
                #tfa_mn_qty2_type-D,
                *[id^="tfa_mn_qty2_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty2_type-L,
                label[id^="tfa_mn_qty2_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty2_amt,
                *[id^="tfa_mn_qty2_amt["] {
                    width: 168px !important;
                }
                #tfa_mn_qty2_amt-D,
                *[id^="tfa_mn_qty2_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty2_amt-L,
                label[id^="tfa_mn_qty2_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty3_quan,
                *[id^="tfa_mn_qty3_quan["] {
                    width: 168px !important;
                }
                #tfa_mn_qty3_quan-D,
                *[id^="tfa_mn_qty3_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty3_quan-L,
                label[id^="tfa_mn_qty3_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty3_on,
                *[id^="tfa_mn_qty3_on["] {
                    width: 168px !important;
                }
                #tfa_mn_qty3_on-D,
                *[id^="tfa_mn_qty3_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty3_on-L,
                label[id^="tfa_mn_qty3_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty3_type,
                *[id^="tfa_mn_qty3_type["] {
                    width: 160px !important;
                }
                #tfa_mn_qty3_type-D,
                *[id^="tfa_mn_qty3_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty3_type-L,
                label[id^="tfa_mn_qty3_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty3_amt,
                *[id^="tfa_mn_qty3_amt["] {
                    width: 168px !important;
                }
                #tfa_mn_qty3_amt-D,
                *[id^="tfa_mn_qty3_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty3_amt-L,
                label[id^="tfa_mn_qty3_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty4_quan,
                *[id^="tfa_mn_qty4_quan["] {
                    width: 168px !important;
                }
                #tfa_mn_qty4_quan-D,
                *[id^="tfa_mn_qty4_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty4_quan-L,
                label[id^="tfa_mn_qty4_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty4_on,
                *[id^="tfa_mn_qty4_on["] {
                    width: 168px !important;
                }
                #tfa_mn_qty4_on-D,
                *[id^="tfa_mn_qty4_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty4_on-L,
                label[id^="tfa_mn_qty4_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty4_type,
                *[id^="tfa_mn_qty4_type["] {
                    width: 160px !important;
                }
                #tfa_mn_qty4_type-D,
                *[id^="tfa_mn_qty4_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty4_type-L,
                label[id^="tfa_mn_qty4_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty4_amt,
                *[id^="tfa_mn_qty4_amt["] {
                    width: 168px !important;
                }
                #tfa_mn_qty4_amt-D,
                *[id^="tfa_mn_qty4_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty4_amt-L,
                label[id^="tfa_mn_qty4_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty5_quan,
                *[id^="tfa_mn_qty5_quan["] {
                    width: 168px !important;
                }
                #tfa_mn_qty5_quan-D,
                *[id^="tfa_mn_qty5_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty5_quan-L,
                label[id^="tfa_mn_qty5_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty5_on,
                *[id^="tfa_mn_qty5_on["] {
                    width: 168px !important;
                }
                #tfa_mn_qty5_on-D,
                *[id^="tfa_mn_qty5_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty5_on-L,
                label[id^="tfa_mn_qty5_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty5_type,
                *[id^="tfa_mn_qty5_type["] {
                    width: 160px !important;
                }
                #tfa_mn_qty5_type-D,
                *[id^="tfa_mn_qty5_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty5_type-L,
                label[id^="tfa_mn_qty5_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty5_amt,
                *[id^="tfa_mn_qty5_amt["] {
                    width: 168px !important;
                }
                #tfa_mn_qty5_amt-D,
                *[id^="tfa_mn_qty5_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty5_amt-L,
                label[id^="tfa_mn_qty5_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty6_quan,
                *[id^="tfa_mn_qty6_quan["] {
                    width: 168px !important;
                }
                #tfa_mn_qty6_quan-D,
                *[id^="tfa_mn_qty6_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty6_quan-L,
                label[id^="tfa_mn_qty6_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty6_on,
                *[id^="tfa_mn_qty6_on["] {
                    width: 168px !important;
                }
                #tfa_mn_qty6_on-D,
                *[id^="tfa_mn_qty6_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty6_on-L,
                label[id^="tfa_mn_qty6_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty6_type,
                *[id^="tfa_mn_qty6_type["] {
                    width: 160px !important;
                }
                #tfa_mn_qty6_type-D,
                *[id^="tfa_mn_qty6_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty6_type-L,
                label[id^="tfa_mn_qty6_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty6_amt,
                *[id^="tfa_mn_qty6_amt["] {
                    width: 168px !important;
                }
                #tfa_mn_qty6_amt-D,
                *[id^="tfa_mn_qty6_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty6_amt-L,
                label[id^="tfa_mn_qty6_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty7_quan,
                *[id^="tfa_mn_qty7_quan["] {
                    width: 168px !important;
                }
                #tfa_mn_qty7_quan-D,
                *[id^="tfa_mn_qty7_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty7_quan-L,
                label[id^="tfa_mn_qty7_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty7_on,
                *[id^="tfa_mn_qty7_on["] {
                    width: 168px !important;
                }
                #tfa_mn_qty7_on-D,
                *[id^="tfa_mn_qty7_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty7_on-L,
                label[id^="tfa_mn_qty7_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty7_type,
                *[id^="tfa_mn_qty7_type["] {
                    width: 160px !important;
                }
                #tfa_mn_qty7_type-D,
                *[id^="tfa_mn_qty7_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty7_type-L,
                label[id^="tfa_mn_qty7_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty7_amt,
                *[id^="tfa_mn_qty7_amt["] {
                    width: 168px !important;
                }
                #tfa_mn_qty7_amt-D,
                *[id^="tfa_mn_qty7_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty7_amt-L,
                label[id^="tfa_mn_qty7_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty8_quan,
                *[id^="tfa_mn_qty8_quan["] {
                    width: 168px !important;
                }
                #tfa_mn_qty8_quan-D,
                *[id^="tfa_mn_qty8_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty8_quan-L,
                label[id^="tfa_mn_qty8_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty8_on,
                *[id^="tfa_mn_qty8_on["] {
                    width: 168px !important;
                }
                #tfa_mn_qty8_on-D,
                *[id^="tfa_mn_qty8_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty8_on-L,
                label[id^="tfa_mn_qty8_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty8_type,
                *[id^="tfa_mn_qty8_type["] {
                    width: 160px !important;
                }
                #tfa_mn_qty8_type-D,
                *[id^="tfa_mn_qty8_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty8_type-L,
                label[id^="tfa_mn_qty8_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty8_amt,
                *[id^="tfa_mn_qty8_amt["] {
                    width: 168px !important;
                }
                #tfa_mn_qty8_amt-D,
                *[id^="tfa_mn_qty8_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty8_amt-L,
                label[id^="tfa_mn_qty8_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty9_quan,
                *[id^="tfa_mn_qty9_quan["] {
                    width: 168px !important;
                }
                #tfa_mn_qty9_quan-D,
                *[id^="tfa_mn_qty9_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty9_quan-L,
                label[id^="tfa_mn_qty9_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty9_on,
                *[id^="tfa_mn_qty9_on["] {
                    width: 168px !important;
                }
                #tfa_mn_qty9_on-D,
                *[id^="tfa_mn_qty9_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty9_on-L,
                label[id^="tfa_mn_qty9_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty9_type,
                *[id^="tfa_mn_qty9_type["] {
                    width: 160px !important;
                }
                #tfa_mn_qty9_type-D,
                *[id^="tfa_mn_qty9_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty9_type-L,
                label[id^="tfa_mn_qty9_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty9_amt,
                *[id^="tfa_mn_qty9_amt["] {
                    width: 168px !important;
                }
                #tfa_mn_qty9_amt-D,
                *[id^="tfa_mn_qty9_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty9_amt-L,
                label[id^="tfa_mn_qty9_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty10_quan,
                *[id^="tfa_mn_qty10_quan["] {
                    width: 168px !important;
                }
                #tfa_mn_qty10_quan-D,
                *[id^="tfa_mn_qty10_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty10_quan-L,
                label[id^="tfa_mn_qty10_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty10_on,
                *[id^="tfa_mn_qty10_on["] {
                    width: 168px !important;
                }
                #tfa_mn_qty10_on-D,
                *[id^="tfa_mn_qty10_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty10_on-L,
                label[id^="tfa_mn_qty10_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty10_type,
                *[id^="tfa_mn_qty10_type["] {
                    width: 160px !important;
                }
                #tfa_mn_qty10_type-D,
                *[id^="tfa_mn_qty10_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty10_type-L,
                label[id^="tfa_mn_qty10_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_mn_qty10_amt,
                *[id^="tfa_mn_qty10_amt["] {
                    width: 168px !important;
                }
                #tfa_mn_qty10_amt-D,
                *[id^="tfa_mn_qty10_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_mn_qty10_amt-L,
                label[id^="tfa_mn_qty10_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_change_type,
                *[id^="tfa_nj_change_type["] {
                    width: 354px !important;
                }
                #tfa_nj_change_type-D,
                *[id^="tfa_nj_change_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_price_bot,
                *[id^="tfa_nj_price_bot["] {
                    width: 143px !important;
                }
                #tfa_nj_price_bot-D,
                *[id^="tfa_nj_price_bot["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_price_case,
                *[id^="tfa_nj_price_case["] {
                    width: 143px !important;
                }
                #tfa_nj_price_case-D,
                *[id^="tfa_nj_price_case["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_std_cost,
                *[id^="tfa_nj_std_cost["] {
                    width: 143px !important;
                }
                #tfa_nj_std_cost-D,
                *[id^="tfa_nj_std_cost["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_purch_cost,
                *[id^="tfa_nj_purch_cost["] {
                    width: 143px !important;
                }
                #tfa_nj_purch_cost-D,
                *[id^="tfa_nj_purch_cost["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_fob,
                *[id^="tfa_nj_fob["] {
                    width: 143px !important;
                }
                #tfa_nj_fob-D,
                *[id^="tfa_nj_fob["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty1_quan,
                *[id^="tfa_nj_qty1_quan["] {
                    width: 168px !important;
                }
                #tfa_nj_qty1_quan-D,
                *[id^="tfa_nj_qty1_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty1_quan-L,
                label[id^="tfa_nj_qty1_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty1_on,
                *[id^="tfa_nj_qty1_on["] {
                    width: 168px !important;
                }
                #tfa_nj_qty1_on-D,
                *[id^="tfa_nj_qty1_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty1_on-L,
                label[id^="tfa_nj_qty1_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty1_type,
                *[id^="tfa_nj_qty1_type["] {
                    width: 160px !important;
                }
                #tfa_nj_qty1_type-D,
                *[id^="tfa_nj_qty1_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty1_type-L,
                label[id^="tfa_nj_qty1_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty1_amt,
                *[id^="tfa_nj_qty1_amt["] {
                    width: 168px !important;
                }
                #tfa_nj_qty1_amt-D,
                *[id^="tfa_nj_qty1_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty1_amt-L,
                label[id^="tfa_nj_qty1_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty2_quan,
                *[id^="tfa_nj_qty2_quan["] {
                    width: 168px !important;
                }
                #tfa_nj_qty2_quan-D,
                *[id^="tfa_nj_qty2_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty2_quan-L,
                label[id^="tfa_nj_qty2_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty2_on,
                *[id^="tfa_nj_qty2_on["] {
                    width: 168px !important;
                }
                #tfa_nj_qty2_on-D,
                *[id^="tfa_nj_qty2_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty2_on-L,
                label[id^="tfa_nj_qty2_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty2_type,
                *[id^="tfa_nj_qty2_type["] {
                    width: 160px !important;
                }
                #tfa_nj_qty2_type-D,
                *[id^="tfa_nj_qty2_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty2_type-L,
                label[id^="tfa_nj_qty2_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty2_amt,
                *[id^="tfa_nj_qty2_amt["] {
                    width: 168px !important;
                }
                #tfa_nj_qty2_amt-D,
                *[id^="tfa_nj_qty2_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty2_amt-L,
                label[id^="tfa_nj_qty2_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty3_quan,
                *[id^="tfa_nj_qty3_quan["] {
                    width: 168px !important;
                }
                #tfa_nj_qty3_quan-D,
                *[id^="tfa_nj_qty3_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty3_quan-L,
                label[id^="tfa_nj_qty3_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty3_on,
                *[id^="tfa_nj_qty3_on["] {
                    width: 168px !important;
                }
                #tfa_nj_qty3_on-D,
                *[id^="tfa_nj_qty3_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty3_on-L,
                label[id^="tfa_nj_qty3_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty3_type,
                *[id^="tfa_nj_qty3_type["] {
                    width: 160px !important;
                }
                #tfa_nj_qty3_type-D,
                *[id^="tfa_nj_qty3_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty3_type-L,
                label[id^="tfa_nj_qty3_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty3_amt,
                *[id^="tfa_nj_qty3_amt["] {
                    width: 168px !important;
                }
                #tfa_nj_qty3_amt-D,
                *[id^="tfa_nj_qty3_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty3_amt-L,
                label[id^="tfa_nj_qty3_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty4_quan,
                *[id^="tfa_nj_qty4_quan["] {
                    width: 168px !important;
                }
                #tfa_nj_qty4_quan-D,
                *[id^="tfa_nj_qty4_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty4_quan-L,
                label[id^="tfa_nj_qty4_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty4_on,
                *[id^="tfa_nj_qty4_on["] {
                    width: 168px !important;
                }
                #tfa_nj_qty4_on-D,
                *[id^="tfa_nj_qty4_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty4_on-L,
                label[id^="tfa_nj_qty4_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty4_type,
                *[id^="tfa_nj_qty4_type["] {
                    width: 160px !important;
                }
                #tfa_nj_qty4_type-D,
                *[id^="tfa_nj_qty4_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty4_type-L,
                label[id^="tfa_nj_qty4_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty4_amt,
                *[id^="tfa_nj_qty4_amt["] {
                    width: 168px !important;
                }
                #tfa_nj_qty4_amt-D,
                *[id^="tfa_nj_qty4_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty4_amt-L,
                label[id^="tfa_nj_qty4_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty5_quan,
                *[id^="tfa_nj_qty5_quan["] {
                    width: 168px !important;
                }
                #tfa_nj_qty5_quan-D,
                *[id^="tfa_nj_qty5_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty5_quan-L,
                label[id^="tfa_nj_qty5_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty5_on,
                *[id^="tfa_nj_qty5_on["] {
                    width: 168px !important;
                }
                #tfa_nj_qty5_on-D,
                *[id^="tfa_nj_qty5_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty5_on-L,
                label[id^="tfa_nj_qty5_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty5_type,
                *[id^="tfa_nj_qty5_type["] {
                    width: 160px !important;
                }
                #tfa_nj_qty5_type-D,
                *[id^="tfa_nj_qty5_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty5_type-L,
                label[id^="tfa_nj_qty5_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty5_amt,
                *[id^="tfa_nj_qty5_amt["] {
                    width: 168px !important;
                }
                #tfa_nj_qty5_amt-D,
                *[id^="tfa_nj_qty5_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty5_amt-L,
                label[id^="tfa_nj_qty5_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty6_quan,
                *[id^="tfa_nj_qty6_quan["] {
                    width: 168px !important;
                }
                #tfa_nj_qty6_quan-D,
                *[id^="tfa_nj_qty6_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty6_quan-L,
                label[id^="tfa_nj_qty6_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty6_on,
                *[id^="tfa_nj_qty6_on["] {
                    width: 168px !important;
                }
                #tfa_nj_qty6_on-D,
                *[id^="tfa_nj_qty6_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty6_on-L,
                label[id^="tfa_nj_qty6_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty6_type,
                *[id^="tfa_nj_qty6_type["] {
                    width: 160px !important;
                }
                #tfa_nj_qty6_type-D,
                *[id^="tfa_nj_qty6_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty6_type-L,
                label[id^="tfa_nj_qty6_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty6_amt,
                *[id^="tfa_nj_qty6_amt["] {
                    width: 168px !important;
                }
                #tfa_nj_qty6_amt-D,
                *[id^="tfa_nj_qty6_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty6_amt-L,
                label[id^="tfa_nj_qty6_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty7_quan,
                *[id^="tfa_nj_qty7_quan["] {
                    width: 168px !important;
                }
                #tfa_nj_qty7_quan-D,
                *[id^="tfa_nj_qty7_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty7_quan-L,
                label[id^="tfa_nj_qty7_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty7_on,
                *[id^="tfa_nj_qty7_on["] {
                    width: 168px !important;
                }
                #tfa_nj_qty7_on-D,
                *[id^="tfa_nj_qty7_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty7_on-L,
                label[id^="tfa_nj_qty7_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty7_type,
                *[id^="tfa_nj_qty7_type["] {
                    width: 160px !important;
                }
                #tfa_nj_qty7_type-D,
                *[id^="tfa_nj_qty7_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty7_type-L,
                label[id^="tfa_nj_qty7_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty7_amt,
                *[id^="tfa_nj_qty7_amt["] {
                    width: 168px !important;
                }
                #tfa_nj_qty7_amt-D,
                *[id^="tfa_nj_qty7_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty7_amt-L,
                label[id^="tfa_nj_qty7_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty8_quan,
                *[id^="tfa_nj_qty8_quan["] {
                    width: 168px !important;
                }
                #tfa_nj_qty8_quan-D,
                *[id^="tfa_nj_qty8_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty8_quan-L,
                label[id^="tfa_nj_qty8_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty8_on,
                *[id^="tfa_nj_qty8_on["] {
                    width: 168px !important;
                }
                #tfa_nj_qty8_on-D,
                *[id^="tfa_nj_qty8_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty8_on-L,
                label[id^="tfa_nj_qty8_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty8_type,
                *[id^="tfa_nj_qty8_type["] {
                    width: 160px !important;
                }
                #tfa_nj_qty8_type-D,
                *[id^="tfa_nj_qty8_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty8_type-L,
                label[id^="tfa_nj_qty8_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty8_amt,
                *[id^="tfa_nj_qty8_amt["] {
                    width: 168px !important;
                }
                #tfa_nj_qty8_amt-D,
                *[id^="tfa_nj_qty8_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty8_amt-L,
                label[id^="tfa_nj_qty8_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty9_quan,
                *[id^="tfa_nj_qty9_quan["] {
                    width: 168px !important;
                }
                #tfa_nj_qty9_quan-D,
                *[id^="tfa_nj_qty9_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty9_quan-L,
                label[id^="tfa_nj_qty9_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty9_on,
                *[id^="tfa_nj_qty9_on["] {
                    width: 168px !important;
                }
                #tfa_nj_qty9_on-D,
                *[id^="tfa_nj_qty9_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty9_on-L,
                label[id^="tfa_nj_qty9_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty9_type,
                *[id^="tfa_nj_qty9_type["] {
                    width: 160px !important;
                }
                #tfa_nj_qty9_type-D,
                *[id^="tfa_nj_qty9_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty9_type-L,
                label[id^="tfa_nj_qty9_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty9_amt,
                *[id^="tfa_nj_qty9_amt["] {
                    width: 168px !important;
                }
                #tfa_nj_qty9_amt-D,
                *[id^="tfa_nj_qty9_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty9_amt-L,
                label[id^="tfa_nj_qty9_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty10_quan,
                *[id^="tfa_nj_qty10_quan["] {
                    width: 168px !important;
                }
                #tfa_nj_qty10_quan-D,
                *[id^="tfa_nj_qty10_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty10_quan-L,
                label[id^="tfa_nj_qty10_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty10_on,
                *[id^="tfa_nj_qty10_on["] {
                    width: 168px !important;
                }
                #tfa_nj_qty10_on-D,
                *[id^="tfa_nj_qty10_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty10_on-L,
                label[id^="tfa_nj_qty10_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty10_type,
                *[id^="tfa_nj_qty10_type["] {
                    width: 160px !important;
                }
                #tfa_nj_qty10_type-D,
                *[id^="tfa_nj_qty10_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty10_type-L,
                label[id^="tfa_nj_qty10_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nj_qty10_amt,
                *[id^="tfa_nj_qty10_amt["] {
                    width: 168px !important;
                }
                #tfa_nj_qty10_amt-D,
                *[id^="tfa_nj_qty10_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nj_qty10_amt-L,
                label[id^="tfa_nj_qty10_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_change_type,
                *[id^="tfa_nyr_change_type["] {
                    width: 354px !important;
                }
                #tfa_nyr_change_type-D,
                *[id^="tfa_nyr_change_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_price_bot,
                *[id^="tfa_nyr_price_bot["] {
                    width: 143px !important;
                }
                #tfa_nyr_price_bot-D,
                *[id^="tfa_nyr_price_bot["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_price_case,
                *[id^="tfa_nyr_price_case["] {
                    width: 143px !important;
                }
                #tfa_nyr_price_case-D,
                *[id^="tfa_nyr_price_case["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_std_cost,
                *[id^="tfa_nyr_std_cost["] {
                    width: 143px !important;
                }
                #tfa_nyr_std_cost-D,
                *[id^="tfa_nyr_std_cost["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_purch_cost,
                *[id^="tfa_nyr_purch_cost["] {
                    width: 143px !important;
                }
                #tfa_nyr_purch_cost-D,
                *[id^="tfa_nyr_purch_cost["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_fob,
                *[id^="tfa_nyr_fob["] {
                    width: 143px !important;
                }
                #tfa_nyr_fob-D,
                *[id^="tfa_nyr_fob["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty1_quan,
                *[id^="tfa_nyr_qty1_quan["] {
                    width: 168px !important;
                }
                #tfa_nyr_qty1_quan-D,
                *[id^="tfa_nyr_qty1_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty1_quan-L,
                label[id^="tfa_nyr_qty1_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty1_on,
                *[id^="tfa_nyr_qty1_on["] {
                    width: 168px !important;
                }
                #tfa_nyr_qty1_on-D,
                *[id^="tfa_nyr_qty1_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty1_on-L,
                label[id^="tfa_nyr_qty1_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty1_type,
                *[id^="tfa_nyr_qty1_type["] {
                    width: 160px !important;
                }
                #tfa_nyr_qty1_type-D,
                *[id^="tfa_nyr_qty1_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty1_type-L,
                label[id^="tfa_nyr_qty1_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty1_amt,
                *[id^="tfa_nyr_qty1_amt["] {
                    width: 168px !important;
                }
                #tfa_nyr_qty1_amt-D,
                *[id^="tfa_nyr_qty1_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty1_amt-L,
                label[id^="tfa_nyr_qty1_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty2_quan,
                *[id^="tfa_nyr_qty2_quan["] {
                    width: 168px !important;
                }
                #tfa_nyr_qty2_quan-D,
                *[id^="tfa_nyr_qty2_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty2_quan-L,
                label[id^="tfa_nyr_qty2_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty2_on,
                *[id^="tfa_nyr_qty2_on["] {
                    width: 168px !important;
                }
                #tfa_nyr_qty2_on-D,
                *[id^="tfa_nyr_qty2_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty2_on-L,
                label[id^="tfa_nyr_qty2_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty2_type,
                *[id^="tfa_nyr_qty2_type["] {
                    width: 160px !important;
                }
                #tfa_nyr_qty2_type-D,
                *[id^="tfa_nyr_qty2_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty2_type-L,
                label[id^="tfa_nyr_qty2_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty2_amt,
                *[id^="tfa_nyr_qty2_amt["] {
                    width: 168px !important;
                }
                #tfa_nyr_qty2_amt-D,
                *[id^="tfa_nyr_qty2_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty2_amt-L,
                label[id^="tfa_nyr_qty2_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty3_quan,
                *[id^="tfa_nyr_qty3_quan["] {
                    width: 168px !important;
                }
                #tfa_nyr_qty3_quan-D,
                *[id^="tfa_nyr_qty3_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty3_quan-L,
                label[id^="tfa_nyr_qty3_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty3_on,
                *[id^="tfa_nyr_qty3_on["] {
                    width: 168px !important;
                }
                #tfa_nyr_qty3_on-D,
                *[id^="tfa_nyr_qty3_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty3_on-L,
                label[id^="tfa_nyr_qty3_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty3_type,
                *[id^="tfa_nyr_qty3_type["] {
                    width: 160px !important;
                }
                #tfa_nyr_qty3_type-D,
                *[id^="tfa_nyr_qty3_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty3_type-L,
                label[id^="tfa_nyr_qty3_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty3_amt,
                *[id^="tfa_nyr_qty3_amt["] {
                    width: 168px !important;
                }
                #tfa_nyr_qty3_amt-D,
                *[id^="tfa_nyr_qty3_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty3_amt-L,
                label[id^="tfa_nyr_qty3_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty4_quan,
                *[id^="tfa_nyr_qty4_quan["] {
                    width: 168px !important;
                }
                #tfa_nyr_qty4_quan-D,
                *[id^="tfa_nyr_qty4_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty4_quan-L,
                label[id^="tfa_nyr_qty4_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty4_on,
                *[id^="tfa_nyr_qty4_on["] {
                    width: 168px !important;
                }
                #tfa_nyr_qty4_on-D,
                *[id^="tfa_nyr_qty4_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty4_on-L,
                label[id^="tfa_nyr_qty4_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty4_type,
                *[id^="tfa_nyr_qty4_type["] {
                    width: 160px !important;
                }
                #tfa_nyr_qty4_type-D,
                *[id^="tfa_nyr_qty4_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty4_type-L,
                label[id^="tfa_nyr_qty4_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty4_amt,
                *[id^="tfa_nyr_qty4_amt["] {
                    width: 168px !important;
                }
                #tfa_nyr_qty4_amt-D,
                *[id^="tfa_nyr_qty4_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty4_amt-L,
                label[id^="tfa_nyr_qty4_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty5_quan,
                *[id^="tfa_nyr_qty5_quan["] {
                    width: 168px !important;
                }
                #tfa_nyr_qty5_quan-D,
                *[id^="tfa_nyr_qty5_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty5_quan-L,
                label[id^="tfa_nyr_qty5_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty5_on,
                *[id^="tfa_nyr_qty5_on["] {
                    width: 168px !important;
                }
                #tfa_nyr_qty5_on-D,
                *[id^="tfa_nyr_qty5_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty5_on-L,
                label[id^="tfa_nyr_qty5_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty5_type,
                *[id^="tfa_nyr_qty5_type["] {
                    width: 160px !important;
                }
                #tfa_nyr_qty5_type-D,
                *[id^="tfa_nyr_qty5_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty5_type-L,
                label[id^="tfa_nyr_qty5_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty5_amt,
                *[id^="tfa_nyr_qty5_amt["] {
                    width: 168px !important;
                }
                #tfa_nyr_qty5_amt-D,
                *[id^="tfa_nyr_qty5_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty5_amt-L,
                label[id^="tfa_nyr_qty5_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty6_quan,
                *[id^="tfa_nyr_qty6_quan["] {
                    width: 168px !important;
                }
                #tfa_nyr_qty6_quan-D,
                *[id^="tfa_nyr_qty6_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty6_quan-L,
                label[id^="tfa_nyr_qty6_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty6_on,
                *[id^="tfa_nyr_qty6_on["] {
                    width: 168px !important;
                }
                #tfa_nyr_qty6_on-D,
                *[id^="tfa_nyr_qty6_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty6_on-L,
                label[id^="tfa_nyr_qty6_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty6_type,
                *[id^="tfa_nyr_qty6_type["] {
                    width: 160px !important;
                }
                #tfa_nyr_qty6_type-D,
                *[id^="tfa_nyr_qty6_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty6_type-L,
                label[id^="tfa_nyr_qty6_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty6_amt,
                *[id^="tfa_nyr_qty6_amt["] {
                    width: 168px !important;
                }
                #tfa_nyr_qty6_amt-D,
                *[id^="tfa_nyr_qty6_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty6_amt-L,
                label[id^="tfa_nyr_qty6_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty7_quan,
                *[id^="tfa_nyr_qty7_quan["] {
                    width: 168px !important;
                }
                #tfa_nyr_qty7_quan-D,
                *[id^="tfa_nyr_qty7_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty7_quan-L,
                label[id^="tfa_nyr_qty7_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty7_on,
                *[id^="tfa_nyr_qty7_on["] {
                    width: 168px !important;
                }
                #tfa_nyr_qty7_on-D,
                *[id^="tfa_nyr_qty7_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty7_on-L,
                label[id^="tfa_nyr_qty7_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty7_type,
                *[id^="tfa_nyr_qty7_type["] {
                    width: 160px !important;
                }
                #tfa_nyr_qty7_type-D,
                *[id^="tfa_nyr_qty7_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty7_type-L,
                label[id^="tfa_nyr_qty7_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty7_amt,
                *[id^="tfa_nyr_qty7_amt["] {
                    width: 168px !important;
                }
                #tfa_nyr_qty7_amt-D,
                *[id^="tfa_nyr_qty7_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty7_amt-L,
                label[id^="tfa_nyr_qty7_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty8_quan,
                *[id^="tfa_nyr_qty8_quan["] {
                    width: 168px !important;
                }
                #tfa_nyr_qty8_quan-D,
                *[id^="tfa_nyr_qty8_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty8_quan-L,
                label[id^="tfa_nyr_qty8_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty8_on,
                *[id^="tfa_nyr_qty8_on["] {
                    width: 168px !important;
                }
                #tfa_nyr_qty8_on-D,
                *[id^="tfa_nyr_qty8_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty8_on-L,
                label[id^="tfa_nyr_qty8_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty8_type,
                *[id^="tfa_nyr_qty8_type["] {
                    width: 160px !important;
                }
                #tfa_nyr_qty8_type-D,
                *[id^="tfa_nyr_qty8_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty8_type-L,
                label[id^="tfa_nyr_qty8_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty8_amt,
                *[id^="tfa_nyr_qty8_amt["] {
                    width: 168px !important;
                }
                #tfa_nyr_qty8_amt-D,
                *[id^="tfa_nyr_qty8_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty8_amt-L,
                label[id^="tfa_nyr_qty8_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty9_quan,
                *[id^="tfa_nyr_qty9_quan["] {
                    width: 168px !important;
                }
                #tfa_nyr_qty9_quan-D,
                *[id^="tfa_nyr_qty9_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty9_quan-L,
                label[id^="tfa_nyr_qty9_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty9_on,
                *[id^="tfa_nyr_qty9_on["] {
                    width: 168px !important;
                }
                #tfa_nyr_qty9_on-D,
                *[id^="tfa_nyr_qty9_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty9_on-L,
                label[id^="tfa_nyr_qty9_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty9_type,
                *[id^="tfa_nyr_qty9_type["] {
                    width: 160px !important;
                }
                #tfa_nyr_qty9_type-D,
                *[id^="tfa_nyr_qty9_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty9_type-L,
                label[id^="tfa_nyr_qty9_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty9_amt,
                *[id^="tfa_nyr_qty9_amt["] {
                    width: 168px !important;
                }
                #tfa_nyr_qty9_amt-D,
                *[id^="tfa_nyr_qty9_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty9_amt-L,
                label[id^="tfa_nyr_qty9_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty10_quan,
                *[id^="tfa_nyr_qty10_quan["] {
                    width: 168px !important;
                }
                #tfa_nyr_qty10_quan-D,
                *[id^="tfa_nyr_qty10_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty10_quan-L,
                label[id^="tfa_nyr_qty10_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty10_on,
                *[id^="tfa_nyr_qty10_on["] {
                    width: 168px !important;
                }
                #tfa_nyr_qty10_on-D,
                *[id^="tfa_nyr_qty10_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty10_on-L,
                label[id^="tfa_nyr_qty10_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty10_type,
                *[id^="tfa_nyr_qty10_type["] {
                    width: 160px !important;
                }
                #tfa_nyr_qty10_type-D,
                *[id^="tfa_nyr_qty10_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty10_type-L,
                label[id^="tfa_nyr_qty10_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyr_qty10_amt,
                *[id^="tfa_nyr_qty10_amt["] {
                    width: 168px !important;
                }
                #tfa_nyr_qty10_amt-D,
                *[id^="tfa_nyr_qty10_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyr_qty10_amt-L,
                label[id^="tfa_nyr_qty10_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_change_type,
                *[id^="tfa_nyw_change_type["] {
                    width: 354px !important;
                }
                #tfa_nyw_change_type-D,
                *[id^="tfa_nyw_change_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_price_bot,
                *[id^="tfa_nyw_price_bot["] {
                    width: 143px !important;
                }
                #tfa_nyw_price_bot-D,
                *[id^="tfa_nyw_price_bot["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_price_case,
                *[id^="tfa_nyw_price_case["] {
                    width: 143px !important;
                }
                #tfa_nyw_price_case-D,
                *[id^="tfa_nyw_price_case["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_std_cost,
                *[id^="tfa_nyw_std_cost["] {
                    width: 143px !important;
                }
                #tfa_nyw_std_cost-D,
                *[id^="tfa_nyw_std_cost["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_purch_cost,
                *[id^="tfa_nyw_purch_cost["] {
                    width: 143px !important;
                }
                #tfa_nyw_purch_cost-D,
                *[id^="tfa_nyw_purch_cost["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_fob,
                *[id^="tfa_nyw_fob["] {
                    width: 143px !important;
                }
                #tfa_nyw_fob-D,
                *[id^="tfa_nyw_fob["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty1_quan,
                *[id^="tfa_nyw_qty1_quan["] {
                    width: 168px !important;
                }
                #tfa_nyw_qty1_quan-D,
                *[id^="tfa_nyw_qty1_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty1_quan-L,
                label[id^="tfa_nyw_qty1_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty1_on,
                *[id^="tfa_nyw_qty1_on["] {
                    width: 168px !important;
                }
                #tfa_nyw_qty1_on-D,
                *[id^="tfa_nyw_qty1_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty1_on-L,
                label[id^="tfa_nyw_qty1_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty1_type,
                *[id^="tfa_nyw_qty1_type["] {
                    width: 160px !important;
                }
                #tfa_nyw_qty1_type-D,
                *[id^="tfa_nyw_qty1_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty1_type-L,
                label[id^="tfa_nyw_qty1_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty1_amt,
                *[id^="tfa_nyw_qty1_amt["] {
                    width: 168px !important;
                }
                #tfa_nyw_qty1_amt-D,
                *[id^="tfa_nyw_qty1_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty1_amt-L,
                label[id^="tfa_nyw_qty1_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty2_quan,
                *[id^="tfa_nyw_qty2_quan["] {
                    width: 168px !important;
                }
                #tfa_nyw_qty2_quan-D,
                *[id^="tfa_nyw_qty2_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty2_quan-L,
                label[id^="tfa_nyw_qty2_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty2_on,
                *[id^="tfa_nyw_qty2_on["] {
                    width: 168px !important;
                }
                #tfa_nyw_qty2_on-D,
                *[id^="tfa_nyw_qty2_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty2_on-L,
                label[id^="tfa_nyw_qty2_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty2_type,
                *[id^="tfa_nyw_qty2_type["] {
                    width: 160px !important;
                }
                #tfa_nyw_qty2_type-D,
                *[id^="tfa_nyw_qty2_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty2_type-L,
                label[id^="tfa_nyw_qty2_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty2_amt,
                *[id^="tfa_nyw_qty2_amt["] {
                    width: 168px !important;
                }
                #tfa_nyw_qty2_amt-D,
                *[id^="tfa_nyw_qty2_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty2_amt-L,
                label[id^="tfa_nyw_qty2_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty3_quan,
                *[id^="tfa_nyw_qty3_quan["] {
                    width: 168px !important;
                }
                #tfa_nyw_qty3_quan-D,
                *[id^="tfa_nyw_qty3_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty3_quan-L,
                label[id^="tfa_nyw_qty3_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty3_on,
                *[id^="tfa_nyw_qty3_on["] {
                    width: 168px !important;
                }
                #tfa_nyw_qty3_on-D,
                *[id^="tfa_nyw_qty3_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty3_on-L,
                label[id^="tfa_nyw_qty3_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty3_type,
                *[id^="tfa_nyw_qty3_type["] {
                    width: 160px !important;
                }
                #tfa_nyw_qty3_type-D,
                *[id^="tfa_nyw_qty3_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty3_type-L,
                label[id^="tfa_nyw_qty3_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty3_amt,
                *[id^="tfa_nyw_qty3_amt["] {
                    width: 168px !important;
                }
                #tfa_nyw_qty3_amt-D,
                *[id^="tfa_nyw_qty3_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty3_amt-L,
                label[id^="tfa_nyw_qty3_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty4_quan,
                *[id^="tfa_nyw_qty4_quan["] {
                    width: 168px !important;
                }
                #tfa_nyw_qty4_quan-D,
                *[id^="tfa_nyw_qty4_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty4_quan-L,
                label[id^="tfa_nyw_qty4_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty4_on,
                *[id^="tfa_nyw_qty4_on["] {
                    width: 168px !important;
                }
                #tfa_nyw_qty4_on-D,
                *[id^="tfa_nyw_qty4_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty4_on-L,
                label[id^="tfa_nyw_qty4_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty4_type,
                *[id^="tfa_nyw_qty4_type["] {
                    width: 160px !important;
                }
                #tfa_nyw_qty4_type-D,
                *[id^="tfa_nyw_qty4_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty4_type-L,
                label[id^="tfa_nyw_qty4_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty4_amt,
                *[id^="tfa_nyw_qty4_amt["] {
                    width: 168px !important;
                }
                #tfa_nyw_qty4_amt-D,
                *[id^="tfa_nyw_qty4_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty4_amt-L,
                label[id^="tfa_nyw_qty4_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty5_quan,
                *[id^="tfa_nyw_qty5_quan["] {
                    width: 168px !important;
                }
                #tfa_nyw_qty5_quan-D,
                *[id^="tfa_nyw_qty5_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty5_quan-L,
                label[id^="tfa_nyw_qty5_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty5_on,
                *[id^="tfa_nyw_qty5_on["] {
                    width: 168px !important;
                }
                #tfa_nyw_qty5_on-D,
                *[id^="tfa_nyw_qty5_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty5_on-L,
                label[id^="tfa_nyw_qty5_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty5_type,
                *[id^="tfa_nyw_qty5_type["] {
                    width: 160px !important;
                }
                #tfa_nyw_qty5_type-D,
                *[id^="tfa_nyw_qty5_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty5_type-L,
                label[id^="tfa_nyw_qty5_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty5_amt,
                *[id^="tfa_nyw_qty5_amt["] {
                    width: 168px !important;
                }
                #tfa_nyw_qty5_amt-D,
                *[id^="tfa_nyw_qty5_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty5_amt-L,
                label[id^="tfa_nyw_qty5_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty6_quan,
                *[id^="tfa_nyw_qty6_quan["] {
                    width: 168px !important;
                }
                #tfa_nyw_qty6_quan-D,
                *[id^="tfa_nyw_qty6_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty6_quan-L,
                label[id^="tfa_nyw_qty6_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty6_on,
                *[id^="tfa_nyw_qty6_on["] {
                    width: 168px !important;
                }
                #tfa_nyw_qty6_on-D,
                *[id^="tfa_nyw_qty6_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty6_on-L,
                label[id^="tfa_nyw_qty6_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty6_type,
                *[id^="tfa_nyw_qty6_type["] {
                    width: 160px !important;
                }
                #tfa_nyw_qty6_type-D,
                *[id^="tfa_nyw_qty6_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty6_type-L,
                label[id^="tfa_nyw_qty6_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty6_amt,
                *[id^="tfa_nyw_qty6_amt["] {
                    width: 168px !important;
                }
                #tfa_nyw_qty6_amt-D,
                *[id^="tfa_nyw_qty6_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty6_amt-L,
                label[id^="tfa_nyw_qty6_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty7_quan,
                *[id^="tfa_nyw_qty7_quan["] {
                    width: 168px !important;
                }
                #tfa_nyw_qty7_quan-D,
                *[id^="tfa_nyw_qty7_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty7_quan-L,
                label[id^="tfa_nyw_qty7_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty7_on,
                *[id^="tfa_nyw_qty7_on["] {
                    width: 168px !important;
                }
                #tfa_nyw_qty7_on-D,
                *[id^="tfa_nyw_qty7_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty7_on-L,
                label[id^="tfa_nyw_qty7_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty7_type,
                *[id^="tfa_nyw_qty7_type["] {
                    width: 160px !important;
                }
                #tfa_nyw_qty7_type-D,
                *[id^="tfa_nyw_qty7_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty7_type-L,
                label[id^="tfa_nyw_qty7_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty7_amt,
                *[id^="tfa_nyw_qty7_amt["] {
                    width: 168px !important;
                }
                #tfa_nyw_qty7_amt-D,
                *[id^="tfa_nyw_qty7_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty7_amt-L,
                label[id^="tfa_nyw_qty7_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty8_quan,
                *[id^="tfa_nyw_qty8_quan["] {
                    width: 168px !important;
                }
                #tfa_nyw_qty8_quan-D,
                *[id^="tfa_nyw_qty8_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty8_quan-L,
                label[id^="tfa_nyw_qty8_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty8_on,
                *[id^="tfa_nyw_qty8_on["] {
                    width: 168px !important;
                }
                #tfa_nyw_qty8_on-D,
                *[id^="tfa_nyw_qty8_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty8_on-L,
                label[id^="tfa_nyw_qty8_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty8_type,
                *[id^="tfa_nyw_qty8_type["] {
                    width: 160px !important;
                }
                #tfa_nyw_qty8_type-D,
                *[id^="tfa_nyw_qty8_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty8_type-L,
                label[id^="tfa_nyw_qty8_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty8_amt,
                *[id^="tfa_nyw_qty8_amt["] {
                    width: 168px !important;
                }
                #tfa_nyw_qty8_amt-D,
                *[id^="tfa_nyw_qty8_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty8_amt-L,
                label[id^="tfa_nyw_qty8_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty9_quan,
                *[id^="tfa_nyw_qty9_quan["] {
                    width: 168px !important;
                }
                #tfa_nyw_qty9_quan-D,
                *[id^="tfa_nyw_qty9_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty9_quan-L,
                label[id^="tfa_nyw_qty9_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty9_on,
                *[id^="tfa_nyw_qty9_on["] {
                    width: 168px !important;
                }
                #tfa_nyw_qty9_on-D,
                *[id^="tfa_nyw_qty9_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty9_on-L,
                label[id^="tfa_nyw_qty9_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty9_type,
                *[id^="tfa_nyw_qty9_type["] {
                    width: 160px !important;
                }
                #tfa_nyw_qty9_type-D,
                *[id^="tfa_nyw_qty9_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty9_type-L,
                label[id^="tfa_nyw_qty9_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty9_amt,
                *[id^="tfa_nyw_qty9_amt["] {
                    width: 168px !important;
                }
                #tfa_nyw_qty9_amt-D,
                *[id^="tfa_nyw_qty9_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty9_amt-L,
                label[id^="tfa_nyw_qty9_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty10_quan,
                *[id^="tfa_nyw_qty10_quan["] {
                    width: 168px !important;
                }
                #tfa_nyw_qty10_quan-D,
                *[id^="tfa_nyw_qty10_quan["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty10_quan-L,
                label[id^="tfa_nyw_qty10_quan["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty10_on,
                *[id^="tfa_nyw_qty10_on["] {
                    width: 168px !important;
                }
                #tfa_nyw_qty10_on-D,
                *[id^="tfa_nyw_qty10_on["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty10_on-L,
                label[id^="tfa_nyw_qty10_on["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty10_type,
                *[id^="tfa_nyw_qty10_type["] {
                    width: 160px !important;
                }
                #tfa_nyw_qty10_type-D,
                *[id^="tfa_nyw_qty10_type["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty10_type-L,
                label[id^="tfa_nyw_qty10_type["] {
                    width: 61px !important;
                    min-width: 0px;
                }
            
                #tfa_nyw_qty10_amt,
                *[id^="tfa_nyw_qty10_amt["] {
                    width: 168px !important;
                }
                #tfa_nyw_qty10_amt-D,
                *[id^="tfa_nyw_qty10_amt["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_nyw_qty10_amt-L,
                label[id^="tfa_nyw_qty10_amt["] {
                    width: 58px !important;
                    min-width: 0px;
                }
            
                #tfa_os_price1_bottle,
                *[id^="tfa_os_price1_bottle["] {
                    width: 129px !important;
                }
                #tfa_os_price1_bottle-D,
                *[id^="tfa_os_price1_bottle["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_os_price2_bottle,
                *[id^="tfa_os_price2_bottle["] {
                    width: 129px !important;
                }
                #tfa_os_price2_bottle-D,
                *[id^="tfa_os_price2_bottle["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_os_price3_bottle,
                *[id^="tfa_os_price3_bottle["] {
                    width: 129px !important;
                }
                #tfa_os_price3_bottle-D,
                *[id^="tfa_os_price3_bottle["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_os_price4_bottle,
                *[id^="tfa_os_price4_bottle["] {
                    width: 129px !important;
                }
                #tfa_os_price4_bottle-D,
                *[id^="tfa_os_price4_bottle["][class~="field-container-D"] {
                    width: auto !important;
                }
            
                #tfa_os_price5_bottle,
                *[id^="tfa_os_price5_bottle["] {
                    width: 129px !important;
                }
                #tfa_os_price5_bottle-D,
                *[id^="tfa_os_price5_bottle["][class~="field-container-D"] {
                    width: auto !important;
                }
            </style><div class=""><div class="wForm" id="4691404-WRPR" dir="ltr">
<div class="codesection" id="code-4691404"></div>
<h3 class="wFormTitle" id="4691404-T">Price Posting Form</h3>
<form method="post" action="responses.php" class="hintsBelow labelsAbove" id="4691404" role="form">
<div class="htmlSection" id="tfa_1"><div class="htmlContent" id="tfa_1-HTML"><h4><u>Instructions</u></h4></div></div>
<div class="htmlSection" id="tfa_2"><div class="htmlContent" id="tfa_2-HTML"><span style="font-size: 14.4px;">Welcome to the MHW Price Posting form. Please carefully review and complete all required information. If you have any questions, please contact us at priceposting@mhwltd.com.</span><br style="font-size: 14.4px;"><br style="font-size: 14.4px;"><i style="font-size: 14.4px;">This form is used to add, change, and delete price postings for existing items.&nbsp;<span style="font-size: 14.4px; word-spacing: normal;">A&nbsp;</span><span style="font-size: 14.4px; word-spacing: normal;">new&nbsp;</span><u style="font-size: 14.4px; word-spacing: normal;">item,&nbsp;</u><span style="font-size: 14.4px; word-spacing: normal;">new&nbsp;</span><u style="font-size: 14.4px; word-spacing: normal;">vintage,&nbsp;</u><span style="font-size: 14.4px; word-spacing: normal;">new&nbsp;</span><u style="font-size: 14.4px; word-spacing: normal;">configuration</u><u style="font-size: 14.4px; word-spacing: normal;">,</u><span style="font-size: 14.4px; word-spacing: normal;">&nbsp;or new&nbsp;</span><u style="font-size: 14.4px; word-spacing: normal;">size</u><span style="font-size: 14.4px; word-spacing: normal;">, requires a&nbsp;</span><u style="font-size: 14.4px; word-spacing: normal;">NEW</u><span style="font-size: 14.4px; word-spacing: normal;">&nbsp;item be added to the&nbsp;<a href="productsetup.php">Product &amp; Item Specifications Form</a>.</span></i><br style="font-size: 14.4px;"><br style="font-size: 14.4px;"><span style="font-size: 14.4px;">Please follow the below instructions carefully:</span><br style="font-size: 14.4px;"><ul style="font-size: 14.4px;"><li>To price post an item for a given state, select "Yes" from the drop-down menu under the "Price Posting States" header</li><li>For each state, select whether you are adding a posting for a new item, deleting an existing posting, updating an existing posting, or no change. "No change" should only be selected if you would like the posting to remain the same as the prior month</li><li>For postings to remain Active, please select "Active" from the drop-down menu for each state. If you select, "Delete" from the first menu, please also select "Inactive" from the second menu</li><li>FOB is the shipping point; all retail price postings in NY should indicate "NYS"</li><li>Wholesale price postings can be assigned to the following FOBs: "NJ" - shipping from warehouse/stateside; "DI" - direct import orders (for multiple FOBs, please use the first two letters of the exporting country); "EX" - ex-cellars (distributor picks up from winery)</li><li>To post discounts, please select "Yes" from the Discounts drop-down menu for each applicable state. This will activate a series of fields in which you must enter the quantity at which the discount should occur, on which unit of measure, in dollars or on a percentage basis, and what amount in dollars or percent. For example, Quantity: 5; On: Container; Type: $; Amount: 1 means a discount of $1 at 5 bottles</li><li>If you would like to add sample or combo package pricing, please reach out to your operations team contact.</li></ul><div style="font-size: 14.4px;">A CONFIRMATION E-MAIL will be sent to each client upon&nbsp;<span style="font-size: 14.4px; word-spacing: normal;">receipt of their pricing schedule. If a confirmation e-mail is&nbsp;</span><span style="font-size: 14.4px; word-spacing: normal;">not received within 48 hours of a submission, please&nbsp;</span><span style="font-size: 14.4px; word-spacing: normal;">contact MHW's price posting team to ensure the submission has been&nbsp;</span><span style="font-size: 14.4px; word-spacing: normal;">received.</span></div><div style="font-size: 14.4px;"><span style="font-size: 14.4px; word-spacing: normal;"><br></span></div><div style="font-size: 14.4px;"><b><u>Price Posting Calendar</u></b></div><div style="font-size: 14.4px;"><ul style="font-size: 14.4px;"><li><u>1st of the Month</u><u style="font-weight: bold;">:</u><span style="font-weight: 700;">&nbsp;</span>New Jersey Retail:&nbsp;<i style="font-size: 14.4px; word-spacing: normal;">Effective the 1st of the following month (e.g., January postings are due December 1st)</i></li><li><u>10th of the Month<span style="font-weight: 700;">:</span></u><span style="font-weight: 700;">&nbsp;</span>New York Wholesale:&nbsp;<i>Effective the 1st of the second succeeding month (e.g., January postings are due November 10th)</i></li><li><span style="text-decoration-line: underline;">20th of the Month</span><span style="font-weight: 700; text-decoration-line: underline;">:</span>&nbsp;Massachusetts Wholesale; Minnesota Wholesale; Connecticut Wholesale; New York Retail:&nbsp;<i>Effective the 1st of the second succeeding month (e.g., January postings are due November 20th)</i></li></ul><div style="font-size: 14.4px;">Please note all requests received after the above due dates will be subject to a $300 late fee.</div></div></div></div>

<div class="oneField field-container-D    " id="tfa_client_name-D">
	<label id="tfa_client_name-L" class="label preField " for="tfa_client_name"><b>Client Name</b></label><br>
	<div class="inputWrapper">
		<select id="tfa_client_name" name="tfa_client_name" title="Client Name" aria-required="true" class="required">
		<?php
		$clients = explode(";",$_SESSION['mhwltdphp_userclients']);
		
		foreach ($clients as &$clientvalue) {
			if(isset($pf_client_name) && $pf_client_name==$clientvalue){
				echo "<option value=\"".$clientvalue."\" class=\"\" selected>".$clientvalue."</option>";
			}
			else{
					echo "<option value=\"".$clientvalue."\" class=\"\">".$clientvalue."</option>";
			}
		}
		?>					
		</select>
	</div>
	<div class="float-right"><button type="reset" value="Reset" id="clearform" class="btn btn-sm btn-outline-danger">Clear Form</button></div>
</div>

<!--<fieldset id="tfa_4" class="repeat section" data-repeatlabel="Add a new Item">-->
<fieldset id="tfa_4" class="section" >
	<?php
		if($itemID > 0){ echo "<legend id=\"tfa_4-L\">Item: editing item_id ".$itemID."</legend>"; }
		else{  echo "<legend id=\"tfa_4-L\">Item</legend>"; }
	?>
	
	<div id="tfa_7" class="section inline group">

		<div class="oneField field-container-D    " id="tfa_item_desc-D">
			<label id="tfa_item_desc-L" class="label preField " for="tfa_item_desc"><b>Item Description</b></label><br>
			<div class="inputWrapper"><input type="text" id="tfa_item_desc" name="tfa_item_desc" value="" title="Item Description" data-dataset-allow-free-responses="" class="required validate-alphanum"></div>
		</div>

		<div class="oneField field-container-D    " id="tfa_item_code-D">
			<label id="tfa_item_code-L" class="label preField " for="tfa_item_code"><b>Item Code</b></label><br>
			<div class="inputWrapper"><input type="text" id="tfa_item_code" name="tfa_item_code" value="" title="Item Code" data-dataset-allow-free-responses="" class="validate-alphanum"></div>
		</div>

		<div class="oneField field-container-D    " id="tfa_stock_uom-D">
			<label id="tfa_stock_uom-L" class="label preField " for="tfa_stock_uom"><b>Stock UOM</b></label><br>
			<div class="inputWrapper"><input type="text" id="tfa_stock_uom" name="tfa_stock_uom" value="" title="Stock UOM" data-dataset-allow-free-responses="" class=""></div>
		</div>

		<div class="oneField field-container-D    " id="tfa_bottles_per_case-D">
			<label id="tfa_bottles_per_case-L" class="label preField " for="tfa_bottles_per_case"><b>Bottles per Case</b></label><br>
			<div class="inputWrapper"><input type="text" id="tfa_bottles_per_case" name="tfa_bottles_per_case" value="" title="Bottles per Case" data-dataset-allow-free-responses="" class=""></div>
		</div>

<!---------TBD: RENAME FIELDS BELOW / ENFORCE VALIDATION / ENFORCE CONDITIONAL DISPLAY ------>
<div class="oneField field-container-D    " id="tfa_nat_price-D">
<label id="tfa_nat_price-L" class="label preField " for="tfa_nat_price"><b>National Price</b></label><br><div class="inputWrapper">
<input type="text" id="tfa_nat_price" name="tfa_nat_price" value="" aria-describedby="tfa_nat_price-HH" title="National Price" data-dataset-allow-free-responses="" class="validate-float"><span class="field-hint-inactive" id="tfa_nat_price-H"><span id="tfa_nat_price-HH" class="hint">(if applicable)</span></span>
</div>
</div>
</div>
<div class="htmlSection" id="tfa_51"><div class="htmlContent" id="tfa_51-HTML"><h4><u style="">Price Posting States</u></h4></div></div>
<div id="tfa_39" class="section inline group">
<div class="oneField field-container-D    " id="tfa_ca_state-D">
<label id="tfa_ca_state-L" class="label preField " for="tfa_ca_state"><b>CA</b></label><br><div class="inputWrapper">
<select id="tfa_ca_state" name="tfa_ca_state" title="CA" aria-describedby="tfa_ca_state-HH" class=""><option value="">Please select...</option>
<option id="tfa_10" data-conditionals="#tfa_40" class="">Yes</option></select><span class="field-hint-inactive" id="tfa_ca_state-H"><span id="tfa_ca_state-HH" class="hint">(Beer only)</span></span>
</div>
</div>
<div class="oneField field-container-D    " id="tfa_ct_state-D">
<label id="tfa_ct_state-L" class="label preField " for="tfa_ct_state"><b>CT Wholesale</b></label><br><div class="inputWrapper"><select id="tfa_ct_state" name="tfa_ct_state" title="CT Wholesale" class=""><option value="">Please select...</option>
<option id="tfa_18" data-conditionals="#tfa_468" class="">Yes</option></select></div>
</div>
<div class="oneField field-container-D    " id="tfa_ma_state-D">
<label id="tfa_ma_state-L" class="label preField " for="tfa_ma_state"><b>MA Wholesale</b></label><br><div class="inputWrapper"><select id="tfa_ma_state" name="tfa_ma_state" title="MA Wholesale" class=""><option value="">Please select...</option>
<option id="tfa_22" data-conditionals="#tfa_708" class="">Yes</option></select></div>
</div>
<div class="oneField field-container-D    " id="tfa_mn_state-D">
<label id="tfa_mn_state-L" class="label preField " for="tfa_mn_state"><b>MN Wholesale</b></label><br><div class="inputWrapper">
<select id="tfa_mn_state" name="tfa_mn_state" title="MN Wholesale" aria-describedby="tfa_mn_state-HH" class=""><option value="">Please select...</option>
<option id="tfa_26" data-conditionals="#tfa_948" class="">Yes</option></select><span class="field-hint-inactive" id="tfa_mn_state-H"><span id="tfa_mn_state-HH" class="hint">(Spirits only)</span></span>
</div>
</div>
<div class="oneField field-container-D    " id="tfa_nj_state-D">
<label id="tfa_nj_state-L" class="label preField " for="tfa_nj_state"><b>NJ Retail</b></label><br><div class="inputWrapper"><select id="tfa_nj_state" name="tfa_nj_state" title="NJ Retail" class=""><option value="">Please select...</option>
<option id="tfa_30" data-conditionals="#tfa_1188" class="">Yes</option></select></div>
</div>
<div class="oneField field-container-D    " id="tfa_nyw_state-D">
<label id="tfa_nyw_state-L" class="label preField " for="tfa_nyw_state"><b>NY Wholesale</b></label><br><div class="inputWrapper"><select id="tfa_nyw_state" name="tfa_nyw_state" title="NY Wholesale" class=""><option value="">Please select...</option>
<option id="tfa_34" data-conditionals="#tfa_1668" class="">Yes</option></select></div>
</div>
<div class="oneField field-container-D    " id="tfa_nyr_state-D">
<label id="tfa_nyr_state-L" class="label preField " for="tfa_nyr_state"><b>NY Retail</b></label><br><div class="inputWrapper"><select id="tfa_nyr_state" name="tfa_nyr_state" title="NY Retail" class=""><option value="">Please select...</option>
<option id="tfa_38" data-conditionals="#tfa_1428" class="">Yes</option></select></div>
</div>
<div class="oneField field-container-D    " id="tfa_other_state-D">
<label id="tfa_other_state-L" class="label preField " for="tfa_other_state"><b>Other States</b></label><br><div class="inputWrapper"><select id="tfa_other_state" name="tfa_other_state" title="Other States" class=""><option value="">Please select...</option>
<option id="tfa_1826" data-conditionals="#tfa_1827" class="">Yes</option></select></div>
</div>
</div>
<fieldset id="tfa_40" class="section offstate" data-condition="`#tfa_10`">
<legend id="tfa_40-L">CA Price Posting (Beer Only)</legend>
<div id="tfa_49" class="section inline group">
<div class="oneField field-container-D    " id="tfa_ca_change_type-D">
<label id="tfa_ca_change_type-L" class="label preField reqMark" for="tfa_ca_change_type"><b>CA No Change / Add / Update / Delete</b></label><br><div class="inputWrapper">
<select id="tfa_ca_change_type" name="tfa_ca_change_type" title="CA No Change / Add / Update / Delete" aria-required="true" aria-describedby="tfa_ca_change_type-HH" class="required"><option value="">Please select...</option>
<option id="tfa_42" class="">No Change</option>
<option id="tfa_43" class="">Add</option>
<option id="tfa_44" class="">Update</option>
<option id="tfa_45" class="">Delete</option></select><span class="field-hint-inactive" id="tfa_ca_change_type-H"><span id="tfa_ca_change_type-HH" class="hint">(Select "Add" for new items. "No Change" will post item at price below)</span></span>
</div>
</div>
<div class="oneField field-container-D    " id="tfa_ca_price_post_active-D">
<label id="tfa_ca_price_post_active-L" class="label preField reqMark" for="tfa_ca_price_post_active"><b>CA Price Posting Active</b></label><br><div class="inputWrapper">
<select id="tfa_ca_price_post_active" name="tfa_ca_price_post_active" title="CA Price Posting Active" aria-required="true" aria-describedby="tfa_ca_price_post_active-HH" class="required"><option value="">Please select...</option>
<option id="tfa_47" data-conditionals="#tfa_50" class="">Active</option>
<option id="tfa_48" class="">Inactive</option></select><span class="field-hint-inactive" id="tfa_ca_price_post_active-H"><span id="tfa_ca_price_post_active-HH" class="hint">(Must be marked "Active" to post)</span></span>
</div>
</div>
</div>
<div id="tfa_50" class="section group" data-condition="`#tfa_47`"><div id="tfa_55" class="section inline group">
<div class="oneField field-container-D     wf-acl-hidden" id="tfa_ca_price_bottle-D">
<label id="tfa_ca_price_bottle-L" class="label preField " for="tfa_ca_price_bottle"><b>CA Price / Bottle</b></label><br><div class="inputWrapper"><input type="text" id="tfa_ca_price_bottle" name="tfa_ca_price_bottle" value="" title="CA Price / Bottle" data-dataset-allow-free-responses="" class="validate-float"></div>
</div>
<div class="oneField field-container-D     wf-acl-hidden" id="tfa_ca_price_case-D">
<label id="tfa_ca_price_case-L" class="label preField " for="tfa_ca_price_case"><b>CA Price / Case</b></label><br><div class="inputWrapper"><input type="text" id="tfa_ca_price_case" name="tfa_ca_price_case" value="" title="CA Price / Case" data-dataset-allow-free-responses="" class="validate-float"></div>
</div>
<div class="oneField field-container-D     wf-acl-hidden" id="tfa_ca_stand_cost-D">
<label id="tfa_ca_stand_cost-L" class="label preField " for="tfa_ca_stand_cost"><b>CA Standard Cost</b></label><br><div class="inputWrapper"><input type="text" id="tfa_ca_stand_cost" name="tfa_ca_stand_cost" value="" title="CA Standard Cost" data-dataset-allow-free-responses="" class="validate-float"></div>
</div>
<div class="oneField field-container-D     wf-acl-hidden" id="tfa_ca_purch_cost-D">
<label id="tfa_ca_purch_cost-L" class="label preField " for="tfa_ca_purch_cost"><b>CA Purchase Cost</b></label><br><div class="inputWrapper"><input type="text" id="tfa_ca_purch_cost" name="tfa_ca_purch_cost" value="" title="CA Purchase Cost" data-dataset-allow-free-responses="" class="validate-float"></div>
</div>
<div class="oneField field-container-D     wf-acl-hidden" id="tfa_ca_fob-D">
<label id="tfa_ca_fob-L" class="label preField " for="tfa_ca_fob"><b>CA FOB</b></label><br><div class="inputWrapper">
<input type="text" id="tfa_ca_fob" name="tfa_ca_fob" value="" aria-describedby="tfa_ca_fob-HH" title="CA FOB" data-dataset-allow-free-responses="" class="validate-alphanum"><span class="field-hint-inactive" id="tfa_ca_fob-H"><span id="tfa_ca_fob-HH" class="hint">(See instructions above)</span></span>
</div>
</div>
<div class="oneField field-container-D    " id="tfa_ca_brand-D">
<label id="tfa_ca_brand-L" class="label preField reqMark" for="tfa_ca_brand"><b>Brand &amp; Beverage</b></label><br><div class="inputWrapper">
<input type="text" id="tfa_ca_brand" name="tfa_ca_brand" value="" aria-required="true" aria-describedby="tfa_ca_brand-HH" title="Brand &amp; Beverage" data-dataset-allow-free-responses="" class="required"><span class="field-hint-inactive" id="tfa_ca_brand-H"><span id="tfa_ca_brand-HH" class="hint">(Must match COLA)</span></span>
</div>
</div>
<div class="oneField field-container-D    " id="tfa_ca_package-D">
<label id="tfa_ca_package-L" class="label preField reqMark" for="tfa_ca_package"><b>CA Package</b></label><br><div class="inputWrapper">
<input type="text" id="tfa_ca_package" name="tfa_ca_package" value="" aria-required="true" aria-describedby="tfa_ca_package-HH" title="CA Package" data-dataset-allow-free-responses="" class="required"><span class="field-hint-inactive" id="tfa_ca_package-H"><span id="tfa_ca_package-HH" class="hint">(# of items)</span></span>
</div>
</div>
<div class="oneField field-container-D    " id="tfa_ca_contents-D">
<label id="tfa_ca_contents-L" class="label preField reqMark" for="tfa_ca_contents"><b>CA Contents</b></label><br><div class="inputWrapper">
<input type="text" id="tfa_ca_contents" name="tfa_ca_contents" value="" aria-required="true" aria-describedby="tfa_ca_contents-HH" title="CA Contents" data-dataset-allow-free-responses="" class="required"><span class="field-hint-inactive" id="tfa_ca_contents-H"><span id="tfa_ca_contents-HH" class="hint">(e.g., 5 Gallons)</span></span>
</div>
</div>
<div class="oneField field-container-D    " id="tfa_ca_cont_charge-D">
<label id="tfa_ca_cont_charge-L" class="label preField " for="tfa_ca_cont_charge"><b>Container Charge / Deposit</b></label><br><div class="inputWrapper"><input type="text" id="tfa_ca_cont_charge" name="tfa_ca_cont_charge" value="" title="Container Charge / Deposit" data-dataset-allow-free-responses="" class=""></div>
</div>
<div id="tfa_1816" class="section group">
<div class="oneField field-container-D    " id="tfa_ca_price_retail-D">
<label id="tfa_ca_price_retail-L" class="label preField " for="tfa_ca_price_retail"><b>CA Price to Retail</b></label><br><div class="inputWrapper"><input type="text" id="tfa_ca_price_retail" name="tfa_ca_price_retail" value="" title="CA Price to Retail" data-dataset-allow-free-responses="" class=""></div>
</div>
<div class="oneField field-container-D    " id="tfa_ca_price_wholesale-D">
<label id="tfa_ca_price_wholesale-L" class="label preField " for="tfa_ca_price_wholesale"><b>CA Price to Wholesale</b></label><br><div class="inputWrapper"><input type="text" id="tfa_ca_price_wholesale" name="tfa_ca_price_wholesale" value="" title="CA Price to Wholesale" data-dataset-allow-free-responses="" class=""></div>
</div>
<div class="oneField field-container-D    " id="tfa_ca_fob_del-D">
<label id="tfa_ca_fob_del-L" class="label preField reqMark" for="tfa_ca_fob_del"><b>Price FOB or Delivered?</b></label><br><div class="inputWrapper"><select id="tfa_ca_fob_del" name="tfa_ca_fob_del" title="Price FOB or Delivered?" aria-required="true" class="required"><option value="">Please select...</option>
<option value="FOB" id="tfa_1814" class="">FOB</option>
<option value="Delivered" id="tfa_1815" class="">Delivered</option></select></div>
</div>
<div class="oneField field-container-D    " id="tfa_ca_foe-D">
<label id="tfa_ca_foe-L" class="label preField " for="tfa_ca_foe"><b>Freight on Empties</b></label><br><div class="inputWrapper"><input type="text" id="tfa_ca_foe" name="tfa_ca_foe" value="" title="Freight on Empties" data-dataset-allow-free-responses="" class=""></div>
</div>
<div class="oneField field-container-D    " id="tfa_ca_counties-D">
<label id="tfa_ca_counties-L" class="label preField reqMark" for="tfa_ca_counties"><b>Counties</b></label><br><div class="inputWrapper">
<input type="text" id="tfa_ca_counties" name="tfa_ca_counties" value="" aria-required="true" aria-describedby="tfa_ca_counties-HH" title="Counties" data-dataset-allow-free-responses="" class="required"><span class="field-hint-inactive" id="tfa_ca_counties-H"><span id="tfa_ca_counties-HH" class="hint">(List counties where wholesaler or retailer is located)</span></span>
</div>
</div>
<div class="oneField field-container-D    " id="tfa_ca_dist-D">
<label id="tfa_ca_dist-L" class="label preField " for="tfa_ca_dist"><b>CA Distributor</b></label><br><div class="inputWrapper"><input type="text" id="tfa_ca_dist" name="tfa_ca_dist" value="" title="CA Distributor" data-dataset-allow-free-responses="" class=""></div>
</div>
</div>
<div class="oneField field-container-D     wf-acl-hidden" id="tfa_ca_disc-D">
<label id="tfa_ca_disc-L" class="label preField " for="tfa_ca_disc"><b>Discounts?</b></label><br><div class="inputWrapper">
<select id="tfa_ca_disc" name="tfa_ca_disc" title="Discounts?" aria-describedby="tfa_ca_disc-HH" class=""><option value="">Please select...</option>
<option id="tfa_63" data-conditionals="#tfa_64" class="">Yes</option></select><span class="field-hint-inactive" id="tfa_ca_disc-H"><span id="tfa_ca_disc-HH" class="hint">(Select "Yes" to modify discounts)</span></span>
</div>
</div>
</div></div>
<div id="tfa_64" class="section group wf-acl-hidden" data-condition="`#tfa_63`">
<label class="label preField" id="tfa_64-L"><u style=""><b>Discounts</b></u></label><br><div id="tfa_155" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_158-GRID" class="">Quantity </th>
<th scope="col" id="tfa_161-GRID" class="">On </th>
<th scope="col" id="tfa_163-GRID" class="">Type </th>
<th scope="col" id="tfa_159-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_156" class="section    alternate-0 ">
<th scope="row" id="tfa_156-L" class="headerCol">CA Qty1 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_158-D"><div class="inputWrapper"><input type="text" id="tfa_158" name="tfa_158" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_161-D"><div class="inputWrapper"><select id="tfa_161" name="tfa_161" title="On" class=""><option value="">Please select...</option>
<option value="tfa_164" id="tfa_164" class="">Stock UOM</option>
<option value="tfa_165" id="tfa_165" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_163-D"><div class="inputWrapper"><select id="tfa_163" name="tfa_163" title="Type" class=""><option value="">Please select...</option>
<option value="tfa_166" id="tfa_166" class="">$</option>
<option value="tfa_167" id="tfa_167" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_159-D"><div class="inputWrapper"><input type="text" id="tfa_159" name="tfa_159" value="" title="Amount" data-dataset-allow-free-responses="" class=""></div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_178" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_180-GRID" class="">Quantity </th>
<th scope="col" id="tfa_181-GRID" class="">On </th>
<th scope="col" id="tfa_184-GRID" class="">Type </th>
<th scope="col" id="tfa_187-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_179" class="section    alternate-0 ">
<th scope="row" id="tfa_179-L" class="headerCol">CA Qty2 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_180-D"><div class="inputWrapper"><input type="text" id="tfa_180" name="tfa_180" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_181-D"><div class="inputWrapper"><select id="tfa_181" name="tfa_181" title="On" class=""><option value="">Please select...</option>
<option value="tfa_182" id="tfa_182" class="">Stock UOM</option>
<option value="tfa_183" id="tfa_183" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_184-D"><div class="inputWrapper"><select id="tfa_184" name="tfa_184" title="Type" class=""><option value="">Please select...</option>
<option value="tfa_185" id="tfa_185" class="">$</option>
<option value="tfa_186" id="tfa_186" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_187-D"><div class="inputWrapper"><input type="text" id="tfa_187" name="tfa_187" value="" title="Amount" data-dataset-allow-free-responses="" class=""></div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_198" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_200-GRID" class="">Quantity </th>
<th scope="col" id="tfa_201-GRID" class="">On </th>
<th scope="col" id="tfa_204-GRID" class="">Type </th>
<th scope="col" id="tfa_207-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_199" class="section    alternate-0 ">
<th scope="row" id="tfa_199-L" class="headerCol">CA Qty3 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_200-D"><div class="inputWrapper"><input type="text" id="tfa_200" name="tfa_200" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_201-D"><div class="inputWrapper"><select id="tfa_201" name="tfa_201" title="On" class=""><option value="">Please select...</option>
<option value="tfa_202" id="tfa_202" class="">Stock UOM</option>
<option value="tfa_203" id="tfa_203" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_204-D"><div class="inputWrapper"><select id="tfa_204" name="tfa_204" title="Type" class=""><option value="">Please select...</option>
<option value="tfa_205" id="tfa_205" class="">$</option>
<option value="tfa_206" id="tfa_206" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_207-D"><div class="inputWrapper"><input type="text" id="tfa_207" name="tfa_207" value="" title="Amount" data-dataset-allow-free-responses="" class=""></div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_218" class="section group"><table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_220-GRID" class="">Quantity </th>
<th scope="col" id="tfa_221-GRID" class="">On </th>
<th scope="col" id="tfa_224-GRID" class="">Type </th>
<th scope="col" id="tfa_227-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_219" class="section    alternate-0 ">
<th scope="row" id="tfa_219-L" class="headerCol">CA Qty4 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_220-D"><div class="inputWrapper"><input type="text" id="tfa_220" name="tfa_220" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_221-D"><div class="inputWrapper"><select id="tfa_221" name="tfa_221" title="On" class=""><option value="">Please select...</option>
<option value="tfa_222" id="tfa_222" class="">Stock UOM</option>
<option value="tfa_223" id="tfa_223" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_224-D"><div class="inputWrapper"><select id="tfa_224" name="tfa_224" title="Type" class=""><option value="">Please select...</option>
<option value="tfa_225" id="tfa_225" class="">$</option>
<option value="tfa_226" id="tfa_226" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_227-D"><div class="inputWrapper"><input type="text" id="tfa_227" name="tfa_227" value="" title="Amount" data-dataset-allow-free-responses="" class=""></div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table></div>
<div id="tfa_238" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_240-GRID" class="">Quantity </th>
<th scope="col" id="tfa_241-GRID" class="">On </th>
<th scope="col" id="tfa_244-GRID" class="">Type </th>
<th scope="col" id="tfa_247-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_239" class="section    alternate-0 ">
<th scope="row" id="tfa_239-L" class="headerCol">CA Qty5 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_240-D"><div class="inputWrapper"><input type="text" id="tfa_240" name="tfa_240" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_241-D"><div class="inputWrapper"><select id="tfa_241" name="tfa_241" title="On" class=""><option value="">Please select...</option>
<option value="tfa_242" id="tfa_242" class="">Stock UOM</option>
<option value="tfa_243" id="tfa_243" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_244-D"><div class="inputWrapper"><select id="tfa_244" name="tfa_244" title="Type" class=""><option value="">Please select...</option>
<option value="tfa_245" id="tfa_245" class="">$</option>
<option value="tfa_246" id="tfa_246" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_247-D"><div class="inputWrapper"><input type="text" id="tfa_247" name="tfa_247" value="" title="Amount" data-dataset-allow-free-responses="" class=""></div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_258" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_260-GRID" class="">Quantity </th>
<th scope="col" id="tfa_261-GRID" class="">On </th>
<th scope="col" id="tfa_264-GRID" class="">Type </th>
<th scope="col" id="tfa_267-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_259" class="section    alternate-0 ">
<th scope="row" id="tfa_259-L" class="headerCol">CA Qty6 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_260-D"><div class="inputWrapper"><input type="text" id="tfa_260" name="tfa_260" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_261-D"><div class="inputWrapper"><select id="tfa_261" name="tfa_261" title="On" class=""><option value="">Please select...</option>
<option value="tfa_262" id="tfa_262" class="">Stock UOM</option>
<option value="tfa_263" id="tfa_263" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_264-D"><div class="inputWrapper"><select id="tfa_264" name="tfa_264" title="Type" class=""><option value="">Please select...</option>
<option value="tfa_265" id="tfa_265" class="">$</option>
<option value="tfa_266" id="tfa_266" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_267-D"><div class="inputWrapper"><input type="text" id="tfa_267" name="tfa_267" value="" title="Amount" data-dataset-allow-free-responses="" class=""></div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_278" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_280-GRID" class="">Quantity </th>
<th scope="col" id="tfa_281-GRID" class="">On </th>
<th scope="col" id="tfa_284-GRID" class="">Type </th>
<th scope="col" id="tfa_287-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_279" class="section    alternate-0 ">
<th scope="row" id="tfa_279-L" class="headerCol">CA Qty7 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_280-D"><div class="inputWrapper"><input type="text" id="tfa_280" name="tfa_280" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_281-D"><div class="inputWrapper"><select id="tfa_281" name="tfa_281" title="On" class=""><option value="">Please select...</option>
<option value="tfa_282" id="tfa_282" class="">Stock UOM</option>
<option value="tfa_283" id="tfa_283" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_284-D"><div class="inputWrapper"><select id="tfa_284" name="tfa_284" title="Type" class=""><option value="">Please select...</option>
<option value="tfa_285" id="tfa_285" class="">$</option>
<option value="tfa_286" id="tfa_286" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_287-D"><div class="inputWrapper"><input type="text" id="tfa_287" name="tfa_287" value="" title="Amount" data-dataset-allow-free-responses="" class=""></div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_298" class="section group"><table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_300-GRID" class="">Quantity </th>
<th scope="col" id="tfa_301-GRID" class="">On </th>
<th scope="col" id="tfa_304-GRID" class="">Type </th>
<th scope="col" id="tfa_307-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_299" class="section    alternate-0 ">
<th scope="row" id="tfa_299-L" class="headerCol">CA Qty8 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_300-D"><div class="inputWrapper"><input type="text" id="tfa_300" name="tfa_300" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_301-D"><div class="inputWrapper"><select id="tfa_301" name="tfa_301" title="On" class=""><option value="">Please select...</option>
<option value="tfa_302" id="tfa_302" class="">Stock UOM</option>
<option value="tfa_303" id="tfa_303" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_304-D"><div class="inputWrapper"><select id="tfa_304" name="tfa_304" title="Type" class=""><option value="">Please select...</option>
<option value="tfa_305" id="tfa_305" class="">$</option>
<option value="tfa_306" id="tfa_306" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_307-D"><div class="inputWrapper"><input type="text" id="tfa_307" name="tfa_307" value="" title="Amount" data-dataset-allow-free-responses="" class=""></div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table></div>
<div id="tfa_318" class="section group"><table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_320-GRID" class="">Quantity </th>
<th scope="col" id="tfa_321-GRID" class="">On </th>
<th scope="col" id="tfa_324-GRID" class="">Type </th>
<th scope="col" id="tfa_327-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_319" class="section    alternate-0 ">
<th scope="row" id="tfa_319-L" class="headerCol">CA Qty9 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_320-D"><div class="inputWrapper"><input type="text" id="tfa_320" name="tfa_320" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_321-D"><div class="inputWrapper"><select id="tfa_321" name="tfa_321" title="On" class=""><option value="">Please select...</option>
<option value="tfa_322" id="tfa_322" class="">Stock UOM</option>
<option value="tfa_323" id="tfa_323" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_324-D"><div class="inputWrapper"><select id="tfa_324" name="tfa_324" title="Type" class=""><option value="">Please select...</option>
<option value="tfa_325" id="tfa_325" class="">$</option>
<option value="tfa_326" id="tfa_326" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_327-D"><div class="inputWrapper"><input type="text" id="tfa_327" name="tfa_327" value="" title="Amount" data-dataset-allow-free-responses="" class=""></div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table></div>
<div id="tfa_338" class="section group"><table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_340-GRID" class="">Quantity </th>
<th scope="col" id="tfa_341-GRID" class="">On </th>
<th scope="col" id="tfa_344-GRID" class="">Type </th>
<th scope="col" id="tfa_347-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_339" class="section    alternate-0 ">
<th scope="row" id="tfa_339-L" class="headerCol">CA Qty10 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_340-D"><div class="inputWrapper"><input type="text" id="tfa_340" name="tfa_340" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_341-D"><div class="inputWrapper"><select id="tfa_341" name="tfa_341" title="On" class=""><option value="">Please select...</option>
<option value="tfa_342" id="tfa_342" class="">Stock UOM</option>
<option value="tfa_343" id="tfa_343" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_344-D"><div class="inputWrapper"><select id="tfa_344" name="tfa_344" title="Type" class=""><option value="">Please select...</option>
<option value="tfa_345" id="tfa_345" class="">$</option>
<option value="tfa_346" id="tfa_346" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_347-D"><div class="inputWrapper"><input type="text" id="tfa_347" name="tfa_347" value="" title="Amount" data-dataset-allow-free-responses="" class=""></div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</fieldset>
<fieldset id="tfa_468" class="section" data-condition="`#tfa_18`">
<legend id="tfa_468-L">CT Price Posting</legend>
<div id="tfa_469" class="section inline group">
<div class="oneField field-container-D    " id="tfa_ct_change_type-D">
<label id="tfa_ct_change_type-L" class="label preField reqMark" for="tfa_ct_change_type"><b>CT No Change / Add / Update / Delete</b></label><br><div class="inputWrapper">
<select id="tfa_ct_change_type" name="tfa_ct_change_type" title="CT No Change / Add / Update / Delete" aria-required="true" aria-describedby="tfa_ct_change_type-HH" class="required"><option value="">Please select...</option>
<option id="tfa_471" class="">No Change</option>
<option id="tfa_472" class="">Add</option>
<option id="tfa_473" class="">Update</option>
<option id="tfa_474" class="">Delete</option></select><span class="field-hint-inactive" id="tfa_ct_change_type-H"><span id="tfa_ct_change_type-HH" class="hint">(Select "Add" for new items. "No Change" will post item at price below)</span></span>
</div>
</div>
<div class="oneField field-container-D    " id="tfa_ct_price_posting-D">
<label id="tfa_ct_price_posting-L" class="label preField reqMark" for="tfa_ct_price_posting"><b>CT Price Posting Active</b></label><br><div class="inputWrapper">
<select id="tfa_ct_price_posting" name="tfa_ct_price_posting" title="CT Price Posting Active" aria-required="true" aria-describedby="tfa_ct_price_posting-HH" class="required"><option value="">Please select...</option>
<option id="tfa_476" data-conditionals="#tfa_478" class="">Active</option>
<option id="tfa_477" class="">Inactive</option></select><span class="field-hint-inactive" id="tfa_ct_price_posting-H"><span id="tfa_ct_price_posting-HH" class="hint">(Must be marked "Active" to post)</span></span>
</div>
</div>
</div>
<div id="tfa_478" class="section group" data-condition="`#tfa_476`"><div id="tfa_479" class="section inline group">
<div class="oneField field-container-D    " id="tfa_ct_price_bot-D">
<label id="tfa_ct_price_bot-L" class="label preField reqMark" for="tfa_ct_price_bot"><b>CT Price / Bottle</b></label><br><div class="inputWrapper"><input type="text" id="tfa_ct_price_bot" name="tfa_ct_price_bot" value="" aria-required="true" title="CT Price / Bottle" data-dataset-allow-free-responses="" class="validate-float required"></div>
</div>
<div class="oneField field-container-D    " id="tfa_ct_price_case-D">
<label id="tfa_ct_price_case-L" class="label preField reqMark" for="tfa_ct_price_case"><b>CT Price / Case</b></label><br><div class="inputWrapper"><input type="text" id="tfa_ct_price_case" name="tfa_ct_price_case" value="" aria-required="true" title="CT Price / Case" data-dataset-allow-free-responses="" class="validate-float required"></div>
</div>
<div class="oneField field-container-D    " id="tfa_ct_stand_cost-D">
<label id="tfa_ct_stand_cost-L" class="label preField " for="tfa_ct_stand_cost"><b>CT Standard Cost</b></label><br><div class="inputWrapper"><input type="text" id="tfa_ct_stand_cost" name="tfa_ct_stand_cost" value="" title="CT Standard Cost" data-dataset-allow-free-responses="" class="validate-float"></div>
</div>
<div class="oneField field-container-D    " id="tfa_ct_purch_cost-D">
<label id="tfa_ct_purch_cost-L" class="label preField " for="tfa_ct_purch_cost"><b>CT Purchase Cost</b></label><br><div class="inputWrapper"><input type="text" id="tfa_ct_purch_cost" name="tfa_ct_purch_cost" value="" title="CT Purchase Cost" data-dataset-allow-free-responses="" class="validate-float"></div>
</div>
<div class="oneField field-container-D    " id="tfa_ct_fob-D">
<label id="tfa_ct_fob-L" class="label preField reqMark" for="tfa_ct_fob"><b>CT FOB</b></label><br><div class="inputWrapper">
<input type="text" id="tfa_ct_fob" name="tfa_ct_fob" value="" aria-required="true" aria-describedby="tfa_ct_fob-HH" title="CT FOB" data-dataset-allow-free-responses="" class="validate-alphanum required"><span class="field-hint-inactive" id="tfa_ct_fob-H"><span id="tfa_ct_fob-HH" class="hint">(See instructions above)</span></span>
</div>
</div>
</div></div>
</fieldset>
<fieldset id="tfa_708" class="section" data-condition="`#tfa_22`">
<legend id="tfa_708-L">MA Price Posting</legend>
<div id="tfa_709" class="section inline group">
<div class="oneField field-container-D    " id="tfa_ma_change_type-D">
<label id="tfa_ma_change_type-L" class="label preField reqMark" for="tfa_ma_change_type"><b>MA No Change / Add / Update / Delete</b></label><br><div class="inputWrapper">
<select id="tfa_ma_change_type" name="tfa_ma_change_type" title="MA No Change / Add / Update / Delete" aria-required="true" aria-describedby="tfa_ma_change_type-HH" class="required"><option value="">Please select...</option>
<option id="tfa_711" class="">No Change</option>
<option id="tfa_712" class="">Add</option>
<option id="tfa_713" class="">Update</option>
<option id="tfa_714" class="">Delete</option></select><span class="field-hint-inactive" id="tfa_ma_change_type-H"><span id="tfa_ma_change_type-HH" class="hint">(Select "Add" for new items. "No Change" will post item at price below)</span></span>
</div>
</div>
<div class="oneField field-container-D    " id="tfa_ma_price_posting-D">
<label id="tfa_ma_price_posting-L" class="label preField reqMark" for="tfa_ma_price_posting"><b>MA Price Posting Active</b></label><br><div class="inputWrapper">
<select id="tfa_ma_price_posting" name="tfa_ma_price_posting" title="MA Price Posting Active" aria-required="true" aria-describedby="tfa_ma_price_posting-HH" class="required"><option value="">Please select...</option>
<option id="tfa_716" data-conditionals="#tfa_718" class="">Active</option>
<option id="tfa_717" class="">Inactive</option></select><span class="field-hint-inactive" id="tfa_ma_price_posting-H"><span id="tfa_ma_price_posting-HH" class="hint">(Must be marked "Active" to post)</span></span>
</div>
</div>
</div>
<div id="tfa_718" class="section group" data-condition="`#tfa_716`"><div id="tfa_719" class="section inline group">
<div class="oneField field-container-D    " id="tfa_ma_price_bot-D">
<label id="tfa_ma_price_bot-L" class="label preField reqMark" for="tfa_ma_price_bot"><b>MA Price / Bottle</b></label><br><div class="inputWrapper"><input type="text" id="tfa_ma_price_bot" name="tfa_ma_price_bot" value="" aria-required="true" title="MA Price / Bottle" data-dataset-allow-free-responses="" class="validate-float required"></div>
</div>
<div class="oneField field-container-D    " id="tfa_ma_price_case-D">
<label id="tfa_ma_price_case-L" class="label preField reqMark" for="tfa_ma_price_case"><b>MA Price / Case</b></label><br><div class="inputWrapper"><input type="text" id="tfa_ma_price_case" name="tfa_ma_price_case" value="" aria-required="true" title="MA Price / Case" data-dataset-allow-free-responses="" class="validate-float required"></div>
</div>
<div class="oneField field-container-D    " id="tfa_ma_stand_cost-D">
<label id="tfa_ma_stand_cost-L" class="label preField " for="tfa_ma_stand_cost"><b>MA Standard Cost</b></label><br><div class="inputWrapper"><input type="text" id="tfa_ma_stand_cost" name="tfa_ma_stand_cost" value="" title="MA Standard Cost" data-dataset-allow-free-responses="" class="validate-float"></div>
</div>
<div class="oneField field-container-D    " id="tfa_ma_purch_cost-D">
<label id="tfa_ma_purch_cost-L" class="label preField " for="tfa_ma_purch_cost"><b>MA Purchase Cost</b></label><br><div class="inputWrapper"><input type="text" id="tfa_ma_purch_cost" name="tfa_ma_purch_cost" value="" title="MA Purchase Cost" data-dataset-allow-free-responses="" class="validate-float"></div>
</div>
<div class="oneField field-container-D    " id="tfa_ma_fob-D">
<label id="tfa_ma_fob-L" class="label preField reqMark" for="tfa_ma_fob"><b>MA FOB</b></label><br><div class="inputWrapper">
<input type="text" id="tfa_ma_fob" name="tfa_ma_fob" value="" aria-required="true" aria-describedby="tfa_ma_fob-HH" title="MA FOB" data-dataset-allow-free-responses="" class="validate-alphanum required"><span class="field-hint-inactive" id="tfa_ma_fob-H"><span id="tfa_ma_fob-HH" class="hint">(See instructions above)</span></span>
</div>
</div>
<div class="oneField field-container-D    " id="tfa_ma_dist-D">
<label id="tfa_ma_dist-L" class="label preField reqMark" for="tfa_ma_dist"><b>MA Distributor</b></label><br><div class="inputWrapper"><select id="tfa_ma_dist" name="tfa_ma_dist" title="MA Distributor" aria-required="true" class="required"><option value="">Please select...</option>
<option id="tfa_2328" class="">A and E Distributors Inc</option>
<option id="tfa_2329" class="">Abacus Distributing</option>
<option id="tfa_2330" class="">Air Station Cape Cod</option>
<option id="tfa_2331" class="">AJ Lukes Importing and Distributing</option>
<option id="tfa_2332" class="">AKG Distributors, Inc</option>
<option id="tfa_2333" class="">Alternative Import Export</option>
<option id="tfa_2334" class="">Arborway Imports</option>
<option id="tfa_2335" class="">Atlantic Importing Company Inc.</option>
<option id="tfa_2336" class="">Atlas Distributing Inc</option>
<option id="tfa_2337" class="">Bay State Wine Company</option>
<option id="tfa_2338" class="">Bexx International</option>
<option id="tfa_2339" class="">Blue Coast Beverage - MA</option>
<option id="tfa_2340" class="">Blueprint Brands - MA</option>
<option id="tfa_2341" class="">Burke Distributing Corporation</option>
<option id="tfa_2342" class="">Cafe Europa</option>
<option id="tfa_2343" class="">Cape Cod Wholesale Wines and Spirit</option>
<option id="tfa_2344" class="">Capital Distributing Company Inc</option>
<option id="tfa_2345" class="">Carolina Wine - Martignetti</option>
<option id="tfa_2346" class="">Charles River Wine Company</option>
<option id="tfa_2347" class="">Classic Wine Imports</option>
<option id="tfa_2348" class="">Colonial Wholesale Beverage</option>
<option id="tfa_2349" class="">Commercial Distributors</option>
<option id="tfa_2350" class="">Commonwealth Wine and Spirits</option>
<option id="tfa_2351" class="">Corporate Wines</option>
<option id="tfa_2352" class="">Craft Brewers Guild BEER ONLY</option>
<option id="tfa_2353" class="">Cray-Burke Co.Inc.</option>
<option id="tfa_2354" class="">D.B. Wines</option>
<option id="tfa_2355" class="">DOS FAMILIAS WINE IMPORTS</option>
<option id="tfa_2356" class="">East Coast Beverage Co</option>
<option id="tfa_2357" class="">Eno Massachusetts Inc.</option>
<option id="tfa_2358" class="">Essex Entities Inc., d/b/a 21 st Century</option>
<option id="tfa_2359" class="">Gate Gourmet - Boston</option>
<option id="tfa_2360" class="">Genuine Wine Selections Inc.</option>
<option id="tfa_2361" class="">Gilbert Distributors</option>
<option id="tfa_2362" class="">Gordons Liquor Store</option>
<option id="tfa_2363" class="">GWFC IMPORTS</option>
<option id="tfa_2364" class="">Hangtime Wholesale Wine Company</option>
<option id="tfa_2365" class="">Heritage Link Brands</option>
<option id="tfa_2366" class="">Hogshead Wine Co</option>
<option id="tfa_2367" class="">Horizon Beverage Company - MA</option>
<option id="tfa_2368" class="">HUB WINE CORP-MA</option>
<option id="tfa_2369" class="">Ideal Wine and Spirits Co. Inc.</option>
<option id="tfa_2370" class="">J Polep Distribution Services</option>
<option id="tfa_2371" class="">Joanna Imports</option>
<option id="tfa_2372" class="">Kappy's Importers and Dist. Co. Inc</option>
<option id="tfa_2373" class="">KLIN SPIRITS LLC</option>
<option id="tfa_2374" class="">L Knife and Son</option>
<option id="tfa_2375" class="">Lamezia Importing Co.</option>
<option id="tfa_2376" class="">Latitude Beverage Company</option>
<option id="tfa_2377" class="">Mariposa Partners LLC</option>
<option id="tfa_2378" class="">Martignetti Companies MA</option>
<option id="tfa_2379" class="">Masciarelli Wine Company - MA</option>
<option id="tfa_2380" class="">Massachusetts Beverage Alliance</option>
<option id="tfa_2381" class="">Master Brands MA, LLC</option>
<option id="tfa_2382" class="">Merrimack Valley Distributing</option>
<option id="tfa_2383" class="">Millesime Fine Wine Traders</option>
<option id="tfa_2384" class="">Milton Distributors</option>
<option id="tfa_2385" class="">Monsieur Touton Selection - MA</option>
<option id="tfa_2386" class="">MS Walker MA</option>
<option id="tfa_2387" class="">Nejamie's Wine</option>
<option id="tfa_2388" class="">Night Shift Distributing, LLC</option>
<option id="tfa_2389" class="">Oak Barrel Imports</option>
<option id="tfa_2390" class="">Oz Wine Company</option>
<option id="tfa_2391" class="">PANTHER DISTRIBUTING</option>
<option id="tfa_2392" class="">Paradise Promotions</option>
<option id="tfa_2393" class="">Paradiso Importing Co. Ltd</option>
<option id="tfa_2394" class="">PETERSEN IMPORTS LLC</option>
<option id="tfa_2395" class="">Polar Beverages</option>
<option id="tfa_2396" class="">Rio's Wine and Liquors</option>
<option id="tfa_2397" class="">Riverside Liquors Inc.</option>
<option id="tfa_2398" class="">Rolivia Inc.</option>
<option id="tfa_2399" class="">Ruby Wines Inc.</option>
<option id="tfa_2400" class="">Saraiva Enterprises Inc</option>
<option id="tfa_2401" class="">Sarmento Imports</option>
<option id="tfa_2402" class="">Seaboard Products-Craft Brewers</option>
<option id="tfa_2403" class="">Select Wine Imports LLC - MA</option>
<option id="tfa_2404" class="">Specialty Marketing Group</option>
<option id="tfa_2405" class="">Spinning Jig</option>
<option id="tfa_2406" class="">Sun Wholesale</option>
<option id="tfa_2407" class="">Terroir Wines LLC</option>
<option id="tfa_2408" class="">THE PIERPONT GROUP LLC</option>
<option id="tfa_2409" class="">The United GroupUnited Liquors Ltd</option>
<option id="tfa_2410" class="">Trio Wine Company</option>
<option id="tfa_2411" class="">United Liquors</option>
<option id="tfa_2412" class="">VIN Distributors LLC</option>
<option id="tfa_2413" class="">VINE VENTURES INC</option>
<option id="tfa_2414" class="">Vineyard Road</option>
<option id="tfa_2415" class="">Whitehall Company Ltd.</option>
<option id="tfa_2416" class="">Wine Cask Imports</option>
<option id="tfa_2417" class="">Winebow Boston</option>
<option id="tfa_2418" class="">World Vintage Imports Inc.</option>
<option id="tfa_2419" class="">World Wide Wine of New England, LLC</option>
<option id="tfa_2420" class="">Yankee Spirits Sturbridge</option></select></div>
</div>
<div class="oneField field-container-D    " id="tfa_ma_disc-D">
<label id="tfa_ma_disc-L" class="label preField " for="tfa_ma_disc"><b>Discounts?</b></label><br><div class="inputWrapper">
<select id="tfa_ma_disc" name="tfa_ma_disc" title="Discounts?" aria-describedby="tfa_ma_disc-HH" class=""><option value="">Please select...</option>
<option id="tfa_726" data-conditionals="#tfa_727" class="">Yes</option></select><span class="field-hint-inactive" id="tfa_ma_disc-H"><span id="tfa_ma_disc-HH" class="hint">(Select "Yes" to modify discounts)</span></span>
</div>
</div>
</div></div>
<div id="tfa_727" class="section group" data-condition="`#tfa_726`">
<label class="label preField" id="tfa_727-L"><u style=""><b>Discounts</b></u></label><br><div id="tfa_728" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_ma_qty1_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_ma_qty1_on-GRID" class="">On </th>
<th scope="col" id="tfa_ma_qty1_type-GRID" class="">Type </th>
<th scope="col" id="tfa_ma_qty1_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_729" class="section    alternate-0 ">
<th scope="row" id="tfa_729-L" class="headerCol">MA Qty1 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty1_quan-D"><div class="inputWrapper"><input type="text" id="tfa_ma_qty1_quan" name="tfa_ma_qty1_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty1_on-D"><div class="inputWrapper"><select id="tfa_ma_qty1_on" name="tfa_ma_qty1_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_732" class="">Stock UOM</option>
<option id="tfa_733" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty1_type-D"><div class="inputWrapper"><select id="tfa_ma_qty1_type" name="tfa_ma_qty1_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_735" class="">$</option>
<option id="tfa_736" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty1_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_ma_qty1_amt" name="tfa_ma_qty1_amt" value="" aria-describedby="tfa_ma_qty1_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_ma_qty1_amt-H"><span id="tfa_ma_qty1_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_738" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_ma_qty2_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_ma_qty2_on-GRID" class="">On </th>
<th scope="col" id="tfa_ma_qty2_type-GRID" class="">Type </th>
<th scope="col" id="tfa_ma_qty2_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_739" class="section    alternate-0 ">
<th scope="row" id="tfa_739-L" class="headerCol">MA Qty2 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty2_quan-D"><div class="inputWrapper"><input type="text" id="tfa_ma_qty2_quan" name="tfa_ma_qty2_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty2_on-D"><div class="inputWrapper"><select id="tfa_ma_qty2_on" name="tfa_ma_qty2_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_742" class="">Stock UOM</option>
<option id="tfa_743" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty2_type-D"><div class="inputWrapper"><select id="tfa_ma_qty2_type" name="tfa_ma_qty2_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_745" class="">$</option>
<option id="tfa_746" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty2_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_ma_qty2_amt" name="tfa_ma_qty2_amt" value="" aria-describedby="tfa_ma_qty2_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_ma_qty2_amt-H"><span id="tfa_ma_qty2_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_748" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_ma_qty3_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_ma_qty3_on-GRID" class="">On </th>
<th scope="col" id="tfa_ma_qty3_type-GRID" class="">Type </th>
<th scope="col" id="tfa_ma_qty3_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_749" class="section    alternate-0 ">
<th scope="row" id="tfa_749-L" class="headerCol">MA Qty3 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty3_quan-D"><div class="inputWrapper"><input type="text" id="tfa_ma_qty3_quan" name="tfa_ma_qty3_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty3_on-D"><div class="inputWrapper"><select id="tfa_ma_qty3_on" name="tfa_ma_qty3_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_752" class="">Stock UOM</option>
<option id="tfa_753" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty3_type-D"><div class="inputWrapper"><select id="tfa_ma_qty3_type" name="tfa_ma_qty3_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_755" class="">$</option>
<option id="tfa_756" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty3_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_ma_qty3_amt" name="tfa_ma_qty3_amt" value="" aria-describedby="tfa_ma_qty3_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_ma_qty3_amt-H"><span id="tfa_ma_qty3_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_758" class="section group"><table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_ma_qty4_quan-GRID" class="">Quantity  </th>
<th scope="col" id="tfa_ma_qty4_on-GRID" class="">On </th>
<th scope="col" id="tfa_ma_qty4_type-GRID" class="">Type </th>
<th scope="col" id="tfa_ma_qty4_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_759" class="section    alternate-0 ">
<th scope="row" id="tfa_759-L" class="headerCol">MA Qty4 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty4_quan-D"><div class="inputWrapper"><input type="text" id="tfa_ma_qty4_quan" name="tfa_ma_qty4_quan" value="" title="Quantity " data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty4_on-D"><div class="inputWrapper"><select id="tfa_ma_qty4_on" name="tfa_ma_qty4_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_762" class="">Stock UOM</option>
<option id="tfa_763" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty4_type-D"><div class="inputWrapper"><select id="tfa_ma_qty4_type" name="tfa_ma_qty4_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_765" class="">$</option>
<option id="tfa_766" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty4_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_ma_qty4_amt" name="tfa_ma_qty4_amt" value="" aria-describedby="tfa_ma_qty4_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_ma_qty4_amt-H"><span id="tfa_ma_qty4_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table></div>
<div id="tfa_768" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_ma_qty5_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_ma_qty5_on-GRID" class="">On </th>
<th scope="col" id="tfa_ma_qty5_type-GRID" class="">Type </th>
<th scope="col" id="tfa_ma_qty5_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_769" class="section    alternate-0 ">
<th scope="row" id="tfa_769-L" class="headerCol">MA Qty5 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty5_quan-D"><div class="inputWrapper"><input type="text" id="tfa_ma_qty5_quan" name="tfa_ma_qty5_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty5_on-D"><div class="inputWrapper"><select id="tfa_ma_qty5_on" name="tfa_ma_qty5_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_772" class="">Stock UOM</option>
<option id="tfa_773" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty5_type-D"><div class="inputWrapper"><select id="tfa_ma_qty5_type" name="tfa_ma_qty5_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_775" class="">$</option>
<option id="tfa_776" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty5_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_ma_qty5_amt" name="tfa_ma_qty5_amt" value="" aria-describedby="tfa_ma_qty5_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_ma_qty5_amt-H"><span id="tfa_ma_qty5_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_778" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_ma_qty6_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_ma_qty6_on-GRID" class="">On </th>
<th scope="col" id="tfa_ma_qty6_type-GRID" class="">Type </th>
<th scope="col" id="tfa_ma_qty6_type-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_779" class="section    alternate-0 ">
<th scope="row" id="tfa_779-L" class="headerCol">MA Qty6 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty6_quan-D"><div class="inputWrapper"><input type="text" id="tfa_ma_qty6_quan" name="tfa_ma_qty6_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty6_on-D"><div class="inputWrapper"><select id="tfa_ma_qty6_on" name="tfa_ma_qty6_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_782" class="">Stock UOM</option>
<option id="tfa_783" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty6_type-D"><div class="inputWrapper"><select id="tfa_ma_qty6_type" name="tfa_ma_qty6_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_785" class="">$</option>
<option id="tfa_786" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty6_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_ma_qty6_amt" name="tfa_ma_qty6_amt" value="" aria-describedby="tfa_ma_qty6_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_ma_qty6_amt-H"><span id="tfa_ma_qty6_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_788" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_ma_qty7_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_ma_qty7_on-GRID" class="">On </th>
<th scope="col" id="tfa_ma_qty7_type-GRID" class="">Type </th>
<th scope="col" id="tfa_ma_qty7_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_789" class="section    alternate-0 ">
<th scope="row" id="tfa_789-L" class="headerCol">MA Qty7 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty7_quan-D"><div class="inputWrapper"><input type="text" id="tfa_ma_qty7_quan" name="tfa_ma_qty7_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty7_on-D"><div class="inputWrapper"><select id="tfa_ma_qty7_on" name="tfa_ma_qty7_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_792" class="">Stock UOM</option>
<option id="tfa_793" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty7_type-D"><div class="inputWrapper"><select id="tfa_ma_qty7_type" name="tfa_ma_qty7_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_795" class="">$</option>
<option id="tfa_796" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty7_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_ma_qty7_amt" name="tfa_ma_qty7_amt" value="" aria-describedby="tfa_ma_qty7_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_ma_qty7_amt-H"><span id="tfa_ma_qty7_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_798" class="section group"><table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_ma_qty8_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_ma_qty8_on-GRID" class="">On </th>
<th scope="col" id="tfa_ma_qty8_type-GRID" class="">Type </th>
<th scope="col" id="tfa_ma_qty8_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_799" class="section    alternate-0 ">
<th scope="row" id="tfa_799-L" class="headerCol">MA Qty8 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty8_quan-D"><div class="inputWrapper"><input type="text" id="tfa_ma_qty8_quan" name="tfa_ma_qty8_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty8_on-D"><div class="inputWrapper"><select id="tfa_ma_qty8_on" name="tfa_ma_qty8_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_802" class="">Stock UOM</option>
<option id="tfa_803" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty8_type-D"><div class="inputWrapper"><select id="tfa_ma_qty8_type" name="tfa_ma_qty8_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_805" class="">$</option>
<option id="tfa_806" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty8_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_ma_qty8_amt" name="tfa_ma_qty8_amt" value="" aria-describedby="tfa_ma_qty8_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_ma_qty8_amt-H"><span id="tfa_ma_qty8_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table></div>
<div id="tfa_808" class="section group"><table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_ma_qty9_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_ma_qty9_on-GRID" class="">On </th>
<th scope="col" id="tfa_ma_qty9_type-GRID" class="">Type </th>
<th scope="col" id="tfa_ma_qty9_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_809" class="section    alternate-0 ">
<th scope="row" id="tfa_809-L" class="headerCol">MA Qty9 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty9_quan-D"><div class="inputWrapper"><input type="text" id="tfa_ma_qty9_quan" name="tfa_ma_qty9_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty9_on-D"><div class="inputWrapper"><select id="tfa_ma_qty9_on" name="tfa_ma_qty9_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_812" class="">Stock UOM</option>
<option id="tfa_813" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty9_type-D"><div class="inputWrapper"><select id="tfa_ma_qty9_type" name="tfa_ma_qty9_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_815" class="">$</option>
<option id="tfa_816" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty9_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_ma_qty9_amt" name="tfa_ma_qty9_amt" value="" aria-describedby="tfa_ma_qty9_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_ma_qty9_amt-H"><span id="tfa_ma_qty9_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table></div>
<div id="tfa_818" class="section group"><table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_ma_qty10_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_ma_qty10_on-GRID" class="">On </th>
<th scope="col" id="tfa_ma_qty10_type-GRID" class="">Type </th>
<th scope="col" id="tfa_ma_qty10_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_819" class="section    alternate-0 ">
<th scope="row" id="tfa_819-L" class="headerCol">MA Qty10 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty10_quan-D"><div class="inputWrapper"><input type="text" id="tfa_ma_qty10_quan" name="tfa_ma_qty10_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty10_on-D"><div class="inputWrapper"><select id="tfa_ma_qty10_on" name="tfa_ma_qty10_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_822" class="">Stock UOM</option>
<option id="tfa_823" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty10_type-D"><div class="inputWrapper"><select id="tfa_ma_qty10_type" name="tfa_ma_qty10_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_825" class="">$</option>
<option id="tfa_826" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_ma_qty10_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_ma_qty10_amt" name="tfa_ma_qty10_amt" value="" aria-describedby="tfa_ma_qty10_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_ma_qty10_amt-H"><span id="tfa_ma_qty10_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</fieldset>
<fieldset id="tfa_948" class="section" data-condition="`#tfa_26`">
<legend id="tfa_948-L">MN Price Posting (Spirits Only)</legend>
<div id="tfa_949" class="section inline group">
<div class="oneField field-container-D    " id="tfa_mn_change_type-D">
<label id="tfa_mn_change_type-L" class="label preField reqMark" for="tfa_mn_change_type"><b>MN No Change / Add / Update / Delete</b></label><br><div class="inputWrapper">
<select id="tfa_mn_change_type" name="tfa_mn_change_type" title="MN No Change / Add / Update / Delete" aria-required="true" aria-describedby="tfa_mn_change_type-HH" class="required"><option value="">Please select...</option>
<option id="tfa_951" class="">No Change</option>
<option id="tfa_952" class="">Add</option>
<option id="tfa_953" class="">Update</option>
<option id="tfa_954" class="">Delete</option></select><span class="field-hint-inactive" id="tfa_mn_change_type-H"><span id="tfa_mn_change_type-HH" class="hint">(Select "Add" for new items. "No Change" will post item at price below)</span></span>
</div>
</div>
<div class="oneField field-container-D    " id="tfa_mn_price_post-D">
<label id="tfa_mn_price_post-L" class="label preField reqMark" for="tfa_mn_price_post"><b>MN Price Posting Active</b></label><br><div class="inputWrapper">
<select id="tfa_mn_price_post" name="tfa_mn_price_post" title="MN Price Posting Active" aria-required="true" aria-describedby="tfa_mn_price_post-HH" class="required"><option value="">Please select...</option>
<option id="tfa_956" data-conditionals="#tfa_958" class="">Active</option>
<option id="tfa_957" class="">Inactive</option></select><span class="field-hint-inactive" id="tfa_955-H"><span id="tfa_955-HH" class="hint">(Must be marked "Active" to post)</span></span>
</div>
</div>
</div>
<div id="tfa_958" class="section group" data-condition="`#tfa_956`"><div id="tfa_959" class="section inline group">
<div class="oneField field-container-D    " id="tfa_mn_price_bot-D">
<label id="tfa_mn_price_bot-L" class="label preField reqMark" for="tfa_mn_price_bot"><b>MN Price / Bottle</b></label><br><div class="inputWrapper"><input type="text" id="tfa_mn_price_bot" name="tfa_mn_price_bot" value="" aria-required="true" title="MN Price / Bottle" data-dataset-allow-free-responses="" class="validate-float required"></div>
</div>
<div class="oneField field-container-D    " id="tfa_mn_price_case-D">
<label id="tfa_mn_price_case-L" class="label preField reqMark" for="tfa_mn_price_case"><b>MN Price / Case</b></label><br><div class="inputWrapper"><input type="text" id="tfa_mn_price_case" name="tfa_mn_price_case" value="" aria-required="true" title="MN Price / Case" data-dataset-allow-free-responses="" class="validate-float required"></div>
</div>
<div class="oneField field-container-D    " id="tfa_mn_std_cost-D">
<label id="tfa_mn_std_cost-L" class="label preField " for="tfa_mn_std_cost"><b>MN Standard Cost</b></label><br><div class="inputWrapper"><input type="text" id="tfa_mn_std_cost" name="tfa_mn_std_cost" value="" title="MN Standard Cost" data-dataset-allow-free-responses="" class="validate-float"></div>
</div>
<div class="oneField field-container-D    " id="tfa_mn_purch_cost-D">
<label id="tfa_mn_purch_cost-L" class="label preField " for="tfa_mn_purch_cost"><b>MN Purchase Cost</b></label><br><div class="inputWrapper"><input type="text" id="tfa_mn_purch_cost" name="tfa_mn_purch_cost" value="" title="MN Purchase Cost" data-dataset-allow-free-responses="" class="validate-float"></div>
</div>
<div class="oneField field-container-D    " id="tfa_mn_fob-D">
<label id="tfa_mn_fob-L" class="label preField reqMark" for="tfa_mn_fob"><b>MN FOB</b></label><br><div class="inputWrapper">
<input type="text" id="tfa_mn_fob" name="tfa_mn_fob" value="" aria-required="true" aria-describedby="tfa_mn_fob-HH" title="MN FOB" data-dataset-allow-free-responses="" class="validate-alphanum required"><span class="field-hint-inactive" id="tfa_mn_fob-H"><span id="tfa_mn_fob-HH" class="hint">(See instructions above)</span></span>
</div>
</div>
<div class="oneField field-container-D    " id="tfa_mn_disc-D">
<label id="tfa_mn_disc-L" class="label preField " for="tfa_mn_disc"><b>Discounts?</b></label><br><div class="inputWrapper">
<select id="tfa_mn_disc" name="tfa_mn_disc" title="Discounts?" aria-describedby="tfa_mn_disc-HH" class=""><option value="">Please select...</option>
<option id="tfa_966" data-conditionals="#tfa_967" class="">Yes</option></select><span class="field-hint-inactive" id="tfa_mn_disc-H"><span id="tfa_mn_disc-HH" class="hint">(Select "Yes" to modify discounts)</span></span>
</div>
</div>
</div></div>
<div id="tfa_967" class="section group" data-condition="`#tfa_966`">
<label class="label preField" id="tfa_967-L"><u style=""><b>Discounts</b></u></label><br><div id="tfa_968" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_mn_qty1_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_mn_qty1_on-GRID" class="">On </th>
<th scope="col" id="tfa_mn_qty1_type-GRID" class="">Type </th>
<th scope="col" id="tfa_mn_qty1_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_969" class="section    alternate-0 ">
<th scope="row" id="tfa_969-L" class="headerCol">MN Qty1 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty1_quan-D"><div class="inputWrapper"><input type="text" id="tfa_mn_qty1_quan" name="tfa_mn_qty1_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty1_on-D"><div class="inputWrapper"><select id="tfa_mn_qty1_on" name="tfa_mn_qty1_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_972" class="">Stock UOM</option>
<option id="tfa_973" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty1_type-D"><div class="inputWrapper"><select id="tfa_mn_qty1_type" name="tfa_mn_qty1_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_975" class="">$</option>
<option id="tfa_976" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty1_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_mn_qty1_amt" name="tfa_mn_qty1_amt" value="" aria-describedby="tfa_mn_qty1_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_mn_qty1_amt-H"><span id="tfa_mn_qty1_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_978" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_mn_qty2_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_mn_qty2_on-GRID" class="">On </th>
<th scope="col" id="tfa_mn_qty2_type-GRID" class="">Type </th>
<th scope="col" id="tfa_mn_qty2_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_979" class="section    alternate-0 ">
<th scope="row" id="tfa_979-L" class="headerCol">MN Qty2 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty2_quan-D"><div class="inputWrapper"><input type="text" id="tfa_mn_qty2_quan" name="tfa_mn_qty2_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty2_on-D"><div class="inputWrapper"><select id="tfa_mn_qty2_on" name="tfa_mn_qty2_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_982" class="">Stock UOM</option>
<option id="tfa_983" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty2_type-D"><div class="inputWrapper"><select id="tfa_mn_qty2_type" name="tfa_mn_qty2_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_985" class="">$</option>
<option id="tfa_986" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty2_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_mn_qty2_amt" name="tfa_mn_qty2_amt" value="" aria-describedby="tfa_mn_qty2_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_mn_qty2_amt-H"><span id="tfa_mn_qty2_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_988" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_mn_qty3_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_mn_qty3_on-GRID" class="">On </th>
<th scope="col" id="tfa_mn_qty3_type-GRID" class="">Type </th>
<th scope="col" id="tfa_mn_qty3_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_989" class="section    alternate-0 ">
<th scope="row" id="tfa_989-L" class="headerCol">MN Qty3 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty3_quan-D"><div class="inputWrapper"><input type="text" id="tfa_mn_qty3_quan" name="tfa_mn_qty3_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty3_on-D"><div class="inputWrapper"><select id="tfa_mn_qty3_on" name="tfa_mn_qty3_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_992" class="">Stock UOM</option>
<option id="tfa_993" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty3_type-D"><div class="inputWrapper"><select id="tfa_mn_qty3_type" name="tfa_mn_qty3_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_995" class="">$</option>
<option id="tfa_996" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty3_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_mn_qty3_amt" name="tfa_mn_qty3_amt" value="" aria-describedby="tfa_mn_qty3_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_mn_qty3_amt-H"><span id="tfa_mn_qty3_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_998" class="section group"><table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_mn_qty4_quan-GRID" class="">Quantity  </th>
<th scope="col" id="tfa_mn_qty4_on-GRID" class="">On </th>
<th scope="col" id="tfa_mn_qty4_type-GRID" class="">Type </th>
<th scope="col" id="tfa_mn_qty4_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_999" class="section    alternate-0 ">
<th scope="row" id="tfa_999-L" class="headerCol">MN Qty4 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty4_quan-D"><div class="inputWrapper"><input type="text" id="tfa_mn_qty4_quan" name="tfa_mn_qty4_quan" value="" title="Quantity " data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty4_on-D"><div class="inputWrapper"><select id="tfa_mn_qty4_on" name="tfa_mn_qty4_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1002" class="">Stock UOM</option>
<option id="tfa_1003" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty4_type-D"><div class="inputWrapper"><select id="tfa_mn_qty4_type" name="tfa_mn_qty4_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1005" class="">$</option>
<option id="tfa_1006" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty4_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_mn_qty4_amt" name="tfa_mn_qty4_amt" value="" aria-describedby="tfa_mn_qty4_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_mn_qty4_amt-H"><span id="tfa_mn_qty4_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table></div>
<div id="tfa_1008" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_mn_qty5_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_mn_qty5_on-GRID" class="">On </th>
<th scope="col" id="tfa_mn_qty5_type-GRID" class="">Type </th>
<th scope="col" id="tfa_mn_qty5_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1009" class="section    alternate-0 ">
<th scope="row" id="tfa_1009-L" class="headerCol">MN Qty5 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty5_quan-D"><div class="inputWrapper"><input type="text" id="tfa_mn_qty5_quan" name="tfa_mn_qty5_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty5_on-D"><div class="inputWrapper"><select id="tfa_mn_qty5_on" name="tfa_mn_qty5_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1012" class="">Stock UOM</option>
<option id="tfa_1013" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty5_type-D"><div class="inputWrapper"><select id="tfa_mn_qty5_type" name="tfa_mn_qty5_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1015" class="">$</option>
<option id="tfa_1016" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty5_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_mn_qty5_amt" name="tfa_mn_qty5_amt" value="" aria-describedby="tfa_mn_qty5_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_mn_qty5_amt-H"><span id="tfa_mn_qty5_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_1018" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_mn_qty6_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_mn_qty6_on-GRID" class="">On </th>
<th scope="col" id="tfa_mn_qty6_type-GRID" class="">Type </th>
<th scope="col" id="tfa_mn_qty6_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1019" class="section    alternate-0 ">
<th scope="row" id="tfa_1019-L" class="headerCol">MN Qty6 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty6_quan-D"><div class="inputWrapper"><input type="text" id="tfa_mn_qty6_quan" name="tfa_mn_qty6_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty6_on-D"><div class="inputWrapper"><select id="tfa_mn_qty6_on" name="tfa_mn_qty6_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1022" class="">Stock UOM</option>
<option id="tfa_1023" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty6_type-D"><div class="inputWrapper"><select id="tfa_mn_qty6_type" name="tfa_mn_qty6_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1025" class="">$</option>
<option id="tfa_1026" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty6_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_mn_qty6_amt" name="tfa_mn_qty6_amt" value="" aria-describedby="tfa_mn_qty6_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_mn_qty6_amt-H"><span id="tfa_mn_qty6_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_1028" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_mn_qty7_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_mn_qty7_on-GRID" class="">On </th>
<th scope="col" id="tfa_mn_qty7_type-GRID" class="">Type </th>
<th scope="col" id="tfa_mn_qty7_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1029" class="section    alternate-0 ">
<th scope="row" id="tfa_1029-L" class="headerCol">MN Qty7 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty7_quan-D"><div class="inputWrapper"><input type="text" id="tfa_mn_qty7_quan" name="tfa_mn_qty7_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty7_on-D"><div class="inputWrapper"><select id="tfa_mn_qty7_on" name="tfa_mn_qty7_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1032" class="">Stock UOM</option>
<option id="tfa_1033" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty7_type-D"><div class="inputWrapper"><select id="tfa_mn_qty7_type" name="tfa_mn_qty7_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1035" class="">$</option>
<option id="tfa_1036" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty7_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_mn_qty7_amt" name="tfa_mn_qty7_amt" value="" aria-describedby="tfa_mn_qty7_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_mn_qty7_amt-H"><span id="tfa_mn_qty7_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_1038" class="section group"><table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_mn_qty8_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_mn_qty8_on-GRID" class="">On </th>
<th scope="col" id="tfa_mn_qty8_type-GRID" class="">Type </th>
<th scope="col" id="tfa_mn_qty8_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1039" class="section    alternate-0 ">
<th scope="row" id="tfa_1039-L" class="headerCol">MN Qty8 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty8_quan-D"><div class="inputWrapper"><input type="text" id="tfa_mn_qty8_quan" name="tfa_mn_qty8_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty8_on-D"><div class="inputWrapper"><select id="tfa_mn_qty8_on" name="tfa_mn_qty8_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1042" class="">Stock UOM</option>
<option id="tfa_1043" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty8_type-D"><div class="inputWrapper"><select id="tfa_mn_qty8_type" name="tfa_mn_qty8_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1045" class="">$</option>
<option id="tfa_1046" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty8_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_mn_qty8_amt" name="tfa_mn_qty8_amt" value="" aria-describedby="tfa_mn_qty8_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_mn_qty8_amt-H"><span id="tfa_mn_qty8_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table></div>
<div id="tfa_1048" class="section group"><table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_mn_qty9_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_mn_qty9_on-GRID" class="">On </th>
<th scope="col" id="tfa_mn_qty9_type-GRID" class="">Type </th>
<th scope="col" id="tfa_mn_qty9_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1049" class="section    alternate-0 ">
<th scope="row" id="tfa_1049-L" class="headerCol">MN Qty9 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty9_quan-D"><div class="inputWrapper"><input type="text" id="tfa_mn_qty9_quan" name="tfa_mn_qty9_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty9_on-D"><div class="inputWrapper"><select id="tfa_mn_qty9_on" name="tfa_mn_qty9_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1052" class="">Stock UOM</option>
<option id="tfa_1053" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty9_type-D"><div class="inputWrapper"><select id="tfa_mn_qty9_type" name="tfa_mn_qty9_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1055" class="">$</option>
<option id="tfa_1056" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty9_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_mn_qty9_amt" name="tfa_mn_qty9_amt" value="" aria-describedby="tfa_mn_qty9_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_mn_qty9_amt-H"><span id="tfa_mn_qty9_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table></div>
<div id="tfa_1058" class="section group"><table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_mn_qty10_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_mn_qty10_on-GRID" class="">On </th>
<th scope="col" id="tfa_mn_qty10_type-GRID" class="">Type </th>
<th scope="col" id="tfa_mn_qty10_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1059" class="section    alternate-0 ">
<th scope="row" id="tfa_1059-L" class="headerCol">MN Qty10 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty10_quan-D"><div class="inputWrapper"><input type="text" id="tfa_mn_qty10_quan" name="tfa_mn_qty10_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty10_on-D"><div class="inputWrapper"><select id="tfa_mn_qty10_on" name="tfa_mn_qty10_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1062" class="">Stock UOM</option>
<option id="tfa_1063" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty10_type-D"><div class="inputWrapper"><select id="tfa_mn_qty10_type" name="tfa_mn_qty10_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1065" class="">$</option>
<option id="tfa_1066" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_mn_qty10_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_mn_qty10_amt" name="tfa_mn_qty10_amt" value="" aria-describedby="tfa_mn_qty10_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_mn_qty10_amt-H"><span id="tfa_mn_qty10_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</fieldset>
<fieldset id="tfa_1188" class="section" data-condition="`#tfa_30`">
<legend id="tfa_1188-L">NJ Price Posting</legend>
<div id="tfa_1189" class="section inline group">
<div class="oneField field-container-D    " id="tfa_nj_change_type-D">
<label id="tfa_nj_change_type-L" class="label preField reqMark" for="tfa_nj_change_type"><b>NJ No Change / Add / Update / Delete</b></label><br><div class="inputWrapper">
<select id="tfa_nj_change_type" name="tfa_nj_change_type" title="NJ No Change / Add / Update / Delete" aria-required="true" aria-describedby="tfa_nj_change_type-HH" class="required"><option value="">Please select...</option>
<option id="tfa_1191" class="">No Change</option>
<option id="tfa_1192" class="">Add</option>
<option id="tfa_1193" class="">Update</option>
<option id="tfa_1194" class="">Delete</option></select><span class="field-hint-inactive" id="tfa_nj_change_type-H"><span id="tfa_nj_change_type-HH" class="hint">(Select "Add" for new items. "No Change" will post item at price below)</span></span>
</div>
</div>
<div class="oneField field-container-D    " id="tfa_nj_price_post-D">
<label id="tfa_nj_price_post-L" class="label preField reqMark" for="tfa_nj_price_post"><b>NJ Price Posting Active</b></label><br><div class="inputWrapper">
<select id="tfa_nj_price_post" name="tfa_nj_price_post" title="NJ Price Posting Active" aria-required="true" aria-describedby="tfa_nj_price_post-HH" class="required"><option value="">Please select...</option>
<option id="tfa_1196" data-conditionals="#tfa_1198" class="">Active</option>
<option id="tfa_1197" class="">Inactive</option></select><span class="field-hint-inactive" id="tfa_nj_price_post-H"><span id="tfa_nj_price_post-HH" class="hint">(Must be marked "Active" to post)</span></span>
</div>
</div>
</div>
<div id="tfa_1198" class="section group" data-condition="`#tfa_1196`"><div id="tfa_1199" class="section inline group">
<div class="oneField field-container-D    " id="tfa_nj_price_bot-D">
<label id="tfa_nj_price_bot-L" class="label preField reqMark" for="tfa_nj_price_bot"><b>NJ Price / Bottle</b></label><br><div class="inputWrapper"><input type="text" id="tfa_nj_price_bot" name="tfa_nj_price_bot" value="" aria-required="true" title="NJ Price / Bottle" data-dataset-allow-free-responses="" class="validate-float required"></div>
</div>
<div class="oneField field-container-D    " id="tfa_nj_price_case-D">
<label id="tfa_nj_price_case-L" class="label preField reqMark" for="tfa_nj_price_case"><b>NJ Price / Case</b></label><br><div class="inputWrapper"><input type="text" id="tfa_nj_price_case" name="tfa_nj_price_case" value="" aria-required="true" title="NJ Price / Case" data-dataset-allow-free-responses="" class="validate-float required"></div>
</div>
<div class="oneField field-container-D    " id="tfa_nj_std_cost-D">
<label id="tfa_nj_std_cost-L" class="label preField " for="tfa_nj_std_cost"><b>NJ Standard Cost</b></label><br><div class="inputWrapper"><input type="text" id="tfa_nj_std_cost" name="tfa_nj_std_cost" value="" title="NJ Standard Cost" data-dataset-allow-free-responses="" class="validate-float"></div>
</div>
<div class="oneField field-container-D    " id="tfa_nj_purch_cost-D">
<label id="tfa_nj_purch_cost-L" class="label preField " for="tfa_nj_purch_cost"><b>NJ Purchase Cost</b></label><br><div class="inputWrapper"><input type="text" id="tfa_nj_purch_cost" name="tfa_nj_purch_cost" value="" title="NJ Purchase Cost" data-dataset-allow-free-responses="" class="validate-float"></div>
</div>
<div class="oneField field-container-D    " id="tfa_nj_fob-D">
<label id="tfa_nj_fob-L" class="label preField reqMark" for="tfa_nj_fob"><b>NJ FOB</b></label><br><div class="inputWrapper">
<input type="text" id="tfa_nj_fob" name="tfa_nj_fob" value="" aria-required="true" aria-describedby="tfa_nj_fob-HH" title="NJ FOB" data-dataset-allow-free-responses="" class="validate-alphanum required"><span class="field-hint-inactive" id="tfa_nj_fob-H"><span id="tfa_nj_fob-HH" class="hint">(See instructions above)</span></span>
</div>
</div>
<div class="oneField field-container-D    " id="tfa_nj_disc-D">
<label id="tfa_nj_disc-L" class="label preField " for="tfa_nj_disc"><b>Discounts?</b></label><br><div class="inputWrapper">
<select id="tfa_nj_disc" name="tfa_nj_disc" title="Discounts?" aria-describedby="tfa_nj_disc-HH" class=""><option value="">Please select...</option>
<option id="tfa_1206" data-conditionals="#tfa_1207" class="">Yes</option></select><span class="field-hint-inactive" id="tfa_nj_disc-H"><span id="tfa_nj_disc-HH" class="hint">(Select "Yes" to modify discounts)</span></span>
</div>
</div>
</div></div>
<div id="tfa_1207" class="section group" data-condition="`#tfa_1206`">
<label class="label preField" id="tfa_1207-L"><u style=""><b>Discounts</b></u></label><br><div id="tfa_1794" class="section inline group">
<div class="oneField field-container-D    " id="tfa_nj_mix_match-D">
<label id="tfa_nj_mix_match-L" class="label preField " for="tfa_nj_mix_match"><b>NJ Mix Match</b></label><br><div class="inputWrapper"><select id="tfa_nj_mix_match" name="tfa_nj_mix_match" title="NJ Mix Match" class=""><option value="">Please select...</option>
<option id="tfa_1789" class="">Yes</option></select></div>
</div>
<div class="oneField field-container-D    " id="tfa_nj_fam_price-D">
<label id="tfa_nj_fam_price-L" class="label preField " for="tfa_nj_fam_price"><b>NJ Family Pricing</b></label><br><div class="inputWrapper"><select id="tfa_nj_fam_price" name="tfa_nj_fam_price" title="NJ Family Pricing" class=""><option value="">Please select...</option>
<option id="tfa_1793" class="">Yes</option></select></div>
</div>
</div>
<div id="tfa_1208" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_nj_qty1_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_nj_qty1_on-GRID" class="">On </th>
<th scope="col" id="tfa_nj_qty1_type-GRID" class="">Type </th>
<th scope="col" id="tfa_nj_qty1_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1209" class="section    alternate-0 ">
<th scope="row" id="tfa_1209-L" class="headerCol">NJ Qty1 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty1_quan-D"><div class="inputWrapper"><input type="text" id="tfa_nj_qty1_quan" name="tfa_nj_qty1_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty1_on-D"><div class="inputWrapper"><select id="tfa_nj_qty1_on" name="tfa_nj_qty1_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1212" class="">Stock UOM</option>
<option id="tfa_1213" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty1_type-D"><div class="inputWrapper"><select id="tfa_nj_qty1_type" name="tfa_nj_qty1_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1215" class="">$</option>
<option id="tfa_1216" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty1_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_nj_qty1_amt" name="tfa_nj_qty1_amt" value="" aria-describedby="tfa_nj_qty1_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_nj_qty1_amt-H"><span id="tfa_nj_qty1_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_1218" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_nj_qty2_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_nj_qty2_on-GRID" class="">On </th>
<th scope="col" id="tfa_nj_qty2_type-GRID" class="">Type </th>
<th scope="col" id="tfa_nj_qty2_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1219" class="section    alternate-0 ">
<th scope="row" id="tfa_1219-L" class="headerCol">NJ Qty2 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty2_quan-D"><div class="inputWrapper"><input type="text" id="tfa_nj_qty2_quan" name="tfa_nj_qty2_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty2_on-D"><div class="inputWrapper"><select id="tfa_nj_qty2_on" name="tfa_nj_qty2_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1222" class="">Stock UOM</option>
<option id="tfa_1223" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty2_type-D"><div class="inputWrapper"><select id="tfa_nj_qty2_type" name="tfa_nj_qty2_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1225" class="">$</option>
<option id="tfa_1226" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty2_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_nj_qty2_amt" name="tfa_nj_qty2_amt" value="" aria-describedby="tfa_nj_qty2_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_nj_qty2_amt-H"><span id="tfa_nj_qty2_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_1228" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_nj_qty3_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_nj_qty3_on-GRID" class="">On </th>
<th scope="col" id="tfa_nj_qty3_type-GRID" class="">Type </th>
<th scope="col" id="tfa_nj_qty3_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1229" class="section    alternate-0 ">
<th scope="row" id="tfa_1229-L" class="headerCol">NJ Qty3 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty3_quan-D"><div class="inputWrapper"><input type="text" id="tfa_nj_qty3_quan" name="tfa_nj_qty3_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty3_on-D"><div class="inputWrapper"><select id="tfa_nj_qty3_on" name="tfa_nj_qty3_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1232" class="">Stock UOM</option>
<option id="tfa_1233" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty3_type-D"><div class="inputWrapper"><select id="tfa_nj_qty3_type" name="tfa_nj_qty3_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1235" class="">$</option>
<option id="tfa_1236" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty3_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_nj_qty3_amt" name="tfa_nj_qty3_amt" value="" aria-describedby="tfa_nj_qty3_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_nj_qty3_amt-H"><span id="tfa_nj_qty3_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_1238" class="section group"><table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_nj_qty4_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_nj_qty4_on-GRID" class="">On </th>
<th scope="col" id="tfa_nj_qty4_type-GRID" class="">Type </th>
<th scope="col" id="tfa_nj_qty4_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1239" class="section    alternate-0 ">
<th scope="row" id="tfa_1239-L" class="headerCol">NJ Qty4 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty4_quan-D"><div class="inputWrapper"><input type="text" id="tfa_nj_qty4_quan" name="tfa_nj_qty4_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty4_on-D"><div class="inputWrapper"><select id="tfa_nj_qty4_on" name="tfa_nj_qty4_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1242" class="">Stock UOM</option>
<option id="tfa_1243" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty4_type-D"><div class="inputWrapper"><select id="tfa_nj_qty4_type" name="tfa_nj_qty4_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1245" class="">$</option>
<option id="tfa_1246" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty4_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_nj_qty4_amt" name="tfa_nj_qty4_amt" value="" aria-describedby="tfa_nj_qty4_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_nj_qty4_amt-H"><span id="tfa_nj_qty4_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table></div>
<div id="tfa_1248" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_nj_qty5_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_nj_qty5_on-GRID" class="">On </th>
<th scope="col" id="tfa_nj_qty5_type-GRID" class="">Type </th>
<th scope="col" id="tfa_nj_qty5_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1249" class="section    alternate-0 ">
<th scope="row" id="tfa_1249-L" class="headerCol">NJ Qty5 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty5_quan-D"><div class="inputWrapper"><input type="text" id="tfa_nj_qty5_quan" name="tfa_nj_qty5_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty5_on-D"><div class="inputWrapper"><select id="tfa_nj_qty5_on" name="tfa_nj_qty5_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1252" class="">Stock UOM</option>
<option id="tfa_1253" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty5_type-D"><div class="inputWrapper"><select id="tfa_nj_qty5_type" name="tfa_nj_qty5_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1255" class="">$</option>
<option id="tfa_1256" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty5_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_nj_qty5_amt" name="tfa_nj_qty5_amt" value="" aria-describedby="tfa_nj_qty5_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_nj_qty5_amt-H"><span id="tfa_nj_qty5_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_1258" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_nj_qty6_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_nj_qty6_on-GRID" class="">On </th>
<th scope="col" id="tfa_nj_qty6_type-GRID" class="">Type </th>
<th scope="col" id="tfa_nj_qty6_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1259" class="section    alternate-0 ">
<th scope="row" id="tfa_1259-L" class="headerCol">NJ Qty6 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty6_quan-D"><div class="inputWrapper"><input type="text" id="tfa_nj_qty6_quan" name="tfa_nj_qty6_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty6_on-D"><div class="inputWrapper"><select id="tfa_nj_qty6_on" name="tfa_nj_qty6_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1262" class="">Stock UOM</option>
<option id="tfa_1263" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty6_type-D"><div class="inputWrapper"><select id="tfa_nj_qty6_type" name="tfa_nj_qty6_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1265" class="">$</option>
<option id="tfa_1266" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty6_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_nj_qty6_amt" name="tfa_nj_qty6_amt" value="" aria-describedby="tfa_nj_qty6_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_nj_qty6_amt-H"><span id="tfa_nj_qty6_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_1268" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_nj_qty7_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_nj_qty7_on-GRID" class="">On </th>
<th scope="col" id="tfa_nj_qty7_type-GRID" class="">Type </th>
<th scope="col" id="tfa_nj_qty7_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1269" class="section    alternate-0 ">
<th scope="row" id="tfa_1269-L" class="headerCol">NJ Qty7 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty7_quan-D"><div class="inputWrapper"><input type="text" id="tfa_nj_qty7_quan" name="tfa_nj_qty7_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty7_on-D"><div class="inputWrapper"><select id="tfa_nj_qty7_on" name="tfa_nj_qty7_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1272" class="">Stock UOM</option>
<option id="tfa_1273" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty7_type-D"><div class="inputWrapper"><select id="tfa_nj_qty7_type" name="tfa_nj_qty7_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1275" class="">$</option>
<option id="tfa_1276" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty7_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_nj_qty7_amt" name="tfa_nj_qty7_amt" value="" aria-describedby="tfa_nj_qty7_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_nj_qty7_amt-H"><span id="tfa_nj_qty7_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_1278" class="section group"><table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_nj_qty8_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_nj_qty8_on-GRID" class="">On </th>
<th scope="col" id="tfa_nj_qty8_type-GRID" class="">Type </th>
<th scope="col" id="tfa_nj_qty8_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1279" class="section    alternate-0 ">
<th scope="row" id="tfa_1279-L" class="headerCol">NJ Qty8 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty8_quan-D"><div class="inputWrapper"><input type="text" id="tfa_nj_qty8_quan" name="tfa_nj_qty8_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty8_on-D"><div class="inputWrapper"><select id="tfa_nj_qty8_on" name="tfa_nj_qty8_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1282" class="">Stock UOM</option>
<option id="tfa_1283" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty8_type-D"><div class="inputWrapper"><select id="tfa_nj_qty8_type" name="tfa_nj_qty8_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1285" class="">$</option>
<option id="tfa_1286" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty8_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_nj_qty8_amt" name="tfa_nj_qty8_amt" value="" aria-describedby="tfa_nj_qty8_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_nj_qty8_amt-H"><span id="tfa_nj_qty8_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table></div>
<div id="tfa_1288" class="section group"><table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_nj_qty9_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_nj_qty9_on-GRID" class="">On </th>
<th scope="col" id="tfa_nj_qty9_type-GRID" class="">Type </th>
<th scope="col" id="tfa_nj_qty9_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1289" class="section    alternate-0 ">
<th scope="row" id="tfa_1289-L" class="headerCol">NJ Qty9 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty9_quan-D"><div class="inputWrapper"><input type="text" id="tfa_nj_qty9_quan" name="tfa_nj_qty9_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty9_on-D"><div class="inputWrapper"><select id="tfa_nj_qty9_on" name="tfa_nj_qty9_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1292" class="">Stock UOM</option>
<option id="tfa_1293" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty9_type-D"><div class="inputWrapper"><select id="tfa_nj_qty9_type" name="tfa_nj_qty9_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1295" class="">$</option>
<option id="tfa_1296" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty9_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_nj_qty9_amt" name="tfa_nj_qty9_amt" value="" aria-describedby="tfa_nj_qty9_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_nj_qty9_amt-H"><span id="tfa_nj_qty9_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table></div>
<div id="tfa_1298" class="section group"><table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_nj_qty10_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_nj_qty10_on-GRID" class="">On </th>
<th scope="col" id="tfa_nj_qty10_type-GRID" class="">Type </th>
<th scope="col" id="tfa_nj_qty10_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1299" class="section    alternate-0 ">
<th scope="row" id="tfa_1299-L" class="headerCol">NJ Qty10 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty10_quan-D"><div class="inputWrapper"><input type="text" id="tfa_nj_qty10_quan" name="tfa_nj_qty10_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty10_on-D"><div class="inputWrapper"><select id="tfa_nj_qty10_on" name="tfa_nj_qty10_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1302" class="">Stock UOM</option>
<option id="tfa_1303" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty10_type-D"><div class="inputWrapper"><select id="tfa_nj_qty10_type" name="tfa_nj_qty10_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1305" class="">$</option>
<option id="tfa_1306" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nj_qty10_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_nj_qty10_amt" name="tfa_nj_qty10_amt" value="" aria-describedby="tfa_nj_qty10_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_nj_qty10_amt-H"><span id="tfa_nj_qty10_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</fieldset>
<fieldset id="tfa_1428" class="section" data-condition="`#tfa_38`">
<legend id="tfa_1428-L">NY Retail Price Posting</legend>
<div id="tfa_1429" class="section inline group">
<div class="oneField field-container-D    " id="tfa_nyr_change_type-D">
<label id="tfa_nyr_change_type-L" class="label preField reqMark" for="tfa_nyr_change_type"><b>NYR No Change / Add / Update / Delete</b></label><br><div class="inputWrapper">
<select id="tfa_nyr_change_type" name="tfa_nyr_change_type" title="NYR No Change / Add / Update / Delete" aria-required="true" aria-describedby="tfa_nyr_change_type-HH" class="required"><option value="">Please select...</option>
<option id="tfa_1431" class="">No Change</option>
<option id="tfa_1432" class="">Add</option>
<option id="tfa_1433" class="">Update</option>
<option id="tfa_1434" class="">Delete</option></select><span class="field-hint-inactive" id="tfa_nyr_change_type-H"><span id="tfa_nyr_change_type-HH" class="hint">(Select "Add" for new items. "No Change" will post item at price below)</span></span>
</div>
</div>
<div class="oneField field-container-D    " id="tfa_nyr_price_post-D">
<label id="tfa_nyr_price_post-L" class="label preField reqMark" for="tfa_nyr_price_post"><b>NYR Price Posting Active</b></label><br><div class="inputWrapper">
<select id="tfa_nyr_price_post" name="tfa_nyr_price_post" title="NYR Price Posting Active" aria-required="true" aria-describedby="tfa_nyr_price_post-HH" class="required"><option value="">Please select...</option>
<option id="tfa_1436" data-conditionals="#tfa_1438" class="">Active</option>
<option id="tfa_1437" class="">Inactive</option></select><span class="field-hint-inactive" id="tfa_nyr_price_post-H"><span id="tfa_nyr_price_post-HH" class="hint">(Must be marked "Active" to post)</span></span>
</div>
</div>
</div>
<div id="tfa_1438" class="section group" data-condition="`#tfa_1436`">
<div id="tfa_1439" class="section inline group">
<div class="oneField field-container-D    " id="tfa_nyr_price_bot-D">
<label id="tfa_nyr_price_bot-L" class="label preField reqMark" for="tfa_nyr_price_bot"><b>NYR Price / Bottle</b></label><br><div class="inputWrapper"><input type="text" id="tfa_nyr_price_bot" name="tfa_nyr_price_bot" value="" aria-required="true" title="NYR Price / Bottle" data-dataset-allow-free-responses="" class="validate-float required"></div>
</div>
<div class="oneField field-container-D    " id="tfa_nyr_price_case-D">
<label id="tfa_nyr_price_case-L" class="label preField reqMark" for="tfa_nyr_price_case"><b>NYR Price / Case</b></label><br><div class="inputWrapper"><input type="text" id="tfa_nyr_price_case" name="tfa_nyr_price_case" value="" aria-required="true" title="NYR Price / Case" data-dataset-allow-free-responses="" class="validate-float required"></div>
</div>
<div class="oneField field-container-D    " id="tfa_nyr_std_cost-D">
<label id="tfa_nyr_std_cost-L" class="label preField " for="tfa_nyr_std_cost"><b>NYR Standard Cost</b></label><br><div class="inputWrapper"><input type="text" id="tfa_nyr_std_cost" name="tfa_nyr_std_cost" value="" title="NYR Standard Cost" data-dataset-allow-free-responses="" class="validate-float"></div>
</div>
<div class="oneField field-container-D    " id="tfa_nyr_purch_cost-D">
<label id="tfa_nyr_purch_cost-L" class="label preField " for="tfa_nyr_purch_cost"><b>NYR Purchase Cost</b></label><br><div class="inputWrapper"><input type="text" id="tfa_nyr_purch_cost" name="tfa_nyr_purch_cost" value="" title="NYR Purchase Cost" data-dataset-allow-free-responses="" class="validate-float"></div>
</div>
<div class="oneField field-container-D    " id="tfa_nyr_fob-D">
<label id="tfa_nyr_fob-L" class="label preField reqMark" for="tfa_nyr_fob"><b>NYR FOB</b></label><br><div class="inputWrapper">
<input type="text" id="tfa_nyr_fob" name="tfa_nyr_fob" value="" aria-required="true" aria-describedby="tfa_nyr_fob-HH" title="NYR FOB" data-dataset-allow-free-responses="" class="validate-alphanum required"><span class="field-hint-inactive" id="tfa_nyr_fob-H"><span id="tfa_nyr_fob-HH" class="hint">(See instructions above)</span></span>
</div>
</div>
</div>
<div class="oneField field-container-D    " id="tfa_nyr_disc-D">
<label id="tfa_nyr_disc-L" class="label preField " for="tfa_nyr_disc"><b>Discounts?</b></label><br><div class="inputWrapper">
<select id="tfa_nyr_disc" name="tfa_nyr_disc" title="Discounts?" aria-describedby="tfa_nyr_disc-HH" class=""><option value="">Please select...</option>
<option id="tfa_1446" data-conditionals="#tfa_1447" class="">Yes</option></select><span class="field-hint-inactive" id="tfa_nyr_disc-H"><span id="tfa_nyr_disc-HH" class="hint">(Select "Yes" to modify discounts)</span></span>
</div>
</div>
</div>
<div id="tfa_1447" class="section group" data-condition="`#tfa_1446`">
<label class="label preField" id="tfa_1447-L"><u style=""><b>Discounts</b></u></label><br><div id="tfa_1448" class="section group">
<div class="oneField field-container-D    " id="tfa_nyr_mix_match-D">
<label id="tfa_nyr_mix_match-L" class="label preField " for="tfa_nyr_mix_match"><b>NYR Mix Match</b></label><br><div class="inputWrapper"><select id="tfa_nyr_mix_match" name="tfa_nyr_mix_match" title="NYR Mix Match" class=""><option value="">Please select...</option>
<option id="tfa_1796" class="">Yes</option></select></div>
</div>
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_nyr_qty1_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_nyr_qty1_on-GRID" class="">On </th>
<th scope="col" id="tfa_nyr_qty1_type-GRID" class="">Type </th>
<th scope="col" id="tfa_nyr_qty1_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1449" class="section    alternate-0 ">
<th scope="row" id="tfa_1449-L" class="headerCol">NYR Qty1 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty1_quan-D"><div class="inputWrapper"><input type="text" id="tfa_nyr_qty1_quan" name="tfa_nyr_qty1_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty1_on-D"><div class="inputWrapper"><select id="tfa_nyr_qty1_on" name="tfa_nyr_qty1_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1452" class="">Stock UOM</option>
<option id="tfa_1453" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty1_type-D"><div class="inputWrapper"><select id="tfa_nyr_qty1_type" name="tfa_nyr_qty1_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1455" class="">$</option>
<option id="tfa_1456" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty1_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_nyr_qty1_amt" name="tfa_nyr_qty1_amt" value="" aria-describedby="tfa_nyr_qty1_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_nyr_qty1_amt-H"><span id="tfa_nyr_qty1_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_1458" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_nyr_qty2_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_nyr_qty2_on-GRID" class="">On </th>
<th scope="col" id="tfa_nyr_qty2_type-GRID" class="">Type </th>
<th scope="col" id="tfa_nyr_qty2_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1459" class="section    alternate-0 ">
<th scope="row" id="tfa_1459-L" class="headerCol">NYR Qty2 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty2_quan-D"><div class="inputWrapper"><input type="text" id="tfa_nyr_qty2_quan" name="tfa_nyr_qty2_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty2_on-D"><div class="inputWrapper"><select id="tfa_nyr_qty2_on" name="tfa_nyr_qty2_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1462" class="">Stock UOM</option>
<option id="tfa_1463" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty2_type-D"><div class="inputWrapper"><select id="tfa_nyr_qty2_type" name="tfa_nyr_qty2_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1465" class="">$</option>
<option id="tfa_1466" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty2_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_nyr_qty2_amt" name="tfa_nyr_qty2_amt" value="" aria-describedby="tfa_nyr_qty2_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_nyr_qty2_amt-H"><span id="tfa_nyr_qty2_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_1468" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_nyr_qty3_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_nyr_qty3_on-GRID" class="">On </th>
<th scope="col" id="tfa_nyr_qty3_type-GRID" class="">Type </th>
<th scope="col" id="tfa_nyr_qty3_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1469" class="section    alternate-0 ">
<th scope="row" id="tfa_1469-L" class="headerCol">NYR Qty3 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty3_quan-D"><div class="inputWrapper"><input type="text" id="tfa_nyr_qty3_quan" name="tfa_nyr_qty3_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty3_on-D"><div class="inputWrapper"><select id="tfa_nyr_qty3_on" name="tfa_nyr_qty3_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1472" class="">Stock UOM</option>
<option id="tfa_1473" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty3_type-D"><div class="inputWrapper"><select id="tfa_nyr_qty3_type" name="tfa_nyr_qty3_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1475" class="">$</option>
<option id="tfa_1476" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty3_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_nyr_qty3_amt" name="tfa_nyr_qty3_amt" value="" aria-describedby="tfa_nyr_qty3_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_nyr_qty3_amt-H"><span id="tfa_nyr_qty3_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_1478" class="section group"><table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_nyr_qty4_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_nyr_qty4_on-GRID" class="">On </th>
<th scope="col" id="tfa_nyr_qty4_type-GRID" class="">Type </th>
<th scope="col" id="tfa_nyr_qty4_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1479" class="section    alternate-0 ">
<th scope="row" id="tfa_1479-L" class="headerCol">NYR Qty4 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty4_quan-D"><div class="inputWrapper"><input type="text" id="tfa_nyr_qty4_quan" name="tfa_nyr_qty4_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty4_on-D"><div class="inputWrapper"><select id="tfa_nyr_qty4_on" name="tfa_nyr_qty4_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1482" class="">Stock UOM</option>
<option id="tfa_1483" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty4_type-D"><div class="inputWrapper"><select id="tfa_nyr_qty4_type" name="tfa_nyr_qty4_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1485" class="">$</option>
<option id="tfa_1486" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty4_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_nyr_qty4_amt" name="tfa_nyr_qty4_amt" value="" aria-describedby="tfa_nyr_qty4_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_nyr_qty4_amt-H"><span id="tfa_nyr_qty4_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table></div>
<div id="tfa_1488" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_nyr_qty5_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_nyr_qty5_on-GRID" class="">On </th>
<th scope="col" id="tfa_nyr_qty5_type-GRID" class="">Type </th>
<th scope="col" id="tfa_nyr_qty5_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1489" class="section    alternate-0 ">
<th scope="row" id="tfa_1489-L" class="headerCol">NYR Qty5 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty5_quan-D"><div class="inputWrapper"><input type="text" id="tfa_nyr_qty5_quan" name="tfa_nyr_qty5_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty5_on-D"><div class="inputWrapper"><select id="tfa_nyr_qty5_on" name="tfa_nyr_qty5_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1492" class="">Stock UOM</option>
<option id="tfa_1493" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty5_type-D"><div class="inputWrapper"><select id="tfa_nyr_qty5_type" name="tfa_nyr_qty5_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1495" class="">$</option>
<option id="tfa_1496" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty5_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_nyr_qty5_amt" name="tfa_nyr_qty5_amt" value="" aria-describedby="tfa_nyr_qty5_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_nyr_qty5_amt-H"><span id="tfa_nyr_qty5_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_1498" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_nyr_qty6_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_nyr_qty6_on-GRID" class="">On </th>
<th scope="col" id="tfa_nyr_qty6_type-GRID" class="">Type </th>
<th scope="col" id="tfa_nyr_qty6_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1499" class="section    alternate-0 ">
<th scope="row" id="tfa_1499-L" class="headerCol">NYR Qty6 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty6_quan-D"><div class="inputWrapper"><input type="text" id="tfa_nyr_qty6_quan" name="tfa_nyr_qty6_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty6_on-D"><div class="inputWrapper"><select id="tfa_nyr_qty6_on" name="tfa_nyr_qty6_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1502" class="">Stock UOM</option>
<option id="tfa_1503" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty6_type-D"><div class="inputWrapper"><select id="tfa_nyr_qty6_type" name="tfa_nyr_qty6_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1505" class="">$</option>
<option id="tfa_1506" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty6_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_nyr_qty6_amt" name="tfa_nyr_qty6_amt" value="" aria-describedby="tfa_nyr_qty6_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_nyr_qty6_amt-H"><span id="tfa_nyr_qty6_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_1508" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_nyr_qty7_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_nyr_qty7_on-GRID" class="">On </th>
<th scope="col" id="tfa_nyr_qty7_type-GRID" class="">Type </th>
<th scope="col" id="tfa_nyr_qty7_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1509" class="section    alternate-0 ">
<th scope="row" id="tfa_1509-L" class="headerCol">NYR Qty7 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty7_quan-D"><div class="inputWrapper"><input type="text" id="tfa_nyr_qty7_quan" name="tfa_nyr_qty7_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty7_on-D"><div class="inputWrapper"><select id="tfa_nyr_qty7_on" name="tfa_nyr_qty7_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1512" class="">Stock UOM</option>
<option id="tfa_1513" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty7_type-D"><div class="inputWrapper"><select id="tfa_nyr_qty7_type" name="tfa_nyr_qty7_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1515" class="">$</option>
<option id="tfa_1516" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty7_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_nyr_qty7_amt" name="tfa_nyr_qty7_amt" value="" aria-describedby="tfa_nyr_qty7_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_nyr_qty7_amt-H"><span id="tfa_nyr_qty7_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_1518" class="section group"><table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_nyr_qty8_quan-GRID" class="">Quantity  </th>
<th scope="col" id="tfa_nyr_qty8_on-GRID" class="">On </th>
<th scope="col" id="tfa_nyr_qty8_type-GRID" class="">Type </th>
<th scope="col" id="tfa_nyr_qty8_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1519" class="section    alternate-0 ">
<th scope="row" id="tfa_1519-L" class="headerCol">NYR Qty8 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty8_quan-D"><div class="inputWrapper"><input type="text" id="tfa_nyr_qty8_quan" name="tfa_nyr_qty8_quan" value="" title="Quantity " data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty8_on-D"><div class="inputWrapper"><select id="tfa_nyr_qty8_on" name="tfa_nyr_qty8_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1522" class="">Stock UOM</option>
<option id="tfa_1523" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty8_type-D"><div class="inputWrapper"><select id="tfa_nyr_qty8_type" name="tfa_nyr_qty8_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1525" class="">$</option>
<option id="tfa_1526" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty8_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_nyr_qty8_amt" name="tfa_nyr_qty8_amt" value="" aria-describedby="tfa_nyr_qty8_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_nyr_qty8_amt-H"><span id="tfa_nyr_qty8_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table></div>
<div id="tfa_1528" class="section group"><table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_nyr_qty9_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_nyr_qty9_on-GRID" class="">On </th>
<th scope="col" id="tfa_nyr_qty9_type-GRID" class="">Type </th>
<th scope="col" id="tfa_nyr_qty9_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1529" class="section    alternate-0 ">
<th scope="row" id="tfa_1529-L" class="headerCol">NYR Qty9 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty9_quan-D"><div class="inputWrapper"><input type="text" id="tfa_nyr_qty9_quan" name="tfa_nyr_qty9_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty9_on-D"><div class="inputWrapper"><select id="tfa_nyr_qty9_on" name="tfa_nyr_qty9_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1532" class="">Stock UOM</option>
<option id="tfa_1533" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty9_type-D"><div class="inputWrapper"><select id="tfa_nyr_qty9_type" name="tfa_nyr_qty9_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1535" class="">$</option>
<option id="tfa_1536" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty9_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_nyr_qty9_amt" name="tfa_nyr_qty9_amt" value="" aria-describedby="tfa_nyr_qty9_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_nyr_qty9_amt-H"><span id="tfa_nyr_qty9_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table></div>
<div id="tfa_1538" class="section group"><table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_nyr_qty10_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_nyr_qty10_on-GRID" class="">On </th>
<th scope="col" id="tfa_nyr_qty10_type-GRID" class="">Type </th>
<th scope="col" id="tfa_nyr_qty10_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1539" class="section    alternate-0 ">
<th scope="row" id="tfa_1539-L" class="headerCol">NYR Qty10 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty10_quan-D"><div class="inputWrapper"><input type="text" id="tfa_nyr_qty10_quan" name="tfa_nyr_qty10_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty10_on-D"><div class="inputWrapper"><select id="tfa_nyr_qty10_on" name="tfa_nyr_qty10_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1542" class="">Stock UOM</option>
<option id="tfa_1543" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty10_type-D"><div class="inputWrapper"><select id="tfa_nyr_qty10_type" name="tfa_nyr_qty10_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1545" class="">$</option>
<option id="tfa_1546" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyr_qty10_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_nyr_qty10_amt" name="tfa_nyr_qty10_amt" value="" aria-describedby="tfa_nyr_qty10_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_nyr_qty10_amt-H"><span id="tfa_nyr_qty10_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</fieldset>
<fieldset id="tfa_1668" class="section" data-condition="`#tfa_34`">
<legend id="tfa_1668-L">NY Wholesale Price Posting</legend>
<div id="tfa_1669" class="section inline group">
<div class="oneField field-container-D    " id="tfa_nyw_change_type-D">
<label id="tfa_nyw_change_type-L" class="label preField reqMark" for="tfa_nyw_change_type"><b>NYW No Change / Add / Update / Delete</b></label><br><div class="inputWrapper">
<select id="tfa_nyw_change_type" name="tfa_nyw_change_type" title="NYW No Change / Add / Update / Delete" aria-required="true" aria-describedby="tfa_nyw_change_type-HH" class="required"><option value="">Please select...</option>
<option id="tfa_1671" class="">No Change</option>
<option id="tfa_1672" class="">Add</option>
<option id="tfa_1673" class="">Update</option>
<option id="tfa_1674" class="">Delete</option></select><span class="field-hint-inactive" id="tfa_nyw_change_type-H"><span id="tfa_nyw_change_type-HH" class="hint">(Select "Add" for new items. "No Change" will post item at price below)</span></span>
</div>
</div>
<div class="oneField field-container-D    " id="tfa_nyw_price_post-D">
<label id="tfa_nyw_price_post-L" class="label preField reqMark" for="tfa_nyw_price_post"><b>NYW Price Posting Active</b></label><br><div class="inputWrapper">
<select id="tfa_nyw_price_post" name="tfa_nyw_price_post" title="NYW Price Posting Active" aria-required="true" aria-describedby="tfa_nyw_price_post-HH" class="required"><option value="">Please select...</option>
<option id="tfa_1676" data-conditionals="#tfa_1678" class="">Active</option>
<option id="tfa_1677" class="">Inactive</option></select><span class="field-hint-inactive" id="tfa_nyw_price_post-H"><span id="tfa_nyw_price_post-HH" class="hint">(Must be marked "Active" to post)</span></span>
</div>
</div>
</div>
<div id="tfa_1678" class="section group" data-condition="`#tfa_1676`">
<div id="tfa_1679" class="section inline group">
<div class="oneField field-container-D    " id="tfa_nyw_price_bot-D">
<label id="tfa_nyw_price_bot-L" class="label preField reqMark" for="tfa_nyw_price_bot"><b>NYW Price / Bottle</b></label><br><div class="inputWrapper"><input type="text" id="tfa_nyw_price_bot" name="tfa_nyw_price_bot" value="" aria-required="true" title="NYW Price / Bottle" data-dataset-allow-free-responses="" class="validate-float required"></div>
</div>
<div class="oneField field-container-D    " id="tfa_nyw_price_case-D">
<label id="tfa_nyw_price_case-L" class="label preField reqMark" for="tfa_nyw_price_case"><b>NYW Price / Case</b></label><br><div class="inputWrapper"><input type="text" id="tfa_nyw_price_case" name="tfa_nyw_price_case" value="" aria-required="true" title="NYW Price / Case" data-dataset-allow-free-responses="" class="validate-float required"></div>
</div>
<div class="oneField field-container-D    " id="tfa_nyw_std_cost-D">
<label id="tfa_nyw_std_cost-L" class="label preField " for="tfa_nyw_std_cost"><b>NYW Standard Cost</b></label><br><div class="inputWrapper"><input type="text" id="tfa_nyw_std_cost" name="tfa_nyw_std_cost" value="" title="NYW Standard Cost" data-dataset-allow-free-responses="" class="validate-float"></div>
</div>
<div class="oneField field-container-D    " id="tfa_nyw_purch_cost-D">
<label id="tfa_nyw_purch_cost-L" class="label preField " for="tfa_nyw_purch_cost"><b>NYW Purchase Cost</b></label><br><div class="inputWrapper"><input type="text" id="tfa_nyw_purch_cost" name="tfa_nyw_purch_cost" value="" title="NYW Purchase Cost" data-dataset-allow-free-responses="" class="validate-float"></div>
</div>
<div class="oneField field-container-D    " id="tfa_nyw_fob-D">
<label id="tfa_nyw_fob-L" class="label preField reqMark" for="tfa_nyw_fob"><b>NYW FOB</b></label><br><div class="inputWrapper">
<input type="text" id="tfa_nyw_fob" name="tfa_nyw_fob" value="" aria-required="true" aria-describedby="tfa_nyw_fob-HH" title="NYW FOB" data-dataset-allow-free-responses="" class="validate-alphanum required"><span class="field-hint-inactive" id="tfa_nyw_fob-H"><span id="tfa_nyw_fob-HH" class="hint">(See instructions above)</span></span>
</div>
</div>
<div class="oneField field-container-D    " id="tfa_nyw_dist-D">
<label id="tfa_nyw_dist-L" class="label preField reqMark" for="tfa_nyw_dist"><b>NY Distributor</b></label><br><div class="inputWrapper"><select id="tfa_nyw_dist" name="tfa_nyw_dist" title="NY Distributor" aria-required="true" class="required"><option value="">Please select...</option>
<option id="tfa_2422" class="">88 Hicksville Beer and Soda</option>
<option id="tfa_2423" class="">A.L. GEORGE-Onondaga Beverage</option>
<option id="tfa_2424" class="">Adamba Inports Int'l INC</option>
<option id="tfa_2425" class="">Amanda PIcotte</option>
<option id="tfa_2426" class="">Americas Soc-Council of the America</option>
<option id="tfa_2427" class="">Angels' Share Wines</option>
<option id="tfa_2428" class="">Apollo Fine Spirits</option>
<option id="tfa_2429" class="">Arthur R. Gren Co.</option>
<option id="tfa_2430" class="">Aviva Vino - NY</option>
<option id="tfa_2431" class="">BANVILLE AND JONES WINE MERCHANTS</option>
<option id="tfa_2432" class="">Baron Francois LTD</option>
<option id="tfa_2433" class="">Beer World Catskills</option>
<option id="tfa_2434" class="">Beers of the World</option>
<option id="tfa_2435" class="">Beverage Group International</option>
<option id="tfa_2436" class="">Beverage Island, LLC</option>
<option id="tfa_2437" class="">Biagio Cru and Estate Wines LLC</option>
<option id="tfa_2438" class="">Biba International LLC</option>
<option id="tfa_2439" class="">Big Blue Distributors Inc</option>
<option id="tfa_2440" class="">Black Sea Imports, Inc.</option>
<option id="tfa_2441" class="">Blueprint Brands</option>
<option id="tfa_2442" class="">Boening Brothers</option>
<option id="tfa_2443" class="">Bradley Alan Imports LLC</option>
<option id="tfa_2444" class="">Bravo Distributing</option>
<option id="tfa_2445" class="">Bridge Consulting Services</option>
<option id="tfa_2446" class="">Bridge Imports - C and C Wine Conne</option>
<option id="tfa_2447" class="">Brooklyn Beer and Soda Corp</option>
<option id="tfa_2448" class="">Brotherhood Winery</option>
<option id="tfa_2449" class="">Cable Beverages</option>
<option id="tfa_2450" class="">California Wine Cellars</option>
<option id="tfa_2451" class="">Cazanove Opici Wine Corp.</option>
<option id="tfa_2452" class="">Certo Brothers Distributing</option>
<option id="tfa_2453" class="">Classic NY Beer Inc.</option>
<option id="tfa_2454" class="">Clever NRG LLC</option>
<option id="tfa_2455" class="">Craft Beer Guild Distributors of NY</option>
<option id="tfa_2456" class="">Crisafulli Bev Group- Westmere</option>
<option id="tfa_2457" class="">DeCrescente Dist</option>
<option id="tfa_2458" class="">DiSalvo and Sons</option>
<option id="tfa_2459" class="">DO and CO NY Catering</option>
<option id="tfa_2460" class="">Doen Zhumir Importers and Dist.</option>
<option id="tfa_2461" class="">Domaine Select Wine Estates LLC NY</option>
<option id="tfa_2462" class="">DOZORTSEV and SONS</option>
<option id="tfa_2463" class="">Dreyfus Ashby Inc</option>
<option id="tfa_2464" class="">Duggans Distillers Products Corp</option>
<option id="tfa_2465" class="">Eagle Beverage Company Inc</option>
<option id="tfa_2466" class="">East Coast Specialty Wines Ltd.</option>
<option id="tfa_2467" class="">East Park Beverage</option>
<option id="tfa_2468" class="">Ellenville Beer and Soda</option>
<option id="tfa_2469" class="">Ellicotville Distillery</option>
<option id="tfa_2470" class="">Elmira Distributing Co.</option>
<option id="tfa_2471" class="">Empire Merchants Metro</option>
<option id="tfa_2472" class="">EMPIRE MERCHANTS NORTH LLC</option>
<option id="tfa_2473" class="">Esprit Du Vin LLC</option>
<option id="tfa_2474" class="">FANT Corporation</option>
<option id="tfa_2475" class="">FAROPIAN WINES</option>
<option id="tfa_2476" class="">Frederick Wildman and Sons Ltd</option>
<option id="tfa_2477" class="">Fruit of the Vine - NY</option>
<option id="tfa_2478" class="">Gabriella Wines</option>
<option id="tfa_2479" class="">Garal Wholesalers Ltd</option>
<option id="tfa_2480" class="">Gasko and Meyer Inc</option>
<option id="tfa_2481" class="">Global Market Supply Inc</option>
<option id="tfa_2482" class="">Goodo Beverages</option>
<option id="tfa_2483" class="">Gouli United Trade</option>
<option id="tfa_2484" class="">Grapes and Greens - J Kings Food S</option>
<option id="tfa_2485" class="">Half Time</option>
<option id="tfa_2486" class="">HIGH PEAKS DISTRIBUTING LLC</option>
<option id="tfa_2487" class="">House of Bacchus</option>
<option id="tfa_2488" class="">Industry City Distillery</option>
<option id="tfa_2489" class="">Innis and Gunn Bill Only</option>
<option id="tfa_2490" class="">International Wine Traders Inc.</option>
<option id="tfa_2491" class="">J KINGS FOOD SERVICE PROFESSIONALS</option>
<option id="tfa_2492" class="">JB-NY Distributors, Inc. - TapRm</option>
<option id="tfa_2493" class="">John J Olivers Distributors Inc</option>
<option id="tfa_2494" class="">KIP COLLECTIONS SERVICES</option>
<option id="tfa_2495" class="">Kyodo Beverage Co Inc</option>
<option id="tfa_2496" class="">LAKE BEVERAGE CORP</option>
<option id="tfa_2497" class="">Liberation Distrib. Dba Libdib - NY</option>
<option id="tfa_2498" class="">Lieber Bros Inc.</option>
<option id="tfa_2499" class="">Manhattan Beer Dist LLC</option>
<option id="tfa_2500" class="">Marble Hill Cellars</option>
<option id="tfa_2501" class="">Martin Scott Wines NY</option>
<option id="tfa_2502" class="">Massanois Imports,LLC</option>
<option id="tfa_2503" class="">McCraith Beverage</option>
<option id="tfa_2504" class="">MESIVTA AND KOLLEL BOYANE</option>
<option id="tfa_2505" class="">METRO WINE dba Orbus - NY</option>
<option id="tfa_2506" class="">METROPOLIS WINE MERCHANTS</option>
<option id="tfa_2507" class="">Mexican Radio Corp</option>
<option id="tfa_2508" class="">MFW Wine Company dba T Elenteny</option>
<option id="tfa_2509" class="">Michael Skurnik Wines NEW YORK</option>
<option id="tfa_2510" class="">Monsieur Touton Selection NY</option>
<option id="tfa_2511" class="">Mountain View Distillery</option>
<option id="tfa_2512" class="">MS Walker NY</option>
<option id="tfa_2513" class="">Nestor Imports</option>
<option id="tfa_2514" class="">New Beer King</option>
<option id="tfa_2515" class="">NOBLE HOUSE WINES</option>
<option id="tfa_2516" class="">Oak Beverage</option>
<option id="tfa_2517" class="">Ole Imports LLC</option>
<option id="tfa_2518" class="">Opici Family NY</option>
<option id="tfa_2519" class="">PADANIA WINES LLC</option>
<option id="tfa_2520" class="">Pasternak Wine Imports</option>
<option id="tfa_2521" class="">Platinum</option>
<option id="tfa_2522" class="">PM Spirits Distributing - NY</option>
<option id="tfa_2523" class="">Prospect Heights Beer Works Corp</option>
<option id="tfa_2524" class="">Rad Grapes</option>
<option id="tfa_2525" class="">Rashbi Wines Corp.</option>
<option id="tfa_2526" class="">Remy Cointreau USA</option>
<option id="tfa_2527" class="">Restaurant Depot DBA Jetro</option>
<option id="tfa_2528" class="">Roadhouse Wine Merchants</option>
<option id="tfa_2529" class="">Rogers Beer Distributor</option>
<option id="tfa_2530" class="">Royal Wines Corporation - NY</option>
<option id="tfa_2531" class="">S.K.I. Wholesale Beer Corp.</option>
<option id="tfa_2532" class="">Saratoga Eagle Sales and Services</option>
<option id="tfa_2533" class="">Sarene Craft Beer</option>
<option id="tfa_2534" class="">Savorian Inc</option>
<option id="tfa_2535" class="">SMD SELECTIONS LLC</option>
<option id="tfa_2536" class="">Solstars Inc.</option>
<option id="tfa_2537" class="">Sommelier Imports Inc</option>
<option id="tfa_2538" class="">Southern W and S AMERICA Division</option>
<option id="tfa_2539" class="">Sovereign Brands</option>
<option id="tfa_2540" class="">Spirit and Sanzone Distributing Co</option>
<option id="tfa_2541" class="">Star Quality Marketing</option>
<option id="tfa_2542" class="">Summit Selections</option>
<option id="tfa_2543" class="">Sunset Beer Distributor</option>
<option id="tfa_2544" class="">T. Edward Wines Ltd.</option>
<option id="tfa_2545" class="">Testa Wines Of The World Ltd</option>
<option id="tfa_2546" class="">The Beer Connection</option>
<option id="tfa_2547" class="">The House of Burgundy</option>
<option id="tfa_2548" class="">The Wine Trust</option>
<option id="tfa_2549" class="">TJ Sheehan</option>
<option id="tfa_2550" class="">Tri Valley Beverage Inc</option>
<option id="tfa_2551" class="">Tri-Vin Imports Inc. NY</option>
<option id="tfa_2552" class="">Triboro Beverage</option>
<option id="tfa_2553" class="">Tricana Imports</option>
<option id="tfa_2554" class="">Try-It Distributing</option>
<option id="tfa_2555" class="">Union Beer Distributors BEER ONLY</option>
<option id="tfa_2556" class="">Uno AgnecUno Agnecy</option>
<option id="tfa_2557" class="">USA Wines Imports</option>
<option id="tfa_2558" class="">Van Loans Discount Beverage</option>
<option id="tfa_2559" class="">Verity Wine Partners - NY</option>
<option id="tfa_2560" class="">Vias Imports - NY</option>
<option id="tfa_2561" class="">Vinaio Imports</option>
<option id="tfa_2562" class="">Vino Fiamma</option>
<option id="tfa_2563" class="">Vinvino Wine Company</option>
<option id="tfa_2564" class="">Vision Wine Brands LLC</option>
<option id="tfa_2565" class="">VOS SELECTIONS INC - NY</option>
<option id="tfa_2566" class="">WindmillPhoenix Beverage Inc</option>
<option id="tfa_2567" class="">WINE SYMPHONY INC. - BELL SELECTION</option>
<option id="tfa_2568" class="">Wineberry America LLC</option>
<option id="tfa_2569" class="">Winebow Inc. NY</option>
<option id="tfa_2570" class="">Wines of the World</option>
<option id="tfa_2571" class="">WLP Marketing Promotions, Inc</option>
<option id="tfa_2572" class="">Wooden Ships Wines Inc</option>
<option id="tfa_2573" class="">Wright-Wisner Distributing Corp</option>
<option id="tfa_2574" class="">XV Exclusives Inc NY</option></select></div>
</div>
</div>
<div class="oneField field-container-D    " id="tfa_nyw_disc-D">
<label id="tfa_nyw_disc-L" class="label preField " for="tfa_nyw_disc"><b>Discounts?</b></label><br><div class="inputWrapper">
<select id="tfa_nyw_disc" name="tfa_nyw_disc" title="Discounts?" aria-describedby="tfa_nyw_disc-HH" class=""><option value="">Please select...</option>
<option id="tfa_1686" data-conditionals="#tfa_1687" class="">Yes</option></select><span class="field-hint-inactive" id="tfa_nyw_disc-H"><span id="tfa_nyw_disc-HH" class="hint">(Select "Yes" to modify discounts)</span></span>
</div>
</div>
</div>
<div id="tfa_1687" class="section group" data-condition="`#tfa_1686`">
<label class="label preField" id="tfa_1687-L"><u style=""><b>Discounts</b></u></label><br><div id="tfa_1688" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_nyw_qty1_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_nyw_qty1_on-GRID" class="">On </th>
<th scope="col" id="tfa_nyw_qty1_type-GRID" class="">Type </th>
<th scope="col" id="tfa_nyw_qty1_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1689" class="section    alternate-0 ">
<th scope="row" id="tfa_1689-L" class="headerCol">NYW Qty1 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty1_quan-D"><div class="inputWrapper"><input type="text" id="tfa_nyw_qty1_quan" name="tfa_nyw_qty1_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty1_on-D"><div class="inputWrapper"><select id="tfa_nyw_qty1_on" name="tfa_nyw_qty1_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1692" class="">Stock UOM</option>
<option id="tfa_1693" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty1_type-D"><div class="inputWrapper"><select id="tfa_nyw_qty1_type" name="tfa_nyw_qty1_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1695" class="">$</option>
<option id="tfa_1696" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty1_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_nyw_qty1_amt" name="tfa_nyw_qty1_amt" value="" aria-describedby="tfa_nyw_qty1_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_nyw_qty1_amt-H"><span id="tfa_nyw_qty1_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_1698" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_nyw_qty2_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_nyw_qty2_on-GRID" class="">On </th>
<th scope="col" id="tfa_nyw_qty2_type-GRID" class="">Type </th>
<th scope="col" id="tfa_nyw_qty2_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1699" class="section    alternate-0 ">
<th scope="row" id="tfa_1699-L" class="headerCol">NYW Qty2 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty2_quan-D"><div class="inputWrapper"><input type="text" id="tfa_nyw_qty2_quan" name="tfa_nyw_qty2_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty2_on-D"><div class="inputWrapper"><select id="tfa_nyw_qty2_on" name="tfa_nyw_qty2_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1702" class="">Stock UOM</option>
<option id="tfa_1703" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty2_type-D"><div class="inputWrapper"><select id="tfa_nyw_qty2_type" name="tfa_nyw_qty2_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1705" class="">$</option>
<option id="tfa_1706" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty2_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_nyw_qty2_amt" name="tfa_nyw_qty2_amt" value="" aria-describedby="tfa_nyw_qty2_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_nyw_qty2_amt-H"><span id="tfa_nyw_qty2_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_1708" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_nyw_qty3_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_nyw_qty3_on-GRID" class="">On </th>
<th scope="col" id="tfa_nyw_qty3_type-GRID" class="">Type </th>
<th scope="col" id="tfa_nyw_qty3_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1709" class="section    alternate-0 ">
<th scope="row" id="tfa_1709-L" class="headerCol">NYW Qty3 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty3_quan-D"><div class="inputWrapper"><input type="text" id="tfa_nyw_qty3_quan" name="tfa_nyw_qty3_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty3_on-D"><div class="inputWrapper"><select id="tfa_nyw_qty3_on" name="tfa_nyw_qty3_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1712" class="">Stock UOM</option>
<option id="tfa_1713" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty3_type-D"><div class="inputWrapper"><select id="tfa_nyw_qty3_type" name="tfa_nyw_qty3_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1715" class="">$</option>
<option id="tfa_1716" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty3_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_nyw_qty3_amt" name="tfa_nyw_qty3_amt" value="" aria-describedby="tfa_nyw_qty3_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_nyw_qty3_amt-H"><span id="tfa_nyw_qty3_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_1718" class="section group"><table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_nyw_qty4_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_nyw_qty4_on-GRID" class="">On </th>
<th scope="col" id="tfa_nyw_qty4_type-GRID" class="">Type </th>
<th scope="col" id="tfa_nyw_qty4_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1719" class="section    alternate-0 ">
<th scope="row" id="tfa_1719-L" class="headerCol">NYW Qty4 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty4_quan-D"><div class="inputWrapper"><input type="text" id="tfa_nyw_qty4_quan" name="tfa_nyw_qty4_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty4_on-D"><div class="inputWrapper"><select id="tfa_nyw_qty4_on" name="tfa_nyw_qty4_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1722" class="">Stock UOM</option>
<option id="tfa_1723" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty4_type-D"><div class="inputWrapper"><select id="tfa_nyw_qty4_type" name="tfa_nyw_qty4_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1725" class="">$</option>
<option id="tfa_1726" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty4_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_nyw_qty4_amt" name="tfa_nyw_qty4_amt" value="" aria-describedby="tfa_nyw_qty4_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_nyw_qty4_amt-H"><span id="tfa_nyw_qty4_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table></div>
<div id="tfa_1728" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_nyw_qty5_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_nyw_qty5_on-GRID" class="">On </th>
<th scope="col" id="tfa_nyw_qty5_type-GRID" class="">Type </th>
<th scope="col" id="tfa_nyw_qty5_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1729" class="section    alternate-0 ">
<th scope="row" id="tfa_1729-L" class="headerCol">NYW Qty5 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty5_quan-D"><div class="inputWrapper"><input type="text" id="tfa_nyw_qty5_quan" name="tfa_nyw_qty5_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty5_on-D"><div class="inputWrapper"><select id="tfa_nyw_qty5_on" name="tfa_nyw_qty5_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1732" class="">Stock UOM</option>
<option id="tfa_1733" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty5_type-D"><div class="inputWrapper"><select id="tfa_nyw_qty5_type" name="tfa_nyw_qty5_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1735" class="">$</option>
<option id="tfa_1736" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty5_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_nyw_qty5_amt" name="tfa_nyw_qty5_amt" value="" aria-describedby="tfa_nyw_qty5_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_nyw_qty5_amt-H"><span id="tfa_nyw_qty5_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_1738" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_nyw_qty6_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_nyw_qty6_on-GRID" class="">On </th>
<th scope="col" id="tfa_nyw_qty6_type-GRID" class="">Type </th>
<th scope="col" id="tfa_nyw_qty6_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1739" class="section    alternate-0 ">
<th scope="row" id="tfa_1739-L" class="headerCol">NYW Qty6 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty6_quan-D"><div class="inputWrapper"><input type="text" id="tfa_nyw_qty6_quan" name="tfa_nyw_qty6_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty6_on-D"><div class="inputWrapper"><select id="tfa_nyw_qty6_on" name="tfa_nyw_qty6_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1742" class="">Stock UOM</option>
<option id="tfa_1743" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty6_type-D"><div class="inputWrapper"><select id="tfa_nyw_qty6_type" name="tfa_nyw_qty6_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1745" class="">$</option>
<option id="tfa_1746" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty6_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_nyw_qty6_amt" name="tfa_nyw_qty6_amt" value="" aria-describedby="tfa_nyw_qty6_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_nyw_qty6_amt-H"><span id="tfa_nyw_qty6_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_1748" class="section group">
<table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_nyw_qty7_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_nyw_qty7_on-GRID" class="">On </th>
<th scope="col" id="tfa_nyw_qty7_type-GRID" class="">Type </th>
<th scope="col" id="tfa_nyw_qty7_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1749" class="section    alternate-0 ">
<th scope="row" id="tfa_1749-L" class="headerCol">NYW Qty7 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty7_quan-D"><div class="inputWrapper"><input type="text" id="tfa_nyw_qty7_quan" name="tfa_nyw_qty7_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty7_on-D"><div class="inputWrapper"><select id="tfa_nyw_qty7_on" name="tfa_nyw_qty7_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1752" class="">Stock UOM</option>
<option id="tfa_1753" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty7_type-D"><div class="inputWrapper"><select id="tfa_nyw_qty7_type" name="tfa_nyw_qty7_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1755" class="">$</option>
<option id="tfa_1756" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty7_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_nyw_qty7_amt" name="tfa_nyw_qty7_amt" value="" aria-describedby="tfa_nyw_qty7_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_nyw_qty7_amt-H"><span id="tfa_nyw_qty7_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table>
<div id="tfa_1758" class="section group"><table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_nyw_qty8_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_nyw_qty8_on-GRID" class="">On </th>
<th scope="col" id="tfa_nyw_qty8_type-GRID" class="">Type </th>
<th scope="col" id="tfa_nyw_qty8_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1759" class="section    alternate-0 ">
<th scope="row" id="tfa_1759-L" class="headerCol">NYW Qty8 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty8_quan-D"><div class="inputWrapper"><input type="text" id="tfa_nyw_qty8_quan" name="tfa_nyw_qty8_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty8_on-D"><div class="inputWrapper"><select id="tfa_nyw_qty8_on" name="tfa_nyw_qty8_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1762" class="">Stock UOM</option>
<option id="tfa_1763" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty8_type-D"><div class="inputWrapper"><select id="tfa_nyw_qty8_type" name="tfa_nyw_qty8_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1765" class="">$</option>
<option id="tfa_1766" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty8_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_nyw_qty8_amt" name="tfa_nyw_qty8_amt" value="" aria-describedby="tfa_nyw_qty8_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_nyw_qty8_amt-H"><span id="tfa_nyw_qty8_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table></div>
<div id="tfa_1768" class="section group"><table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_nyw_qty9_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_nyw_qty9_on-GRID" class="">On </th>
<th scope="col" id="tfa_nyw_qty9_type-GRID" class="">Type </th>
<th scope="col" id="tfa_nyw_qty9_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1769" class="section    alternate-0 ">
<th scope="row" id="tfa_1769-L" class="headerCol">NYW Qty9 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty9_quan-D"><div class="inputWrapper"><input type="text" id="tfa_nyw_qty9_quan" name="tfa_nyw_qty9_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty9_on-D"><div class="inputWrapper"><select id="tfa_nyw_qty9_on" name="tfa_nyw_qty9_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1772" class="">Stock UOM</option>
<option id="tfa_1773" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty9_type-D"><div class="inputWrapper"><select id="tfa_nyw_qty9_type" name="tfa_nyw_qty9_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1775" class="">$</option>
<option id="tfa_1776" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty9_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_nyw_qty9_amt" name="tfa_nyw_qty9_amt" value="" aria-describedby="tfa_nyw_qty9_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_nyw_qty9_amt-H"><span id="tfa_nyw_qty9_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table></div>
<div id="tfa_1778" class="section group"><table class="gridLayout ">
<thead><tr class="headerRow">
<th> </th>
<th scope="col" id="tfa_nyw_qty10_quan-GRID" class="">Quantity </th>
<th scope="col" id="tfa_nyw_qty10_on-GRID" class="">On </th>
<th scope="col" id="tfa_nyw_qty10_type-GRID" class="">Type </th>
<th scope="col" id="tfa_nyw_qty10_amt-GRID" class="">Amount </th>
</tr></thead>
<tbody><tr id="tfa_1779" class="section    alternate-0 ">
<th scope="row" id="tfa_1779-L" class="headerCol">NYW Qty10 </th>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty10_quan-D"><div class="inputWrapper"><input type="text" id="tfa_nyw_qty10_quan" name="tfa_nyw_qty10_quan" value="" title="Quantity" data-dataset-allow-free-responses="" class=""></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty10_on-D"><div class="inputWrapper"><select id="tfa_nyw_qty10_on" name="tfa_nyw_qty10_on" title="On" class=""><option value="">Please select...</option>
<option id="tfa_1782" class="">Stock UOM</option>
<option id="tfa_1783" class="">Container</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty10_type-D"><div class="inputWrapper"><select id="tfa_nyw_qty10_type" name="tfa_nyw_qty10_type" title="Type" class=""><option value="">Please select...</option>
<option id="tfa_1785" class="">$</option>
<option id="tfa_1786" class="">%</option></select></div></div></td>
<td class=""><div class="oneField field-container-D    " id="tfa_nyw_qty10_amt-D"><div class="inputWrapper">
<input type="text" id="tfa_nyw_qty10_amt" name="tfa_nyw_qty10_amt" value="" aria-describedby="tfa_nyw_qty10_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_nyw_qty10_amt-H"><span id="tfa_nyw_qty10_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
</div></div></td>
<td style="display: none"></td>
</tr></tbody>
</table></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</fieldset>
<fieldset id="tfa_1827" class="section" data-condition="`#tfa_1826`">
<legend id="tfa_1827-L">Other States Pricing</legend>
<div class="htmlSection" id="tfa_1828"><div class="htmlContent" id="tfa_1828-HTML">Enter up to 5 additional prices and select the states for which that price applies. Please contact priceposting@mhwltd.com if you would like to add more than 5 additional prices.</div></div>
<div id="tfa_1834" class="section inline group">
<div class="oneField field-container-D    " id="tfa_os_price1_bottle-D">
<label id="tfa_os_price1_bottle-L" class="label preField " for="tfa_os_price1_bottle"><b>Price per Bottle 1</b></label><br><div class="inputWrapper"><input type="text" id="tfa_os_price1_bottle" name="tfa_os_price1_bottle" value="" title="Price per Bottle 1" data-dataset-allow-free-responses="" class="validate-float"></div>
</div>
<div class="oneField field-container-D    " id="tfa_os_price1_states-D">
<label id="tfa_os_price1_states-L" class="label preField " for="tfa_os_price1_states"><b>Price 1 Applicable States</b></label><br><div class="inputWrapper">
<select id="tfa_os_price1_states" multiple name="tfa_os_price1_states[]" title="Price 1 Applicable States" aria-describedby="tfa_os_price1_states-HH" class=""><option value="">Please select...</option>
<option value="AL" id="tfa_states1_Alabama" class="">Alabama</option>
<option value="AK" id="tfa_states1_Alaska" class="">Alaska</option>
<option value="AZ" id="tfa_states1_Arizona" class="">Arizona</option>
<option value="AR" id="tfa_states1_Arkansas" class="">Arkansas</option>
<option value="CA" id="tfa_states1_California" class="">California</option>
<option value="CO" id="tfa_states1_Colorado" class="">Colorado</option>
<option value="CT" id="tfa_states1_Connecticut" class="">Connecticut</option>
<option value="DE" id="tfa_states1_Delaware" class="">Delaware</option>
<option value="DC" id="tfa_states1_District_of_Columbia" class="">District of Columbia</option>
<option value="FL" id="tfa_states1_Florida" class="">Florida</option>
<option value="GA" id="tfa_states1_Georgia" class="">Georgia</option>
<option value="HI" id="tfa_states1_Hawaii" class="">Hawaii</option>
<option value="ID" id="tfa_states1_Idaho" class="">Idaho</option>
<option value="IL" id="tfa_states1_Illinois" class="">Illinois</option>
<option value="IN" id="tfa_states1_Indiana" class="">Indiana</option>
<option value="IA" id="tfa_states1_Iowa" class="">Iowa</option>
<option value="KS" id="tfa_states1_Kansas" class="">Kansas</option>
<option value="KY" id="tfa_states1_Kentucky" class="">Kentucky</option>
<option value="LA" id="tfa_states1_Louisiana" class="">Louisiana</option>
<option value="ME" id="tfa_states1_Maine" class="">Maine</option>
<option value="MD" id="tfa_states1_Maryland" class="">Maryland</option>
<option value="MA" id="tfa_states1_Massachusetts" class="">Massachusetts</option>
<option value="MI" id="tfa_states1_Michigan" class="">Michigan</option>
<option value="MN" id="tfa_states1_Minnesota" class="">Minnesota</option>
<option value="MS" id="tfa_states1_Mississippi" class="">Mississippi</option>
<option value="MO" id="tfa_states1_Missouri" class="">Missouri</option>
<option value="MT" id="tfa_states1_Montana" class="">Montana</option>
<option value="NE" id="tfa_states1_Nebraska" class="">Nebraska</option>
<option value="NV" id="tfa_states1_Nevada" class="">Nevada</option>
<option value="NH" id="tfa_states1_New_Hampshire" class="">New Hampshire</option>
<option value="NJ" id="tfa_states1_New_Jersey" class="">New Jersey</option>
<option value="NM" id="tfa_states1_New_Mexico" class="">New Mexico</option>
<option value="NY" id="tfa_states1_New_York" class="">New York</option>
<option value="NC" id="tfa_states1_North_Carolina" class="">North Carolina</option>
<option value="ND" id="tfa_states1_North_Dakota" class="">North Dakota</option>
<option value="OH" id="tfa_states1_Ohio" class="">Ohio</option>
<option value="OK" id="tfa_states1_Oklahoma" class="">Oklahoma</option>
<option value="OR" id="tfa_states1_Oregon" class="">Oregon</option>
<option value="PA" id="tfa_states1_Pennsylvania" class="">Pennsylvania</option>
<option value="RI" id="tfa_states1_Rhode_Island" class="">Rhode Island</option>
<option value="SC" id="tfa_states1_South_Carolina" class="">South Carolina</option>
<option value="SD" id="tfa_states1_South_Dakota" class="">South Dakota</option>
<option value="TN" id="tfa_states1_Tennessee" class="">Tennessee</option>
<option value="TX" id="tfa_states1_Texas" class="">Texas</option>
<option value="UT" id="tfa_states1_Utah" class="">Utah</option>
<option value="VT" id="tfa_states1_Vermont" class="">Vermont</option>
<option value="VA" id="tfa_states1_Virginia" class="">Virginia</option>
<option value="WA" id="tfa_states1_Washington" class="">Washington</option>
<option value="WV" id="tfa_states1_West_Virginia" class="">West Virginia</option>
<option value="WI" id="tfa_states1_Wisconsin" class="">Wisconsin</option>
<option value="WY" id="tfa_states1_Wyoming" class="">Wyoming</option>
</select><span class="field-hint-inactive" id="tfa_os_price1_states-H"><span id="tfa_os_price1_states-HH" class="hint">(Hold down Ctrl to select multiple states)</span></span>
</div>
</div>
<div class="oneField field-container-D    " id="tfa_os_price2_bottle-D">
<label id="tfa_os_price2_bottle-L" class="label preField " for="tfa_os_price2_bottle"><b>Price per Bottle 2</b></label><br><div class="inputWrapper"><input type="text" id="tfa_os_price2_bottle" name="tfa_os_price2_bottle" value="" title="Price per Bottle 2" data-dataset-allow-free-responses="" class="validate-float"></div>
</div>
<div class="oneField field-container-D    " id="tfa_os_price2_states-D">
<label id="tfa_os_price2_states-L" class="label preField " for="tfa_os_price2_states"><b>Price 2 Applicable States</b></label><br><div class="inputWrapper">
<select id="tfa_os_price2_states" multiple name="tfa_os_price2_states[]" title="Price 2 Applicable States" aria-describedby="tfa_os_price2_states-HH" class=""><option value="">Please select...</option>
<option value="AL" id="tfa_states2_Alabama" class="">Alabama</option>
<option value="AK" id="tfa_states2_Alaska" class="">Alaska</option>
<option value="AZ" id="tfa_states2_Arizona" class="">Arizona</option>
<option value="AR" id="tfa_states2_Arkansas" class="">Arkansas</option>
<option value="CA" id="tfa_states2_California" class="">California</option>
<option value="CO" id="tfa_states2_Colorado" class="">Colorado</option>
<option value="CT" id="tfa_states2_Connecticut" class="">Connecticut</option>
<option value="DE" id="tfa_states2_Delaware" class="">Delaware</option>
<option value="DC" id="tfa_states2_District_of_Columbia" class="">District of Columbia</option>
<option value="FL" id="tfa_states2_Florida" class="">Florida</option>
<option value="GA" id="tfa_states2_Georgia" class="">Georgia</option>
<option value="HI" id="tfa_states2_Hawaii" class="">Hawaii</option>
<option value="ID" id="tfa_states2_Idaho" class="">Idaho</option>
<option value="IL" id="tfa_states2_Illinois" class="">Illinois</option>
<option value="IN" id="tfa_states2_Indiana" class="">Indiana</option>
<option value="IA" id="tfa_states2_Iowa" class="">Iowa</option>
<option value="KS" id="tfa_states2_Kansas" class="">Kansas</option>
<option value="KY" id="tfa_states2_Kentucky" class="">Kentucky</option>
<option value="LA" id="tfa_states2_Louisiana" class="">Louisiana</option>
<option value="ME" id="tfa_states2_Maine" class="">Maine</option>
<option value="MD" id="tfa_states2_Maryland" class="">Maryland</option>
<option value="MA" id="tfa_states2_Massachusetts" class="">Massachusetts</option>
<option value="MI" id="tfa_states2_Michigan" class="">Michigan</option>
<option value="MN" id="tfa_states2_Minnesota" class="">Minnesota</option>
<option value="MS" id="tfa_states2_Mississippi" class="">Mississippi</option>
<option value="MO" id="tfa_states2_Missouri" class="">Missouri</option>
<option value="MT" id="tfa_states2_Montana" class="">Montana</option>
<option value="NE" id="tfa_states2_Nebraska" class="">Nebraska</option>
<option value="NV" id="tfa_states2_Nevada" class="">Nevada</option>
<option value="NH" id="tfa_states2_New_Hampshire" class="">New Hampshire</option>
<option value="NJ" id="tfa_states2_New_Jersey" class="">New Jersey</option>
<option value="NM" id="tfa_states2_New_Mexico" class="">New Mexico</option>
<option value="NY" id="tfa_states2_New_York" class="">New York</option>
<option value="NC" id="tfa_states2_North_Carolina" class="">North Carolina</option>
<option value="ND" id="tfa_states2_North_Dakota" class="">North Dakota</option>
<option value="OH" id="tfa_states2_Ohio" class="">Ohio</option>
<option value="OK" id="tfa_states2_Oklahoma" class="">Oklahoma</option>
<option value="OR" id="tfa_states2_Oregon" class="">Oregon</option>
<option value="PA" id="tfa_states2_Pennsylvania" class="">Pennsylvania</option>
<option value="RI" id="tfa_states2_Rhode_Island" class="">Rhode Island</option>
<option value="SC" id="tfa_states2_South_Carolina" class="">South Carolina</option>
<option value="SD" id="tfa_states2_South_Dakota" class="">South Dakota</option>
<option value="TN" id="tfa_states2_Tennessee" class="">Tennessee</option>
<option value="TX" id="tfa_states2_Texas" class="">Texas</option>
<option value="UT" id="tfa_states2_Utah" class="">Utah</option>
<option value="VT" id="tfa_states2_Vermont" class="">Vermont</option>
<option value="VA" id="tfa_states2_Virginia" class="">Virginia</option>
<option value="WA" id="tfa_states2_Washington" class="">Washington</option>
<option value="WV" id="tfa_states2_West_Virginia" class="">West Virginia</option>
<option value="WI" id="tfa_states2_Wisconsin" class="">Wisconsin</option>
<option value="WY" id="tfa_states2_Wyoming" class="">Wyoming</option>
</select><span class="field-hint-inactive" id="tfa_os_price2_states-H"><span id="tfa_os_price2_states-HH" class="hint">(Hold down Ctrl to select multiple states)</span></span>
</div>
</div>
</div>
<div id="tfa_2079" class="section inline group">
<div class="oneField field-container-D    " id="tfa_os_price3_bottle-D">
<label id="tfa_os_price3_bottle-L" class="label preField " for="tfa_os_price3_bottle"><b>Price per Bottle 3</b></label><br><div class="inputWrapper"><input type="text" id="tfa_os_price3_bottle" name="tfa_os_price3_bottle" value="" title="Price per Bottle 3" data-dataset-allow-free-responses="" class="validate-float"></div>
</div>
<div class="oneField field-container-D    " id="tfa_os_price3_states-D">
<label id="tfa_os_price3_states-L" class="label preField " for="tfa_os_price3_states"><b>Price 3 Applicable States</b></label><br><div class="inputWrapper">
<select id="tfa_os_price3_states" multiple name="tfa_os_price3_states[]" title="Price 3 Applicable States" aria-describedby="tfa_os_price3_states-HH" class=""><option value="">Please select...</option>
<option value="AL" id="tfa_states3_Alabama" class="">Alabama</option>
<option value="AK" id="tfa_states3_Alaska" class="">Alaska</option>
<option value="AZ" id="tfa_states3_Arizona" class="">Arizona</option>
<option value="AR" id="tfa_states3_Arkansas" class="">Arkansas</option>
<option value="CA" id="tfa_states3_California" class="">California</option>
<option value="CO" id="tfa_states3_Colorado" class="">Colorado</option>
<option value="CT" id="tfa_states3_Connecticut" class="">Connecticut</option>
<option value="DE" id="tfa_states3_Delaware" class="">Delaware</option>
<option value="DC" id="tfa_states3_District_of_Columbia" class="">District of Columbia</option>
<option value="FL" id="tfa_states3_Florida" class="">Florida</option>
<option value="GA" id="tfa_states3_Georgia" class="">Georgia</option>
<option value="HI" id="tfa_states3_Hawaii" class="">Hawaii</option>
<option value="ID" id="tfa_states3_Idaho" class="">Idaho</option>
<option value="IL" id="tfa_states3_Illinois" class="">Illinois</option>
<option value="IN" id="tfa_states3_Indiana" class="">Indiana</option>
<option value="IA" id="tfa_states3_Iowa" class="">Iowa</option>
<option value="KS" id="tfa_states3_Kansas" class="">Kansas</option>
<option value="KY" id="tfa_states3_Kentucky" class="">Kentucky</option>
<option value="LA" id="tfa_states3_Louisiana" class="">Louisiana</option>
<option value="ME" id="tfa_states3_Maine" class="">Maine</option>
<option value="MD" id="tfa_states3_Maryland" class="">Maryland</option>
<option value="MA" id="tfa_states3_Massachusetts" class="">Massachusetts</option>
<option value="MI" id="tfa_states3_Michigan" class="">Michigan</option>
<option value="MN" id="tfa_states3_Minnesota" class="">Minnesota</option>
<option value="MS" id="tfa_states3_Mississippi" class="">Mississippi</option>
<option value="MO" id="tfa_states3_Missouri" class="">Missouri</option>
<option value="MT" id="tfa_states3_Montana" class="">Montana</option>
<option value="NE" id="tfa_states3_Nebraska" class="">Nebraska</option>
<option value="NV" id="tfa_states3_Nevada" class="">Nevada</option>
<option value="NH" id="tfa_states3_New_Hampshire" class="">New Hampshire</option>
<option value="NJ" id="tfa_states3_New_Jersey" class="">New Jersey</option>
<option value="NM" id="tfa_states3_New_Mexico" class="">New Mexico</option>
<option value="NY" id="tfa_states3_New_York" class="">New York</option>
<option value="NC" id="tfa_states3_North_Carolina" class="">North Carolina</option>
<option value="ND" id="tfa_states3_North_Dakota" class="">North Dakota</option>
<option value="OH" id="tfa_states3_Ohio" class="">Ohio</option>
<option value="OK" id="tfa_states3_Oklahoma" class="">Oklahoma</option>
<option value="OR" id="tfa_states3_Oregon" class="">Oregon</option>
<option value="PA" id="tfa_states3_Pennsylvania" class="">Pennsylvania</option>
<option value="RI" id="tfa_states3_Rhode_Island" class="">Rhode Island</option>
<option value="SC" id="tfa_states3_South_Carolina" class="">South Carolina</option>
<option value="SD" id="tfa_states3_South_Dakota" class="">South Dakota</option>
<option value="TN" id="tfa_states3_Tennessee" class="">Tennessee</option>
<option value="TX" id="tfa_states3_Texas" class="">Texas</option>
<option value="UT" id="tfa_states3_Utah" class="">Utah</option>
<option value="VT" id="tfa_states3_Vermont" class="">Vermont</option>
<option value="VA" id="tfa_states3_Virginia" class="">Virginia</option>
<option value="WA" id="tfa_states3_Washington" class="">Washington</option>
<option value="WV" id="tfa_states3_West_Virginia" class="">West Virginia</option>
<option value="WI" id="tfa_states3_Wisconsin" class="">Wisconsin</option>
<option value="WY" id="tfa_states3_Wyoming" class="">Wyoming</option>
</select><span class="field-hint-inactive" id="tfa_os_price3_states-H"><span id="tfa_os_price3_states-HH" class="hint">(Hold down Ctrl to select multiple states)</span></span>
</div>
</div>
<div class="oneField field-container-D    " id="tfa_os_price4_bottle-D">
<label id="tfa_os_price4_bottle-L" class="label preField " for="tfa_os_price4_bottle"><b>Price per Bottle 4</b></label><br><div class="inputWrapper"><input type="text" id="tfa_os_price4_bottle" name="tfa_os_price4_bottle" value="" title="Price per Bottle 4" data-dataset-allow-free-responses="" class="validate-float"></div>
</div>
<div class="oneField field-container-D    " id="tfa_os_price4_states-D">
<label id="tfa_os_price4_states-L" class="label preField " for="tfa_os_price4_states"><b>Price 4 Applicable States</b></label><br><div class="inputWrapper">
<select id="tfa_os_price4_states" multiple name="tfa_os_price4_states[]" title="Price 4 Applicable States" aria-describedby="tfa_os_price4_states-HH" class=""><option value="">Please select...</option>
<option value="AL" id="tfa_states4_Alabama" class="">Alabama</option>
<option value="AK" id="tfa_states4_Alaska" class="">Alaska</option>
<option value="AZ" id="tfa_states4_Arizona" class="">Arizona</option>
<option value="AR" id="tfa_states4_Arkansas" class="">Arkansas</option>
<option value="CA" id="tfa_states4_California" class="">California</option>
<option value="CO" id="tfa_states4_Colorado" class="">Colorado</option>
<option value="CT" id="tfa_states4_Connecticut" class="">Connecticut</option>
<option value="DE" id="tfa_states4_Delaware" class="">Delaware</option>
<option value="DC" id="tfa_states4_District_of_Columbia" class="">District of Columbia</option>
<option value="FL" id="tfa_states4_Florida" class="">Florida</option>
<option value="GA" id="tfa_states4_Georgia" class="">Georgia</option>
<option value="HI" id="tfa_states4_Hawaii" class="">Hawaii</option>
<option value="ID" id="tfa_states4_Idaho" class="">Idaho</option>
<option value="IL" id="tfa_states4_Illinois" class="">Illinois</option>
<option value="IN" id="tfa_states4_Indiana" class="">Indiana</option>
<option value="IA" id="tfa_states4_Iowa" class="">Iowa</option>
<option value="KS" id="tfa_states4_Kansas" class="">Kansas</option>
<option value="KY" id="tfa_states4_Kentucky" class="">Kentucky</option>
<option value="LA" id="tfa_states4_Louisiana" class="">Louisiana</option>
<option value="ME" id="tfa_states4_Maine" class="">Maine</option>
<option value="MD" id="tfa_states4_Maryland" class="">Maryland</option>
<option value="MA" id="tfa_states4_Massachusetts" class="">Massachusetts</option>
<option value="MI" id="tfa_states4_Michigan" class="">Michigan</option>
<option value="MN" id="tfa_states4_Minnesota" class="">Minnesota</option>
<option value="MS" id="tfa_states4_Mississippi" class="">Mississippi</option>
<option value="MO" id="tfa_states4_Missouri" class="">Missouri</option>
<option value="MT" id="tfa_states4_Montana" class="">Montana</option>
<option value="NE" id="tfa_states4_Nebraska" class="">Nebraska</option>
<option value="NV" id="tfa_states4_Nevada" class="">Nevada</option>
<option value="NH" id="tfa_states4_New_Hampshire" class="">New Hampshire</option>
<option value="NJ" id="tfa_states4_New_Jersey" class="">New Jersey</option>
<option value="NM" id="tfa_states4_New_Mexico" class="">New Mexico</option>
<option value="NY" id="tfa_states4_New_York" class="">New York</option>
<option value="NC" id="tfa_states4_North_Carolina" class="">North Carolina</option>
<option value="ND" id="tfa_states4_North_Dakota" class="">North Dakota</option>
<option value="OH" id="tfa_states4_Ohio" class="">Ohio</option>
<option value="OK" id="tfa_states4_Oklahoma" class="">Oklahoma</option>
<option value="OR" id="tfa_states4_Oregon" class="">Oregon</option>
<option value="PA" id="tfa_states4_Pennsylvania" class="">Pennsylvania</option>
<option value="RI" id="tfa_states4_Rhode_Island" class="">Rhode Island</option>
<option value="SC" id="tfa_states4_South_Carolina" class="">South Carolina</option>
<option value="SD" id="tfa_states4_South_Dakota" class="">South Dakota</option>
<option value="TN" id="tfa_states4_Tennessee" class="">Tennessee</option>
<option value="TX" id="tfa_states4_Texas" class="">Texas</option>
<option value="UT" id="tfa_states4_Utah" class="">Utah</option>
<option value="VT" id="tfa_states4_Vermont" class="">Vermont</option>
<option value="VA" id="tfa_states4_Virginia" class="">Virginia</option>
<option value="WA" id="tfa_states4_Washington" class="">Washington</option>
<option value="WV" id="tfa_states4_West_Virginia" class="">West Virginia</option>
<option value="WI" id="tfa_states4_Wisconsin" class="">Wisconsin</option>
<option value="WY" id="tfa_states4_Wyoming" class="">Wyoming</option>
</select><span class="field-hint-inactive" id="tfa_os_price4_states-H"><span id="tfa_os_price4_states-HH" class="hint">(Hold down Ctrl to select multiple states)</span></span>
</div>
</div>
</div>
<div id="tfa_2277" class="section inline group">
<div class="oneField field-container-D    " id="tfa_os_price5_bottle-D">
<label id="tfa_os_price5_bottle-L" class="label preField " for="tfa_os_price5_bottle"><b>Price per Bottle 5</b></label><br><div class="inputWrapper"><input type="text" id="tfa_os_price5_bottle" name="tfa_os_price5_bottle" value="" title="Price per Bottle 5" data-dataset-allow-free-responses="" class="validate-float"></div>
</div>
<div class="oneField field-container-D    " id="tfa_os_price5_states-D">
<label id="tfa_os_price5_states-L" class="label preField " for="tfa_os_price5_states"><b>Price 5 Applicable States</b></label><br><div class="inputWrapper">
<select id="tfa_os_price5_states" multiple name="tfa_os_price5_states[]" title="Price 5 Applicable States" aria-describedby="tfa_os_price5_states-HH" class=""><option value="">Please select...</option>
<option value="AL" id="tfa_states5_Alabama" class="">Alabama</option>
<option value="AK" id="tfa_states5_Alaska" class="">Alaska</option>
<option value="AZ" id="tfa_states5_Arizona" class="">Arizona</option>
<option value="AR" id="tfa_states5_Arkansas" class="">Arkansas</option>
<option value="CA" id="tfa_states5_California" class="">California</option>
<option value="CO" id="tfa_states5_Colorado" class="">Colorado</option>
<option value="CT" id="tfa_states5_Connecticut" class="">Connecticut</option>
<option value="DE" id="tfa_states5_Delaware" class="">Delaware</option>
<option value="DC" id="tfa_states5_District_of_Columbia" class="">District of Columbia</option>
<option value="FL" id="tfa_states5_Florida" class="">Florida</option>
<option value="GA" id="tfa_states5_Georgia" class="">Georgia</option>
<option value="HI" id="tfa_states5_Hawaii" class="">Hawaii</option>
<option value="ID" id="tfa_states5_Idaho" class="">Idaho</option>
<option value="IL" id="tfa_states5_Illinois" class="">Illinois</option>
<option value="IN" id="tfa_states5_Indiana" class="">Indiana</option>
<option value="IA" id="tfa_states5_Iowa" class="">Iowa</option>
<option value="KS" id="tfa_states5_Kansas" class="">Kansas</option>
<option value="KY" id="tfa_states5_Kentucky" class="">Kentucky</option>
<option value="LA" id="tfa_states5_Louisiana" class="">Louisiana</option>
<option value="ME" id="tfa_states5_Maine" class="">Maine</option>
<option value="MD" id="tfa_states5_Maryland" class="">Maryland</option>
<option value="MA" id="tfa_states5_Massachusetts" class="">Massachusetts</option>
<option value="MI" id="tfa_states5_Michigan" class="">Michigan</option>
<option value="MN" id="tfa_states5_Minnesota" class="">Minnesota</option>
<option value="MS" id="tfa_states5_Mississippi" class="">Mississippi</option>
<option value="MO" id="tfa_states5_Missouri" class="">Missouri</option>
<option value="MT" id="tfa_states5_Montana" class="">Montana</option>
<option value="NE" id="tfa_states5_Nebraska" class="">Nebraska</option>
<option value="NV" id="tfa_states5_Nevada" class="">Nevada</option>
<option value="NH" id="tfa_states5_New_Hampshire" class="">New Hampshire</option>
<option value="NJ" id="tfa_states5_New_Jersey" class="">New Jersey</option>
<option value="NM" id="tfa_states5_New_Mexico" class="">New Mexico</option>
<option value="NY" id="tfa_states5_New_York" class="">New York</option>
<option value="NC" id="tfa_states5_North_Carolina" class="">North Carolina</option>
<option value="ND" id="tfa_states5_North_Dakota" class="">North Dakota</option>
<option value="OH" id="tfa_states5_Ohio" class="">Ohio</option>
<option value="OK" id="tfa_states5_Oklahoma" class="">Oklahoma</option>
<option value="OR" id="tfa_states5_Oregon" class="">Oregon</option>
<option value="PA" id="tfa_states5_Pennsylvania" class="">Pennsylvania</option>
<option value="RI" id="tfa_states5_Rhode_Island" class="">Rhode Island</option>
<option value="SC" id="tfa_states5_South_Carolina" class="">South Carolina</option>
<option value="SD" id="tfa_states5_South_Dakota" class="">South Dakota</option>
<option value="TN" id="tfa_states5_Tennessee" class="">Tennessee</option>
<option value="TX" id="tfa_states5_Texas" class="">Texas</option>
<option value="UT" id="tfa_states5_Utah" class="">Utah</option>
<option value="VT" id="tfa_states5_Vermont" class="">Vermont</option>
<option value="VA" id="tfa_states5_Virginia" class="">Virginia</option>
<option value="WA" id="tfa_states5_Washington" class="">Washington</option>
<option value="WV" id="tfa_states5_West_Virginia" class="">West Virginia</option>
<option value="WI" id="tfa_states5_Wisconsin" class="">Wisconsin</option>
<option value="WY" id="tfa_states5_Wyoming" class="">Wyoming</option>
</select><span class="field-hint-inactive" id="tfa_os_price5_states-H"><span id="tfa_os_price5_states-HH" class="hint">(Hold down Ctrl to select multiple states)</span></span>
</div>
</div>
</div>
</fieldset>
<div class="oneField field-container-D     wf-acl-hidden" id="tfa_1804-D">
<label id="tfa_1804-L" class="label preField " for="tfa_1804">Item Record ID</label><br><div class="inputWrapper"><input type="text" id="tfa_1804" name="tfa_1804" value="" title="Item Record ID" data-dataset-allow-free-responses="" class=""></div>
</div>
</fieldset>
<div class="oneField field-container-D     wf-acl-hidden" id="tfa_1803-D">
<label id="tfa_1803-L" class="label preField " for="tfa_1803">Account ID</label><br><div class="inputWrapper"><input type="text" id="tfa_1803" name="tfa_1803" value="" title="Account ID" data-dataset-allow-free-responses="" class=""></div>
</div>
<div class="htmlSection" id="tfa_1805"><div class="htmlContent" id="tfa_1805-HTML">Please be patient while your information is submitted. Do not exit this form.</div></div>
<div class="actions" id="4691404-A"><input type="submit" data-label="Submit" class="primaryAction" id="submit_button" value="Submit"></div>
<div style="clear:both"></div>
<input type="hidden" value="367-5d21605af94a29b3b7dda2042df088fa" name="tfa_dbCounters" id="tfa_dbCounters" autocomplete="off"><input type="hidden" value="4691404" name="tfa_dbFormId" id="tfa_dbFormId"><input type="hidden" value="" name="tfa_dbResponseId" id="tfa_dbResponseId"><input type="hidden" value="37c281428c38f8faccf75944e4bb7ac5" name="tfa_dbControl" id="tfa_dbControl"><input type="hidden" value="1554226189" name="tfa_dbTimeStarted" id="tfa_dbTimeStarted" autocomplete="off"><input type="hidden" value="64" name="tfa_dbVersionId" id="tfa_dbVersionId"><input type="hidden" value="" name="tfa_switchedoff" id="tfa_switchedoff">


<input type="hidden" name="itemID" id="itemID" value="<?php echo $itemID; ?>">
<input type="hidden" name="userID" id="userID" value="<?php echo $_SESSION['mhwltdphp_user']; ?>">

</form>
</div></div><div class="wFormFooter"><p class="supportInfo"><br></p></div>
  <p class="supportInfo" >
    

      </p>
 </div>    </div>

        <script src='js/iframe_resize_helper_internal.js'></script>
</body>

<script>
    $(document).ready(function() {
        // distributors list
        var distributors = [<?=join(",\n", array_map(function($d) {
            return "{DistKey:'".$d['DistKey']."',Name:'".addslashes(utf8_encode($d['Name']))."'}";
        }, $distributorsArray))?>];
        console.log(distributors);


    });
</script>

</html>
<?php
//}
?>