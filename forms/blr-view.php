<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    include("prepend.php");
    include("settings.php");

    if(!isset($_SESSION['mhwltdphp_user'])){
        header("Location: login.php");
    }

    include("dbconnect.php");
    include('head.php');
    $isAdmin = in_array($_SESSION['mhwltdphp_usertype'], ['ADMIN','SUPERUSER']);
    $showRefreshDistributorButton = $isAdmin || count($showDistributorsArray) > 0;
?>

<style type="text/css">
    .validation-error {background-color: #ffc2ab !important;}
</style>

<div class="container-fluid">
    <div class="row justify-content-md-left float-left">
        <div class="col-md-auto oneField field-container-D" id="client_name-D">
			<label id="client_name-L" class="label preField " for="client_name"><b>Client Name</b></label>
			<div class="inputWrapper">
				<select id="client_name" name="client_name" title="Client Name" aria-required="true">
				<?php
				$clients = explode(";",$_SESSION['mhwltdphp_userclients']);
				if (count($clients)>1) {  
                    echo "<option value='all' class=''>ALL</option>"; 
                }
				foreach ($clients as &$clientvalue) {
					echo "<option value='$clientvalue' class=''>$clientvalue</option>";
				}
				?>					
				</select>
			</div>
		</div>
		<div class="col-md-auto oneField field-container-D">
			<label class="label preField " for="view_type"><b>View Type</b></label>
			<div class="inputWrapper">
				<select id="view_type" name="view_type" title="View Type" aria-required="true">
				<?php
				foreach (array("Product Requests", "State Requests", "All Products", "All States") as $viewType) {
					echo "<option value='$viewType' >$viewType</option>";
				}
				?>					
				</select>
			</div>
		</div>
    </div>

    <table id="blrViewTable" class="table table-hover" data-toggle="table" data-pagination="false" data-show-pagination-switch="true"  data-show-columns="true" data-show-toggle="true" data-search="true" data-show-export="true" data-id-field="ID" data-editable-emptytext="...." data-editable-url="#">
        <thead>
        </thead>
        <tbody>
        </tbody>
    </table>

    <script>

        //table button
        var refreshFormatter = (value, item) => {
            return '<button type="button" class="btn btn-primary btn-sm btn-refresh" data-product_id="'+item.product_id+'">State Refresh</button>\
                    <?php if ($showRefreshDistributorButton) { ?>
                    <button type="button" class="btn btn-primary btn-sm btn-distributor-refresh" data-product_id="'+item.product_id+'">Distributor Refresh</button>\
                    <?php } ?>
                    <button type="button" class="btn btn-primary btn-sm btn-federal-refresh" data-product_id="'+item.product_id+'">Federal Refresh</button>';
        }
		var STrefreshFormatter = (value, item) => {
            return '<button type="button" class="btn btn-primary btn-sm btn-refreshST" data-product_id="'+item.product_id+'">State Refresh</button>\
                    <?php if ($showRefreshDistributorButton) { ?>
                    <button type="button" class="btn btn-primary btn-sm btn-distributor-refresh" data-product_id="'+item.product_id+'">Distributor Refresh</button>\
                    <?php } ?>
                    <button type="button" class="btn btn-primary btn-sm btn-federal-refresh" data-product_id="'+item.product_id+'">Federal Refresh</button>';
        }
        var regSubTypeFormatter = (value, item) => {
            switch (item.blr_reg_type) {
                case "New COLA - Product Never Registered":
                    switch (value) {
                        case '1':
                            return 'This is the 1st COLA of a new Product that has never been registered';
                            break;
                        case '2':
                            return 'This is an additional COLA of a new product that has never been registered';
                            break;
                        default:
                    }
                    break;
                case "New COLA - Existing Product Already Registered":
                    switch (value) {
                        case '1':
                            return 'I only want to update the COLA and Price Posting in applicable states';
                            break;
                        case '2':
                            return 'I would like to register in new states';
                            break;
                        default:
                    }
                default:
            }
            return "n/a";
        }

        var productEvents = {
            'click .btn-refresh': (e, value, row, index) => {
                var curTable = $(e.target).parent().parent().parent().parent();
                console.log(e, curTable, value, row, index);
                /*
                $.post('query-request.php', {
                    qry_type: "blr-refresh",
                    lkID: row.TTB_ID.trim(),
                    ccID: row.client_code.trim()
                }, (result) => {
                    console.log(result);
                })
                */
                $.get('https://mhwltd.app/sc_api/curl_sc_GetStateRegistrationStatusByCola.php', {
                    output: 'responseonly',
                    lkID: row.TTB_ID.trim(),
                    ccID: row.client_code.trim()
                }, (result) => {
                    console.log(result);
                    curTable.bootstrapTable('updateCell', {index: index, field: 'state_status', value: result});
                })
            },
			'click .btn-refreshST': (e, value, row, index) => {
                var curTable = $(e.target).parent().parent().parent().parent();
                console.log(e, curTable, value, row, index);

                $.get('https://mhwltd.app/sc_api/curl_sc_GetStateRegistrationStatusByCola.php', {
                    output: 'statusonly',
					outputstate: row.State.trim(),
                    lkID: row.TTB_ID.trim(),
                    ccID: row.client_code.trim()
                }, (result) => {
                    console.log(result);
                    curTable.bootstrapTable('updateCell', {index: index, field: 'state_status', value: result});
                })
            },
            'click .btn-federal-refresh': (e, value, row, index) => {
                var curTable = $(e.target).parent().parent().parent().parent();
                console.log(e, curTable, value, row, index);
                
                $.get('https://mhwltd.app/sc_api/curl_sc_GetFederalRegistrationStatusByCola.php', {
                    output: 'statusonly',
                    lkID: row.TTB_ID.trim(),
                    ccID: row.client_code.trim()
                }, (result) => {
                    console.log(result);
                    curTable.bootstrapTable('updateCell', {index: index, field: 'federal_status', value: result});
                })
            },
            'click .btn-distributor-refresh': (e, value, row, index) => {
                var curTable = $(e.target).parent().parent().parent().parent();
                console.log(e, curTable, value, row, index);
                
                $.get('https://mhwltd.app/sc_api/curl_sc_GetProductAvailabilityInAllStates.php', {
                    output: 'statusonly',
                    lkID: row.TTB_ID.trim(),
                    ccID: row.client_code.trim()
                }, (result) => {
                    console.log(result);
                    location.reload();
                });
                $.get('https://mhwltd.app/sc_api/curl_sc_GetProductAvailabilityInAllStates.php', {
                    output: 'statusonly',
                    lkID: row.TTB_ID.trim(),
                    ccID: 'mhwltdws'
                }, (result) => {
                    console.log(result);
                    location.reload();
                });
            }
        };

        var columns = [];
        columns['State Requests'] = [
            {
                field: 'blr_state_name',
                title: 'State',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'blr_state_abbrev',
                title: '',
                align: 'left',
                valign: 'middle',
                sortable: true
            }
        ];
        columns['Product Requests'] = [
            {
                field: 'product_desc',
                title: 'Product Description',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'brand_name',
                title: 'Brand',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'product_mhw_code',
                title: 'Product Code',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'TTB_ID',
                title: 'TTB ID',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'federal_type',
                title: 'Federal Type',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'federal_status',
                title: 'Federal Status',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'product_id',
                events: productEvents,
                formatter: refreshFormatter,
            },
        ];

        columns['All States'] = [
            {
                field: 'state_name',
                title: 'State',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'state_abbrev',
                title: '',
                align: 'left',
                valign: 'middle',
                sortable: true
            }
        ];
        columns['All Products'] = [
            {
                field: 'product_desc',
                title: 'Product Description',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'brand_name',
                title: 'Brand',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'product_mhw_code',
                title: 'Product Code',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'TTB_ID',
                title: 'TTB ID',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'federal_type',
                title: 'Federal Type',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'federal_status',
                title: 'Federal Status',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'product_id',
                events: productEvents,
                formatter: refreshFormatter,
            },
        ];


        var subColumns = [];

        subColumns['State Requests'] = [
            {
                field: 'product_desc',
                title: 'Product Description',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'brand_name',
                title: 'Brand',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'product_mhw_code',
                title: 'Product Code',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'TTB_ID',
                title: 'TTB ID',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'federal_type',
                title: 'Federal Type',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'blr_distributors',
                title: 'Distributors',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
			{
                field: 'state_status',
                title: 'State Status',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
			{
                field: 'ExpirationDate',
                title: 'State Expiration Date',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
			{
                field: 'StateApprovalNumber',
                title: 'State Approval Number',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
			{
                field: 'State',
                title: 'State',
				visible: false
            },
            {
                field: 'federal_status',
                title: 'Federal Status',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'blr_state_note',
                title: 'Notes',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'blr_contact',
                title: 'Contact',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'blr_email',
                title: 'Email',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'blr_phone',
                title: 'Phone',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'blr_confirmation',
                title: 'Confirmation',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
			{
                field: 'blr_reg_type',
                title: 'Reg.Type',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'blr_reg_sub_type',
                title: 'Reg.Sub-Type',
                align: 'left',
                valign: 'middle',
                formatter: regSubTypeFormatter,
                sortable: true
            },
            {
                field: 'create_date',
                title: 'Reg.Date',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'product_id',
                events: productEvents,
                formatter: STrefreshFormatter,
            },
        ];

        subColumns['Product Requests'] = [
            {
                field: 'blr_state_name',
                title: 'State',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'state_status',
                title: 'State Status',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
			{
                field: 'ExpirationDate',
                title: 'State Expiration Date',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
			{
                field: 'StateApprovalNumber',
                title: 'State Approval Number',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'blr_state_note',
                title: 'Notes',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'blr_contact',
                title: 'Contact',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'blr_email',
                title: 'Email',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'blr_phone',
                title: 'Phone',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'blr_distributors',
                title: 'Distributors',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'blr_confirmation',
                title: 'Confirmation',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
			{
                field: 'blr_reg_type',
                title: 'Reg.Type',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'blr_reg_sub_type',
                title: 'Reg.Sub-Type',
                align: 'left',
                valign: 'middle',
                formatter: regSubTypeFormatter,
                sortable: true
            },
            {
                field: 'create_date',
                title: 'Reg.Date',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
        ];

        subColumns['All States'] = [
            {
                field: 'product_desc',
                title: 'Product Description',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'brand_name',
                title: 'Brand',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'product_mhw_code',
                title: 'Product Code',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'TTB_ID',
                title: 'TTB ID',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'blr_distributors',
                title: 'Distributors',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'federal_type',
                title: 'Federal Type',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'state_status',
                title: 'State Status',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
			{
                field: 'ExpirationDate',
                title: 'State Expiration Date',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
			{
                field: 'StateApprovalNumber',
                title: 'State Approval Number',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
			{
                field: 'State',
                title: 'State',
				visible: false
            },
            {
                field: 'federal_status',
                title: 'Federal Status',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'blr_reg_type',
                title: 'Reg.Type',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'blr_reg_sub_type',
                title: 'Reg.Sub-Type',
                align: 'left',
                valign: 'middle',
                formatter: regSubTypeFormatter,
                sortable: true
            },
            {
                field: 'blr_confirmation',
                title: 'Confirmation',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'create_date',
                title: 'Reg.Date',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'product_id',
                events: productEvents,
                formatter: STrefreshFormatter,
            },
        ];

        subColumns['All Products'] = [
            {
                field: 'State',
                title: 'State',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'state_status',
                title: 'State Status',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
			{
                field: 'blr_distributors',
                title: 'Distributors',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'ExpirationDate',
                title: 'State Expiration Date',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
			{
                field: 'StateApprovalNumber',
                title: 'State Approval Number',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'blr_reg_type',
                title: 'Reg.Type',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'blr_reg_sub_type',
                title: 'Reg.Sub-Type',
                align: 'left',
                valign: 'middle',
                formatter: regSubTypeFormatter,
                sortable: true
            },
            {
                field: 'blr_confirmation',
                title: 'Confirmation',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'create_date',
                title: 'Reg.Date',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
        ];

        $(document).ready(()=>{

            var getHeight = () => {
                var winH = $(window).height();
                var navH = $(".navbar").height();
                return winH - navH - 30;
            }

            var updateTable = () => {
                var qry_type = "blr-view";
                var view_type = $('#view_type').val();
                var client = $('#client_name').val();
                var loadingMessage = '<span class="mhwLoading"><img src="css/img/Logo_Chevron_Animated.gif"/> Loading</span>';

                $('#blrViewTable').bootstrapTable('showLoading');
                console.log(view_type, columns[view_type]);
                $.post('query-request.php', {
                    qry_type: qry_type,
                    view_type: view_type,
                    client: client
                }, (dataPF) => { 
                    //console.log(dataPF);
                    var data = [];
                    try {
                        data = JSON.parse(dataPF);
                        $('#blrViewTable').bootstrapTable('destroy').bootstrapTable('showLoading').bootstrapTable({
                            data: data,
                            height: getHeight(),
                            stickyHeader:true,
                            stickyHeaderOffsetY:60,
                            formatLoadingMessage: () => loadingMessage,
                            detailView: true,
                            columns: columns[view_type],
                            //fixedColumns:true,
                            //fixedNumber:7,
                            onExpandRow: (idx, row, el) => {
                                var subTable = el.html('<table class="table-info"></table>').find('table');
                                subTable.bootstrapTable({formatLoadingMessage: () => loadingMessage}).bootstrapTable('showLoading');
                                var qry_type = "blr-view-expand";
                                var key = view_type == 'State Requests' ? row.blr_state_abbrev : view_type == 'All States' ? row.state_name : row.product_id;
                                $.post('query-request.php', {
                                    key: key,
                                    qry_type: qry_type,
                                    view_type: view_type,
                                    client: client,
                                }, (dataRaw) => {
                                    try {
                                        dataExpand = JSON.parse(dataRaw);
                                        //item sub table
                                        subTable.bootstrapTable('destroy').bootstrapTable('showLoading').bootstrapTable({
                                            data: dataExpand,
                                            formatLoadingMessage: () => loadingMessage,
                                            detailView: false,
                                            columns: subColumns[view_type]
                                        })

                                    } catch (error) {
                                        //location.reload();
                                        console.log(error, dataPF);
                                    }
                                })
                            } // on expand row
                        }); //show main table
                        
                    } catch (error) {
                        //location.reload();
                        console.log(error, dataPF);
                    }

                });
            }; // updateTable

            $('#view_type').on('change',updateTable);
            $('#client_name').on('change',updateTable);

            updateTable();

        });

    </script>

</div>


<?php
    include('footer.php');
?>
    