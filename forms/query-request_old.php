<?php
error_reporting(E_ALL); //displays an error

	//echo '<pre>'; print_r($_POST); echo '</pre>';
include("dbconnect.php");

//Establishes the connection
$conn = sqlsrv_connect($serverName, $connectionOptions);
if( $conn === false) {
		die( print_r( sqlsrv_errors(), true));
}

if($_POST['qrytype']=='prodByID')
{
	if(isset($_POST['prodID']) && $_POST['prodID']!=='' && isset($_POST['client']) && $_POST['client']!=='')
	{
		$tsql= "SELECT p.[product_id],p.[client_name],p.[brand_name],p.[product_desc],p.[product_mhw_code],p.[product_mhw_code_search],p.[TTB_ID] as [TTB],p.[federal_type],p.[compliance_type],p.[product_class],p.[mktg_prod_type],p.[bev_type],p.[fanciful],p.[country],p.[appellation],p.[lot_item],p.[bottle_material],p.[alcohol_pct],s.[supplier_id],s.[supplier_name],s.[supplier_contact],s.[supplier_fda_number],s.[tax_reduction_allocation],s.[supplier_address_1],s.[supplier_address_2],s.[supplier_address_3],s.[supplier_city],s.[supplier_state],s.[supplier_country],s.[supplier_zip],s.[supplier_phone],s.[supplier_email],s.[federal_basic_permit] FROM [dbo].[mhw_app_prod] p left outer join [mhw_app_prod_supplier] s on s.[supplier_id] = p.[supplier_id] WHERE p.[product_id] = ".$_POST['prodID']." and p.[client_name] IN ('".$_POST['client']."') AND p.[active] = 1 and p.[deleted] = 0";
		//echo $tsql;
	}
}
if($_POST['qrytype']=='suppByID')
{
	if(isset($_POST['suppID']) && $_POST['suppID']!=='')
	{
		$tsql= "SELECT s.[supplier_name],s.[supplier_contact],s.[supplier_fda_number],s.[tax_reduction_allocation],s.[supplier_address_1],s.[supplier_address_2],s.[supplier_address_3],s.[supplier_city],s.[supplier_state],s.[supplier_country],s.[supplier_zip],s.[supplier_phone],s.[supplier_email],s.[federal_basic_permit] FROM [mhw_app_prod_supplier] s WHERE s.[supplier_id] = ".$_POST['suppID']." AND s.[active] = 1 and s.[deleted] = 0";
		//echo $tsql;
	}
}
if($_POST['qrytype']=='itemByID')
{
	if(isset($_POST['itemID']) && $_POST['itemID']!=='' && isset($_POST['client']) && $_POST['client']!=='')
	{
		$tsql= "SELECT i.[item_id], i.[item_mhw_code], i.[stock_uom], i.[bottles_per_case], p.[product_id],p.[client_name],p.[brand_name],p.[product_desc],p.[product_mhw_code],p.[product_mhw_code_search],p.[TTB_ID] as [TTB],p.[federal_type],p.[compliance_type],p.[product_class],p.[mktg_prod_type],p.[bev_type],p.[fanciful],p.[country],p.[appellation],p.[lot_item],p.[bottle_material],p.[alcohol_pct] FROM [dbo].[mhw_app_prod_item] i left outer join [dbo].[mhw_app_prod] p on p.[product_id] = i.[product_id] WHERE i.[item_id] = ".$_POST['itemID']." and p.[client_name] IN ('".$_POST['client']."') AND p.[active] = 1 and p.[deleted] = 0 AND i.[active] = 1 and i.[deleted] = 0";
		//echo $tsql;
	}
}


$getResults= sqlsrv_query($conn, $tsql);
//echo ("Reading data from table" . PHP_EOL);
if ($getResults == FALSE)
	echo (sqlsrv_errors());

 /* Processing query results 
 */

/* Setup an empty array */
$json = array();
/* Iterate through the table rows populating the array */
do {
     while ($row = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
     $json[] = $row;
     }
} while ( sqlsrv_next_result($getResults) );
 
/* Run the tabular results through json_encode() */
/* And ensure numbers don't get cast to strings */
echo json_encode($json);

/* Free statement and connection resources. */
sqlsrv_free_stmt( $getResults);
sqlsrv_close( $conn);




?>