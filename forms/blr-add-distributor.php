<?php

	error_reporting(E_ALL); //displays an error

	session_start();
	if(!isset($_SESSION['mhwltdphp_user'])){
		die( "Not authenticated !" );  
	}

	include("dbconnect.php");

	//Establishes the connection
	$conn = sqlsrv_connect($serverName, $connectionOptions);
	if( $conn === false) {
			die( print_r( sqlsrv_errors(), true));
	}

	$dist_key = uniqid();
	$dist_code = $dist_key;

	$tsql = "
		INSERT INTO [dbo].[sc_distributor-list] (
			[DistKey]
			,[DistCode]
			,[Name]
			,[Addr1]
			,[Addr2]
			,[State]
		)
		VALUES (
			?,?,?,?,?,?
		);
	";

	$params = array (
		$dist_key
		,$dist_code
		,$_POST['distributor_name']
		,$_POST['distributor_address']
		,$_POST['distributor_contact']." "
		.$_POST['distributor_phone']." "
		.$_POST['distributor_email']
		,$_POST['distributor_state']
	);

	$stmt = sqlsrv_prepare( $conn, $tsql, $params);  

	if( $stmt === false )  
	{  
		 echo "Statement could not be prepared.\n";  
		 die( print_r( sqlsrv_errors(), true));  
	}  

	if( sqlsrv_execute($stmt) === false )  
	{  
	  echo "Statement could not be executed.\n";  
	  die( print_r( sqlsrv_errors(), true));  
	}  

	$json = array(
		'result' => sqlsrv_rows_affected ( $stmt ),
		'key' => $dist_key,
		'state' => $_POST['distributor_state'],
		'name' => $_POST['distributor_name']
	);
	header('Content-Type: application/json');
	echo json_encode($json);

	/* Free statement and connection resources. */  
	sqlsrv_free_stmt( $stmt);  
	sqlsrv_close( $conn); 

	//header("Location: result.php");

?>