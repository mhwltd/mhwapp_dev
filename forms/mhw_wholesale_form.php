<?php
session_start();
include("prepend.php");
include("settings.php");

if (!isset($_SESSION['mhwltdphp_user'])){
	echo "test";
	header("Location: login.php");
} else{
	    include('dbconnect.php');
		include('head.php');
		
		if (isset($_POST["company_name"]) !== false) {
		$company_name = $_POST['company_name'];
		$company_phone_number = $_POST['company_phone_number'];
		$fax_phone_number = $_POST['fax_phone_number'];
		$address = $_POST['address'];
		$city = $_POST['city'];
		$state = $_POST['state'];
		$zipcode = $_POST['zipcode'];

		$contact_Nm_Ops = $_POST['contactNmOps'];
		$contact_Email_Ops = $_POST['contactEmailOps'];
		$delivery_Type = $_POST['deliveryType'];
		$delivery_Days = $_POST['deliveryDays'];

		$territory = $_POST['territory'];
		$on_off_Premise = $_POST['onoffPremise'];
		$st_120 = $_POST['st120'];
		$delivery_Instructions = $_POST['deliveryInstructions'];

		$shipping_address = $_POST['shipping_address'];
		$shipping_city = $_POST['shipping_city'];
		$shipping_state = $_POST['shipping_state'];
		$shipping_zipcode = $_POST['shipping_zipcode'];
		$shipping_phone = $_POST['shipping_phone'];

		$license_number = $_POST['license_number'];
		$license_expiration_date = $_POST['license_expiration_date'];
		$ein_number = $_POST['ein_number'];
		$ap_contact_person = $_POST['ap_contact_person'];
		$ap_contact_email = $_POST['contactEmailAp'];
		
		$ar_email_address = $_POST['ar_email_address'];

		$terms = $_POST['terms'];
		$website = $_POST['website'];

		$billing_address = $_POST['address'];
		$billing_city = $_POST['city'];
		$billing_state = $_POST['state'];
		$billing_zipcode = $_POST['zipcode'];
		
		$shipping_address = $_POST['shipping_address'];
		$shipping_city = $_POST['shipping_city'];
		$shipping_state = $_POST['shipping_state'];
		$shipping_phone = $_POST['shipping_phone'];
		$shipping_zipcode = $_POST['shipping_zipcode'];
		
		$shipping1_address = $_POST['shipping_address1'];
		$shipping1_city = $_POST['shipping_city1'];
		$shipping1_state = $_POST['shipping_state1'];
		$shipping1_phone = $_POST['shipping_phone1'];
		$shipping1_zipcode = $_POST['shipping_zipcode1'];

		$shipping2_address = $_POST['shipping_address2'];
		$shipping2_city = $_POST['shipping_city2'];
		$shipping2_state = $_POST['shipping_state2'];
		$shipping2_phone = $_POST['shipping_phone2'];
		$shipping2_zipcode = $_POST['shipping_zipcode2'];

		$shipping3_address = $_POST['shipping_address3'];
		$shipping3_city = $_POST['shipping_city3'];
		$shipping3_state = $_POST['shipping_state3'];
		$shipping3_phone = $_POST['shipping_phone3'];
		$shipping3_zipcode = $_POST['shipping_zipcode3'];

		$userID = $_POST['userID'];
		
		//echo "test";
		//echo "<pre>";
		//var_dump($_REQUEST); 
		//echo "</pre>";
		//echo "test2";

		//Establishes the connection
		$conn = sqlsrv_connect($serverName, $connectionOptions);
	
		if( $conn === false) {
			//echo "Connection could not be established.<br />";
			die( print_r( sqlsrv_errors(), true));
	    } //else {
			//echo "Connection established";
			//console.log("Connection established");
		//}

		$mainsql = "INSERT INTO mhw_app_wholesale_form (company_name, company_phone_number, company_fax_number, license_number, license_expiration_date,
		                                            ein_number, ap_contact_person, ar_email_address, terms, website, 
													contactNmOps, contactEmailOps, deliveryType, deliveryDays, territory, onoffPremise, st120, deliveryInstructions, contactEmailAp,
													created_by) 
                 VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ); SELECT SCOPE_IDENTITY() AS IDENTITY_COLUMN_NAME";
        
		// prepare and bind
		$mainparams = array($company_name, $company_phone_number, $fax_phone_number, $license_number, $license_expiration_date, $ein_number, $ap_contact_person, $ar_email_address, $terms, $website, 
							$contact_Nm_Ops, $contact_Email_Ops, $delivery_Type, $delivery_Days, $territory, $on_off_Premise, $st_120, $delivery_Instructions, $ap_contact_email, $userID);		
		$stmt = sqlsrv_query( $conn, $mainsql, $mainparams);

		if( $stmt === false ) {
			die( print_r( sqlsrv_errors(), true));
			}
			//else
			//{
			//echo "Record add successfully";
			//}

		sqlsrv_next_result($stmt);  //note this line!!
        sqlsrv_fetch($stmt); 	
		$wholesaleId = sqlsrv_get_field($stmt, 0);
		

		//****** Billing Address ***********
		$billsql = "INSERT INTO mhw_app_address (wholesale_id, address_type, address, city, state, zipcode, created_by) 
                    VALUES (?, ?, ?, ?, ?, ?, ?)";
		$billparams = array($wholesaleId, 1, $address, $city, $state, $zipcode, $userID);		
		$billstmt = sqlsrv_query( $conn, $billsql, $billparams);

		if( $billstmt === false ) {
		die( print_r( sqlsrv_errors(), true));
		}

		//****** Shipping Address ***********
	    $shipsql = "INSERT INTO mhw_app_address (wholesale_id, address_type, address, city, state, zipcode, phone_number, created_by) 
	                    VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
	    $shipparams = array($wholesaleId, 2, $shipping_address, $shipping_city, $shipping_state, $shipping_zipcode, $shipping_phone, $userID);		
	    $shipstmt = sqlsrv_query( $conn, $shipsql, $shipparams);

		if( $shipstmt === false ) {
		die( print_r( sqlsrv_errors(), true));
		}
			

		//****** Shipping1 Address ***********
		if (( $shipping1_address != '') || ( $shipping1_city != '') || ( $shipping1_state != '') || ( $shipping1_zipcode != '') || ( $shipping1_phone != '')) {
			$ship1sql = "INSERT INTO mhw_app_address (wholesale_id, address_type, address, city, state, zipcode, phone_number, created_by) 
			VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
			$ship1params = array($wholesaleId, 2, $shipping1_address, $shipping1_city, $shipping1_state, $shipping1_zipcode, $shipping1_phone, $userID);		
			$ship1stmt = sqlsrv_query( $conn, $ship1sql, $ship1params);

			if( $ship1stmt === false ) {
			die( print_r( sqlsrv_errors(), true));
			}
	    }	

		//****** Shipping2 Address ***********
        if (($shipping2_address != '') || ($shipping2_city != '') || ($shipping2_state != '') || ($shipping2_zipcode != '') || ($shipping2_phone != '')) {
            $ship2sql = "INSERT INTO mhw_app_address (wholesale_id, address_type, address, city, state, zipcode, phone_number, created_by) 
						VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            $ship2params = array($wholesaleId, 2, $shipping2_address, $shipping2_city, $shipping2_state, $shipping2_zipcode, $shipping2_phone, $userID);
            $ship2stmt = sqlsrv_query($conn, $ship2sql, $ship2params);

            if ($ship2stmt === false) {
                die(print_r(sqlsrv_errors(), true));
            }
        }
	
		//****** Shipping3 Address ***********
        if (($shipping3_address != '') || ($shipping3_city != '') || ($shipping3_state != '') || ($shipping3_zipcode != '') || ($shipping3_phone != '')) {
            $ship3sql = "INSERT INTO mhw_app_address (wholesale_id, address_type, address, city, state, zipcode, phone_number, created_by) 
			VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            $ship3params = array($wholesaleId, 2, $shipping3_address, $shipping3_city, $shipping3_state, $shipping3_zipcode, $shipping3_phone, $userID);
            $ship3stmt = sqlsrv_query($conn, $ship3sql, $ship3params);

            if ($ship3stmt === false) {
                die(print_r(sqlsrv_errors(), true));
            }
        }

		sqlsrv_close($conn);
	}
?>

<script src="main.js"></script>	
<script src="js/repeater.js"></script>


<style type="text/css">
.warning {display:block; margin:15px 0;}
.warning span {color:red;}
input[type='number'] {width:18rem; display:inline;}
#productvalid {display:none;}
</style>	

    <link href="css/wforms-layout_v530-14.css" rel="stylesheet" type="text/css" />
    <link href="css/theme-52661.css" rel="stylesheet" type="text/css" />
    <link href="css/wforms-jsonly_v530-14.css" rel="alternate stylesheet" title="This stylesheet activated by javascript" type="text/css" />
	<link href="css/wforms-inline.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/wforms_v530-14.js"></script>
    <script type="text/javascript" src="js/localization-en_US_v530-14.js"></script>         
    
</head>
<body class="default wFormWebPage">
<div class="container-fluid">
	<div id="tfaContent">
		<div class="wFormContainer" style="max-width: 85%; width:auto;" >
			<div class="row">
				<div class="col-sm-12">

    				<div class="wFormHeader"></div>
    		
				<h3 class="wFormTitle" style="text-align:center;text-decoration:underline;margin-bottom:2rem;">MHW Wholesale Set-up Form</h3>
					
					<form method="post" action="mhw_wholesale_form.php" class="hintsBelow labelsAbove" id="4690986" role="form" enctype="multipart/form-data">									

						  <div class="form-row">
							<div class="form-group col-md-6">
							  <label>Company Name:</label>
							  <input type="text" class="form-control" placeholder="Company Name" name="company_name" maxlength="50" required>
							</div>
							<div class="form-group col-md-3">
							  <label>Phone Number:</label>
							  <input type="tel" class="form-control" placeholder="Phone Number" name="company_phone_number" maxlength="25" required>
							</div>
							<div class="form-group col-md-3">
							  <label>Fax Number:</label>
							  <input type="tel" class="form-control" placeholder="Fax Number" name="fax_phone_number" maxlength="25">
							</div>							
						  </div>	
						  
						  <div class="form-row">
						    <div class="form-group col-md-6">
							  <label>Billing Address:</label>
							  <input type="text" class="form-control" placeholder="Billing Address" name="address" maxlength="100" required>
   						    </div>						  
							<div class="form-group col-md-3">
							  <label>City:</label>
							  <input type="text" class="form-control" placeholder="City" name="city" maxlength="50" required>
							</div>
							<div class="form-group col-md-1">
							  <label>State:</label>
							  <input type="text" class="form-control" placeholder="State" name="state" maxlength="50" required>
							</div>
							<div class="form-group col-md-2">
							  <label>Zip Code:</label>
							  <input type="text" class="form-control" placeholder="Zip Code" name="zipcode" maxlength="50" required>
							</div>
						  </div>	
						  
						  <div class="form-group">						
							  <label>Contact Person (First,Last): </label>
							  <input type="text" class="form-control" placeholder="Contact Person" name="contactNmOps" required maxlength="125">
   						  </div>	

						  <div class="form-group">						
							  <label>Contact Email Address: </label>
							  <input type="email" class="form-control" placeholder="Contact Email Address" name="contactEmailOps" required maxlength="150">
   						  </div>

						  <div class="form-row">
						    <div class="form-group col-md-2">						
							  <label>Devlivery Type: </label>
							  <select class="custom-select" name="deliveryType" required>
							  		<option value=""></option>								  
									<option value="Domestic">Domestic</option>
									<option value="Foreign">Foreign</option>								  
							  </select>
   						    </div>
							<div class="form-group col-md-2">						
								<label>Territory: </label>
								<input type="text" class="form-control" placeholder="Territory" name="territory" required maxlength="150">
							</div>
							<div class="form-group col-md-2">						
							  <label>On/Off Premise: </label>
							  <select class="custom-select" name="onoffPremise" required>
							        <option value=""></option>								  
									<option value="OnPremise">On Premise</option>
									<option value="OffPremise">Off Premise</option>								  
							  </select>
   						    </div>
						  </div>

						  <div class="form-row">
							<div class="form-group col-md-2">
									<label>ST-120:</label>
									<input type="text" class="form-control" placeholder="ST-120" name="st120" maxlength="50" required>
								</div>
								<div class="form-group col-md-6">						
									<label>Delivery Days: </label>
									<select class="custom-select" name="deliveryDays" required>	
									    <option value=""></option>							  
										<option value="ALL">MONDAY^TUESDAY^WEDNESDAY^THURSDAY^FRIDAY^SATURDAY^SUNDAY</option>
										<option value="F">FRIDAY</option>
										<option value="MThF">MONDAY^THURSDAY^FRIDAY</option>
										<option value="MTuTh">MONDAY^TUESDAY^THURSDAY</option>
										<option value="MTuThF">MONDAY^TUESDAY^THURSDAY^FRIDAY</option>
										<option value="MTuThFSa">MONDAY^TUESDAY^THURSDAY^FRIDAY^SATURDAY</option>
										<option value="MTuThSa">MONDAY^TUESDAY^THURSDAY^SATURDAY</option>
										<option value="MTuW">MONDAY^TUESDAY^WEDNESDAY</option>
										<option value="MTuWF">MONDAY^TUESDAY^WEDNESDAY^FRIDAY</option>
										<option value="MTuWTh">MONDAY^TUESDAY^WEDNESDAY^THURSDAY</option>
										<option value="MTuWThFSa">MONDAY^TUESDAY^WEDNESDAY^THURSDAY^FRIDAY^SATURDAY</option>
										<option value="MWF">MONDAY^WEDNESDAY^FRIDAY</option>
										<option value="MWThF">MONDAY^WEDNESDAY^THURSDAY^FRIDAY</option>
										<option value="Th">THURSDAY</option>
										<option value="ThF">THURSDAY^FRIDAY</option>
										<option value="ThFSa">THURSDAY^FRIDAY^SATURDAY</option>
										<option value="Thu">Thursday</option>
										<option value="Tu">TUESDAY</option>
										<option value="TuF">TUESDAY^FRIDAY</option>
										<option value="TuTh">TUESDAY^THURSDAY</option>
										<option value="TuThF">TUESDAY^THURSDAY^FRIDAY</option>
										<option value="TuW">TUESDAY^WEDNESDAY</option>
										<option value="TuWF">TUESDAY^WEDNESDAY^FRIDAY</option>
										<option value="TuWTh">TUESDAY^WEDNESDAY^THURSDAY</option>
										<option value="TuWThF">TUESDAY^WEDNESDAY^THURSDAY^FRIDAY</option>
										<option value="TuWThFSa">TUESDAY^WEDNESDAY^THURSDAY^FRIDAY^SATURDAY</option>
										<option value="W">WEDNESDAY</option>
										<option value="WEEKDAY">MONDAY^TUESDAY^WEDNESDAY^THURSDAY^FRIDAY</option>
										<option value="WF">WEDNESDAY^FRIDAY</option>
										<option value="WFSa">WEDNESDAY^FRIDAY^SATURDAY</option>
										<option value="WTh">WEDNESDAY^THURSDAY</option>
										<option value="WThF">WEDNESDAY^THURSDAY^FRIDAY</option>					  
								</select>
								</div>
						  </div>

						  <div class="form-row">
								<div class="form-group col-md-8">						
									<label> Delivery Instructions: </label>
									<textarea class="form-control" name="deliveryInstructions" maxlength="250"></textarea>
								</div>
						  </div>

						  <div class="form-row">
						    <div class="form-group col-md-6">
							  <label>Shipping Address: (For use of multiple locations)</label>
							  <input type="text" class="form-control" placeholder="Shipping Address" name="shipping_address" maxlength="100" required>
   						    </div>						  
							<div class="form-group col-md-2">
							  <label>City:</label>
							  <input type="text" class="form-control" placeholder="City" name="shipping_city" maxlength="50" required>
							</div>
							<div class="form-group col-md-1">
							  <label>State:</label>
							  <input type="text" class="form-control" placeholder="State" name="shipping_state" maxlength="50" required>
							</div>
							<div class="form-group col-md-1">
							  <label>Zip Code:</label>
							  <input type="text" class="form-control" placeholder="Zip Code" name="shipping_zipcode" maxlength="50" required>
							</div>
							<div class="form-group col-md-2">
							  <label>Phone:</label>
							  <input type="tel" class="form-control" placeholder="Phone" name="shipping_phone" maxlength="50" required>
							</div>							
						  </div>
						  
						  <div class="form-row">
						    <div class="form-group col-md-6">						
							  <input type="text" class="form-control" placeholder="Shipping Address" name="shipping_address1" maxlength="100">
   						    </div>						  
							<div class="form-group col-md-2">							  
							  <input type="text" class="form-control" placeholder="City" name="shipping_city1" maxlength="50">
							</div>
							<div class="form-group col-md-1">							  
							  <input type="text" class="form-control" placeholder="State" name="shipping_state1" maxlength="50">
							</div>
							<div class="form-group col-md-1">							  
							  <input type="text" class="form-control" placeholder="Zip" name="shipping_zipcode1" maxlength="50">
							</div>
							<div class="form-group col-md-2">							  
							  <input type="tel" class="form-control" placeholder="Phone" name="shipping_phone1" maxlength="50">
							</div>	
						   </div>
						   
						  <div class="form-row">
						    <div class="form-group col-md-6">						
							  <input type="text" class="form-control" placeholder="Shipping Address" name="shipping_address2" maxlength="100">
   						    </div>						  
							<div class="form-group col-md-2">							  
							  <input type="text" class="form-control" placeholder="City" name="shipping_city2" maxlength="50"> 
							</div>
							<div class="form-group col-md-1">							  
							  <input type="text" class="form-control" placeholder="State" name="shipping_state2" maxlength="50">
							</div>
							<div class="form-group col-md-1">							  
							  <input type="text" class="form-control" placeholder="Zip" name="shipping_zipcode2" maxlength="50">  
							</div>
							<div class="form-group col-md-2">							  
							  <input type="tel" class="form-control" placeholder="Phone" name="shipping_phone2" maxlength="50">
							</div>	
						   </div>
						   
						  <div class="form-row">
						    <div class="form-group col-md-6">						
							  <input type="text" class="form-control" placeholder="Shipping Address" name="shipping_address3" maxlength="100">
   						    </div>						  
							<div class="form-group col-md-2">							  
							  <input type="text" class="form-control" placeholder="City" name="shipping_city3" maxlength="50">
							</div>
							<div class="form-group col-md-1">							  
							  <input type="text" class="form-control" placeholder="State" name="shipping_state3" maxlength="50">
							</div>
							<div class="form-group col-md-1">							  
							  <input type="text" class="form-control" placeholder="Zip" name="shipping_zipcode3" maxlength="50">
							</div>
							<div class="form-group col-md-2">							  
							  <input type="tel" class="form-control" placeholder="Phone" name="shipping_phone3" maxlength="50">
							</div>	
						   </div>	
						   
						  <div class="form-row">
						    <div class="form-group col-md-9">						
							  <label> License Number: (Please provide a copy as well) </label>
							  <input type="text" class="form-control" placeholder="License Number" name="license_number" required maxlength="75">
   						    </div>						  
							<div class="form-group col-md-3">						
								<label>Expiration Date:</label>
							  <input type="date" class="form-control" placeholder="Exp. Date" name="license_expiration_date" required>
							</div>
						   </div>
						   
							<div class="form-group">						
							  <label> EIN Number (Federal Tax ID): </label>
							  <input type="text" class="form-control" placeholder="EIN Number" name="ein_number" required maxlength="75">
   						    </div>							   
							
							<div class="form-group">						
							  <label> A/P Contact Person (First,Last): </label>
							  <input type="text" class="form-control" placeholder="A/P Contact Person" name="ap_contact_person" required maxlength="125">
   						    </div>	

							<div class="form-group">						
							  <label> A/P Contact Email Address: </label>
							  <input type="email" class="form-control" placeholder="A/P Contact Email Address" name="contactEmailAp" required maxlength="150">
   						    </div>

							<div class="form-group">						
							  <label> A/R Email Address: </label>
							  <input type="email" class="form-control" placeholder="A/R Email Address" name="ar_email_address" required maxlength="150">
   						    </div>

							<div class="form-group">						
							  <label> Terms: </label>
							  <input type="text" class="form-control" placeholder="Terms" name="terms" required maxlength="150">
   						    </div>

							<div class="form-group">						
							  <label> Website (if applicable): </label>
							  <input type="text" class="form-control" placeholder="Website" name="website" maxlength="150">
   						    </div>	
					
					<div class="actions" id="4690986-A"><input type="submit" data-label="Submit" class="primaryAction btn btn-primary" id="submit_button" value="Submit"></div>
					<div style="clear:both"></div>
					
					<input type="hidden" name="userID" id="userID" value="<?php echo $_SESSION['mhwltdphp_user']; ?>">

					</form>
				</div>
			</div>
		</div>    
	</div>

	<script src='js/iframe_resize_helper_internal.js'></script>
</div>
</body>
</html>
<?php
}
?>