		$(document).ready(function(){
			/*======BEGIN INITIAL PAGE LOAD======*/
			
			//if product id is passed to page, preload with data
			var prodIDinit = $("#prodID").val();
			if(parseInt(prodIDinit) > 0){
				prefill_prod(prodIDinit);

				cbb_formatter();
			}

			$('input[name=emailsupplier]').click(function(){
				if($(this).is(':checked'))
				{
				    $("[name='emailpo']").prop("required", true);  
				} else {
					$("[name='emailpo']").prop("required", false); 
				}
			  })

		
			  $('#supplier_name').change(function() {
				var supplierName = $("[name='supplier_name']").val();
				if (supplierName == 'New Supplier') {
					$("[name='supplier_contact']").prop("required", true);
					$("[name='supplier_fda']").prop("required", true);
					$("[name='supplier_tax_red']").prop("required", false);
					$("[name='supplier_address']").prop("required", true);
					$("[name='supplier_address2']").prop("required", false);
					$("[name='supplier_address3']").prop("required", false);
					$("[name='supplier_city']").prop("required", true);
					$("[name='supplier_state']").prop("required", false);
					$("[name='supplier_country']").prop("required", true);
					$("[name='supplier_zip']").prop("required", false);
					$("[name='supplier_phone']").prop("required", true);
					$("[name='supplier_email']").prop("required", true);


				} else {
					$("[name='supplier_contact']").prop("required", false);
					$("[name='supplier_fda']").prop("required", false);
					$("[name='supplier_tax_red']").prop("required", false);
					$("[name='supplier_address']").prop("required", false);
					$("[name='supplier_address2']").prop("required", false);
					$("[name='supplier_address3']").prop("required", false);
					$("[name='supplier_city']").prop("required", false);
					$("[name='supplier_state']").prop("required", false);
					$("[name='supplier_country']").prop("required", false);
					$("[name='supplier_zip']").prop("required", false);
					$("[name='supplier_phone']").prop("required", false);
					$("[name='supplier_email']").prop("required", false);

					$("[name='supplier_contact']").val("");
					$("[name='supplier_fda']").val("");
					$("[name='supplier_tax_red']").val("");
					$("[name='supplier_address']").val("");
					$("[name='supplier_address2']").val("");
					$("[name='supplier_address3']").val("");
					$("[name='supplier_city']").val("");
					$("[name='supplier_state']").val("");
					$("[name='supplier_country']").val("");
					$("[name='supplier_zip']").val("");
					$("[name='supplier_phone']").val("");
					$("[name='supplier_email']").val("");
				}
			  });

			/*======END INITIAL PAGE LOAD======*/

			/*======BEGIN comboboxes==========*/
			function cbb_formatter(){ 				
				$('ul.ac_results').css("margin-left","0em");
				$('ul.ac_results').css("margin","0em");
				$('.ac_button').css("height","28px");
				$('.ac_button').css("background","transparent");
				$('.ac_button').css("margin-left","-35px");
				$('.ac_button').css("border","0px");
				$('.ac_subinfo dl').css("margin-left","0em");
				$('.ac_subinfo dl').css("margin","0em");
			}
			//load form fields - as a result of selecting product_desc combobox
			function prefill_prod(prodID){
				var qrytype = "prodByID";
				var client = $('#tfa_client_name').val(); //get value of the client combobox
				$.post('query-request.php', {qrytype:qrytype,prodID:prodID,client:client}, function(dataPx){ // ajax request query-request.php, send brand as post variable, return dataX variable, parse into JSON

					var t = JSON.parse(dataPx);

					$.each( t, function( key, value ) {
						//product

						$('#product_mhw_code').val(value.product_mhw_code);
						$('#product_mhw_code_search').val(value.product_mhw_code_search);	
	
					});
				});
			}
			function isProdValidSelection(){
				var isProdID = $('#tfa_product_desc')[0].hasAttribute("sub_info"); //free-type data will not have sub_info attribute
				if(isProdID){
					var json = $('#tfa_product_desc').attr('sub_info');
					//if additional sub_info elements are added, additional apostrophe replacements need to be added
					var json2 = json.replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"');
					var jsonObj = JSON.parse(json2);

					$("#prodID").val(jsonObj.id);  //set field for processing update
					//$("#tfa_399-L").html("Product: editing product_id "+jsonObj.id); //display message for update
					//prefill_prod(jsonObj.id); //prefill form fields					
					$("#productvalid").hide();
				}
				else{
					//$("#tfa_399-L").html("Product: new entry"); //display message for insert
					$("#prodID").val("");  //clear field for insert	
					$("#productvalid").show();					
				}
			}
			
			//initialize product combobox
			function initialize_product(){
				//$('#tfa_product_desc').parent().find(".ac_button").remove();  //clear elements of previous instance
				//$('#tfa_product_desc').parent().find(".ac_result_area").remove();  //clear elements of previous instance
				//var client = $('#tfa_client_name').val(); //get value of the client combobox
				var client = 'MHW Web Demo';
				//var brand = $('#tfa_brand_name').val(); //get value of the brand combobox
				var brand = '';
				var cbbtype = 'product';
				$.post('select-request.php', {cbbtype:cbbtype,client:client,brand:brand}, function(dataX){ // ajax request select-request.php, send brand as post variable, return dataX variable, parse into JSON
					var data = JSON.parse(dataX);
					$(function() {
						$('#tfa_product_desc').ajaxComboBox(
							data,
							{
							  bind_to: 'tfa_product_desc',
							  sub_info: true,
							  sub_as: {
								id: 'Product ID',
								code: 'MHW Product Code',
								ttb: 'TTB ID'
							  },
							}
						).bind('tfa_product_desc', function() {
						  var json = $(this).attr('sub_info');
						  //if additional sub_info elements are added, additional apostrophe replacements need to be added
						  var json2 = json.replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"');
						  var jsonObj = JSON.parse(json2);

						  if(parseInt(jsonObj.id) > 0){
							$("#prodID").val(jsonObj.id); //set field for processing update
							$("#tfa_399-L").html("Product: editing product_id "+jsonObj.id); //display message for update
							prefill_prod(jsonObj.id); //prefill form fields
						  }
						  isProdValidSelection();

						});
						cbb_formatter(); //apply css and classname changes to combobox elements
					});
				});
				$('#tfa_product_desc').bind("change", function(){ 
					isProdValidSelection();
					disableAddBtn();
				});
			}
			initialize_product();
			disableAddBtn();

			/*======END comboboxes==========*/
			
			//Display/Hide Divs functionality
			
			CheckMMBoxes();
			
			$("form").change(function(){   				
				   CheckMMBoxes();
			});
						
			$("#submit_button").click(function(e) {								
				if($("#repeater .items").length < 1) {
					e.preventDefault();
					alert("Please input at least one item");
				}
			});
						
		   function CheckMMBoxes() {
			   $(".hiddeninputs").each(function () {
					 var currdiv = $(this).attr('data-div');
									
					
				 if ($(this).is(":checked")) {	         
					   $("#" + currdiv).show();
					   $("#" + currdiv + " input").prop('required',true);
					 } else {
							$("#" + currdiv).hide();
							$("#" + currdiv + " input").prop('required',false);
						 }
				});
		   } 
		   
			function disableAddBtn() {
				
				var isProdID = $('#tfa_product_desc')[0].hasAttribute("sub_info"); //free-type data will not have sub_info attribute
							
				if(!isProdID)
					$(".repeater-add-btn").prop('disabled',true);
				else
					$(".repeater-add-btn").attr('disabled',false);			
			}
			
			function displayNewSupplier(sbox) {
			   if(suppliername.val() == "new") {
				  $("#nsupplier_fields").show();
				  $(".supplierreq").prop('required',true);
			   }
			   else {
					$("#nsupplier_fields").hide();
					$(".supplierreq").prop('required',false);
			   }
		   }		   
		   
		   //Hide/Display Supplier Fields
		   	var suppliername = $("#supplier_name");	   
			suppliername.change(function(supplier){displayNewSupplier(supplier)});
		   
		   //Initialize repeating boxes
		   $("#repeater").createRepeater();	 

			$(".repeater-add-btn").click(function() {
					$(".repeater-add-btn").prop('disabled',true);					
			});
			
			//Clear form
			$("#clearform").click(function() {
				$("4690986").trigger('reset');
				setTimeout(function(){
					$("#supplier_name").trigger('change');
					$("#repeater .items").remove();
				},500);
			});		   

			

		});