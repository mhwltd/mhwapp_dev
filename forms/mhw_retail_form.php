<?php
session_start();
include("prepend.php");
include("settings.php");

if(!isset($_SESSION['mhwltdphp_user'])){

	header("Location: login.php");
}
else{
	include('head.php');
	include('dbconnect.php');

	if(isset($_POST['legal_name'])){
		$legal_name = $_POST['legal_name'];
		$dba = $_POST['dba'];
		$street_address = $_POST['street_address'];
		$citystatezip = $_POST['citystatezip'];
		$phone_number = $_POST['phone_number'];
		$fax_number = $_POST['fax_number'];
		$contact_person = $_POST['contact_person'];

		$contactNm_OPS = $_POST['contactNm_OPS'];
		$contactEmail_Ops = $_POST['contactEmail_Ops'];
		$contact_person_Email = $_POST['contact_person_Email'];
		$st120 = $_POST['st120'];
		$deliveryDays = $_POST['deliveryDays'];
		$territory = $_POST['territory'];

		$ap_contact = $_POST['ap_contact'];
		$ap_email_address = $_POST['ap_email_address'];
		$license_serial_number = $_POST['license_serial_number'];
		$expiration_date = $_POST['expiration_date'];
		$ny_nj_ca_sales_tax_id = $_POST['ny_nj_ca_sales_tax_id'];
		$delivery_instructions = $_POST['delivery_instructions'];
		$sales_person = $_POST['sales_person'];

		$userID = $_POST['userID'];

		//echo "<pre>";
		//var_dump($_REQUEST); 
		//echo "</pre>";

		//Establishes the connection
		$conn = sqlsrv_connect($serverName, $connectionOptions);

		if( $conn === false) {
			//echo "Connection could not be established.<br />";
			die( print_r( sqlsrv_errors(), true));
		} 

		$mainsql = "INSERT INTO mhw_app_retail_form (legal_name, dba, street_address, citystatezip, phone_number,
												fax_number, contact_person, ap_contact, ap_email_address, 
												license_serial_number, expiration_date, ny_nj_ca_sales_tax_id, delivery_instructions, 
												sales_person, contactNmOPS, contactEmailOps, contactPersonEmail, st120, deliveryDays, territory, created_by) 
					VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?); SELECT SCOPE_IDENTITY() AS IDENTITY_COLUMN_NAME";
		
		// prepare and bind
		$mainparams = array($legal_name, $dba, $street_address, $citystatezip, $phone_number, 
							$fax_number, $contact_person, $ap_contact, $ap_email_address, $license_serial_number, $expiration_date, $ny_nj_ca_sales_tax_id, $delivery_instructions, 
							$sales_person, $contactNm_OPS,  $contactEmail_Ops,  $contact_person_Email, $st120, $deliveryDays, $territory, $userID);		
		$stmt = sqlsrv_query( $conn, $mainsql, $mainparams);

		if( $stmt === false ) {
			die( print_r( sqlsrv_errors(), true));
		}

	}
?>

    <link href="css/wforms-layout_v530-14.css" rel="stylesheet" type="text/css" />
    <link href="css/theme-52661.css" rel="stylesheet" type="text/css" />
    <link href="css/wforms-jsonly_v530-14.css" rel="alternate stylesheet" title="This stylesheet activated by javascript" type="text/css" />
	<link href="css/wforms-inline.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/wforms_v530-14.js"></script>
    <script type="text/javascript" src="js/localization-en_US_v530-14.js"></script>         
    
</head>
<body class="default wFormWebPage">
<div class="container-fluid">
	<div id="tfaContent">
		<div class="wFormContainer" style="max-width: 85%; width:auto;" >
			<div class="row">
				<div class="col-sm-12">

    				<div class="wFormHeader"></div>
    		
					<h3 class="wFormTitle" style="text-align:center;text-decoration:underline;margin-bottom:2rem;">MHW Customer Set-up Form</h3>
								
					<form method="post" action="mhw_retail_form.php" class="hintsBelow labelsAbove" id="4690986" role="form" enctype="multipart/form-data">
									
						 <div class="form-group row">
							<label class="col-sm-2 col-form-label">Legal name:</label>
							<div class="col-sm-10">
							  <input type="text" class="form-control" placeholder="Legal Name" name="legal_name" maxlength="150" required>
							</div>
						  </div>
						  
						  <div class="form-group row">
							<label class="col-sm-2 col-form-label">DBA:</label>
							<div class="col-sm-10">
							  <input type="text" class="form-control" placeholder="DBA" name="dba" maxlength="150" required>
							</div>
						  </div>
						  
						  <div class="form-group row">
							<label class="col-sm-2 col-form-label">Street address:</label>
							<div class="col-sm-10">
							  <input type="text" class="form-control" placeholder="Street Address" name="street_address" maxlength="100" required>
							</div>
						  </div>					  
						  
						  <div class="form-group row">
							<label class="col-sm-2 col-form-label">City,State,Zip:</label>
							<div class="col-sm-10">
							  <input type="text" class="form-control" placeholder="City,State,Zip" name="citystatezip" maxlength="200" required>
							</div>
						  </div>	

						  <div class="form-group row">
							<label class="col-sm-2 col-form-label">Phone number:</label>
							<div class="col-sm-10">
							  <input type="tel" class="form-control" placeholder="Phone number" name="phone_number" maxlength="25" required>
							</div>
						  </div>					  					  
						  
						  <div class="form-group row">
							<label class="col-sm-2 col-form-label">Fax number:</label>
							<div class="col-sm-10">
							  <input type="tel" class="form-control" placeholder="Fax number" name="fax_number" maxlength="25" required >
							</div>
						  </div>	

						  <div class="form-group row">
							<label class="col-sm-2 col-form-label">Contact Person:</label>
							<div class="col-sm-10">
							  <input type="text" class="form-control" placeholder="Contact Person" name="contact_person" maxlength="25" required>
							</div>
						  </div>	

						  <div class="form-group row">	
						      <label class="col-sm-2 col-form-label">Contact Email Address:</label>	
							  <div class="col-sm-10">				
							     <input type="email" class="form-control" placeholder="Contact Email Address" name="contact_person_Email" required maxlength="150">
							  </div>
   						  </div>

						  <div class="form-group row">
							<label class="col-sm-2 col-form-label">Contact OPS:</label>
							<div class="col-sm-10">
							  <input type="text" class="form-control" placeholder="Contact OPS" name="contactNm_OPS" maxlength="25" required>
							</div>
						  </div>	

						  <div class="form-group row">	
						      <label class="col-sm-2 col-form-label">Contact OPS Email Address:</label>		      
							  <div class="col-sm-10">				
							     <input type="email" class="form-control" placeholder="Contact OPS Email Address" name="contactEmail_Ops" required maxlength="150">
							  </div>
   						  </div>

						  <div class="form-group row">
						      <label class="col-sm-2 col-form-label">ST-120:</label>	
							  <div class="col-sm-10">
									<input type="text" class="form-control" placeholder="ST-120" name="st120" maxlength="50" required>
							  </div>
						   </div>

						  <div class="form-group row">			
						      <label class="col-sm-2 col-form-label">Delivery Days:</label>	
							  <div class="col-sm-6">						
								<select class="custom-select" name="deliveryDays" required>	
									<option value=""></option>							  
									<option value="ALL">MONDAY^TUESDAY^WEDNESDAY^THURSDAY^FRIDAY^SATURDAY^SUNDAY</option>
									<option value="F">FRIDAY</option>
									<option value="MThF">MONDAY^THURSDAY^FRIDAY</option>
									<option value="MTuTh">MONDAY^TUESDAY^THURSDAY</option>
									<option value="MTuThF">MONDAY^TUESDAY^THURSDAY^FRIDAY</option>
									<option value="MTuThFSa">MONDAY^TUESDAY^THURSDAY^FRIDAY^SATURDAY</option>
									<option value="MTuThSa">MONDAY^TUESDAY^THURSDAY^SATURDAY</option>
									<option value="MTuW">MONDAY^TUESDAY^WEDNESDAY</option>
									<option value="MTuWF">MONDAY^TUESDAY^WEDNESDAY^FRIDAY</option>
									<option value="MTuWTh">MONDAY^TUESDAY^WEDNESDAY^THURSDAY</option>
									<option value="MTuWThFSa">MONDAY^TUESDAY^WEDNESDAY^THURSDAY^FRIDAY^SATURDAY</option>
									<option value="MWF">MONDAY^WEDNESDAY^FRIDAY</option>
									<option value="MWThF">MONDAY^WEDNESDAY^THURSDAY^FRIDAY</option>
									<option value="Th">THURSDAY</option>
									<option value="ThF">THURSDAY^FRIDAY</option>
									<option value="ThFSa">THURSDAY^FRIDAY^SATURDAY</option>
									<option value="Thu">Thursday</option>
									<option value="Tu">TUESDAY</option>
									<option value="TuF">TUESDAY^FRIDAY</option>
									<option value="TuTh">TUESDAY^THURSDAY</option>
									<option value="TuThF">TUESDAY^THURSDAY^FRIDAY</option>
									<option value="TuW">TUESDAY^WEDNESDAY</option>
									<option value="TuWF">TUESDAY^WEDNESDAY^FRIDAY</option>
									<option value="TuWTh">TUESDAY^WEDNESDAY^THURSDAY</option>
									<option value="TuWThF">TUESDAY^WEDNESDAY^THURSDAY^FRIDAY</option>
									<option value="TuWThFSa">TUESDAY^WEDNESDAY^THURSDAY^FRIDAY^SATURDAY</option>
									<option value="W">WEDNESDAY</option>
									<option value="WEEKDAY">MONDAY^TUESDAY^WEDNESDAY^THURSDAY^FRIDAY</option>
									<option value="WF">WEDNESDAY^FRIDAY</option>
									<option value="WFSa">WEDNESDAY^FRIDAY^SATURDAY</option>
									<option value="WTh">WEDNESDAY^THURSDAY</option>
									<option value="WThF">WEDNESDAY^THURSDAY^FRIDAY</option>					  
								</select>
								</div>
						  </div>

						 <div class="form-group row">
							<label class="col-sm-2 col-form-label">Territory:</label>
							<div class="col-sm-10">
							  <input type="text" class="form-control" placeholder="Territory" name="territory" maxlength="100" required>
							</div>
						  </div>

						  <div class="form-group row">
							<label class="col-sm-2 col-form-label">A\P Contact:</label>
							<div class="col-sm-10">
							  <input type="text" class="form-control" placeholder="A\P Contact" name="ap_contact" maxlength="100" required>
							</div>
						  </div>					  					
						  
						  <div class="form-group row">
							<label class="col-sm-2 col-form-label">A\P Email address(Invoices will go to this email address automatically)</label>
							<div class="col-sm-10">
							  <input type="email" class="form-control" placeholder="A\P Email address" name="ap_email_address" maxlength="150" required>
							</div>
						  </div>	

						  <div class="form-group row">
							<label class="col-sm-2 col-form-label">License Serial #:</label>
							<div class="col-sm-10">
							  <input type="text" class="form-control" placeholder="License Serial #" name="license_serial_number" maxlength="75" required>
							</div>
						  </div>					  										  
						  
						  <div class="form-group row">
							<label class="col-sm-2 col-form-label">Expiration Date:</label>
							<div class="col-sm-10">
							  <input type="text" class="form-control" placeholder="Expiration Date" name="expiration_date" required> 
							</div>
						  </div>					  										  
						  
						  <div class="form-group row">
							<label class="col-sm-2 col-form-label">NY/NJ/CA Sales Tax ID #:</label>
							<div class="col-sm-10">
							  <input type="text" class="form-control" placeholder="NY/NJ/CA Sales Tax ID #" name="ny_nj_ca_sales_tax_id" maxlength="50" required >
							</div>
						  </div>					  						

						   <div class="form-group row">
							<label class="col-sm-2 col-form-label">Delivery Instructions:</label>
							<div class="col-sm-10">
							  <textarea class="form-control" placeholder="Delivery Instructions" name="delivery_instructions" maxlength="250" required></textarea>
							</div>
						  </div>					  					
						  
						  <div class="form-group row">
							<label class="col-sm-2 col-form-label">Salesperson:</label>
							<div class="col-sm-10">
							  <input type="text" class="form-control" placeholder="Salesperson" name="sales_person" maxlength="150" required>
							</div>
						  </div>	
					
						 <div class="actions"><input type="submit" data-label="Submit" class="primaryAction btn btn-primary" id="submit_button" value="Submit"></div>
							<div style="clear:both"></div>
							
						 <input type="hidden" name="userID" id="userID" value="<?php echo $_SESSION['mhwltdphp_user']; ?>">

					</form>
				</div>
			</div>
		</div>    
	</div>

	<script src='js/iframe_resize_helper_internal.js'></script>
</div>
</body>
</html>
<?php
}
?>