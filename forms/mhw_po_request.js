var suppliers = [];

		$(document).ready(function(){
			/*======BEGIN INITIAL PAGE LOAD======*/
			
			//if product id is passed to page, preload with data
			var prodIDinit = $("#prodID").val();
			if(parseInt(prodIDinit) > 0){
				prefill_prod(prodIDinit);

				cbb_formatter();
			}

			/*======END INITIAL PAGE LOAD======*/

			/*======BEGIN comboboxes==========*/
			function cbb_formatter(){ 				
				$('ul.ac_results').css("margin-left","0em");
				$('ul.ac_results').css("margin","0em");
				$('.ac_button').css("height","28px");
				$('.ac_button').css("background","transparent");
				$('.ac_button').css("margin-left","-35px");
				$('.ac_button').css("border","0px");
				$('.ac_subinfo dl').css("margin-left","0em");
				$('.ac_subinfo dl').css("margin","0em");
			}
			//load form fields - as a result of selecting product_desc combobox
			function prefill_prod(prodID){
				var qrytype = "prodByID";
				var client = $('#tfa_client_name').val(); //get value of the client combobox
				$.post('query-request.php', {qrytype:qrytype,prodID:prodID,client:client}, function(dataPx){ // ajax request query-request.php, send brand as post variable, return dataX variable, parse into JSON

					var t = JSON.parse(dataPx);

					$.each( t, function( key, value ) {
						//product

						$('#product_mhw_code').val(value.product_mhw_code);
						$('#product_mhw_code_search').val(value.product_mhw_code_search);	
	
					});
				});
			}
			function isProdValidSelection(){
				var isProdID = $('#tfa_product_desc')[0].hasAttribute("sub_info"); //free-type data will not have sub_info attribute
				if(isProdID){
					var json = $('#tfa_product_desc').attr('sub_info');
					//if additional sub_info elements are added, additional apostrophe replacements need to be added
					json = json.replace( /\'/g, '"' );
					var jsonObj = JSON.parse(json);

					$("#prodID").val(jsonObj.id);  //set field for processing update
					//$("#tfa_399-L").html("Product: editing product_id "+jsonObj.id); //display message for update
					//prefill_prod(jsonObj.id); //prefill form fields					
					$("#productvalid").hide();
				}
				else{
					//$("#tfa_399-L").html("Product: new entry"); //display message for insert
					$("#prodID").val("");  //clear field for insert	
					$("#productvalid").show();					
				}
			}
			
			//initialize product combobox
			function initialize_product(){
				//$('#tfa_product_desc').parent().find(".ac_button").remove();  //clear elements of previous instance
				//$('#tfa_product_desc').parent().find(".ac_result_area").remove();  //clear elements of previous instance
				//var client = $('#tfa_client_name').val(); //get value of the client combobox
				var client = 'MHW Web Demo';
				//var brand = $('#tfa_brand_name').val(); //get value of the brand combobox
				var brand = '';
				var cbbtype = 'item';
				$.post('select-request.php', {cbbtype:cbbtype,client:client,brand:brand}, function(dataX){ // ajax request select-request.php, send brand as post variable, return dataX variable, parse into JSON
					var data = JSON.parse(dataX);
					$(function() {
						$('#tfa_product_desc').ajaxComboBox(
							data,
							{
							  bind_to: 'tfa_product_desc',
							  sub_info: true,
							  sub_as: {
								id: 'Item ID',
								code: 'MHW Item Code',
								code2: 'Client Item Code',
								parent: 'Product Code',
								parent2: 'Product Description',
								brand: 'Brand'
							  },
							}
						).bind('tfa_product_desc', function() {
						  var json = $(this).attr('sub_info');
						  //if additional sub_info elements are added, additional apostrophe replacements need to be added
						  json = json.replace( /\'/g, '"' );
						  var jsonObj = JSON.parse(json);

						  if(parseInt(jsonObj.id) > 0){
							$("#prodID").val(jsonObj.id); //set field for processing update
							//$("#tfa_399-L").html("Product: editing product_id "+jsonObj.id); //display message for update
							prefill_prod(jsonObj.id); //prefill form fields
						  }
						  isProdValidSelection();

						});
						cbb_formatter(); //apply css and classname changes to combobox elements
					});
				});
				$('#tfa_product_desc').bind("change", function(){ 
					isProdValidSelection();
					disableAddBtn();
				});
			}
			initialize_product();
			disableAddBtn();


			function isSupplierValidSelection(){
				var isInfo = $('#suppliers_combo')[0].hasAttribute("sub_info"); //free-type data will not have sub_info attribute
				if(isInfo){
					var json = $('#suppliers_combo').attr('sub_info');
					//if additional sub_info elements are added, additional apostrophe replacements need to be added
					json = json.replace( /\'/g, '"' );
					var jsonObj = JSON.parse(json);
					$("#supplierID").val(jsonObj.id);  //set field for processing update
				}
				else{
					$("#supplierID").val("");  //clear field for insert	
				}
			}

			//initialize supplier combobox
			function init_suppliers(){
				$.post('select-request.php', {cbbtype:'po-suppliers'}, function(dataX) { 
					var data = JSON.parse(dataX);
					suppliers = data;
					
					$('#supplier_selected').text("");
					$('#supplier_selected').append("<option value=''>Select Supplier ...</option>");
					suppliers.forEach(s => {
						$('#supplier_selected').append(`<option value='${s.id}'>${s.supplier_name}</option>`);
					});

					/*
					$(function() {
						$('#suppliers_combo').ajaxComboBox(
							data,
							{
							  bind_to: 'suppliers_combo',
							  sub_info: true,
							  sub_as: {
								id: 'ID',
								supplier_contact: 'Contact',
								supplier_email: 'Email',
								supplier_phone: 'Phone',
							  },
							}
						).bind('suppliers_combo', function() {
						  var json = $(this).attr('sub_info');
						  //if additional sub_info elements are added, additional apostrophe replacements need to be added
						  json = json.replace( /\'/g, '"' );
						  var jsonObj = JSON.parse(json);
						  console.log(jsonObj);
						});
						cbb_formatter(); //apply css and classname changes to combobox elements
					});
					*/
				});

				$('#suppliers_combo').bind("change", function(){ 
					isSupplierValidSelection();
					//disableAddBtn();
				});
			}
			init_suppliers();
			
			/*======END comboboxes==========*/
			
			//Display/Hide Divs functionality
			
			CheckMMBoxes();
			
			$("form").change(function(){   				
				   CheckMMBoxes();
			});
						
			$("#submit_button").click(function(e) {								
				if($("#items-repeater .items").length < 1) {
					e.preventDefault();
					alert("Please input at least one item");
				}

				if($("#is_mhw_arranging_freight").val()=="No") {
					if($("#suppliers-repeater .items").length < 1) {
						e.preventDefault();
						alert("Please input at least one supplier");
					}
				}
				
			});
						
		   function CheckMMBoxes() {
			   $(".hiddeninputs").each(function () {
					 var currdiv = $(this).attr('data-div');
									
					
				 if ($(this).is(":checked")) {	         
					   $("#" + currdiv).show();
					   $("#" + currdiv + " input").prop('required',true);
					 } else {
							$("#" + currdiv).hide();
							$("#" + currdiv + " input").prop('required',false);
						 }
				});
		   } 
		   
			function disableAddBtn() {
				
				var isProdID = $('#tfa_product_desc')[0].hasAttribute("sub_info"); //free-type data will not have sub_info attribute
							
				if(!isProdID)
					$(".repeater-add-item-btn").prop('disabled',true);
				else
					$(".repeater-add-item-btn").attr('disabled',false);			
			}

			/*
			function displayNewSupplier(sbox) {
			   if(suppliername.val() == "new") {
				  $("#nsupplier_fields").show();
				  $(".supplierreq").prop('required',true);
			   }
			   else {
					$("#nsupplier_fields").hide();
					$(".supplierreq").prop('required',false);
			   }
		   }		   
		   
		   //Hide/Display Supplier Fields
		   	var suppliername = $("#supplier_name");	   
			suppliername.change(function(supplier){displayNewSupplier(supplier)});
		   */


		   //Initialize repeating boxes
		   $("#items-repeater").createRepeater();
		   $("#suppliers-repeater").createRepeater();

			$(".repeater-add-item-btn").click(function() {
				$(".repeater-add-item-btn").prop('disabled',true);					
			});
			
			//Clear form
			$("#clearform").click(function() {
				$("4690986").trigger('reset');
				setTimeout(function(){
					$("#supplier_name").trigger('change');
					$("#items-repeater .items").remove();
					$("#suppliers-repeater .items").remove();
				},500);
			});		   

			$("#is_mhw_arranging_freight").change(e=>{
				if (e.target.value=="Yes") {
					$(".freight_forwarder_block").hide();
					$(".freight_forwarder_block_reverse").show();
					$(".mhw_arranging_freight_required").show();
					
					$("#container_type").prop('required',true);
					$("#container_temperature").prop('required',true);

					$("#freight_forwarder_name").removeAttr("required");
					$("#eta_to_port").removeAttr("required");
					$("#booked_to").removeAttr("required");

				} else {
					$(".freight_forwarder_block").show();
					$(".freight_forwarder_block_reverse").hide();
					$(".mhw_arranging_freight_required").hide();

					$("#container_type").removeAttr("required");
					$("#container_temperature").removeAttr("required");

					$("#freight_forwarder_name").prop('required',true);
					$("#eta_to_port").prop('required',true);
					$("#booked_to").prop('required',true);
				}
			});

			$("#container_type").change(e=>{
				if (e.target.value=="LCL") {
					$(".lcl_block").show();
					$("input[name='number_of_pallets']").attr('required');
					$("input[name='total_weight']").attr('required');
				}else{
					$(".lcl_block").hide();
					$("input[name='number_of_pallets']").removeAttr('required');
					$("input[name='total_weight']").removeAttr('required');
				}
			});

			$("#add_supplier").click(function() {
				$.post('add-po-supplier.php', $("#supplier-form").serialize(), function(dataX) { 
					console.log(dataX);
					$("#addSupplierModal").modal('hide');
					init_suppliers();
				});
			});

		});