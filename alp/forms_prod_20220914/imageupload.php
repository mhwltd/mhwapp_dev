<?php
//session_start();
include("sessionhandler.php");

include("prepend.php");

//=============settings============
include("settings.php");

$pageref = str_replace(".php","",substr(__FILE__,strrpos(__FILE__,"\\")+1));


if(intval($_GET['pid']) > 0){ $prodID = intval($_GET['pid']); }
if(intval($_POST['prodID']) > 0){ $prodID = intval($_POST['prodID']); }

$clientlist =  "'".str_replace(";","','",$_SESSION['mhwltdphp_userclients'])."'";
//=============settings============

//=============functions============
include("functions.php");
//=============functions============



if(!isset($_SESSION['mhwltdphp_user'])){

	header("Location: login.php");
}
else{
	include("head.php");
	echo "<div class=\"container-fluid\">";
	echo "<div id=\"".$pageref."\">";
	//=============file upload processing============
	if(isset($_POST['uploadform'])){
	
		if(isset($prodID) && intval($prodID) > 0){

			/***BEGIN Product Code Lookup for Filename***/
			include("dbconnect.php");
			$conn = sqlsrv_connect($serverName, $connectionOptions);
			if( $conn === false) {
		    		die( print_r( sqlsrv_errors(), true));
			}
		
			$tsql_pcode= "SELECT product_mhw_code FROM [dbo].[mhw_app_prod] WHERE [product_id] = ".$prodID; 
			
			$prodCODE = '';
			
			$stmt_pcode = sqlsrv_query($conn, $tsql_pcode);
		
			while ($row_pcode = sqlsrv_fetch_array($stmt_pcode, SQLSRV_FETCH_ASSOC)) {
				$prodCODE = $row_pcode['product_mhw_code']; 
			}
			
			if( $stmt_pcode === false ) {
				if( ($errors = sqlsrv_errors() ) != null) {
					//log sql errors for writing to server later
					foreach( $errors as $error ) {
						$errorlog.= $stmt_pcode."\n";
						$errorlog.= "SQLSTATE: ".$error[ 'SQLSTATE']."\n";
						$errorlog.= "code: ".$error[ 'code']."\n";
						$errorlog.= "message: ".$error[ 'message']."\n";
					}
				}
				$errorFree=0;
			} //end error handling
	  
			//release sql statement
	  		sqlsrv_free_stmt($stmt_img);
			sqlsrv_close($conn); 
			/***END Product Code Lookup for Filename***/

			if(isset($_POST['img_type']) && $_POST['img_type'] !== ""){

				/* Getting file name */
					$filename = convert_ascii($_FILES['filetoupload']['name']);
				$filetype = str_replace(" ", "",$_POST['img_type']);

				/* Location */
				$location = $upload_dir.$filename;
				$uploadOk = 1;
				$imageFileType = pathinfo($location,PATHINFO_EXTENSION);

				copy($_FILES['filetoupload']['tmp_name'],$upload_dir."temp/".strtotime("now").$filename);
				//check if no file selected.
				if(!is_uploaded_file($_FILES['filetoupload']['tmp_name'])){
					$pageerr="Upload Failure: Please select a file to upload\n";
					$uploadOk=0;
				}
				if($pageerr==""){
					//check if the directory exists or not.
					if (!is_dir($upload_dir)) {
						$pageerr="Upload Failure: The upload directory does not exist\n";
						$uploadOk=0;
					}
				}
				if($pageerr==""){
					//check if the directory is writable.
					if (!is_writeable($upload_dir)){
						$pageerr="Upload Failure: The upload directory is not writable\n";
						$uploadOk=0;

					}
				}
				 if($pageerr==""){
					//Get the Size of the File
					if(isset($size_bytes_limit)){
						$size = $_FILES['filetoupload']['size'];
						//Make sure that file size is correct
						if($size > $size_bytes_limit){
							$kb = $size_bytes_limit / 1024;
							$pageerr="Upload Failure: File size is too large. File must be under ".$kb." KB\n";
								$uploadOk=0;
						}
					}
				} 
				if($pageerr==""){
					//check file for timeout
					$size = $_FILES['filetoupload']['size'];
					if($size==0){
						$pageerr="Upload Failure: Temporary file expired.  Please try again\n";
						$uploadOk=0;
					}
				}
				if($pageerr==""){
					//check file extension
					$ext = strrchr($_FILES['filetoupload'][name],'.');
					$ext = strtolower($ext);
					if(($extlimit == "yes") && (!in_array($ext,$limitedext))){
						$pageerr="Upload Failure: File uploads of this type ".$ext." are not allowed\n";
						$uploadOk=0;
					}
				}

				if($pageerr==""){
					//$filename will hold the value of the file name submitted from the form.
						$filename = 'p'.$prodID.'_'.$prodCODE.'_'.$filetype.'__'.convert_ascii($_FILES['filetoupload']['name']);
					    $filename = str_replace(' ','',$filename);
					    $filename = str_replace('%20','',$filename);
						$filename = str_replace('+','',$filename);
						$filename = str_replace("'","",$filename);
					// Check if file is already exists.
					if(file_exists($upload_dir.$filename)){
						//$pageerr="Upload Failure: The file named $filename already exists\n";
						//$uploadOk=0;
						$filename = 'p'.$prodID.'_'.$prodCODE.'_'.$filetype.'__'.strtotime("now").'_'.convert_ascii($_FILES['filetoupload']['name']);
					    $filename = str_replace(' ','',$filename);
					    $filename = str_replace('%20','',$filename);
						$filename = str_replace('+','',$filename);
						$filename = str_replace("'","",$filename);						
					}
				}
				if($pageerr==""){
					//echo $upload_dir.$filename;
					//Move the File to the Directory of your choice
					//move_uploaded_file('filename','destination') Moves file to a new location.
					if(move_uploaded_file($_FILES['filetoupload']['tmp_name'],$upload_dir.$filename)){
						createThumbs($upload_dir,$thumb_dir,$filename,200);
					}	
					else{
						$pageerr="Upload Failure: There was a problem moving your file\n";
						$uploadOk=0;
					}
				}
			}
			else{
				$pageerr="Upload Failure: Image Type is required\n";
				$uploadOk=0;
			}
		}
		else{
			$pageerr="Upload Failure: There was no product associated with the upload\n";
			$uploadOk=0;
			
		}	
		/*
		if($uploadOk == 0){
		   echo $pageerr;
		}else{
		   //Upload file 
		   if(move_uploaded_file($_FILES['file']['tmp_name'],$location)){
		      echo $location;
		   }else{
		      echo $pageerr;
		   }
		}*/
		
		
		if($uploadOk == 0){
		   //echo $pageerr;
		   $errorFree=0;
		   echo "<div class=\"alert alert-danger\" role=\"alert\">".$pageerr."</div>";
		}
		else{
		/***BEGIN FILE DB LOGGING***/
			include("dbconnect.php");
			$conn = sqlsrv_connect($serverName, $connectionOptions);
			if( $conn === false) {
		    		die( print_r( sqlsrv_errors(), true));
			}
		
			 $tsql_img= "EXEC [dbo].[usp_product_image] 
			 @product_id =".$prodID."
			,@image_body = '".base64_encode($_FILES['filetoupload'])."'
			,@image_name = '".$filename."'
			,@image_dim = '".str_replace("'","",$_POST['img_dim'])."'
			,@image_type = '".$_POST['img_type']."'
			,@image_note = '".str_replace("'","''",$_POST['img_note'])."'
			,@create_via = 'imageupload'
			,@username = '".$_SESSION['mhwltdphp_user']."'"; 
			
			$imgIDx = 0;
			
			$stmt_img = sqlsrv_query($conn, $tsql_img);
		
			while ($row_img = sqlsrv_fetch_array($stmt_img, SQLSRV_FETCH_ASSOC)) {
				$imgIDx = $row_img['imgID']; 
			}
			//echo  "IMGID:".$imgIDx;
			
			if( $stmt_img === false ) {
				if( ($errors = sqlsrv_errors() ) != null) {
					//log sql errors for writing to server later
					foreach( $errors as $error ) {
						$errorlog.= $tsql_img."\n";
						$errorlog.= "SQLSTATE: ".$error[ 'SQLSTATE']."\n";
						$errorlog.= "code: ".$error[ 'code']."\n";
						$errorlog.= "message: ".$error[ 'message']."\n";
					}
				}
				$errorFree=0;
			} //end error handling
	  
			//release sql statement
	  		sqlsrv_free_stmt($stmt_img);
			sqlsrv_close($conn); 
		/***END FILE DB LOGGING***/
		} //end file upload success

	}

	//=============file upload processing============

	//=============file upload form============
	if($prod_class!=="READONLY"){
		echo "<div class=\"row\">";
  		echo "<div class=\"col-5\">";
		
		echo "<div class=\"jumbotron jumbotron-fluid\">";
		  echo "<div class=\"container\">";
		    echo "<h1 class=\"display-4\">Upload a Product Image:</h1>";
		    echo "<p class=\"lead\">Please upload your label images or approved COLA.<br /><br />";

				echo "Note each image file must:";
				echo "<ul><li>Have a file type of JPG, PDF or TIFF (file extensions: .jpg/.jpeg/.jpe or .pdf or .tif/.tiff)</li>";
				echo "<li>Not exceed 7500KB in size</li>";
				echo "<li>Have a compression/quality ratio set at Medium (7 out of 10 or 70 out of 100)</li>";
				echo "<li>Utilize colors in the RGB color mode, not colors in the CMYK color mode</li>";
				echo "<li>Have no surrounding white space or printer&apos;s proof detail; this must be cropped out</li></ul></p>";
		  echo "</div>";
		echo "</div>";
		
		echo "</div>";
  echo "<div class=\"col-7\">";

		
		echo "<div id=\"".$pageref."_form\">";
		echo "<div class=\"form-group\">";
			//echo "<h2>Add a File Attachment:</h2>";
			echo "<form method=\"post\" enctype=\"multipart/form-data\" action=\"imageupload.php\">";
			//echo "<form method=\"post\" enctype=\"multipart/form-data\" action=\"\" onsubmit=\"return false;\">";
			
			echo "<div id=\"prodID_lbl\" class=\"pairlbl\">Product:  <span style=\"color:red;\">*</span></div>";
			echo "<SELECT ID=\"prodID\" NAME=\"prodID\" pageref=\"".$pageref."\" class=\"valdrop required\">";
			
				include("dbconnect.php");
				$conn = sqlsrv_connect($serverName, $connectionOptions);
				if( $conn === false) {
			    		die( print_r( sqlsrv_errors(), true));
				}
				
				//$tsql_prod = "SELECT [product_id], [product_mhw_code], [product_desc], [brand_name], [client_name] FROM [dbo].[mhw_app_prod] WHERE client_name IN (".$clientlist.") AND [active] = 1 AND [deleted] = 0";
				$tsql_prod = "EXEC [dbo].[usp_viewfiles] @querytype = 'imageupload', @client = ".$clientlist.", @prodID = 0, @imgID = 0";
					//echo $tsql_prod;
				$stmt_prod = sqlsrv_query($conn, $tsql_prod);
				
				if(!isset($prodID) || intval($prodID)==0){ echo "<option value=\"\" class=\"\">Please select...</option>"; }
				
				while ($row_prod = sqlsrv_fetch_array($stmt_prod, SQLSRV_FETCH_ASSOC)) {

					if($prodID==$row_prod['product_id']){
						echo "<option value=\"".$row_prod['product_id']."\" class=\"\" selected>".$row_prod['brand_name']." - ".$row_prod['product_desc']."</option>";
					}
					else{
						echo "<option value=\"".$row_prod['product_id']."\" class=\"\">".$row_prod['brand_name']." - ".$row_prod['product_desc']."</option>";
					}
				}
				sqlsrv_free_stmt($stmt_prod);
			
				sqlsrv_close($conn);  
			echo "</SELECT>";
			echo "<BR />";
			//echo "<input type=\"text\" name=\"prodID\" value=\"".$prodID."\">";
			
			echo "<div id=\"img_filename_lbl\" class=\"pairlbl\">File:  <span style=\"color:red;\">*</span></div>";
			echo "<input type=\"file\"   accept=\".jpg,.jpeg,.jpe,.pdf,.tif,.tiff\" size=\"50\"  class=\"form-control-file required\" name=\"filetoupload\" id=\"filetoupload\">";
			echo "<BR />";

			echo "<div id=\"img_type_lbl\" class=\"pairlbl\">Image Type: <span style=\"color:red;\">*</span></div>";
			echo "<SELECT ID=\"img_type\" NAME=\"img_type\" pageref=\"".$pageref."\" class=\"valdrop required\">";
			//include('core/options_img-file-class.php');
			echo "<option value=\"\">Please select...</option>";
			echo "<option value=\"Front\" class=\"\">Front</option>";
			echo "<option value=\"Back\" class=\"\">Back</option>";
			echo "<option value=\"Neck\" class=\"\">Neck</option>";
			echo "<option value=\"Approved COLA\" class=\"\">Approved COLA</option>";
			echo "<option value=\"Other\" class=\"\">Other</option>";
			echo "</SELECT>";
			echo "<BR />";
			
			echo "<div id=\"img_dim_lbl\" class=\"pairlbl\">Image Dimensions: </div>";
			echo "<input type=\"text\" id=\"img_dim\" name=\"img_dim\" size/></TEXTAREA>";
			echo "<BR />";
				
			echo "<div id=\"img_note_lbl\" class=\"pairlbl\">Note: </div>";
			echo "<TEXTAREA id=\"img_note\" name=\"img_note\" ROWS=\"6\" COLS=\"30\"/></TEXTAREA>";
			echo "<BR />";

			echo "<div id=\"img_btns\">";
			echo "<input type=\"hidden\" name=\"action\" value=\"upload\">";
			//echo "<input type=\"Submit\" name=\"uploadform\" class=\"btn btn-primary\" value=\"Upload File\">";
			 //echo "<input type='button' class='btn btn-info' value='Upload' id='upload'>Upload</button>";

			 echo "<button type=\"submit\" name=\"uploadform\" class=\"btn btn-primary bg_arrow_blue\">";
			 echo "<span class=\"oi oi-data-transfer-upload\" title=\"data-transfer-upload\" aria-hidden=\"true\"></span> Upload";
			 echo "</button>";

			echo "</div>";
		echo "</div>";
		echo "</form>";
		echo "</div><br><br>";
	}
	  echo "</div>";
echo "</div>";

echo "<div class=\"row\">";
echo "<div class=\"col-12\">";
	//=============list attached files============
		echo "<h2>Attached Files:</h2>";
		echo "<div id=\"attached\">";
		include('viewfiles.php');
		echo "</div>";
	echo "</div>";	
echo "</div>";	
echo "</div>";
}
?>
<div class="modal modal-wide fade" id="fileModali" tabindex="-1" role="dialog" aria-labelledby="fileModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="fileModalLabel">Files</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"></div>
	  <div id='preview'></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script>

$(document).ready(function(){
	/*$('a.dwnld').click(function(e) {
	    e.preventDefault();  //stop the browser from following
		var imagefile = $(this).data('url');
	    window.location.href = imagefile;
	});*/

	//initialize download button for outside modal
	$('.dwnld').on('click', function () {
		var imagefile = $(this).data('url');
		var imagename = $(this).data('imagename');
		$.ajax({
			url: imagefile,
			method: 'GET',
			xhrFields: {
				responseType: 'blob'
			},
			success: function (data) {
				var a = document.createElement('a');
				var url = window.URL.createObjectURL(data);
				a.href = url;
				a.download = imagename;
				a.click();
				window.URL.revokeObjectURL(url);
			}
		});
	});
	//launch modal
	$('.prodimg_thumb').on('click',function(){
		var imageid = $(this).data('imageid');
		var productid = $(this).data('product');
		var imageurl = 'viewfiles.php?iid='+imageid;

	    $('.modal-body').load(imageurl,function(){
	        $('#fileModali').modal({show:true});

			//initialize download button in-modal
			$('.dwnld').on('click', function () {
				var imagefile = $(this).data('url');
				var imagename = $(this).data('imagename');
				$.ajax({
					url: imagefile,
					method: 'GET',
					xhrFields: {
						responseType: 'blob'
					},
					success: function (data) {
						var a = document.createElement('a');
						var url = window.URL.createObjectURL(data);
						a.href = url;
						a.download = imagename;
						a.click();
						window.URL.revokeObjectURL(url);
					}
				});
			});

	    });
	});
	//product is changed
	$("#prodID").on("change", function() {
		var prodID = $(this).val();
		var whereami = "imageupload";
		$.post('viewfiles.php?pid='+prodID, {prodID:prodID,whereami:whereami}, function(data){ // ajax request query-request.php, send brand as post variable, return dataX variable, parse into JSON

			$("#attached").html(data); //replace Attached Files section with product-specific table
			
			//re-bind launch modal
			$('.prodimg_thumb').bind('click',function(){
				var imageid = $(this).data('imageid');
				var productid = $(this).data('product');
				var imageurl = 'viewfiles.php?iid='+imageid;

				$('.modal-body').load(imageurl,function(){
					$('#fileModali').modal({show:true});

					//initialize download button in-modal
					$('.dwnld').on('click', function () {
						var imagefile = $(this).data('url');
						var imagename = $(this).data('imagename');
						$.ajax({
							url: imagefile,
							method: 'GET',
							xhrFields: {
								responseType: 'blob'
							},
							success: function (data) {
								var a = document.createElement('a');
								var url = window.URL.createObjectURL(data);
								a.href = url;
								a.download = imagename;
								a.click();
								window.URL.revokeObjectURL(url);
							}
						});
					});

				});
			});		
		});
	});

	$(document).off('change', '#filetoupload').on('change', '#filetoupload',function(e) {
		//alert('change');
		//on change event  
		var filesizelimit =  <?php echo $size_bytes_limit; ?> / 1024;
		var filesize;
		var filevalidated = 1;
		formdata = new FormData();
		if($(this).prop('files').length > 0)
		{
			$.each(this.files, function (index, value) {
				filesize = Math.round((value.size / 1024));
				if(filesize > filesizelimit) {  
					alert('File size is too large. Limit is '+filesizelimit+' KB');
					filevalidated = 0;
				}
			})
			if(filevalidated==1){
			file =$(this).prop('files')[0];
			formdata.append("filetoupload", file);

			jQuery.ajax({
				url: "ajax_save_file.php?from=imageupload",
				type: "POST",
				data: formdata,
				processData: false,
				contentType: false,
				success: function (result) {
					 // if all is well
					 //alert(result);
				}
			});
			}
			else{
				$(this).val(null);
			}
		}
	});

});

</script>