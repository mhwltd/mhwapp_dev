<?php
ini_set('memory_limit','512M');
ini_set('max_execution_time', '500');
include('settings.php');
include('functions.php');
include ('Classes/PHPExcel/IOFactory.php');
error_reporting(E_ALL); //displays an error
//set_time_limit(0);    
 

session_start();
if(!isset($_SESSION['mhwltdphp_user'])){
	die( "Not authenticated !" );  
}
$isAdmin = in_array($_SESSION['mhwltdphp_usertype'], ['ADMIN','SUPERUSER']);
include("dbconnect.php");

//Establishes the connection
$conn = sqlsrv_connect($serverName, $connectionOptions);
if( $conn === false) {
		die( print_r( sqlsrv_errors(), true));
}


//echo "<pre>"; print_r($_POST); exit; 

// total_num pagination 
$pagination_total_num=0;
$tsql_total_num="";
$redirect_url="viewproducts.php";

/* ********************* added by Jobaidur :start ************************ */
if($_POST['export_view_type']=='viewproducts' && $_POST['export_qry_type']=='VPitems')
{
	    $redirect_url="viewproducts.php";
	    $client_name_qry="p.[client_name] = '".addslashes($_POST['export_client'])."' AND ";
		// pagination custom searching 
		$filter_txt_qry="";
		if($_POST['export_filter_txt'] && $_POST['export_filter_txt']!='')
		{
			$filter_txt_val=$_POST['export_filter_txt'];
			$filter_txt_qry=" AND ( p.[product_mhw_code] like '%".$filter_txt_val."%'  OR p.[brand_name] like '%".$filter_txt_val."%' OR p.[product_desc] like '%".$filter_txt_val."%'
			OR i.[item_client_code] like '%".$filter_txt_val."%'  OR i.[item_mhw_code] like '%".$filter_txt_val."%' OR i.[item_description] like '%".$filter_txt_val."%'
			OR i.[container_type] like '%".$filter_txt_val."%'  OR i.[container_size] like '%".$filter_txt_val."%' OR i.[stock_uom] like '%".$filter_txt_val."%'
			OR i.[bottles_per_case] like '%".$filter_txt_val."%'  OR i.[upc] like '%".$filter_txt_val."%' OR i.[scc] like '%".$filter_txt_val."%'
			OR i.[vintage] like '%".$filter_txt_val."%'  OR i.[various_vintages] like '%".$filter_txt_val."%' OR i.[height] like '%".$filter_txt_val."%'
			OR i.[length] like '%".$filter_txt_val."%'  OR i.[width] like '%".$filter_txt_val."%' OR i.[weight] like '%".$filter_txt_val."%'
			OR i.[chill_storage] like '%".$filter_txt_val."%'  OR i.[outer_shipper] like '%".$filter_txt_val."%' OR i.[bottle_material] like '%".$filter_txt_val."%'
			OR i.[create_via] like '%".$filter_txt_val."%')";
		}	
		
		// fetching total_num for pagination 
		
		$tsql_total_num = "SELECT count(i.[item_id]) as total_num	
			FROM [dbo].[mhw_app_prod_item] i WITH (NOLOCK) 
			left outer join [dbo].[mhw_app_prod] p WITH (NOLOCK) on p.[product_id] = i.[product_id] 
			WHERE  $client_name_qry  p.[active] = 1 AND p.[deleted] = 0 AND i.[active] = 1 AND i.[deleted] = 0  AND p.[product_desc] <> 'STAR' $filter_txt_qry";
		
		
		
		$tsql = "SELECT i.[item_id],i.[item_client_code],i.[item_mhw_code],
			  i.[item_description]
			,i.[container_type],i.[container_size],i.[stock_uom],i.[bottles_per_case],i.[upc],i.[scc],i.[vintage],i.[various_vintages],i.[height],i.[length],i.[width],i.[weight],i.[chill_storage],i.[item_status],i.[outer_shipper],i.[bottle_material],i.[create_via],i.[create_date],i.[edit_date],p.[product_id],p.[client_code],p.[client_name]
			,p.[brand_name]
			,p.[product_desc]
			,p.[product_mhw_code]
            ,p.[federal_type]
			,convert(varchar, i.create_date, 120) as item_creation_date
			,(select top 1 username from mhw_app_workflow where record_id=i.item_id and workflow_type in ('product_item','product_item_import') 
			order by workflow_id asc) as item_created_by
            ,i.finalized
            ,i.item_class
            ,i.unit_dimensions
            ,i.weight_uom	
			FROM [dbo].[mhw_app_prod_item] i WITH (NOLOCK) 
			left outer join [dbo].[mhw_app_prod] p WITH (NOLOCK) on p.[product_id] = i.[product_id] 
			WHERE  $client_name_qry  p.[active] = 1  AND p.[deleted] = 0 AND i.[active] = 1 AND i.[deleted] = 0  AND p.[product_desc] <> 'STAR' $filter_txt_qry
			ORDER BY p.[product_id], i.[create_date] desc
           ";
		   
	
}
else if($_POST['export_view_type']=='viewproducts' && $_POST['export_qry_type']=='VPdefault')
{
	    $redirect_url="viewproducts.php";
	    $client_name_qry="p.[client_name] = '".addslashes($_POST['export_client'])."' AND ";
		// pagination custom searching 
		$filter_txt_qry="";
		if($_POST['export_filter_txt'] && $_POST['export_filter_txt']!='')
		{
			$filter_txt_val=$_POST['export_filter_txt'];
			$filter_txt_qry=" AND ( p.[client_code] like '%".$filter_txt_val."%'  OR p.[client_name] like '%".$filter_txt_val."%' OR p.[brand_name] like '%".$filter_txt_val."%'  OR p.[product_desc] like '%".$filter_txt_val."%' 
			OR p.[product_mhw_code] like '%".$filter_txt_val."%' OR p.[product_mhw_code_search] like '%".$filter_txt_val."%'  OR p.[TTB_ID] like '%".$filter_txt_val."%' OR p.[federal_type] like '%".$filter_txt_val."%'
			OR p.[compliance_type] like '%".$filter_txt_val."%'  OR p.[product_class] like '%".$filter_txt_val."%' OR p.[mktg_prod_type] like '%".$filter_txt_val."%'
			OR p.[bev_type] like '%".$filter_txt_val."%'  OR p.[fanciful] like '%".$filter_txt_val."%' OR p.[country] like '%".$filter_txt_val."%'
			OR p.[appellation] like '%".$filter_txt_val."%'  OR p.[lot_item] like '%".$filter_txt_val."%' OR p.[bottle_material] like '%".$filter_txt_val."%'
			OR p.[alcohol_pct] like '%".$filter_txt_val."%'  OR p.[varietal] like '%".$filter_txt_val."%' OR s.[supplier_name] like '%".$filter_txt_val."%'
			OR s.[supplier_contact] like '%".$filter_txt_val."%'  OR s.[supplier_fda_number] like '%".$filter_txt_val."%' OR s.[tax_reduction_allocation] like '%".$filter_txt_val."%'
			OR s.[supplier_address_1] like '%".$filter_txt_val."%' )";	
		}	
		
		// fetching total_num for pagination 
		
		$tsql_total_num = "SELECT count(p.[product_id]) as total_num FROM [dbo].[mhw_app_prod] p WITH (NOLOCK) 
			 LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] s WITH (NOLOCK) on s.[supplier_id] = p.[supplier_id] 
			 WHERE $client_name_qry p.[active] = 1 AND p.[deleted] = 0  AND p.[product_desc] <> 'STAR' $filter_txt_qry";
		
		
		
		$tsql = "SELECT p.[product_id],p.[client_code],p.[client_name],p.[brand_name],p.[product_desc],
				p.[product_mhw_code],p.[product_mhw_code_search],p.[TTB_ID],p.[federal_type],
				p.[compliance_type],p.[product_class],p.[mktg_prod_type],p.[bev_type],p.[fanciful],p.[country],p.[appellation],p.[lot_item],p.[bottle_material],p.[alcohol_pct],p.[varietal],
				p.[create_date],s.[supplier_id],s.[supplier_name],s.[supplier_contact],s.[supplier_fda_number],s.[tax_reduction_allocation],s.[supplier_address_1],s.[supplier_address_2],
				s.[supplier_address_3],s.[supplier_city],s.[supplier_state],s.[supplier_country] ,s.[supplier_zip]  ,s.[supplier_phone] ,s.[supplier_email], s.[federal_basic_permit] ,s.[create_via] ,
				s.[create_date],s.[edit_date] 
			,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_item] i WHERE i.[product_id] = p.[product_id] AND i.[active] = 1 AND i.[deleted] = 0) as 'itemcount' 
			,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount' 
			,convert(varchar, p.create_date, 120) as product_creation_date
            ,(select top 1 username from mhw_app_workflow where record_id=p.product_id and workflow_type in ('product','product_import') 
order by workflow_id asc) as product_created_by
            ,p.finalized
			 FROM [dbo].[mhw_app_prod] p WITH (NOLOCK) 
			 LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] s WITH (NOLOCK) on s.[supplier_id] = p.[supplier_id] 
			 WHERE $client_name_qry p.[active] = 1 AND p.[deleted] = 0  AND p.[product_desc] <> 'STAR' $filter_txt_qry
			 ORDER BY p.[create_date] desc";
		   
	
}
else if($_POST['export_view_type']=='viewproducts' && $_POST['export_qry_type']=='VPunfinalized')
{
	    $redirect_url="viewproducts.php";
	    $client_name_qry="p.[client_name] = '".addslashes($_POST['export_client'])."' AND ";
		// pagination custom searching 
		$filter_txt_qry="";
		if($_POST['export_filter_txt'] && $_POST['export_filter_txt']!='')
		{
			$filter_txt_val=$_POST['export_filter_txt'];
			$filter_txt_qry=" AND ( p.[client_code] like '%".$filter_txt_val."%'  OR p.[client_name] like '%".$filter_txt_val."%' OR p.[brand_name] like '%".$filter_txt_val."%'  OR p.[product_desc] like '%".$filter_txt_val."%' 
			OR p.[product_mhw_code] like '%".$filter_txt_val."%' OR p.[product_mhw_code_search] like '%".$filter_txt_val."%'  OR p.[TTB_ID] like '%".$filter_txt_val."%' OR p.[federal_type] like '%".$filter_txt_val."%'
			OR p.[compliance_type] like '%".$filter_txt_val."%'  OR p.[product_class] like '%".$filter_txt_val."%' OR p.[mktg_prod_type] like '%".$filter_txt_val."%'
			OR p.[bev_type] like '%".$filter_txt_val."%'  OR p.[fanciful] like '%".$filter_txt_val."%' OR p.[country] like '%".$filter_txt_val."%'
			OR p.[appellation] like '%".$filter_txt_val."%'  OR p.[lot_item] like '%".$filter_txt_val."%' OR p.[bottle_material] like '%".$filter_txt_val."%'
			OR p.[alcohol_pct] like '%".$filter_txt_val."%'  OR p.[varietal] like '%".$filter_txt_val."%' OR s.[supplier_name] like '%".$filter_txt_val."%'
			OR s.[supplier_contact] like '%".$filter_txt_val."%'  OR s.[supplier_fda_number] like '%".$filter_txt_val."%' OR s.[tax_reduction_allocation] like '%".$filter_txt_val."%'
			OR s.[supplier_address_1] like '%".$filter_txt_val."%' )";	
		}	
		
		// fetching total_num for pagination 
		
		$tsql_total_num = "SELECT count(p.[product_id]) as total_num FROM [dbo].[mhw_app_prod] p WITH (NOLOCK)
			 LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] s WITH (NOLOCK) on s.[supplier_id] = p.[supplier_id] 
			 WHERE $client_name_qry (p.[finalized] = 0 OR p.[product_id] in (select mhw_app_prod_item.product_id from mhw_app_prod_item 
			 where mhw_app_prod_item.finalized<>1 or mhw_app_prod_item.finalized is null)) AND p.[active] = 1 AND p.[deleted] = 0  AND p.[product_desc] <> 'STAR' $filter_txt_qry";
		
		
		
		$tsql = "SELECT p.[product_id],p.[client_code],p.[client_name]
			,p.[brand_name]
			,p.[product_desc]
			,p.[product_mhw_code],p.[product_mhw_code_search],p.[TTB_ID],p.[federal_type],p.[compliance_type],p.[product_class],p.[mktg_prod_type],p.[bev_type]
			,p.[fanciful]
			,p.[country]
			,p.[appellation]
			,p.[lot_item],p.[bottle_material],p.[alcohol_pct],p.[varietal],p.[create_date],s.[supplier_id]
			,s.[supplier_name]
			,s.[supplier_contact]
			,s.[supplier_fda_number],s.[tax_reduction_allocation],s.[supplier_address_1],s.[supplier_address_2],s.[supplier_address_1]
			,s.[supplier_city]
			,s.[supplier_state],s.[supplier_country]  ,s.[supplier_zip]  ,s.[supplier_phone] ,s.[supplier_email] ,s.[federal_basic_permit] ,s.[create_via] ,s.[create_date] ,s.[edit_date]
			,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_item] i WHERE i.[product_id] = p.[product_id] AND i.[active] = 1 AND i.[deleted] = 0) as 'itemcount' 
			,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount' 
			,(SELECT TOP 1 r.approvalstatus + '<br />'+ remarks FROM [dbo].[mhw_app_finalized_product_review] r WITH (NOLOCK) where r.[prod_id] = p.product_id AND r.processed = 1 AND r.approvalstatus = 'REJECTED' order by review_date desc) as 'rejremark'
			,convert(varchar, p.create_date, 120) as product_creation_date
            ,(select top 1 username from mhw_app_workflow where record_id=p.product_id and workflow_type in ('product','product_import') 
order by workflow_id asc) as product_created_by
            ,p.finalized
			 FROM [dbo].[mhw_app_prod] p WITH (NOLOCK)
			 LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] s WITH (NOLOCK) on s.[supplier_id] = p.[supplier_id] 
			 WHERE $client_name_qry (p.[finalized] = 0 OR p.[product_id] in (select mhw_app_prod_item.product_id from mhw_app_prod_item 
			 where mhw_app_prod_item.finalized<>1 or mhw_app_prod_item.finalized is null)) AND p.[active] = 1 AND p.[deleted] = 0  AND p.[product_desc] <> 'STAR' $filter_txt_qry
			 ORDER BY p.[create_date] desc";
		   
	
}	
else if($_POST['export_view_type']=='viewproducts' && $_POST['export_qry_type']=='VPpending')
{
	    $redirect_url="viewproducts.php";
	    $client_name_qry="p.[client_name] = '".addslashes($_POST['export_client'])."' AND ";
		// pagination custom searching 
		$filter_txt_qry="";
		if($_POST['export_filter_txt'] && $_POST['export_filter_txt']!='')
		{
			$filter_txt_val=$_POST['export_filter_txt'];
			$filter_txt_qry=" AND ( p.[client_code] like '%".$filter_txt_val."%'  OR p.[client_name] like '%".$filter_txt_val."%' OR p.[brand_name] like '%".$filter_txt_val."%'  OR p.[product_desc] like '%".$filter_txt_val."%' 
			OR p.[product_mhw_code] like '%".$filter_txt_val."%' OR p.[product_mhw_code_search] like '%".$filter_txt_val."%'  OR p.[TTB_ID] like '%".$filter_txt_val."%' OR p.[federal_type] like '%".$filter_txt_val."%'
			OR p.[compliance_type] like '%".$filter_txt_val."%'  OR p.[product_class] like '%".$filter_txt_val."%' OR p.[mktg_prod_type] like '%".$filter_txt_val."%'
			OR p.[bev_type] like '%".$filter_txt_val."%'  OR p.[fanciful] like '%".$filter_txt_val."%' OR p.[country] like '%".$filter_txt_val."%'
			OR p.[appellation] like '%".$filter_txt_val."%'  OR p.[lot_item] like '%".$filter_txt_val."%' OR p.[bottle_material] like '%".$filter_txt_val."%'
			OR p.[alcohol_pct] like '%".$filter_txt_val."%'  OR p.[varietal] like '%".$filter_txt_val."%' OR s.[supplier_name] like '%".$filter_txt_val."%'
			OR s.[supplier_contact] like '%".$filter_txt_val."%'  OR s.[supplier_fda_number] like '%".$filter_txt_val."%' OR s.[tax_reduction_allocation] like '%".$filter_txt_val."%'
			OR s.[supplier_address_1] like '%".$filter_txt_val."%' )";	
		}	
		
		// fetching total_num for pagination 
		
		$tsql_total_num = "SELECT count(p.[product_id]) as total_num FROM [dbo].[mhw_app_prod] p WITH (NOLOCK) 
			 LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] s WITH (NOLOCK) on s.[supplier_id] = p.[supplier_id] 
			 WHERE $client_name_qry p.[finalized] = 1 AND p.[processed] = 0 AND p.[active] = 1 AND p.[deleted] = 0  AND p.[product_desc] <> 'STAR' $filter_txt_qry";
		
		
		
		$tsql = "SELECT p.[product_id],p.[client_code],p.[client_name]
			,p.[brand_name]
			,p.[product_desc]
			,p.[product_mhw_code],p.[product_mhw_code_search],p.[TTB_ID],p.[federal_type],p.[compliance_type],p.[product_class],p.[mktg_prod_type],p.[bev_type]
			,p.[fanciful]
			,p.[country]
			,p.[appellation]
			,p.[lot_item],p.[bottle_material],p.[alcohol_pct],p.[varietal],p.[create_date],s.[supplier_id]
			,s.[supplier_name]
			,s.[supplier_contact],s.[supplier_fda_number],s.[tax_reduction_allocation]
			,s.[supplier_address_1]
			,s.[supplier_address_2]
			,s.[supplier_address_3]
			,s.[supplier_city]
			,s.[supplier_state],s.[supplier_country]  ,s.[supplier_zip]  ,s.[supplier_phone] ,s.[supplier_email] ,s.[federal_basic_permit] ,s.[create_via] ,s.[create_date] ,s.[edit_date] 
			,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_item] i WHERE i.[product_id] = p.[product_id] AND i.[active] = 1 AND i.[deleted] = 0) as 'itemcount' 
			,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount' 
			,convert(varchar, p.create_date, 120) as product_creation_date
            ,(select top 1 username from mhw_app_workflow where record_id=p.product_id and workflow_type in ('product','product_import') 
order by workflow_id asc) as product_created_by
             ,p.finalized
			 FROM [dbo].[mhw_app_prod] p WITH (NOLOCK) 
			 LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] s WITH (NOLOCK) on s.[supplier_id] = p.[supplier_id] 
			 WHERE $client_name_qry p.[finalized] = 1 AND p.[processed] = 0 AND p.[active] = 1 AND p.[deleted] = 0  AND p.[product_desc] <> 'STAR'  $filter_txt_qry
			 ORDER BY p.[create_date] desc";
		   
	
}
else if($_POST['export_view_type']=='viewproducts' && $_POST['export_qry_type']=='VPfinalized')
{
	    $redirect_url="viewproducts.php";
	    $client_name_qry="p.[client_name] = '".addslashes($_POST['export_client'])."' AND ";
		// pagination custom searching 
		$filter_txt_qry="";
		if($_POST['export_filter_txt'] && $_POST['export_filter_txt']!='')
		{
			$filter_txt_val=$_POST['export_filter_txt'];
			$filter_txt_qry=" AND ( p.[client_code] like '%".$filter_txt_val."%'  OR p.[client_name] like '%".$filter_txt_val."%' OR p.[brand_name] like '%".$filter_txt_val."%'  OR p.[product_desc] like '%".$filter_txt_val."%' 
			OR p.[product_mhw_code] like '%".$filter_txt_val."%' OR p.[product_mhw_code_search] like '%".$filter_txt_val."%'  OR p.[TTB_ID] like '%".$filter_txt_val."%' OR p.[federal_type] like '%".$filter_txt_val."%'
			OR p.[compliance_type] like '%".$filter_txt_val."%'  OR p.[product_class] like '%".$filter_txt_val."%' OR p.[mktg_prod_type] like '%".$filter_txt_val."%'
			OR p.[bev_type] like '%".$filter_txt_val."%'  OR p.[fanciful] like '%".$filter_txt_val."%' OR p.[country] like '%".$filter_txt_val."%'
			OR p.[appellation] like '%".$filter_txt_val."%'  OR p.[lot_item] like '%".$filter_txt_val."%' OR p.[bottle_material] like '%".$filter_txt_val."%'
			OR p.[alcohol_pct] like '%".$filter_txt_val."%'  OR p.[varietal] like '%".$filter_txt_val."%' OR s.[supplier_name] like '%".$filter_txt_val."%'
			OR s.[supplier_contact] like '%".$filter_txt_val."%'  OR s.[supplier_fda_number] like '%".$filter_txt_val."%' OR s.[tax_reduction_allocation] like '%".$filter_txt_val."%'
			OR s.[supplier_address_1] like '%".$filter_txt_val."%' )";	
		}	
		
		// fetching total_num for pagination 
		
		$tsql_total_num = "SELECT count(p.[product_id]) as total_num FROM [dbo].[mhw_app_prod] p WITH (NOLOCK) 
			 LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] s WITH (NOLOCK) on s.[supplier_id] = p.[supplier_id] 
			 WHERE $client_name_qry p.[finalized] = 1 AND p.[processed] = 1 AND p.[active] = 1 AND p.[deleted] = 0  AND p.[product_desc] <> 'STAR' $filter_txt_qry";
		
		
		
		$tsql = "SELECT p.[product_id],p.[client_code],p.[client_name]
			,p.[brand_name]
			,p.[product_desc]
			,p.[product_mhw_code],p.[product_mhw_code_search],p.[TTB_ID],p.[federal_type],p.[compliance_type],p.[product_class],p.[mktg_prod_type],p.[bev_type]
			,p.[fanciful]
			,p.[country]
			,p.[appellation]
			,p.[lot_item],p.[bottle_material],p.[alcohol_pct],p.[varietal],p.[create_date],s.[supplier_id]
			,s.[supplier_name]
			,s.[supplier_contact],s.[supplier_fda_number],s.[tax_reduction_allocation],s.[supplier_address_1],s.[supplier_address_2],s.[supplier_address_3]
			,s.[supplier_city]
			,s.[supplier_state],s.[supplier_country]  ,s.[supplier_zip]  ,s.[supplier_phone] ,s.[supplier_email] ,s.[federal_basic_permit] ,s.[create_via] ,s.[create_date] ,s.[edit_date] 
			,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_item] i WHERE i.[product_id] = p.[product_id] AND i.[active] = 1 AND i.[deleted] = 0) as 'itemcount' 
			,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount' 
			,(SELECT TOP 1 r.approvalstatus + '<br />'+ remarks FROM [dbo].[mhw_app_finalized_product_review] r WITH (NOLOCK) where r.[prod_id] = p.product_id AND r.processed = 1 AND r.approvalstatus = 'APPROVED' order by review_date desc) as 'rejremark'
			,convert(varchar, p.create_date, 120) as product_creation_date
            ,(select top 1 username from mhw_app_workflow where record_id=p.product_id and workflow_type in ('product','product_import') 
order by workflow_id asc) as product_created_by
            ,p.finalized
			 FROM [dbo].[mhw_app_prod] p WITH (NOLOCK) 
			 LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] s WITH (NOLOCK) on s.[supplier_id] = p.[supplier_id] 
			 WHERE $client_name_qry p.[finalized] = 1 AND p.[processed] = 1 AND p.[active] = 1 AND p.[deleted] = 0  AND p.[product_desc] <> 'STAR' $filter_txt_qry
			 ORDER BY p.[create_date] desc ";
		   
	
}
else if ($_POST['export_qry_type']=='blr-status' && $_POST['export_view_type']!='') {

	if (empty($_POST['export_view_type']) || empty($_POST['export_client']) ) {
		echo("Wrong parameters !"); exit;
	}
	
    if ($_POST['export_view_type'] === 'Federal product approvals') {
         
		$redirect_url="prod_comp_status_reports.php"; 
		$client_name_qry=" AND p.[client_name] = '".addslashes($_POST['export_client'])."'";	
		$prod_ids_qry_part="";
		if($_POST['export_prodlistids']!==''&& $_POST['export_prodlistids']!=='[]')
		{
			$prodids =  str_replace("]","",str_replace("[","",$_POST['export_prodlistids']));
			$prod_ids_qry_part=" AND p.[product_id] IN (".$prodids.") ";			
		}
		
		$advance_searching_qry_part="";
		if($_POST['export_searching_data']!==''&& $_POST['export_searching_data']!=='{}')
		{
			$searching_data_val =  json_decode($_POST['export_searching_data']);
		    foreach($searching_data_val as $key=>$val)
			{
				if(trim($val)!='' && (substr($key, 0, 2)=='p.' || substr($key, 0, 2)=='f.'))
				{					
						$advance_searching_qry_part=$advance_searching_qry_part. " AND ". $key ." like '%".$val."%'";
				}
			}
			
			if(isset($searching_data_val->all_clients) && $searching_data_val->all_clients=='all_clients')
				{
					if($advance_searching_qry_part)
					{
					$client_name_qry="";
                    }					
				}			
		}
		
		// pagination custom searching 
		$filter_txt_qry="";
		if($_POST['export_filter_txt'] && $_POST['export_filter_txt']!='')
		{
			$filter_txt_val=$_POST['export_filter_txt'];
			$filter_txt_qry=" AND ( p.[product_id] like '%".$filter_txt_val."%'  OR p.[TTB_ID] like '%".$filter_txt_val."%' OR p.[product_desc] like '%".$filter_txt_val."%'
OR p.[brand_name] like '%".$filter_txt_val."%'  OR p.[product_mhw_code] like '%".$filter_txt_val."%' OR p.[federal_type] like '%".$filter_txt_val."%'
OR p.[client_code] like '%".$filter_txt_val."%'  OR f.[status] like '%".$filter_txt_val."%' OR p.[product_class] like '%".$filter_txt_val."%'
OR f.[primaryfederalbasicpermitnumber] like '%".$filter_txt_val."%'  OR p.[country] like '%".$filter_txt_val."%' OR f.[legalnameusedonlabel] like '%".$filter_txt_val."%'
OR f.[applicantname] like '%".$filter_txt_val."%')";
		}
		
$tsql_total_num = "select count(*) as total_num from (
		SELECT DISTINCT
				p.[product_id], p.[product_desc],
				p.[brand_name], p.[product_mhw_code],
				p.[TTB_ID], p.[federal_type], p.[client_code],
				f.[status] as federal_status,f.[status] as status,
				convert(varchar, f.[dateapproved], 120) as dateapproved,p.[product_class],f.[primaryfederalbasicpermitnumber],
				p.[country],f.[legalnameusedonlabel],f.[applicantname],convert(varchar, f.[datecompleted], 120) as datecompleted
			FROM  [dbo].[mhw_app_prod] p
			LEFT JOIN [dbo].[sc_cola_federal_sts] f
				ON f.[cola] = p.[TTB_ID]
				AND f.[client] = p.[client_code]
			WHERE p.[product_id] IS NOT NULL  
				AND p.active = 1  
			   	  $client_name_qry     
				AND f.[status] is not NULL and f.[status]!=''
				$prod_ids_qry_part $advance_searching_qry_part $filter_txt_qry
		) as srctbl";		
		
		/* REPORT FED QUERY - ONLY DISPLAY PRODUCTS WITH APPROVALS */
         
		$tsql = "SELECT DISTINCT
				p.[product_id],CASE
    WHEN ISNUMERIC(SUBSTRING(p.[TTB_ID],1,14))=1 AND len(SUBSTRING(p.[TTB_ID],1,14))=14 THEN SUBSTRING(p.[TTB_ID],1,14)
    ELSE ''
END AS prefix_ttb_id, p.[product_desc],
				p.[brand_name], p.[product_mhw_code],
				p.[TTB_ID], p.[federal_type], p.[client_code],
				f.[status] as federal_status,f.[status] as status,
				convert(varchar, f.[dateapproved], 120) as dateapproved,p.[product_class],f.[primaryfederalbasicpermitnumber],
				p.[country],f.[legalnameusedonlabel],f.[applicantname],convert(varchar, f.[datecompleted], 120) as datecompleted
				,'' as est_dt_approval,'' as user_notes,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount'
			FROM  [dbo].[mhw_app_prod] p
			LEFT JOIN [dbo].[sc_cola_federal_sts] f
				ON f.[cola] = p.[TTB_ID]
				AND f.[client] = p.[client_code]
			WHERE p.[product_id] IS NOT NULL  
				AND p.active = 1  
			   	  $client_name_qry     
				AND f.[status] is not NULL and f.[status]!=''
				$prod_ids_qry_part $advance_searching_qry_part $filter_txt_qry
			ORDER BY p.[product_desc] ASC
		";
	//	echo $tsql; exit;
	}
	else if ($_POST['export_view_type'] === 'State product approvals') {
		
		 $redirect_url="prod_comp_status_reports.php"; 
		 $client_name_qry=" AND p.[client_name] = '".addslashes($_POST['export_client'])."'";
		 $prod_ids_qry_part="";
		if($_POST['export_prodlistids']!==''&& $_POST['export_prodlistids']!=='[]')
		{
			$prodids =  str_replace("]","",str_replace("[","",$_POST['export_prodlistids']));
			$prod_ids_qry_part=" AND p.[product_id] IN (".$prodids.") ";			
		}
		
		$advance_searching_qry_part="";
		if($_POST['export_searching_data']!==''&& $_POST['export_searching_data']!=='{}')
		{
			$searching_data_val =  json_decode($_POST['export_searching_data']);
		    foreach($searching_data_val as $key=>$val)
			{ 
				if(trim($val)!='' && (substr($key, 0, 2)=='p.' || substr($key, 0, 2)=='s.'))
				{					
						$advance_searching_qry_part=$advance_searching_qry_part. " AND ". $key ." like '%".$val."%'";
				}
			}
						
			if(isset($searching_data_val->all_clients) && $searching_data_val->all_clients=='all_clients')
				{
					if($advance_searching_qry_part)
					{
						$client_name_qry="";	
                    }						
				}
		}
		
		
	// pagination custom searching 
		$filter_txt_qry="";
		if($_POST['export_filter_txt'] && $_POST['export_filter_txt']!='')
		{
			$filter_txt_val=$_POST['export_filter_txt'];
			$filter_txt_qry=" AND ( p.[product_id] like '%".$filter_txt_val."%'  OR p.[TTB_ID] like '%".$filter_txt_val."%' OR p.[product_desc] like '%".$filter_txt_val."%'
OR p.[brand_name] like '%".$filter_txt_val."%'  OR p.[product_mhw_code] like '%".$filter_txt_val."%' OR p.[federal_type] like '%".$filter_txt_val."%'
OR p.[client_code] like '%".$filter_txt_val."%'  OR s.[StateApprovalNumber] like '%".$filter_txt_val."%' OR s.[Status] like '%".$filter_txt_val."%'
OR s.[State] like '%".$filter_txt_val."%' OR s.[Preparer] like '%".$filter_txt_val."%')";
		}
		
$tsql_total_num = "select count(*) as total_num from (
		SELECT DISTINCT
						p.[product_id], p.[product_desc],
						p.[brand_name], p.[product_mhw_code],
						p.[TTB_ID], p.[federal_type], p.[client_code],
						s.StateApprovalNumber as StateApprovalNumber,s.Status as Status,s.EffectiveDate as EffectiveDate,s.State as State,s.Preparer as Preparer
					FROM  [dbo].[mhw_app_prod] p
					left outer join [__fact_state_reg_status] s 
				   on s.[ProductKey] = p.[product_mhw_code] and s.client=p.client_code			
					WHERE p.[product_id] IS NOT NULL
					AND p.active = 1
						  $client_name_qry  
					$prod_ids_qry_part  $advance_searching_qry_part $filter_txt_qry
		) as srctbl";			

		/* REPORT STATE QUERY - ONLY DISPLAY PRODUCTS WITH STATE REGISTRATIONS */

		
		$tsql = "SELECT DISTINCT
				p.[product_id], p.[product_desc],CASE
    WHEN ISNUMERIC(SUBSTRING(p.[TTB_ID],1,14))=1 AND len(SUBSTRING(p.[TTB_ID],1,14))=14 THEN SUBSTRING(p.[TTB_ID],1,14)
    ELSE ''
END AS prefix_ttb_id,
				p.[brand_name], p.[product_mhw_code],
				p.[TTB_ID], p.[federal_type], p.[client_code],
				s.StateApprovalNumber as StateApprovalNumber,s.Status as Status,s.EffectiveDate as EffectiveDate,s.State as State,s.Preparer as Preparer,'' as est_dt_approval,'' as mhw_user_notes,
				(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount'
			FROM  [dbo].[mhw_app_prod] p
            left outer join [__fact_state_reg_status] s 
           on s.[ProductKey] = p.[product_mhw_code] and s.client=p.client_code			
            WHERE p.[product_id] IS NOT NULL
			AND p.active = 1
				  $client_name_qry  
			$prod_ids_qry_part  $advance_searching_qry_part $filter_txt_qry
			ORDER BY p.[product_desc] ASC";
			
			// echo $tsql;exit;
	}
    else if ($_POST['export_view_type'] === 'Federal product compliance status') {
		
		$redirect_url="prod_comp_status.php"; 
		$client_name_qry=" AND p.[client_name] = '".addslashes($_POST['export_client'])."'";
		
		$prod_ids_qry_part="";
		if($_POST['export_prodlistids']!==''&& $_POST['export_prodlistids']!=='[]')
		{
			$prodids =  str_replace("]","",str_replace("[","",$_POST['export_prodlistids']));
			$prod_ids_qry_part=" AND p.[product_id] IN (".$prodids.") ";			
		}
		
		$advance_searching_qry_part="";
		if($_POST['export_searching_data']!==''&& $_POST['export_searching_data']!=='{}')
		{
			$searching_data_val =  json_decode($_POST['export_searching_data']);
		    foreach($searching_data_val as $key=>$val)
			{
				if(trim($val)!='' && (substr($key, 0, 2)=='p.' || substr($key, 0, 2)=='f.'))
				{					
						$advance_searching_qry_part=$advance_searching_qry_part. " AND ". $key ." like '%".$val."%'";
				}
			}
			
			if(isset($searching_data_val->all_clients) && $searching_data_val->all_clients=='all_clients')
				{
					if($advance_searching_qry_part)
					{
					$client_name_qry="";
                    }					
				}
				
		}
		
		// pagination custom searching 
		$filter_txt_qry="";
		if($_POST['export_filter_txt'] && $_POST['export_filter_txt']!='')
		{
			$filter_txt_val=$_POST['export_filter_txt'];
			$filter_txt_qry=" AND ( p.[product_id] like '%".$filter_txt_val."%'  OR p.[TTB_ID] like '%".$filter_txt_val."%' OR p.[product_desc] like '%".$filter_txt_val."%'
OR p.[brand_name] like '%".$filter_txt_val."%'  OR p.[product_mhw_code] like '%".$filter_txt_val."%' OR p.[federal_type] like '%".$filter_txt_val."%'
OR p.[client_code] like '%".$filter_txt_val."%'  OR f.[status] like '%".$filter_txt_val."%' OR p.[product_class] like '%".$filter_txt_val."%'
OR f.[primaryfederalbasicpermitnumber] like '%".$filter_txt_val."%'  OR p.[country] like '%".$filter_txt_val."%' OR f.[legalnameusedonlabel] like '%".$filter_txt_val."%'
OR f.[applicantname] like '%".$filter_txt_val."%')";
		}
		
		
$tsql_total_num = "select count(*) as total_num from (
		SELECT DISTINCT
				p.[product_id], p.[product_desc],
				p.[brand_name], p.[product_mhw_code],
				p.[TTB_ID], p.[federal_type], p.[client_code],
				f.[status] as federal_status,f.[status] as status,
				convert(varchar, f.[dateapproved], 120) as dateapproved,p.[product_class],f.[primaryfederalbasicpermitnumber],
				p.[country],f.[legalnameusedonlabel],f.[applicantname],convert(varchar, f.[datecompleted], 120) as datecompleted FROM  [dbo].[mhw_app_prod] p
			LEFT JOIN [dbo].[sc_cola_federal_sts] f
				ON f.[cola] = p.[TTB_ID]
				AND f.[client] = p.[client_code]
				AND f.[status] is not NULL and f.[status]!=''
			WHERE p.[product_id] IS NOT NULL  
				AND p.active = 1
				  $client_name_qry  
				$prod_ids_qry_part $advance_searching_qry_part $filter_txt_qry
		) as srctbl";			

		
		/* BLR STATUS FEDERAL QUERY - DISPLAY EACH PRODUCT WITH OR WITHOUT APPROVAL */
		
		$tsql = "SELECT DISTINCT
				p.[product_id],CASE
    WHEN ISNUMERIC(SUBSTRING(p.[TTB_ID],1,14))=1 AND len(SUBSTRING(p.[TTB_ID],1,14))=14 THEN SUBSTRING(p.[TTB_ID],1,14)
    ELSE ''
END AS prefix_ttb_id, p.[product_desc],
				p.[brand_name], p.[product_mhw_code],
				p.[TTB_ID], p.[federal_type], p.[client_code],
				f.[status] as federal_status,f.[status] as status,
				convert(varchar, f.[dateapproved], 120) as dateapproved,p.[product_class],f.[primaryfederalbasicpermitnumber],
				p.[country],f.[legalnameusedonlabel],f.[applicantname],convert(varchar, f.[datecompleted], 120) as datecompleted
				,'' as est_dt_approval,'' as user_notes,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount'
			FROM  [dbo].[mhw_app_prod] p
			LEFT JOIN [dbo].[sc_cola_federal_sts] f
				ON f.[cola] = p.[TTB_ID]
				AND f.[client] = p.[client_code]
				AND f.[status] is not NULL and f.[status]!=''
			WHERE p.[product_id] IS NOT NULL  
				AND p.active = 1
				  $client_name_qry  
				$prod_ids_qry_part $advance_searching_qry_part $filter_txt_qry
			ORDER BY p.[product_desc] ASC
		";
		// echo $tsql; exit;
	}
	else if ($_POST['export_view_type'] === 'State product compliance status') {
		
		 $redirect_url="prod_comp_status.php"; 
		 $client_name_qry=" AND p.[client_name] = '".addslashes($_POST['export_client'])."'";
		 $prod_ids_qry_part="";
		if($_POST['export_prodlistids']!==''&& $_POST['export_prodlistids']!=='[]')
		{
			$prodids =  str_replace("]","",str_replace("[","",$_POST['export_prodlistids']));
			$prod_ids_qry_part=" AND p.[product_id] IN (".$prodids.") ";			
		}
		
		$advance_searching_qry_part="";
		if($_POST['export_searching_data']!==''&& $_POST['export_searching_data']!=='{}')
		{
			$searching_data_val =  json_decode($_POST['export_searching_data']);
		    foreach($searching_data_val as $key=>$val)
			{ 
				if(trim($val)!='' && (substr($key, 0, 2)=='p.' || substr($key, 0, 2)=='s.' || substr($key, 0, 2)=='r.'))
				{					
						$advance_searching_qry_part=$advance_searching_qry_part. " AND ". $key ." like '%".$val."%'";
				}
			}
			
			if(isset($searching_data_val->all_clients) && $searching_data_val->all_clients=='all_clients')
				{
					if($advance_searching_qry_part)
					{
						$client_name_qry="";	
                    }						
				}
			
		}
	
	// pagination custom searching 
		$filter_txt_qry="";
		if($_POST['export_filter_txt'] && $_POST['export_filter_txt']!='')
		{
			$filter_txt_val=$_POST['export_filter_txt'];
			$filter_txt_qry=" AND ( p.[product_id] like '%".$filter_txt_val."%'  OR p.[TTB_ID] like '%".$filter_txt_val."%' OR p.[product_desc] like '%".$filter_txt_val."%'
OR p.[brand_name] like '%".$filter_txt_val."%'  OR p.[product_mhw_code] like '%".$filter_txt_val."%' OR p.[federal_type] like '%".$filter_txt_val."%'
OR p.[client_code] like '%".$filter_txt_val."%'  OR s.[StateApprovalNumber] like '%".$filter_txt_val."%' OR s.[Status] like '%".$filter_txt_val."%'
OR s.[State] like '%".$filter_txt_val."%' OR s.[Preparer] like '%".$filter_txt_val."%')";
		}
		
$tsql_total_num = "select count(*) as total_num from (
		SELECT DISTINCT
				p.[product_id],
				p.[brand_name], p.[product_mhw_code],
				p.[TTB_ID], p.[federal_type], p.[client_code],
				s.StateApprovalNumber as StateApprovalNumber,s.Status as Status,s.EffectiveDate as EffectiveDate,r.state_name as State,s.Preparer as Preparer
			FROM  [dbo].[mhw_app_prod] p
			cross join [dbo].[__ref_states] r with (nolock)
            left outer join [__fact_state_reg_status] s 
           on s.[ProductKey] = p.[product_mhw_code] and s.client=p.client_code and s.State = r.state_name 			
            WHERE p.[product_id] IS NOT NULL
			AND p.active = 1
				  $client_name_qry  
			$prod_ids_qry_part  $advance_searching_qry_part $filter_txt_qry
		) as srctbl";			

		//echo $tsql_total_num."<br><br><br>";
		/* BLR STATUS STATE QUERY - DISPLAY EACH PRODUCT AND EACH STATE WITH OR WITHOUT REGISTRATION */
		
		$tsql = "SELECT DISTINCT
				p.[product_id], p.[product_desc],CASE
    WHEN ISNUMERIC(SUBSTRING(p.[TTB_ID],1,14))=1 AND len(SUBSTRING(p.[TTB_ID],1,14))=14 THEN SUBSTRING(p.[TTB_ID],1,14)
    ELSE ''
END AS prefix_ttb_id,
				p.[brand_name], p.[product_mhw_code],
				p.[TTB_ID], p.[federal_type], p.[client_code],
				s.StateApprovalNumber as StateApprovalNumber,s.Status as Status,s.EffectiveDate as EffectiveDate,r.state_name as State,s.Preparer as Preparer,'' as est_dt_approval,'' as mhw_user_notes,
				(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount'
			FROM  [dbo].[mhw_app_prod] p
			cross join [dbo].[__ref_states] r with (nolock)
            left outer join [__fact_state_reg_status] s 
           on s.[ProductKey] = p.[product_mhw_code] and s.client=p.client_code and s.State = r.state_name 			
            WHERE p.[product_id] IS NOT NULL
			AND p.active = 1
				  $client_name_qry  
			$prod_ids_qry_part  $advance_searching_qry_part $filter_txt_qry
			ORDER BY p.[product_desc]
			";	
			//echo $tsql;exit;
			//ORDER BY p.[product_desc],r.state_name ASC
	}	
	else {
		echo("Wrong parameters !"); exit;
	}

}




/* ********************* added by Jobaidur :End ************************ */

//echo "<pre>"; print_r($_POST); echo "</pre>";

if($tsql_total_num!="")
	{
		$getResults_totalNum = sqlsrv_query($conn, $tsql_total_num);

		if ($getResults_totalNum == FALSE)
			echo (sqlsrv_errors());

		while ($row_totalNum = sqlsrv_fetch_array($getResults_totalNum, SQLSRV_FETCH_ASSOC)) {
			$pagination_total_num=$row_totalNum['total_num'];
		}
	}
	
if($pagination_total_num>35000)
{
	?>
<script type="text/javascript">
alert("Error Exporting, dataset too large, please try filtering data with Advanced Search or use pre-filtered Reports screen");
window.location = "<?php echo $redirect_url;?>";
</script>
<?php 
echo "Error Exporting, dataset too large, please try filtering data with Advanced Search or use pre-filtered Reports screen";
exit;
}


/* Processing query results */

/* federal product approvals : data export */

if(($_POST['export_view_type'] === 'Federal product approvals') && $_POST['export_qry_type']=='blr-status')
{
$file_name=$_POST['export_view_type'].".xlsx";	
$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("jahid0604015@gmail.com")
							 ->setTitle($file_name)
							 ->setSubject($file_name);

$row = 1;
$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Product Code')
                              ->setCellValue('B'.$row, 'Brand Name')
							  ->setCellValue('C'.$row, 'Product Description')
							  ->setCellValue('D'.$row, 'Federal Type')
							  ->setCellValue('E'.$row, 'Product Class')
							  ->setCellValue('F'.$row, 'TTB ID')
							  ->setCellValue('G'.$row, 'Status')
							  ->setCellValue('H'.$row, 'Date of Application')
							  ->setCellValue('I'.$row, 'Estimated Date of Approval')
							  ->setCellValue('J'.$row, 'Date Approved')
							  ->setCellValue('K'.$row, 'TTB Basic Importer #')
							  ->setCellValue('L'.$row, 'Country of Origin')
							  ->setCellValue('M'.$row, 'TTB Basic Importer Name')
							  ->setCellValue('N'.$row, 'MHW User that filed')
							  ->setCellValue('O'.$row, 'MHW Team')
							  ->setCellValue('P'.$row, 'CompID');
							  							  
$row++;

$sheet_one_num=60000;
$sheet_two_num=0;
if($pagination_total_num<=60000)
{
$sheet_one_num=$pagination_total_num;
$sheet_two_num=0;
}
else if($pagination_total_num>60000)
{
$sheet_two_num=$pagination_total_num;
}

for($total_rows=0;$total_rows<$sheet_one_num;$total_rows=$total_rows+5000)
{
	$max_limit=5000;
	$pagination_qry="OFFSET $total_rows ROWS FETCH NEXT $max_limit ROWS ONLY";
	$tsql_temp=$tsql." ".$pagination_qry;
	//echo "<br><hr>".$tsql_temp."<hr><br>"; 
	$getResults= sqlsrv_query($conn, $tsql_temp);
    if ($getResults == FALSE)
	echo (sqlsrv_errors());

    do {
     while ($each_val = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
		 
     $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $each_val['product_mhw_code'])
                                  ->setCellValue('B'.$row, $each_val['brand_name'])
								  ->setCellValue('C'.$row, $each_val['product_desc'])
								  ->setCellValue('D'.$row, $each_val['federal_type'])
								  ->setCellValue('E'.$row, $each_val['product_class'])
								  ->setCellValueExplicit('F'.$row,(string)$each_val['TTB_ID'],PHPExcel_Cell_DataType::TYPE_STRING)
								  ->setCellValue('G'.$row, $each_val['status'])
								  ->setCellValue('H'.$row, '')
								  ->setCellValue('I'.$row, $each_val['est_dt_approval'])
								  ->setCellValue('J'.$row, $each_val['dateapproved'])
								  ->setCellValue('K'.$row, $each_val['primaryfederalbasicpermitnumber'])
								  ->setCellValue('L'.$row, $each_val['country'])
								  ->setCellValue('M'.$row, $each_val['legalnameusedonlabel'])
								  ->setCellValue('N'.$row, $each_val['applicantname'])
								  ->setCellValue('O'.$row, '')
								  ->setCellValue('P'.$row, '');
								  
				
    $row++;
     }
} while ( sqlsrv_next_result($getResults) );

sqlsrv_free_stmt( $getResults);

}

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setTitle('Sheet One');	

/* second sheet : start */

if($sheet_two_num>0)
{
	// Create a new worksheet, after the default sheet
     $objPHPExcel->createSheet();
     $objPHPExcel->setActiveSheetIndex(1);
	 
	 $row = 1;
$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Product Code')
                              ->setCellValue('B'.$row, 'Brand Name')
							  ->setCellValue('C'.$row, 'Product Description')
							  ->setCellValue('D'.$row, 'Federal Type')
							  ->setCellValue('E'.$row, 'Product Class')
							  ->setCellValue('F'.$row, 'TTB ID')
							  ->setCellValue('G'.$row, 'Status')
							  ->setCellValue('H'.$row, 'Date of Application')
							  ->setCellValue('I'.$row, 'Estimated Date of Approval')
							  ->setCellValue('J'.$row, 'Date Approved')
							  ->setCellValue('K'.$row, 'TTB Basic Importer #')
							  ->setCellValue('L'.$row, 'Country of Origin')
							  ->setCellValue('M'.$row, 'TTB Basic Importer Name')
							  ->setCellValue('N'.$row, 'MHW User that filed')
							  ->setCellValue('O'.$row, 'MHW Team')
							  ->setCellValue('P'.$row, 'CompID');
							  							  
$row++;

	 
	 for($total_rows2=$total_rows;$total_rows2<$sheet_two_num;$total_rows2=$total_rows2+5000)
{
	$max_limit=5000;
	$pagination_qry="OFFSET $total_rows2 ROWS FETCH NEXT $max_limit ROWS ONLY";
	$tsql_temp=$tsql." ".$pagination_qry;
	//echo "<br><hr>".$tsql_temp."<hr><br>"; 
	$getResults= sqlsrv_query($conn, $tsql_temp);
    if ($getResults == FALSE)
	echo (sqlsrv_errors());

    do {
     while ($each_val = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
		 
     $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $each_val['product_mhw_code'])
                                  ->setCellValue('B'.$row, $each_val['brand_name'])
								  ->setCellValue('C'.$row, $each_val['product_desc'])
								  ->setCellValue('D'.$row, $each_val['federal_type'])
								  ->setCellValue('E'.$row, $each_val['product_class'])
								  ->setCellValueExplicit('F'.$row,(string)$each_val['TTB_ID'],PHPExcel_Cell_DataType::TYPE_STRING)
								  ->setCellValue('G'.$row, $each_val['status'])
								  ->setCellValue('H'.$row, '')
								  ->setCellValue('I'.$row, $each_val['est_dt_approval'])
								  ->setCellValue('J'.$row, $each_val['dateapproved'])
								  ->setCellValue('K'.$row, $each_val['primaryfederalbasicpermitnumber'])
								  ->setCellValue('L'.$row, $each_val['country'])
								  ->setCellValue('M'.$row, $each_val['legalnameusedonlabel'])
								  ->setCellValue('N'.$row, $each_val['applicantname'])
								  ->setCellValue('O'.$row, '')
								  ->setCellValue('P'.$row, '');
								  
				
    $row++;
     }
} while ( sqlsrv_next_result($getResults) );

sqlsrv_free_stmt( $getResults);

}

	
}

$objPHPExcel->getActiveSheet()->setTitle('Sheet Two');

$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$file_name.'"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
}
else if(($_POST['export_view_type'] === 'State product approvals') && $_POST['export_qry_type']=='blr-status')
{	
 /* state product approvals : data export */
$file_name=$_POST['export_view_type'].".xlsx";	
$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("jahid0604015@gmail.com")
							 ->setTitle($file_name)
							 ->setSubject($file_name);

$row = 1;
$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Product Code')
                              ->setCellValue('B'.$row, 'Brand Name')
							  ->setCellValue('C'.$row, 'Product Description')
							  ->setCellValue('D'.$row, 'Federal Type')
							  ->setCellValue('E'.$row, 'State')
							  ->setCellValue('F'.$row, 'Status')
							  ->setCellValue('G'.$row, 'Submitted Date')
							  ->setCellValue('H'.$row, 'Effective Date')
							  ->setCellValue('I'.$row, 'Expiration Date')
							  ->setCellValue('J'.$row, 'Estimated Date of Approval')
							  ->setCellValue('K'.$row, 'Brand label registration (BLR) #')
							  ->setCellValue('L'.$row, 'MHW user that filed')
							  ->setCellValue('M'.$row, 'MHW Team')
							  ->setCellValue('N'.$row, 'CompID');
							  							  
$row++;

$sheet_one_num=60000;
$sheet_two_num=0;
if($pagination_total_num<=60000)
{
$sheet_one_num=$pagination_total_num;
$sheet_two_num=0;
}
else if($pagination_total_num>60000)
{
$sheet_two_num=$pagination_total_num;
}

for($total_rows=0;$total_rows<$sheet_one_num;$total_rows=$total_rows+5000)
{
	$max_limit=5000;
	$pagination_qry="OFFSET $total_rows ROWS FETCH NEXT $max_limit ROWS ONLY";
	$tsql_temp=$tsql." ".$pagination_qry;
	//echo "<br><hr>".$tsql_temp."<hr><br>"; 
	$getResults= sqlsrv_query($conn, $tsql_temp);
    if ($getResults == FALSE)
	echo (sqlsrv_errors());

    do {
     while ($each_val = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
		 
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $each_val['product_mhw_code'])
                                  ->setCellValue('B'.$row, $each_val['brand_name'])
								  ->setCellValue('C'.$row, $each_val['product_desc'])
								  ->setCellValue('D'.$row, $each_val['federal_type'])
								  ->setCellValue('E'.$row, $each_val['State'])
								  ->setCellValue('F'.$row, $each_val['Status'])
								  ->setCellValue('G'.$row, '')
								  ->setCellValue('H'.$row, $each_val['EffectiveDate'])
								  ->setCellValue('I'.$row, '')
								  ->setCellValue('J'.$row, $each_val['est_dt_approval'])
								  ->setCellValue('K'.$row, $each_val['StateApprovalNumber'])
								  ->setCellValue('L'.$row, $each_val['Preparer'])
								  ->setCellValue('M'.$row, '')
								  ->setCellValue('N'.$row, '');
								  
				
    $row++;
     }
} while ( sqlsrv_next_result($getResults) );

sqlsrv_free_stmt( $getResults);

}

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setTitle('Sheet One');	

/* second sheet : start */

if($sheet_two_num>0)
{
	// Create a new worksheet, after the default sheet
     $objPHPExcel->createSheet();
     $objPHPExcel->setActiveSheetIndex(1);
	 
	 $row = 1;
$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Product Code')
                              ->setCellValue('B'.$row, 'Brand Name')
							  ->setCellValue('C'.$row, 'Product Description')
							  ->setCellValue('D'.$row, 'Federal Type')
							  ->setCellValue('E'.$row, 'State')
							  ->setCellValue('F'.$row, 'Status')
							  ->setCellValue('G'.$row, 'Submitted Date')
							  ->setCellValue('H'.$row, 'Effective Date')
							  ->setCellValue('I'.$row, 'Expiration Date')
							  ->setCellValue('J'.$row, 'Estimated Date of Approval')
							  ->setCellValue('K'.$row, 'Brand label registration (BLR) #')
							  ->setCellValue('L'.$row, 'MHW user that filed')
							  ->setCellValue('M'.$row, 'MHW Team')
							  ->setCellValue('N'.$row, 'CompID');
							  							  
$row++;

	 
	 for($total_rows2=$total_rows;$total_rows2<$sheet_two_num;$total_rows2=$total_rows2+5000)
{
	$max_limit=5000;
	$pagination_qry="OFFSET $total_rows2 ROWS FETCH NEXT $max_limit ROWS ONLY";
	$tsql_temp=$tsql." ".$pagination_qry;
	//echo "<br><hr>".$tsql_temp."<hr><br>"; 
	$getResults= sqlsrv_query($conn, $tsql_temp);
    if ($getResults == FALSE)
	echo (sqlsrv_errors());

    do {
     while ($each_val = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
		 
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $each_val['product_mhw_code'])
                                  ->setCellValue('B'.$row, $each_val['brand_name'])
								  ->setCellValue('C'.$row, $each_val['product_desc'])
								  ->setCellValue('D'.$row, $each_val['federal_type'])
								  ->setCellValue('E'.$row, $each_val['State'])
								  ->setCellValue('F'.$row, $each_val['Status'])
								  ->setCellValue('G'.$row, '')
								  ->setCellValue('H'.$row, $each_val['EffectiveDate'])
								  ->setCellValue('I'.$row, '')
								  ->setCellValue('J'.$row, $each_val['est_dt_approval'])
								  ->setCellValue('K'.$row, $each_val['StateApprovalNumber'])
								  ->setCellValue('L'.$row, $each_val['Preparer'])
								  ->setCellValue('M'.$row, '')
								  ->setCellValue('N'.$row, '');
								  
				
    $row++;
     }
} while ( sqlsrv_next_result($getResults) );

sqlsrv_free_stmt( $getResults);

}

	
}

$objPHPExcel->getActiveSheet()->setTitle('Sheet Two');

$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$file_name.'"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
}
else if(($_POST['export_view_type'] === 'Federal product compliance status') && $_POST['export_qry_type']=='blr-status')
{
/* federal product compliance status : data export */
$file_name="Fedetal Status.xlsx";	
$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("jahid0604015@gmail.com")
							 ->setTitle($file_name)
							 ->setSubject($file_name);

$row = 1;
$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Product Code')
                              ->setCellValue('B'.$row, 'Brand Name')
							  ->setCellValue('C'.$row, 'Product Description')
							  ->setCellValue('D'.$row, 'Federal Type')
							  ->setCellValue('E'.$row, 'Product Class')
							  ->setCellValue('F'.$row, 'TTB ID')
							  ->setCellValue('G'.$row, 'Status')
							  ->setCellValue('H'.$row, 'Date of Application')
							  ->setCellValue('I'.$row, 'Estimated Date of Approval')
							  ->setCellValue('J'.$row, 'Date Approved')
							  ->setCellValue('K'.$row, 'TTB Basic Importer #')
							  ->setCellValue('L'.$row, 'Country of Origin')
							  ->setCellValue('M'.$row, 'TTB Basic Importer Name')
							  ->setCellValue('N'.$row, 'MHW User that filed')
							  ->setCellValue('O'.$row, 'MHW Team')
							  ->setCellValue('P'.$row, 'CompID');
							  							  
$row++;

$sheet_one_num=60000;
$sheet_two_num=0;
if($pagination_total_num<=60000)
{
$sheet_one_num=$pagination_total_num;
$sheet_two_num=0;
}
else if($pagination_total_num>60000)
{
$sheet_two_num=$pagination_total_num;
}

for($total_rows=0;$total_rows<$sheet_one_num;$total_rows=$total_rows+5000)
{
	$max_limit=5000;
	$pagination_qry="OFFSET $total_rows ROWS FETCH NEXT $max_limit ROWS ONLY";
	$tsql_temp=$tsql." ".$pagination_qry;
	//echo "<br><hr>".$tsql_temp."<hr><br>"; 
	$getResults= sqlsrv_query($conn, $tsql_temp);
    if ($getResults == FALSE)
	echo (sqlsrv_errors());

    do {
     while ($each_val = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
		 
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $each_val['product_mhw_code'])
                                  ->setCellValue('B'.$row, $each_val['brand_name'])
								  ->setCellValue('C'.$row, $each_val['product_desc'])
								  ->setCellValue('D'.$row, $each_val['federal_type'])
								  ->setCellValue('E'.$row, $each_val['product_class'])
								  ->setCellValueExplicit('F'.$row,(string)$each_val['TTB_ID'],PHPExcel_Cell_DataType::TYPE_STRING)
								  ->setCellValue('G'.$row, $each_val['status'])
								  ->setCellValue('H'.$row, '')
								  ->setCellValue('I'.$row, $each_val['est_dt_approval'])
								  ->setCellValue('J'.$row, $each_val['dateapproved'])
								  ->setCellValue('K'.$row, $each_val['primaryfederalbasicpermitnumber'])
								  ->setCellValue('L'.$row, $each_val['country'])
								  ->setCellValue('M'.$row, $each_val['legalnameusedonlabel'])
								  ->setCellValue('N'.$row, $each_val['applicantname'])
								  ->setCellValue('O'.$row, '')
								  ->setCellValue('P'.$row, '');
								  
				
    $row++;
     }
} while ( sqlsrv_next_result($getResults) );

sqlsrv_free_stmt( $getResults);

}

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setTitle('Sheet One');	

/* second sheet : start */

if($sheet_two_num>0)
{
	// Create a new worksheet, after the default sheet
     $objPHPExcel->createSheet();
     $objPHPExcel->setActiveSheetIndex(1);
	 
	 $row = 1;
$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Product Code')
                              ->setCellValue('B'.$row, 'Brand Name')
							  ->setCellValue('C'.$row, 'Product Description')
							  ->setCellValue('D'.$row, 'Federal Type')
							  ->setCellValue('E'.$row, 'Product Class')
							  ->setCellValue('F'.$row, 'TTB ID')
							  ->setCellValue('G'.$row, 'Status')
							  ->setCellValue('H'.$row, 'Date of Application')
							  ->setCellValue('I'.$row, 'Estimated Date of Approval')
							  ->setCellValue('J'.$row, 'Date Approved')
							  ->setCellValue('K'.$row, 'TTB Basic Importer #')
							  ->setCellValue('L'.$row, 'Country of Origin')
							  ->setCellValue('M'.$row, 'TTB Basic Importer Name')
							  ->setCellValue('N'.$row, 'MHW User that filed')
							  ->setCellValue('O'.$row, 'MHW Team')
							  ->setCellValue('P'.$row, 'CompID');
							  							  
$row++;

	 
	 for($total_rows2=$total_rows;$total_rows2<$sheet_two_num;$total_rows2=$total_rows2+5000)
{
	$max_limit=5000;
	$pagination_qry="OFFSET $total_rows2 ROWS FETCH NEXT $max_limit ROWS ONLY";
	$tsql_temp=$tsql." ".$pagination_qry;
	//echo "<br><hr>".$tsql_temp."<hr><br>"; 
	$getResults= sqlsrv_query($conn, $tsql_temp);
    if ($getResults == FALSE)
	echo (sqlsrv_errors());

    do {
     while ($each_val = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
		 
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $each_val['product_mhw_code'])
                                  ->setCellValue('B'.$row, $each_val['brand_name'])
								  ->setCellValue('C'.$row, $each_val['product_desc'])
								  ->setCellValue('D'.$row, $each_val['federal_type'])
								  ->setCellValue('E'.$row, $each_val['product_class'])
								  ->setCellValueExplicit('F'.$row,(string)$each_val['TTB_ID'],PHPExcel_Cell_DataType::TYPE_STRING)
								  ->setCellValue('G'.$row, $each_val['status'])
								  ->setCellValue('H'.$row, '')
								  ->setCellValue('I'.$row, $each_val['est_dt_approval'])
								  ->setCellValue('J'.$row, $each_val['dateapproved'])
								  ->setCellValue('K'.$row, $each_val['primaryfederalbasicpermitnumber'])
								  ->setCellValue('L'.$row, $each_val['country'])
								  ->setCellValue('M'.$row, $each_val['legalnameusedonlabel'])
								  ->setCellValue('N'.$row, $each_val['applicantname'])
								  ->setCellValue('O'.$row, '')
								  ->setCellValue('P'.$row, '');
								  
				
    $row++;
     }
} while ( sqlsrv_next_result($getResults) );

sqlsrv_free_stmt( $getResults);

}

	
}

$objPHPExcel->getActiveSheet()->setTitle('Sheet Two');

$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$file_name.'"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
}
else if(($_POST['export_view_type'] === 'State product compliance status') && $_POST['export_qry_type']=='blr-status')
{
/* state product compliance status : data export */

$file_name="State Status.xlsx";	
$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("jahid0604015@gmail.com")
							 ->setTitle($file_name)
							 ->setSubject($file_name);

$row = 1;
$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Product Code')
                              ->setCellValue('B'.$row, 'Brand Name')
							  ->setCellValue('C'.$row, 'Product Description')
							  ->setCellValue('D'.$row, 'Federal Type')
							  ->setCellValue('E'.$row, 'State')
							  ->setCellValue('F'.$row, 'Status')
							  ->setCellValue('G'.$row, 'Submitted Date')
							  ->setCellValue('H'.$row, 'Effective Date')
							  ->setCellValue('I'.$row, 'Expiration Date')
							  ->setCellValue('J'.$row, 'Estimated Date of Approval')
							  ->setCellValue('K'.$row, 'Brand label registration (BLR) #')
							  ->setCellValue('L'.$row, 'MHW user that filed')
							  ->setCellValue('M'.$row, 'MHW Team')
							  ->setCellValue('N'.$row, 'CompID');
		
							  							  
$row++;


$sheet_one_num=60000;
$sheet_two_num=0;
if($pagination_total_num<=60000)
{
$sheet_one_num=$pagination_total_num;
$sheet_two_num=0;
}
else if($pagination_total_num>60000)
{
$sheet_two_num=$pagination_total_num;
}

for($total_rows=0;$total_rows<$sheet_one_num;$total_rows=$total_rows+5000)
{
	$max_limit=5000;
	$pagination_qry="OFFSET $total_rows ROWS FETCH NEXT $max_limit ROWS ONLY";
	$tsql_temp=$tsql." ".$pagination_qry;
	//echo "<br><hr>".$tsql_temp."<hr><br>"; exit;
	$getResults= sqlsrv_query($conn, $tsql_temp);
    if ($getResults == FALSE)
	echo (sqlsrv_errors());

    do {
     while ($each_val = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
		 
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $each_val['product_mhw_code'])
                                  ->setCellValue('B'.$row, $each_val['brand_name'])
								  ->setCellValue('C'.$row, $each_val['product_desc'])
								  ->setCellValue('D'.$row, $each_val['federal_type'])
								  ->setCellValue('E'.$row, $each_val['State'])
								  ->setCellValue('F'.$row, $each_val['Status'])
								  ->setCellValue('G'.$row, '')
								  ->setCellValue('H'.$row, $each_val['EffectiveDate'])
								  ->setCellValue('I'.$row, '')
								  ->setCellValue('J'.$row, $each_val['est_dt_approval'])
								  ->setCellValue('K'.$row, $each_val['StateApprovalNumber'])
								  ->setCellValue('L'.$row, $each_val['Preparer'])
								  ->setCellValue('M'.$row, '')
								  ->setCellValue('N'.$row, '');
				
    $row++;
     }
} while ( sqlsrv_next_result($getResults) );

sqlsrv_free_stmt( $getResults);

}

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setTitle('Sheet One');	

/* second sheet : start */

if($sheet_two_num>0)
{
	// Create a new worksheet, after the default sheet
     $objPHPExcel->createSheet();
     $objPHPExcel->setActiveSheetIndex(1);
	 
	 $row = 1;
$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Product Code')
                              ->setCellValue('B'.$row, 'Brand Name')
							  ->setCellValue('C'.$row, 'Product Description')
							  ->setCellValue('D'.$row, 'Federal Type')
							  ->setCellValue('E'.$row, 'State')
							  ->setCellValue('F'.$row, 'Status')
							  ->setCellValue('G'.$row, 'Submitted Date')
							  ->setCellValue('H'.$row, 'Effective Date')
							  ->setCellValue('I'.$row, 'Expiration Date')
							  ->setCellValue('J'.$row, 'Estimated Date of Approval')
							  ->setCellValue('K'.$row, 'Brand label registration (BLR) #')
							  ->setCellValue('L'.$row, 'MHW user that filed')
							  ->setCellValue('M'.$row, 'MHW Team')
							  ->setCellValue('N'.$row, 'CompID');
		
							  							  
$row++;

	 
	 for($total_rows2=$total_rows;$total_rows2<$sheet_two_num;$total_rows2=$total_rows2+5000)
{
	$max_limit=5000;
	$pagination_qry="OFFSET $total_rows2 ROWS FETCH NEXT $max_limit ROWS ONLY";
	$tsql_temp=$tsql." ".$pagination_qry;
	//echo "<br><hr>".$tsql_temp."<hr><br>"; 
	$getResults= sqlsrv_query($conn, $tsql_temp);
    if ($getResults == FALSE)
	echo (sqlsrv_errors());

    do {
     while ($each_val = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
		 
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $each_val['product_mhw_code'])
                                  ->setCellValue('B'.$row, $each_val['brand_name'])
								  ->setCellValue('C'.$row, $each_val['product_desc'])
								  ->setCellValue('D'.$row, $each_val['federal_type'])
								  ->setCellValue('E'.$row, $each_val['State'])
								  ->setCellValue('F'.$row, $each_val['Status'])
								  ->setCellValue('G'.$row, '')
								  ->setCellValue('H'.$row, $each_val['EffectiveDate'])
								  ->setCellValue('I'.$row, '')
								  ->setCellValue('J'.$row, $each_val['est_dt_approval'])
								  ->setCellValue('K'.$row, $each_val['StateApprovalNumber'])
								  ->setCellValue('L'.$row, $each_val['Preparer'])
								  ->setCellValue('M'.$row, '')
								  ->setCellValue('N'.$row, '');
								  				
    $row++;
     }
} while ( sqlsrv_next_result($getResults) );

sqlsrv_free_stmt( $getResults);

}

	
}

$objPHPExcel->getActiveSheet()->setTitle('Sheet Two');

$objPHPExcel->setActiveSheetIndex(0);


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$file_name.'"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');

header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
}
else if($_POST['export_view_type']=='viewproducts' && $_POST['export_qry_type']=='VPitems')
{
	
	
$file_name="Items.xlsx";	
$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("jahid0604015@gmail.com")
							 ->setTitle($file_name)
							 ->setSubject($file_name);

$row = 1;


$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Product Code')
                              ->setCellValue('B'.$row, 'Brand Name')
							  ->setCellValue('C'.$row, 'Product Description')
							  ->setCellValue('D'.$row, 'Item Code (Client)')
							  ->setCellValue('E'.$row, 'Item Code (MWH)')
							  ->setCellValue('F'.$row, 'Item Description')
							  ->setCellValue('G'.$row, 'Container Type')
							  ->setCellValue('H'.$row, 'Container Size')
							  ->setCellValue('I'.$row, 'Stock UOM')
							  ->setCellValue('J'.$row, 'Bottles Per Case')
							  ->setCellValue('K'.$row, 'UPC')
							  ->setCellValue('L'.$row, 'SSC')
							  ->setCellValue('M'.$row, 'Height')
							  ->setCellValue('N'.$row, 'Length')
							  ->setCellValue('O'.$row, 'Width')
							  ->setCellValue('P'.$row, 'Case/Unit Dimentions')
							  ->setCellValue('Q'.$row, 'Weight')
							  ->setCellValue('R'.$row, 'Weight UOM')
							  ->setCellValue('S'.$row, 'Vintage')
							  ->setCellValue('T'.$row, 'Chill Storage')
							  ->setCellValue('U'.$row, 'Item Class')
							  ->setCellValue('V'.$row, 'Created User')
							  ->setCellValue('W'.$row, 'Created Date');
							  
							
							   						  
$row++;


$sheet_one_num=60000;
$sheet_two_num=0;
if($pagination_total_num<=60000)
{
$sheet_one_num=$pagination_total_num;
$sheet_two_num=0;
}
else if($pagination_total_num>60000)
{
$sheet_two_num=$pagination_total_num;
}

for($total_rows=0;$total_rows<$sheet_one_num;$total_rows=$total_rows+5000)
{
	$max_limit=5000;
	$pagination_qry="OFFSET $total_rows ROWS FETCH NEXT $max_limit ROWS ONLY";
	$tsql_temp=$tsql." ".$pagination_qry;
	//echo "<br><hr>".$tsql_temp."<hr><br>"; exit;
	$getResults= sqlsrv_query($conn, $tsql_temp);
    if ($getResults == FALSE)
	echo (sqlsrv_errors());

    do {
     while ($each_val = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
		 
$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $each_val['product_mhw_code'])
                                  ->setCellValue('B'.$row, $each_val['brand_name'])
								  ->setCellValue('C'.$row, $each_val['product_desc'])
								  ->setCellValue('D'.$row, $each_val['item_client_code'])
								  ->setCellValue('E'.$row, $each_val['item_mhw_code'])
								  ->setCellValue('F'.$row, $each_val['item_description'])
								  ->setCellValue('G'.$row, $each_val['container_type'])
								  ->setCellValue('H'.$row, $each_val['container_size'])
								  ->setCellValue('I'.$row, $each_val['stock_uom'])
								  ->setCellValue('J'.$row, $each_val['bottles_per_case'])
								  ->setCellValue('K'.$row, $each_val['upc'])
								  ->setCellValue('L'.$row, $each_val['scc'])
								  ->setCellValue('M'.$row, $each_val['height'])
								  ->setCellValue('N'.$row, $each_val['length'])
								  ->setCellValue('O'.$row, $each_val['width'])
								  ->setCellValue('P'.$row, $each_val['unit_dimensions'])
								  ->setCellValue('Q'.$row, $each_val['weight'])
								  ->setCellValue('R'.$row, $each_val['weight_uom'])
								  ->setCellValue('S'.$row, $each_val['vintage'])
								  ->setCellValue('T'.$row, $each_val['chill_storage'])
								  ->setCellValue('U'.$row, $each_val['item_class'])
								  ->setCellValue('V'.$row, $each_val['item_created_by'])
								  ->setCellValue('W'.$row, $each_val['item_creation_date']);
								  
							  	
    $row++;
     }
} while ( sqlsrv_next_result($getResults) );

sqlsrv_free_stmt( $getResults);

}

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setTitle('Sheet One');	

/* second sheet : start */

if($sheet_two_num>0)
{
	// Create a new worksheet, after the default sheet
     $objPHPExcel->createSheet();
     $objPHPExcel->setActiveSheetIndex(1);
	 
	 $row = 1;
		 
$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Product Code')
                              ->setCellValue('B'.$row, 'Brand Name')
							  ->setCellValue('C'.$row, 'Product Description')
							  ->setCellValue('D'.$row, 'Item Code (Client)')
							  ->setCellValue('E'.$row, 'Item Code (MWH)')
							  ->setCellValue('F'.$row, 'Item Description')
							  ->setCellValue('G'.$row, 'Container Type')
							  ->setCellValue('H'.$row, 'Container Size')
							  ->setCellValue('I'.$row, 'Stock UOM')
							  ->setCellValue('J'.$row, 'Bottles Per Case')
							  ->setCellValue('K'.$row, 'UPC')
							  ->setCellValue('L'.$row, 'SSC');
		
							  							  
$row++;
	 
	 for($total_rows2=$total_rows;$total_rows2<$sheet_two_num;$total_rows2=$total_rows2+5000)
{
	$max_limit=5000;
	$pagination_qry="OFFSET $total_rows2 ROWS FETCH NEXT $max_limit ROWS ONLY";
	$tsql_temp=$tsql." ".$pagination_qry;
	//echo "<br><hr>".$tsql_temp."<hr><br>"; 
	$getResults= sqlsrv_query($conn, $tsql_temp);
    if ($getResults == FALSE)
	echo (sqlsrv_errors());

    do {
     while ($each_val = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
		 
$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $each_val['product_mhw_code'])
                                  ->setCellValue('B'.$row, $each_val['brand_name'])
								  ->setCellValue('C'.$row, $each_val['product_desc'])
								  ->setCellValue('D'.$row, $each_val['item_client_code'])
								  ->setCellValue('E'.$row, $each_val['item_mhw_code'])
								  ->setCellValue('F'.$row, $each_val['item_description'])
								  ->setCellValue('G'.$row, $each_val['container_type'])
								  ->setCellValue('H'.$row, $each_val['container_size'])
								  ->setCellValue('I'.$row, $each_val['stock_uom'])
								  ->setCellValue('J'.$row, $each_val['bottles_per_case'])
								  ->setCellValue('K'.$row, $each_val['upc'])
								  ->setCellValue('L'.$row, $each_val['scc']);
							  	
								  				
    $row++;
     }
} while ( sqlsrv_next_result($getResults) );

sqlsrv_free_stmt( $getResults);

}

	
}

$objPHPExcel->getActiveSheet()->setTitle('Sheet Two');

$objPHPExcel->setActiveSheetIndex(0);


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$file_name.'"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');

header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
	
}
else if($_POST['export_view_type']=='viewproducts' && ($_POST['export_qry_type']=='VPdefault' || $_POST['export_qry_type']=='VPpending'))
{
	

$file_name="Products.xlsx";	

if($_POST['export_qry_type']=='VPunfinalized')
{
	$file_name="Draft Products.xlsx";	
}
else if($_POST['export_qry_type']=='VPpending')
{
	$file_name="Processing Products.xlsx";	
}	
else if($_POST['export_qry_type']=='VPfinalized')
{
	$file_name="Finalized Products.xlsx";	
}

$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("jahid0604015@gmail.com")
							 ->setTitle($file_name)
							 ->setSubject($file_name);

$row = 1;


$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Brand Name')
                              ->setCellValue('B'.$row, 'Product Code(MHW)')
							  ->setCellValue('C'.$row, 'Product Description')
							  ->setCellValue('D'.$row, 'TTB ID')
							  ->setCellValue('E'.$row, 'Federal Type')
							  ->setCellValue('F'.$row, 'Compliance Type')
							  ->setCellValue('G'.$row, 'Product Class')
							  ->setCellValue('H'.$row, 'Mktg Product Type')
							  ->setCellValue('I'.$row, 'Beverage Type')
							  ->setCellValue('J'.$row, 'Country')
							  ->setCellValue('K'.$row, 'Appellation')
							  ->setCellValue('L'.$row, 'Lot Item')
							  ->setCellValue('M'.$row, 'Alcohol %')
							  ->setCellValue('N'.$row, 'Created Date')
							  ->setCellValue('O'.$row, 'Created User')
							  ->setCellValue('P'.$row, 'Supplier Name')
							  ->setCellValue('Q'.$row, 'Contact')
							  ->setCellValue('R'.$row, 'FDA #')
							  ->setCellValue('S'.$row, 'Tax Reduction Allocation');
							  
							   						  
$row++;



$sheet_one_num=60000;
$sheet_two_num=0;
if($pagination_total_num<=60000)
{
$sheet_one_num=$pagination_total_num;
$sheet_two_num=0;
}
else if($pagination_total_num>60000)
{
$sheet_two_num=$pagination_total_num;
}

for($total_rows=0;$total_rows<$sheet_one_num;$total_rows=$total_rows+5000)
{
	$max_limit=5000;
	$pagination_qry="OFFSET $total_rows ROWS FETCH NEXT $max_limit ROWS ONLY";
	$tsql_temp=$tsql." ".$pagination_qry;
	//echo "<br><hr>".$tsql_temp."<hr><br>"; exit;
	$getResults= sqlsrv_query($conn, $tsql_temp);
    if ($getResults == FALSE)
	echo (sqlsrv_errors());

    do {
     while ($each_val = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
				 
     $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $each_val['brand_name'])
                                  ->setCellValue('B'.$row, $each_val['product_mhw_code'])
								  ->setCellValue('C'.$row, $each_val['product_desc'])
								  ->setCellValueExplicit('D'.$row,(string)$each_val['TTB_ID'],PHPExcel_Cell_DataType::TYPE_STRING)
								  ->setCellValue('E'.$row, $each_val['federal_type'])
								  ->setCellValue('F'.$row, $each_val['compliance_type'])
								  ->setCellValue('G'.$row, $each_val['product_class'])
								  ->setCellValue('H'.$row, $each_val['mktg_prod_type'])
								  ->setCellValue('I'.$row, $each_val['bev_type'])
								  ->setCellValue('J'.$row, $each_val['country'])
								  ->setCellValue('K'.$row, $each_val['appellation'])
								  ->setCellValue('L'.$row, $each_val['lot_item'])
								  ->setCellValue('M'.$row, $each_val['alcohol_pct'])
								  ->setCellValue('N'.$row, $each_val['product_creation_date'])
								  ->setCellValue('O'.$row, $each_val['product_created_by'])
								  ->setCellValue('P'.$row, $each_val['supplier_name'])
								  ->setCellValue('Q'.$row, $each_val['supplier_contact'])
								  ->setCellValue('R'.$row, $each_val['supplier_fda_number'])
								  ->setCellValue('S'.$row, $each_val['tax_reduction_allocation']);
								  
				  	
    $row++;
     }
} while ( sqlsrv_next_result($getResults) );

sqlsrv_free_stmt( $getResults);

}

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setTitle('Sheet One');	


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$file_name.'"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');

header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
	
}
else if($_POST['export_view_type']=='viewproducts' && ($_POST['export_qry_type']=='VPunfinalized' || $_POST['export_qry_type']=='VPfinalized'))
{
	
$file_name="Draft Products.xlsx";

if($_POST['export_qry_type']=='VPunfinalized')
{
	$file_name="Draft Products.xlsx";	
}
else if($_POST['export_qry_type']=='VPpending')
{
	$file_name="Processing Products.xlsx";	
}	
else if($_POST['export_qry_type']=='VPfinalized')
{
	$file_name="Finalized Products.xlsx";	
}	
	
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("jahid0604015@gmail.com")
							 ->setTitle($file_name)
							 ->setSubject($file_name);

$row = 1;

$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Brand Name')
                              ->setCellValue('B'.$row, 'Product Code(MHW)')
							  ->setCellValue('C'.$row, 'Product Description')
							  ->setCellValue('D'.$row, 'TTB ID')
							  ->setCellValue('E'.$row, 'Federal Type')
							  ->setCellValue('F'.$row, 'Compliance Type')
							  ->setCellValue('G'.$row, 'Product Class')
							  ->setCellValue('H'.$row, 'Mktg Product Type')
							  ->setCellValue('I'.$row, 'Beverage Type')
							  ->setCellValue('J'.$row, 'Country')
							  ->setCellValue('K'.$row, 'Appellation')
							  ->setCellValue('L'.$row, 'Lot Item')
							  ->setCellValue('M'.$row, 'Alcohol %')
							  ->setCellValue('N'.$row, 'Created Date')
							  ->setCellValue('O'.$row, 'Created User')
							  ->setCellValue('P'.$row, 'Supplier Name')
							  ->setCellValue('Q'.$row, 'Contact')
							  ->setCellValue('R'.$row, 'FDA #')
							  ->setCellValue('S'.$row, 'Tax Reduction Allocation')
							  ->setCellValue('T'.$row, 'MHW Review');
							  
							   						  
$row++;



$sheet_one_num=60000;
$sheet_two_num=0;
if($pagination_total_num<=60000)
{
$sheet_one_num=$pagination_total_num;
$sheet_two_num=0;
}
else if($pagination_total_num>60000)
{
$sheet_two_num=$pagination_total_num;
}

for($total_rows=0;$total_rows<$sheet_one_num;$total_rows=$total_rows+5000)
{
	$max_limit=5000;
	$pagination_qry="OFFSET $total_rows ROWS FETCH NEXT $max_limit ROWS ONLY";
	$tsql_temp=$tsql." ".$pagination_qry;
	//echo "<br><hr>".$tsql_temp."<hr><br>"; exit;
	$getResults= sqlsrv_query($conn, $tsql_temp);
    if ($getResults == FALSE)
	echo (sqlsrv_errors());

    do {
     while ($each_val = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
				 
     $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $each_val['brand_name'])
                                  ->setCellValue('B'.$row, $each_val['product_mhw_code'])
								  ->setCellValue('C'.$row, $each_val['product_desc'])
								  ->setCellValueExplicit('D'.$row,(string)$each_val['TTB_ID'],PHPExcel_Cell_DataType::TYPE_STRING)
								  ->setCellValue('E'.$row, $each_val['federal_type'])
								  ->setCellValue('F'.$row, $each_val['compliance_type'])
								  ->setCellValue('G'.$row, $each_val['product_class'])
								  ->setCellValue('H'.$row, $each_val['mktg_prod_type'])
								  ->setCellValue('I'.$row, $each_val['bev_type'])
								  ->setCellValue('J'.$row, $each_val['country'])
								  ->setCellValue('K'.$row, $each_val['appellation'])
								  ->setCellValue('L'.$row, $each_val['lot_item'])
								  ->setCellValue('M'.$row, $each_val['alcohol_pct'])
								  ->setCellValue('N'.$row, $each_val['product_creation_date'])
								  ->setCellValue('O'.$row, $each_val['product_created_by'])
								  ->setCellValue('P'.$row, $each_val['supplier_name'])
								  ->setCellValue('Q'.$row, $each_val['supplier_contact'])
								  ->setCellValue('R'.$row, $each_val['supplier_fda_number'])
								  ->setCellValue('S'.$row, $each_val['tax_reduction_allocation'])
								  ->setCellValue('T'.$row, $each_val['rejremark']);
								  
				  	
    $row++;
     }
} while ( sqlsrv_next_result($getResults) );

sqlsrv_free_stmt( $getResults);

}

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setTitle('Sheet One');	


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$file_name.'"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');

header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;	
}

   
function utf8ize($d) {
	if (is_array($d)) {
		foreach ($d as $k => $v) {
			$d[$k] = utf8ize($v);
		}
	} else if (is_string ($d)) {
		return utf8_encode($d);
	}
	return $d;
}

/* Free statement and connection resources. */

sqlsrv_free_stmt( $trkResults);
sqlsrv_close( $conn);
?>