<?php
include('settings.php');
error_reporting(E_ALL); //displays an error

session_start();
if(!isset($_SESSION['mhwltdphp_user'])){
	die( "Not authenticated !" );  
}

include("dbconnect.php");

//Establishes the connection
$conn = sqlsrv_connect($serverName, $connectionOptions);
if( $conn === false) {
		die( print_r( sqlsrv_errors(), true));
}

// total_num pagination 
$pagination_total_num=0;
$tsql_total_num="";

if($_SESSION['mhwltdphp_user']!='' && $_POST['qrytype']!=''){
	$trksql= "INSERT INTO [mhw_app_workflow] VALUES ('_comprev__".$_POST['qrytype']."', 0, GETDATE(), GETDATE(), '".$_SESSION['mhwltdphp_user']."', 1, 0)";
	$trkResults= sqlsrv_query($conn, $trksql);
}

if($_POST['qrytype']=='prodByID')
{
	if(isset($_POST['prodID']) && $_POST['prodID']!=='' && isset($_POST['client']) && $_POST['client']!=='')
	{
		//$tsql= "SELECT p.[product_id],p.[client_name],p.[brand_name],p.[product_desc],p.[product_mhw_code],p.[product_mhw_code_search],p.[TTB_ID] as [TTB],p.[federal_type],p.[compliance_type],CASE WHEN p.[product_class] = 'Foreign' THEN 'Imported' ELSE p.[product_class] END AS [product_class],p.[mktg_prod_type],p.[bev_type],p.[fanciful],p.[country],p.[appellation],p.[lot_item],p.[bottle_material],p.[alcohol_pct],s.[supplier_id],s.[supplier_name],s.[supplier_contact],s.[supplier_fda_number],s.[tax_reduction_allocation],s.[supplier_address_1],s.[supplier_address_2],s.[supplier_address_3],s.[supplier_city],s.[supplier_state],s.[supplier_country],s.[supplier_zip],s.[supplier_phone],s.[supplier_email],s.[federal_basic_permit],(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[requirement_met] = 1 AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount' FROM [dbo].[mhw_app_prod] p left outer join [mhw_app_prod_supplier] s on s.[supplier_id] = p.[supplier_id] WHERE p.[product_id] = ".$_POST['prodID']." and p.[client_name] IN ('".$_POST['client']."') AND p.[active] = 1 and p.[deleted] = 0";
		
		$tsql= "EXEC [dbo].[usp_query_request] @querytype = '".$_POST['qrytype']."', @client = '".$_POST['client']."', @prodID = ".$_POST['prodID'].", @suppID = 0";
		//echo $tsql;
	}
}
if($_POST['qrytype']=='suppByID')
{
	if(isset($_POST['suppID']) && $_POST['suppID']!=='')
	{
		$tsql= "SELECT s.[supplier_name],s.[supplier_contact],s.[supplier_fda_number],s.[tax_reduction_allocation],s.[supplier_address_1],s.[supplier_address_2],s.[supplier_address_3],s.[supplier_city],s.[supplier_state],s.[supplier_country],s.[supplier_zip],s.[supplier_phone],s.[supplier_email],s.[federal_basic_permit] FROM [mhw_app_prod_supplier] s WHERE s.[supplier_id] = ".$_POST['suppID']." AND s.[active] = 1 and s.[deleted] = 0";
		//echo $tsql;
	}
}
if($_POST['qrytype']=='VPdefault'){
  if(isset($_POST['active_pagination']) && $_POST['active_pagination']=='yes')
	{
		// pagination custom searching 
		$filter_txt_qry="";
		/*
		if($_POST['filter_txt'] && $_POST['filter_txt']!='')
		{
			$filter_txt_val=$_POST['filter_txt'];
			$filter_txt_qry=" AND ( r.[ClientName] like '%".$filter_txt_val."%'  OR r.[Value1] like '%".$filter_txt_val."%' OR r.[Value2] like '%".$filter_txt_val."%'  OR r.[CustomerName] like '%".$filter_txt_val."%' 
			OR r.[Value3] like '%".$filter_txt_val."%' OR r.[OrderDate] like '%".$filter_txt_val."%'  OR r.[SlsLocn] like '%".$filter_txt_val."%' OR r.[OpsTeam] like '%".$filter_txt_val."%'
			OR r.[ShipToAddr] like '%".$filter_txt_val."%'  OR r.[City] like '%".$filter_txt_val."%' OR r.[State] like '%".$filter_txt_val."%'
			OR r.[Zip] like '%".$filter_txt_val."%' )";		
		}	
		*/
		
		$product_id_qry="";
		if(isset($_POST['val1']) && $_POST['val1']!=='')
		{   
	        $temp_prod_id=".$_POST['val1'].";
			$product_id_qry=" AND p.[product_id] = $temp_prod_id ";
		}
		
		// fetching total_num for pagination 
		$tsql_total_num = "SELECT count(p.[product_id]) as total_num FROM [dbo].[mhw_app_prod] p WITH (NOLOCK) 
				 LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] s WITH (NOLOCK) on s.[supplier_id] = p.[supplier_id] 
				 LEFT OUTER JOIN [dbo].[mhw_app_finalized_product_review] r WITH (NOLOCK) on r.[prod_id] = p.product_id AND r.active = 1 AND r.processed = 0
				 LEFT OUTER JOIN [dbo].[tray_email_routing] t WITH (NOLOCK) on t.[Client ID] = p.[client_code]
				 WHERE  p.[active] = 1 AND p.[deleted] = 0 AND p.[finalized] = 1 AND p.[processed] = 0   AND p.[product_desc] <> 'STAR' $product_id_qry $filter_txt_qry";
		
		
		/* BLR STATUS FEDERAL QUERY - DISPLAY EACH PRODUCT WITH OR WITHOUT APPROVAL */
		
        $per_page=($_POST['per_page_size']!='')?$_POST['per_page_size']:10;
		$start_from=($_POST['page_number']!='')?$_POST['page_number']:0;
		$start_from=$start_from-1;
		$start_from=($start_from<0)?0:$start_from;
		$starts_from=$per_page*$start_from;
		
		// Check Export Action 
		$pagination_qry="OFFSET $starts_from ROWS FETCH NEXT $per_page ROWS ONLY";
		if($_POST['is_export_data'] && $_POST['is_export_data']=='yes')
		{
			$pagination_qry="";
		}	

	
		$tsql = "SELECT p.[product_id],p.[client_code],p.[client_name]
			,p.[brand_name]
			,p.[product_desc]
				,p.[product_mhw_code],p.[product_mhw_code_search],p.[TTB_ID],p.[federal_type],p.[compliance_type],p.[product_class],p.[mktg_prod_type],p.[bev_type]
				,p.[fanciful]
				,p.[country]
			,p.[appellation]
				,p.[lot_item],p.[bottle_material],p.[alcohol_pct],p.[varietal],p.[create_date],s.[supplier_id]
				,s.[supplier_name]
				,s.[supplier_contact],s.[supplier_fda_number],s.[tax_reduction_allocation]
				,s.[supplier_address_1],s.[supplier_address_2],s.[supplier_address_3]
				,s.[supplier_city]
				,s.[supplier_state],s.[supplier_country]  ,s.[supplier_zip]  ,s.[supplier_phone] ,s.[supplier_email], s.[federal_basic_permit] ,s.[create_via] ,s.[create_date] ,s.[edit_date] 
				,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_item] i WHERE i.[product_id] = p.[product_id] AND i.[active] = 1 AND i.[deleted] = 0) as 'itemcount' 
				,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount' 
				,'<textarea cols=30 rows=4 id='+char(39)+'rr__'+cast(p.[product_id] as varchar)+char(39)+'></textarea>' as [Value8]
				,'<button id='+char(39)+'btna__'+cast(p.[product_id] as varchar)+char(39)+' data-id='+char(39)+cast(p.[product_id] as varchar)+char(39)+' class='+char(39)+'btna btn btn-success btn-lg'+char(39)+'><i class='+char(39)+'fas fa-check-double'+char(39)+'></i> Approve</button> <button id='+char(39)+'btnr__'+cast(p.[product_id] as varchar)+char(39)+' data-id='+char(39)+cast(p.[product_id] as varchar)+char(39)+' class='+char(39)+'btnr btn btn-danger btn-lg'+char(39)+'><i class='+char(39)+'fas fa-ban'+char(39)+'></i> Reject</button>'  as [Value9]
				,DATEADD(HOUR, -4, r.create_date) as 'finalized_date'
				,r.fq_username 
				,r.fq_useremail
				,'<span>'+CAST(DATEADD(HOUR, -4, r.create_date) as varchar)+'<br />'+r.fq_username +'</span>' as 'finalized_data'
				,ISNULL((Select top 1 DATEADD(HOUR, -4, edit_date) from mhw_app_workflow WHERE workflow_type IN ('product','product edit') AND record_id = p.product_id order by workflow_id desc),DATEADD(HOUR, -4, p.edit_date)) as 'product_date'
				,(Select top 1 DATEADD(HOUR, -4, edit_date) from mhw_app_workflow WHERE workflow_type = 'product_image' AND record_id IN (SELECT i.image_id from mhw_app_prod_image i where i.active = 1 AND i.product_id = p.product_id) order by workflow_id desc) as 'image_date'
				,'<span>Product: '+cast(ISNULL((Select top 1 DATEADD(HOUR, -4, edit_date) from mhw_app_workflow WHERE workflow_type IN ('product','product edit') AND record_id = p.product_id order by workflow_id desc),DATEADD(HOUR, -4, p.edit_date)) as varchar)+'<br />Image:'+cast((Select top 1 DATEADD(HOUR, -4, edit_date) from mhw_app_workflow WHERE workflow_type = 'product_image' AND record_id IN (SELECT i.image_id from mhw_app_prod_image i where i.active = 1 AND i.product_id = p.product_id) order by workflow_id desc) as varchar)+'</span>' as 'edited_data'
				,t.[Current Team Assignment ID] as 'mhwteam'
				 FROM [dbo].[mhw_app_prod] p WITH (NOLOCK) 
				 LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] s WITH (NOLOCK) on s.[supplier_id] = p.[supplier_id] 
				 LEFT OUTER JOIN [dbo].[mhw_app_finalized_product_review] r WITH (NOLOCK) on r.[prod_id] = p.product_id AND r.active = 1 AND r.processed = 0
				 LEFT OUTER JOIN [dbo].[tray_email_routing] t WITH (NOLOCK) on t.[Client ID] = p.[client_code]
				 WHERE  p.[active] = 1 AND p.[deleted] = 0 AND p.[finalized] = 1 AND p.[processed] = 0   AND p.[product_desc] <> 'STAR' $product_id_qry $filter_txt_qry
				 ORDER BY p.[client_name], p.[create_date] desc  $pagination_qry ";
							
		   // echo $tsql; exit;
	} 
   else
   {
     
		if(isset($_POST['val1']) && $_POST['val1']!=='')
		{
			//$tsql= "EXEC [dbo].[usp_query_request_comprev] @querytype = '".$_POST['qrytype']."', @client = ".$_POST['clientlist'].", @prodID = ".$_POST['val1'].", @suppID = 0";
			$tsql= "EXEC [dbo].[usp_query_request_comprev] @querytype = '".$_POST['qrytype']."', @client = '', @prodID = ".$_POST['val1'].", @suppID = 0";
		}
		else
		{
			//$tsql= "EXEC [dbo].[usp_query_request_comprev] @querytype = '".$_POST['qrytype']."', @client = ".$_POST['clientlist'].", @prodID = 0, @suppID = 0";
			$tsql= "EXEC [dbo].[usp_query_request_comprev] @querytype = '".$_POST['qrytype']."', @client = '', @prodID = 0, @suppID = 0";
		}
		//$tsql= "SELECT p.[product_id],p.[client_code],p.[client_name],p.[brand_name],p.[product_desc],p.[product_mhw_code],p.[product_mhw_code_search],p.[TTB_ID],p.[federal_type],p.[compliance_type],p.[product_class],p.[mktg_prod_type],p.[bev_type],p.[fanciful],p.[country],p.[appellation],p.[lot_item],p.[bottle_material],p.[alcohol_pct],p.[create_date],s.[supplier_id],s.[supplier_name],s.[supplier_contact],s.[supplier_fda_number],s.[tax_reduction_allocation],s.[supplier_address_1],s.[supplier_address_2],s.[supplier_address_3],s.[supplier_city],s.[supplier_state],s.[supplier_country]  ,s.[supplier_zip]  ,s.[supplier_phone] ,s.[supplier_email], s.[federal_basic_permit] ,s.[create_via] ,s.[create_date] ,s.[edit_date] ,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_item] i WHERE i.[product_id] = p.[product_id] AND i.[active] = 1 AND i.[deleted] = 0) as 'itemcount' ,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount' FROM [dbo].[mhw_app_prod] p WITH (NOLOCK) LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] s WITH (NOLOCK) on s.[supplier_id] = p.[supplier_id] WHERE p.[client_name] IN (".$_POST['clientlist'].") AND p.[active] = 1 AND p.[deleted] = 0 /* AND s.[active] = 1 AND s.[deleted] = 0 */ ORDER BY p.[create_date] desc";
	}
}
if($_POST['qrytype']=='ProdRevApp' || $_POST['qrytype']=='ProdRevRej' /*|| $_POST['qrytype']=='LineRevApp' || $_POST['qrytype']=='LineRevRej'*/ ){
	if(isset($_POST['qryid']) && intval($_POST['qryid'])>0)
	{
		$tsql= "EXEC [dbo].[usp_upd_product_review] @qrytype = '".$_POST['qrytype']."', @qryid = ".intval($_POST['qryid']).", @qrymsg = '".$_POST['qrymsg']."', @qryuser = '".$_POST['qryuser']."'";
		echo $tsql;
	}
}
if($_POST['qrytype']=='VPitems'){
	//if(isset($_POST['clientlist']) && $_POST['clientlist']!=='')
	//{
		//$tsql= "EXEC [dbo].[usp_query_request_comprev] @querytype = '".$_POST['qrytype']."', @client = ".$_POST['clientlist'].", @prodID = 0, @suppID = 0";
		$tsql= "EXEC [dbo].[usp_query_request_comprev] @querytype = '".$_POST['qrytype']."', @client = '', @prodID = 0, @suppID = 0";
		
		//$tsql= "SELECT i.[item_id],i.[item_client_code],i.[item_mhw_code],i.[item_description],i.[container_type],i.[container_size],i.[stock_uom],i.[bottles_per_case],i.[upc],i.[scc],i.[vintage],i.[various_vintages],i.[height],i.[length],i.[width],i.[weight],i.[item_status],i.[create_via],i.[create_date],i.[edit_date],p.[product_id],p.[client_code],p.[client_name],p.[brand_name],p.[product_desc],p.[product_mhw_code] FROM [dbo].[mhw_app_prod_item] i WITH (NOLOCK) left outer join [dbo].[mhw_app_prod] p WITH (NOLOCK) on p.[product_id] = i.[product_id] WHERE p.[client_name] IN (".$_POST['clientlist'].") AND p.[active] = 1 AND p.[deleted] = 0 AND i.[active] = 1 AND i.[deleted] = 0 ORDER BY p.[product_id], i.[create_date] desc";
	//}
}
if($_POST['qrytype']=='VPproditems'){
	if(isset($_POST['prodID']) && $_POST['prodID']!=='')
	{
		$tsql= "EXEC [dbo].[usp_query_request_comprev] @querytype = '".$_POST['qrytype']."', @client = '', @prodID = ".$_POST['prodID'].", @suppID = 0";

		//$tsql= "SELECT i.[item_id],i.[item_client_code],i.[item_mhw_code],i.[item_description]	      ,i.[container_type],i.[container_size],i.[stock_uom],i.[bottles_per_case],i.[upc],i.[scc],i.[vintage],i.[various_vintages],i.[height],i.[length],i.[width],i.[weight],i.[item_status],i.[create_via],i.[create_date],i.[edit_date],p.[product_id],p.[client_code],p.[client_name],p.[brand_name],p.[product_desc],p.[product_mhw_code] FROM [dbo].[mhw_app_prod_item] i WITH (NOLOCK) left outer join [dbo].[mhw_app_prod] p WITH (NOLOCK) on p.[product_id] = i.[product_id] WHERE i.[product_id] = '".$_POST['prodID']."' AND p.[active] = 1 AND p.[deleted] = 0 AND i.[active] = 1 AND i.[deleted] = 0 ORDER BY p.[product_id], i.[create_date] desc";
	}
}
if($_POST['qrytype']=='VPunfinalized'){
	if(isset($_POST['clientlist']) && $_POST['clientlist']!=='')
	{
		
		$tsql= "EXEC [dbo].[usp_query_request] @querytype = '".$_POST['qrytype']."', @client = ".$_POST['clientlist'].", @prodID = 0, @suppID = 0";

		//$tsql= "SELECT p.[product_id],p.[client_code],p.[client_name],p.[brand_name],p.[product_desc],p.[product_mhw_code],p.[product_mhw_code_search],p.[TTB_ID],p.[federal_type],p.[compliance_type],p.[product_class],p.[mktg_prod_type],p.[bev_type],p.[fanciful],p.[country],p.[appellation],p.[lot_item],p.[bottle_material],p.[alcohol_pct],p.[create_date],s.[supplier_id],s.[supplier_name],s.[supplier_contact],s.[supplier_fda_number],s.[tax_reduction_allocation],s.[supplier_address_1],s.[supplier_address_2],s.[supplier_address_3],s.[supplier_city],s.[supplier_state],s.[supplier_country]  ,s.[supplier_zip]  ,s.[supplier_phone] ,s.[supplier_email] ,s.[federal_basic_permit] ,s.[create_via] ,s.[create_date] ,s.[edit_date] ,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_item] i WHERE i.[product_id] = p.[product_id] AND i.[active] = 1 AND i.[deleted] = 0) as 'itemcount' ,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount' FROM [dbo].[mhw_app_prod] p WITH (NOLOCK) LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] s WITH (NOLOCK) on s.[supplier_id] = p.[supplier_id] WHERE p.[client_name] IN (".$_POST['clientlist'].") AND p.[finalized] = 0 AND p.[processed] = 0 AND p.[active] = 1 AND p.[deleted] = 0 AND s.[active] = 1 AND s.[deleted] = 0 ORDER BY p.[create_date] desc";
	}
}
if($_POST['qrytype']=='VPpending'){
	if(isset($_POST['clientlist']) && $_POST['clientlist']!=='')
	{
		$tsql= "EXEC [dbo].[usp_query_request] @querytype = '".$_POST['qrytype']."', @client = ".$_POST['clientlist'].", @prodID = 0, @suppID = 0";
		
		//$tsql= "SELECT p.[product_id],p.[client_code],p.[client_name],p.[brand_name],p.[product_desc],p.[product_mhw_code],p.[product_mhw_code_search],p.[TTB_ID],p.[federal_type],p.[compliance_type],p.[product_class],p.[mktg_prod_type],p.[bev_type],p.[fanciful],p.[country],p.[appellation],p.[lot_item],p.[bottle_material],p.[alcohol_pct],p.[create_date],s.[supplier_id],s.[supplier_name],s.[supplier_contact],s.[supplier_fda_number],s.[tax_reduction_allocation],s.[supplier_address_1],s.[supplier_address_2],s.[supplier_address_3],s.[supplier_city],s.[supplier_state],s.[supplier_country]  ,s.[supplier_zip]  ,s.[supplier_phone] ,s.[supplier_email] ,s.[federal_basic_permit] ,s.[create_via] ,s.[create_date] ,s.[edit_date] ,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_item] i WHERE i.[product_id] = p.[product_id] AND i.[active] = 1 AND i.[deleted] = 0) as 'itemcount' ,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount' FROM [dbo].[mhw_app_prod] p WITH (NOLOCK) LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] s WITH (NOLOCK) on s.[supplier_id] = p.[supplier_id] WHERE p.[client_name] IN (".$_POST['clientlist'].") AND p.[finalized] = 1 AND p.[processed] = 0 AND p.[active] = 1 AND p.[deleted] = 0 /* AND s.[active] = 1 AND s.[deleted] = 0 */ ORDER BY p.[create_date] desc";
	}
}
if($_POST['qrytype']=='VPfinalized'){
	if(isset($_POST['clientlist']) && $_POST['clientlist']!=='')
	{
		$tsql= "EXEC [dbo].[usp_query_request] @querytype = '".$_POST['qrytype']."', @client = ".$_POST['clientlist'].", @prodID = 0, @suppID = 0";
		
		//$tsql= "SELECT p.[product_id],p.[client_code],p.[client_name],p.[brand_name],p.[product_desc],p.[product_mhw_code],p.[product_mhw_code_search],p.[TTB_ID],p.[federal_type],p.[compliance_type],p.[product_class],p.[mktg_prod_type],p.[bev_type],p.[fanciful],p.[country],p.[appellation],p.[lot_item],p.[bottle_material],p.[alcohol_pct],p.[create_date],s.[supplier_id],s.[supplier_name],s.[supplier_contact],s.[supplier_fda_number],s.[tax_reduction_allocation],s.[supplier_address_1],s.[supplier_address_2],s.[supplier_address_3],s.[supplier_city],s.[supplier_state],s.[supplier_country]  ,s.[supplier_zip]  ,s.[supplier_phone] ,s.[supplier_email] ,s.[federal_basic_permit] ,s.[create_via] ,s.[create_date] ,s.[edit_date] ,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_item] i WHERE i.[product_id] = p.[product_id] AND i.[active] = 1 AND i.[deleted] = 0) as 'itemcount' ,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount' FROM [dbo].[mhw_app_prod] p WITH (NOLOCK) LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] s WITH (NOLOCK) on s.[supplier_id] = p.[supplier_id] WHERE p.[client_name] IN (".$_POST['clientlist'].") AND p.[finalized] = 1 AND p.[processed] = 1 AND p.[active] = 1 AND p.[deleted] = 0 /* AND s.[active] = 1 AND s.[deleted] = 0 */ ORDER BY p.[create_date] desc";
	}
}
if($_POST['qrytype']=='STAT_COUNT_unfinalized'){
	if(isset($_POST['clientlist']) && $_POST['clientlist']!=='')
	{
		$tsql= "SELECT COUNT(*) as 'unfinalizedCount' FROM [dbo].[mhw_app_prod] p WITH (NOLOCK) LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] s WITH (NOLOCK) on s.[supplier_id] = p.[supplier_id] WHERE p.[client_name] IN ('".$_POST['clientlist']."') AND p.[finalized] = 0 AND p.[processed] = 0 AND p.[active] = 1 AND p.[deleted] = 0 /* AND s.[active] = 1 AND s.[deleted] = 0 */";
	}
}
if($_POST['qrytype']=='processFinalize'){
	if(isset($_POST['prodlist']) && $_POST['prodlist']!=='')
	{
		$ProdIDString =  str_replace("]","",str_replace("[","",$_POST['prodlist']));
		
		$tsql= "UPDATE [dbo].[mhw_app_prod] SET [finalized] = 1 WHERE [product_id] IN (".$ProdIDString.")";
		//echo  $tsql;
	}
}
if($_POST['qrytype']=='alertFinalize'){
	if(isset($_POST['prodlist']) && $_POST['prodlist']!=='')
	{
		$ProdIDString =  str_replace("]","",str_replace("[","",$_POST['prodlist']));
		$ProdCodeString =  str_replace("]","",str_replace("[","",$_POST['codelist']));
		
		$tsql= "EXEC [dbo].[usp_product_finalize] '".$_POST['qrytype']."','".$ProdIDString."',".$_POST['clientlist'].",'".$ProdCodeString."','".$siteroot."','".$_SESSION['mhwltdphp_user']."','".$_SESSION['mhwltdphp_useremail']."'";
		//echo  $tsql;
	}
}
if($_POST['qrytype']=='itemByID')
	{
	if(isset($_POST['itemID']) && $_POST['itemID']!=='')
	{
		$itemID = intval($_POST['itemID']);
		$tsql= "
		SELECT TOP 1 [item_id]
			,[product_id]
			,[container_type]
			,[container_size]
			,[stock_uom]
			,[bottles_per_case]
			,[upc]
			,[scc]
			,[vintage]
			,[various_vintages]
			,[item_status]
			,[active]
			,[deleted]
			,[height]
			,[length]
			,[width]
			,[weight]
		FROM [dbo].[mhw_app_prod_item]
		WHERE [item_id] = $itemID";
	}
}
if ($_POST['qry_type']=='pricing'){
	if(isset($_POST['view_type']) && $_POST['view_type']!=='' && isset($_POST['view_state']) && $_POST['view_state']!=='' && isset($_POST['client']) && $_POST['client']!=='')
	{
		$whereArr = [];
		$whereMonth = "";
		if(isset($_POST['row_id']) && $_POST['row_id']!=='') {
			$whereArr[] = "l.ID = ".intval($_POST['row_id']);
		}
		if(isset($_POST['itemID']) && $_POST['itemID']!=='') {
			$whereArr[] = "l.itemID = ".intval($_POST['itemID']);
		}
		if (isset($_POST['effective_month'])) {
			$whereMonth = "WHERE tfa_effective_month = '".$_POST['effective_month']."' ";
		}
		if ($_POST['view_type']=='Open States' || $_POST['view_state']=='tfa_os') {
			$tableName = 'tfa_os';
			$state = str_replace("'","\'",$_REQUEST['view_state']);
			if ($state == "Deleted" || $_POST['view_type']=='Deleted Items') {
				$whereArr[] = "l.deleted = 1";
			} else {
				$whereArr[] = "ISNULL(l.deleted, 0) <> 1";
			}
			if ($state != "All" && $state != "Deleted" && $state != "tfa_os") {
				$whereArr[] = "l.tfa_os_price_state = '$state'";
			}
			$where = "AND ".implode($whereArr, " AND ");
			$tsql = "SELECT l.*,  convert(varchar, l.created_at, 120) AS created,
							i.item_description, i.container_size, i.bottles_per_case, i.vintage
						FROM [dbo].[$tableName] AS l
						LEFT JOIN mhw_app_prod_item i
						ON i.item_id = l.itemID
						LEFT JOIN [dbo].[mhw_app_prod] p WITH (NOLOCK) 
						ON p.[product_id] = i.[product_id]
						WHERE l.ID IN (
							SELECT ID FROM (
								SELECT itemID, tfa_os_price_state, MAX(ID) AS ID
								FROM [dbo].[$tableName]
								$whereMonth 
								GROUP BY itemID, tfa_os_price_state
							) SUBT
						)
						AND p.[client_name] = '".$_POST['client']."'
						$where
						ORDER BY i.item_description, tfa_os_price_state DESC
						";
		}else{

			$tableName = $_POST['view_state'];
			//$where = "AND l.".$tableName."_change_type <> 'Delete' ";
			if ($_POST['view_type']=='Deleted Items' ) {
				$whereArr[] = "l.deleted = 1";
			} else{
				$whereArr[] = "ISNULL(l.deleted, 0) <> 1 ";
	}
			$where = "AND ".implode($whereArr, " AND ");
			$tsql = "SELECT *,  convert(varchar, created_at, 120) AS created, 
							i.item_description, i.container_size, i.bottles_per_case, i.vintage
						FROM [dbo].[$tableName] AS l
						LEFT JOIN mhw_app_prod_item i
						ON i.item_id = l.itemID
						LEFT JOIN [dbo].[mhw_app_prod] p WITH (NOLOCK) 
						ON p.[product_id] = i.[product_id]
						WHERE l.ID IN (
							SELECT ID FROM (
								SELECT itemID, MAX(ID) AS ID
								FROM [dbo].[$tableName] 
								$whereMonth 
								GROUP BY itemID
							) SUBT
						)
						AND p.[client_name] = '".$_POST['client']."'
						$where
						ORDER BY i.item_description DESC
						";
		};
	}
}

if ($_POST['qry_type']=='blr-view') {

	/*
	$tsql = "SELECT s.[blr_state_abbrev], s.[blr_state_name], s.[blr_state_note],
				p.[product_id], p.[product_desc],
				p.[brand_name], p.[product_mhw_code], p.[product_desc],
				p.[TTB_ID], p.[federal_type],
				b.[blr_id], b.[client_name],
				b.[blr_contact], b.[blr_email], b.[blr_phone],
				b.[blr_reg_type], b.[blr_reg_sub_type], b.[blr_phone],
				c.[Status]
			FROM [dbo].[mhw_app_blr] b
			LEFT JOIN [dbo].[mhw_app_blr_states] s
				ON s.[blr_id] = b.[blr_id]
			LEFT JOIN [dbo].[mhw_app_prod] p
				ON p.[product_id] = b.[product_id]
			LEFT JOIN [dbo].[sc_cola_state_sts] c
				ON c.[cola] = p.[TTB_ID]
				AND c.[client] = p.[client_code]
				AND c.[State] = s.[blr_state_name]
			WHERE p.[product_id] IS NOT NULL
				AND b.[client_name] = '".$_POST['client']."'
	";
	*/

	if (empty($_POST['view_type']) || empty($_POST['client']) ) {
		echo("Wrong parameters !"); exit;
	}

	if ($_POST['view_type'] === 'State View') {

		$tsql = "SELECT DISTINCT
				s.[blr_state_abbrev], s.[blr_state_name]
			FROM [dbo].[mhw_app_blr_states] s
			LEFT JOIN [dbo].[mhw_app_blr] b
				ON s.[blr_id] = b.[blr_id]
			LEFT JOIN [dbo].[mhw_app_prod] p
				ON p.[product_id] = b.[product_id]
			WHERE p.[product_id] IS NOT NULL
				AND b.[client_name] = '".$_POST['client']."'
		";

	} else if ($_POST['view_type'] === 'Product View') {

		$tsql = "SELECT DISTINCT
				p.[product_id], p.[product_desc],
				p.[brand_name], p.[product_mhw_code],
				p.[TTB_ID], p.[federal_type], p.[client_code]
			FROM [dbo].[mhw_app_blr] b
			LEFT JOIN [dbo].[mhw_app_prod] p
				ON p.[product_id] = b.[product_id]
			WHERE p.[product_id] IS NOT NULL
				AND b.[client_name] = '".$_POST['client']."'
		";

	} else {
		echo("Wrong parameters !"); exit;
	}

}

if ($_POST['qry_type']=='blr-view-expand') {

	if (empty($_POST['view_type']) || empty($_POST['client']) || empty($_POST['key'])) {
		echo("Wrong parameters !"); exit;
	}

	if ($_POST['view_type'] === 'State View') {

		$tsql = "SELECT s.[blr_state_note],
				s.[blr_distributor_id], s.[blr_state_id],
				p.[product_id], p.[product_desc],
				p.[brand_name], p.[product_mhw_code],
				p.[TTB_ID], p.[federal_type], p.[client_code],
				b.[blr_id], b.[blr_confirmation], b.[client_name],
				b.[blr_contact], b.[blr_email], b.[blr_phone],
				b.[blr_reg_type], b.[blr_reg_sub_type],
				c.[Status]
			FROM [dbo].[mhw_app_blr] b
			LEFT JOIN [dbo].[mhw_app_prod] p
				ON p.[product_id] = b.[product_id]
			LEFT JOIN [dbo].[mhw_app_blr_states] s
				ON s.[blr_id] = b.[blr_id]
			LEFT JOIN [dbo].[sc_cola_state_sts] c
				ON c.[cola] = p.[TTB_ID]
				AND c.[client] = p.[client_code]
				AND c.[State] = s.[blr_state_name]
			WHERE p.[product_id] IS NOT NULL
				AND s.[blr_state_abbrev] = '".$_POST['key']."'
				AND b.[client_name] = '".$_POST['client']."'
		";

	} else if ($_POST['view_type'] === 'Product View') {

			$tsql = "SELECT
				s.[blr_state_abbrev], s.[blr_state_name], s.[blr_state_note],
				s.[blr_distributor_id], s.[blr_state_id],
				b.[blr_id], b.[blr_confirmation], b.[client_name],
				b.[blr_contact], b.[blr_email], b.[blr_phone],
				b.[blr_reg_type], b.[blr_reg_sub_type],
				c.[Status]
			FROM [dbo].[mhw_app_blr_states] s
			LEFT JOIN [dbo].[mhw_app_blr] b
				ON s.[blr_id] = b.[blr_id]
			INNER JOIN [dbo].[mhw_app_prod] p
				ON p.[product_id] = b.[product_id]
			LEFT JOIN [dbo].[sc_cola_state_sts] c
				ON c.[cola] = p.[TTB_ID]
				AND c.[client] = b.[client_code]
				AND c.[State] = s.[blr_state_name]
			WHERE b.[product_id] = '".$_POST['key']."'
				AND b.[client_name] = '".$_POST['client']."'
		";

	} else {
		echo("Wrong parameters !"); exit;
	}
}

if ($_POST['qry_type']=='blr-refresh') {

	$refreshStatusUrl = "https://mhwltd.app/sc_api/curl_sc_GetFederalRegistrationStatusByCola.php";
	$refreshStatusUrl .= "?output=responseonly";
	$refreshStatusUrl .= "&lkID=".urlencode($_POST['lkID']);
	$refreshStatusUrl .= "&ccID=".urlencode($_POST['ccID']);
	$ch = curl_init(); 
	curl_setopt($ch, CURLOPT_URL, $refreshStatusUrl); 
	//curl_setopt($ch, CURLOPT_POST, count($fields));
	//curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
	//return the transfer as a string 
	//curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	// $output contains the output string 
	$output = curl_exec($ch); 
	// close curl resource to free up system resources 
	curl_close($ch);      
	echo $refreshStatusUrl." - ".$output; exit;
}

function utf8ize($d) {
	if (is_array($d)) {
		foreach ($d as $k => $v) {
			$d[$k] = utf8ize($v);
		}
	} else if (is_string ($d)) {
		return utf8_encode($d);
	}
	return $d;
}

//echo( $tsql);exit;
$getResults= sqlsrv_query($conn, $tsql);
//echo ("Reading data from table" . PHP_EOL);
if ($getResults == FALSE)
	echo (sqlsrv_errors());

/* Processing query results */

/* Setup an empty array */
$json = array();
/* Iterate through the table rows populating the array */
do {
     while ($row = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
     $json[] = utf8ize($row);
     }
} while ( sqlsrv_next_result($getResults) );
 

//$json = utf8ize($json);

/* Run the tabular results through json_encode() */
/* And ensure numbers don't get cast to strings */
echo json_encode($json);

/* Free statement and connection resources. */
sqlsrv_free_stmt( $getResults);
sqlsrv_free_stmt( $trkResults);
sqlsrv_close( $conn);




?>