<?php
//session_start();

ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
//error_reporting(E_ALL);
include ('Classes/PHPExcel/IOFactory.php');

include("sessionhandler.php");

include("prepend.php");

//=============settings============
include("settings.php");

$pageref = str_replace(".php","",substr(__FILE__,strrpos(__FILE__,"\\")+1));


$clientlist =  "'".str_replace(";","','",$_SESSION['mhwltdphp_userclients'])."'";
$curr_client_name=$_SESSION['mhwltdphp_userclients'];
$curr_client_code=$_SESSION['mhwltdphp_user_clients_codes'];
$user_id=$_SESSION['mhwltdphp_user'];

//=============settings============

//=============functions============
include("functions.php");
//=============functions============



if(!isset($_SESSION['mhwltdphp_user'])){

	header("Location: login.php");
}
else{
	include("head.php");
	$sample_file_link=$siteroot."/bulk_uploaded_files/bulk_template_data.xlsx";
	$sample_empty_file_link=$siteroot."/bulk_uploaded_files/bulk_empty_template.xlsx";
	
	include("dbconnect.php");
	$conn = sqlsrv_connect($serverName, $connectionOptions);
	if( $conn === false) {
			die( print_r( sqlsrv_errors(), true));
	}
	
	
			
 /************************ Fetch client information : start ***********************/	
	$client_sql= "SELECT * FROM [vw_Clients] where client_name<>'' order by client_name";
	$client_stmt = sqlsrv_query( $conn, $client_sql);
	$client_data=array();
	$client_data_by_name=array();
	
	if ($client_stmt == FALSE)
		echo (sqlsrv_errors());

	while ($row_client = sqlsrv_fetch_array($client_stmt, SQLSRV_FETCH_ASSOC)) {
		$c_code=trim($row_client['client_code']);
		$c_name=trim($row_client['client_name']);
		$client_data[$c_code]=$c_name;	
		$client_data_by_name[$c_name]=$c_code;
	}	
	sqlsrv_free_stmt($client_stmt);
	
 /************************ Fetch client information : end ***********************/	
	
	$err_msg="";
	$succ_msg="";
	$uploadOk=1;
    $size_kb_limit=2048;
	$pageerr="";
	$validext = array(".xls",".xlsx");
	$p_empty_count=0;
	$new_product_info=array();
	
	$first_header_arr=['Product Information',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Item Information',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL];
	
	echo "<div class=\"container-fluid\">";
	echo "<div id=\"".$pageref."\">";
	//=============file upload processing============
	if(isset($_POST['bulkuploadform'])){
			
	$allowedFileType = [
        'application/vnd.ms-excel',
        'text/xls',
        'text/xlsx',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    ];
	
	if(isset($size_kb_limit)){
			$size = $_FILES['bulkfiletoupload']['size']/1024;
			//Make sure that file size is correct
			if($size > $size_kb_limit){
				$pageerr="Upload Failure: File size is too large. File must be under ".$size_kb_limit." KB\n";
					$uploadOk=0;
			}
		}
	
	if ($uploadOk==1) {
		//check file extension
		$ext = strrchr($_FILES['bulkfiletoupload']['name'],'.');
		$ext = strtolower($ext);
		if((!in_array($_FILES["bulkfiletoupload"]["type"], $allowedFileType)) || (!in_array($ext,$validext))){
			$pageerr="Upload Failure: File uploads of this type ".$ext." are not allowed\n";
			$uploadOk=0;
		}
	}
	
	
	if ($uploadOk==1) {
         $temp = explode(".", $_FILES["bulkfiletoupload"]["name"]);
		 $f_type=$_FILES["bulkfiletoupload"]["type"];
		 $file_first_part=$temp[0]."_".date('YmdHis').mt_rand(10000,99999);
		 $newfilename = $file_first_part.'.' . end($temp);		
		 move_uploaded_file($_FILES["bulkfiletoupload"]["tmp_name"], "bulk_uploaded_files/" . $newfilename);
		 
		 
			$uploaded_by=$_SESSION['mhwltdphp_user'];
			$ori_name=$temp[0];
			
		
         /*  *********  Replacing special character,double code to empty : start  **************** */		
function trim_characters_str($raw_str)
			 {
				 $search_str  = array('è','é','ì','â','ò','"',"'","`",",");
                 $replace_str = array('e','e','i','a','o','',"","","");
                 return str_replace($search_str, $replace_str, trim($raw_str));
			 }	
			 
        /*  *********  Replacing special character,double code to empty : end  **************** */
		
		 
		 /* Item description generation function : start   */
		 
		 
function generate_item_desc_self($federal_type,$container_type,$container_size,$stock_uom,$outer_shipper,$brand_product_desc,$vintage,$container_per_case)
        {
			
				$mhwitmdesc = "";
				$mhwitmdesc1 = "";
				$mhwitmdesc2 = "";
				$mhwitemdesc2LEN = 0;
				$prodfedtype = $federal_type;
				$itmcontainertype = $container_type;
				$itmcontainersize = $container_size;
				$itmstockuom = $stock_uom;
				$itmoutershipper = $outer_shipper;
				$lenNotTaken = 60;
				$itmclientdesc = $brand_product_desc;
				$itmvintage = $vintage;
				$itmbottlespercase = $container_per_case;
				
				/* strip trailing zeros from size (covers last char of double zero and trailing zero from decimal values) */
				
				$itmcontainersize = str_replace(".00 Lit","L",$itmcontainersize);
				$itmcontainersize = str_replace(".00 Gal","G",$itmcontainersize);
				$itmcontainersize = str_replace(".00 NA","NA",$itmcontainersize);
				$itmcontainersize = str_replace(".00 ML","ML",$itmcontainersize);
				$itmcontainersize = str_replace(".00 OZ","OZ",$itmcontainersize);
				
				/* strip spaces between size and units */
				
				$itmcontainersize = str_replace(" ","",$itmcontainersize);
				
				/* strip zero right after decimal (covers first char of double zero, while retaining decimals with zero before final non-zero character) */
				
				$itmcontainersize = str_replace(".0Lit",".L",$itmcontainersize);
				$itmcontainersize = str_replace(".0Gal",".G",$itmcontainersize);
				$itmcontainersize = str_replace(".0L",".L",$itmcontainersize);
				$itmcontainersize = str_replace(".0G",".G",$itmcontainersize);
				$itmcontainersize = str_replace(".0NA",".NA",$itmcontainersize);
				$itmcontainersize = str_replace(".0ML",".ML",$itmcontainersize);
				$itmcontainersize = str_replace(".0OZ",".OZ",$itmcontainersize);
				
				/* abbreviate units if not already done during zero handling */
				
				$itmcontainersize = str_replace(".Lit","L",$itmcontainersize); 
				$itmcontainersize = str_replace(".Gal","G",$itmcontainersize);
				$itmcontainersize = str_replace(".L","L",$itmcontainersize);
				$itmcontainersize = str_replace(".G","G",$itmcontainersize);
				$itmcontainersize = str_replace(".NA","NA",$itmcontainersize);
				$itmcontainersize = str_replace(".ML","ML",$itmcontainersize);
				$itmcontainersize = str_replace(".OZ","OZ",$itmcontainersize);
				
				/* abbreviate units if not already done during zero handling */
				
				$itmcontainersize = str_replace("Lit","L",$itmcontainersize);
				$itmcontainersize = str_replace("Gal","G",$itmcontainersize);


				if($itmstockuom && $itmstockuom!='')
				{
				$itmstockuom4 =substr($itmstockuom,0,4);
				$itmstockuom = strtoupper($itmstockuom);
				$itmstockuom4 = strtoupper($itmstockuom4);
				}

				
				
				if(strtoupper($itmcontainertype)=='BOTTLE'){
					if($itmstockuom=='CASE' || $itmstockuom4=='CASE'){
						if(strtoupper($prodfedtype)=="WINE"){
							$mhwitmdesc2 = $itmvintage.' '.$itmcontainersize .'-'.$itmbottlespercase;
						}
						else if(strtoupper($prodfedtype)=="DISTILLED SPIRITS" && $itmvintage!=''){
							$mhwitmdesc2 = $itmvintage.' '.$itmcontainersize.'-'.$itmbottlespercase;
						}
						else{
							$mhwitmdesc2 = $itmcontainersize.'-'.$itmbottlespercase;
						}
					}
					else if(strtoupper($itmstockuom)=='BOTTLE'){
						if(strtoupper($prodfedtype)=="WINE"){
							$mhwitmdesc2 = $itmvintage.' '.$itmcontainersize .'-BT';
						}
						else if(strtoupper($prodfedtype)=="DISTILLED SPIRITS" && $itmvintage!=''){
							$mhwitmdesc2 = $itmvintage.' '.$itmcontainersize.'-BT';
						}
						else{
							$mhwitmdesc2 = $itmcontainersize.'-BT';
						}
					}

					if(strtoupper($itmoutershipper)=='WOOD'){ $mhwitmdesc2 = $mhwitmdesc2.' WOOD'; }

					$mhwitemdesc2LEN = strlen($mhwitmdesc2);
					$mhwitmdesc1 =substr($itmclientdesc,0,59-$mhwitemdesc2LEN);					
					$mhwitmdesc = $mhwitmdesc1.' '.$mhwitmdesc2;
					$lenNotTaken = 59-$mhwitemdesc2LEN; 
				}
				else if(strtoupper($itmcontainertype)=='CAN'){
					if($itmstockuom=='CASE' || $itmstockuom4=='CASE'){
						if(strtoupper($prodfedtype)=="WINE"){
							$mhwitmdesc2 = $itmvintage.' '.$itmcontainersize.' CAN-'.$itmbottlespercase;
						}
						else if(strtoupper($prodfedtype)=="DISTILLED SPIRITS" && $itmvintage!=''){
							$mhwitmdesc2 = $itmvintage.' '.$itmcontainersize.' CAN-'.$itmbottlespercase;
						}
						else{
							$mhwitmdesc2 = $itmcontainersize.' CAN-'.$itmbottlespercase;
						}
					}
					else if(strtoupper($itmstockuom)=='BOTTLE'){
						if(strtoupper($prodfedtype)=="WINE"){
							$mhwitmdesc2 = $itmvintage.' '.$itmcontainersize.' CAN';
						}
						else if(strtoupper($prodfedtype)=="DISTILLED SPIRITS" && $itmvintage!=''){
							$mhwitmdesc2 = $itmvintage.' '.$itmcontainersize.' CAN';
						}
						else{
							$mhwitmdesc2 = $itmcontainersize.' CAN';
						}
					}

					if(strtoupper($itmoutershipper)=='WOOD'){ $mhwitmdesc2 = $mhwitmdesc2.' WOOD'; }

					$mhwitemdesc2LEN = strlen($mhwitmdesc2);
					$mhwitmdesc1 = substr($itmclientdesc,0,59-$mhwitemdesc2LEN);
					$mhwitmdesc = $mhwitmdesc1.' '.$mhwitmdesc2;
					$lenNotTaken = 59-$mhwitemdesc2LEN; 
				}
				else if(strtoupper($itmcontainertype)=='KEG' || strtoupper($itmcontainertype)=='KEG-ONEWAY' || strtoupper($itmcontainertype)=='KEG-DEPOSIT'){
					$mhwitmdesc2 = $itmcontainersize.' KEG';
					$mhwitemdesc2LEN = strlen($mhwitmdesc2);
					$mhwitmdesc1 = substr($itmclientdesc,0,59-$mhwitemdesc2LEN);
					$mhwitmdesc = $mhwitmdesc1 .' '.$mhwitmdesc2;
					$lenNotTaken = 59-$mhwitemdesc2LEN; 
				}
				else if(strtoupper($itmcontainertype)=='OTHER'){
					$mhwitmdesc2 = $itmcontainersize.' '.$itmcontainertype;
					$mhwitemdesc2LEN = strlen($mhwitmdesc2);
					$mhwitmdesc1 = substr($itmclientdesc,0,59-$mhwitemdesc2LEN);
					$mhwitmdesc = $mhwitmdesc1.' '.$mhwitmdesc2;
					$lenNotTaken = 59-$mhwitemdesc2LEN;
				}
				else{
					$mhwitmdesc = $itmclientdesc;
					$lenNotTaken = 60-$mhwitemdesc2LEN;
				}
				
			return $mhwitmdesc;
    }			
	
		
		 /* Item description generation function  : end */
		 
		 /*  searching value in array function : start */
		 
		 function search_val_in_arr($array, $key, $val) {
                 $result_arr=array();			 
				foreach ($array as $item)
				{
					if (isset($item[$key]) && $item[$key] == $val)
						$result_arr=$item;
				}	
				return $result_arr;
			}

		 /*  searching value in array function : end */
			
		 
		 /***************************************** process uploaded file : start  *************************************/
		
		 $inputFileName = "bulk_uploaded_files/".$newfilename;
		
		//  Read your Excel workbook
		try {
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}

		//  Get worksheet dimensions
		//getHighestDataColumn  getHighestDataRow
		$sheet = $objPHPExcel->getSheet(0); 
		//$highestRow = $sheet->getHighestRow(); 
		$highestRow = $sheet->getHighestDataRow();
		$highestColumn = $sheet->getHighestDataColumn();
		
		
		/* ************** validate row and column min and max length  ************** */
		$data_okay=1;
		
		if($highestColumn!='AL')
		{
			$err_msg=$err_msg."|| template column number not matched";
			$data_okay=0;
		}
		
		if($highestRow<3)
		{
			$err_msg=$err_msg."|| template minimum rows not found";
			$data_okay=0;
		}
		
		if($highestRow>2000)
		{
            $err_msg=$err_msg."|| Maximum row number will be 2000";	
            $data_okay=0;			
		}
		
		
         if($data_okay==1)
		 {
		$full_sheet_data=array();
		
		//  Loop through each row of the worksheet in turn
		for ($row = 1; $row <= $highestRow; $row++){ 
		
		    //  Read a row of data into an array
			$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
											NULL,
											TRUE,
											FALSE);
			//  Insert row data array into your database of choice here
			$full_sheet_data[$row]=$rowData[0];
		
		    /* ***********  check first header row *********************  */
			if($row==1)
			{				
				if($first_header_arr !== $rowData[0])
				{
					$err_msg=$err_msg."|| First header Row not matched with template";	
					$data_okay=0;
				}
                					
			}
			else if($row>1 && $data_okay==1)
			{
				
			}
			
		}
		
		
        /* checking client code : start */
		//$tem_client_code=trim_characters_str($full_sheet_data[3][0]);
		
		$tem_client_code=trim_characters_str($_POST['selected_client_name']);
		
		
		if($tem_client_code!=$curr_client_code)
		{
			//$err_msg=$err_msg."|| Client Code does not match with the selected Client Code";	
			//$data_okay=0;
		}
				
		
		/* checking client code : end */
		 
		 /* process row data : Start  */
		 if($data_okay==1)
		 {
		 foreach($full_sheet_data as $row_key=>$row_data)
		 {
			 			 			
			if($row_key>2)
			{				
				//$client_code=trim_characters_str($row_data[0]);
				$client_code=trim_characters_str($tem_client_code);
				$brand_name=trim_characters_str($row_data[1]);
				
				$brand_product_desc=trim_characters_str($row_data[2]);
				$product_desc=trim_characters_str(str_replace($brand_name,"",$brand_product_desc));
				$product_code=trim_characters_str($row_data[3]);
				$TTB_ID=trim_characters_str($row_data[4]);
				$federal_type=trim_characters_str($row_data[5]);
				$compliance_type=trim_characters_str($row_data[6]);
				$product_classification=trim_characters_str($row_data[7]);
				$mktg=trim_characters_str($row_data[8]);
				$beverage_type=trim_characters_str($row_data[9]);
				$varietal=trim_characters_str($row_data[10]);
				$fanciful_name=trim_characters_str($row_data[11]);
				$origin_country=trim_characters_str($row_data[12]);
				$appellation=trim_characters_str($row_data[13]);				
				$lot_item=trim_characters_str($row_data[14]);
				$alcohol=trim_characters_str($row_data[15]);
				
				if($alcohol!='')
				{
					$alcohol=floatval($alcohol);
				}
				if($alcohol!='' && $alcohol<1)
				{
					$alcohol=$alcohol*100;
				}
				
				
				$client_item_code=trim_characters_str($row_data[16]);
				$client_item_code_option=trim_characters_str($row_data[17]);
				$container_type=trim_characters_str($row_data[18]);
				$container_size=trim_characters_str($row_data[19]);
				$container_material=trim_characters_str($row_data[20]);
				
				
				$stock_uom=trim_characters_str($row_data[21]);
				$container_per_case=trim_characters_str($row_data[22]);
				$vintage=trim_characters_str($row_data[23]);
				$chill_storage=trim_characters_str($row_data[24]);
				$outer_shipper=trim_characters_str($row_data[25]);
				
				$item_class=trim_characters_str($row_data[26]);
				$case_height=trim_characters_str($row_data[27]);
				$case_length=trim_characters_str($row_data[28]);
				$case_width=trim_characters_str($row_data[29]);
				$case_unit_dimensions=trim_characters_str($row_data[30]);
				
				$case_weight=trim_characters_str($row_data[31]);
				$weight_uom=trim_characters_str($row_data[32]);
				$case_per_pallet=trim_characters_str($row_data[33]);
				$case_per_layer=trim_characters_str($row_data[34]);
				$layer_per_pallet=trim_characters_str($row_data[35]);
				
				$upc=trim_characters_str($row_data[36]);
				$scc=trim_characters_str($row_data[37]);
				
				$item_mhw_code='';
				
				if(strtoupper($client_item_code_option)=="YES")
				{ 
			      $item_mhw_code = $client_item_code; 
			    }
				
				$item_desc_details="";
				$item_desc_details=generate_item_desc_self($federal_type,$container_type,$container_size,$stock_uom,$outer_shipper,$brand_product_desc,$vintage,$container_per_case);
                
				/* If product code is provided then check valid product code : start  */
				$temp_product_id=0;
				$temp_product_code='';
				$prodID = 0; 
				$prodCODE =''; 
				$itemID=0;
				
			if($product_code!="")
				{
				$product_code_sql="select top 1 product_id,client_code,product_mhw_code from mhw_app_prod where product_mhw_code='$product_code' and client_code='$client_code'";
				$product_code_data = sqlsrv_query($conn, $product_code_sql);

				if ($product_code_data == FALSE)
					echo (sqlsrv_errors());

				while ($row_product_code_data = sqlsrv_fetch_array($product_code_data, SQLSRV_FETCH_ASSOC)) {
					$temp_product_id=$row_product_code_data['product_id'];
					$temp_product_code=$row_product_code_data['product_mhw_code'];							
				}	
				sqlsrv_free_stmt($product_code_data);
				
				if($temp_product_id==0)
				{
					$err_msg=$err_msg."|| Product ID not found at Row no : $row_key";
				}
				else if($temp_product_id>0)
				{
					
				   /* item creation : start */
				   
				   $tfa_item_description='';
				   $tfa_various_vintages='';
				
			
								
				
				
				 $tsql_item= "EXEC [dbo].[usp_product_item_draft] 
				 @product_id =".$temp_product_id."
				,@item_client_code = '".$client_item_code."'
				,@item_mhw_code = '".$item_mhw_code."'
				,@item_description = '".$item_desc_details."'
				,@container_type = '".$container_type."'
				,@container_size = '".$container_size."'
				,@stock_uom = '".$stock_uom."'
				,@bottles_per_case = ".str_replace("'","",str_replace(",","",$container_per_case))."
				,@upc = '".str_replace("'","",str_replace(",","",$upc))."'
				,@scc = '".str_replace("'","",str_replace(",","",$scc))."'
				,@vintage  = '".str_replace("'","",str_replace(",","",$vintage))."'
				,@various_vintages = '".$tfa_various_vintages."'
				,@height = '".str_replace("'","",str_replace(",","",$case_height))."'
				,@length = '".str_replace("'","",str_replace(",","",$case_length))."'
				,@width = '".str_replace("'","",str_replace(",","",$case_width))."'
				,@weight = '".str_replace("'","",str_replace(",","",$case_weight))."'
				,@item_status = 'A'
				,@create_via = 'bulk_import'
				,@username = '".$user_id."'
				,@client_description = '".convert_ascii(trim_characters_str($tfa_item_description))."'
				,@chill = '".$chill_storage."'
				,@bottle_material = '".$container_material."'
				,@outer_shipper = '".$outer_shipper."'
				,@pallet_cases = '".$case_per_pallet."'
				,@pallet_layers_cases = '".$case_per_layer."'
				,@pallet_layers = '".$layer_per_pallet."'
				,@item_class = '".$item_class."'
				,@unit_dimensions = '".$case_unit_dimensions."'
				,@weight_uom = '".$weight_uom."'
				"; 
				
			if($item_desc_details && $item_desc_details!='' && $item_desc_details!=null)
			{
				$stmt_item = sqlsrv_query($conn, $tsql_item);
					
				while ($row_item = sqlsrv_fetch_array($stmt_item, SQLSRV_FETCH_ASSOC)) {
					$itemID = $row_item['itemID']; 
				}
				
				if( $stmt_item === false ) {
					if( ($errors = sqlsrv_errors() ) != null) {
						foreach( $errors as $error ) {
							$errorlog.= $tsql_item."\n";
							$errorlog.= "SQLSTATE: ".$error[ 'SQLSTATE']."\n";
							$errorlog.= "code: ".$error[ 'code']."\n";
							$errorlog.= "message: ".$error[ 'message']."\n";
						}
					}
					$ITMerrorFree=0;
					$errorFree=0;
				} 
				
				sqlsrv_free_stmt($stmt_item);
			}			
				   /* item creation : end */
					
					
					
				}
					
	           
	           /* If product code is provided then check valid product code : end  */
			  }
			  else 
			  {
				  /*  Create new product */
				  
				        $client_name=$client_data[$client_code];
						$product_mhw_code_search='';
							
				  	    $product_add_tsql= "EXEC [dbo].[usp_product_draft] 
						 @product_id = ".$temp_product_id."
						,@client_code = '".$client_code."'
						,@client_name = '".$client_name."'
						,@brand_name = '".$brand_name."'
						,@product_desc = '".$brand_product_desc."'
						,@product_mhw_code = '".$product_code."'
						,@product_mhw_code_search = '".$product_mhw_code_search."'
						,@TTB_ID = '".$TTB_ID."'
						,@federal_type = '".$federal_type."'
						,@compliance_type = '".$compliance_type."'
						,@product_class = '".$product_classification."'
						,@mktg_prod_type = '".$mktg."'
						,@bev_type = '".$beverage_type."'
						,@fanciful = '".$fanciful_name."'
						,@country = '".$origin_country."'
						,@appellation = '".$appellation."'
						,@lot_item = '".$lot_item."'
						,@varietal = '".$varietal."'
						,@alcohol_pct = '".$alcohol."'
						,@create_via  = 'bulk_import'
						,@username  = '".$user_id."'";
						
			/* checking if already product created : start */
			$eligible_new_product=1;
			
			if($p_empty_count>0)
			{
				$existed_product_info=array();
				$existed_product_info=search_val_in_arr($new_product_info,'product_brand_des',$brand_product_desc);
				if(count($existed_product_info)>0)
				{
					$prodID=$existed_product_info['product_id'];
					$prodCODE=$existed_product_info['product_code'];
					$eligible_new_product=0;
				}
			}
            /* checking if already product created : end */
			
				/* product creation execution : start  */
			if($client_code!="" && $client_name!="" && $brand_name!="" && $eligible_new_product>0)	
			{
				$product_add_stmt = sqlsrv_query($conn, $product_add_tsql);
		
				while ($row = sqlsrv_fetch_array($product_add_stmt, SQLSRV_FETCH_ASSOC)) {
					$prodID = $row['prodID']; 
					$prodCODE = $row['prodCODE']; 
					$p_empty_count=$p_empty_count+1;
				    $new_product_info[$row_key]=array('product_id'=>$prodID,'product_brand_des'=>$brand_product_desc,'product_code'=>$prodCODE);				   
				} 		
				
				if( $product_add_stmt === false ) {
					if( ($errors = sqlsrv_errors() ) != null) {
						foreach( $errors as $error ) {
							$errorlog.= $product_add_tsql."\n";
							$errorlog.= "SQLSTATE: ".$error[ 'SQLSTATE']."\n";
							$errorlog.= "code: ".$error[ 'code']."\n";
							$errorlog.= "message: ".$error[ 'message']."\n";
						}
					}
					$errorFree=0;
				}
				sqlsrv_free_stmt($product_add_stmt);
			}
			
                /* product creation execution : end  */	
				

               if($prodID>0)
			   {
				   
				   /* item creation : start */
				   
				   $tfa_item_description='';
				   $tfa_various_vintages='';
				
				//$item_class=trim_characters_str($row_data[26]);				
				//$case_unit_dimensions=trim_characters_str($row_data[30]);
				//$weight_uom=trim_characters_str($row_data[32]);
				
				
				 $tsql_item= "EXEC [dbo].[usp_product_item_draft] 
				 @product_id =".$prodID."
				,@item_client_code = '".$client_item_code."'
				,@item_mhw_code = '".$item_mhw_code."'
				,@item_description = '".$item_desc_details."'
				,@container_type = '".$container_type."'
				,@container_size = '".$container_size."'
				,@stock_uom = '".$stock_uom."'
				,@bottles_per_case = ".str_replace("'","",str_replace(",","",$container_per_case))."
				,@upc = '".str_replace("'","",str_replace(",","",$upc))."'
				,@scc = '".str_replace("'","",str_replace(",","",$scc))."'
				,@vintage  = '".str_replace("'","",str_replace(",","",$vintage))."'
				,@various_vintages = '".$tfa_various_vintages."'
				,@height = '".str_replace("'","",str_replace(",","",$case_height))."'
				,@length = '".str_replace("'","",str_replace(",","",$case_length))."'
				,@width = '".str_replace("'","",str_replace(",","",$case_width))."'
				,@weight = '".str_replace("'","",str_replace(",","",$case_weight))."'
				,@item_status = 'A'
				,@create_via = 'bulk_import'
				,@username = '".$user_id."'
				,@client_description = '".convert_ascii(trim_characters_str($tfa_item_description))."'
				,@chill = '".$chill_storage."'
				,@bottle_material = '".$container_material."'
				,@outer_shipper = '".$outer_shipper."'
				,@pallet_cases = '".$case_per_pallet."'
				,@pallet_layers_cases = '".$case_per_layer."'
				,@pallet_layers = '".$layer_per_pallet."'
				,@item_class = '".$item_class."'
				,@unit_dimensions = '".$case_unit_dimensions."'
				,@weight_uom = '".$weight_uom."'
				"; 
				
				
				if($item_desc_details && $item_desc_details!='' && $item_desc_details!=null)
				{
				$stmt_item = sqlsrv_query($conn, $tsql_item);
					
				while ($row_item = sqlsrv_fetch_array($stmt_item, SQLSRV_FETCH_ASSOC)) {
					$itemID = $row_item['itemID']; 
				}
				
				if( $stmt_item === false ) {
					if( ($errors = sqlsrv_errors() ) != null) {
						foreach( $errors as $error ) {
							$errorlog.= $tsql_item."\n";
							$errorlog.= "SQLSTATE: ".$error[ 'SQLSTATE']."\n";
							$errorlog.= "code: ".$error[ 'code']."\n";
							$errorlog.= "message: ".$error[ 'message']."\n";
						}
					}
					$ITMerrorFree=0;
					$errorFree=0;
				} 
				
				sqlsrv_free_stmt($stmt_item);
				}		
				   /* item creation : end */
			   }				   
				  
			  }
			}
         			
		 }		 
		 } 		 
		//exit;		 
		 /* process row data : End  */
		 
		 
		 if($data_okay==1)
		  {
		 //$client_code=$full_sheet_data[3][0];		 
		 $client_code=$_POST['selected_client_name'];
		 $final_file_name=$file_first_part."_".$client_code.'.' . end($temp);	 
		 rename("bulk_uploaded_files/" . $newfilename, "bulk_uploaded_files/" .$final_file_name);
		 
		 /***************************************** process uploaded file : end  *************************************/
		 
		 $tsql_file="insert into mhw_app_bulk_files (client_code,uploaded_file_name,stored_file_name,file_type,create_via,created_by,create_date,edit_date,active,deleted,is_validated)
		 values ('$client_code','$ori_name','$final_file_name','$f_type','bulk_import','$uploaded_by',getdate(),getdate(),1,0,1)";
		
	     $stmt_img = sqlsrv_query($conn, $tsql_file);
		 $succ_msg="File has been uploaded successfully";		 
		  }
		 
		 
		 }
	}
	else 
	{
		//$err_msg = $err_msg. "Invalid File Type. Please upload valid Excel File.";
		$err_msg=$pageerr;
	}

	}

	//=============file upload processing============
     $is_proceed=1; 
	//=============file upload form============
	if($is_proceed==1){
		echo "<div class=\"row\">";
  		echo "<div class=\"col-7\">";
		
		echo "<div class=\"jumbotron jumbotron-fluid\">";
		  echo "<div class=\"container\">";
		    echo "<h3 >BULK PRODUCT & ITEM IMPORT : </h3>";
		    echo "<p class=\"lead\"> 
			Click <a href='".$sample_file_link."' target='_blank' >here</a> to download sample Bulk File Template with Data<br /><br/>
			Click <a href='".$sample_empty_file_link."' target='_blank' >here</a> to download empty Bulk File Template<br /><br/>";

				echo "Note each bulk file must fillup below criteria:";
				echo "<ul><li>File should be excel file (file extensions: .xlsx)</li>";
				echo "<li>Not exceed 5MB in size</li>";
				echo "<li>Product Description : Product Description must include brand name</li>";
				echo "<li>Product Code : For new products, Please leave BLANK, for existing products, please include PRODUCT ID found on the CRD</li>
				<li>TTB_ID : Please add a P if pending</li>
				<li>Client Item Code : Max 30 Characters.</li>
				<li>MHW Item Code : If Yes is chosen, MHW will use the Client Item Code listed (MHW prefix must be included).<br/>
				If NO is chosen, MHW will assign the item code.<br/>If both fields are left BLANK, MHW will assign the item code.</li>
				<li>Container Type : If the Container type is Can, please choose BOTTLE.</li>
				<li>Container Size : If item is POS Material, please choose 0.00 ML</li>
				<li>UPC : UPC must be between 12-13 digits.</li>
				<li>SCC : SCC codes must be 14 digit long.</li>
				</ul></p>";
		  echo "</div>";
		echo "</div>";
		
		echo "</div>";
  echo "<div class=\"col-5\">";

		
		echo "<div id=\"".$pageref."_form\">";
		echo "<div class=\"form-group\">";
			//echo "<h2>Add a File Attachment:</h2>";
			
			
            
            if(trim($succ_msg)!="" && trim($err_msg)!="")
			{
				echo "<br/><div class='alert alert-success' role='alert'>$succ_msg</div>";
				if (strpos($err_msg, '||') !== false) {
						$all_errs=explode("||",ltrim($err_msg,"||"));
						echo "<ul class='list-group'>";
						foreach($all_errs as $kr=>$vr)
						{
							echo "<li class='list-group-item list-group-item-danger'>$vr</li>";
						}
						echo "</ul>";
							}
							else 
							{
								echo "<br/><div class='alert alert-danger' role='alert'>$err_msg</div>";
							}

			}
            else if(trim($succ_msg)!="")
			{
				echo "<br/><div class='alert alert-success' role='alert'>$succ_msg</div>";
			}
	        else if(trim($err_msg)!="")
			{
					if (strpos($err_msg, '||') !== false) {
						$all_errs=explode("||",ltrim($err_msg,"||"));
						echo "<ul class='list-group'>";
						foreach($all_errs as $kr=>$vr)
						{
							echo "<li class='list-group-item list-group-item-danger'>$vr</li>";
						}
						echo "</ul>";
							}
							else 
							{
								echo "<br/><br/><div class='alert alert-danger' role='alert'>$err_msg</div>";
							}

				
			}
			
			echo "<form method=\"post\" enctype=\"multipart/form-data\" action=\"bulk_import.php\">";
				
			echo "<BR />";
			//echo "<input type=\"text\" name=\"prodID\" value=\"".$prodID."\">";
			
			echo "<div style='padding-bottom : 10px;' id=\"selected_cl_name\" class=\"pairlbl\">Client Name :  <span style=\"color:red;\">*</span>";
			  ?>
			  <select id="selected_client_name" name="selected_client_name" title="Client Name" aria-required="true">
				<?php
				$all_clients = explode(";",$_SESSION['mhwltdphp_userclients']);			
				foreach ($all_clients as &$clientvalue) {
					$curr_cl_code=$client_data_by_name[$clientvalue];
					echo "<option value='$curr_cl_code' class=''>$clientvalue</option>";
				}
				?>					
				</select>
				</div>			
			<?php 
			
			echo "<div id=\"img_filename_lbl\" class=\"pairlbl\">Upload Bulk File :  <span style=\"color:red;\">*</span></div>";
			echo "<input type=\"file\" accept=\".xlsx\"  class=\"form-control-file required\" name=\"bulkfiletoupload\" id=\"bulkfiletoupload\">";
			echo "<BR />";	
			
			echo "<div id=\"img_btns\">";
			echo "<input type=\"hidden\" name=\"action\" value=\"upload\">";
			
			 echo "<button type=\"submit\" name=\"bulkuploadform\" class=\"btn btn-primary bg_arrow_blue\">";
			 echo "<span class=\"oi oi-data-transfer-upload\" title=\"data-transfer-upload\" aria-hidden=\"true\"></span> Upload";
			 echo "</button>";

			echo "</div>";
		echo "</div>";
		echo "</form>";
		echo "</div><br><br>";
	}
	  echo "</div>";
echo "</div>";

echo "<div class=\"row\">";
echo "<div class=\"col-12\">";
	//=============list attached files============
		echo "<h2>Uploaded Bulk Files:</h2>";
		echo "<div id=\"attached\">";
		$tsql_file_list = "select file_id,client_code,stored_file_name,created_by,create_via,create_date,edit_date from mhw_app_bulk_files where is_validated=1 and client_code='$curr_cl_code' order by file_id desc";	  
		echo "<div class=\"container-fluid\"><table class=\"table table-bordered table-striped table-hover\"><thead><tr>
		  <th>File ID</th>
		  <th>Client Code</th>
          <th>Uploaded File Name</th>
          <th>Download</th>		  	
		  <th>Uploaded Date</th>
		  <th>Create Via</th>
          <th>Uploaded By</th>		  
        </tr></thead>
      	<tbody>";
		
		 $getResults_files= sqlsrv_query($conn, $tsql_file_list);
		 if ($getResults_files == FALSE)
	        echo (sqlsrv_errors());
		
		while ($row_each_file = sqlsrv_fetch_array($getResults_files, SQLSRV_FETCH_ASSOC)) {
					
				  $download_link=$siteroot."/bulk_uploaded_files/".$row_each_file['stored_file_name'];		
				  $up_date=$row_each_file['edit_date']->format('Y-m-d H:i:s');
				  
				  
				  echo "<tr>";
				  echo "<td>$row_each_file[file_id]</td>
				         <td>$row_each_file[client_code]</td>
						 <td>$row_each_file[stored_file_name]</td>
						 <td><a href='".$download_link."' target='_blank'>Download</a></td>
						 <td>$up_date</td>
						 <td>$row_each_file[create_via]</td>
						 <td>$row_each_file[created_by]</td>";
				 	
					echo "</tr>"; 
	    
				} //end loop
				
		
		echo "</tbody></table></div>";
			
		
		echo "</div>";
	echo "</div>";	
echo "</div>";	
echo "</div>";
}
?>


<script>

$(document).ready(function(){


});

</script>