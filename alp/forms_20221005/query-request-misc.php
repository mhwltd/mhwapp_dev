<?php
include('settings.php');
error_reporting(E_ALL); //displays an error

session_start();
if(!isset($_SESSION['mhwltdphp_user'])){
	die( "Not authenticated !" );  
}

//echo '<pre>'; print_r($_POST); echo '</pre>';
include("dbconnect-misc.php");

//Establishes the connection
$conn = sqlsrv_connect($serverName_misc, $connectionOptions_misc);
if( $conn === false) {
		die( print_r( sqlsrv_errors(), true));
}

// total_num pagination 
$pagination_total_num=0;
$tsql_total_num="";

if($_POST['qrytype']=='VPdefault'){
  if(isset($_POST['active_pagination']) && $_POST['active_pagination']=='yes')
	{
		// pagination custom searching 
		$filter_txt_qry="";
		if($_POST['filter_txt'] && $_POST['filter_txt']!='')
		{
			$filter_txt_val=$_POST['filter_txt'];
			$filter_txt_qry=" AND ( r.[ClientName] like '%".$filter_txt_val."%'  OR r.[Value1] like '%".$filter_txt_val."%' OR r.[Value2] like '%".$filter_txt_val."%'  OR r.[CustomerName] like '%".$filter_txt_val."%' 
			OR r.[Value3] like '%".$filter_txt_val."%' OR r.[OrderDate] like '%".$filter_txt_val."%'  OR r.[SlsLocn] like '%".$filter_txt_val."%' OR r.[OpsTeam] like '%".$filter_txt_val."%'
			OR r.[ShipToAddr] like '%".$filter_txt_val."%'  OR r.[City] like '%".$filter_txt_val."%' OR r.[State] like '%".$filter_txt_val."%'
			OR r.[Zip] like '%".$filter_txt_val."%' )";		
		}	
		
		// fetching total_num for pagination 
		$tsql_total_num = "SELECT count(r.[tcr_id]) as total_num FROM [dbo].[tray_compliance_review] r WITH (NOLOCK) WHERE isnull(r.active,1) > 0 $filter_txt_qry";
		
		
		/* BLR STATUS FEDERAL QUERY - DISPLAY EACH PRODUCT WITH OR WITHOUT APPROVAL */
		
        $per_page=($_POST['per_page_size']!='')?$_POST['per_page_size']:10;
		$start_from=($_POST['page_number']!='')?$_POST['page_number']:0;
		$start_from=$start_from-1;
		$start_from=($start_from<0)?0:$start_from;
		$starts_from=$per_page*$start_from;
		
		// Check Export Action 
		$pagination_qry="OFFSET $starts_from ROWS FETCH NEXT $per_page ROWS ONLY";
		if($_POST['is_export_data'] && $_POST['is_export_data']=='yes')
		{
			$pagination_qry="";
		}	

	
		$main_tsql = "SELECT  r.[tcr_id]
			,r.[Value1]
			  ,r.[Value2]
			  ,r.[Value3]
			  ,r.[active]
			  ,r.[create_date]
			  ,'<textarea cols=40 rows=3 id='+char(39)+'rr__'+cast(r.[tcr_id] as varchar)+char(39)+'></textarea>' as [Value8]
			  ,'<button id='+char(39)+'btna__'+cast(r.[tcr_id] as varchar)+char(39)+' data-id='+char(39)+cast(r.[tcr_id] as varchar)+char(39)+' class='+char(39)+'btna btn btn-success btn-lg'+char(39)+'><i class='+char(39)+'fas fa-check-double'+char(39)+'></i> Approve</button><button id='+char(39)+'btnr__'+cast(r.[tcr_id] as varchar)+char(39)+' data-id='+char(39)+cast(r.[tcr_id] as varchar)+char(39)+' class='+char(39)+'btnr btn btn-danger btn-lg'+char(39)+'><i class='+char(39)+'fas fa-ban'+char(39)+'></i> Reject</button>'  as [Value9]
			  ,(SELECT COUNT(*) FROM tray_compliance_line l WITH (NOLOCK) WHERE l.[Value1] = r.[Value1] AND l.[Value2] = r.[Value2] AND l.[Value3] = r.[Value3]) as 'itemcount' 
			  ,r.[CustomerName]
			  ,r.[ShipToAddr]
			  ,r.[City]
			  ,r.[State]
			  ,r.[Zip]
			  ,r.[ClientName]
			  ,r.[SlsLocn]
			  ,r.[OpsTeam]
			  ,r.[OrderDate]
				FROM [dbo].[tray_compliance_review] r WITH (NOLOCK) ";
				
			
			if(isset($_POST['val1']) && $_POST['val1']<>'')
			{
				$tsql=$main_tsql. " WHERE r.[Value1] = '".$_POST['val1']."' $filter_txt_qry order by r.tcr_id asc $pagination_qry";
				$tsql_total_num = "SELECT count(r.[tcr_id]) as total_num FROM [dbo].[tray_compliance_review] r WITH (NOLOCK) WHERE r.[Value1] = '".$_POST['val1']."' $filter_txt_qry";
			}
            else 
            {
				$tsql=$main_tsql. " WHERE isnull(r.active,1) > 0  $filter_txt_qry order by r.tcr_id asc $pagination_qry";
			}				
		   // echo $tsql; exit;
	} 
   else
    {		
	$tsql= "EXEC [dbo].[usp_query_request] @querytype = '".$_POST['qrytype']."', @val1 = '".$_POST['val1']."', @val2 = '', @val3 = ''";
	}
}
if($_POST['qrytype']=='VPitems'){
  if(isset($_POST['active_pagination']) && $_POST['active_pagination']=='yes')
	{
		// pagination custom searching 
		$filter_txt_qry="";
		if($_POST['filter_txt'] && $_POST['filter_txt']!='')
		{
			$filter_txt_val=$_POST['filter_txt'];
			$filter_txt_qry=" AND ( l.[Value1] like '%".$filter_txt_val."%' OR l.[Value2] like '%".$filter_txt_val."%' OR l.[Value3] like '%".$filter_txt_val."%' 
			OR l.[Value4] like '%".$filter_txt_val."%' OR l.[Value5] like '%".$filter_txt_val."%' OR l.[Value6] like '%".$filter_txt_val."%' 
			OR l.[Value7] like '%".$filter_txt_val."%' )";		
		}	
		
		// fetching total_num for pagination 
		$tsql_total_num = "SELECT count(l.[tcl_id]) as total_num FROM [dbo].[tray_compliance_line] l WITH (NOLOCK) WHERE isnull(l.active,1) > 0 $filter_txt_qry";
		
		
		/* BLR STATUS FEDERAL QUERY - DISPLAY EACH PRODUCT WITH OR WITHOUT APPROVAL */
		
        $per_page=($_POST['per_page_size']!='')?$_POST['per_page_size']:10;
		$start_from=($_POST['page_number']!='')?$_POST['page_number']:0;
		$start_from=$start_from-1;
		$start_from=($start_from<0)?0:$start_from;
		$starts_from=$per_page*$start_from;
		
		// Check Export Action 
		$pagination_qry="OFFSET $starts_from ROWS FETCH NEXT $per_page ROWS ONLY";
		if($_POST['is_export_data'] && $_POST['is_export_data']=='yes')
		{
			$pagination_qry="";
		}	

	
		$tsql = "SELECT l.[tcl_id]
				  ,l.[Value1]
				  ,'<a href='+char(39)+'orderreview.php?ok='+l.[Value1]+char(39)+'>'+l.[Value1]+'</a>' as 'Value1_html'
				  ,l.[Value2]
				  ,l.[Value3]
				  ,l.[Value4]
				  ,l.[Value5]
				  ,l.[Value6]
				  ,l.[Value7]
				  ,l.[active]
				  ,l.[create_date]
				  ,'<textarea cols=40 rows=3 id='+char(39)+'rrl__'+cast(l.[tcl_id] as varchar)+char(39)+'></textarea>' as [Value8]
				  ,'<button id='+char(39)+'btnla__'+cast(l.[tcl_id] as varchar)+char(39)+' data-id='+char(39)+cast(l.[tcl_id] as varchar)+char(39)+' class='+char(39)+'btnla btn btn-success'+char(39)+'><i class='+char(39)+'fas fa-check-double'+char(39)+'></i> Approve</button> <button id='+char(39)+'btnlr__'+cast(l.[tcl_id] as varchar)+char(39)+' data-id='+char(39)+cast(l.[tcl_id] as varchar)+char(39)+' class='+char(39)+'btnlr btn btn-danger'+char(39)+'><i class='+char(39)+'fas fa-ban'+char(39)+'></i> Reject</button>'  as [Value9]
			 FROM [dbo].[tray_compliance_line] l WITH (NOLOCK)
			 WHERE isnull(l.active,1) > 0 $filter_txt_qry order by l.tcl_id asc $pagination_qry ";				
	}
    else 
    {		
	$tsql= "EXEC [dbo].[usp_query_request] @querytype = '".$_POST['qrytype']."', @val1 = '', @val2 = '', @val3 = ''";
	}
}
if($_POST['qrytype']=='VPproditems'){
	//if(isset($_POST['prodID']) && $_POST['prodID']!=='')
	//{
		$tsql= "EXEC [dbo].[usp_query_request] @querytype = '".$_POST['qrytype']."', @val1 = '".$_POST['val1']."', @val2 = '".$_POST['val2']."', @val3 = '".$_POST['val3']."'";
	//}
}
if($_POST['qrytype']=='OrdRevApp' || $_POST['qrytype']=='OrdRevRej' || $_POST['qrytype']=='LineRevApp' || $_POST['qrytype']=='LineRevRej' ){
	if(isset($_POST['qryid']) && intval($_POST['qryid'])>0)
	{
		$tsql= "EXEC [dbo].[usp_upd_order_review] @qrytype = '".$_POST['qrytype']."', @qryid = ".intval($_POST['qryid']).", @qrymsg = '".$_POST['qrymsg']."', @qryuser = '".$_POST['qryuser']."'";
		echo $tsql;
	}
}

function utf8ize($d) {
	if (is_array($d)) {
		foreach ($d as $k => $v) {
			$d[$k] = utf8ize($v);
		}
	} else if (is_string ($d)) {
		return utf8_encode($d);
	}
	return $d;
}

$getResults= sqlsrv_query($conn, $tsql);
//echo ("Reading data from table" . PHP_EOL);
if ($getResults == FALSE)
	echo (sqlsrv_errors());

/* Processing query results */

/* Setup an empty array */
$json = array();
/* Iterate through the table rows populating the array */
do {
     while ($row = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
     $json[] = utf8ize($row);
     }
} while ( sqlsrv_next_result($getResults) );
 

//$json = utf8ize($json);

/* Run the tabular results through json_encode() */
/* And ensure numbers don't get cast to strings */
//echo json_encode($json);

if (($_POST['qrytype']=='VPitems' || $_POST['qrytype']=='VPdefault') && isset($_POST['active_pagination']) && $_POST['active_pagination']=='yes')
{	
    if($tsql_total_num!="")
	{
		$getResults_totalNum = sqlsrv_query($conn, $tsql_total_num);

		if ($getResults_totalNum == FALSE)
			echo (sqlsrv_errors());

		while ($row_totalNum = sqlsrv_fetch_array($getResults_totalNum, SQLSRV_FETCH_ASSOC)) {
			$pagination_total_num=$row_totalNum['total_num'];
		}
	}
	
	
	$datas = array();
	$datas['total']=$pagination_total_num;
	$datas['rows']=$json;
	echo json_encode($datas);
}
else 
{
echo json_encode($json);
}

/* Free statement and connection resources. */
sqlsrv_free_stmt( $getResults);
sqlsrv_close( $conn);




?>