<?php
ini_set('memory_limit','512M');
ini_set('max_execution_time', '500');
include('settings.php');
include('functions.php');
include ('Classes/PHPExcel/IOFactory.php');
error_reporting(E_ALL); //displays an error
//set_time_limit(0);    
 

session_start();
if(!isset($_SESSION['mhwltdphp_user'])){
	die( "Not authenticated !" );  
}
$isAdmin = in_array($_SESSION['mhwltdphp_usertype'], ['ADMIN','SUPERUSER']);
include("dbconnect.php");

//Establishes the connection
$conn = sqlsrv_connect($serverName, $connectionOptions);
if( $conn === false) {
		die( print_r( sqlsrv_errors(), true));
}


//echo "<pre>"; print_r($_POST); exit; 

// total_num pagination 
$pagination_total_num=0;
$tsql_total_num="";

/* ********************* added by Jobaidur :start ************************ */

if ($_POST['export_qry_type']=='blr-status') {

	if (empty($_POST['export_view_type']) || empty($_POST['export_client']) ) {
		echo("Wrong parameters !"); exit;
	}

    if ($_POST['export_view_type'] === 'Federal product approvals') {

		$client_name_qry=" AND p.[client_name] = '".addslashes($_POST['export_client'])."'";	
		$prod_ids_qry_part="";
		if($_POST['export_prodlistids']!==''&& $_POST['export_prodlistids']!=='[]')
		{
			$prodids =  str_replace("]","",str_replace("[","",$_POST['export_prodlistids']));
			$prod_ids_qry_part=" AND p.[product_id] IN (".$prodids.") ";			
		}
		
		$advance_searching_qry_part="";
		if($_POST['export_searching_data']!==''&& $_POST['export_searching_data']!=='{}')
		{
			$searching_data_val =  json_decode($_POST['export_searching_data']);
		    foreach($searching_data_val as $key=>$val)
			{
				if(trim($val)!='' && (substr($key, 0, 2)=='p.' || substr($key, 0, 2)=='f.'))
				{					
						$advance_searching_qry_part=$advance_searching_qry_part. " AND ". $key ." like '%".$val."%'";
				}
			}
			
			if(isset($searching_data_val->all_clients) && $searching_data_val->all_clients=='all_clients')
				{
					if($advance_searching_qry_part)
					{
					$client_name_qry="";
                    }					
				}			
		}
		
		// pagination custom searching 
		$filter_txt_qry="";
		if($_POST['export_filter_txt'] && $_POST['export_filter_txt']!='')
		{
			$filter_txt_val=$_POST['export_filter_txt'];
			$filter_txt_qry=" AND ( p.[product_id] like '%".$filter_txt_val."%'  OR p.[TTB_ID] like '%".$filter_txt_val."%' OR p.[product_desc] like '%".$filter_txt_val."%'
OR p.[brand_name] like '%".$filter_txt_val."%'  OR p.[product_mhw_code] like '%".$filter_txt_val."%' OR p.[federal_type] like '%".$filter_txt_val."%'
OR p.[client_code] like '%".$filter_txt_val."%'  OR f.[status] like '%".$filter_txt_val."%' OR p.[product_class] like '%".$filter_txt_val."%'
OR f.[primaryfederalbasicpermitnumber] like '%".$filter_txt_val."%'  OR p.[country] like '%".$filter_txt_val."%' OR f.[legalnameusedonlabel] like '%".$filter_txt_val."%'
OR f.[applicantname] like '%".$filter_txt_val."%')";
		}
		
$tsql_total_num = "select count(*) as total_num from (
		SELECT DISTINCT
				p.[product_id], p.[product_desc],
				p.[brand_name], p.[product_mhw_code],
				p.[TTB_ID], p.[federal_type], p.[client_code],
				f.[status] as federal_status,f.[status] as status,
				convert(varchar, f.[dateapproved], 120) as dateapproved,p.[product_class],f.[primaryfederalbasicpermitnumber],
				p.[country],f.[legalnameusedonlabel],f.[applicantname],convert(varchar, f.[datecompleted], 120) as datecompleted
			FROM  [dbo].[mhw_app_prod] p
			LEFT JOIN [dbo].[sc_cola_federal_sts] f
				ON f.[cola] = p.[TTB_ID]
				AND f.[client] = p.[client_code]
			WHERE p.[product_id] IS NOT NULL  
				AND p.active = 1  
			   	  $client_name_qry     
				AND f.[status] is not NULL and f.[status]!=''
				$prod_ids_qry_part $advance_searching_qry_part $filter_txt_qry
		) as srctbl";		
		
		/* REPORT FED QUERY - ONLY DISPLAY PRODUCTS WITH APPROVALS */
         
		$tsql = "SELECT DISTINCT
				p.[product_id],CASE
    WHEN ISNUMERIC(SUBSTRING(p.[TTB_ID],1,14))=1 AND len(SUBSTRING(p.[TTB_ID],1,14))=14 THEN SUBSTRING(p.[TTB_ID],1,14)
    ELSE ''
END AS prefix_ttb_id, p.[product_desc],
				p.[brand_name], p.[product_mhw_code],
				p.[TTB_ID], p.[federal_type], p.[client_code],
				f.[status] as federal_status,f.[status] as status,
				convert(varchar, f.[dateapproved], 120) as dateapproved,p.[product_class],f.[primaryfederalbasicpermitnumber],
				p.[country],f.[legalnameusedonlabel],f.[applicantname],convert(varchar, f.[datecompleted], 120) as datecompleted
				,'' as est_dt_approval,'' as user_notes,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount'
			FROM  [dbo].[mhw_app_prod] p
			LEFT JOIN [dbo].[sc_cola_federal_sts] f
				ON f.[cola] = p.[TTB_ID]
				AND f.[client] = p.[client_code]
			WHERE p.[product_id] IS NOT NULL  
				AND p.active = 1  
			   	  $client_name_qry     
				AND f.[status] is not NULL and f.[status]!=''
				$prod_ids_qry_part $advance_searching_qry_part $filter_txt_qry
			ORDER BY p.[product_desc] ASC
		";
	//	echo $tsql; exit;
	}
	else if ($_POST['export_view_type'] === 'State product approvals') {
		
		 $client_name_qry=" AND p.[client_name] = '".addslashes($_POST['export_client'])."'";
		 $prod_ids_qry_part="";
		if($_POST['export_prodlistids']!==''&& $_POST['export_prodlistids']!=='[]')
		{
			$prodids =  str_replace("]","",str_replace("[","",$_POST['export_prodlistids']));
			$prod_ids_qry_part=" AND p.[product_id] IN (".$prodids.") ";			
		}
		
		$advance_searching_qry_part="";
		if($_POST['export_searching_data']!==''&& $_POST['export_searching_data']!=='{}')
		{
			$searching_data_val =  json_decode($_POST['export_searching_data']);
		    foreach($searching_data_val as $key=>$val)
			{ 
				if(trim($val)!='' && (substr($key, 0, 2)=='p.' || substr($key, 0, 2)=='s.'))
				{					
						$advance_searching_qry_part=$advance_searching_qry_part. " AND ". $key ." like '%".$val."%'";
				}
			}
						
			if(isset($searching_data_val->all_clients) && $searching_data_val->all_clients=='all_clients')
				{
					if($advance_searching_qry_part)
					{
						$client_name_qry="";	
                    }						
				}
		}
		
		
	// pagination custom searching 
		$filter_txt_qry="";
		if($_POST['export_filter_txt'] && $_POST['export_filter_txt']!='')
		{
			$filter_txt_val=$_POST['export_filter_txt'];
			$filter_txt_qry=" AND ( p.[product_id] like '%".$filter_txt_val."%'  OR p.[TTB_ID] like '%".$filter_txt_val."%' OR p.[product_desc] like '%".$filter_txt_val."%'
OR p.[brand_name] like '%".$filter_txt_val."%'  OR p.[product_mhw_code] like '%".$filter_txt_val."%' OR p.[federal_type] like '%".$filter_txt_val."%'
OR p.[client_code] like '%".$filter_txt_val."%'  OR s.[StateApprovalNumber] like '%".$filter_txt_val."%' OR s.[Status] like '%".$filter_txt_val."%'
OR s.[State] like '%".$filter_txt_val."%' OR s.[Preparer] like '%".$filter_txt_val."%')";
		}
		
$tsql_total_num = "select count(*) as total_num from (
		SELECT DISTINCT
						p.[product_id], p.[product_desc],
						p.[brand_name], p.[product_mhw_code],
						p.[TTB_ID], p.[federal_type], p.[client_code],
						s.StateApprovalNumber as StateApprovalNumber,s.Status as Status,s.EffectiveDate as EffectiveDate,s.State as State,s.Preparer as Preparer
					FROM  [dbo].[mhw_app_prod] p
					left outer join [__fact_state_reg_status] s 
				   on s.[ProductKey] = p.[product_mhw_code] and s.client=p.client_code			
					WHERE p.[product_id] IS NOT NULL
					AND p.active = 1
						  $client_name_qry  
					$prod_ids_qry_part  $advance_searching_qry_part $filter_txt_qry
		) as srctbl";			

		/* REPORT STATE QUERY - ONLY DISPLAY PRODUCTS WITH STATE REGISTRATIONS */

		
		$tsql = "SELECT DISTINCT
				p.[product_id], p.[product_desc],CASE
    WHEN ISNUMERIC(SUBSTRING(p.[TTB_ID],1,14))=1 AND len(SUBSTRING(p.[TTB_ID],1,14))=14 THEN SUBSTRING(p.[TTB_ID],1,14)
    ELSE ''
END AS prefix_ttb_id,
				p.[brand_name], p.[product_mhw_code],
				p.[TTB_ID], p.[federal_type], p.[client_code],
				s.StateApprovalNumber as StateApprovalNumber,s.Status as Status,s.EffectiveDate as EffectiveDate,s.State as State,s.Preparer as Preparer,'' as est_dt_approval,'' as mhw_user_notes,
				(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount'
			FROM  [dbo].[mhw_app_prod] p
            left outer join [__fact_state_reg_status] s 
           on s.[ProductKey] = p.[product_mhw_code] and s.client=p.client_code			
            WHERE p.[product_id] IS NOT NULL
			AND p.active = 1
				  $client_name_qry  
			$prod_ids_qry_part  $advance_searching_qry_part $filter_txt_qry
			ORDER BY p.[product_desc] ASC";
			
			// echo $tsql;exit;
	}
    else if ($_POST['export_view_type'] === 'Federal product compliance status') {
		
		$client_name_qry=" AND p.[client_name] = '".addslashes($_POST['export_client'])."'";
		
		$prod_ids_qry_part="";
		if($_POST['export_prodlistids']!==''&& $_POST['export_prodlistids']!=='[]')
		{
			$prodids =  str_replace("]","",str_replace("[","",$_POST['export_prodlistids']));
			$prod_ids_qry_part=" AND p.[product_id] IN (".$prodids.") ";			
		}
		
		$advance_searching_qry_part="";
		if($_POST['export_searching_data']!==''&& $_POST['export_searching_data']!=='{}')
		{
			$searching_data_val =  json_decode($_POST['export_searching_data']);
		    foreach($searching_data_val as $key=>$val)
			{
				if(trim($val)!='' && (substr($key, 0, 2)=='p.' || substr($key, 0, 2)=='f.'))
				{					
						$advance_searching_qry_part=$advance_searching_qry_part. " AND ". $key ." like '%".$val."%'";
				}
			}
			
			if(isset($searching_data_val->all_clients) && $searching_data_val->all_clients=='all_clients')
				{
					if($advance_searching_qry_part)
					{
					$client_name_qry="";
                    }					
				}
				
		}
		
		// pagination custom searching 
		$filter_txt_qry="";
		if($_POST['export_filter_txt'] && $_POST['export_filter_txt']!='')
		{
			$filter_txt_val=$_POST['export_filter_txt'];
			$filter_txt_qry=" AND ( p.[product_id] like '%".$filter_txt_val."%'  OR p.[TTB_ID] like '%".$filter_txt_val."%' OR p.[product_desc] like '%".$filter_txt_val."%'
OR p.[brand_name] like '%".$filter_txt_val."%'  OR p.[product_mhw_code] like '%".$filter_txt_val."%' OR p.[federal_type] like '%".$filter_txt_val."%'
OR p.[client_code] like '%".$filter_txt_val."%'  OR f.[status] like '%".$filter_txt_val."%' OR p.[product_class] like '%".$filter_txt_val."%'
OR f.[primaryfederalbasicpermitnumber] like '%".$filter_txt_val."%'  OR p.[country] like '%".$filter_txt_val."%' OR f.[legalnameusedonlabel] like '%".$filter_txt_val."%'
OR f.[applicantname] like '%".$filter_txt_val."%')";
		}
		
		
$tsql_total_num = "select count(*) as total_num from (
		SELECT DISTINCT
				p.[product_id], p.[product_desc],
				p.[brand_name], p.[product_mhw_code],
				p.[TTB_ID], p.[federal_type], p.[client_code],
				f.[status] as federal_status,f.[status] as status,
				convert(varchar, f.[dateapproved], 120) as dateapproved,p.[product_class],f.[primaryfederalbasicpermitnumber],
				p.[country],f.[legalnameusedonlabel],f.[applicantname],convert(varchar, f.[datecompleted], 120) as datecompleted FROM  [dbo].[mhw_app_prod] p
			LEFT JOIN [dbo].[sc_cola_federal_sts] f
				ON f.[cola] = p.[TTB_ID]
				AND f.[client] = p.[client_code]
				AND f.[status] is not NULL and f.[status]!=''
			WHERE p.[product_id] IS NOT NULL  
				AND p.active = 1
				  $client_name_qry  
				$prod_ids_qry_part $advance_searching_qry_part $filter_txt_qry
		) as srctbl";			

		
		/* BLR STATUS FEDERAL QUERY - DISPLAY EACH PRODUCT WITH OR WITHOUT APPROVAL */
		
		$tsql = "SELECT DISTINCT
				p.[product_id],CASE
    WHEN ISNUMERIC(SUBSTRING(p.[TTB_ID],1,14))=1 AND len(SUBSTRING(p.[TTB_ID],1,14))=14 THEN SUBSTRING(p.[TTB_ID],1,14)
    ELSE ''
END AS prefix_ttb_id, p.[product_desc],
				p.[brand_name], p.[product_mhw_code],
				p.[TTB_ID], p.[federal_type], p.[client_code],
				f.[status] as federal_status,f.[status] as status,
				convert(varchar, f.[dateapproved], 120) as dateapproved,p.[product_class],f.[primaryfederalbasicpermitnumber],
				p.[country],f.[legalnameusedonlabel],f.[applicantname],convert(varchar, f.[datecompleted], 120) as datecompleted
				,'' as est_dt_approval,'' as user_notes,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount'
			FROM  [dbo].[mhw_app_prod] p
			LEFT JOIN [dbo].[sc_cola_federal_sts] f
				ON f.[cola] = p.[TTB_ID]
				AND f.[client] = p.[client_code]
				AND f.[status] is not NULL and f.[status]!=''
			WHERE p.[product_id] IS NOT NULL  
				AND p.active = 1
				  $client_name_qry  
				$prod_ids_qry_part $advance_searching_qry_part $filter_txt_qry
			ORDER BY p.[product_desc] ASC
		";
		// echo $tsql; exit;
	}
	else if ($_POST['export_view_type'] === 'State product compliance status') {
		
		 $client_name_qry=" AND p.[client_name] = '".addslashes($_POST['export_client'])."'";
		 $prod_ids_qry_part="";
		if($_POST['export_prodlistids']!==''&& $_POST['export_prodlistids']!=='[]')
		{
			$prodids =  str_replace("]","",str_replace("[","",$_POST['export_prodlistids']));
			$prod_ids_qry_part=" AND p.[product_id] IN (".$prodids.") ";			
		}
		
		$advance_searching_qry_part="";
		if($_POST['export_searching_data']!==''&& $_POST['export_searching_data']!=='{}')
		{
			$searching_data_val =  json_decode($_POST['export_searching_data']);
		    foreach($searching_data_val as $key=>$val)
			{ 
				if(trim($val)!='' && (substr($key, 0, 2)=='p.' || substr($key, 0, 2)=='s.' || substr($key, 0, 2)=='r.'))
				{					
						$advance_searching_qry_part=$advance_searching_qry_part. " AND ". $key ." like '%".$val."%'";
				}
			}
			
			if(isset($searching_data_val->all_clients) && $searching_data_val->all_clients=='all_clients')
				{
					if($advance_searching_qry_part)
					{
						$client_name_qry="";	
                    }						
				}
			
		}
	
	// pagination custom searching 
		$filter_txt_qry="";
		if($_POST['export_filter_txt'] && $_POST['export_filter_txt']!='')
		{
			$filter_txt_val=$_POST['export_filter_txt'];
			$filter_txt_qry=" AND ( p.[product_id] like '%".$filter_txt_val."%'  OR p.[TTB_ID] like '%".$filter_txt_val."%' OR p.[product_desc] like '%".$filter_txt_val."%'
OR p.[brand_name] like '%".$filter_txt_val."%'  OR p.[product_mhw_code] like '%".$filter_txt_val."%' OR p.[federal_type] like '%".$filter_txt_val."%'
OR p.[client_code] like '%".$filter_txt_val."%'  OR s.[StateApprovalNumber] like '%".$filter_txt_val."%' OR s.[Status] like '%".$filter_txt_val."%'
OR s.[State] like '%".$filter_txt_val."%' OR s.[Preparer] like '%".$filter_txt_val."%')";
		}
		
$tsql_total_num = "select count(*) as total_num from (
		SELECT DISTINCT
				p.[product_id],
				p.[brand_name], p.[product_mhw_code],
				p.[TTB_ID], p.[federal_type], p.[client_code],
				s.StateApprovalNumber as StateApprovalNumber,s.Status as Status,s.EffectiveDate as EffectiveDate,r.state_name as State,s.Preparer as Preparer
			FROM  [dbo].[mhw_app_prod] p
			cross join [dbo].[__ref_states] r with (nolock)
            left outer join [__fact_state_reg_status] s 
           on s.[ProductKey] = p.[product_mhw_code] and s.client=p.client_code and s.State = r.state_name 			
            WHERE p.[product_id] IS NOT NULL
			AND p.active = 1
				  $client_name_qry  
			$prod_ids_qry_part  $advance_searching_qry_part $filter_txt_qry
		) as srctbl";			

		//echo $tsql_total_num."<br><br><br>";
		/* BLR STATUS STATE QUERY - DISPLAY EACH PRODUCT AND EACH STATE WITH OR WITHOUT REGISTRATION */
		
		$tsql = "SELECT DISTINCT
				p.[product_id], p.[product_desc],CASE
    WHEN ISNUMERIC(SUBSTRING(p.[TTB_ID],1,14))=1 AND len(SUBSTRING(p.[TTB_ID],1,14))=14 THEN SUBSTRING(p.[TTB_ID],1,14)
    ELSE ''
END AS prefix_ttb_id,
				p.[brand_name], p.[product_mhw_code],
				p.[TTB_ID], p.[federal_type], p.[client_code],
				s.StateApprovalNumber as StateApprovalNumber,s.Status as Status,s.EffectiveDate as EffectiveDate,r.state_name as State,s.Preparer as Preparer,'' as est_dt_approval,'' as mhw_user_notes,
				(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount'
			FROM  [dbo].[mhw_app_prod] p
			cross join [dbo].[__ref_states] r with (nolock)
            left outer join [__fact_state_reg_status] s 
           on s.[ProductKey] = p.[product_mhw_code] and s.client=p.client_code and s.State = r.state_name 			
            WHERE p.[product_id] IS NOT NULL
			AND p.active = 1
				  $client_name_qry  
			$prod_ids_qry_part  $advance_searching_qry_part $filter_txt_qry
			ORDER BY p.[product_desc]
			";	
			//echo $tsql;exit;
			//ORDER BY p.[product_desc],r.state_name ASC
	}	
	else {
		echo("Wrong parameters !"); exit;
	}

}




/* ********************* added by Jobaidur :End ************************ */

//echo "<pre>"; print_r($_POST); echo "</pre>";

if($tsql_total_num!="")
	{
		$getResults_totalNum = sqlsrv_query($conn, $tsql_total_num);

		if ($getResults_totalNum == FALSE)
			echo (sqlsrv_errors());

		while ($row_totalNum = sqlsrv_fetch_array($getResults_totalNum, SQLSRV_FETCH_ASSOC)) {
			$pagination_total_num=$row_totalNum['total_num'];
		}
	}
	
if($pagination_total_num>35000)
{
	$pagination_total_num=34999;
}	

/* Processing query results */

/* federal product approvals : data export */

if(($_POST['export_view_type'] === 'Federal product approvals') && $_POST['export_qry_type']=='blr-status')
{
$file_name=$_POST['export_view_type'].".xlsx";	
$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("jahid0604015@gmail.com")
							 ->setTitle($file_name)
							 ->setSubject($file_name);

$row = 1;
$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Product Code')
                              ->setCellValue('B'.$row, 'Brand Name')
							  ->setCellValue('C'.$row, 'Product Description')
							  ->setCellValue('D'.$row, 'Federal Type')
							  ->setCellValue('E'.$row, 'Product Class')
							  ->setCellValue('F'.$row, 'TTB ID')
							  ->setCellValue('G'.$row, 'Status')
							  ->setCellValue('H'.$row, 'Date of Application')
							  ->setCellValue('I'.$row, 'Estimated Date of Approval')
							  ->setCellValue('J'.$row, 'Date Approved')
							  ->setCellValue('K'.$row, 'TTB Basic Importer #')
							  ->setCellValue('L'.$row, 'Country of Origin')
							  ->setCellValue('M'.$row, 'TTB Basic Importer Name')
							  ->setCellValue('N'.$row, 'MHW User that filed')
							  ->setCellValue('O'.$row, 'MHW Team')
							  ->setCellValue('P'.$row, 'CompID');
							  							  
$row++;

$sheet_one_num=60000;
$sheet_two_num=0;
if($pagination_total_num<=60000)
{
$sheet_one_num=$pagination_total_num;
$sheet_two_num=0;
}
else if($pagination_total_num>60000)
{
$sheet_two_num=$pagination_total_num;
}

for($total_rows=0;$total_rows<$sheet_one_num;$total_rows=$total_rows+5000)
{
	$max_limit=5000;
	$pagination_qry="OFFSET $total_rows ROWS FETCH NEXT $max_limit ROWS ONLY";
	$tsql_temp=$tsql." ".$pagination_qry;
	//echo "<br><hr>".$tsql_temp."<hr><br>"; 
	$getResults= sqlsrv_query($conn, $tsql_temp);
    if ($getResults == FALSE)
	echo (sqlsrv_errors());

    do {
     while ($each_val = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
		 
     $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $each_val['product_mhw_code'])
                                  ->setCellValue('B'.$row, $each_val['brand_name'])
								  ->setCellValue('C'.$row, $each_val['product_desc'])
								  ->setCellValue('D'.$row, $each_val['federal_type'])
								  ->setCellValue('E'.$row, $each_val['product_class'])
								  ->setCellValueExplicit('F'.$row,(string)$each_val['TTB_ID'],PHPExcel_Cell_DataType::TYPE_STRING)
								  ->setCellValue('G'.$row, $each_val['status'])
								  ->setCellValue('H'.$row, '')
								  ->setCellValue('I'.$row, $each_val['est_dt_approval'])
								  ->setCellValue('J'.$row, $each_val['dateapproved'])
								  ->setCellValue('K'.$row, $each_val['primaryfederalbasicpermitnumber'])
								  ->setCellValue('L'.$row, $each_val['country'])
								  ->setCellValue('M'.$row, $each_val['legalnameusedonlabel'])
								  ->setCellValue('N'.$row, $each_val['applicantname'])
								  ->setCellValue('O'.$row, '')
								  ->setCellValue('P'.$row, '');
								  
				
    $row++;
     }
} while ( sqlsrv_next_result($getResults) );

sqlsrv_free_stmt( $getResults);

}

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setTitle('Sheet One');	

/* second sheet : start */

if($sheet_two_num>0)
{
	// Create a new worksheet, after the default sheet
     $objPHPExcel->createSheet();
     $objPHPExcel->setActiveSheetIndex(1);
	 
	 $row = 1;
$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Product Code')
                              ->setCellValue('B'.$row, 'Brand Name')
							  ->setCellValue('C'.$row, 'Product Description')
							  ->setCellValue('D'.$row, 'Federal Type')
							  ->setCellValue('E'.$row, 'Product Class')
							  ->setCellValue('F'.$row, 'TTB ID')
							  ->setCellValue('G'.$row, 'Status')
							  ->setCellValue('H'.$row, 'Date of Application')
							  ->setCellValue('I'.$row, 'Estimated Date of Approval')
							  ->setCellValue('J'.$row, 'Date Approved')
							  ->setCellValue('K'.$row, 'TTB Basic Importer #')
							  ->setCellValue('L'.$row, 'Country of Origin')
							  ->setCellValue('M'.$row, 'TTB Basic Importer Name')
							  ->setCellValue('N'.$row, 'MHW User that filed')
							  ->setCellValue('O'.$row, 'MHW Team')
							  ->setCellValue('P'.$row, 'CompID');
							  							  
$row++;

	 
	 for($total_rows2=$total_rows;$total_rows2<$sheet_two_num;$total_rows2=$total_rows2+5000)
{
	$max_limit=5000;
	$pagination_qry="OFFSET $total_rows2 ROWS FETCH NEXT $max_limit ROWS ONLY";
	$tsql_temp=$tsql." ".$pagination_qry;
	//echo "<br><hr>".$tsql_temp."<hr><br>"; 
	$getResults= sqlsrv_query($conn, $tsql_temp);
    if ($getResults == FALSE)
	echo (sqlsrv_errors());

    do {
     while ($each_val = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
		 
     $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $each_val['product_mhw_code'])
                                  ->setCellValue('B'.$row, $each_val['brand_name'])
								  ->setCellValue('C'.$row, $each_val['product_desc'])
								  ->setCellValue('D'.$row, $each_val['federal_type'])
								  ->setCellValue('E'.$row, $each_val['product_class'])
								  ->setCellValueExplicit('F'.$row,(string)$each_val['TTB_ID'],PHPExcel_Cell_DataType::TYPE_STRING)
								  ->setCellValue('G'.$row, $each_val['status'])
								  ->setCellValue('H'.$row, '')
								  ->setCellValue('I'.$row, $each_val['est_dt_approval'])
								  ->setCellValue('J'.$row, $each_val['dateapproved'])
								  ->setCellValue('K'.$row, $each_val['primaryfederalbasicpermitnumber'])
								  ->setCellValue('L'.$row, $each_val['country'])
								  ->setCellValue('M'.$row, $each_val['legalnameusedonlabel'])
								  ->setCellValue('N'.$row, $each_val['applicantname'])
								  ->setCellValue('O'.$row, '')
								  ->setCellValue('P'.$row, '');
								  
				
    $row++;
     }
} while ( sqlsrv_next_result($getResults) );

sqlsrv_free_stmt( $getResults);

}

	
}

$objPHPExcel->getActiveSheet()->setTitle('Sheet Two');

$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$file_name.'"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
}
else if(($_POST['export_view_type'] === 'State product approvals') && $_POST['export_qry_type']=='blr-status')
{	
 /* state product approvals : data export */
$file_name=$_POST['export_view_type'].".xlsx";	
$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("jahid0604015@gmail.com")
							 ->setTitle($file_name)
							 ->setSubject($file_name);

$row = 1;
$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Product Code')
                              ->setCellValue('B'.$row, 'Brand Name')
							  ->setCellValue('C'.$row, 'Product Description')
							  ->setCellValue('D'.$row, 'Federal Type')
							  ->setCellValue('E'.$row, 'State')
							  ->setCellValue('F'.$row, 'Status')
							  ->setCellValue('G'.$row, 'Submitted Date')
							  ->setCellValue('H'.$row, 'Effective Date')
							  ->setCellValue('I'.$row, 'Expiration Date')
							  ->setCellValue('J'.$row, 'Estimated Date of Approval')
							  ->setCellValue('K'.$row, 'Brand label registration (BLR) #')
							  ->setCellValue('L'.$row, 'MHW user that filed')
							  ->setCellValue('M'.$row, 'MHW Team')
							  ->setCellValue('N'.$row, 'CompID');
							  							  
$row++;

$sheet_one_num=60000;
$sheet_two_num=0;
if($pagination_total_num<=60000)
{
$sheet_one_num=$pagination_total_num;
$sheet_two_num=0;
}
else if($pagination_total_num>60000)
{
$sheet_two_num=$pagination_total_num;
}

for($total_rows=0;$total_rows<$sheet_one_num;$total_rows=$total_rows+5000)
{
	$max_limit=5000;
	$pagination_qry="OFFSET $total_rows ROWS FETCH NEXT $max_limit ROWS ONLY";
	$tsql_temp=$tsql." ".$pagination_qry;
	//echo "<br><hr>".$tsql_temp."<hr><br>"; 
	$getResults= sqlsrv_query($conn, $tsql_temp);
    if ($getResults == FALSE)
	echo (sqlsrv_errors());

    do {
     while ($each_val = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
		 
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $each_val['product_mhw_code'])
                                  ->setCellValue('B'.$row, $each_val['brand_name'])
								  ->setCellValue('C'.$row, $each_val['product_desc'])
								  ->setCellValue('D'.$row, $each_val['federal_type'])
								  ->setCellValue('E'.$row, $each_val['State'])
								  ->setCellValue('F'.$row, $each_val['Status'])
								  ->setCellValue('G'.$row, '')
								  ->setCellValue('H'.$row, $each_val['EffectiveDate'])
								  ->setCellValue('I'.$row, '')
								  ->setCellValue('J'.$row, $each_val['est_dt_approval'])
								  ->setCellValue('K'.$row, $each_val['StateApprovalNumber'])
								  ->setCellValue('L'.$row, $each_val['Preparer'])
								  ->setCellValue('M'.$row, '')
								  ->setCellValue('N'.$row, '');
								  
				
    $row++;
     }
} while ( sqlsrv_next_result($getResults) );

sqlsrv_free_stmt( $getResults);

}

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setTitle('Sheet One');	

/* second sheet : start */

if($sheet_two_num>0)
{
	// Create a new worksheet, after the default sheet
     $objPHPExcel->createSheet();
     $objPHPExcel->setActiveSheetIndex(1);
	 
	 $row = 1;
$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Product Code')
                              ->setCellValue('B'.$row, 'Brand Name')
							  ->setCellValue('C'.$row, 'Product Description')
							  ->setCellValue('D'.$row, 'Federal Type')
							  ->setCellValue('E'.$row, 'State')
							  ->setCellValue('F'.$row, 'Status')
							  ->setCellValue('G'.$row, 'Submitted Date')
							  ->setCellValue('H'.$row, 'Effective Date')
							  ->setCellValue('I'.$row, 'Expiration Date')
							  ->setCellValue('J'.$row, 'Estimated Date of Approval')
							  ->setCellValue('K'.$row, 'Brand label registration (BLR) #')
							  ->setCellValue('L'.$row, 'MHW user that filed')
							  ->setCellValue('M'.$row, 'MHW Team')
							  ->setCellValue('N'.$row, 'CompID');
							  							  
$row++;

	 
	 for($total_rows2=$total_rows;$total_rows2<$sheet_two_num;$total_rows2=$total_rows2+5000)
{
	$max_limit=5000;
	$pagination_qry="OFFSET $total_rows2 ROWS FETCH NEXT $max_limit ROWS ONLY";
	$tsql_temp=$tsql." ".$pagination_qry;
	//echo "<br><hr>".$tsql_temp."<hr><br>"; 
	$getResults= sqlsrv_query($conn, $tsql_temp);
    if ($getResults == FALSE)
	echo (sqlsrv_errors());

    do {
     while ($each_val = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
		 
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $each_val['product_mhw_code'])
                                  ->setCellValue('B'.$row, $each_val['brand_name'])
								  ->setCellValue('C'.$row, $each_val['product_desc'])
								  ->setCellValue('D'.$row, $each_val['federal_type'])
								  ->setCellValue('E'.$row, $each_val['State'])
								  ->setCellValue('F'.$row, $each_val['Status'])
								  ->setCellValue('G'.$row, '')
								  ->setCellValue('H'.$row, $each_val['EffectiveDate'])
								  ->setCellValue('I'.$row, '')
								  ->setCellValue('J'.$row, $each_val['est_dt_approval'])
								  ->setCellValue('K'.$row, $each_val['StateApprovalNumber'])
								  ->setCellValue('L'.$row, $each_val['Preparer'])
								  ->setCellValue('M'.$row, '')
								  ->setCellValue('N'.$row, '');
								  
				
    $row++;
     }
} while ( sqlsrv_next_result($getResults) );

sqlsrv_free_stmt( $getResults);

}

	
}

$objPHPExcel->getActiveSheet()->setTitle('Sheet Two');

$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$file_name.'"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
}
else if(($_POST['export_view_type'] === 'Federal product compliance status') && $_POST['export_qry_type']=='blr-status')
{
/* federal product compliance status : data export */
$file_name="Fedetal Status.xlsx";	
$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("jahid0604015@gmail.com")
							 ->setTitle($file_name)
							 ->setSubject($file_name);

$row = 1;
$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Product Code')
                              ->setCellValue('B'.$row, 'Brand Name')
							  ->setCellValue('C'.$row, 'Product Description')
							  ->setCellValue('D'.$row, 'Federal Type')
							  ->setCellValue('E'.$row, 'Product Class')
							  ->setCellValue('F'.$row, 'TTB ID')
							  ->setCellValue('G'.$row, 'Status')
							  ->setCellValue('H'.$row, 'Date of Application')
							  ->setCellValue('I'.$row, 'Estimated Date of Approval')
							  ->setCellValue('J'.$row, 'Date Approved')
							  ->setCellValue('K'.$row, 'TTB Basic Importer #')
							  ->setCellValue('L'.$row, 'Country of Origin')
							  ->setCellValue('M'.$row, 'TTB Basic Importer Name')
							  ->setCellValue('N'.$row, 'MHW User that filed')
							  ->setCellValue('O'.$row, 'MHW Team')
							  ->setCellValue('P'.$row, 'CompID');
							  							  
$row++;

$sheet_one_num=60000;
$sheet_two_num=0;
if($pagination_total_num<=60000)
{
$sheet_one_num=$pagination_total_num;
$sheet_two_num=0;
}
else if($pagination_total_num>60000)
{
$sheet_two_num=$pagination_total_num;
}

for($total_rows=0;$total_rows<$sheet_one_num;$total_rows=$total_rows+5000)
{
	$max_limit=5000;
	$pagination_qry="OFFSET $total_rows ROWS FETCH NEXT $max_limit ROWS ONLY";
	$tsql_temp=$tsql." ".$pagination_qry;
	//echo "<br><hr>".$tsql_temp."<hr><br>"; 
	$getResults= sqlsrv_query($conn, $tsql_temp);
    if ($getResults == FALSE)
	echo (sqlsrv_errors());

    do {
     while ($each_val = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
		 
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $each_val['product_mhw_code'])
                                  ->setCellValue('B'.$row, $each_val['brand_name'])
								  ->setCellValue('C'.$row, $each_val['product_desc'])
								  ->setCellValue('D'.$row, $each_val['federal_type'])
								  ->setCellValue('E'.$row, $each_val['product_class'])
								  ->setCellValueExplicit('F'.$row,(string)$each_val['TTB_ID'],PHPExcel_Cell_DataType::TYPE_STRING)
								  ->setCellValue('G'.$row, $each_val['status'])
								  ->setCellValue('H'.$row, '')
								  ->setCellValue('I'.$row, $each_val['est_dt_approval'])
								  ->setCellValue('J'.$row, $each_val['dateapproved'])
								  ->setCellValue('K'.$row, $each_val['primaryfederalbasicpermitnumber'])
								  ->setCellValue('L'.$row, $each_val['country'])
								  ->setCellValue('M'.$row, $each_val['legalnameusedonlabel'])
								  ->setCellValue('N'.$row, $each_val['applicantname'])
								  ->setCellValue('O'.$row, '')
								  ->setCellValue('P'.$row, '');
								  
				
    $row++;
     }
} while ( sqlsrv_next_result($getResults) );

sqlsrv_free_stmt( $getResults);

}

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setTitle('Sheet One');	

/* second sheet : start */

if($sheet_two_num>0)
{
	// Create a new worksheet, after the default sheet
     $objPHPExcel->createSheet();
     $objPHPExcel->setActiveSheetIndex(1);
	 
	 $row = 1;
$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Product Code')
                              ->setCellValue('B'.$row, 'Brand Name')
							  ->setCellValue('C'.$row, 'Product Description')
							  ->setCellValue('D'.$row, 'Federal Type')
							  ->setCellValue('E'.$row, 'Product Class')
							  ->setCellValue('F'.$row, 'TTB ID')
							  ->setCellValue('G'.$row, 'Status')
							  ->setCellValue('H'.$row, 'Date of Application')
							  ->setCellValue('I'.$row, 'Estimated Date of Approval')
							  ->setCellValue('J'.$row, 'Date Approved')
							  ->setCellValue('K'.$row, 'TTB Basic Importer #')
							  ->setCellValue('L'.$row, 'Country of Origin')
							  ->setCellValue('M'.$row, 'TTB Basic Importer Name')
							  ->setCellValue('N'.$row, 'MHW User that filed')
							  ->setCellValue('O'.$row, 'MHW Team')
							  ->setCellValue('P'.$row, 'CompID');
							  							  
$row++;

	 
	 for($total_rows2=$total_rows;$total_rows2<$sheet_two_num;$total_rows2=$total_rows2+5000)
{
	$max_limit=5000;
	$pagination_qry="OFFSET $total_rows2 ROWS FETCH NEXT $max_limit ROWS ONLY";
	$tsql_temp=$tsql." ".$pagination_qry;
	//echo "<br><hr>".$tsql_temp."<hr><br>"; 
	$getResults= sqlsrv_query($conn, $tsql_temp);
    if ($getResults == FALSE)
	echo (sqlsrv_errors());

    do {
     while ($each_val = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
		 
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $each_val['product_mhw_code'])
                                  ->setCellValue('B'.$row, $each_val['brand_name'])
								  ->setCellValue('C'.$row, $each_val['product_desc'])
								  ->setCellValue('D'.$row, $each_val['federal_type'])
								  ->setCellValue('E'.$row, $each_val['product_class'])
								  ->setCellValueExplicit('F'.$row,(string)$each_val['TTB_ID'],PHPExcel_Cell_DataType::TYPE_STRING)
								  ->setCellValue('G'.$row, $each_val['status'])
								  ->setCellValue('H'.$row, '')
								  ->setCellValue('I'.$row, $each_val['est_dt_approval'])
								  ->setCellValue('J'.$row, $each_val['dateapproved'])
								  ->setCellValue('K'.$row, $each_val['primaryfederalbasicpermitnumber'])
								  ->setCellValue('L'.$row, $each_val['country'])
								  ->setCellValue('M'.$row, $each_val['legalnameusedonlabel'])
								  ->setCellValue('N'.$row, $each_val['applicantname'])
								  ->setCellValue('O'.$row, '')
								  ->setCellValue('P'.$row, '');
								  
				
    $row++;
     }
} while ( sqlsrv_next_result($getResults) );

sqlsrv_free_stmt( $getResults);

}

	
}

$objPHPExcel->getActiveSheet()->setTitle('Sheet Two');

$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$file_name.'"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
}
else if(($_POST['export_view_type'] === 'State product compliance status') && $_POST['export_qry_type']=='blr-status')
{
/* state product compliance status : data export */

$file_name="State Status.xlsx";	
$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("jahid0604015@gmail.com")
							 ->setTitle($file_name)
							 ->setSubject($file_name);

$row = 1;
$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Product Code')
                              ->setCellValue('B'.$row, 'Brand Name')
							  ->setCellValue('C'.$row, 'Product Description')
							  ->setCellValue('D'.$row, 'Federal Type')
							  ->setCellValue('E'.$row, 'State')
							  ->setCellValue('F'.$row, 'Status')
							  ->setCellValue('G'.$row, 'Submitted Date')
							  ->setCellValue('H'.$row, 'Effective Date')
							  ->setCellValue('I'.$row, 'Expiration Date')
							  ->setCellValue('J'.$row, 'Estimated Date of Approval')
							  ->setCellValue('K'.$row, 'Brand label registration (BLR) #')
							  ->setCellValue('L'.$row, 'MHW user that filed')
							  ->setCellValue('M'.$row, 'MHW Team')
							  ->setCellValue('N'.$row, 'CompID');
		
							  							  
$row++;


$sheet_one_num=60000;
$sheet_two_num=0;
if($pagination_total_num<=60000)
{
$sheet_one_num=$pagination_total_num;
$sheet_two_num=0;
}
else if($pagination_total_num>60000)
{
$sheet_two_num=$pagination_total_num;
}

for($total_rows=0;$total_rows<$sheet_one_num;$total_rows=$total_rows+5000)
{
	$max_limit=5000;
	$pagination_qry="OFFSET $total_rows ROWS FETCH NEXT $max_limit ROWS ONLY";
	$tsql_temp=$tsql." ".$pagination_qry;
	//echo "<br><hr>".$tsql_temp."<hr><br>"; exit;
	$getResults= sqlsrv_query($conn, $tsql_temp);
    if ($getResults == FALSE)
	echo (sqlsrv_errors());

    do {
     while ($each_val = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
		 
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $each_val['product_mhw_code'])
                                  ->setCellValue('B'.$row, $each_val['brand_name'])
								  ->setCellValue('C'.$row, $each_val['product_desc'])
								  ->setCellValue('D'.$row, $each_val['federal_type'])
								  ->setCellValue('E'.$row, $each_val['State'])
								  ->setCellValue('F'.$row, $each_val['Status'])
								  ->setCellValue('G'.$row, '')
								  ->setCellValue('H'.$row, $each_val['EffectiveDate'])
								  ->setCellValue('I'.$row, '')
								  ->setCellValue('J'.$row, $each_val['est_dt_approval'])
								  ->setCellValue('K'.$row, $each_val['StateApprovalNumber'])
								  ->setCellValue('L'.$row, $each_val['Preparer'])
								  ->setCellValue('M'.$row, '')
								  ->setCellValue('N'.$row, '');
				
    $row++;
     }
} while ( sqlsrv_next_result($getResults) );

sqlsrv_free_stmt( $getResults);

}

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setTitle('Sheet One');	

/* second sheet : start */

if($sheet_two_num>0)
{
	// Create a new worksheet, after the default sheet
     $objPHPExcel->createSheet();
     $objPHPExcel->setActiveSheetIndex(1);
	 
	 $row = 1;
$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Product Code')
                              ->setCellValue('B'.$row, 'Brand Name')
							  ->setCellValue('C'.$row, 'Product Description')
							  ->setCellValue('D'.$row, 'Federal Type')
							  ->setCellValue('E'.$row, 'State')
							  ->setCellValue('F'.$row, 'Status')
							  ->setCellValue('G'.$row, 'Submitted Date')
							  ->setCellValue('H'.$row, 'Effective Date')
							  ->setCellValue('I'.$row, 'Expiration Date')
							  ->setCellValue('J'.$row, 'Estimated Date of Approval')
							  ->setCellValue('K'.$row, 'Brand label registration (BLR) #')
							  ->setCellValue('L'.$row, 'MHW user that filed')
							  ->setCellValue('M'.$row, 'MHW Team')
							  ->setCellValue('N'.$row, 'CompID');
		
							  							  
$row++;

	 
	 for($total_rows2=$total_rows;$total_rows2<$sheet_two_num;$total_rows2=$total_rows2+5000)
{
	$max_limit=5000;
	$pagination_qry="OFFSET $total_rows2 ROWS FETCH NEXT $max_limit ROWS ONLY";
	$tsql_temp=$tsql." ".$pagination_qry;
	//echo "<br><hr>".$tsql_temp."<hr><br>"; 
	$getResults= sqlsrv_query($conn, $tsql_temp);
    if ($getResults == FALSE)
	echo (sqlsrv_errors());

    do {
     while ($each_val = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
		 
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $each_val['product_mhw_code'])
                                  ->setCellValue('B'.$row, $each_val['brand_name'])
								  ->setCellValue('C'.$row, $each_val['product_desc'])
								  ->setCellValue('D'.$row, $each_val['federal_type'])
								  ->setCellValue('E'.$row, $each_val['State'])
								  ->setCellValue('F'.$row, $each_val['Status'])
								  ->setCellValue('G'.$row, '')
								  ->setCellValue('H'.$row, $each_val['EffectiveDate'])
								  ->setCellValue('I'.$row, '')
								  ->setCellValue('J'.$row, $each_val['est_dt_approval'])
								  ->setCellValue('K'.$row, $each_val['StateApprovalNumber'])
								  ->setCellValue('L'.$row, $each_val['Preparer'])
								  ->setCellValue('M'.$row, '')
								  ->setCellValue('N'.$row, '');
								  				
    $row++;
     }
} while ( sqlsrv_next_result($getResults) );

sqlsrv_free_stmt( $getResults);

}

	
}

$objPHPExcel->getActiveSheet()->setTitle('Sheet Two');

$objPHPExcel->setActiveSheetIndex(0);


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$file_name.'"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');

header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
}

   
function utf8ize($d) {
	if (is_array($d)) {
		foreach ($d as $k => $v) {
			$d[$k] = utf8ize($v);
		}
	} else if (is_string ($d)) {
		return utf8_encode($d);
	}
	return $d;
}

/* Free statement and connection resources. */

sqlsrv_free_stmt( $trkResults);
sqlsrv_close( $conn);
?>