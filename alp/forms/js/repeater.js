jQuery.fn.extend({
    createRepeater: function (options = {}) {
        var hasOption = function (optionKey) {
            return options.hasOwnProperty(optionKey);
        };
        var option = function (optionKey) {
            return options[optionKey];
        };
        var addItem = function (items, key, fresh = true) {
            var itemContent = items;
            var group = itemContent.data("group");
            var item = itemContent;
            var input = item.find('input,select');
            input.each(function (index, el) {
                var attrName = $(el).data('name');
                var skipName = $(el).data('skip-name');				
                if (skipName != true) {
                    $(el).attr("name", group + "[" + key + "]" + attrName);					
                } else {
                    if (attrName != 'undefined') {
                        $(el).attr("name", attrName);
						/* Custom code to add value to product box */
						if($(el).hasClass('tfa_product_desc')) {
							fresh = false;
							var tfcprod = $('#tfa_product_desc').val();												
							$(el).attr('value',tfcprod);

                            console.log(3, el, $(el), el.value);

							$('#tfa_product_desc').val('');
							$('#tfa_product_desc').trigger('change');
							$("#productvalid").hide();
						} else if ($(el).hasClass('tfa_product_id')) {
                            fresh = false;
							var prodID = $('#prodID').val();
                            el.value = prodID;
                            $(el).attr('value',prodID);
                            //if ( $('#tfa_product_desc')[0].hasAttribute("sub_info") ) {
                                //var json = $('#tfa_product_desc')[0].attr('sub_info');
                                //json = json.replace( /\'/g, '"' );
                                //try {
                                //    jsonObj = JSON.parse(json);
                                //    $(el).attr('value', jsonObj.id);
                                //} catch (error) {
                                //    console.log(error, json);
                                //}
                            //}
                        } else if ($(el).hasClass('supplier_id')) {
                            fresh = false;
							var supplier_id = $('#supplier_selected').val();
                            el.value = supplier_id;
                            $(el).attr('value',supplier_id);
                        } else if ($(el).hasClass('supplier_name')) {
                            fresh = false;
                            var supplier_id = $('#supplier_selected').val();
                            var s = suppliers.find(s=>s.id==supplier_id)
                            $(el).attr('value',s.supplier_name);
                        } else if ($(el).hasClass('supplier_contact')) {
                            fresh = false;
                            var supplier_id = $('#supplier_selected').val();
                            var s = suppliers.find(s=>s.id==supplier_id)
                            $(el).attr('value',s.supplier_contact);
                        } else if ($(el).hasClass('supplier_email')) {
                            fresh = false;
                            var supplier_id = $('#supplier_selected').val();
                            var s = suppliers.find(s=>s.id==supplier_id)
                            $(el).attr('value',s.supplier_email);
                        } else if ($(el).hasClass('supplier_phone')) {
                            fresh = false;
                            var supplier_id = $('#supplier_selected').val();
                            var s = suppliers.find(s=>s.id==supplier_id)
                            el.value = s.supplier_phone;
                            $(el).attr('value',s.supplier_phone);
						}
						/* End of custom code */
                    }
                }
                if (fresh == true) {
                    $(el).attr('value', '');
                }
            })
            var itemClone = items;

            /* Handling remove btn */
            var removeButton = itemClone.find('.remove-btn');

            if (key == 0) {
                removeButton.attr('disabled', true);
            } else {
                removeButton.attr('disabled', false);
            }

            removeButton.attr('onclick', '$(this).parents(\'.items\').remove()');

            $("<div class='items'>" + itemClone.html() + "<div/>").appendTo(repeater);
        };
        /* find elements */
        var repeater = this;
        var items = repeater.find(".items");
        var key = 0;
        var addButton = repeater.find('.repeater-add-btn');

        items.each(function (index, item) {
            items.remove();
            if (hasOption('showFirstItemToDefault') && option('showFirstItemToDefault') == true) {
                addItem($(item), key);
                key++;
            } else {
                if (items.length > 1) {
                    addItem($(item), key);
                    key++;
                }
            }
        });

        /* handle click and add items */
        addButton.on("click", function (e) {
			e.preventDefault();
            addItem($(items[0]), key);
            key++;
        });
    }
});