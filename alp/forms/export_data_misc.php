<?php
ini_set('memory_limit','512M');
ini_set('max_execution_time', '500');
include('settings.php');
include('functions.php');
include ('Classes/PHPExcel/IOFactory.php');
error_reporting(E_ALL); //displays an error
//set_time_limit(0);    
 
session_start();
if(!isset($_SESSION['mhwltdphp_user'])){
	die( "Not authenticated !" );  
}

$isAdmin = in_array($_SESSION['mhwltdphp_usertype'], ['ADMIN','SUPERUSER']);
include("dbconnect-misc.php");

//Establishes the connection
$conn = sqlsrv_connect($serverName_misc, $connectionOptions_misc);
if( $conn === false) {
		die( print_r( sqlsrv_errors(), true));
}




// total_num pagination 
$pagination_total_num=0;
$tsql_total_num="";
$redirect_url="orderreview.php";

/* ********************* added by Jobaidur :start ************************ */
if($_POST['export_view_type']=='orderreview' && $_POST['export_qry_type']=='VPitems')
{
	    $redirect_url="orderreview.php";
	    $client_name_qry="p.[client_name] = '".addslashes($_POST['export_client'])."' AND ";
		// pagination custom searching 
		$filter_txt_qry="";
		if($_POST['export_filter_txt'] && $_POST['export_filter_txt']!='')
		{
			$filter_txt_val=$_POST['export_filter_txt'];
			$filter_txt_qry=" AND ( l.[Value1] like '%".$filter_txt_val."%' OR l.[Value2] like '%".$filter_txt_val."%' OR l.[Value3] like '%".$filter_txt_val."%' 
			OR l.[Value4] like '%".$filter_txt_val."%' OR l.[Value5] like '%".$filter_txt_val."%' OR l.[Value6] like '%".$filter_txt_val."%' 
			OR l.[Value7] like '%".$filter_txt_val."%' ) ";
		}	
		
		// fetching total_num for pagination 
		
		$tsql_total_num = "SELECT count(l.[tcl_id]) as total_num FROM [dbo].[tray_compliance_line] l WITH (NOLOCK) WHERE isnull(l.active,1) > 0 $filter_txt_qry";
		

		$tsql = "SELECT l.[tcl_id]
				  ,l.[Value1]
				  ,'<a href='+char(39)+'orderreview.php?ok='+l.[Value1]+char(39)+'>'+l.[Value1]+'</a>' as 'Value1_html'
				  ,l.[Value2]
				  ,l.[Value3]
				  ,l.[Value4]
				  ,l.[Value5]
				  ,l.[Value6]
				  ,l.[Value7]
				  ,l.[active]
				  ,l.[create_date]
				  ,'<textarea cols=40 rows=3 id='+char(39)+'rrl__'+cast(l.[tcl_id] as varchar)+char(39)+'></textarea>' as [Value8]
				  ,'<button id='+char(39)+'btnla__'+cast(l.[tcl_id] as varchar)+char(39)+' data-id='+char(39)+cast(l.[tcl_id] as varchar)+char(39)+' class='+char(39)+'btnla btn btn-success'+char(39)+'><i class='+char(39)+'fas fa-check-double'+char(39)+'></i> Approve</button> <button id='+char(39)+'btnlr__'+cast(l.[tcl_id] as varchar)+char(39)+' data-id='+char(39)+cast(l.[tcl_id] as varchar)+char(39)+' class='+char(39)+'btnlr btn btn-danger'+char(39)+'><i class='+char(39)+'fas fa-ban'+char(39)+'></i> Reject</button>'  as [Value9]
			 FROM [dbo].[tray_compliance_line] l WITH (NOLOCK)
			 WHERE isnull(l.active,1) > 0 $filter_txt_qry order by l.tcl_id asc";	
		   
	
}
else if($_POST['export_view_type']=='orderreview' && $_POST['export_qry_type']=='VPdefault')
{
	    $redirect_url="viewproducts.php";
	    $client_name_qry="p.[client_name] = '".addslashes($_POST['export_client'])."' AND ";
		// pagination custom searching 
		$filter_txt_qry="";
		if($_POST['export_filter_txt'] && $_POST['export_filter_txt']!='')
		{
			$filter_txt_val=$_POST['export_filter_txt'];
			$filter_txt_qry=" AND ( r.[ClientName] like '%".$filter_txt_val."%'  OR r.[Value1] like '%".$filter_txt_val."%' OR r.[Value2] like '%".$filter_txt_val."%'  OR r.[CustomerName] like '%".$filter_txt_val."%' 
			OR r.[Value3] like '%".$filter_txt_val."%' OR r.[OrderDate] like '%".$filter_txt_val."%'  OR r.[SlsLocn] like '%".$filter_txt_val."%' OR r.[OpsTeam] like '%".$filter_txt_val."%'
			OR r.[ShipToAddr] like '%".$filter_txt_val."%'  OR r.[City] like '%".$filter_txt_val."%' OR r.[State] like '%".$filter_txt_val."%'
			OR r.[Zip] like '%".$filter_txt_val."%' )";	
		}	
		
		// fetching total_num for pagination 
		
		$tsql_total_num = "SELECT count(r.[tcr_id]) as total_num FROM [dbo].[tray_compliance_review] r WITH (NOLOCK) WHERE isnull(r.active,1) > 0 $filter_txt_qry";
		
		
		$main_tsql = "SELECT  r.[tcr_id]
			,r.[Value1]
			  ,r.[Value2]
			  ,r.[Value3]
			  ,r.[active]
			  ,r.[create_date]
			  ,'<textarea cols=40 rows=3 id='+char(39)+'rr__'+cast(r.[tcr_id] as varchar)+char(39)+'></textarea>' as [Value8]
			  ,'<button id='+char(39)+'btna__'+cast(r.[tcr_id] as varchar)+char(39)+' data-id='+char(39)+cast(r.[tcr_id] as varchar)+char(39)+' class='+char(39)+'btna btn btn-success btn-lg'+char(39)+'><i class='+char(39)+'fas fa-check-double'+char(39)+'></i> Approve</button><button id='+char(39)+'btnr__'+cast(r.[tcr_id] as varchar)+char(39)+' data-id='+char(39)+cast(r.[tcr_id] as varchar)+char(39)+' class='+char(39)+'btnr btn btn-danger btn-lg'+char(39)+'><i class='+char(39)+'fas fa-ban'+char(39)+'></i> Reject</button>'  as [Value9]
			  ,(SELECT COUNT(*) FROM tray_compliance_line l WITH (NOLOCK) WHERE l.[Value1] = r.[Value1] AND l.[Value2] = r.[Value2] AND l.[Value3] = r.[Value3]) as 'itemcount' 
			  ,r.[CustomerName]
			  ,r.[ShipToAddr]
			  ,r.[City]
			  ,r.[State]
			  ,r.[Zip]
			  ,r.[ClientName]
			  ,r.[SlsLocn]
			  ,r.[OpsTeam]
			  ,r.[OrderDate]
				FROM [dbo].[tray_compliance_review] r WITH (NOLOCK) ";
				
			
			if(isset($_POST['val1']) && $_POST['val1']<>'')
			{
				$tsql=$main_tsql. " WHERE r.[Value1] = '".$_POST['val1']."' $filter_txt_qry order by r.tcr_id asc";
				$tsql_total_num = "SELECT count(r.[tcr_id]) as total_num FROM [dbo].[tray_compliance_review] r WITH (NOLOCK) WHERE r.[Value1] = '".$_POST['val1']."' $filter_txt_qry";
			}
            else 
            {
				$tsql=$main_tsql. " WHERE isnull(r.active,1) > 0  $filter_txt_qry order by r.tcr_id asc";
			}	
		   
	
   }
	else {
		echo("Wrong parameters !"); exit;
	}





/* ********************* added by Jobaidur :End ************************ */

//echo "<pre>"; print_r($_POST); echo "</pre>";

if($tsql_total_num!="")
	{
		$getResults_totalNum = sqlsrv_query($conn, $tsql_total_num);

		if ($getResults_totalNum == FALSE)
			echo (sqlsrv_errors());

		while ($row_totalNum = sqlsrv_fetch_array($getResults_totalNum, SQLSRV_FETCH_ASSOC)) {
			$pagination_total_num=$row_totalNum['total_num'];
		}
	}
	
if($pagination_total_num>35000)
{
	?>
<script type="text/javascript">
alert("Error Exporting, dataset too large, please try filtering data with Advanced Search or use pre-filtered Reports screen");
window.location = "<?php echo $redirect_url;?>";
</script>
<?php 
echo "Error Exporting, dataset too large, please try filtering data with Advanced Search or use pre-filtered Reports screen";
exit;
}


/* Processing query results */


if($_POST['export_view_type']=='orderreview' && $_POST['export_qry_type']=='VPdefault')
{
	

$file_name="Orderreview-orders.xlsx";	


$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("jahid0604015@gmail.com")
							 ->setTitle($file_name)
							 ->setSubject($file_name);

$row = 1;


$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Client Name')
                              ->setCellValue('B'.$row, 'Order')
							  ->setCellValue('C'.$row, 'Customer')
							  ->setCellValue('D'.$row, 'CustomerName')
							  ->setCellValue('E'.$row, 'Territory')
							  ->setCellValue('F'.$row, 'Order Date')
							  ->setCellValue('G'.$row, 'Sales Location')
							  ->setCellValue('H'.$row, 'Ops Team')
							  ->setCellValue('I'.$row, 'Ship To Address')
							  ->setCellValue('J'.$row, 'City')
							  ->setCellValue('K'.$row, 'State')
							  ->setCellValue('L'.$row, 'Zip');
							  
							   						  
$row++;



$sheet_one_num=60000;
$sheet_two_num=0;
if($pagination_total_num<=60000)
{
$sheet_one_num=$pagination_total_num;
$sheet_two_num=0;
}
else if($pagination_total_num>60000)
{
$sheet_two_num=$pagination_total_num;
}

for($total_rows=0;$total_rows<$sheet_one_num;$total_rows=$total_rows+5000)
{
	$max_limit=5000;
	$pagination_qry="OFFSET $total_rows ROWS FETCH NEXT $max_limit ROWS ONLY";
	$tsql_temp=$tsql." ".$pagination_qry;
	//echo "<br><hr>".$tsql_temp."<hr><br>"; exit;
	$getResults= sqlsrv_query($conn, $tsql_temp);
    if ($getResults == FALSE)
	echo (sqlsrv_errors());

    do {
     while ($each_val = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
				 
     $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $each_val['ClientName'])
                                  ->setCellValue('B'.$row, $each_val['Value1'])
								  ->setCellValue('C'.$row, $each_val['Value2'])
								  ->setCellValue('D'.$row, $each_val['CustomerName'])
								  ->setCellValue('E'.$row, $each_val['Value3'])
								  ->setCellValue('F'.$row, $each_val['OrderDate'])
								  ->setCellValue('G'.$row, $each_val['SlsLocn'])
								  ->setCellValue('H'.$row, $each_val['OpsTeam'])
								  ->setCellValue('I'.$row, $each_val['ShipToAddr'])
								  ->setCellValue('J'.$row, $each_val['City'])
								  ->setCellValue('K'.$row, $each_val['State'])
								  ->setCellValue('L'.$row, $each_val['Zip']);
								  				  	
    $row++;
	
     }
} while ( sqlsrv_next_result($getResults) );

sqlsrv_free_stmt( $getResults);

}

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setTitle('Sheet One');	


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$file_name.'"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');

header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
	
}
else if($_POST['export_view_type']=='orderreview' && $_POST['export_qry_type']=='VPitems')
{
	
$file_name="Orderreview-lines.xlsx";

	
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("jahid0604015@gmail.com")
							 ->setTitle($file_name)
							 ->setSubject($file_name);

$row = 1;

$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, 'Client Name')
                              ->setCellValue('B'.$row, 'Order')
							  ->setCellValue('C'.$row, 'Customer')
							  ->setCellValue('D'.$row, 'CustomerName')
							  ->setCellValue('E'.$row, 'Territory')
							  ->setCellValue('F'.$row, 'Order Date')
							  ->setCellValue('G'.$row, 'Sales Location')
							  ->setCellValue('H'.$row, 'Ops Team')
							  ->setCellValue('I'.$row, 'Item')
							  ->setCellValue('J'.$row, 'Item Description')
							  ->setCellValue('K'.$row, 'Qty Ordered')
							  ->setCellValue('L'.$row, 'Error Message');
							  
							   						  
$row++;



$sheet_one_num=60000;
$sheet_two_num=0;
if($pagination_total_num<=60000)
{
$sheet_one_num=$pagination_total_num;
$sheet_two_num=0;
}
else if($pagination_total_num>60000)
{
$sheet_two_num=$pagination_total_num;
}

for($total_rows=0;$total_rows<$sheet_one_num;$total_rows=$total_rows+5000)
{
	$max_limit=5000;
	$pagination_qry="OFFSET $total_rows ROWS FETCH NEXT $max_limit ROWS ONLY";
	$tsql_temp=$tsql." ".$pagination_qry;
	//echo "<br><hr>".$tsql_temp."<hr><br>"; exit;
	$getResults= sqlsrv_query($conn, $tsql_temp);
    if ($getResults == FALSE)
	echo (sqlsrv_errors());

    do {
     while ($each_val = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
				 
     $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $each_val['ClientName'])
                                  ->setCellValue('B'.$row, $each_val['Value1'])
								  ->setCellValue('C'.$row, $each_val['Value2'])
								  ->setCellValue('D'.$row, $each_val['CustomerName'])
								  ->setCellValue('E'.$row, $each_val['Value3'])
								  ->setCellValue('F'.$row, $each_val['OrderDate'])
								  ->setCellValue('G'.$row, $each_val['SlsLocn'])
								  ->setCellValue('H'.$row, $each_val['OpsTeam'])
								  ->setCellValue('I'.$row, $each_val['Value4'])
								  ->setCellValue('J'.$row, $each_val['Value5'])
								  ->setCellValue('K'.$row, $each_val['Value6'])
								  ->setCellValue('L'.$row, $each_val['Value7']);
								  
				  	
    $row++;
     }
} while ( sqlsrv_next_result($getResults) );

sqlsrv_free_stmt( $getResults);

}

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setTitle('Sheet One');	


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$file_name.'"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');

header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;	
}

   
function utf8ize($d) {
	if (is_array($d)) {
		foreach ($d as $k => $v) {
			$d[$k] = utf8ize($v);
		}
	} else if (is_string ($d)) {
		return utf8_encode($d);
	}
	return $d;
}

/* Free statement and connection resources. */

sqlsrv_free_stmt( $trkResults);
sqlsrv_close( $conn);
?>