<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
	session_start();
    include("prepend.php");
    include("settings.php");

    if(!isset($_SESSION['mhwltdphp_user'])){
        header("Location: login.php");
    }
	if($_SESSION['mhwltdphp_usertype'] != "SUPERUSER" && $_SESSION['mhwltdphp_usertype'] != "ADMIN" ){ 
		echo "Access Denied";
		//header("Location: ../mhwlogin.php");
		header("Location: home.php");
	} 
    include("dbconnect.php");
	$conn = sqlsrv_connect($serverName, $connectionOptions);
	if( $conn === false) {
		die( print_r( sqlsrv_errors(), true));
    }

    $postResult = "";
    if (!empty($_POST)) {
        $product_id = $_POST['prodID'];
        $product_code = $_POST['tfa_product_code'];
        $state_abbrev = $_POST['state_abbrev'];
        $distributor_key = $_POST['distributor_key'];
        $status = $_POST['status'];
        $res_type = $_POST['res_type'];

        if (!empty($product_id) && !empty($state_abbrev) && !empty($status)) {
            $tsql= "INSERT INTO [dbo].[mhw_app_prod_state_status]
                    VALUES (?, ?, ?, ?, ?, ?)
            ";
            $stmt = sqlsrv_query($conn, $tsql, array ( 
                $product_id,
                $state_abbrev,
                $distributor_key,
                $status,
                $_SESSION['mhwltdphp_user'],
                date("Y-m-d H:i:s")
            ));

            if( $stmt === false ) {
                $errorlog = "";
                if( ($errors = sqlsrv_errors() ) != null) {
                    //log sql errors for writing to server later
                    foreach( $errors as $error ) {
                        $errorlog.= $tsql."\n";
                        $errorlog.= "SQLSTATE: ".$error[ 'SQLSTATE']."\n";
                        $errorlog.= "code: ".$error[ 'code']."\n";
                        $errorlog.= "message: ".$error[ 'message']."\n";
                    }
                }
                $postResult = "Submit Error. ".$errorlog;
            } else {
                $postResult = "$product_code - successfully submitted";
            }
        }
        if ($res_type==="json") {
            header("Content-type:application/json");
            echo json_encode(["result"=>$postResult]);
            exit;
        }
    }

    /* get states */
	$statesArray = array();
	$tsql= "SELECT * FROM [dbo].[view_states]";
	$stmt = sqlsrv_query($conn, $tsql);
	if( $stmt ) {
		while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
			array_push($statesArray, $row);
		}
	}

    include('head.php');
?>

<link href="css/wforms-layout_v530-14.css" rel="stylesheet" type="text/css" />
<link href="css/theme-52661.css" rel="stylesheet" type="text/css" />
<link href="css/wforms-jsonly_v530-14.css" rel="alternate stylesheet" title="This stylesheet activated by javascript" type="text/css" />
<link href="css/wforms-inline.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/wforms_v530-14.js"></script>
<script type="text/javascript" src="js/localization-en_US_v530-14.js"></script>         


<style type="text/css">
    .item-not-found {color: #d21729 !important;}
    .validation-error {background-color: #ffc2ab !important;}
</style>


<script>

    var validate = function () {
        if ($("#prodID").val().length>0) {
            //return true;
            //let's send in ajax
            $("#res_type").val("json");
            var form_data = $("form").serializeArray();
            console.log(form_data);
            $.post("/forms/product_state_status.php",form_data,function(res){
				$("#postResult").html(res.result);  
				$("#prodID").val("");  
                $("#tfa_product_code").val("")
                $(".item-not-found").remove();
                $("#submit_button").val("Submit");
                $("#submit_button").prop('disabled', false);
            });
        } else {
            $("#tfa_product_code").parent().parent().parent().prepend($("<div class='item-not-found'>The product is not found, <a href='/forms/productsetup.php'>add a new one here</a></div>" ));
            $("#submit_button").val("Submit");
            $("#submit_button").prop('disabled', false);
            return false;
        }
        return false;
    }

	$(document).ready(function(){

        /*======BEGIN comboboxes==========*/
		function cbb_formatter(){ 
			$('ul.ac_results').css("margin-left","0em");
			$('ul.ac_results').css("margin","0em");
			$('.ac_button').css("height","28px");
			$('.ac_button').css("background","transparent");
			$('.ac_button').css("margin-left","-35px");
			$('.ac_button').css("border","0px");
			$('.ac_subinfo dl').css("margin-left","0em");
			$('.ac_subinfo dl').css("margin","0em");
        }
        
		function isProdValidSelection() {
            var isProdID = $('#tfa_product_code')[0].hasAttribute("sub_info"); //free-type data will not have sub_info attribute
            
			if ( isProdID ) {
                var json = $('#tfa_product_code').attr('sub_info').replace( /\'/g, '"' );
                try {
                    var jsonObj = JSON.parse(json);
                    $("#prodID").val(jsonObj.id);  //set field for processing update
                    $(".item-not-found").remove();
                } catch (error) {
                    isProdID = false;
                    console.log(error, json);
                }
            }

			if( !isProdID ) {
				$("#prodID").val("");  
                $(".item-not-found").remove();
                if ($("#tfa_product_code").val().length > 0) {
                    $("#tfa_product_code").parent().parent().parent().prepend($("<div class='item-not-found'>The product is not found, <a href='/forms/productsetup.php'>add a new one here</a></div>" ));
                }
			}
		}

		//initialize product combobox
		function initialize_product(){
			$('#tfa_product_code').parent().find(".ac_button").remove();  //clear elements of previous instance
			$('#tfa_product_code').parent().find(".ac_result_area").remove();  //clear elements of previous instance
			var cbbtype = 'product2';
			$.post('select-request.php', {
                cbbtype: cbbtype
            }, function(dataX) {
				var data = [];
				try {
					data = JSON.parse(dataX);
				} catch (error) {
					//location.reload();
					console.log(error, dataX);
				}
				$(function() {
					$('#tfa_product_code').ajaxComboBox(
						data,
						{
							bind_to: 'tfa_product_code',
							sub_info: true,
							sub_as: {
								id: 'Product ID',
								ttb: 'TTB ID'
							},
						}
					).bind('tfa_product_code', function() {
                        var json = $(this).attr('sub_info').replace( /\'/g, '"' );
                        try {
                            var jsonObj = JSON.parse(json);
                            if(parseInt(jsonObj.id) > 0){
                                $("#prodID").val(jsonObj.id); //set field for processing update
                            }
                            isProdValidSelection();
                        } catch (error) {
                            console.error(error);
                        }
					});
					cbb_formatter(); //apply css and classname changes to combobox elements
				});
			});
			$('#tfa_product_code').bind("change", function(){ 
				isProdValidSelection();
			});
		}
        initialize_product();
        
        $('#submit_button').click(function() { 
            validate();
        });

        function loadDistributors() {
            console.log($('#state_abbrev').val());
			$.post('query-request.php', {
                qry_type: "distributors-list",
                state_abbrev: $('#state_abbrev').val()
            }, function(res) {

                var distributors = [];
                try {
                    distributors = JSON.parse(res);
                } catch (error) {
                    //location.reload(); // not authorized?
                    console.log(error, res);
                }
                console.log(distributors);

                $("#distributor_key").html('');
                $("#distributor_key").append('<option value="all">All distributors</option>');
                $("#distributor_key").append(
                    //distributors.filter(d=>d.State===state).map(d=>`<option value="${d.DistKey}">${d.Name}</option>`).join('\n')
                    distributors.map(d=>`<option value="${d.DistKey}">${d.Name}</option>`).join('\n')
                );

            });


        }
        $('#state_abbrev').change(loadDistributors);
        loadDistributors();

    });

</script>


<div class="wFormContainer" style="max-width: 85%; width:auto;" >
    <div class="row">
        <div class="col-sm-12">
            
            <div class="wFormHeader"></div>
            <h3 class="wFormTitle">Distributor Registration Cancel - Product / State / Distributor / Status </h3>
            <h5 id="postResult"><?=$postResult?></h5>

            <form method="post" onsubmit="return validate()" >
                <div class="row justify-content-md-left float-left">
                    <div id="tfa_856" class="section inline group">
                        <div class="oneField field-container-D">
                            <label class="label preField reqMark" for="tfa_product_code">Product Code</label><br>
                            <div class="inputWrapper">
                                <input type="text" id="tfa_product_code" name="tfa_product_code" value="" title="Product Code" data-dataset-allow-free-responses="">
                            </div>
                            <br/>

                            <label class="label preField " for="state_abbrev">State</label>
                            <div class="inputWrapper">
                                <select id="state_abbrev" name="state_abbrev" title="State" style="width: 20em;" aria-required="true" class="required">
                                    <?php foreach ($statesArray as $state) { ?>
                                        <option value="<?=$state['state_abbrev']?>"><?=$state['state_name']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <br/>

                            <label class="label preField " for="distributor_key">Disributor</label>
                            <div class="inputWrapper">
                                <select id="distributor_key" name="distributor_key" title="Disributor" style="width: 20em;">
                                    <option value="all">All distributors</option>
                                </select>
                            </div>
                            <br/>

                            <label class="label preField " for="status">Status</label>
                            <div class="inputWrapper">
                                <select id="status" name="status" title="Status" style="width: 20em;">
                                    <option>Active</option>
                                    <option>Inactive</option>
                                </select>
                            </div>
                            <br/>

                            <input type="hidden" name="res_type" id="res_type" value="">
                            <input type="hidden" name="prodID" id="prodID" value="">
                            <div class="actions">
                                <input type="button" data-label="Submit" class="btn btn-primary primaryAction" id="submit_button" value="Submit">
                            </div>
                        </div>
                    </div>
                </div>

            </form>

        </div>
    </div>
</div>

<?php
    include('footer.php');
?>
    