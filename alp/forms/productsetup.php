<?php
//session_start();
include("sessionhandler.php");
include("prepend.php");
include("settings.php");
if(!isset($_SESSION['mhwltdphp_user'])){

	header("Location: login.php");
}
else{
	include('head.php');

	$isAdmin = in_array($_SESSION['mhwltdphp_usertype'], ["SUPERUSER", "ADMIN"]);

	//echo "<pre>";
	//var_dump($_REQUEST); 
	//echo "</pre>"; exit;
	$product_finalized=2;
		
	if($_POST['referringform']=='productsetup.php'){
		include('productsetup_processor.php');
	}
     
 
	if(isset($_POST['product_id'])){
		$prodID = $_POST['product_id'];
		$product_mhw_code = $_POST['product_mhw_code'];
		$product_mhw_code_search = $_POST['product_mhw_code_search'];
		$pf_client_name = $_POST['client_name'];
		$pf_brand_name = $_POST['brand_name'];
		$pf_product_desc = $_POST['product_desc'];	
		$product_finalized = $_POST['product_finalized'];		
	}

	include("dbconnect.php");
	$conn = sqlsrv_connect($serverName, $connectionOptions);
	if( $conn === false) {
		die( print_r( sqlsrv_errors(), true));
	}

	
		
	
		
	/* *************************************** Fetching Federal_type and MKTG_PROD_TYPE : start *************  */    
	$federal_mktg_prod_list = array();
	$federal_type_opts=array();
	$mktg_prod_type_opts=array();
	$conn = sqlsrv_connect($serverName, $connectionOptions);
	if( $conn === false) {
		die( print_r( sqlsrv_errors(), true));
	}
	
	$federal_mktg_map = "select federal_type,mktg_prod_type from mhw_app_federal_type_mktg_prod_map where active=1 order by id asc";
	$federal_mktg_opts = sqlsrv_query($conn, $federal_mktg_map);

	if ($federal_mktg_opts == FALSE)
		echo (sqlsrv_errors());

	while ($row_federal_mktg_opts = sqlsrv_fetch_array($federal_mktg_opts, SQLSRV_FETCH_ASSOC)) {
		$temp_federal_type=$row_federal_mktg_opts['federal_type'];
		$federal_mktg_prod_list[$temp_federal_type][]=$row_federal_mktg_opts['mktg_prod_type'];
		array_push($federal_type_opts, $row_federal_mktg_opts['federal_type']);
		array_push($mktg_prod_type_opts, $row_federal_mktg_opts['mktg_prod_type']);		
	}	
	sqlsrv_free_stmt($federal_mktg_opts);
	
	$federal_type_opts = array_values(array_unique($federal_type_opts));
	$mktg_prod_type_opts = array_values(array_unique($mktg_prod_type_opts));
	
	/* *************************************** Fetching Federal_type and MKTG_PROD_TYPE : end ********************** */	

	
	


/* *************************************** Fetching Federal_type and Compliance Type : start *************  */    
	$federal_com_type_list = array();
	$com_type_opts=array();
	$conn = sqlsrv_connect($serverName, $connectionOptions);
	if( $conn === false) {
		die( print_r( sqlsrv_errors(), true));
	}
	
	$federal_com_type_map = "select federal_type,compliance_type from mhw_app_federal_type_comp_type_map where active=1 order by id asc";
	$federal_comtype_opts = sqlsrv_query($conn, $federal_com_type_map);

	if ($federal_comtype_opts == FALSE)
		echo (sqlsrv_errors());

	while ($row_federal_comtype_opts = sqlsrv_fetch_array($federal_comtype_opts, SQLSRV_FETCH_ASSOC)) {
		$temp_federal_type=$row_federal_comtype_opts['federal_type'];
		$federal_com_type_list[$temp_federal_type][]=$row_federal_comtype_opts['compliance_type'];
		array_push($com_type_opts, $row_federal_comtype_opts['compliance_type']);		
	}	
	sqlsrv_free_stmt($federal_comtype_opts);
	
	$com_type_opts = array_values(array_unique($com_type_opts));
	
	/* *************************************** Fetching Federal_type and Compliance Type : end ********************** */	


	
	

   /* **************************************** Vintage dropdown options generating : Start ************************* */	
   
    $vintage_start_year=1898;
	$current_year=date("Y");
	$vintage_end_year=$current_year+5;
	$vintage_dropdown_options="<option value=''>Please select...</option><option value='0000'>NV</option>";

	for($sy=$vintage_start_year;$sy<=$vintage_end_year;$sy++)
	{
		$vintage_dropdown_options=$vintage_dropdown_options."<option value='$sy'>$sy</option>";
	}
   
   /* **************************************** Vintage dropdown options generating : End ************************* */	
   
   
?>

<!DOCTYPE HTML>
<html lang="en-US">
<head>

    <title>Product &amp; Item Specifications Form</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function(){
            const FORM_TIME_START = Math.floor((new Date).getTime()/1000);
            let formElement = document.getElementById("tfa_0");
            if (null === formElement) {
                formElement = document.getElementById("0");
            }
            let appendJsTimerElement = function(){
                let formTimeDiff = Math.floor((new Date).getTime()/1000) - FORM_TIME_START;
                let cumulatedTimeElement = document.getElementById("tfa_dbCumulatedTime");
                if (null !== cumulatedTimeElement) {
                    let cumulatedTime = parseInt(cumulatedTimeElement.value);
                    if (null !== cumulatedTime && cumulatedTime > 0) {
                        formTimeDiff += cumulatedTime;
                    }
                }
                let jsTimeInput = document.createElement("input");
                jsTimeInput.setAttribute("type", "hidden");
                jsTimeInput.setAttribute("value", formTimeDiff.toString());
                jsTimeInput.setAttribute("name", "tfa_dbElapsedJsTime");
                jsTimeInput.setAttribute("id", "tfa_dbElapsedJsTime");
                jsTimeInput.setAttribute("autocomplete", "off");
                if (null !== formElement) {
                    formElement.appendChild(jsTimeInput);
                }
            };
            if (null !== formElement) {
                if(formElement.addEventListener){
                    formElement.addEventListener('submit', appendJsTimerElement, false);
                } else if(formElement.attachEvent){
                    formElement.attachEvent('onsubmit', appendJsTimerElement);
                }
            }
        });
		</script>
		<script>
		var itemMhwCodes = [];
		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();

			<?php
			$tsql = "SELECT DISTINCT item_mhw_code FROM mhw_app_prod_item
				WHERE active = 1 AND item_mhw_code <> '' 
				ORDER BY item_mhw_code";
			$stmt = sqlsrv_query($conn, $tsql);

			if ($stmt) {
				while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
				?>
					itemMhwCodes.push('<?=$row['item_mhw_code']?>');
				<?php
				}
			}
			sqlsrv_free_stmt($stmt);
			?>

			function checkItemMhwCode() {
				if ($('#clientcodeoverride').is(":checked") && itemMhwCodes.includes($("#tfa_client_code").val())) {
					$('#tfa_client_code_warning').text('This code is already in use');

					$('#clientcodeoverride').prop( "checked", false );
				} else {
					$('#tfa_client_code_warning').text('');
				}
			}

			//$("#tfa_client_code").on('change', checkItemMhwCode);
			$('#tfa_client_code').bind("change", function() {
				checkItemMhwCode();
			});
			//$("#clientcodeoverride").on('change', checkItemMhwCode);
			$('#clientcodeoverride').bind("change", function() {
				checkItemMhwCode();
			});

			/*======SPECIAL CHARACTER PREVENTION======*/
			var regex = /^[A-Za-z0-9 \' \- \. \, \( \) \\ \@ \& \# \+ \! \* \: \/]+$/;
			var regexALERT = "Only Latin Alphabet [A-Z], Numbers [0-9] and limited special characters [-+,.:\\/@&#!*'()] are allowed.";

			function isIE(){
			  var ua = window.navigator.userAgent;

			  return ua.indexOf('MSIE ') > 0 || ua.indexOf('Trident/') > 0 || ua.indexOf('Edge/') > 0
			}
			$("input").keypress(function (e) {
				var keyCode = e.keyCode || e.which;
	 
				//$("#lblError").html("");
	 
				//Regex for Valid Characters i.e. Alphabets and Numbers.
				//var regex = /^[A-Za-z0-9 \- \. \, \( \) \\ \& \# \+ \! \* \: \/]+$/;
	 
				//Validate TextBox value against the Regex.
				var isValid = regex.test(String.fromCharCode(keyCode));
				if (!isValid) {
					//$("#lblError").html("Only Alphabets and Numbers allowed.");
					alert(regexALERT);
				}
	 
				return isValid;
			});
			/*======SPECIAL CHARACTER PREVENTION======*/

			/*======BEGIN INITIAL PAGE LOAD======*/
			
			function itemConditionals(){
				//default wine is hidden and not required
				var wineSet = false;
				$(".WineSection").hide();  
				$(".WineVintage").removeClass("required");
				$(".WineVintagelbl").removeClass("reqMark");
				//Wine selection on product toggles Wine fields on items(s)
				$(".federalType option:selected" ).each(function() {
				  var thistxt = $( this ).text();
				  if(thistxt=="Wine"){ 
					 wineSet = true;
					 $(".WineSection").show();
				  }
				  if(thistxt=="Distilled Spirits"){ 
					 wineSet = false;
					 $(".WineSection").show();
				  }
				});
				//cycle through all item fields in an item section
				$(".itmfldst").each(function() {
					var anyItmSet = false;
					var useClientItemCode = false;
					var $fldset = $(this);
					//if any item-level required field is set
					$fldset.find(".itmobj").each(function() {
						if($(this).val()!=""){
							anyItmSet = true;
						}
					});
					$fldset.find(".clientcodeoverride").each(function() {
						//if($(this).val()=="1"){
						if ( $(this).checked || $(this).prop( "checked" ) || $(this).is( ":checked" ) ){
							useClientItemCode = true;
						}
						else{
							useClientItemCode = false;
						}
					});
					$fldset.find(".client_code").each(function() {
						if(!useClientItemCode){
							$(this).removeClass( "required" );  //not checked, clear client item code requirement
						}
						else{
							$(this).addClass( "required" );  //set client item code as required
						}
					});
					$fldset.find(".client_code_lbl").each(function() {
						if(!useClientItemCode){
							$(this).removeClass( "reqMark" );  //not checked, clear required from label
						}
						else{
							$(this).addClass( "reqMark" ); //set label as required
						}
					});
					//toggle item validation requirements for all item-level required fields in section
					$fldset.find(".itmobj").each(function() {
						if(!anyItmSet){
							$(this).removeClass( "required" );  //nothing set, clear all item requirements including wine
						}
						else{
							$(this).addClass( "required" );  //something is set, set required
							if(wineSet){
								if($(this).hasClass("WineVintage")){
									$(this).addClass("required");   //wine is set
								}
							}
							else{
								if($(this).hasClass("WineVintage")){
									$(this).removeClass("required"); //wine is not set
								}
							}
						}
					});
					//toggle item label asterisk for all item-level labels
					$fldset.find(".itmlbl").each(function() {
						if(!anyItmSet){
							$(this).removeClass( "reqMark" );  //nothing set, clear all item requirements including wine
						}
						else{
							$(this).addClass( "reqMark" );
							if(wineSet){
								if($(this).hasClass("WineVintagelbl")){
									$(this).addClass("reqMark"); //wine is set
								}
							}
							else{
								if($(this).hasClass("WineVintagelbl")){
									$(this).removeClass("reqMark"); //wine is not set
								}
							}
						}
					});
				});
			}
			itemConditionals();
	
			function itemDescFormat(obj){

				var $fldset =  obj.parents('fieldset');
				var itemseq1 = $fldset.attr('id');
				var itemseq2 = itemseq1.replace("]","");
				var itemseq3 = itemseq2.replace("tfa_825[","");
				

				var mhwitmdesc = "";
				var mhwitmdesc1 = "";
				var mhwitmdesc2 = "";
				var mhwitemdesc2LEN = 0;
				var prodfedtype = $('#tfa_federal_type').val();
				var itmcontainertype = "";
				var itmcontainersize = "";
				var itmstockuom = "";
				var itmoutershipper = "";
				var lenNotTaken = 60;

				var itmclientdesc = $('#product_mhw_desc').val();

				var anyItmSet = false;
				//if any item-level required field is set
				$fldset.find(".itmobj").each(function() {
					if($(this).val()!=""){
						anyItmSet = true;
					}
				});

				var itmvintage =  $('select[name="tfa_vintage['+itemseq3+']"]').val();
				
				
				$fldset.find('.container_type').each(function() {
					var thisid = $(this).attr('id');
					if(thisid=='tfa_container_type['+itemseq3+']'){
						itmcontainertype = $(this).val();
					}
				});
				
				$fldset.find('.container_size').each(function() {
					var thisid = $(this).attr('id');
					if(thisid=='tfa_container_size['+itemseq3+']'){
						itmcontainersize = $(this).val();
					}
				});

				$fldset.find('.stock_uom').each(function() {
					var thisid = $(this).attr('id');
					if(thisid=='tfa_stock_uom['+itemseq3+']'){
						itmstockuom = $(this).val();
					}
				});

				$fldset.find('.outer_shipper').each(function() {
					var thisid = $(this).attr('id');
					if(thisid=='tfa_outer_shipper['+itemseq3+']'){
						itmoutershipper = $(this).val();
					}
				});
				
				//strip trailing zeros from size (covers last char of double zero and trailing zero from decimal values)
				itmcontainersize = itmcontainersize.replace('0 Lit','L');
				itmcontainersize = itmcontainersize.replace('0 Gal','G');
				itmcontainersize = itmcontainersize.replace('0 NA','NA');
				itmcontainersize = itmcontainersize.replace('0 ML','ML');
				itmcontainersize = itmcontainersize.replace('0 OZ','OZ');
				//strip spaces between size and units
				itmcontainersize = itmcontainersize.replace(' ','');
				//strip zero right after decimal (covers first char of double zero, while retaining decimals with zero before final non-zero character)
				itmcontainersize = itmcontainersize.replace('.0Lit','.L');
				itmcontainersize = itmcontainersize.replace('.0Gal','.G');
				itmcontainersize = itmcontainersize.replace('.0L','.L');
				itmcontainersize = itmcontainersize.replace('.0G','.G');
				itmcontainersize = itmcontainersize.replace('.0NA','.NA');
				itmcontainersize = itmcontainersize.replace('.0ML','.ML');
				itmcontainersize = itmcontainersize.replace('.0OZ','.OZ');
				//abbreviate units if not already done during zero handling
				itmcontainersize = itmcontainersize.replace('.Lit','L');
				itmcontainersize = itmcontainersize.replace('.Gal','G');
				itmcontainersize = itmcontainersize.replace('.L','L');
				itmcontainersize = itmcontainersize.replace('.G','G');
				itmcontainersize = itmcontainersize.replace('.NA','NA');
				itmcontainersize = itmcontainersize.replace('.ML','ML');
				itmcontainersize = itmcontainersize.replace('.OZ','OZ');
				//abbreviate units if not already done during zero handling
				itmcontainersize = itmcontainersize.replace('Lit','L');
				itmcontainersize = itmcontainersize.replace('Gal','G');

				//itmcontainersizeLAST = itmcontainersize.charAt(itmcontainersize.length-1);
				//alert(itmcontainersizeLAST);
				//if(itmcontainersizeLAST=="0"){ 
				//		itmcontainersize = itmcontainersize.substring(0,itmcontainersize.length-1); 
				//}

				//var itmstockuom = $('input[name="tfa_stock_uom['+itemseq3+']"]').val();
				if(itmstockuom && itmstockuom!=''){
				var itmstockuom4 = itmstockuom.substring(0,4);
				itmstockuom = itmstockuom.toUpperCase();
				itmstockuom4 = itmstockuom4.toUpperCase();
				}

				var itmbottlespercase = $('input[name="tfa_bottles_per_case['+itemseq3+']"]').val();
				
				if(itmcontainertype=='Bottle'){
					if(itmstockuom=='CASE' || itmstockuom4=='CASE'){
						if(prodfedtype=="Wine"){
							mhwitmdesc2 = itmvintage + ' ' + itmcontainersize + '-' + itmbottlespercase;
						}
						else if(prodfedtype=="Distilled Spirits" && itmvintage!=''){
							mhwitmdesc2 = itmvintage + ' ' + itmcontainersize + '-' + itmbottlespercase;
						}
						else{
							mhwitmdesc2 = itmcontainersize+'-'+itmbottlespercase;
						}
					}
					else if(itmstockuom=='BOTTLE'){
						if(prodfedtype=="Wine"){
							mhwitmdesc2 = itmvintage + ' ' + itmcontainersize + '-BT';
						}
						else if(prodfedtype=="Distilled Spirits" && itmvintage!=''){
							mhwitmdesc2 = itmvintage + ' ' + itmcontainersize + '-BT';
						}
						else{
							mhwitmdesc2 = itmcontainersize+'-BT';
						}
					}

					if(itmoutershipper=='WOOD'){ mhwitmdesc2 = mhwitmdesc2 + ' WOOD'; }

					mhwitemdesc2LEN = mhwitmdesc2.length;
					mhwitmdesc1 = itmclientdesc.substring(0,59-mhwitemdesc2LEN); //59 because sequence starts at 0
					mhwitmdesc = mhwitmdesc1 + ' ' + mhwitmdesc2;
					lenNotTaken = 59-mhwitemdesc2LEN; //59 because format includes a space between substring item desc and suffixes
				}
				else if(itmcontainertype=='Can'){
					if(itmstockuom=='CASE' || itmstockuom4=='CASE'){
						if(prodfedtype=="Wine"){
							mhwitmdesc2 = itmvintage + ' ' + itmcontainersize + ' CAN-' + itmbottlespercase;
						}
						else if(prodfedtype=="Distilled Spirits" && itmvintage!=''){
							mhwitmdesc2 = itmvintage + ' ' + itmcontainersize + ' CAN-' + itmbottlespercase;
						}
						else{
							mhwitmdesc2 = itmcontainersize+' CAN-'+itmbottlespercase;
						}
					}
					else if(itmstockuom=='BOTTLE'){
						if(prodfedtype=="Wine"){
							mhwitmdesc2 = itmvintage + ' ' + itmcontainersize + ' CAN';
						}
						else if(prodfedtype=="Distilled Spirits" && itmvintage!=''){
							mhwitmdesc2 = itmvintage + ' ' + itmcontainersize + ' CAN';
						}
						else{
							mhwitmdesc2 = itmcontainersize+' CAN';
						}
					}

					if(itmoutershipper=='WOOD'){ mhwitmdesc2 = mhwitmdesc2 + ' WOOD'; }

					mhwitemdesc2LEN = mhwitmdesc2.length;
					mhwitmdesc1 = itmclientdesc.substring(0,59-mhwitemdesc2LEN); //59 because sequence starts at 0
					mhwitmdesc = mhwitmdesc1 + ' ' + mhwitmdesc2;
					lenNotTaken = 59-mhwitemdesc2LEN; //59 because format includes a space between substring item desc and suffixes
				}
				else if(itmcontainertype=='Keg' || itmcontainertype=='Keg-oneway' || itmcontainertype=='Keg-deposit'){
					mhwitmdesc2 = itmcontainersize+' KEG';
					mhwitemdesc2LEN = mhwitmdesc2.length;
					mhwitmdesc1 = itmclientdesc.substring(0,59-mhwitemdesc2LEN); //59 because sequence starts at 0
					mhwitmdesc = mhwitmdesc1 + ' ' + mhwitmdesc2;
					lenNotTaken = 59-mhwitemdesc2LEN; //59 because format includes a space between substring item desc and suffixes
				}
				else if(itmcontainertype=='Other'){
					mhwitmdesc2 = itmcontainersize+' '+itmcontainertype;
					mhwitemdesc2LEN = mhwitmdesc2.length;
					mhwitmdesc1 = itmclientdesc.substring(0,59-mhwitemdesc2LEN); //59 because sequence starts at 0
					mhwitmdesc = mhwitmdesc1 + ' ' + mhwitmdesc2;
					lenNotTaken = 59-mhwitemdesc2LEN; //59 because format includes a space between substring item desc and suffixes
				}
				else{
					mhwitmdesc = itmclientdesc;
					lenNotTaken = 60-mhwitemdesc2LEN;
				}
				/*
				//length indicator / character left hint
				var lenDesc = itmclientdesc.length;
				$('input[name="tfa_item_description['+itemseq3+']"]').attr('maxlength', lenNotTaken);
				$fldset.find('.item_description_hint').html('Up to '+ lenNotTaken + ' characters');
				//$('input[name="tfa_item_description['+itemseq3+']-HH"]')
				$fldset.find('.lengthIndicator').first().html(lenNotTaken-lenDesc + " characters left.");
				*/

				if(anyItmSet){
					$('input[name="tfa_mhw_item_description['+itemseq3+']"]').val(mhwitmdesc);
				}

			}

			//when federal type changes, toggle wine conditional fields
			$( ".federalType" ).change(function() {
				itemConditionals();

				$(".container_type").each(function() { //choose each of 1st required field in the item fieldset so that the function can access the parent fieldset and check all fields within
					itemDescFormat($(this));
				});
				
				/* onchange federal type, change the MKTG Prod Type */
				 var selected_federal_type=$('#tfa_federal_type').find(":selected").text();
				 // first time federal type empty and mktg is empty 
				 if(selected_federal_type=="Please select...")
				 {
					var output = [];
					output.push('<option value="">Please select...</option>');
					$('#tfa_mktg_prod_type').html(output.join('')); 
				 }
				 else 
				 {
				    var federal_mktg_prod_list_js=<?php echo json_encode($federal_mktg_prod_list); ?>;
					var selected_mktg_list=federal_mktg_prod_list_js[selected_federal_type];
					var output = [];
					output.push('<option value="">Please select...</option>');
					
					$.each(selected_mktg_list, function(key, value)
					{
					  output.push('<option value="'+ value +'">'+ value +'</option>');
					});
					$('#tfa_mktg_prod_type').html(output.join(''));					
				 }
				
				// onchange federal type, load mktg 
				// if already federal type has value then load mktg accordingly. 
				
				
								
				/* onchange federal type, change the Compliance Type */				 
				 // first time federal type empty and Compliance Type is empty 
				 if(selected_federal_type=="Please select...")
				 {
					var output = [];
					output.push('<option value="">Please select...</option>');
					$('#tfa_compliance_type').html(output.join('')); 
				 }
				 else 
				 {
				    var federal_compliance_list_js=<?php echo json_encode($federal_com_type_list); ?>;
					var selected_compliance_list=federal_compliance_list_js[selected_federal_type];
					var output = [];
					output.push('<option value="">Please select...</option>');
					
					$.each(selected_compliance_list, function(key, value)
					{
					  output.push('<option value="'+ value +'">'+ value +'</option>');
					});
					$('#tfa_compliance_type').html(output.join(''));					
				 }
				
				// onchange federal type, load Compliance Type 
				// if already federal type has value then load Compliance Type accordingly. 

				
			});

			$(document).off('change', '.itmobj').on('change', '.itmobj',function(e) {
				//when any item field changes, toggle other item required fields
				itemConditionals();

				itemDescFormat($(this));
				
			});

			$(document).off('change', '.clientcodeoverride').on('change', '.clientcodeoverride',function(e) {
				//when any client item code override field changes, toggle other item required fields
				itemConditionals();	
			});

			//$(document).off('keyup', '.item_description').on('keyup', '.item_description',function(e) {
			$(document).off('change', '#product_mhw_desc').on('change', '#product_mhw_desc',function(e) {
			//$(document).off('change', '#tfa_product_desc').on('change', '#tfa_product_desc',function(e) {

				//form assembly WFORMS core defaultly updates lengthIndictor for (maxlength - fieldValue.length) in field hint on keydown.  Using Keyup here to override hint value afterwards
				$(".container_type").each(function() { //choose each of 1st required field in the item fieldset so that the function can access the parent fieldset and check all fields within
					itemDescFormat($(this));
				});
			});

			$(document).off('keyup', '#tfa_product_desc').on('keyup', '#tfa_product_desc',function(e) {
				$(".container_type").each(function() { //choose each of 1st required field in the item fieldset so that the function can access the parent fieldset and check all fields within

					itemDescFormat($(this));
				});
			});
			
			//Chill Storage - Item
			$('.chillnote').hide();
			$(document).off('change', '.chill_storage').on('change', '.chill_storage',function(e) {
				var chillval = $(this).val();
				var $fldsetx = $(this).parents('fieldset');
				var itemseq1x = $fldsetx.attr('id');
				var itemseq2x = itemseq1x.replace("]","");
				var itemseq3x = itemseq2x.replace("tfa_825[","");
				if (chillval=='CHILL')
				{

					$fldsetx.find('.chillnote').each(function() {
						var thisidx = $(this).attr('id');
						if(thisidx=='tfa_chill_storage-HH['+itemseq3x+']'){
							$(this).show();
						}
					});
				}
				else{

					$fldsetx.find('.chillnote').each(function() {
						var thisidx = $(this).attr('id');
						if(thisidx=='tfa_chill_storage-HH['+itemseq3x+']'){
							$(this).hide();
						}
					});
				}
			});

			//remove image requirement if requirement previously met
			var imgReq = $('#imgSet').val();
			if(parseInt(imgReq) >= 1){
				$(".fileobj").each(function() {
				  $(this).removeClass( "required" );
				});
				$(".filelbl").each(function() {
				  $(this).removeClass( "reqMark" );
				});
			}
			//if product id is passed to page, preload with data
			var prodIDinit = $("#prodID").val();
			if(parseInt(prodIDinit) > 0){
				prefill_prod(prodIDinit);

				cbb_formatter();
			}
			//function to toggle button in Image section
			function TTBlink(){
				var ttbUrl = '<?php echo $ttblink; ?>';  //defined in settings.php
				var ttbVal = $("#tfa_TTB").val();
				var ttbInt = parseInt(ttbVal);  //TTB value must be integer to be valid, but may begin with leading zeros so use text value for url
				if(ttbVal!="" && ttbVal!="P" && ttbVal!="p" && ttbInt){
					$(".ttbLinkDiv").html("<label id='tfa_ttbLink[0]-L' class='label preField ' for='tfa_ttbLink[0]'>View COLA Details:</label> <div class='inputWrapper'> <a href='"+ttbUrl+ttbVal+"' class='btn btn-outline-primary btn-sm ttbLink' target='_blank'><i class='fas fa-file-contract'></i> TTB Site <i class='fas fa-external-link-square-alt'></i><a/></div>");
				}
				else{
					$(".ttbLinkDiv").html(""); //clear button for invalid or 'P' (pending) TTB ID
				}
			}
			TTBlink(); //run on page load
			$(document).off('change', '#tfa_TTB').on('change', '#tfa_TTB',function(e) {
				TTBlink(); //run when TTB ID changes
			});

			//function to alert of production form submission - FormsDev ignored
			function alertTray(qrytype){
				var trayURL = '<?php echo $trayFormSubmitWorkflow; ?>';  //defined in settings.php
				var referringform = "<?php echo $_POST['referringform']; ?>"; //productsetup form submitted post-back
				var prodID = "<?php echo $prodID; ?>";
				var pf_client_name = "<?php echo $client_name; ?>";
				if(referringform=='productsetup.php'){
					$.post(trayURL, {qrytype:qrytype,prodlist:prodID,clientlist:pf_client_name}, function(dataT){ 
						// ajax request tray web hook, send qrytype, product & client as post variable
					});
				}
			}
			alertTray('formSubmitted'); //run on page load, post-back requirement is validated in-function

			/*======END INITIAL PAGE LOAD======*/

			/*======BEGIN comboboxes==========*/
			function cbb_formatter(){ 
				//$('#tfa_brand_name').addClass('combobox_restyle');
				//$('#tfa_brand_name').css( "border-radius", "0px" );
				$('ul.ac_results').css("margin-left","0em");
				$('ul.ac_results').css("margin","0em");
				$('.ac_button').css("height","28px");
				$('.ac_button').css("background","transparent");
				$('.ac_button').css("margin-left","-35px");
				$('.ac_button').css("border","0px");
				$('.ac_subinfo dl').css("margin-left","0em");
				$('.ac_subinfo dl').css("margin","0em");
			}
			
			/* adding field value as new selected options if data not existed */
			function make_new_val_selected(field_id,field_value)
						{
							if($(""+field_id+" option[value='"+field_value+"']").length > 0)
							{
								$(field_id).val(field_value);
							}
							else 
							{
								$(field_id).append($('<option>', {
									value: field_value,
									text: field_value
								}));
								
								$(field_id).val(field_value);
							}
							
						}
						
			//load form fields - as a result of selecting product_desc combobox
			function prefill_prod(prodID){
				var qrytype = "prodByID";
				var client = $('#tfa_client_name').val(); //get value of the client combobox
				$.post('query-request.php', {qrytype:qrytype,prodID:prodID,client:client}, function(dataPx){ // ajax request query-request.php, send brand as post variable, return dataX variable, parse into JSON

					var t = JSON.parse(dataPx);

					$.each( t, function( key, value ) {
						//product
						$('#tfa_TTB').val(value.TTB);
						//$('#tfa_federal_type').val(value.federal_type);
						make_new_val_selected("#tfa_federal_type",value.federal_type);
												
						$('#product_mhw_code').val(value.product_mhw_code);
						$('#product_mhw_code_search').val(value.product_mhw_code_search);
						$('#product_mhw_desc').val(value.product_desc);
						$('#product_finalized').val(value.finalized);
						
						if(value.finalized==0 || value.finalized=='0')
						{
							//removeReqAtt();
						}	
						
                         generating_compliance_options(value.federal_type,value.compliance_type);
                         generating_mktg_prod_options(value.federal_type,value.mktg_prod_type);
						//wineConditionals();
						//itemRequirements();
						itemConditionals();
						TTBlink();

						//$('#tfa_compliance_type').val(value.compliance_type);
						make_new_val_selected("#tfa_compliance_type",value.compliance_type);
						//$('#tfa_product_class').val(value.product_class);
						make_new_val_selected("#tfa_product_class",value.product_class);
						//$('#tfa_mktg_prod_type').val(value.mktg_prod_type);
						make_new_val_selected("#tfa_mktg_prod_type",value.mktg_prod_type);
						//$('#tfa_bev_type').val(value.bev_type);
						make_new_val_selected("#tfa_bev_type",value.bev_type);
						
						$('#tfa_fanciful').val(value.fanciful);
						$('#tfa_country').val(value.country);
						$('#tfa_appellation').val(value.appellation);
						$('#tfa_lot_item').val(value.lot_item);
						//$('#tfa_bottle_material').val(value.bottle_material);
						$('#tfa_alcohol_pct').val(value.alcohol_pct);
						$('#tfa_varietal').val(value.varietal);
						
						//files which satisfy requirement
						$('#imgSet').val(value.filecount);
						if(parseInt(value.filecount) >= 1){
							$(".fileobj").each(function() {
							  $(this).removeClass( "required" );
							});
							$(".filelbl").each(function() {
							  $(this).removeClass( "reqMark" );
							});
						}
						
						//supplier
						$('#suppID').val(value.supplier_id);
						if(parseInt(value.supplier_id) > 0){
							$("#tfa_1020-L").html("Supplier Information: editing supplier_id "+value.supplier_id); //display message for edit/insert
						}
						$('#tfa_supplier_name').val(value.supplier_name);
						$('#tfa_supplier_contact').val(value.supplier_contact);
						$('#tfa_supplier_fda_number').val(value.supplier_fda_number);
						$('#tfa_tax_reduction_allocation').val(value.tax_reduction_allocation);
						$('#tfa_supplier_address_1').val(value.supplier_address_1);
						$('#tfa_supplier_address_2').val(value.supplier_address_2);
						$('#tfa_supplier_address_3').val(value.supplier_address_3);
						$('#tfa_supplier_city').val(value.supplier_city);
						$('#tfa_supplier_state').val(value.supplier_state);
						$('#tfa_supplier_country').val(value.supplier_country);
						$('#tfa_supplier_zip').val(value.supplier_zip);
						$('#tfa_supplier_phone').val(value.supplier_phone);
						$('#tfa_supplier_email').val(value.supplier_email);
						$('#tfa_supplier_fed_permit').val(value.federal_basic_permit);
					});
					//update item description with product desc
					$(".container_type").each(function() { //choose each of 1st required field in the item fieldset so that the function can access the parent fieldset and check all fields within
			//alert('prefill');				
						itemDescFormat($(this));
					});
				});
			}
			//load form fields - as a result of selecting supplier_name combobox
			function prefill_supp(suppID){
				var qrytype = "suppByID";
				var client = $('#tfa_client_name').val(); //get value of the client combobox
				$.post('query-request.php', {qrytype:qrytype,suppID:suppID,client:client}, function(dataSx){ // ajax request query-request.php, send brand as post variable, return dataX variable, parse into JSON

					var t = JSON.parse(dataSx);

					$.each( t, function( key, value ) {
						//supplier
						$('#tfa_supplier_name').val(value.supplier_name);
						$('#tfa_supplier_contact').val(value.supplier_contact);
						$('#tfa_supplier_fda_number').val(value.supplier_fda_number);
						$('#tfa_tax_reduction_allocation').val(value.tax_reduction_allocation);
						$('#tfa_supplier_address_1').val(value.supplier_address_1);
						$('#tfa_supplier_address_2').val(value.supplier_address_2);
						$('#tfa_supplier_address_3').val(value.supplier_address_3);
						$('#tfa_supplier_city').val(value.supplier_city);
						$('#tfa_supplier_state').val(value.supplier_state);
						$('#tfa_supplier_country').val(value.supplier_country);
						$('#tfa_supplier_zip').val(value.supplier_zip);
						$('#tfa_supplier_phone').val(value.supplier_phone);
						$('#tfa_supplier_email').val(value.supplier_email);
						$('#tfa_supplier_fed_permit').val(value.federal_basic_permit);
					});
				});
			}
			function isProdValidSelection(){
				var isProdID = $('#tfa_product_desc')[0].hasAttribute("sub_info"); //free-type data will not have sub_info attribute
				if(isProdID){
					var json = $('#tfa_product_desc').attr('sub_info');
					//if additional sub_info elements are added, additional apostrophe replacements need to be added
					var json2 = json.replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"');
					var jsonObj = JSON.parse(json2);

					$("#prodID").val(jsonObj.id);  //set field for processing update
					$("#tfa_399-L").html("Product: editing "+jsonObj.code); //display message for update
					prefill_prod(jsonObj.id); //prefill form fields
				}
				else{
					$("#tfa_399-L").html("Product: new entry"); //display message for insert
					$("#prodID").val("");  //clear field for insert
					$('#product_mhw_code').val("");
					$('#product_mhw_code_search').val("");
					$("#product_mhw_desc").val($('#tfa_product_desc').val());

					$(".container_type").each(function() { //choose each of 1st required field in the item fieldset so that the function can access the parent fieldset and check all fields within
			//alert('nomatch');			
						itemDescFormat($(this));
					});

					//new product entry enforce image requirement
					$(".fileobj").each(function() {
					  $(this).addClass( "required" );
					});
					$(".filelbl").each(function() {
					  $(this).addClass( "reqMark" );
					});

				}
			}
			function isSuppValidSelection(){
				var isSuppID = $("#tfa_supplier_name")[0].hasAttribute("sub_info"); //free-type data will not have sub_info attribute
				if(isSuppID){
					var jsonS =  $("#tfa_supplier_name").attr('sub_info');
					//if additional sub_info elements are added, additional apostrophe replacements need to be added
					var json2S = jsonS.replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"');
					var jsonObjS = JSON.parse(json2S);

					$("#suppID").val(jsonObjS.id);  //set field for processing update
					$("#tfa_1020-L").html("Supplier Information: editing supplier_id "+jsonObjS.id); //display message for update
					prefill_supp(jsonObjS.id); //prefill form fields
				}
				else{
					$("#tfa_1020-L").html("Supplier Information: new entry");  //display message for insert
					$("#suppID").val("");  //clear field for insert
				}
			}

			//initialize brand combobox
			function initialize_brand(){
				<?php if($_POST['edit']) { ?>
					return;
				<?php } ?>

				$('#tfa_brand_name').parent().find(".ac_button").remove();  //clear elements of previous instance
				$('#tfa_brand_name').parent().find(".ac_result_area").remove();  //clear elements of previous instance
				var client = $('#tfa_client_name').val(); //get value of the client combobox
				var cbbtype = 'brand';
				$.post('select-request.php', {cbbtype:cbbtype,client:client}, function(dataX){ // ajax request select-request.php, send the client POST variable, return dataX variable, parse as JSON
					var data = JSON.parse(dataX);
					$(function() {
						$('#tfa_brand_name').ajaxComboBox(
							data
						);
						cbb_formatter(); //apply css and classname changes to combobox elements
					});
				});
				$('#tfa_brand_name').bind("change", function() {
					initialize_product();

					isProdValidSelection();

					isSuppValidSelection();

					cbb_formatter(); //apply css and classname changes to combobox elements
				});
			}
			initialize_brand();
			
			//initialize product combobox
			function initialize_product(){
				<?php if($_POST['edit']) { ?>
					return;
				<?php } ?>

				$('#tfa_product_desc').parent().find(".ac_button").remove();  //clear elements of previous instance
				$('#tfa_product_desc').parent().find(".ac_result_area").remove();  //clear elements of previous instance
				var client = $('#tfa_client_name').val(); //get value of the client combobox
				var brand = $('#tfa_brand_name').val(); //get value of the brand combobox
				var cbbtype = 'product';
				$.post('select-request.php', {cbbtype:cbbtype,client:client,brand:brand}, function(dataX){ // ajax request select-request.php, send brand as post variable, return dataX variable, parse into JSON
					var data = JSON.parse(dataX);
					$(function() {
						$('#tfa_product_desc').ajaxComboBox(
							data,
							{
							  bind_to: 'tfa_product_desc',
							  sub_info: true,
							  sub_as: {
								id: 'Product ID',
								code: 'MHW Product Code',
								ttb: 'TTB ID'
							  },
							}
						).bind('tfa_product_desc', function() {
						  var json = $(this).attr('sub_info');
						  //if additional sub_info elements are added, additional apostrophe replacements need to be added
						  var json2 = json.replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"');
						  var jsonObj = JSON.parse(json2);

						  if(parseInt(jsonObj.id) > 0){
							$("#prodID").val(jsonObj.id); //set field for processing update
							$("#tfa_399-L").html("Product: editing "+jsonObj.code); //display message for update
							prefill_prod(jsonObj.id); //prefill form fields
						  }
						  isProdValidSelection();

						});
						cbb_formatter(); //apply css and classname changes to combobox elements
					});
				});
				$('#tfa_product_desc').bind("change", function(){ 
					isProdValidSelection();
				});
			}
			initialize_product();

			//initialize supplier combobox
			function initialize_supplier(){
				$('#tfa_supplier_name').parent().find(".ac_button").remove();  //clear elements of previous instance
				$('#tfa_supplier_name').parent().find(".ac_result_area").remove();  //clear elements of previous instance
				var client = $('#tfa_client_name').val(); //get value of the client combobox
				var cbbtype = 'supplier';
				$.post('select-request.php', {cbbtype:cbbtype,client:client}, function(dataX){ // ajax request select-request.php, send client as post variable, return dataX variable, parse into JSON
					var data = JSON.parse(dataX);
					$(function() {
						$('#tfa_supplier_name').ajaxComboBox(
							data,
							{
							  bind_to: 'tfa_supplier_name',
							  sub_info: true,
							  sub_as: {
								id: 'Supplier ID'
							  },
							}
						).bind('tfa_supplier_name', function() {
						  var json = $(this).attr('sub_info');
						  //if additional sub_info elements are added, additional apostrophe replacements need to be added
						  var json2 = json.replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"');
						  var jsonObj = JSON.parse(json2);

						  if(parseInt(jsonObj.id) > 0){
							$("#suppID").val(jsonObj.id);  //set field for processing update
							$("#tfa_1020-L").html("Supplier Information: editing supplier_id "+jsonObj.id); //display message for update
							prefill_supp(jsonObj.id); //prefill form fields
						  }

						});
						cbb_formatter(); //apply css and classname changes to combobox elements
					});
				});
			}
			initialize_supplier();

			//independent of combobox selection binding, listen for non-select user entry
			$('#tfa_supplier_name').change(function(){ 
				isSuppValidSelection();
			}); 

			//client combobox >> brand combobox
			$('#tfa_client_name').change(function(){ //if client value changes
				//replace combobox elements with original input
				$('#tfa_brand_name').parent().parent().html("<input type=\"text\" id=\"tfa_brand_name\" name=\"tfa_brand_name\" value=\"\" aria-required=\"true\" title=\"Brand Name\" data-dataset-allow-free-responses=\"\" class=\"required\">");
				//replace combobox elements with original input
				$('#tfa_product_desc').parent().parent().html("<input type=\"text\" id=\"tfa_product_desc\" name=\"tfa_product_desc\" value=\"\" aria-required=\"true\" title=\"Product Name\" data-dataset-allow-free-responses=\"\" class=\"required\">");

				initialize_brand();
				
				//re-initialize product combobox
				initialize_product();
				
				//re-initialize supplier combobox
				initialize_supplier();

				isProdValidSelection();

				isSuppValidSelection();

				cbb_formatter(); //apply css and classname changes to combobox elements
			});
			/*======END comboboxes==========*/
			//clear form
			$("#clearform").click(function() {		
				$('#prodID').val('');
				$('#product_mhw_code').val('');
				$('#product_mhw_code_search').val('');
				$('#product_mhw_desc').val('');
				$('#suppID').val('');
				$('#itemReq').val('');
				$('#imgSet').val('');
				
				$('#tfa_TTB').val('');
				$('#tfa_federal_type').val('');
				$('#tfa_compliance_type').val('');
				$('#tfa_product_class').val('');
				$('#tfa_mktg_prod_type').val('');
				$('#tfa_bev_type').val('');
				$('#tfa_fanciful').val('');
				$('#tfa_country').val('');
				$('#tfa_appellation').val('');
				$('#tfa_lot_item').val('');
				//$('#tfa_bottle_material').val('');
				$('#tfa_alcohol_pct').val('');
				$('#tfa_varietal').val('');
	
				$('#tfa_supplier_name').val('');
				$('#tfa_supplier_contact').val('');
				$('#tfa_supplier_fda_number').val('');
				$('#tfa_tax_reduction_allocation').val('');
				$('#tfa_supplier_address_1').val('');
				$('#tfa_supplier_address_2').val('');
				$('#tfa_supplier_address_3').val('');
				$('#tfa_supplier_city').val('');
				$('#tfa_supplier_state').val('');
				$('#tfa_supplier_country').val('');
				$('#tfa_supplier_zip').val('');
				$('#tfa_supplier_phone').val('');
				$('#tfa_supplier_email').val('');
				$('#tfa_supplier_fed_permit').val('');

				$(".fileobj").each(function() {
				  $(this).addClass( "required" );
				});
				$(".filelbl").each(function() {
				  $(this).addClass( "reqMark" );
				});

				$("#tfa_399-L").html("Product: new entry"); //display message for insert
				$("#tfa_1020-L").html("Supplier Information: new entry"); //display message for insert

				//replace combobox elements with original input
				$('#tfa_brand_name').parent().parent().html("<input type=\"text\" id=\"tfa_brand_name\" name=\"tfa_brand_name\" value=\"\" aria-required=\"true\" title=\"Brand Name\" data-dataset-allow-free-responses=\"\" class=\"required\">");
				//replace combobox elements with original input
				$('#tfa_product_desc').parent().parent().html("<input type=\"text\" id=\"tfa_product_desc\" name=\"tfa_product_desc\" value=\"\" aria-required=\"true\" title=\"Product Name\" data-dataset-allow-free-responses=\"\" class=\"required\">");

				initialize_brand();

				//$('#tfa_brand_name').bind("change", function() {
					 // alert( message );
				//});

				initialize_product();

				//$('#tfa_product_desc').bind("change", function() {
					 // alert( message );
				//});

				//hide message when unchecking checkbox
				$('.chillnote').hide();

				itemConditionals();
				TTBlink();

				cbb_formatter();

			});

			//$('.thefile').change(function(){    
			$(document).off('change', '.thefile').on('change', '.thefile',function(e) {
				//alert('change');
				//on change event  
				var filesizelimit =  <?php echo $size_bytes_limit; ?> / 1024;
				var filesize;
				var filevalidated = 1;
				formdata = new FormData();
				if($(this).prop('files').length > 0)
				{
					$.each(this.files, function (index, value) {
						filesize = Math.round((value.size / 1024));
						if(filesize > filesizelimit) {  
							alert('File size is too large. Limit is '+filesizelimit+' KB');
							filevalidated = 0;
						}
					})
					if(filevalidated==1){
					file =$(this).prop('files')[0];
					formdata.append("tfa_image", file);

					jQuery.ajax({
						url: "ajax_save_file.php?from=productsetup",
						type: "POST",
						data: formdata,
						processData: false,
						contentType: false,
						success: function (result) {
							 // if all is well
							 //alert(result);
						}
					});
				}
					else{
						$(this).val(null);
					}
				}
			});
			
	function generating_compliance_options(selected_federal_type,select_compliance_type)
				 {
				 // first time federal type empty and compliance type is empty 
				 if(selected_federal_type=="Please select...")
				 {  
					var output = [];
					output.push('<option value="">Please select...</option>');
					$('#tfa_compliance_type').html(output.join('')); 
				 }
				 else 
				 {
				    var federal_compliance_list_js=<?php echo json_encode($federal_com_type_list); ?>;
					var selected_compliance_list=federal_compliance_list_js[selected_federal_type];
					var output = [];
					
					if(!select_compliance_type || select_compliance_type=="")
					{
						output.push('<option value="">Please select...</option>');
					}
					
					$.each(selected_compliance_list, function(key, value)
					{
					  output.push('<option value="'+ value +'">'+ value +'</option>');
					});
					
					$('#tfa_compliance_type').html(output.join(''));	
					
					if(select_compliance_type && select_compliance_type!="")
					{
						$('#tfa_compliance_type').val(select_compliance_type);
					}
							
				 }
				 }

			
			function generating_mktg_prod_options(selected_federal_type,select_mktg_prod_type)
				 {
				 // first time federal type empty and mktg is empty 
				 if(selected_federal_type=="Please select...")
				 {
					var output = [];
					output.push('<option value="">Please select...</option>');
					$('#tfa_mktg_prod_type').html(output.join('')); 
				 }
				 else 
				 {
				    var federal_mktg_prod_list_js=<?php echo json_encode($federal_mktg_prod_list); ?>;
					var selected_mktg_list=federal_mktg_prod_list_js[selected_federal_type];
					var output = [];
					
					if(!select_mktg_prod_type || select_mktg_prod_type=="")
					{
						output.push('<option value="">Please select...</option>');
					}
					
					$.each(selected_mktg_list, function(key, value)
					{
					  output.push('<option value="'+ value +'">'+ value +'</option>');
					});
					
					$('#tfa_mktg_prod_type').html(output.join(''));	
					
					if(select_mktg_prod_type && select_mktg_prod_type!="")
					{
						$('#tfa_mktg_prod_type').val(select_mktg_prod_type);
					}
							
				 }
				 }
				 
				 
			function removeReqAtt()
				{
					$('form#4690986 input, form#4690986 select').each(
						function(index){  
							var input = $(this);
							input.removeAttr('required');
						}
					);
				}
				 
		});
    </script>

    <link href="css/wforms-layout_v530-14.css" rel="stylesheet" type="text/css" />
    <link href="css/theme-52661.css" rel="stylesheet" type="text/css" />
    <link href="css/wforms-jsonly_v530-14.css" rel="alternate stylesheet" title="This stylesheet activated by javascript" type="text/css" />
	<link href="css/wforms-inline.css" rel="stylesheet" type="text/css" />
	<link href="css/last.css" rel="stylesheet" type="text/css" />
	<?php if (!$isAdmin && ($product_finalized==0 || $product_finalized=="0")) { 
	}
	else if(!$isAdmin)
	{
	?>
	<script type="text/javascript" src="js/wforms_v530-14.js"></script>
    <script type="text/javascript" src="js/localization-en_US_v530-14.js"></script> 
    <?php 	
	}		
	?>
    
</head>
<body class="default wFormWebPage">
<div class="container-fluid">
<div id="tfaContent">
	<div class="wFormContainer" style="max-width: 85%; width:auto;" >
    		<div class="wFormHeader"></div>
    		

		<div class="">
			<div class="wForm" id="4690986-WRPR" dir="ltr">
				<div class="codesection" id="code-4690986"></div>
				<?php if($isAdmin && isset($_POST['product_id'])) { ?>
				<a target="_blank" class="pull-right" href="audit-trail-view.php?product_id=<?=$_POST['product_id']?>">View Audit Trail</a>
				<? } ?>
				<h3 class="wFormTitle" id="4690986-T">Product &amp; Item Specifications Form</h3>
				<form method="post" action="productsetup.php" class="hintsBelow labelsAbove" id="4690986" role="form" enctype="multipart/form-data">
				
				<div class="oneField field-container-D     wf-acl-hidden" id="tfa_396-D">
					<label id="tfa_396-L" class="label preField " for="tfa_396">Account ID</label><br>
					<div class="inputWrapper"><input type="text" id="tfa_396" name="tfa_396" value="<?php echo $_GET['acctid']; ?>" title="Account ID" data-dataset-allow-free-responses="" class=""></div>
				</div>
				<div class="htmlSection" id="tfa_824">
					<div class="htmlContent" id="tfa_824-HTML">
						<span style="font-size: 14.4px;"><b>Instructions</b><br><br>Welcome to the MHW Product &amp; Items Specifications form. Please carefully review and complete all required information. If you have any questions, please contact your client account manager.</span><br style="font-size: 14.4px;"><br style="font-size: 14.4px;"><span style="font-size: 14.4px;">While working through this form, please keep the following in mind:</span><br style="font-size: 14.4px;"><ul style="font-size: 14.4px;"><li>A&nbsp;<u style="font-size: 14.4px; word-spacing: normal;">new vintage, new configuration (</u><span style="font-size: 14.4px; word-spacing: normal;">e.g., CASE12</span><u style="font-size: 14.4px; word-spacing: normal;">),</u><span style="font-size: 14.4px; word-spacing: normal;"> or </span><u style="font-size: 14.4px; word-spacing: normal;">new size (</u><span style="font-size: 14.4px; word-spacing: normal;">e.g., 750ML</span><u style="font-size: 14.4px; word-spacing: normal;">)</u><span style="font-size: 14.4px; word-spacing: normal;">, requires a&nbsp;</span><u style="font-size: 14.4px; word-spacing: normal;">NEW</u><span style="font-size: 14.4px; word-spacing: normal;">&nbsp;item be added to the form to an <u>existing</u>&nbsp;product</span></li><li><span style="font-size: 14.4px; word-spacing: normal;">A completely new product requires a <u>NEW</u>&nbsp;product to be added to the form followed by any associated items</span></li><li>Brand names should match what appears on the COLA</li><li>TTB ID is required (14-digit number on the approved label)</li><li>Please use this form to submit <u>NEW</u>&nbsp;products and items only. To update information for existing items; please send those changes to <a href="mailto:dataintegrity@mhwltd.com">dataintegrity@mhwltd.com</a>.  To submit an updated COLA for an existing product, send your COLA to <a href="mailto:compliance@mhwltd.com">compliance@mhwltd.com</a>.  Please provide the item and/or product ID that you are updating with your request.  Updates will be processed within 2 business days.  Additionally, please send your requests for Value-added packs/gift packs/combo packs to <a href="mailto:dataintegrity@mhwltd.com">dataintegrity@mhwltd.com</a>.</li></ul>
					</div>
				</div>
				<div class="oneField field-container-D    " id="tfa_client_name-D">
					<label id="tfa_client_name-L" class="label preField " for="tfa_client_name"><b>Client Name</b></label><br>
					<div class="inputWrapper">
						<select id="tfa_client_name" name="tfa_client_name" title="Client Name" aria-required="true" class="required">
						<?php
						if ($isAdmin) { 
						$clients = explode(";",$_SESSION['mhwltdphp_userclients']);
							array_push($clients,$pf_client_name);
						}
						else{
							$clients = explode(";",$_SESSION['mhwltdphp_userclients']);
						}
						
						foreach ($clients as &$clientvalue) {
							if(isset($pf_client_name) && $pf_client_name==$clientvalue){
								echo "<option value=\"".$clientvalue."\" class=\"\" selected>".$clientvalue."</option>";
							}
							else{
						    		echo "<option value=\"".$clientvalue."\" class=\"\">".$clientvalue."</option>";
							}
						}
						?>					
						</select>
					</div>
					<?php
					if($_SESSION['mhwltdphp_userclients']=='' || $_SESSION['mhwltdphp_userclients']==' '){
						echo "<span id=\"loginmsg\">There was an error authenticating your login.  Please <a href='https://mhwltd.app/logout.php'>logout</a> and try again.</span>";
						echo "<span id=\"loginmsg\" style=\"display:none;\">".count($clients)."</span>";
					}
					?>
					<div class="float-right"><button type="reset" value="Reset" id="clearform" class="btn btn-sm btn-outline-danger">Clear Form</button></div>
				</div>
	
<!--BEGIN PRODUCT-->				
				<!--<fieldset id="tfa_399" class="repeat section" data-repeatlabel="Add a new product">-->
				<fieldset id="tfa_399" class="section" >
				<?php
					if($prodID > 0){ echo "<legend id=\"tfa_399-L\">Product: editing product_id ".$prodID."</legend>"; }
					else{  echo "<legend id=\"tfa_399-L\">Product: new entry</legend>"; }
				?>
				<div id="tfa_856" class="section inline group">
					<div class="oneField field-container-D    " id="tfa_brand_name-D">
						<label id="tfa_brand_name-L" class="label preField reqMark" for="tfa_brand_name">Brand Name</label><br>
						<div class="inputWrapper"><input type="text" id="tfa_brand_name" name="tfa_brand_name" value="<?php echo $pf_brand_name; ?>" aria-required="true" title="Brand Name" data-dataset-allow-free-responses="" class="required"></div>
					</div>
					<div class="oneField field-container-D    " id="tfa_product_desc-D">
						<label id="tfa_product_desc-L" class="label preField reqMark" for="tfa_product_desc">Product Description</label><br>
						<div class="inputWrapper"><input type="text" id="tfa_product_desc" name="tfa_product_desc" value="<?php echo $pf_product_desc; ?>" aria-required="true" title="Product Description" data-dataset-allow-free-responses="" class="required"></div>
					</div>
					<div class="oneField field-container-D    " id="tfa_TTB-D">
						<label id="tfa_TTB-L" class="label preField reqMark" for="tfa_TTB">TTB ID</label><br>
						<div class="inputWrapper"><input type="text" id="tfa_TTB" name="tfa_TTB" value="<?php echo $pf_TTB_ID; ?>" aria-required="true" aria-describedby="tfa_TTB-HH" title="TTB ID" data-dataset-allow-free-responses="" class="validate-alphanum required"><span class="field-hint-inactive" id="tfa_TTB-H"><span id="tfa_TTB-HH" class="hint">(if pending, enter "P")</span></span>
						</div>
					</div>
				</div>
				<div id="tfa_400" class="section inline group">
					<div class="oneField field-container-D    " id="tfa_federal_type-D">
						<label id="tfa_federal_type-L" class="label preField reqMark" for="tfa_federal_type">Federal Type</label><br>
						<div class="inputWrapper">
							<select id="tfa_federal_type" name="tfa_federal_type" title="Federal Type" aria-required="true" class="required federalType">
							<option value="">Please select...</option>
							
							<?php
							//$federal_type_opts = array('Ciders','Distilled Spirits','Malt Beverage','Non Alcohol','Wine');
							
							foreach ($federal_type_opts as &$federal_type_optsVALUE) {
								if(isset($pf_federal_type) && $pf_federal_type==$federal_type_optsVALUE){
									if($federal_type_optsVALUE=="Wine"){
										echo "<option value=\"".$federal_type_optsVALUE."\" class=\"wineOption\" selected>".$federal_type_optsVALUE."</option>";
									}
									else{
										echo "<option value=\"".$federal_type_optsVALUE."\" class=\"\" selected>".$federal_type_optsVALUE."</option>";
									}
									
								}
								else{
									if($federal_type_optsVALUE=="Wine"){
							    			echo "<option value=\"".$federal_type_optsVALUE."\" class=\"wineOption\">".$federal_type_optsVALUE."</option>";
									}
									else{
										echo "<option value=\"".$federal_type_optsVALUE."\" class=\"\">".$federal_type_optsVALUE."</option>";
									}
								}
							}
							?>
							</select>
						</div>
					</div>
					<div class="oneField field-container-D    " id="tfa_compliance_type-D">
						<label id="tfa_compliance_type-L" class="label preField reqMark" for="tfa_compliance_type">Compliance Type</label><br>
						<div class="inputWrapper">
							<select id="tfa_compliance_type" name="tfa_compliance_type" title="Compliance Type" aria-required="true" class="required">
							<option value="">Please select...</option>
							<?php
								//$compliance_type_opts = array('Cider','Wine based Cider','Malt Based Cider','Spirits','Mixed Drinks','Malt Beverages','Non Alcohol','Wine','Wine Product','Low Alcohol Wine','Domestic Sake');
								
								if($prodID > 0)
								{
									$compliance_type_opts=$com_type_opts;
								}
								else 
								{
									$compliance_type_opts=array();
								}
								
								foreach ($compliance_type_opts as &$compliance_type_optsVALUE) {
									if(isset($pf_compliance_type) && $pf_compliance_type==$compliance_type_optsVALUE){
										echo "<option value=\"".$compliance_type_optsVALUE."\" class=\"\" selected>".$compliance_type_optsVALUE."</option>";
									}
									else{
								    		echo "<option value=\"".$compliance_type_optsVALUE."\" class=\"\">".$compliance_type_optsVALUE."</option>";
									}
								}
							?>
							</select>
						</div>
					</div>
					<div class="oneField field-container-D    " id="tfa_product_class-D">
						<label id="tfa_product_class-L" class="label preField reqMark" for="tfa_product_class">Product Classification</label><br>
						<div class="inputWrapper">
							<select id="tfa_product_class" name="tfa_product_class" title="Product Classification" aria-required="true" class="required">
							<option value="">Please select...</option>
							<option value="Imported" id="tfa_14" class="" <?php if(isset($pf_product_class) && ($pf_product_class=="Imported" || $pf_product_class=="Foreign")){ echo "selected"; } ?>>Imported</option>
							<option value="Domestic" id="tfa_15" class="" <?php if(isset($pf_product_class) && $pf_product_class=="Domestic"){ echo "selected"; } ?>>Domestic</option></select>
						</div>
					</div>
					<div class="oneField field-container-D    " id="tfa_mktg_prod_type-D">
						<label id="tfa_mktg_prod_type-L" class="label preField reqMark" for="tfa_mktg_prod_type">Marketing Category</label><br>
						<div class="inputWrapper">
							<select id="tfa_mktg_prod_type" name="tfa_mktg_prod_type" title="Marketing Category" aria-required="true" class="required">
							<option value="">Please select...</option>
							
							<?php
							//$mktg_prod_type_opts = array("Absinthe","Akvavit","Applejack","Arak","Arrack","Awamori","Baijiu","Beer","Bitters","Borovicka","Brandy","Brandy - Armagnac","Brandy - Cognac","Brandy - Eau-de-vie","Brandy - Fruit","Brandy - Grappa","Brandy - Palinka","Brandy - Pisco","Brandy - Rakia","Brandy - Singani","Brandy - Slivovitz","Brandy - Tuica","Cachaca","Cauim","Champagne","Chicha","Cider","Cider - Flavored","Coolers","Cordials and Liqueurs","Cream Liqueurs","Desi Daru","Flavored Malt Beverages/Coolers","Fortified","Fortified - Madeira","Fortified - Marsala","Fortified - Port","Fortified - Sherry","Fortified - Tonto","Fortified - Vermouth","Fruit Liqueur - Tepache","General","Gin","Horilka","Huangjiu","Icariine Liquor","Kaoliang","Kasiri","Kilju","Kumis","ManX Spirit","Maotai","Mead","Metaxa","Mezcal","Mixers","Neutral Grain Spirit","Nihamanchi","Non-Alcohol","Non-Grape Wine","Ogogoro","Other","Ouzo","Palm wine","Parakari","Raki","Rice Wine","Rose","RTD / Prepared Cocktails","Rum","Rum - Flavored","Rum - Mamajuana","Sake","Sakurá","Sangria","Schnapps","Shochu","Soju","Sonti","Sparkling","Sparkling - Cava","Specialty Spirit","Table Flavored","Table Flavored - Pulque","Table Red","Table White","Tequila","Tiswin","Vinsanto","Vodka","Vodka - Flavored","Water","Whiskey","Whiskey - American","Whiskey - Bourbon","Whiskey - Flavored","Whiskey - Foreign","Whiskey - Irish","Whiskey - Moonshine","Whiskey - Poitín","Whiskey - Rye","Whiskey - Tenessee","Whisky - Canadian","Whisky - Japanese","Whisky - Scotch","Wine - Champagne","Wine - Coolers","Wine - Fortified","Wine - Fortified - Port","Wine - Fortified - Sherry","Wine - Fortified - Vermouth","Wine - Red","Wine - Rose","Wine - Sake","Wine - Sparkling","Wine - White","Wine Product");
							if($prodID > 0)
							{
								$mktg_prod_type_opts_init=$mktg_prod_type_opts;
							}
							else 
							{
								$mktg_prod_type_opts_init=array();
							}
							
							if(isset($pf_mktg_prod_type) && $pf_mktg_prod_type=="GENERAL"){ $pf_mktg_prod_type = "General"; } 
							if(isset($pf_mktg_prod_type) && $pf_mktg_prod_type=="BEER"){ $pf_mktg_prod_type = "Beer"; } 
							if(isset($pf_mktg_prod_type) && $pf_mktg_prod_type=="CIDER"){ $pf_mktg_prod_type = "Cider"; } 
							if(isset($pf_mktg_prod_type) && $pf_mktg_prod_type=="Whiskey - Canadian"){ $pf_mktg_prod_type = "Whisky - Canadian"; } 
							if(isset($pf_mktg_prod_type) && $pf_mktg_prod_type=="Whiskey - Scotch"){ $pf_mktg_prod_type = "Whisky - Scotch"; } 

							foreach ($mktg_prod_type_opts_init as &$mktg_prod_type_optsVALUE) {
								if(isset($pf_mktg_prod_type) && $pf_mktg_prod_type==$mktg_prod_type_optsVALUE){
									echo "<option value=\"".$mktg_prod_type_optsVALUE."\" class=\"\" selected>".$mktg_prod_type_optsVALUE."</option>";
								}
								else{
							    		echo "<option value=\"".$mktg_prod_type_optsVALUE."\" class=\"\">".$mktg_prod_type_optsVALUE."</option>";
								}
							}
							
							?>
							</select>
						</div>
					</div>
					<div class="oneField field-container-D    " id="tfa_bev_type-D">
						<label id="tfa_bev_type-L" class="label preField reqMark" for="tfa_bev_type">Beverage Type</label><br>
						<div class="inputWrapper">
							<select id="tfa_bev_type" name="tfa_bev_type" title="Beverage Type" aria-required="true" class="required">
							<option value="">Please select...</option>
							
							<?php
							$bev_type_opts = array("Carbonated Wine","Dessert Fruit Wine","Dessert Flavored Wine","Dessert Port/Sherry/(Cooking)","Wine Low Alcohol","Other Wine","Sparkling Wine/Champagne","Table Fruit Wine","Table Flavored Wine","Table Red and Rose Wine","Table White Wine","Vermouth","Cider","Beer","Brandy","Cordials &amp; Liqueurs","Cocktails &amp; Specialties","Gin","Imitations","Neutral Spirits","Other Liquor","Tequila","Rum","Whiskey");
							foreach ($bev_type_opts as &$bev_type_optsVALUE) {
								if(isset($pf_bev_type) && $pf_bev_type==$bev_type_optsVALUE){
									echo "<option value=\"".$bev_type_optsVALUE."\" class=\"\" selected>".$bev_type_optsVALUE."</option>";
								}
								else{
							    		echo "<option value=\"".$bev_type_optsVALUE."\" class=\"\">".$bev_type_optsVALUE."</option>";
								}
							}
							
							?>
							</select>
						</div>
					</div>
					<div class="oneField field-container-D    " id="tfa_varietal-D">
						<label id="tfa_varietal-L" class="label preField " for="tfa_varietal">Varietal</label><br>
						<div class="inputWrapper"><input type="text" id="tfa_varietal" name="tfa_varietal" value="<?php echo $pf_varietal; ?>" title="Varietal" data-dataset-allow-free-responses="" class=""></div>
					</div>
				</div>
				
				<div id="tfa_403" class="section inline group">
					<div class="oneField field-container-D    " id="tfa_fanciful-D">
						<label id="tfa_fanciful-L" class="label preField " for="tfa_fanciful">Fanciful (if any)</label><br>
						<div class="inputWrapper"><input type="text" id="tfa_fanciful" name="tfa_fanciful" value="<?php echo $pf_fanciful; ?>" title="Fanciful (if any)" data-dataset-allow-free-responses="" class=""></div>
					</div>
					<div class="oneField field-container-D    " id="tfa_country-D">
						<label id="tfa_country-L" class="label preField reqMark" for="tfa_country">Country of Origin</label><br>
						<div class="inputWrapper">
							<!--<input type="text" id="tfa_country" name="tfa_country" value="<?php echo $pf_country; ?>" aria-required="true" title="Country of Origin" data-dataset-allow-free-responses="" class="required">-->
							<select id="tfa_country" name="tfa_country" title="Country of Origin" aria-required="true" class="required">
							<option value="">Please select...</option>
							<?php
							//////////////////////////
							$country_opts = array();

							$tsql_country = "select DISTINCT REPLACE([Desc],'\"','') + ';;' + [Code] as [countryCombo] from [dbo].[country-list] ORDER BY [countryCombo]";
							$getResults_country = sqlsrv_query($conn, $tsql_country);

							if ($getResults_country == FALSE)
								echo (sqlsrv_errors());

							while ($row_country = sqlsrv_fetch_array($getResults_country, SQLSRV_FETCH_ASSOC)) {
								array_push($country_opts,$row_country['countryCombo']);
							}

							sqlsrv_free_stmt($getResults_country);
							
							foreach ($country_opts as &$country_optsVALUE) {
								$country_optsSPLIT = explode(";;",$country_optsVALUE);
								if(isset($pf_country) && $pf_country==$country_optsSPLIT[1]){
									echo "<option value=\"".$country_optsSPLIT[1]."\" class=\"\" selected>".ucwords($country_optsSPLIT[0])."</option>";
								}
								else{
							    		echo "<option value=\"".$country_optsSPLIT[1]."\" class=\"\">".ucwords($country_optsSPLIT[0])."</option>";
								}
							}
							//////////////////////////

							?>
							</select>
						</div>
					</div>
					<div class="oneField field-container-D    " id="tfa_appellation-D">
						<label id="tfa_appellation-L" class="label preField" for="tfa_appellation">Appellation</label><br>
						<div class="inputWrapper">
							<input type="text" id="tfa_appellation" name="tfa_appellation" value="<?php echo $pf_appellation; ?>" title="Appellation" data-dataset-allow-free-responses="" class="">
						</div>
					</div>
					<div class="oneField field-container-D    " id="tfa_lot_item-D">
						<label id="tfa_lot_item-L" class="label preField " for="tfa_lot_item">Lot Item</label><br>
						<div class="inputWrapper">
							<select id="tfa_lot_item" name="tfa_lot_item" title="Lot Item" class="">
							<option value="">Please select...</option>
							<option value="Yes" id="tfa_850" class="" <?php if(isset($pf_lot_item) && $pf_lot_item=="Yes"){ echo "selected"; } ?>>Yes</option>
							</select>
						</div>
					</div>
					
					<div class="oneField field-container-D    " id="tfa_alcohol_pct-D">
						<label id="tfa_alcohol_pct-L" class="label preField reqMark" for="tfa_alcohol_pct">Alcohol %</label><br>
						<div class="inputWrapper">
							<input type="text" id="tfa_alcohol_pct" name="tfa_alcohol_pct" value="<?php echo $pf_alcohol_pct; ?>" aria-required="true" aria-describedby="tfa_alcohol_pct-HH" title="Alcohol %" data-dataset-allow-free-responses="" class="validate-float required"><span class="field-hint-inactive" id="tfa_alcohol_pct-H"><span id="tfa_alcohol_pct-HH" class="hint">(e.g., 45)</span></span>
						</div>
					</div>
				</div>
<!---END PRODUCT-->	
	<!------FIELD NAME RENAMING---->		
<!---BEGIN SUPPLIER-->				
				<fieldset id="edit_supplier" class="section">
				<?php
					if($suppID > 0){ echo "<legend id=\"tfa_1020-L\">Supplier Information: editing supplier_id ".$suppID."</legend>"; }
					else{  echo "<legend id=\"tfa_1020-L\">Supplier Information: new entry</legend>"; }
				?>
				
				<div id="tfa_1022" class="section inline group">
					<div class="oneField field-container-D    " id="tfa_supplier_name-D">
						<label id="tfa_supplier_name-L" class="label preField reqMark" for="tfa_supplier_name">Supplier Name <i class="far fa-question-circle" data-toggle="tooltip" data-html="true" title="<em>This is the name of the distillery, winery or brewery that produces the product.</em>"></i></label><br>
						<div class="inputWrapper"><input type="text" id="tfa_supplier_name" name="tfa_supplier_name" value="<?php echo $pf_supplier_name; ?>" aria-required="true" title="Supplier Name" data-dataset-allow-free-responses="" class="required"></div>
					</div>
					<div class="oneField field-container-D    " id="tfa_supplier_contact-D">
						<label id="tfa_supplier_contact-L" class="label preField reqMark" for="tfa_supplier_contact">Supplier Contact Person</label><br>
						<div class="inputWrapper"><input type="text" id="tfa_supplier_contact" name="tfa_supplier_contact" value="<?php echo $pf_supplier_contact; ?>" aria-required="true" title="Supplier Contact Person" data-dataset-allow-free-responses="" class="required"></div>
					</div>
					<div class="oneField field-container-D    " id="tfa_supplier_fda_number-D">
						<label id="tfa_supplier_fda_number-L" class="label preField reqMark" for="tfa_supplier_fda_number">Supplier FDA Registration Number</label><br>
						<div class="inputWrapper"><input type="text" id="tfa_supplier_fda_number" name="tfa_supplier_fda_number" value="<?php echo $pf_supplier_fda_number; ?>" aria-required="true" title="Supplier FDA Registration Number" data-dataset-allow-free-responses="" class="required"></div>
					</div>
					<div class="oneField field-container-D    " id="tfa_tax_reduction_allocation-D">
						<label id="tfa_tax_reduction_allocation-L" class="label preField " for="tfa_tax_reduction_allocation">Tax Reduction Allocation</label><br>
						<div class="inputWrapper"><input type="text" id="tfa_tax_reduction_allocation" name="tfa_tax_reduction_allocation" value="<?php echo $pf_tax_reduction_allocation; ?>" title="Tax Reduction Allocation" data-dataset-allow-free-responses="" class="validate-float"></div>
					</div>
				</div>
				<div id="tfa_1028" class="section inline group">
					<div class="oneField field-container-D    " id="tfa_supplier_address_1-D">
						<label id="tfa_supplier_address_1-L" class="label preField reqMark" for="tfa_supplier_address_1">Supplier Address Line 1</label><br>
						<div class="inputWrapper"><input type="text" id="tfa_supplier_address_1" name="tfa_supplier_address_1" value="<?php echo $pf_supplier_address_1; ?>" aria-required="true" title="Supplier Address Line 1" data-dataset-allow-free-responses="" class="required"></div>
					</div>
					<div class="oneField field-container-D    " id="tfa_supplier_address_2-D">
						<label id="tfa_supplier_address_2-L" class="label preField " for="tfa_supplier_address_2">Supplier Address Line 2</label><br>
						<div class="inputWrapper"><input type="text" id="tfa_supplier_address_2" name="tfa_supplier_address_2" value="<?php echo $pf_supplier_address_2; ?>" title="Supplier Address Line 2" data-dataset-allow-free-responses="" class=""></div>
					</div>
					<div class="oneField field-container-D    " id="tfa_supplier_address_3-D">
						<label id="tfa_supplier_address_3-L" class="label preField " for="tfa_supplier_address_3">Supplier Address Line 3</label><br>
						<div class="inputWrapper"><input type="text" id="tfa_supplier_address_3" name="tfa_supplier_address_3" value="<?php echo $pf_supplier_address_3; ?>" title="Supplier Address Line 3" data-dataset-allow-free-responses="" class=""></div>
					</div>
					<div class="oneField field-container-D    " id="tfa_supplier_city-D">
						<label id="tfa_supplier_city-L" class="label preField reqMark" for="tfa_supplier_city">Supplier City</label><br>
						<div class="inputWrapper"><input type="text" id="tfa_supplier_city" name="tfa_supplier_city" value="<?php echo $pf_supplier_city; ?>" aria-required="true" title="Supplier City" data-dataset-allow-free-responses="" class="required"></div>
					</div>
					<div class="oneField field-container-D    " id="tfa_supplier_state-D">
						<label id="tfa_supplier_state-L" class="label preField " for="tfa_supplier_state">Supplier State</label><br>
						<div class="inputWrapper"><input type="text" id="tfa_supplier_state" name="tfa_supplier_state" value="<?php echo $pf_supplier_state; ?>" title="Supplier State" data-dataset-allow-free-responses="" class=""></div>
					</div>
					<div class="oneField field-container-D    " id="tfa_supplier_country-D">
						<label id="tfa_supplier_country-L" class="label preField reqMark" for="tfa_supplier_country">Supplier Country</label><br>
						<div class="inputWrapper"><input type="text" id="tfa_supplier_country" name="tfa_supplier_country" value="<?php echo $pf_supplier_country; ?>" aria-required="true" title="Supplier Country" data-dataset-allow-free-responses="" class="required"></div>
					</div>
					<div class="oneField field-container-D    " id="tfa_supplier_zip-D">
						<label id="tfa_supplier_zip-L" class="label preField " for="tfa_supplier_zip">Supplier Zip Code</label><br>
						<div class="inputWrapper"><input type="text" id="tfa_supplier_zip" name="tfa_supplier_zip" value="<?php echo $pf_supplier_zip; ?>" title="Supplier Zip Code" data-dataset-allow-free-responses="" class=""></div>
					</div>
					<div class="oneField field-container-D    " id="tfa_supplier_phone-D">
						<label id="tfa_supplier_phone-L" class="label preField reqMark" for="tfa_supplier_phone">Supplier Phone</label><br>
						<div class="inputWrapper"><input type="text" id="tfa_supplier_phone" name="tfa_supplier_phone" value="<?php echo $pf_supplier_phone; ?>" aria-required="true" title="Supplier Phone" data-dataset-allow-free-responses="" class="required"></div>
					</div>
					<div class="oneField field-container-D    " id="tfa_supplier_email-D">
						<label id="tfa_supplier_email-L" class="label preField reqMark" for="tfa_supplier_email">Supplier Email</label><br>
						<div class="inputWrapper"><input type="text" id="tfa_supplier_email" name="tfa_supplier_email" value="<?php echo $pf_supplier_email; ?>" aria-required="true" title="Supplier Email" data-dataset-allow-free-responses="" class="required"></div>
					</div>
					<div class="oneField field-container-D    " id="tfa_supplier_fed_permit-D">
						<label id="tfa_supplier_fed_permit-L" class="label preField" for="tfa_supplier_fed_permit">Federal Basic Permit Number</label><br>
						<div class="inputWrapper"><input type="text" id="tfa_supplier_fed_permit" name="tfa_supplier_fed_permit" value="<?php echo $pf_supplier_fed_permit; ?>" aria-required="true" title="Federal Basic Permit Number" data-dataset-allow-free-responses="" class=""></div>
					</div>
				</div>
				</fieldset>
<!---END SUPPLIER-->				
<!---BEGIN COLA UPLOAD-->
				<fieldset id="tfa_1045" class="section">
				<legend id="tfa_1045-L">Label Images / COLA</legend>
				<div class="htmlSection" id="tfa_1046">
					<div class="htmlContent" id="tfa_1046-HTML">
						<i>Please upload your label images or approved COLA below.</i><br><br>Note each image file must:<br><ul><li><span style="font-size: 14.4px;">Have a file type of JPG, PDF or TIFF (file extensions: .jpg/.jpeg/.jpe or .pdf or .tif/.tiff)</span></li><li>Not exceed 7500KB in size</li><li>Have a compression/quality ratio set at Medium (7 out of 10 or 70 out of 100)</li><li>Utilize colors in the RGB color mode, not colors in the CMYK color mode</li><li>Have no surrounding white space or printer's proof detail; this must be cropped out</li></ul>
					</div>
					<div class="ttbLinkDiv" id="tfa_ttbLink[0]-D"></div>
				</div>
				<div id="tfa_1054" class="section group">
					<div id="tfa_1055[0]" class="repeat section inline group" data-repeatlabel="Add another image">
						<div class="oneField field-container-D    " id="tfa_image[0]-D">
							<label id="tfa_image[0]-L" class="label preField filelbl reqMark" for="tfa_image[0]">Image File&nbsp;</label><br>
							<div class="inputWrapper"><input type="file" id="tfa_image[0]" name="tfa_image[0]" size="" title="Image File " class="thefile fileobj required"></div>
						</div>
						
						<div class="oneField field-container-D    " id="tfa_image_type[0]-D">
							<label id="tfa_image_type[0]-L" class="label preField filelbl reqMark" for="tfa_image_type[0]">Image Type</label><br>
							<div class="inputWrapper">
								<select id="tfa_image_type[0]" name="tfa_image_type[0]" title="Image Type" aria-required="true" class="fileobj required">
								<option value="">Please select...</option>
								<option value="Front" class="">Front</option>
								<option value="Back" class="">Back</option>
								<option value="Neck" class="">Neck</option>
								<option value="Approved COLA" class="">Approved COLA</option>
								<option value="Other" class="">Other</option>
								</select>
							</div>
						</div>
						
						<div class="oneField field-container-D    " id="tfa_image_dim[0]-D">
							<label id="tfa_image_dim[0]-L" class="label preField " for="tfa_image_dim[0]">Image Dimensions</label><br>
							<div class="inputWrapper">
								<input type="text" id="tfa_image_dim[0]" name="tfa_image_dim[0]" value="" aria-describedby="tfa_image_dim[0]-HH" title="Image Dimensions" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_image_dim[0]-H"><span id="tfa_image_dim[0]-HH" class="hint">(inches)</span></span>
							</div>
						</div>
					</div>
				</div>
				</fieldset>
<!---END COLA UPLOAD-->			
<!---BEGIN ITEM-->	
				<fieldset id="tfa_825[0]" class="repeat section highlighted itmfldst" data-repeatlabel="Add another item related to this product">
				<legend id="tfa_825[0]-L">Item</legend>
				<div id="tfa_826[0]" class="section inline group">
					<div class="oneField field-container-D    " id="tfa_mhw_item_description[0]-D">
						<label id="tfa_mhw_item_description[0]-L" class="label preField " for="tfa_mhw_item_description[0]">MHW Item Description</label><br>
						<div class="inputWrapper"><input type="text" id="tfa_mhw_item_description[0]" name="tfa_mhw_item_description[0]" value="" title="MHW Item Description" data-dataset-allow-free-responses="" class="mhw_item_desc" readonly></div>
					</div>
				</div>
				<div id="tfa_827[0]" class="section inline group">
					<!--<div class="oneField field-container-D    " id="tfa_item_description[0]-D">
						<label id="tfa_item_description[0]-L" class="label preField " for="tfa_item_description[0]">Client Item Description</label><br>
						<div class="inputWrapper"><input type="text" id="tfa_item_description[0]" name="tfa_item_description[0]" value="" aria-required="true" aria-describedby="tfa_item_description[0]-HH"  maxlength="60" title="Item Description" data-dataset-allow-free-responses="" class="item_description"><span class="field-hint-inactive" id="tfa_item_description[0]-H"><span id="tfa_item_description[0]-HH" class="hint item_description_hint">Up to 60 characters </span></span></div>
					</div>-->
					<div class="oneField field-container-D    " id="tfa_container_type[0]-D">
						<label id="tfa_container_type[0]-L" class="label preField itmlbl reqMark" for="tfa_container_type[0]">Container Type</label><br>
						<div class="inputWrapper">
							<select id="tfa_container_type[0]" name="tfa_container_type[0]" title="Container Type" aria-required="true" class="itmobj container_type required">
							<option value="">Please select...</option>
							<option value="Bottle" id="tfa_132" class="">Bottle</option>
							<option value="Can" id="" class="">Can</option>
							<option value="Keg-oneway" id="" class="">Keg (one way)</option>
							<option value="Keg-deposit" id="" class="">Keg (deposit)</option>
							<option value="Other" id="tfa_134" class="">Other</option></select>
						</div>
					</div>
					<div class="oneField field-container-D    " id="tfa_container_size[0]-D">
						<label id="tfa_container_size[0]-L" class="label preField itmlbl reqMark" for="tfa_container_size[0]">Container Size</label><br>
						<div class="inputWrapper">
							<select id="tfa_container_size[0]" name="tfa_container_size[0]" title="Container Size" aria-required="true" class="itmobj container_size required">
							<option value="">Please select...</option>
							
							<?php
							$container_size_opts = array();

							$tsql_container_size = "select DISTINCT ContainerSize + ' ' + ContainerSizeUOM as [size_opts] from [dbo].[mhw_app_container_size] ORDER BY ContainerSize + ' ' + ContainerSizeUOM ";
							$getResults_container_size = sqlsrv_query($conn, $tsql_container_size);

							if ($getResults_container_size == FALSE)
								echo (sqlsrv_errors());

							while ($row_container_size = sqlsrv_fetch_array($getResults_container_size, SQLSRV_FETCH_ASSOC)) {
								array_push($container_size_opts,$row_container_size['size_opts']);
							}

							sqlsrv_free_stmt($getResults_container_size);
							
							foreach ($container_size_opts as &$container_size_optsVALUE) {
								if(isset($pf_container_size) && $pf_container_size==$container_size_optsVALUE){
									echo "<option value=\"".$container_size_optsVALUE."\" class=\"\" selected>".$container_size_optsVALUE."</option>";
								}
								else{
							    		echo "<option value=\"".$container_size_optsVALUE."\" class=\"\">".$container_size_optsVALUE."</option>";
								}
							}
							?>
							</select>
						</div>
					</div>
					<div class="oneField field-container-D    " id="tfa_bottle_material[0]-D">
						<label id="tfa_bottle_material[0]-L" class="label preField itmlbl reqMark" for="tfa_bottle_material[0]">Container Material</label><br>
						<div class="inputWrapper">
							<select id="tfa_bottle_material[0]" name="tfa_bottle_material[0]" title="Container Material" aria-required="true" class="itmobj bottle_material required">
							<option value="">Please select...</option>
							<option value="Glass" id="tfa_286" class="" <?php if(isset($pf_bottle_material) && $pf_bottle_material=="Glass"){ echo "selected"; } ?>>Glass</option>
							<option value="Plastic" id="tfa_287" class="" <?php if(isset($pf_bottle_material) && $pf_bottle_material=="Plastic"){ echo "selected"; } ?>>Plastic</option>
							<option value="Metal" id="" class="" <?php if(isset($pf_bottle_material) && $pf_bottle_material=="Metal"){ echo "selected"; } ?>>Metal</option>
							<option value="Other" id="tfa_288" class="" <?php if(isset($pf_bottle_material) && $pf_bottle_material=="Other"){ echo "selected"; } ?>>Other</option></select>
						</div>
					</div>
					<div class="oneField field-container-D    " id="tfa_stock_uom[0]-D">
						<label id="tfa_stock_uom[0]-L" class="label preField itmlbl reqMark" for="tfa_stock_uom[0]">Stock UOM</label><br>
						<div class="inputWrapper">
							<!--<input type="text" id="tfa_stock_uom[0]" name="tfa_stock_uom[0]" value="" aria-required="true" aria-describedby="tfa_stock_uom[0]-HH" title="Stock UOM" data-dataset-allow-free-responses="" class="itmobj stock_uom required"><span class="field-hint-inactive" id="tfa_stock_uom[0]-H"><span id="tfa_stock_uom[0]-HH" class="hint">(e.g., CASE)</span></span>-->
							<select id="tfa_stock_uom[0]" name="tfa_stock_uom[0]" title="Stock UOM" aria-required="true" class="itmobj stock_uom required">
							<option value="">Please select...</option>
							<option value="CASE" class="" <?php if(isset($pf_stock_uom) && $pf_stock_uom=="CASE"){ echo "selected"; } ?>>CASE</option>
							<option value="BOTTLE"  class="" <?php if(isset($pf_stock_uom) && $pf_stock_uom=="BOTTLE"){ echo "selected"; } ?>>BOTTLE</option>
							<option value="KEG" class="" <?php if(isset($pf_stock_uom) && $pf_stock_uom=="KEG"){ echo "selected"; } ?>>KEG</option>
							</select>
						</div>
					</div>
					<div class="oneField field-container-D    " id="tfa_bottles_per_case[0]-D">
						<label id="tfa_bottles_per_case[0]-L" class="label preField itmlbl reqMark" for="tfa_bottles_per_case[0]">Bottles per Case</label><br>
						<div class="inputWrapper"><input type="number" autocomplete="off" onkeydown="javascript: return ['Backspace','Delete','ArrowLeft','ArrowRight'].includes(event.code) ? true : !isNaN(Number(event.key)) && event.code!=='Space'" id="tfa_bottles_per_case[0]" name="tfa_bottles_per_case[0]" value="" min="0" max="999999" step="any" aria-required="true" title="Bottles per Case" data-dataset-allow-free-responses="" class="itmobj bottles_per_case validate-integer required"></div>
					</div>

					<div class="oneField field-container-D    " id="tfa_chill_storage[0]-D">
						<label id="tfa_chill_storage[0]-L" class="label preField itmlbl reqMark" for="tfa_chill_storage[0]">Chill Storage?</label><br>
						<div class="inputWrapper">
							<select id="tfa_chill_storage[0]" name="tfa_chill_storage[0]" title="Chill Storage" aria-required="true" class="itmobj chill_storage required">
							<option value="">Please select...</option>
							<option value="NON-CHILL" class="">NON- CHILL</option>
							<option value="CHILL" class="">CHILL</option>
							</select>
						</div>
						<br /><span id="tfa_chill_storage-HH[0]" class="chillnote">Additional Storage Charges May Apply</span>
					</div>

					<div class="oneField field-container-D    " id="tfa_outer_shipper[0]-D">
						<label id="tfa_outer_shipper[0]-L" class="label preField itmlbl reqMark" for="tfa_outer_shipper[0]">Outer Shipper</label><br>
						<div class="inputWrapper">
							<select id="tfa_outer_shipper[0]" name="tfa_outer_shipper[0]" title="Outer Shipper" aria-required="true" class="itmobj outer_shipper required">
							<option value="">Please select...</option>
							<option value="WOOD" class="">Wood</option>
							<option value="CARDBOARD" class="">Cardboard</option>
							<option value="OTHER" class="">Other</option>
							</select>
						</div>
				</div>
					
				</div>
				<div id="tfa_828[0]" class="section inline group">
					
					<div class="oneField field-container-D    " id="tfa_client_code[0]-D">
						<label id="tfa_client_code[0]-L" class="label preField client_code_lbl" for="tfa_client_code">Client Item Code</label>
						<span id='tfa_client_code_warning' style='color:red'></span><br>
						<div class="inputWrapper">
							<input type="text" id="tfa_client_code" name="tfa_client_code[0]" value="" title="Client Item Code" data-dataset-allow-free-responses="" class="client_code"><br/>
							<input type="checkbox" id="clientcodeoverride" name="clientcodeoverride[0]" value="1" class="clientcodeoverride">Must be used as MHW Item Code
						</div>
					</div>

					<div class="oneField field-container-D    " id="tfa_height[0]-D">
						<label id="tfa_height[0]-L" class="label preField " for="tfa_height[0]">Case Height</label><br>
						<div class="inputWrapper"><input type="text" id="tfa_height[0]" name="tfa_height[0]" value="" size="5"  title="Height" data-dataset-allow-free-responses="" class=""></div>
					</div>
					<div class="oneField field-container-D    " id="tfa_length[0]-D">
						<label id="tfa_length[0]-L" class="label preField " for="tfa_length[0]">Case Length</label><br>
						<div class="inputWrapper"><input type="text" id="tfa_length[0]" name="tfa_length[0]" value="" size="5"  title="Length" data-dataset-allow-free-responses="" class=""></div>
					</div>
					<div class="oneField field-container-D    " id="tfa_width[0]-D">
						<label id="tfa_width[0]-L" class="label preField " for="tfa_width[0]">Case Width</label><br>
						<div class="inputWrapper"><input type="text" id="tfa_width[0]" name="tfa_width[0]" value="" size="5"  title="Width" data-dataset-allow-free-responses="" class=""></div>
					</div>
					
					<div class="oneField field-container-D    " id="tfa_unit_dimensions[0]-D">
						<label id="tfa_unit_dimensions[0]-L" class="label preField" for="tfa_unit_dimensions[0]">Case/Unit Dimensions</label><br>
						<div class="inputWrapper">
							<select id="tfa_unit_dimensions[0]" name="tfa_unit_dimensions[0]" title="Case/Unit Dimensions" aria-required="true" class="">
							<option value="">Please select...</option>
							<option value="M" class="">M</option>
							<option value="IN" class="">IN</option>
							<option value="FT" class="">FT</option>
							<option value="CM" class="">CM</option>
							</select>
						</div>  
					</div>
					
					<div class="oneField field-container-D    " id="tfa_weight[0]-D">
						<label id="tfa_weight[0]-L" class="label preField " for="tfa_weight[0]">Case Weight</label><br>
						<div class="inputWrapper"><input type="text" id="tfa_weight[0]" name="tfa_weight[0]" size="5" value="" title="Weight" data-dataset-allow-free-responses="" class=""></div>
					</div>
					
					
					<div class="oneField field-container-D    " id="tfa_weight_uom[0]-D">
						<label id="tfa_weight_uom[0]-L" class="label preField" for="tfa_weight_uom[0]">Weight UOM</label><br>
						<div class="inputWrapper">
							<select id="tfa_weight_uom[0]" name="tfa_weight_uom[0]" title="Weight UOM" aria-required="true" class="">
							<option value="">Please select...</option>
							<option value="KG" class="">KG</option>
							<option value="LB" class="">LB</option>
							<option value="G" class="">G</option>
							</select>
						</div>
				    </div>
				
				
					<div class="oneField field-container-D    " id="tfa_pallet_cases[0]-D">
						<label id="tfa_pallet_cases[0]-L" class="label preField " for="tfa_pallet_cases[0]">Total number of cases per pallet</label><br>
						<div class="inputWrapper"><input type="text" id="tfa_pallet_cases[0]" name="tfa_pallet_cases[0]" value="" size="5"  title="Cases per Pallet" data-dataset-allow-free-responses="" class=""></div>
					</div>
					<div class="oneField field-container-D    " id="tfa_pallet_layer_cases[0]-D">
						<label id="tfa_pallet_layer_cases[0]-L" class="label preField " for="tfa_pallet_layer_cases[0]">Cases per layer</label><br>
						<div class="inputWrapper"><input type="text" id="tfa_pallet_layer_cases[0]" name="tfa_pallet_layer_cases[0]" value="" size="5"  title="Cases per Layer" data-dataset-allow-free-responses="" class=""></div>
					</div>
					<div class="oneField field-container-D    " id="tfa_pallet_layers[0]-D">
						<label id="tfa_pallet_layers[0]-L" class="label preField " for="tfa_pallet_layers[0]">Layers per pallet</label><br>
						<div class="inputWrapper"><input type="text" id="tfa_pallet_layers[0]" name="tfa_pallet_layers[0]" value="" size="5"  title="Layers per Pallet" data-dataset-allow-free-responses="" class=""></div>
					</div>
					<div class="oneField field-container-D    " id="tfa_upc[0]-D">
						<label id="tfa_upc[0]-L" class="label preField itmlbl reqMark" for="tfa_upc[0]">UPC</label><br>
						<div class="inputWrapper"><input type="text" aria-required="true" id="tfa_upc[0]" name="tfa_upc[0]" value="" title="UPC" data-dataset-allow-free-responses="" class="itmobj"></div>
					</div>
					<div class="oneField field-container-D    " id="tfa_scc[0]-D">
						<label id="tfa_scc[0]-L" class="label preField itmlbl reqMark" for="tfa_scc[0]">SCC</label><br>
						<div class="inputWrapper"><input type="text" aria-required="true" id="tfa_scc[0]" name="tfa_scc[0]" value="" title="SCC" data-dataset-allow-free-responses="" class="itmobj"></div>
					</div>



					<div class="oneField field-container-D     wf-acl-hidden" id="tfa_397[0]-D">
						<label id="tfa_397[0]-L" class="label preField " for="tfa_397[0]">Item Record ID</label><br>
						<div class="inputWrapper"><input type="text" id="tfa_397[0]" name="tfa_397[0]" value="" title="Item Record ID" data-dataset-allow-free-responses="" class=""></div>
					</div>
					<div class="oneField field-container-D     wf-acl-hidden" id="tfa_853[0]-D">
						<label id="tfa_853[0]-L" class="label preField " for="tfa_853[0]">Item Code</label><br>
						<div class="inputWrapper"><input type="text" id="tfa_853[0]" name="tfa_853[0]" value="" title="Item Code" data-dataset-allow-free-responses="" class=""></div>
					</div>
					<div class="oneField field-container-D     wf-acl-hidden" id="tfa_855[0]-D">
						<label id="tfa_855[0]-L" class="label preField " for="tfa_855[0]">Product Record ID</label><br>
						<div class="inputWrapper"><input type="text" id="tfa_855[0]" name="tfa_855[0]" value="" title="Product Record ID" data-dataset-allow-free-responses="" class=""></div>
					</div>
					
					<!-- adding new item fields : start --> 
					
					<div class="oneField field-container-D    " id="tfa_item_class[0]-D">
						<label id="tfa_item_class[0]-L" class="label preField" for="tfa_item_class[0]">Item Class</label><br>
						<div class="inputWrapper"><input type="text" id="tfa_item_class[0]" name="tfa_item_class[0]" value="" aria-required="true" title="Item Class" data-dataset-allow-free-responses="" class=""></div>
					</div>

					

					
					
					<!-- adding new item fields : end --> 
					
					
				</div>
			<!--	<div id="tfa_799" class="section group" data-condition="#tfa_23">-->
				<div id="tfa_799[0]" class="section group WineSection">
					<label class="label preField" id="tfa_799[0]-L"><b>Wine Information</b></label><br>
					<div id="tfa_829[0]" class="section inline group">
						<div class="oneField field-container-D    " id="tfa_vintage[0]-D">
							<label id="tfa_vintage[0]-L" class="label preField itmlbl reqMark WineVintagelbl" for="tfa_vintage[0]">Vintage</label><br>
							<div class="inputWrapper">
							<!--<input type="text" id="tfa_vintage[0]" name="tfa_vintage[0]" value="" maxlength="4" aria-required="true" aria-describedby="tfa_vintage[0]-HH" 
title="Vintage" data-dataset-allow-free-responses="" class="validate-integer itmobj required WineVintage"> --> 
								<select id="tfa_vintage[0]" style="width:20em"  name="tfa_vintage[0]" title="Vintage" aria-required="true" class="validate-integer itmobj required WineVintage">
								 <?php echo $vintage_dropdown_options;?>
								</select>
								<span class="field-hint-inactive" id="tfa_vintage[0]-H"><span id="tfa_vintage[0]-HH" class="hint">(REQUIRED for Wine)</span></span>
							</div>
						</div>
						<!--<div class="oneField field-container-D    " id="tfa_various_vintages[0]-D">
							<label id="tfa_various_vintages[0]-L" class="label preField " for="tfa_various_vintages[0]">Various Vintages?</label><br>
							<div class="inputWrapper">
								<select id="tfa_various_vintages[0]" name="tfa_various_vintages[0]" title="Various Vintages?" class="">
								<option value="">Please select...</option>
								<option value="Yes" id="tfa_291" class="">Yes</option>
								<option value="No" id="tfa_292" class="">No</option></select>
							</div>
						</div>-->
					</div>
				</div>
				</fieldset>
				<div class="oneField field-container-D     wf-acl-hidden" id="tfa_830-D">
					<label id="tfa_830-L" class="label preField " for="tfa_830">Product Record ID</label><br>
					<div class="inputWrapper"><input type="text" id="tfa_830" name="tfa_830" value="" title="Product Record ID" data-dataset-allow-free-responses="" class=""></div>
				</div>
				</fieldset>
	<!---END ITEM-->	

				<div class="actions" id="4690986-A">
				<input type="checkbox" id="clientacceptance" name="clientacceptance" value="1" class="required">By checking this box I am confirming that this is the accurate description for the item which will appear on all MHW generated documents (invoices, purchase orders, etc).  If the description that you desire for this item does not match the product selected please create a new product.<br />
				<input type="submit" data-label="Save &amp; Proceed" class="primaryAction" id="submit_button" value="Save &amp; Proceed"></div>
				<div style="clear:both"></div>
				<input type="hidden" value="163-d9f8128d44498e3b4c2819f2109426ef" name="tfa_dbCounters" id="tfa_dbCounters" autocomplete="off">
				<input type="hidden" value="4690986" name="tfa_dbFormId" id="tfa_dbFormId">
				<input type="hidden" value="" name="tfa_dbResponseId" id="tfa_dbResponseId">
				<input type="hidden" value="7a322b3b0eabc58f0a5235a6ef00e7de" name="tfa_dbControl" id="tfa_dbControl">
				<input type="hidden" value="1554227340" name="tfa_dbTimeStarted" id="tfa_dbTimeStarted" autocomplete="off">
				<input type="hidden" value="41" name="tfa_dbVersionId" id="tfa_dbVersionId">
				<input type="hidden" value="" name="tfa_switchedoff" id="tfa_switchedoff">
				<input type="hidden" name="referringform" id="referringform" value="productsetup.php">
				
				<input type="hidden" name="prodID" id="prodID" value="<?php echo $prodID; ?>">
				<input type="hidden" name="product_mhw_code" id="product_mhw_code" value="<?php echo $product_mhw_code; ?>">
				<input type="hidden" name="product_mhw_code_search" id="product_mhw_code_search" value="<?php echo $product_mhw_code_search; ?>">
				<input type="hidden" name="product_mhw_desc" id="product_mhw_desc" value="<?php echo $pf_product_desc; ?>">
				<input type="hidden" name="suppID" id="suppID" value="<?php echo $suppID; ?>">
				<input type="hidden" name="userID" id="userID" value="<?php echo $_SESSION['mhwltdphp_user']; ?>">

				<input type="hidden" name="productsetup_submit_hash" value="<?=md5(microtime(true))?>">

				<input type="hidden" name="imgSet" id="imgSet" value="<?php echo $imgSet; ?>">
				<input type="hidden" name="itemReq" id="itemReq" value="<?php echo $itemReq; ?>">
				<input type="hidden" name="product_finalized" id="product_finalized" value="<?php echo $product_finalized; ?>">
				</form>
			</div>
		</div>
		<div class="wFormFooter"><p class="supportInfo"><br></p></div>
  		<p class="supportInfo" ></p>
	</div>    
</div>

<script src='js/iframe_resize_helper_internal.js'></script>
</div>
</body>
</html>

<script>
		$(document).ready(function(){
			    
				 

		});
</script>		


<?php
sqlsrv_close($conn);
}
?>

