<?php
//session_start();
include("sessionhandler.php");
include("settings.php");

$pageref = str_replace(".php","",substr(__FILE__,strrpos(__FILE__,"\\")+1));
$clientlist =  "'".str_replace(";","','",$_SESSION['mhwltdphp_userclients'])."'";



if(!isset($_SESSION['mhwltdphp_user'])){
	header("Location: login.php");
}
else if((isset($_GET['fileProdID']) && intval($_GET['fileProdID']))>0)
{	
$prodID=$_GET['fileProdID'];

include("dbconnect.php");
	/* Fetching Product Dropdown : start */
		
	   $product_dropdown="";
      	
		$conn = sqlsrv_connect($serverName, $connectionOptions);
		if( $conn === false) {
				die( print_r( sqlsrv_errors(), true));
		}

		$tsql_prod_dropdown = "EXEC [dbo].[usp_viewfiles] @querytype = 'imageupload', @client = ".$clientlist.", @prodID = 0, @imgID = 0";
		$stmt_prod_dropdown = sqlsrv_query($conn, $tsql_prod_dropdown);

		
		$product_dropdown=$product_dropdown."<option value=''>Please select...</option>";

		while ($row_prod = sqlsrv_fetch_array($stmt_prod_dropdown, SQLSRV_FETCH_ASSOC)) {
		  $selected_text=($prodID==$row_prod['product_id'])?"selected":"";	
		  $product_dropdown=$product_dropdown."<option $selected_text value='".$row_prod['product_id']."'>".$row_prod[brand_name]." - ".$row_prod[product_desc]."</option>";
		}
		sqlsrv_free_stmt($stmt_prod_dropdown);
		sqlsrv_close($conn);
		
		$image_type_dropdowns="<option value=''>Please select...</option><option value='Front' class=''>Front</option><option value='Back' class=''>Back</option><option value='Neck' class=''>Neck</option><option value='Approved COLA' class=''>Approved COLA</option><option value='Other' class=''>Other</option>";
	 
    /* Fetching Product Dropdown : End */
?>

<div class="form-group">
		  <label>Product:<span style="color:red;">*</span></label>
		  <select ID="fileProdID" NAME="fileProdID" pageref="<?php echo $pageref;?>"  class="form-control valdrop required" required="required">
             <?php echo $product_dropdown; ?>
          </select>
	 </div>
	
	 <div class="form-group">
		  <label>File:<span style="color:red;">*</span></label>
		  <input type="file" size="50" class="form-control form-control-file required" name="filetoupload" id="filetoupload" required="required">
		  
	 </div>
	 
	 <div class="form-group">
		  <label>Image Type:<span style="color:red;">*</span></label>
		  <select ID="img_type" NAME="img_type" pageref="<?php echo $pageref;?>"  class="form-control valdrop required" required="required">
             <?php echo $image_type_dropdowns; ?>
          </select>
	 </div>
	 
	 <div class="form-group">
		  <label>Image Dimensions:</label>
		  <input type="text" id="img_dim" name="img_dim" class="form-control">
	 </div>
	 
				 
	 <div class="form-group">
			<label>Note</label>
			<TEXTAREA type="text" id="img_note" name="img_note" class="md-textarea form-control" ROWS="2"></TEXTAREA>
	</div>	

    <input type="hidden" name="action" value="upload">

<?php 
}
?>
