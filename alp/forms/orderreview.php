<?php
//session_start();
include("sessionhandler.php");
include("prepend.php");
include("settings.php");
include("functions.php");

if(!isset($_SESSION['mhwltdphp_user'])){

	header("Location: login.php");
}
else {	
	if($_SESSION['mhwltdphp_usertype'] != "SUPERUSER" && $_SESSION['mhwltdphp_usertype'] != "ADMIN" ){ 
		echo "Access Denied";
		//header("Location: ../mhwlogin.php");
		header("Location: home.php");
	} 
	else{
		include("dbconnect.php");
		include('head.php');

		$param1 = $_GET["ok"];
?>

<script>
	$( document ).ready(function() {

		$("#finalize").hide();

		//var ordkey = '<?php echo $_GET["ok"]; ?>';
//alert(ordkey);
		//if(ordkey && ordkey!=''){
			//$('#prodTable').bootstrapTable('filterBy', {Value1: ordkey});
		//}

		$('#client_name').on('change', function () {
			var value = $(this).val().toLowerCase();
			var valorig = $(this).val();
			
			if(value!="all"){
				$('#prodTable').bootstrapTable('filterBy', {client_name: valorig});
			}
			else{
				$('#prodTable').bootstrapTable('filterBy', '');
			}

			$('.prodedit_btn').on("click", function() {
					var prod = $(this).data("target");
				$("#editprod_"+prod).submit();
			});
		});
		
		$(".prodedit_btn").on("click", function() {
			var prod = $(this).data("target");
			$("#editprod_"+prod).submit();
		});
/////////////////////////////////////////////////////////////
		function processResponse(qryTYPE, qryID, qryMSG){
			var qryUSER = '<?php echo $_SESSION["mhwltdphp_user"]; ?>';
			$.post('query-request-misc.php', {qrytype:qryTYPE,qryid:qryID,qrymsg:qryMSG,qryuser:qryUSER}, function(dataTH){ 
				// ajax request query-request-misc.php
				//alert(dataTH);
				//var resp = JSON.parse(dataTH);
				//$.each( resp, function( key, value ) {
				//	alert(value);

				//});
			});
		}

		$(document).off('click', '.btna').on('click', '.btna',function(){
			//alert('approved');
			var thisid = $(this).data('id');
			var thismsg = $('#rr__'+thisid).val();

//tbd-require lines to be auto-expanded?

			//20200812-approval no longer requires a remark
			//if(!thismsg || thismsg==''){ 
			//	alert('review remark is required'); 
			//	$('#rr__'+thisid).focus(); 
			//}
			//else{

//tbd-ajax post 
processResponse('OrdRevApp',thisid,thismsg);

				var trparent = $(this).closest("tr");
				var trdetail = trparent.next();
				if(trdetail.hasClass("detail-view")){
					trdetail.hide();
				}
				trparent.css("color", "green");
				trparent.slideUp("slow");
			//}
		});

		$(document).off('click', '.btnr').on('click', '.btnr',function(){
			//alert('rejected');
			var thisid = $(this).data('id');
			var thismsg = $('#rr__'+thisid).val();

//tbd-require lines to be auto-expanded?

			if(!thismsg || thismsg==''){ 
				alert('review remark is required');
				$('#rr__'+thisid).focus(); 
			}
			else{

//tbd-ajax post 
processResponse('OrdRevRej',thisid,thismsg);

				var trparent = $(this).closest("tr");
				var trdetail = trparent.next();
				if(trdetail.hasClass("detail-view")){
					trdetail.hide();
				}
				trparent.css("color", "red");
				trparent.slideUp("slow");
			}
		});

		$(document).off('click', '.btnla').on('click', '.btnla',function(){
			//alert('approved');
			var thisid = $(this).data('id');
			var thisparentid = $(this).data('parentid');
			var thismsg = $('#rrl__'+thisid).val();

			//20200812-approval no longer requires a remark
			//if(!thismsg || thismsg==''){ 
			//	alert('review remark is required'); 
			//	$('#rrl__'+thisid).focus(); 
			//}
			//else{

//tbd-ajax post 
processResponse('LineRevApp',thisid,thismsg);

//tbd-disable parent buttons
$('#btnr__'+thisparentid).hide();
$('#btna__'+thisparentid).hide();
				var trparent = $(this).closest("tr");
				var trdetail = trparent.next();
				if(trdetail.hasClass("detail-view")){
					trdetail.hide();
				}
				trparent.css("color", "green");
				trparent.slideUp("slow");
			//}
		});

		$(document).off('click', '.btnlr').on('click', '.btnlr',function(){
			//alert('rejected');
			var thisid = $(this).data('id');
			var thisparentid = $(this).data('parentid');
			var thismsg = $('#rrl__'+thisid).val();

			if(!thismsg || thismsg==''){ 
				alert('review remark is required');
				$('#rrl__'+thisid).focus(); 
			}
			else{

//tbd-ajax post 
processResponse('LineRevRej',thisid,thismsg);

//tbd-disable parent buttons
$('#btnr__'+thisparentid).hide();
$('#btna__'+thisparentid).hide();

				var trparent = $(this).closest("tr");
				var trdetail = trparent.next();
				if(trdetail.hasClass("detail-view")){
					trdetail.hide();
				}
				trparent.css("color", "red");
				trparent.slideUp("slow");
			}
		});

		$(document).off('click', '.btn_itemtoggle').on('click', '.btn_itemtoggle',function(e) {
			var trparent = $(this).closest("tr");
			var prodindex = $(this).data('index');
			var exp = trparent.find(".detail-icon");
			$( exp ).trigger( "click" );
		}); 
//////////////////////////////////////////////////////////////////////////////////////
		//function (to be called within viewproducts.php) to update alert area in nav (head.php)
		function refreshAlert(){
			var clientlistH = '<?php echo $_SESSION["mhwltdphp_userclients"]; ?>';
			var qrytypeH = 'STAT_COUNT_unfinalized';
			$.post('query-request.php', {qrytype:qrytypeH,clientlist:clientlistH}, function(dataTH){ 
				// ajax request query-request.php, send qrytype & client as post variable, return dataX variable, parse into JSON
				var resp = JSON.parse(dataTH);
				$.each( resp, function( key, value ) {
					if(parseInt(value.unfinalizedCount)>0){
						$("#userAlerts").html("<i class=\"fas fa-exclamation-triangle\"></i><a href=\"viewproducts.php?v=nf\"> Drafts <span class=\"badge badge-light\">"+value.unfinalizedCount+"</span></a>");
					}
					else{ 
						$("#userAlerts").html(""); 
					}

				});
			});
		}
		//function to initialize all of the in-modal functionality
		function modalStuff(){
			//from image details to image full preview
			$('.prodimg_thumb').bind('click',function(){
				var imageid = $(this).data('imageid');
				var productid = $(this).data('product');
				var imageurl = 'viewfiles.php?iid='+imageid+'&b='+productid;
				$('.modal-body').load(imageurl,function(){
					$('#fileModalx').modal({show:true});
					modalStuff(); //recursive but necessary to initialize other states on each change
				});
			});
			//back link in modal from image full to image details
			$('.prodimg_back').bind('click',function(){
				var productid = $(this).data('product');
				var producturl = 'viewfiles.php?pid='+productid;
				$('.modal-body').load(producturl,function(){
					$('#fileModalx').modal({show:true});
					modalStuff(); //recursive but necessary to initialize other states on each change
				});
			});
			//initialize prod image modal functionality again
			$('.prodimg_btn').bind('click',function(){
				var productid = $(this).data('product');
				var imageurl = 'viewfiles.php?pid='+productid;
				$('.modal-body').load(imageurl,function(){
					$('#fileModalx').modal({show:true});
					modalStuff(); //recursive but necessary to initialize other states on each change
				});
			});
			//download image button
			$('.dwnld').bind('click', function () {
				var imagefile = $(this).data('url');
				var imagename = $(this).data('imagename');
				$.ajax({
					url: imagefile,
					method: 'GET',
					xhrFields: {
						responseType: 'blob'
					},
					success: function (data) {
						var a = document.createElement('a');
						var url = window.URL.createObjectURL(data);
						a.href = url;
						a.download = imagename;
						a.click();
						window.URL.revokeObjectURL(url);
					}
				});
			});
			//go to upload page for product
			$('.prod_upload').bind('click', function () {
				var prod = $(this).data('prod');
				window.location = 'imageupload.php?pid='+prod;
			});
			$(".prodedit_btn").bind("click", function() {
				var prod = $(this).data("target");
				$("#editprod_"+prod).submit();
			});
		};
		/*function alertTray(qrytype,prodlist,codelist){
			var clientlist = $("#clientlist").val(); 
			var trayURL = '<?php echo $trayFinalizedWorkflow; ?>';  //defined in settings.php

			//$.post(trayURL, {qrytype:qrytype,prodlist:prodlist,clientlist:clientlist,codelist:codelist}, function(dataT){ 
				// ajax request tray web hook, send qrytype, products & clients as post variables
			$.post('query-request.php', {qrytype:qrytype,prodlist:prodlist,clientlist:clientlist,codelist:codelist}, function(dataT){ 
				// ajax request to queue finalization, send qrytype, products & clients as post variables
			});
		}*/

		//Bootstrap table
		var $table = $('#prodTable')
		var $remove = $('#remove')
		var selections = []

		function getIdSelections() {
			return $.map($table.bootstrapTable('getSelections'), function (row) {
				var viewname_raw = $("#screenview_name").val();
				if(viewname_raw=="Items") { 
					return row.item_id
				}
				else{
					return row.product_id
					//return row.product_mhw_code
				}
			})
		}
		function getCodeSelections() {
			return $.map($table.bootstrapTable('getSelections'), function (row) {
				var viewname_raw = $("#screenview_name").val();
				if(viewname_raw=="Items") { 
					return row.item_id
				}
				else{
					//return row.product_id
					return row.product_mhw_code
				}
			})
		}
		function expandTable($detail, row) {
			$detail.bootstrapTable('showLoading');
			subtableBuilder($detail.html('<table class="table-info"></table>').find('table'), 'VPproditems', row.Value1, row.Value2, row.Value3)
		}
		function responseHandler(res) {
			$.each(res.rows, function (i, row) {
			  row.state = $.inArray(row.id, selections) !== -1
			})
			return res
		}

		function detailFormatter(index, row) {
			var html = []
			$.each(row, function (key, value) {
			  html.push('<p><b>' + key + ':</b> ' + value + '</p>')
			})
			return html.join('')
		}

		window.operateEvents = {
			'click .like': function (e, value, row, index) {
			  alert('You click like action, row: ' + JSON.stringify(row))
			},
			'click .prodimg_btn': function (e, value, row, index) {
			  alert('You click prodimg_btn action, row: ' + JSON.stringify(row))
			},
			'click .remove': function (e, value, row, index) {
			  $table.bootstrapTable('remove', {
				field: 'id',
				values: [row.id]
			  })
			}
		}
		
		//buttons
		function operateFormatter(value, row, index) {
			return [
				'<!--<button type="button" class="btn btn-outline-secondary prodimg_btn btn-sm bg_arrow_lightblue" data-product="'+row.product_id+'">-->',
				'<!--Files <span class="badge badge-light">'+row.filecount+'</span></button>-->',
				'<button type="button" class="btn btn-primary btn-sm bg_arrow_blue btn_itemtoggle" data-toggle="collapse" data-index="'+index+'" data-target=".itemprod'+row.product_id+'" data-prod="'+row.product_id+'">',
				'Lines <span class="badge badge-light">'+row.itemcount+'</span></button>',
				'<!--<form id="editprod_'+row.product_id+'" method="POST" action="productsetup.php"><input type="hidden" id="product_id" name="product_id" value="'+row.product_id+'">-->',
				'<!--<input type="hidden" id="product_desc" name="product_desc" value="'+row.product_desc+'">-->',
				'<!--<input type="hidden" id="product_mhw_code" name="product_mhw_code" value="'+row.product_mhw_code+'">-->',
				'<!--<input type="hidden" id="brand_name" name="brand_name" value="'+row.brand_name+'">-->',
				'<!--<input type="hidden" id="client_name" name="client_name" value="'+row.client_name+'"> </form>-->',
				'<!--<button type="button" class="btn btn-dark btn-sm prodedit_btn bg_arrow_darkblue" data-target="'+row.product_id+'">-->',
				'<!--<i class=\"far fa-edit\"></i> Edit</button>-->'
			].join('')
		}
		function subtableBuilder ($el,qrytype, val1,val2,val3) {
			var data;
			$el.bootstrapTable('showLoading');
			$.post('query-request-misc.php', {qrytype:qrytype,val1:val1,val2:val2,val3:val3}, function(dataIT){ // ajax request query-request.php, send qrytype & client as post variable, return dataX variable, parse into JSON

				data = JSON.parse(dataIT);

				//item sub table 
				$el.bootstrapTable('destroy').bootstrapTable('showLoading').bootstrapTable({
					data: data,
					formatLoadingMessage: function () {
						return '<span class="mhwLoading"><img src="css/img/Logo_Chevron_Animated.gif"/> Loading</span>';
					},
					columns: [
					[{
					  field: 'Value4',
					  title: 'Item',
					  sortable: true,
					  align: 'left'
					},
					{
					  title: 'Item Description',
					  field: 'Value5',
					  align: 'left',
					  valign: 'middle',
					  sortable: true  
					},
					{
					  title: 'Qty Ordered',
					  field: 'Value6',
					  align: 'left',
					  valign: 'middle',
					  sortable: true  
					},
					{
					  title: 'Error Message',
					  field: 'Value7',
					  align: 'left',
					  valign: 'middle',
					  sortable: true  
					},
					{
					  title: 'Review Remarks',
					  field: 'Value8',
					  align: 'center',
					  valign: 'middle',
					  sortable: false  
					},
					{
					  field: 'Value9',
					  title: 'Approve/Reject',
					  sortable: false,
					  align: 'center',
					  visible:true
					}]
				]
				}); //sub table init
			}); //POST
		}
		function tableBuilder (qrytype) {
			var data;
			var clientlist = $("#clientlist").val();
			var val1 = '<?php echo $_GET["ok"]; ?>';
			
			var curr_page_number=$('#page_number').val();
			var curr_per_page_size=$('#per_page_size').val();
			var curr_filter_txt=$('#filter_txt').val();
			var curr_is_export_data=$('#is_export_data').val();

			$('#prodTable').bootstrapTable('showLoading');
			$.post('query-request-misc.php', {
				qrytype:qrytype,
				val1:val1,
				page_number:curr_page_number,
				per_page_size:curr_per_page_size,
				filter_txt : curr_filter_txt,
				is_export_data : 'no',
				active_pagination : 'yes',
				}, function(dataPT){ // ajax request query-request.php, send qrytype & client as post variable, return dataX variable, parse into JSON

				data = JSON.parse(dataPT);
                $("#total_rows_num").val(data.total);
				var winH = $(window).height();
				var navH = $(".navbar").height();
				var tblH = (winH-navH)-50;

				if(qrytype=="VPitems"){
					//product/item views
					$('#prodTable').bootstrapTable('destroy').bootstrapTable('showLoading').bootstrapTable({
						data: data.rows,
						height:tblH,
						stickyHeader:true,
						stickyHeaderOffsetY:60,
						search : true,
						searchOnEnterKey : true,
						pagination: true,
						sidePagination : 'server',
						pageSize: parseInt(curr_per_page_size), 
						pageNumber  : parseInt(curr_page_number),
						pageList: [10, 25, 50, 100],
						totalRows: data.total,
						searchText : curr_filter_txt,		
						formatLoadingMessage: function () {
							return  '<span class="mhwLoading"><img src="css/img/Logo_Chevron_Animated.gif"/> Loading</span>';
						},
						//fixedColumns:true,
						//fixedNumber:7,
						columns: [
						[
							/*{
						  field: 'selected',
						  checkbox: true,
						  align: 'center',
						  valign: 'middle'
						},*/
						{
						  field: 'ClientName',
						  title: 'Client Name',
						  align: 'center',
						  valign: 'middle',
						  sortable: true 
						},
						{
						  field: 'Value1_html',
						  title: 'Order',
						  align: 'center',
						  valign: 'middle',
						  sortable: true
						},
						{
						 field: 'Value2',
						  title: 'Customer',
						  align: 'center',
						  valign: 'middle',
						  sortable: true 
						},
						{
						  field: 'CustomerName',
						  title: 'CustomerName',
						  align: 'center',
						  valign: 'middle',
						  sortable: true 
						}, 
						{
						  field: 'Value3',
						  title: 'Territory',
						  align: 'center',
						  valign: 'middle',
						  sortable: true 
						}, 
						{
						  field: 'OrderDate',
						  title: 'Order Date',
						  align: 'center',
						  valign: 'middle',
						  sortable: true 
						},
						{
						  field: 'SlsLocn',
						  title: 'Sales Location',
						  align: 'center',
						  valign: 'middle',
						  sortable: true 
						},
						{
						  field: 'OpsTeam',
						  title: 'Ops Team',
						  align: 'center',
						  valign: 'middle',
						  sortable: true 
						},
						{
						  field: 'Value4',
						  title: 'Item',
						  align: 'center',
						  valign: 'middle',
						  sortable: true 
						}, 
						{
						  title: 'Item Description',
						  field: 'Value5',
						  align: 'center',
						  valign: 'middle',
						  sortable: true  
						},
						{
						  title: 'Qty Ordered',
						  field: 'Value6',
						  align: 'center',
						  valign: 'middle',
						  sortable: true  
						},
						{
						  title: 'Error Message',
						  field: 'Value7',
						  align: 'center',
						  valign: 'middle',
						  sortable: true  
						}]
						
					]
					}); //table init
				}
				else{
					//product/supplier views
					$('#prodTable').bootstrapTable('destroy').bootstrapTable('showLoading').bootstrapTable({
						data: data.rows,
						height:tblH,
						stickyHeader:true,
						stickyHeaderOffsetY:60,
						search : true,
						searchOnEnterKey : true,
						pagination: true,
						sidePagination : 'server',
						pageSize: parseInt(curr_per_page_size), 
						pageNumber  : parseInt(curr_page_number),
						pageList: [10, 25, 50, 100],
						totalRows: data.total,
						searchText : curr_filter_txt,							
						formatLoadingMessage: function () {
							return '<span class="mhwLoading"><img src="css/img/Logo_Chevron_Animated.gif"/> Loading</span>';
						},
						detailView: true,
						onExpandRow: function (index, row, $detail) {
							expandTable($detail,row)
						},
						//fixedColumns:true,
						//fixedNumber:7,
						columns: [
						[
							/*{
						  field: 'selected',
						  checkbox: true,
						  align: 'center',
						  valign: 'middle'
						},*/
						{
						  field: 'ClientName',
						  title: 'Client Name',
						  align: 'center',
						  valign: 'middle',
						  sortable: true 
						},
						{
						  field: 'Value1',
						  title: 'Order',
						  align: 'center',
						  valign: 'middle',
						  sortable: true
						},
						{
						 field: 'Value2',
						  title: 'Customer',
						  align: 'center',
						  valign: 'middle',
						  sortable: true 
						},
						{
						  field: 'CustomerName',
						  title: 'CustomerName',
						  align: 'center',
						  valign: 'middle',
						  sortable: true 
						}, 
						{
						  field: 'Value3',
						  title: 'Territory',
						  align: 'center',
						  valign: 'middle',
						  sortable: true 
						}, 
						{
						  field: 'OrderDate',
						  title: 'Order Date',
						  align: 'center',
						  valign: 'middle',
						  sortable: true 
						},
						{
						  field: 'SlsLocn',
						  title: 'Sales Location',
						  align: 'center',
						  valign: 'middle',
						  sortable: true 
						},
						{
						  field: 'OpsTeam',
						  title: 'Ops Team',
						  align: 'center',
						  valign: 'middle',
						  sortable: true 
						},
						{
						  field: 'ShipToAddr',
						  title: 'Ship To Address',
						  align: 'center',
						  valign: 'middle',
						  sortable: true 
						}, 
						{
						  field: 'City',
						  title: 'City',
						  align: 'center',
						  valign: 'middle',
						  sortable: true 
						}, 
						{
						  field: 'State',
						  title: 'State',
						  align: 'center',
						  valign: 'middle',
						  sortable: true 
						}, 
						{
						  field: 'Zip',
						  title: 'Zip',
						  align: 'center',
						  valign: 'middle',
						  sortable: true 
						}, 
						{
						  title: 'Review Remarks',
						  field: 'Value8',
						  align: 'center',
						  valign: 'middle',
						  sortable: false  
						},
						{
						  field: 'Value9',
						  title: 'Approve/Reject',
						  sortable: false,
						  align: 'center',
						  visible:true
						}]
						
					]
					}); //table init
				}

				var viewname_raw = $("#screenview_name").val();
				if(viewname_raw=="Drafts") { 

					$table.bootstrapTable('checkAll');  //check all Drafts by default
				}

				var orderkey = '<?php echo $ok; ?>';
				if(orderkey && orderkey!=''){
					$table.bootstrapTable('expandAllRows'); 
				}
				//expand-row.bs.table

				modalStuff(); //bind modal & buttons

				$table.on('check.bs.table uncheck.bs.table ' +
				  'check-all.bs.table uncheck-all.bs.table',
				function () {
				  $remove.prop('disabled', !$table.bootstrapTable('getSelections').length)

				  // save your data, here just save the current page
				  selections = getIdSelections()
				  // push or splice the selections if you want to save all data selections

				})
				$table.on('all.bs.table', function (e, name, args) {
				  console.log(name, args)
				  modalStuff(); //when anything happens (filter, click, etc) re-bind modal & buttons
				})
				
			$table.on('page-change.bs.table', function (e, page_number, per_page_size) {
			  console.log("page_number::"+page_number);
			  console.log("per_page_size::"+per_page_size);
			  var curr_page_number=$('#page_number').val();
			  var curr_per_page_size=$('#per_page_size').val();
              if(curr_page_number!=page_number || curr_per_page_size!=per_page_size)
			  {
				  $("#page_number").val(page_number);
			      $("#per_page_size").val(per_page_size);
				  
				  var viewname_raw = $('#screenview_name').val();
				  console.log("viewname_raw::"+viewname_raw);
				   
				  if(viewname_raw=="Lines") { viewname = "VPitems"; }
					else { viewname = "VPdefault"; }
					
                    console.log("viewname::"+viewname);
					tableBuilder(viewname);

			  }			  
			});
			
			$table.on('search.bs.table', function (e, searching_text) {
			  console.log("searching_text::"+searching_text);	
              var curr_filter_txt=$('#filter_txt').val();
              if(curr_filter_txt!=searching_text)
			  {
				  $("#filter_txt").val(searching_text);
			      
				    var viewname_raw = $('#screenview_name').val();
					console.log("inside search viewname_raw::"+viewname_raw);

                    if(viewname_raw=="Lines") { viewname = "VPitems"; }
			        else { viewname = "VPdefault"; }

                    console.log("inside search viewname::"+viewname);
					tableBuilder(viewname);
			  }			  
			});
			
			
			$("#data_export").submit(function(e){
				
				var export_total_rows_num=$('#total_rows_num').val();
				export_total_rows_num=parseInt(export_total_rows_num);
				if(export_total_rows_num>35000)
				{
					e.preventDefault();
                    e.stopImmediatePropagation();				
					console.log(export_total_rows_num);
					alert("Error Exporting, dataset too large, please try filtering data with Advanced Search or use pre-filtered Reports screen");
					return false;
				}
				else 
				{
				   var viewname_raw = $('#screenview_name').val();
				   console.log("inside export viewname_raw::"+viewname_raw);

                    if(viewname_raw=="Lines") { viewname = "VPitems"; }
			        else { viewname = "VPdefault"; }

                 console.log("inside export viewname::"+viewname);
				 
				$("#export_qry_type").val(viewname);
				 				 
				var export_filter_txt=$('#filter_txt').val();
				$("#export_filter_txt").val(export_filter_txt);	
				
				return true;
				}
			});
			

			
				
				
				$remove.click(function () {
				  var ids = getIdSelections()
				  $table.bootstrapTable('remove', {
					field: 'id',
					values: ids
				  })
				  $remove.prop('disabled', true)
				})
		
			}); //POST

		} //unnamed function wrapping table init

		var initialview = '<?php echo $v; ?>';
		//if(initialview=="nf"){
		//	tableBuilder("VPunfinalized"); //initialize table
		//	$("#screenview_name").val("Drafts");
		//	$("#finalize").show();
		//}
		//else{
			tableBuilder("VPdefault"); //initialize table
		//}

		
		$("#screenview_name").on("change", function () {
			var viewname_raw =  $(this).val();
			$("#finalize").hide();
			//if(viewname_raw=="Drafts") { 
			//	viewname = "VPunfinalized"; 
			//	$("#finalize").show();
			//}
			//else if(viewname_raw=="Processing") { viewname = "VPpending"; }
			//else if(viewname_raw=="Finalized") { viewname = "VPfinalized"; }
			//else 
			if(viewname_raw=="Lines") { viewname = "VPitems"; }
			else { viewname = "VPdefault"; }

			tableBuilder(viewname);
		});

		$("#btn_toggle_sub1").on("click",function () {
		  $table.bootstrapTable('expandAllRows')
		})
		$("#btn_toggle_sub2").on("click",function () {
		  $table.bootstrapTable('collapseAllRows')
		})
/*
		$("#finalize").on("click", function () {
			var ids = getIdSelections();
			var codes = getCodeSelections();
			var qrytype = "processFinalize";
			var prodlist = JSON.stringify(ids);
			var codelist = JSON.stringify(codes);
			var clientlist = $("#clientlist").val(); 
			//codes = codes.replace('[','');
			//codes = codes.replace(']','');
			//codes = codes.replace('"','');
			//var codelist=codes;

			$.post('query-request.php', {qrytype:qrytype,prodlist:prodlist}, function(dataPF){ // ajax request query-request.php, send qrytype & client as post variable, return data variable, parse into JSON

				alertTray('alertFinalize',prodlist,codelist);

				$("#finalize").hide();
				$("#screenview_name").val("Processing");
				tableBuilder("VPpending");

				refreshAlert(); //viewproducts.php-specific function for updating nav (head.php) 
			});
		});
		*/
		
	});  //document ready
	</script>
	
  <div class="container-fluid">
	<div class="row justify-content-md-left float-left">
		<!--<div class="col-md-auto oneField field-container-D" id="client_name-D">
			<label id="client_name-L" class="label preField " for="client_name"><b>Client Name</b></label>
			<div class="inputWrapper">
				<select id="client_name" name="client_name" title="Client Name" aria-required="true">
				<?php
				$clients = explode(";",$_SESSION['mhwltdphp_userclients']);
				if (count($clients)>1){  echo "<option value=\"all\" class=\"\">ALL</option>"; }
				foreach ($clients as &$clientvalue) {
					echo "<option value=\"".$clientvalue."\" class=\"\">".$clientvalue."</option>";
				}
				?>					
				</select>
			</div>
		</div>-->
		<div class="col-md-auto oneField field-container-D" id="screenview_name-D">
			<label id="screenview_name-L" class="label preField " for="screenview_name"><b>View</b></label>
			<div class="inputWrapper">
				<select id="screenview_name" name="screenview_name" title="Screen View" aria-required="true">
				<?php
				$screenviews = array("Orders","Lines");
				//if (count($screenviews)>1){  echo "<option value=\"all\" class=\"\">Default</option>"; }
				foreach ($screenviews as &$screenviewvalue) {
					echo "<option value=\"".$screenviewvalue."\" class=\"\">".$screenviewvalue."</option>";
				}
				?>					
				</select>
			</div>
			<br/>
		</div>

		<div class="col-md-auto align-self-center oneField field-container-D" id="add-new-D">
	        <form name="data_export" id="data_export" method="POST" action="export_data_misc.php" target="_blank">
			<input type="hidden" name="export_view_type" id="export_view_type" value="orderreview">
			<input type="hidden" name="export_qry_type" id="export_qry_type" value="VPdefault">
			<input type="hidden" name="export_client" id="export_client" value="">
			<input type="hidden" name="val1" id="val1" value='<?php echo $_GET["ok"]; ?>'>
			<input type="hidden" name="export_searching_data" id="export_searching_data" value="">
			<input type="hidden" name="export_filter_txt" id="export_filter_txt" value="">
    		<button type="submit" id="export_data_btn" class="btn btn-primary"><i class="fa fa-download"></i> <span id='export_data_sp'>Export Data</span></button>
			</form>
		</div>
		
<?php 
	if($param1 && $param1!==''){
?>
		<div class="col-md-auto oneField field-container-D" id="client_name-D">
			<label id="client_name-L" class="label preField " for="client_name"><b>Order Filter: <?php echo $param1; ?></b></label>
			<div class="inputWrapper">
				<a href='orderreview.php'>clear</a>
			</div>
		</div>
<?php 
	}
	else{
?>
		<!--<div class="col-md-auto oneField field-container-D" id="client_name-D">-->
		<div class="toolbar btn-group">
		  <button id="btn_toggle_sub1" class="btn btn-secondary"><i class="fas fa-expand"></i> Expand All</button>
		  <button id="btn_toggle_sub2" class="btn btn-secondary"><i class="fas fa-compress"></i> Collapse All</button>
		</div>
<?php
	}
?>


				<input type="hidden" name="page_number" id="page_number" value="1">
				<input type="hidden" name="per_page_size" id="per_page_size" value="10">
				<input type="hidden" name="filter_txt" id="filter_txt" value="">
				<input type="hidden" name="is_export_data" id="is_export_data" value="no">
				<input type="hidden" name="total_rows_num" id="total_rows_num" value='0'>
				
		<!--<div class="col-md-auto align-self-center oneField field-container-D" id="finalize-D">
		<button id="finalize" class="btn btn-success"><i class="fas fa-lock"></i> Finalize</button>
		</div>-->
	</div>
    <table id="prodTable" class="table table-hover" data-toggle="table" data-pagination="true" data-show-pagination-switch="true" 
	 data-show-columns="true" data-show-toggle="true" data-search="true"  data-toolbar=".toolbar" 
	data-toolbar-align="right">


	  <?php
	  	$clientlist =  "'".str_replace(";","','",$_SESSION['mhwltdphp_userclients'])."'";


		/*
	    while ($row = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
			$i_supplier=0; //reset supplier count at each product
			$i_item=0; //reset item count at each product
			
			echo "<tr>";
			//echo "<td><button type=\"button\" class=\"btn btn-primary prodimgbtn\" data-toggle=\"modal\" data-target=\"#fileModal\" data-product=\"".$row['product_id']."\">";
					echo "<td><button type=\"button\" class=\"btn btn-outline-secondary prodimg_btn btn-sm bg_arrow_lightblue\" data-product=\"".$row['product_id']."\">";
					echo ("Files <span class=\"badge badge-light\">".$row['filecount']."</span></button>
					<button type=\"button\" class=\"btn btn-primary btn-sm bg_arrow_blue btn_itemtoggle\" data-toggle=\"collapse\" data-target=\".itemprod".$row['product_id']."\" data-prod=\"".$row['product_id']."\">
					Items <span class=\"badge badge-light\">".$row['itemcount']."</span></button>");
					echo "<form id=\"editprod_".$row['product_id']."\" method=\"POST\" action=\"productsetup.php\"><input type=\"hidden\" id=\"product_id\" name=\"product_id\" value=\"".$row['product_id']."\"><input type=\"hidden\" id=\"product_desc\" name=\"product_desc\" value=\"".$row['product_desc']."\"><input type=\"hidden\" id=\"brand_name\" name=\"brand_name\" value=\"".$row['brand_name']."\"><input type=\"hidden\" id=\"client_name\" name=\"client_name\" value=\"".$row['client_name']."\"> </form>";
					echo ("<button type=\"button\" class=\"btn btn-dark btn-sm prodedit_btn bg_arrow_darkblue\" data-target=\"".$row['product_id']."\">
					<i class=\"far fa-edit\"></i> Edit</button></td>");

	     	echo ("<td>".$row['client_name']."</td><td>".$row['brand_name']."</td><td>".$row['product_mhw_code']."</td><td>".$row['product_desc']."</td>
					<td>".$row['TTB_ID']."</td><td>".$row['federal_type']."</td><td>".$row['compliance_type']."</td><td>".$row['product_class']."</td>
					<td>".$row['mktg_prod_type']."</td><td>".$row['bev_type']."</td><td>".$row['fanciful']."</td><td>".$row['country']."</td><td>".$row['appellation']."</td>
					<td>".$row['lot_item']."</td><td>".$row['bottle_material']."</td><td>".$row['alcohol_pct']."</td>
					<td>".$row['supplier_name']."</td>
			        <td>".$row['supplier_contact']."</td>
			        <td>".$row['supplier_fda_number']."</td>
			        <td>".$row['tax_reduction_allocation']."</td>
			        <td>".$row['supplier_phone']."</td>
			        <td>".$row['supplier_email']."</td>
					<td>".$row['federal_basic_permit']."</td>");
			echo ("</tr>". PHP_EOL);		
		 
		
			
			//begin item sub-loop
		  $tsql_item = "SELECT [item_id]
		      ,[item_client_code]
		      ,[item_mhw_code]
		      ,[item_description]
		      ,[container_type]
		      ,[container_size]
		      ,[stock_uom]
		      ,[bottles_per_case]
		      ,[upc]
		      ,[scc]
			  ,[vintage]
			  ,[various_vintages]
		      ,[item_status]
		      ,[create_via]
		      ,[create_date]
		      ,[edit_date]
	         FROM [dbo].[mhw_app_prod_item] WHERE [product_id] = '".$row['product_id']."' AND [active] = 1 AND [deleted] = 0 ORDER BY [create_date] desc";
	    $getResults_item= sqlsrv_query($conn, $tsql_item);

	    if ($getResults_item == FALSE)
	        echo (sqlsrv_errors());
			
	    		while ($row_item = sqlsrv_fetch_array($getResults_item, SQLSRV_FETCH_ASSOC)) {
				if($i_item==0){
					
						echo ("<tr data-card-visibile=\"true\" class=\"table-info thitem_prod".$row['product_id']."\"><td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\"><span style=\"display:none;\">".$row['client_name']."</span></div></td>
						<td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">Item Client Code</div></td>
			            <td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">Item MHW Code</div></td>
			            <td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">Item Description</div></td>
			            <td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">Container Type</div></td>
			            <td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">Container Size</div></td>
			            <td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">Stock UOM</div></td>
						<td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">Bottles Per Case</div></td>
						<td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">UPC</div></td>
						<td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">SCC</div></td>
						<td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">Vintage</div></td>
						<td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">Various Vintages</div></td>
						</tr>". PHP_EOL);
					
				}
					 echo ("<tr data-card-visibile=\"true\" class=\"table-info tritem_prod".$row['product_id']."\"><td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\"><span style=\"display:none;\">".$row['client_name']."</span></div></td>
					 	<td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['item_client_code']."</div></td>
			            <td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['item_mhw_code']."</div></td>
			            <td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['item_description']."</div></td>
			            <td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['container_type']."</div></td>
			            <td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['container_size']."</div></td>
			            <td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['stock_uom']."</div></td>
						<td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['bottles_per_case']."</div></td>
						<td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['upc']."</div></td>
						<td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['scc']."</div></td>
						<td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['vintage']."</div></td>
						<td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['various_vintages']."</div></td>
						</tr>". PHP_EOL);
						  
					$i_item++;   
			} //end item sub-loop
//			echo ("</table></div></td></tr>");
			sqlsrv_free_stmt($getResults_item);
			
	    }
		*/
	sqlsrv_free_stmt($getResults);
		
	sqlsrv_close($conn);  
}
}
?>
      </tbody>
    </table>

<input type="hidden" id="clientlist" value="<?php echo  $clientlist; ?>">

<div class="modal modal-wide fade" id="fileModalx" tabindex="-1" role="dialog" aria-labelledby="fileModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="fileModalLabel">Files</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      </div>
	  <div id='preview'></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script>

$(document).ready(function(){


	//initialize prod image modal functionality
	$('.prodimg_btn').on('click',function(){
		var productid = $(this).data('product');
		var imageurl = 'viewfiles.php?pid='+productid;
		//alert(imageurl);
	    $('.modal-body').load(imageurl,function(){
	        $('#fileModalx').modal({show:true});
	    });
	});
	//initialize prod image download functionality for outside of modal
	$('.dwnld').on('click', function () {
		var imagefile = $(this).data('url');
		var imagename = $(this).data('imagename');
		$.ajax({
			url: imagefile,
			method: 'GET',
			xhrFields: {
				responseType: 'blob'
			},
			success: function (data) {
				var a = document.createElement('a');
				var url = window.URL.createObjectURL(data);
				a.href = url;
				a.download = imagename;
				a.click();
				window.URL.revokeObjectURL(url);
			}
		});
	});

	//var ordkey = '<?php echo $_GET["ok"]; ?>';
//alert(ordkey);
		//if(ordkey && ordkey!=''){
			//$('#prodTable').bootstrapTable('expandAllRows');
			//$('#prodTable').bootstrapTable('filterBy', {Value1: ordkey});
		//}
	//make sure modal initializers are called when modal is shown
	//$('#fileModalx').on('shown.bs.modal', function (e) {
	//	modalStuff();
	//});
	
});

</script>

    </div>
  </body>
</html>