<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
	session_start();
    include("prepend.php");
    include("settings.php");
    include("functions.php");
    if(!isset($_SESSION['mhwltdphp_user'])){
        header("Location: login.php");
    }
    include("dbconnect.php");
    include('head.php');

	$isAdmin = in_array($_SESSION['mhwltdphp_usertype'], ["SUPERUSER", "ADMIN"]);

    /* get distributors from db*/
    $distributorsArray = array();
    $conn = sqlsrv_connect($serverName, $connectionOptions);
    if( $conn === false) {
        die( print_r( sqlsrv_errors(), true));
    }
    $tsql= "SELECT * FROM [dbo].[sc_distributor-list] ORDER BY [Name]";
    $stmt = sqlsrv_query($conn, $tsql);
    if( $stmt ) {
        while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
            array_push($distributorsArray, $row);
        }
    }
    sqlsrv_free_stmt($stmt);

?>

<link href="css/wforms-layout_v530-14.css" rel="stylesheet" type="text/css" />
<link href="css/theme-52661.css" rel="stylesheet" type="text/css" />
<link href="css/wforms-jsonly_v530-14.css" rel="alternate stylesheet" title="This stylesheet activated by javascript" type="text/css" />
<script type="text/javascript" src="js/wforms_v530-14.js"></script>
<script type="text/javascript" src="js/localization-en_US_v530-14.js"></script>
<style type="text/css">
.wForm fieldset.offstate , .offstate {display:none;}
.validation-error {
    background-color: #ffc2ab !important;
}
.select2-input {
    min-width: 150px;
}

.editable-click {
    overflow: hidden;
    text-overflow: ellipsis;
    max-width: 100px;
    white-space: nowrap; 
}

.select2-container-multi .select2-choices li {
    float: none;
}

#dlg-distributors-list {
    margin: 0 5% 0 5%;
    width: 90%;
    min-height: 300px;
}
</style>

<div class="container-fluid">
    <div class="before-table-header">
        <div class="row justify-content-md-left">
            <div class="col-md-auto align-self-center oneField field-container-D" id="pp-calendar">
                <div style="font-size: 14.4px;"><b><u>Price Posting Calendar</u></b></div>
                <div style="font-size: 14.4px;">
                    <ul style="font-size: 14.4px;">
                        <li id="pp-calendar-m1"><u>1st of the Month</u><u style="font-weight: bold;">:</u><span style="font-weight: 700;">&nbsp;</span>New Jersey Retail:&nbsp;<i style="font-size: 14.4px; word-spacing: normal;">Effective the 1st of the following month (e.g., January postings are due December 1st)</i></li>
                        <li id="pp-calendar-m10"><u>10th of the Month<span style="font-weight: 700;">:</span></u><span style="font-weight: 700;">&nbsp;</span>New York Wholesale:&nbsp;<i>Effective the 1st of the second succeeding month (e.g., January postings are due November 10th)</i></li>
                        <li id="pp-calendar-m20"><span style="text-decoration-line: underline;">20th of the Month</span><span style="font-weight: 700; text-decoration-line: underline;">:</span>&nbsp;Massachusetts Wholesale; Minnesota Wholesale; Connecticut Wholesale; New York Retail:&nbsp;<i>Effective the 1st of the second succeeding month (e.g., January postings are due November 20th)</i></li>
                    </ul>
                    <div style="font-size: 14.4px;">Please note all requests received after the above due dates will be subject to a $300 late fee.<br/><br/></div>
                </div>
            </div>
        </div>

	<div class="row justify-content-md-left float-left">

            <div class="col-md-auto align-self-center oneField field-container-D">
                <button class="btn btn-success" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    <i class="fas fa-file"></i> Instructions
                </button>
                <div class="collapse" id="collapseExample">
                    <div class="card card-body">
                        <div class="htmlSection" id="tfa_1"><div class="htmlContent" id="tfa_1-HTML"><h4><u>Instructions</u></h4></div></div>
                            <div class="htmlSection" id="tfa_2"><div class="htmlContent" id="tfa_2-HTML"><span style="font-size: 14.4px;">Welcome to the MHW Price Posting form. Please carefully review and complete all required information. If you have any questions, please contact us at priceposting@mhwltd.com.</span><br style="font-size: 14.4px;"><br style="font-size: 14.4px;"><i style="font-size: 14.4px;">This form is used to add, change, and delete price postings for existing items.&nbsp;<span style="font-size: 14.4px; word-spacing: normal;">A&nbsp;</span><span style="font-size: 14.4px; word-spacing: normal;">new&nbsp;</span><u style="font-size: 14.4px; word-spacing: normal;">item,&nbsp;</u><span style="font-size: 14.4px; word-spacing: normal;">new&nbsp;</span><u style="font-size: 14.4px; word-spacing: normal;">vintage,&nbsp;</u><span style="font-size: 14.4px; word-spacing: normal;">new&nbsp;</span><u style="font-size: 14.4px; word-spacing: normal;">configuration</u><u style="font-size: 14.4px; word-spacing: normal;">,</u><span style="font-size: 14.4px; word-spacing: normal;">&nbsp;or new&nbsp;</span><u style="font-size: 14.4px; word-spacing: normal;">size</u><span style="font-size: 14.4px; word-spacing: normal;">, requires a&nbsp;</span><u style="font-size: 14.4px; word-spacing: normal;">NEW</u><span style="font-size: 14.4px; word-spacing: normal;">&nbsp;item be added to the&nbsp;<a href="productsetup.php">Product &amp; Item Specifications Form</a>.</span></i><br style="font-size: 14.4px;"><br style="font-size: 14.4px;"><span style="font-size: 14.4px;">Please follow the below instructions carefully:</span><br style="font-size: 14.4px;">
                                <ul style="font-size: 14.4px;">
									<li>Select View Type "Price Posting States" > select "View State"<br/>
										-	If adding a new item, > click Add New<br/>
										-	If changing an item, find row below and edit<br/>
										-	If deleting an item, find row below and click delete</li>
									<li>Select View Type "Open States" > select "View State"<br/> 
										-	If adding a new item, > click Add New<br/>
										-	If changing an item, find row below and edit<br/>
										-	If deleting an item, find row below and click delete</li>
									<li>FOB is the shipping point; all retail price postings in NY should indicate "NYS"</li>
                                    <li>Wholesale price postings can be assigned to the following FOBs: "NJ" - shipping from warehouse/stateside; "DI" - direct import orders (for multiple FOBs, please use the first two letters of the exporting country); "EX" - ex-cellars (distributor picks up from winery)</li>
                                    <li>To post discounts, please select "Yes" from the Discounts drop-down menu for each applicable state. This will activate a series of fields in which you must enter the quantity at which the discount should occur, on which unit of measure, in dollars or on a percentage basis, and what amount in dollars or percent. For example, Quantity: 5; On: Container; Type: $; Amount: 1 means a discount of $1 at 5 bottles</li>
									<li>After you are finished with your session of adding, changing or deleting product pricing, Finalize your pricing to submit to MHW for processing.</li>
                                    <li>To assign multiple distributors, hold the Ctrl key and click to select distributors</li>
                                </ul>
                                <div style="font-size: 14.4px;">A CONFIRMATION E-MAIL will be sent to each client upon receipt of their pricing schedule. If a confirmation e-mail is not received within 48 hours of a submission, please contact MHW's price posting team to ensure the submission has been received.
                                </div>
                                <div style="font-size: 14.4px;">
                                    <span style="font-size: 14.4px; word-spacing: normal;"><br></span>
                                </div>
                        </div>
                        </div>
                    </div>
                </div>

            </div>

		<div class="col-md-auto oneField field-container-D" id="client_name-D">
			<label id="client_name-L" class="label preField " for="client_name"><b>Client Name</b></label>
			<div class="inputWrapper">
				<select id="client_name" name="client_name" title="Client Name" aria-required="true">
				<?php
				if ($isAdmin) { 
					//////////////////////////
					
					$clients = array();
					include("dbconnect.php");
					$conn = sqlsrv_connect($serverName, $connectionOptions);
					if( $conn === false) {
						die( print_r( sqlsrv_errors(), true));
					}
					$tsql_clients = "select DISTINCT REPLACE(REPLACE([COMPANY],'\"',''),'''','') as [clientName] from [dbo].[ARCLNT] ORDER BY [clientName]";
					$getResults_clients = sqlsrv_query($conn, $tsql_clients);

					if ($getResults_clients == FALSE)
						echo (sqlsrv_errors());

					while ($row_client = sqlsrv_fetch_array($getResults_clients, SQLSRV_FETCH_ASSOC)) {
						array_push($clients,$row_client['clientName']);
					}

					sqlsrv_free_stmt($getResults_clients);
					sqlsrv_close($conn);
					
					//////////////////////////
					//$clients = explode(";",$_SESSION['mhwltdphp_userclients']);
					//array_push($clients,$pf_client_name);
				}
				else{
				$clients = explode(";",$_SESSION['mhwltdphp_userclients']);
				}
				if (count($clients)>1) {  
                    echo "<option value='all' class=''>(select client)</option>"; 
                }
				foreach ($clients as &$clientvalue) {
					echo "<option value='$clientvalue' class=''>$clientvalue</option>";
				}
				?>					
				</select>
			</div>
		</div>
		<div class="col-md-auto oneField field-container-D">
			<label class="label preField " for="viewType"><b>View Type</b></label>
			<div class="inputWrapper">
				<select id="view_type" name="view_type" title="View Type" aria-required="true">
				<?php
				foreach (array("Price Posting States","Open States","Deleted Items") as $viewType) {
					echo "<option value='$viewType' >$viewType</option>";
				}
				?>					
				</select>
			</div>
		</div>
		<div class="col-md-auto oneField field-container-D">
			<label class="label preField " for="view_state"><b>View State</b></label>
			<div class="inputWrapper">
				<select id="view_state" name="view_state" title="View State" aria-required="true">
				</select>
			</div>
		</div>
            
		<div class="col-md-auto align-self-center oneField field-container-D" id="add-new-D">
    		<button id="btn-add-new" class="btn btn-success"><i class="fas fa-plus"></i> Add new</button>
		</div>

            <div class="col-md-auto align-self-center oneField field-container-D" id="btn-set-distributors-D">
                <button id="btn-set-distributors" class="btn btn-success"><i class="fas fa-users"></i> Set Distributors</button>
	</div>

            <div class="col-md-auto align-self-center oneField field-container-D" id="finalize-D">
                <button id="btn-finalize" class="btn btn-success"><i class="fas fa-check-square"></i> Finalize</button>
            </div>

        </div>
    </div>

    <form id="form-main">
    <table id="priceTable" class="table table-hover" data-toggle="table" data-pagination="false" data-show-pagination-switch="true"  data-show-columns="true" data-show-toggle="true" data-search="true" data-show-export="true" data-id-field="ID" data-editable-emptytext="...." data-editable-url="#">
        <thead>
        </thead>
        <tbody>
        </tbody>
    </table>
    </form>


</div>

<!--  Modals -->

<div class="modal modal-wide fade" id="dlg-discounts" tabindex="-1" role="dialog" aria-labelledby="dlg-discounts-label" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="dlg-discounts-label">Discounts</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="section groadmin    up">
            <form id="form-discounts">
                <?php for($dis_id=1;$dis_id<=10;$dis_id++) {  ?>
                <div class='row'>

                    <div class='col'>
                        <label>Qty <?=$dis_id?> </label>
                        <div class="oneField field-container-D">
                        <div class="inputWrapper">
                            <input type="number" min='0' max='' step='1' id="qty<?=$dis_id?>_quan" name="qty<?=$dis_id?>_quan" value="" title="Quantity <?=$dis_id?>" data-dataset-allow-free-responses="" data-order='<?=$dis_id?>' class="discount_quan_<?=$dis_id?> discount_quan">
                        </div>
                        </div>
                    </div>

                    <div class='col'>
                        <label>On <?=$dis_id?> </label>
                        <div class="oneField field-container-D " ><div class="inputWrapper">
                            <select id="qty<?=$dis_id?>_on" name="qty<?=$dis_id?>_on" title="On" class="">
                                <option value="">Please select...</option>
                                <option class="">Case</option>
                                <option class="">Bottle</option>
                            </select>
                        </div></div>
                    </div>

                    <div class='col'>
                        <label>Type <?=$dis_id?> </label>
                        <div class="oneField field-container-D    " ><div class="inputWrapper">
                            <select id="qty<?=$dis_id?>_type" name="qty<?=$dis_id?>_type" <?=$dis_id>1 ? 'disabled="true"' : '' ?> title="Type <?=$dis_id?>" class="select_qty_type">
                                <option value="">Please select...</option>
                                <option class="">$</option>
                                <option class="">%</option>
                            </select>
                        </div></div>
                    </div>

                    <div class='col'>
                        <label>Amount <?=$dis_id?> </label>
                        <div class="oneField field-container-D    " ><div class="inputWrapper">
                            <input type="text" id="qty<?=$dis_id?>_amt" name="qty<?=$dis_id?>_amt" value="" aria-describedby="qty<?=$dis_id?>_amt-HH" title="Amount <?=$dis_id?>" data-dataset-allow-free-responses="" class=""><br/>
                            </div>
                        </div>
                    </div>

                </div>
                <?php } ?>
            </form>
        </div>
      </div>
	  <div id='preview'></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="btn-dlg-discounts-save">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



<div class="modal modal-wide fade" id="dlg-edit-tfa_ca" tabindex="-1" role="dialog" aria-labelledby="dlg-label-tfa-ca" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="dlg-label-tfa-ca">CA Price Posting (Beer Only)</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">

        <div class="wFormContainer" style="width:auto;margin:auto;" >
        <form id="form-tfa_ca" class="wForm" style="margin:auto;padding:0;">
            <input type="hidden" name="tfa_ca_state" value="Yes">
            <input type="hidden" class="itemID" name="itemID_" value="">
            <div id="tfa_49" class="section inline group">

                <div class="oneField field-container-D">
                    <label class="label preField "><b>Item Description</b></label><br/>
                    <div class="inputWrapper">
                        <input type="text" class="tfa_item_desc" id="tfa_ca_item_desc" name="tfa_item_desc" value="" title="Item Description" data-dataset-allow-free-responses="" class="required validate-alphanum" required>
                    </div>
                </div>

                <div class="oneField field-container-D">
                    <label class="label preField"><b>Effective Month</b></label><br/>
                    <div class="inputWrapper">
                        <select class="tfa_effective_month" name="tfa_effective_month">
                            <option value="">Please select...</option>
                        </select>
                    </div>
                </div>

            </div>
            <div id="tfa_55" class="section inline group">

                <div class="oneField field-container-D">
                    <label><b>Container Size</b></label><br/>
                    <div class="inputWrapper">
                        <input type="text" id="tfa_ca_container_size" class="tfa_container_size" value="" readonly>
                    </div>
                </div>

                <div class="oneField field-container-D">
                    <label><b>Case Qty</b></label><br/>
                    <div class="inputWrapper">
                        <input type="text" id="tfa_ca_bottles_per_case" class="tfa_bottles_per_case" value="" readonly>
                    </div>
                </div>

                <div class="oneField field-container-D">
                    <label><b>Vintage</b></label><br/>
                    <div class="inputWrapper">
                        <input type="text" id="tfa_ca_vintage" class="tfa_vintage" value="" readonly>
                    </div>
            </div>

                <div class="oneField field-container-D     wf-acl-hidden" id="tfa_ca_price_bottle-D">
                    <label id="tfa_ca_price_bottle-L" class="label preField " for="tfa_ca_price_bottle"><b>CA Price / Bottle</b></label><br/>
                    <div class="inputWrapper">
                        <input type="number" min="0.00" step=0.01 id="tfa_ca_price_bottle" name="tfa_ca_price_bottle" value="" title="CA Price / Bottle" data-dataset-allow-free-responses="" class="validate-float">
                    </div>
                </div>
                <div class="oneField field-container-D     wf-acl-hidden" id="tfa_ca_price_case-D">
                    <label id="tfa_ca_price_case-L" class="label preField " for="tfa_ca_price_case"><b>CA Price / Case</b></label><br/>
                    <div class="inputWrapper"><input type="number" min="0.00" step=0.01 id="tfa_ca_price_case" name="tfa_ca_price_case" value="" title="CA Price / Case" data-dataset-allow-free-responses="" readonly class="validate-float"></div>
                </div>
                <div class="oneField field-container-D     wf-acl-hidden" id="tfa_ca_fob-D">
                    <label id="tfa_ca_fob-L" class="label preField " for="tfa_ca_fob"><b>CA FOB Point</b></label><br/>
                    <div class="inputWrapper">
                        <input type="text" id="tfa_ca_fob" name="tfa_ca_fob" value="" aria-describedby="tfa_ca_fob-HH" title="CA FOB" data-dataset-allow-free-responses="" class="validate-alphanum"><br/>
                    </div>
                </div>
                <div class="oneField field-container-D    " id="tfa_ca_package-D">
                    <label id="tfa_ca_package-L" class="label preField reqMark" for="tfa_ca_package"><b>CA Package</b></label><br/>
                    <div class="inputWrapper">
                        <input type="text" id="tfa_ca_package" name="tfa_ca_package" value="" required aria-required="true" aria-describedby="tfa_ca_package-HH" title="CA Package" data-dataset-allow-free-responses="" class="required"><br/>
                        <span class="field-hint-inactive" id="tfa_ca_package-H"><span id="tfa_ca_package-HH" class="hint">(# of items)</span></span>
                    </div>
                </div>
                <div class="oneField field-container-D    " id="tfa_ca_contents-D">
                    <label id="tfa_ca_contents-L" class="label preField reqMark" for="tfa_ca_contents"><b>CA Contents</b></label><br/>
                    <div class="inputWrapper">
                        <input type="text" id="tfa_ca_contents" name="tfa_ca_contents" value=""  required aria-required="true" aria-describedby="tfa_ca_contents-HH" title="CA Contents" data-dataset-allow-free-responses=""  class="required"><br/>
                        <span class="field-hint-inactive" id="tfa_ca_contents-H"><span id="tfa_ca_contents-HH" class="hint">(e.g., 5 Gallons)</span></span>
                    </div>
                </div>
                <div class="oneField field-container-D    " id="tfa_ca_cont_charge-D">
                    <label id="tfa_ca_cont_charge-L" class="label preField " for="tfa_ca_cont_charge"><b>Container Charge / Deposit</b></label><br/>
                    <div class="inputWrapper">
                        <input type="text" id="tfa_ca_cont_charge" name="tfa_ca_cont_charge" value="" title="Container Charge / Deposit" data-dataset-allow-free-responses="" class="">
                    </div>
                </div>
            </div>

            <div id="tfa_1816" class="section inline group">
                <div class="oneField field-container-D    " id="tfa_ca_price_retail-D">
                    <label id="tfa_ca_price_retail-L" class="label preField " for="tfa_ca_price_retail"><b>CA Price to Retail</b></label><br/>
                    <div class="inputWrapper"><input type="number" min=0.00 step=0.01 id="tfa_ca_price_retail" name="tfa_ca_price_retail" value="" title="CA Price to Retail" data-dataset-allow-free-responses="" class=""></div>
                </div>
                <div class="oneField field-container-D    " id="tfa_ca_price_wholesale-D">
                    <label id="tfa_ca_price_wholesale-L" class="label preField " for="tfa_ca_price_wholesale"><b>CA Price to Wholesale</b></label><br/>
                    <div class="inputWrapper"><input type="number" min=0.00 step=0.01 id="tfa_ca_price_wholesale" name="tfa_ca_price_wholesale" value="" title="CA Price to Wholesale" data-dataset-allow-free-responses="" class=""></div>
                </div>
                <div class="oneField field-container-D    " id="tfa_ca_fob_del-D">
                    <label id="tfa_ca_fob_del-L" class="label preField reqMark" for="tfa_ca_fob_del"><b>Price FOB or Delivered?</b></label><br/>
                    <div class="inputWrapper">
                        <select id="tfa_ca_fob_del" name="tfa_ca_fob_del" title="Price FOB or Delivered?"  required aria-required="true" class="required"><option value="">Please select...</option>
                    <option value="FOB" id="tfa_1814" class="">FOB</option>
                            <option value="Delivered" id="tfa_1815" class="">Delivered</option>
                        </select>
                    </div>                        
                </div>
                <div class="oneField field-container-D    " id="tfa_ca_foe-D">
                    <label id="tfa_ca_foe-L" class="label preField " for="tfa_ca_foe"><b>Freight on Empties</b></label><br/>
                    <div class="inputWrapper">
                        <input type="text" id="tfa_ca_foe" name="tfa_ca_foe" value="" title="Freight on Empties" data-dataset-allow-free-responses="" class="">
                    </div>
                </div>
                <div class="oneField field-container-D    " id="tfa_ca_counties-D">
                    <label id="tfa_ca_counties-L" class="label preField reqMark" for="tfa_ca_counties"><b>Counties</b></label><br/>
                    <div class="inputWrapper">
                        <input type="text" id="tfa_ca_counties" name="tfa_ca_counties" value=""  required aria-required="true" aria-describedby="tfa_ca_counties-HH" title="Counties" data-dataset-allow-free-responses="" class="required">
                        <br/>
                        <span class="field-hint-inactive" id="tfa_ca_counties-H"><span id="tfa_ca_counties-HH" class="hint">(List counties where wholesaler or retailer is located)</span></span>
                    </div>
                </div>
                <div class="oneField field-container-D    " id="tfa_ca_dist-D">
                    <label id="tfa_ca_dist-L" class="label preField reqMark" for="tfa_ca_dist"><b>CA Distributors</b></label><br>
                    <div class="inputWrapper">
                    <select multiple id="tfa_ca_dist" name="tfa_ca_dist[]" title="CA Distributors" aria-required="true" class="distributors-list required">
                    </select>
                    </div>
                </div>
            </div>
        </form>
        </div>

      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-save">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>


<div class="modal modal-wide fade" id="dlg-edit-tfa_ct" tabindex="-1" role="dialog" aria-labelledby="dlg-label-tfa-ct" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="dlg-label-tfa-ct">CT Price Posting</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">

        <div class="wFormContainer" style="width:auto;margin:auto;" >
        <form id="form-tfa_ct" class="wForm" style="margin:auto;padding:0;">
            <input type="hidden" name="tfa_ct_state" value="Yes">
            <input type="hidden" class="itemID" name="itemID_" value="">

                <div id="tfa_469" class="section inline group">

                    <div class="oneField field-container-D">
                        <label class="label preField "><b>Item Description</b></label><br/>
                        <div class="inputWrapper">
                            <input type="text" class="tfa_item_desc" id="tfa_ct_item_desc" name="tfa_item_desc" value="" title="Item Description" data-dataset-allow-free-responses=""  required class="required validate-alphanum">
                        </div>
                    </div>

                    <div class="oneField field-container-D">
                        <label class="label preField"><b>Effective Month</b></label><br/>
                        <div class="inputWrapper">
                            <select class="tfa_effective_month" name="tfa_effective_month">
                                <option value="">Please select...</option>
                            </select>
                        </div>
                    </div>
                
                </div>

                <div id="tfa_478" class="section inline group" data-condition="`#tfa_476`">

                    <div class="oneField field-container-D">
                        <label><b>Container Size</b></label><br/>
                        <div class="inputWrapper">
                            <input type="text" id="tfa_ct_container_size" class="tfa_container_size" value="" readonly>
                        </div>
                    </div>

                    <div class="oneField field-container-D">
                        <label><b>Case Qty</b></label><br/>
                        <div class="inputWrapper">
                            <input type="text" id="tfa_ct_bottles_per_case" class="tfa_bottles_per_case" value="" readonly>
                        </div>
                    </div>

                    <div class="oneField field-container-D">
                        <label><b>Vintage</b></label><br/>
                        <div class="inputWrapper">
                            <input type="text" id="tfa_ct_vintage" class="tfa_vintage" value="" readonly>
                        </div>
                    </div>


                    <div id="tfa_479" class="section inline group">
                        <div class="oneField field-container-D    " id="tfa_ct_price_bot-D">
                            <label id="tfa_ct_price_bot-L" class="label preField reqMark" for="tfa_ct_price_bot"><b>CT Price / Bottle</b></label><br/>
                            <div class="inputWrapper"><input type="number" min="0.00" step=0.01 class="tfa_price_bot" id="tfa_ct_price_bot" name="tfa_ct_price_bot" value="" aria-required="true" title="CT Price / Bottle" data-dataset-allow-free-responses="" class="validate-float required"></div>
                        </div>
                        <div class="oneField field-container-D    " id="tfa_ct_price_case-D">
                            <label id="tfa_ct_price_case-L" class="label preField reqMark" for="tfa_ct_price_case"><b>CT Price / Case</b></label><br/>
                            <div class="inputWrapper"><input readonly type="number" step="0.01" min="0.00" id="tfa_ct_price_case" name="tfa_ct_price_case" value="" aria-required="true" readonly title="CT Price / Case" data-dataset-allow-free-responses="" class="validate-float required"></div>
                        </div>
                        <div class="oneField field-container-D    " id="tfa_ct_fob-D">
                            <label id="tfa_ct_fob-L" class="label preField reqMark" for="tfa_ct_fob"><b>CT FOB  Point</b></label><br/>
                            <div class="inputWrapper">
                                <input type="text" id="tfa_ct_fob" name="tfa_ct_fob" value="" aria-required="true" aria-describedby="tfa_ct_fob-HH" title="CT FOB" data-dataset-allow-free-responses="" class="validate-alphanum required"><br/>
                            </div>
                        </div>
                    </div>
                </div>

        </form>
        </div>

      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-save">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>


<div class="modal modal-wide fade" id="dlg-edit-tfa_ma" tabindex="-1" role="dialog" aria-labelledby="dlg-label-tfa-ma" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="dlg-label-tfa-ma">MA Price Posting</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">

        <div class="wFormContainer" style="width:auto;margin:auto;" >
        <form id="form-tfa_ma" class="wForm" style="margin:auto;padding:0;">
            <input type="hidden" name="tfa_ma_state" value="Yes">
            <input type="hidden" class="itemID" name="itemID_" value="">

                <div id="tfa_709" class="section inline group">

                    <div class="oneField field-container-D">
                        <label class="label preField "><b>Item Description</b></label><br/>
                        <div class="inputWrapper">
                            <input type="text" class="tfa_item_desc" id="tfa_ma_item_desc" name="tfa_item_desc" value="" title="Item Description" data-dataset-allow-free-responses="" class="required validate-alphanum">
                        </div>
                    </div>

                    <div class="oneField field-container-D">
                        <label class="label preField"><b>Effective Month</b></label><br/>
                        <div class="inputWrapper">
                            <select class="tfa_effective_month" name="tfa_effective_month">
                                <option value="">Please select...</option>
                            </select>
                        </div>
                    </div>

                </div>

                <div id="tfa_718" class="section group" data-condition="`#tfa_716`"><div id="tfa_719" class="section inline group">

                    <div class="oneField field-container-D">
                        <label><b>Container Size</b></label><br/>
                        <div class="inputWrapper">
                            <input type="text" id="tfa_ma_container_size" class="tfa_container_size" value="" readonly>
                </div>
                </div>

                    <div class="oneField field-container-D">
                        <label><b>Case Qty</b></label><br/>
                        <div class="inputWrapper">
                            <input type="text" id="tfa_ma_bottles_per_case" class="tfa_bottles_per_case" value="" readonly>
                        </div>
                    </div>

                    <div class="oneField field-container-D">
                        <label><b>Vintage</b></label><br/>
                        <div class="inputWrapper">
                            <input type="text" id="tfa_ma_vintage" class="tfa_vintage" value="" readonly>
                </div>
                </div>

                    <div class="oneField field-container-D">
                        <label id="tfa_ma_price_bot-L" class="label preField reqMark" for="tfa_ma_price_bot"><b>MA Price / Bottle</b></label><br>
                        <div class="inputWrapper"><input  type="number" min="0.00" step=0.01 id="tfa_ma_price_bot" name="tfa_ma_price_bot" value="" aria-required="true" title="MA Price / Bottle" data-dataset-allow-free-responses="" class="validate-float required"></div>
                    </div>
                    <div class="oneField field-container-D">
                        <label id="tfa_ma_price_case-L" class="label preField reqMark" for="tfa_ma_price_case"><b>MA Price / Case</b></label><br>
                        <div class="inputWrapper"><input readonly type="number" min="0.00" step=0.01 id="tfa_ma_price_case" name="tfa_ma_price_case" value="" aria-required="true" title="MA Price / Case" data-dataset-allow-free-responses="" class="validate-float required"></div>
                    </div>
                    <div class="oneField field-container-D">
                        <label id="tfa_ma_fob-L" class="label preField reqMark" for="tfa_ma_fob"><b>MA FOB Point</b></label><br><div class="inputWrapper">
                <input type="text" id="tfa_ma_fob" name="tfa_ma_fob" value="" aria-required="true" aria-describedby="tfa_ma_fob-HH" title="MA FOB" data-dataset-allow-free-responses="" class="validate-alphanum required"><br/>
                </div>
                </div>
                <div class="oneField field-container-D">
                    <label id="tfa_ma_dist-L" class="label preField reqMark" for="tfa_ma_dist"><b>MA Distributor</b></label><br>
                    <div class="inputWrapper">
                    <select multiple id="tfa_ma_dist" name="tfa_ma_dist[]" title="MA Distributor" aria-required="true" class="distributors-list required">
                    </select>
                    </div>
                </div>

                <div class="oneField field-container-D discounts-div" id="tfa_ma_disc-D">
                <label id="tfa_ma_disc-L" class="label preField " for="tfa_ma_disc"><b>Discounts?</b></label><br><div class="inputWrapper">
                <select id="tfa_ma_disc" name="tfa_ma_disc" title="Discounts?" aria-describedby="tfa_ma_disc-HH" class=""><option value="">Please select...</option>
                <option id="tfa_726" data-conditionals="#tfa_727" class="">Yes</option></select><br/>
                <span class="field-hint-inactive" id="tfa_ma_disc-H"><span id="tfa_ma_disc-HH" class="hint">(Select "Yes" to modify discounts)</span></span>
                </div>
                </div>
                </div></div>
                <div id="tfa_727" class="section group" data-condition="`#tfa_726`">

                    <label class="label preField"><u style=""><b>Discounts</b></u></label><br>
                    <div class="section group inline">
                <table class="gridLayout ">
                            <thead>
                            <tr class="headerRow">
                <th> </th>
                                <th scope="col">Quantity </th>
                                <th scope="col">On </th>
                                <th scope="col">Type </th>
                                <th scope="col">Amount </th>
                            </tr>
                            </thead>
                            <tbody><tr class="section    alternate-0 ">

                                <?php $st='ma'; for($dis_id=1;$dis_id<=10;$dis_id++) {  ?>

                                <th scope="row" class="headerCol">MA <?=$dis_id?> </th>
                                <td class="">
                                    <div class="oneField field-container-D    " >
                                    <div class="inputWrapper">
                                            <input type="number" min='0' max='' step='1' style="max-width:120px" id="tfa_ma_qty<?=$dis_id?>_quan" name="tfa_ma_qty<?=$dis_id?>_quan" value="" title="Quantity" data-dataset-allow-free-responses="" data-order='<?=$dis_id?>' class="discount_quan_<?=$dis_id?> discount_quan">
                </div>
                </div>
                                </td>
                                <td class="">
                                    <div class="oneField field-container-D" >
                                    <div class="inputWrapper">
                                    <select style="max-width:100px" id="tfa_ma_qty<?=$dis_id?>_on" name="tfa_ma_qty<?=$dis_id?>_on" title="On" class="">
                                        <option value="">Please select...</option>
                                        <option class="">Case</option>
                                        <option class="">Bottle</option>
                                    </select>
                </div>
                </div>
                                </td>

                                <td class="">
                                    <div class="oneField field-container-D" >
                                        <div class="inputWrapper">
                                            <select style="max-width:100px" id="tfa_ma_qty<?=$dis_id?>_type" name="tfa_ma_qty<?=$dis_id?>_type" title="Type"  <?= $dis_id>1 ? 'disabled="true"' : '' ?> class="select_qty_type">
                                                <option value="">Please select...</option>
                                                <option class="">$</option>
                                                <option class="">%</option>
                                            </select>
                </div>
                </div>
                                </td>

                                <td class="">
                                    <div class="oneField field-container-D" >
                                    <div class="inputWrapper">
                                        <input type="number" style="max-width:120px" id="tfa_ma_qty<?=$dis_id?>_amt" name="tfa_ma_qty<?=$dis_id?>_amt" value="" aria-describedby="tfa_ma_qty1_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_ma_qty<?=$dis_id?>_amt-H"><br/>
                                        <span id="tfa_ma_qty<?=$dis_id?>_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
                                    </div>
                </div>
                                </td>
                                <td style="display: none"></td>
                                </tr>

                                <?php } ?>

                            </tbody>
                        </table>

                    </div>
                </div>

        </form>
        </div>

      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-save">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>


<div class="modal modal-wide fade" id="dlg-edit-tfa_mn" tabindex="-1" role="dialog" aria-labelledby="dlg-label-tfa-mn" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="dlg-label-tfa-mn">MN Price Posting (Spirits Only)</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">

        <div class="wFormContainer" style="width:auto;margin:auto;" >
        <form id="form-tfa_mn" class="wForm" style="margin:auto;padding:0;">
            <input type="hidden" name="tfa_mn_state" value="Yes">
            <input type="hidden" class="itemID" name="itemID_" value="">



            <div id="tfa_949" class="section inline group">

                <div class="oneField field-container-D">
                    <label class="label preField "><b>Item Description</b></label><br/>
                    <div class="inputWrapper">
                        <input type="text" class="tfa_item_desc" id="tfa_mn_item_desc" name="tfa_item_desc" value="" title="Item Description" data-dataset-allow-free-responses="" class="required validate-alphanum">
                    </div>
                </div>

                <div class="oneField field-container-D">
                    <label class="label preField"><b>Effective Month</b></label><br/>
                    <div class="inputWrapper">
                        <select class="tfa_effective_month" name="tfa_effective_month">
                            <option value="">Please select...</option>
                        </select>
                    </div>
                </div>
            </div>
            <div id="tfa_958" class="section group" data-condition="`#tfa_956`">
                <div id="tfa_959" class="section inline group">

                    <div class="oneField field-container-D">
                        <label><b>Container Size</b></label><br/>
                        <div class="inputWrapper">
                            <input type="text" id="tfa_mn_container_size" class="tfa_container_size" value="" readonly>
                </div>
                </div>

                    <div class="oneField field-container-D">
                        <label><b>Case Qty</b></label><br/>
                        <div class="inputWrapper">
                            <input type="text" id="tfa_mn_bottles_per_case" class="tfa_bottles_per_case" value="" readonly>
                </div>
                </div>

                    <div class="oneField field-container-D">
                        <label><b>Vintage</b></label><br/>
                        <div class="inputWrapper">
                            <input type="text" id="tfa_mn_vintage" class="tfa_vintage" value="" readonly>
                        </div>
                    </div>

                    <div class="oneField field-container-D">
                    <label id="tfa_mn_price_bot-L" class="label preField reqMark" for="tfa_mn_price_bot"><b>MN Price / Bottle</b></label><br>
                        <div class="inputWrapper"><input type="number" min=0.00 step=0.01 id="tfa_mn_price_bot" name="tfa_mn_price_bot" value="" aria-required="true" title="MN Price / Bottle" data-dataset-allow-free-responses="" class="validate-float required"></div>
                    </div>
                    <div class="oneField field-container-D">
                    <label id="tfa_mn_price_case-L" class="label preField reqMark" for="tfa_mn_price_case"><b>MN Price / Case</b></label><br>
                        <div class="inputWrapper"><input readonly type="number" min=0.00 step=0.01 id="tfa_mn_price_case" name="tfa_mn_price_case" value="" aria-required="true" title="MN Price / Case" data-dataset-allow-free-responses="" class="validate-float required"></div>
                    </div>
                    <div class="oneField field-container-D">
                    <label id="tfa_mn_fob-L" class="label preField reqMark" for="tfa_mn_fob"><b>MN FOB Point</b></label><br><div class="inputWrapper">
                <input type="text" id="tfa_mn_fob" name="tfa_mn_fob" value="" aria-required="true" aria-describedby="tfa_mn_fob-HH" title="MN FOB" data-dataset-allow-free-responses="" class="validate-alphanum required"><br/>
                </div>
                </div>
                <div class="oneField field-container-D discounts-div" id="tfa_mn_disc-D">
                <label id="tfa_mn_disc-L" class="label preField " for="tfa_mn_disc"><b>Discounts?</b></label><br><div class="inputWrapper">
                <select id="tfa_mn_disc" name="tfa_mn_disc" title="Discounts?" aria-describedby="tfa_mn_disc-HH" class=""><option value="">Please select...</option>
                <option id="tfa_966" data-conditionals="#tfa_967" class="">Yes</option></select><br/>
                <span class="field-hint-inactive" id="tfa_mn_disc-H"><span id="tfa_mn_disc-HH" class="hint">(Select "Yes" to modify discounts)</span></span>
                </div>
                </div>
                </div></div>
                <div id="tfa_967" class="section group" data-condition="`#tfa_966`">

                    <label class="label preField"><u style=""><b>Discounts</b></u></label><br>
                    <div class="section group inline">
                <table class="gridLayout ">
                            <thead>
                            <tr class="headerRow">
                <th> </th>
                                <th scope="col">Quantity </th>
                                <th scope="col">On </th>
                                <th scope="col">Type </th>
                                <th scope="col">Amount </th>
                            </tr>
                            </thead>
                            <tbody><tr class="section    alternate-0 ">

                                <?php $st='mn'; for($dis_id=1;$dis_id<=10;$dis_id++) {  ?>

                                <th scope="row" class="headerCol">MN <?=$dis_id?> </th>
                                <td class="">
                                    <div class="oneField field-container-D    " >
                                    <div class="inputWrapper">
                                        <input type="number" min='0' max='' step='1' style="max-width:120px" id="tfa_<?=$st?>_qty<?=$dis_id?>_quan" name="tfa_<?=$st?>_qty<?=$dis_id?>_quan" value="" title="Quantity" data-dataset-allow-free-responses="" data-order='<?=$dis_id?>' class="discount_quan_<?=$dis_id?> discount_quan">
                </div>
                </div>
                                </td>
                                <td class="">
                                    <div class="oneField field-container-D" >
                                    <div class="inputWrapper">
                                    <select style="max-width:100px" id="tfa_<?=$st?>_qty<?=$dis_id?>_on" name="tfa_<?=$st?>_qty<?=$dis_id?>_on" title="On" class="">
                                        <option value="">Please select...</option>
                                        <option class="">Case</option>
                                        <option class="">Bottle</option>
                                    </select>
                </div>
                </div>
                                </td>

                                <td class="">
                                    <div class="oneField field-container-D" >
                                        <div class="inputWrapper">
                                            <select style="max-width:100px" id="tfa_<?=$st?>_qty<?=$dis_id?>_type" name="tfa_<?=$st?>_qty<?=$dis_id?>_type" title="Type"  <?= $dis_id>1 ? 'disabled="true"' : '' ?> class="select_qty_type">
                                                <option value="">Please select...</option>
                                                <option class="">$</option>
                                                <option class="">%</option>
                                            </select>
                                        </div>
                </div>
                                </td>

                                <td class="">
                                    <div class="oneField field-container-D" >
                                    <div class="inputWrapper">
                                        <input type="number" style="max-width:120px" id="tfa_<?=$st?>_qty<?=$dis_id?>_amt" name="tfa_<?=$st?>_qty<?=$dis_id?>_amt" value="" aria-describedby="tfa_<?=$st?>_qty1_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_<?=$st?>_qty<?=$dis_id?>_amt-H"><br/>
                                        <span id="tfa_<?=$st?>_qty<?=$dis_id?>_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
                                    </div>
                                    </div>
                                </td>
                                <td style="display: none"></td>
                                </tr>

                                <?php } ?>

                            </tbody>
                        </table>

                </div>
            </div>

        </form>
        </div>

      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-save">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>

<div class="modal modal-wide fade" id="dlg-edit-tfa_nj" tabindex="-1" role="dialog" aria-labelledby="dlg-label-tfa-nj" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="dlg-label-tfa-nj">NJ Price Posting</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">

        <div class="wFormContainer" style="width:auto;margin:auto;" >
        <form id="form-tfa_nj" class="wForm" style="margin:auto;padding:0;">
            <input type="hidden" name="tfa_nj_state" value="Yes">
            <input type="hidden" class="itemID" name="itemID_" value="">


            <div id="tfa_1189" class="section inline group">

                <div class="oneField field-container-D">
                    <label class="label preField " ><b>Item Description</b></label><br/>
                    <div class="inputWrapper">
                        <input type="text" class="tfa_item_desc" id="tfa_nj_item_desc" name="tfa_item_desc" value="" title="Item Description" data-dataset-allow-free-responses="" class="required validate-alphanum">
                    </div>
                </div>

                <div class="oneField field-container-D">
                    <label class="label preField"><b>Effective Month</b></label><br/>
                    <div class="inputWrapper">
                        <select class="tfa_effective_month" name="tfa_effective_month">
                            <option value="">Please select...</option>
                        </select>
                    </div>
                </div>

            </div>

            <div id="tfa_1198" class="section group" data-condition="`#tfa_1196`"><div id="tfa_1199" class="section inline group">

                <div class="oneField field-container-D">
                    <label><b>Container Size</b></label><br/>
                    <div class="inputWrapper">
                        <input type="text" id="tfa_nj_container_size" class="tfa_container_size" value="" readonly>
                    </div>
                </div>
                <div class="oneField field-container-D">
                    <label><b>Case Qty</b></label><br/>
                    <div class="inputWrapper">
                        <input type="text" id="tfa_nj_bottles_per_case" class="tfa_bottles_per_case" value="" readonly>
                </div>
                </div>
                <div class="oneField field-container-D">
                    <label><b>Vintage</b></label><br/>
                    <div class="inputWrapper">
                        <input type="text" id="tfa_nj_vintage" class="tfa_vintage" value="" readonly>
                </div>
                </div>
                <div class="oneField field-container-D    " id="tfa_nj_price_bot-D">
                    <label id="tfa_nj_price_bot-L" class="label preField reqMark" for="tfa_nj_price_bot"><b>NJ Price / Bottle</b></label><br>
                    <div class="inputWrapper"><input type="number" min=0.00 step=0.01 id="tfa_nj_price_bot" name="tfa_nj_price_bot" value="" aria-required="true" title="NJ Price / Bottle" data-dataset-allow-free-responses="" class="validate-float required"></div>
                </div>
                <div class="oneField field-container-D    " id="tfa_nj_price_case-D">
                    <label id="tfa_nj_price_case-L" class="label preField reqMark" for="tfa_nj_price_case"><b>NJ Price / Case</b></label><br>
                    <div class="inputWrapper"><input readonly type="number" min=0.00 step=0.01 id="tfa_nj_price_case" name="tfa_nj_price_case" value="" aria-required="true" title="NJ Price / Case" data-dataset-allow-free-responses="" class="validate-float required"></div>
                </div>
                
                <!--
                <div class="oneField field-container-D    " id="tfa_nj_fob-D">
                    <label id="tfa_nj_fob-L" class="label preField reqMark" for="tfa_nj_fob"><b>NJ FOB Point</b></label><br>
                    <div class="inputWrapper">
                <input type="text" id="tfa_nj_fob" name="tfa_nj_fob" value="" aria-required="true" aria-describedby="tfa_nj_fob-HH" title="NJ FOB" data-dataset-allow-free-responses="" class="validate-alphanum required"><br/>
                </div>
                </div>
                -->

                <div class="oneField field-container-D discounts-div" id="tfa_nj_disc-D">
                <label id="tfa_nj_disc-L" class="label preField " for="tfa_nj_disc"><b>Discounts?</b></label><br><div class="inputWrapper">
                <select id="tfa_nj_disc" name="tfa_nj_disc" title="Discounts?" aria-describedby="tfa_nj_disc-HH" class=""><option value="">Please select...</option>
                <option id="tfa_1206" data-conditionals="#tfa_1207" class="">Yes</option></select><br/>
                <span class="field-hint-inactive" id="tfa_nj_disc-H"><span id="tfa_nj_disc-HH" class="hint">(Select "Yes" to modify discounts)</span></span>
                </div>
                </div>
                </div></div>
                <div id="tfa_1207" class="section group" data-condition="`#tfa_1206`">
                    <label class="label preField" id="tfa_1207-L"><u style=""><b>Discounts</b></u></label><br>
                    <div id="tfa_1794" class="section inline group">
                
                <div class="oneField field-container-D    " id="tfa_nj_mix_match-D">
                    <label id="tfa_nj_mix_match-L" class="label preField " for="tfa_nj_mix_match"><b>NJ Mix Match</b></label><br><div class="inputWrapper">
                        <select id="tfa_nj_mix_match" name="tfa_nj_mix_match" title="NJ Mix Match" class="">
                            <option value="">Please select...</option>
                            <option value="Yes">Yes</option>
                        </select></div>
                </div>
                <div class="oneField field-container-D    " id="tfa_nj_fam_price-D">
                        <label id="tfa_nj_fam_price-L" class="label preField " for="tfa_nj_fam_price"><b>NJ Family Pricing</b></label><br><div class="inputWrapper">
                        <select id="tfa_nj_fam_price" name="tfa_nj_fam_price" title="NJ Family Pricing" class="">
                        <option value="">Please select...</option>
                        <option value="Yes">Yes</option>
                        </select>
                </div>
                </div>
                </div>

                <div id="tfa_1208" class="section group">
                <table class="gridLayout ">
                            <thead>
                            <tr class="headerRow">
                <th> </th>
                                <th scope="col">Quantity </th>
                                <th scope="col">On </th>
                                <th scope="col">Type </th>
                                <th scope="col">Amount </th>
                            </tr>
                            </thead>
                            <tbody><tr class="section    alternate-0 ">

                                <?php $st='nj'; for($dis_id=1;$dis_id<=10;$dis_id++) {  ?>

                                <th scope="row" class="headerCol">NJ <?=$dis_id?> </th>
                                <td class="">
                                    <div class="oneField field-container-D    " >
                                    <div class="inputWrapper">
                                        <input type="number" min='0' max='' step='1' style="max-width:120px" id="tfa_<?=$st?>_qty<?=$dis_id?>_quan" name="tfa_<?=$st?>_qty<?=$dis_id?>_quan" value="" title="Quantity" data-dataset-allow-free-responses="" data-order='<?=$dis_id?>' class="discount_quan_<?=$dis_id?> discount_quan">
                </div>
                </div>
                                </td>
                                <td class="">
                                    <div class="oneField field-container-D" >
                                    <div class="inputWrapper">
                                    <select style="max-width:100px" id="tfa_<?=$st?>_qty<?=$dis_id?>_on" name="tfa_<?=$st?>_qty<?=$dis_id?>_on" title="On" class="">
                                        <option value="">Please select...</option>
                                        <option class="">Case</option>
                                        <option class="">Bottle</option>
                                    </select>
                                    </div>
                                    </div>
                                </td>

                                <td class="">
                                    <div class="oneField field-container-D" >
                                        <div class="inputWrapper">
                                            <select style="max-width:100px" id="tfa_<?=$st?>_qty<?=$dis_id?>_type" name="tfa_<?=$st?>_qty<?=$dis_id?>_type" title="Type"  <?= $dis_id>1 ? 'disabled="true"' : '' ?> class="select_qty_type">
                                                <option value="">Please select...</option>
                                                <option class="">$</option>
                                                <option class="">%</option>
                                            </select>
                                        </div>
                </div>
                                </td>

                                <td class="">
                                    <div class="oneField field-container-D" >
                                    <div class="inputWrapper">
                                        <input type="number" style="max-width:120px" id="tfa_<?=$st?>_qty<?=$dis_id?>_amt" name="tfa_<?=$st?>_qty<?=$dis_id?>_amt" value="" aria-describedby="tfa_<?=$st?>_qty1_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_<?=$st?>_qty<?=$dis_id?>_amt-H"><br/>
                                        <span id="tfa_<?=$st?>_qty<?=$dis_id?>_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
                </div>
                </div>
                                </td>
                                <td style="display: none"></td>
                                </tr>

                                <?php } ?>

                            </tbody>
                        </table>

                </div>
            </div>

        </form>
        </div>

      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-save">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>


<div class="modal modal-wide fade" id="dlg-edit-tfa_nyw" tabindex="-1" role="dialog" aria-labelledby="dlg-label-tfa-nyw" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="dlg-label-tfa-nyw">NY Wholesale Price Posting</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">

        <div class="wFormContainer" style="width:auto;margin:auto;" >
        <form id="form-tfa_nyw" class="wForm" style="margin:auto;padding:0;">
            <input type="hidden" name="tfa_nyw_state" value="Yes">
            <input type="hidden" class="itemID" name="itemID_" value="">


                <div id="tfa_1669" class="section inline group">

                    <div class="oneField field-container-D" >
                        <label class="label preField " ><b>Item Description</b></label><br/>
                        <div class="inputWrapper">
                            <input type="text" class="tfa_item_desc" id="tfa_nyw_item_desc" name="tfa_item_desc" value="" title="Item Description" data-dataset-allow-free-responses="" class="required validate-alphanum">
                        </div>
                    </div>

                    <div class="oneField field-container-D">
                        <label class="label preField"><b>Effective Month</b></label><br/>
                        <div class="inputWrapper">
                            <select class="tfa_effective_month" name="tfa_effective_month">
                                <option value="">Please select...</option>
                            </select>
                    </div>
                    </div>
                
                </div>

                <div id="tfa_1678" class="section group" data-condition="`#tfa_1676`">
                    <div id="tfa_1679" class="section inline group">

                    <div class="oneField field-container-D">
                        <label><b>Container Size</b></label><br/>
                        <div class="inputWrapper">
                            <input type="text" id="tfa_nyw_container_size" class="tfa_container_size" value="" readonly>
                    </div>
                    </div>

                    <div class="oneField field-container-D">
                        <label><b>Case Qty</b></label><br/>
                        <div class="inputWrapper">
                            <input type="text" id="tfa_nyw_bottles_per_case" class="tfa_bottles_per_case" value="" readonly>
                        </div>
                    </div>

                    <div class="oneField field-container-D">
                        <label><b>Vintage</b></label><br/>
                        <div class="inputWrapper">
                            <input type="text" id="tfa_nyw_vintage" class="tfa_vintage" value="" readonly>
                    </div>
                    </div>

                    <div class="oneField field-container-D">
                    <label id="tfa_nyw_price_bot-L" class="label preField reqMark" for="tfa_nyw_price_bot"><b>NYW Price / Bottle</b></label><br>
                    <div class="inputWrapper"><input type="number" min=0.00 step=0.01 id="tfa_nyw_price_bot" name="tfa_nyw_price_bot" value="" aria-required="true" title="NYW Price / Bottle" data-dataset-allow-free-responses="" class="validate-float required"></div>
                    </div>
                    <div class="oneField field-container-D">
                    <label id="tfa_nyw_price_case-L" class="label preField reqMark" for="tfa_nyw_price_case"><b>NYW Price / Case</b></label><br>
                    <div class="inputWrapper"><input readonly type="number" min=0.00 step=0.01 id="tfa_nyw_price_case" name="tfa_nyw_price_case" value="" aria-required="true" title="NYW Price / Case" data-dataset-allow-free-responses="" class="validate-float required"></div>
                    </div>
                    <div class="oneField field-container-D">
                    <label id="tfa_nyw_fob-L" class="label preField reqMark" for="tfa_nyw_fob"><b>NYW FOB Point</b></label><br><div class="inputWrapper">
                    <input type="text" id="tfa_nyw_fob" name="tfa_nyw_fob" value="" aria-required="true" aria-describedby="tfa_nyw_fob-HH" title="NYW FOB" data-dataset-allow-free-responses="" class="validate-alphanum required"><br/>
                    </div>
                    </div>
                    <div class="oneField field-container-D">
                    <label id="tfa_nyw_dist-L" class="label preField reqMark" for="tfa_nyw_dist"><b>NY Distributor</b></label><br><div class="inputWrapper">
                        <select multiple id="tfa_nyw_dist" name="tfa_nyw_dist[]" title="NY Distributor" aria-required="true" class="distributors-list required"></select>
                    </div>
                    </div>
                    </div>
                    <div class="oneField field-container-D discounts-div" id="tfa_nyw_disc-D">
                    <label id="tfa_nyw_disc-L" class="label preField " for="tfa_nyw_disc"><b>Discounts?</b></label><br><div class="inputWrapper">
                    <select id="tfa_nyw_disc" name="tfa_nyw_disc" title="Discounts?" aria-describedby="tfa_nyw_disc-HH" class=""><option value="">Please select...</option>
                    <option id="tfa_1686" data-conditionals="#tfa_1687" class="">Yes</option></select><span class="field-hint-inactive" id="tfa_nyw_disc-H"><span id="tfa_nyw_disc-HH" class="hint">(Select "Yes" to modify discounts)</span></span>
                    </div>
                    </div>
                    </div>
                    <div id="tfa_1687" class="section group" data-condition="`#tfa_1686`">

                    
                    <label class="label preField"><u style=""><b>Discounts</b></u></label><br>
                    <div class="section group inline">
                    <table class="gridLayout ">
                            <thead>
                            <tr class="headerRow">
                    <th> </th>
                                <th scope="col">Quantity </th>
                                <th scope="col">On </th>
                                <th scope="col">Type </th>
                                <th scope="col">Amount </th>
                            </tr>
                            </thead>
                            <tbody><tr class="section    alternate-0 ">

                                <?php $st='nyw'; for($dis_id=1;$dis_id<=10;$dis_id++) {  ?>

                                <th scope="row" class="headerCol">NYW <?=$dis_id?> </th>
                                <td class="">
                                    <div class="oneField field-container-D    " >
                                    <div class="inputWrapper">
                                        <input type="number" min='0' max='' step='1' style="max-width:120px" id="tfa_<?=$st?>_qty<?=$dis_id?>_quan" name="tfa_<?=$st?>_qty<?=$dis_id?>_quan" value="" title="Quantity" data-dataset-allow-free-responses="" data-order='<?=$dis_id?>' class="discount_quan_<?=$dis_id?> discount_quan">
                                    </div>
                    </div>
                                </td>
                                <td class="">
                                    <div class="oneField field-container-D" >
                                    <div class="inputWrapper">
                                    <select style="max-width:100px" id="tfa_<?=$st?>_qty<?=$dis_id?>_on" name="tfa_<?=$st?>_qty<?=$dis_id?>_on" title="On" class="">
                                        <option value="">Please select...</option>
                                        <option class="">Case</option>
                                        <option class="">Bottle</option>
                                    </select>
                    </div>
                    </div>
                                </td>

                                <td class="">
                                    <div class="oneField field-container-D" >
                                        <div class="inputWrapper">
                                            <select style="max-width:100px" id="tfa_<?=$st?>_qty<?=$dis_id?>_type" name="tfa_<?=$st?>_qty<?=$dis_id?>_type" title="Type"  <?= $dis_id>1 ? 'disabled="true"' : '' ?> class="select_qty_type">
                                                <option value="">Please select...</option>
                                                <option class="">$</option>
                                                <option class="">%</option>
                                            </select>
                    </div>
                    </div>
                                </td>

                                <td class="">
                                    <div class="oneField field-container-D" >
                                    <div class="inputWrapper">
                                        <input type="number" style="max-width:120px" id="tfa_<?=$st?>_qty<?=$dis_id?>_amt" name="tfa_<?=$st?>_qty<?=$dis_id?>_amt" value="" aria-describedby="tfa_<?=$st?>_qty1_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_<?=$st?>_qty<?=$dis_id?>_amt-H"><br/>
                                        <span id="tfa_<?=$st?>_qty<?=$dis_id?>_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
                                    </div>
                                    </div>
                                </td>
                                <td style="display: none"></td>
                                </tr>

                                <?php } ?>

                            </tbody>
                        </table>

                    </div>
                    </div>



        </form>
        </div>

      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-save">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>



<div class="modal modal-wide fade" id="dlg-edit-tfa_nyr" tabindex="-1" role="dialog" aria-labelledby="dlg-label-tfa-nyr" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="dlg-label-tfa-nyr">NY Retail Price Posting</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">

        <div class="wFormContainer" style="width:auto;margin:auto;" >
        <form id="form-tfa_nyr" class="wForm" style="margin:auto;padding:0;">
            <input type="hidden" name="tfa_nyr_state" value="Yes">
            <input type="hidden" class="itemID" name="itemID_" value="">

            <div id="tfa_1429" class="section inline group">
                <div class="oneField field-container-D">
                    <label class="label preField "><b>Item Description</b></label><br/>
                    <div class="inputWrapper">
                        <input type="text" class="tfa_item_desc" id="tfa_nyr_item_desc" name="tfa_item_desc" value="" title="Item Description" data-dataset-allow-free-responses="" class="required validate-alphanum">
                    </div>
                </div>

                <div class="oneField field-container-D">
                    <label class="label preField"><b>Effective Month</b></label><br/>
                    <div class="inputWrapper">
                        <select class="tfa_effective_month" name="tfa_effective_month">
                            <option value="">Please select...</option>
                        </select>
                </div>
                </div>
             </div>

            <div id="tfa_1438" class="section group" data-condition="`#tfa_1436`">
                <div id="tfa_1439" class="section inline group">

                    <div class="oneField field-container-D">
                        <label><b>Container Size</b></label><br/>
                        <div class="inputWrapper">
                            <input type="text" id="tfa_nyr_container_size" class="tfa_container_size" value="" readonly>
                </div>
                </div>

                    <div class="oneField field-container-D">
                        <label><b>Case Qty</b></label><br/>
                        <div class="inputWrapper">
                            <input type="text" id="tfa_nyr_bottles_per_case" class="tfa_bottles_per_case" value="" readonly>
                        </div>
                    </div>

                    <div class="oneField field-container-D">
                        <label><b>Vintage</b></label><br/>
                        <div class="inputWrapper">
                            <input type="text" id="tfa_nyr_vintage" class="tfa_vintage" value="" readonly>
                        </div>
                </div>

                    <div class="oneField field-container-D    " id="tfa_nyr_price_bot-D">
                    <label id="tfa_nyr_price_bot-L" class="label preField reqMark" for="tfa_nyr_price_bot"><b>NYR Price / Bottle</b></label><br>
                    <div class="inputWrapper"><input type="number" min=0.00 step=0.01 id="tfa_nyr_price_bot" name="tfa_nyr_price_bot" value="" aria-required="true" title="NYR Price / Bottle" data-dataset-allow-free-responses="" class="validate-float required"></div>
                </div>
                    
                    <div class="oneField field-container-D    " id="tfa_nyr_price_case-D">
                    <label id="tfa_nyr_price_case-L" class="label preField reqMark" for="tfa_nyr_price_case"><b>NYR Price / Case</b></label><br>
                    <div class="inputWrapper"><input readonly type="number" min=0.00 step=0.01 id="tfa_nyr_price_case" name="tfa_nyr_price_case" value="" aria-required="true" title="NYR Price / Case" data-dataset-allow-free-responses="" class="validate-float required"></div>
                    </div>

                    <!--
                <div class="oneField field-container-D    " id="tfa_nyr_fob-D">
                        <label id="tfa_nyr_fob-L" class="label preField reqMark" for="tfa_nyr_fob"><b>NYR FOB Point</b></label><br>
                        <div class="inputWrapper">
                <input type="text" id="tfa_nyr_fob" name="tfa_nyr_fob" value="" aria-required="true" aria-describedby="tfa_nyr_fob-HH" title="NYR FOB" data-dataset-allow-free-responses="" class="validate-alphanum required"><br/>
                </div>
                </div>
                    -->

                    <div class="oneField field-container-D discounts-div" id="tfa_nyr_disc-D">
                        <label id="tfa_nyr_disc-L" class="label preField " for="tfa_nyr_disc"><b>Discounts?</b></label><br>
                        <div class="inputWrapper">
                <select id="tfa_nyr_disc" name="tfa_nyr_disc" title="Discounts?" aria-describedby="tfa_nyr_disc-HH" class=""><option value="">Please select...</option>
                <option id="tfa_1446" data-conditionals="#tfa_1447" class="">Yes</option></select><span class="field-hint-inactive" id="tfa_nyr_disc-H"><span id="tfa_nyr_disc-HH" class="hint">(Select "Yes" to modify discounts)</span></span>
                </div>
                </div>

                </div>

                </div>
                <div id="tfa_1447" class="section group" data-condition="`#tfa_1446`">
                <label class="label preField" id="tfa_1447-L"><u style=""><b>Discounts</b></u></label><br><div id="tfa_1448" class="section group">
                <!--<div class="oneField field-container-D    " id="tfa_nyr_mix_match-D">
                <label id="tfa_nyr_mix_match-L" class="label preField " for="tfa_nyr_mix_match"><b>NYR Mix Match</b></label><br><div class="inputWrapper"><select id="tfa_nyr_mix_match" name="tfa_nyr_mix_match" title="NYR Mix Match" class=""><option value="">Please select...</option>
                <option id="tfa_1796" class="">Yes</option></select></div>
                </div>-->
                <table class="gridLayout ">
                            <thead>
                            <tr class="headerRow">
                <th> </th>
                                <th scope="col">Quantity </th>
                                <th scope="col">On </th>
                                <th scope="col">Type </th>
                                <th scope="col">Amount </th>
                            </tr>
                            </thead>
                            <tbody><tr class="section    alternate-0 ">

                                <?php $st='nyr'; for($dis_id=1;$dis_id<=10;$dis_id++) {  ?>

                                <th scope="row" class="headerCol">NYR <?=$dis_id?> </th>
                                <td class="">
                                    <div class="oneField field-container-D    " >
                                    <div class="inputWrapper">
                                        <input type="number" min='0' max='' step='1' style="max-width:120px" id="tfa_<?=$st?>_qty<?=$dis_id?>_quan" name="tfa_<?=$st?>_qty<?=$dis_id?>_quan" value="" title="Quantity" data-dataset-allow-free-responses="" data-order='<?=$dis_id?>' class="discount_quan_<?=$dis_id?> discount_quan" >
                </div>
                </div>
                                </td>
                                <td class="">
                                    <div class="oneField field-container-D" >
                                    <div class="inputWrapper">
                                    <select style="max-width:100px" id="tfa_<?=$st?>_qty<?=$dis_id?>_on" name="tfa_<?=$st?>_qty<?=$dis_id?>_on" title="On" class="">
                                        <option value="">Please select...</option>
                                        <option class="">Case</option>
                                        <option class="">Bottle</option>
                                    </select>
                </div>
                </div>
                                </td>

                                <td class="">
                                    <div class="oneField field-container-D" >
                                        <div class="inputWrapper">
                                            <select style="max-width:100px" id="tfa_<?=$st?>_qty<?=$dis_id?>_type" name="tfa_<?=$st?>_qty<?=$dis_id?>_type" title="Type"  <?= $dis_id>1 ? 'disabled="true"' : '' ?> class="select_qty_type">
                                                <option value="">Please select...</option>
                                                <option class="">$</option>
                                                <option class="">%</option>
                                            </select>
                </div>
                </div>
                                </td>

                                <td class="">
                                    <div class="oneField field-container-D" >
                                    <div class="inputWrapper">
                                        <input type="number" style="max-width:120px" id="tfa_<?=$st?>_qty<?=$dis_id?>_amt" name="tfa_<?=$st?>_qty<?=$dis_id?>_amt" value="" aria-describedby="tfa_<?=$st?>_qty1_amt-HH" title="Amount" data-dataset-allow-free-responses="" class=""><span class="field-hint-inactive" id="tfa_<?=$st?>_qty<?=$dis_id?>_amt-H"><br/>
                                        <span id="tfa_<?=$st?>_qty<?=$dis_id?>_amt-HH" class="hint">($ or % amount based on "Type")</span></span>
                                    </div>
                                    </div>
                                </td>
                                <td style="display: none"></td>
                                </tr>

                                <?php } ?>

                            </tbody>
                        </table>

                </div>
            </div>

        </form>
        </div>

      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-save">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>


<div class="modal modal-wide fade" id="dlg-edit-tfa_os" tabindex="-1" role="dialog" aria-labelledby="dlg-label-tfa-os" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="dlg-label-tfa-os">Other States Pricing</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">

        <div class="wFormContainer" style="width:auto;margin:auto;" >
        <form id="form-tfa_os" class="wForm" style="margin:auto;padding:0;">
            <input type="hidden" name="tfa_os_state"  id="tfa_os_state" value="Yes">
            <input type="hidden" class="itemID" name="itemID_" value="">


            <div class="section inline group">

                <div class="oneField field-container-D">
                    <label class="label preField "><b>Item Description</b></label><br/>
                    <div class="inputWrapper">
                        <input type="text" class="tfa_item_desc" id="tfa_os_item_desc" name="tfa_item_desc" value="" title="Item Description" data-dataset-allow-free-responses="" class="required validate-alphanum">
                    </div>
                </div>

                <div class="oneField field-container-D">
                    <label class="label preField"><b>Effective Month</b></label><br/>
                    <div class="inputWrapper">
                        <select class="tfa_effective_month" name="tfa_effective_month">
                            <option value="">Please select...</option>
                        </select>
                    </div>                        
                </div>

            </div>

            <div  class="section inline group">

                <div class="oneField field-container-D    " id="tfa_os_price_bottle-D">
                    <label id="tfa_os_price_bottle-L" class="label preField reqMark" for="tfa_os_price_bottle"><b>Other State Price / Bottle</b></label><br/>
                    <div class="inputWrapper"><input type="number" min=0.00 step=0.01 id="tfa_os_price_bottle" name="tfa_os_price_bottle" value="" aria-required="true" title="Other State Price / Bottle" data-dataset-allow-free-responses="" class="validate-float required"></div>
                </div>

                <div class="oneField field-container-D" id="tfa_os_price_state-D">
                    <label id="tfa_os_price_state_type-L" class="label preField reqMark" for="tfa_os_price_state"><b>Other State</b></label><br/>
                    <div class="inputWrapper">
                        <select id="tfa_os_price_state" name="tfa_os_price_state" title="Other State" aria-required="true" aria-describedby="tfa_os_price_state_-HH" class="required">
                        <option value="NL" id="tfa_states1_Alabama" class="">National</option>
                        <option value="AL" id="tfa_states1_Alabama" class="">Alabama</option>
                        <option value="AK" id="tfa_states1_Alaska" class="">Alaska</option>
                        <option value="AZ" id="tfa_states1_Arizona" class="">Arizona</option>
                        <option value="AR" id="tfa_states1_Arkansas" class="">Arkansas</option>
                        <option value="CA" id="tfa_states1_California" class="">California</option>
                        <option value="CO" id="tfa_states1_Colorado" class="">Colorado</option>
                        <option value="CT" id="tfa_states1_Connecticut" class="">Connecticut</option>
                        <option value="DE" id="tfa_states1_Delaware" class="">Delaware</option>
                        <option value="DC" id="tfa_states1_District_of_Columbia" class="">District of Columbia</option>
                        <option value="FL" id="tfa_states1_Florida" class="">Florida</option>
                        <option value="GA" id="tfa_states1_Georgia" class="">Georgia</option>
                        <option value="HI" id="tfa_states1_Hawaii" class="">Hawaii</option>
                        <option value="ID" id="tfa_states1_Idaho" class="">Idaho</option>
                        <option value="IL" id="tfa_states1_Illinois" class="">Illinois</option>
                        <option value="IN" id="tfa_states1_Indiana" class="">Indiana</option>
                        <option value="IA" id="tfa_states1_Iowa" class="">Iowa</option>
                        <option value="KS" id="tfa_states1_Kansas" class="">Kansas</option>
                        <option value="KY" id="tfa_states1_Kentucky" class="">Kentucky</option>
                        <option value="LA" id="tfa_states1_Louisiana" class="">Louisiana</option>
                        <option value="ME" id="tfa_states1_Maine" class="">Maine</option>
                        <option value="MD" id="tfa_states1_Maryland" class="">Maryland</option>
                        <option value="MA" id="tfa_states1_Massachusetts" class="">Massachusetts</option>
                        <option value="MI" id="tfa_states1_Michigan" class="">Michigan</option>
                        <option value="MN" id="tfa_states1_Minnesota" class="">Minnesota</option>
                        <option value="MS" id="tfa_states1_Mississippi" class="">Mississippi</option>
                        <option value="MO" id="tfa_states1_Missouri" class="">Missouri</option>
                        <option value="MT" id="tfa_states1_Montana" class="">Montana</option>
                        <option value="NE" id="tfa_states1_Nebraska" class="">Nebraska</option>
                        <option value="NV" id="tfa_states1_Nevada" class="">Nevada</option>
                        <option value="NH" id="tfa_states1_New_Hampshire" class="">New Hampshire</option>
                        <option value="NJ" id="tfa_states1_New_Jersey" class="">New Jersey</option>
                        <option value="NM" id="tfa_states1_New_Mexico" class="">New Mexico</option>
                        <option value="NY" id="tfa_states1_New_York" class="">New York</option>
                        <option value="NC" id="tfa_states1_North_Carolina" class="">North Carolina</option>
                        <option value="ND" id="tfa_states1_North_Dakota" class="">North Dakota</option>
                        <option value="OH" id="tfa_states1_Ohio" class="">Ohio</option>
                        <option value="OK" id="tfa_states1_Oklahoma" class="">Oklahoma</option>
                        <option value="OR" id="tfa_states1_Oregon" class="">Oregon</option>
                        <option value="PA" id="tfa_states1_Pennsylvania" class="">Pennsylvania</option>
                        <option value="RI" id="tfa_states1_Rhode_Island" class="">Rhode Island</option>
                        <option value="SC" id="tfa_states1_South_Carolina" class="">South Carolina</option>
                        <option value="SD" id="tfa_states1_South_Dakota" class="">South Dakota</option>
                        <option value="TN" id="tfa_states1_Tennessee" class="">Tennessee</option>
                        <option value="TX" id="tfa_states1_Texas" class="">Texas</option>
                        <option value="UT" id="tfa_states1_Utah" class="">Utah</option>
                        <option value="VT" id="tfa_states1_Vermont" class="">Vermont</option>
                        <option value="VA" id="tfa_states1_Virginia" class="">Virginia</option>
                        <option value="WA" id="tfa_states1_Washington" class="">Washington</option>
                        <option value="WV" id="tfa_states1_West_Virginia" class="">West Virginia</option>
                        <option value="WI" id="tfa_states1_Wisconsin" class="">Wisconsin</option>
                        <option value="WY" id="tfa_states1_Wyoming" class="">Wyoming</option>
                        </select>
                    </div>
                </div>


            </div>



        </form>
        </div>

        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>

      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-save">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>



<div class="modal modal-wide fade" id="dlg-delete" tabindex="-1" role="dialog" aria-labelledby="dlg-delete-label" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="dlg-delete-label">Delete the record</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          Do you want to delete the record ?
      </div>
	  <div id='preview'></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="btn-dlg-delete">Delete</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal modal-wide fade" id="dlg-undelete" tabindex="-1" role="dialog" aria-labelledby="dlg-undelete-label" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="dlg-undelete-label">Undelete the record</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            Do you want to undelete the record ?
      </div>
	  <div id='preview'></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="btn-dlg-undelete">Undelete</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal modal-wide fade" id="dlg-set-distributors" tabindex="-1" role="dialog" aria-labelledby="dlg-set-distributors-label" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="dlg-set-distributors-label">Set Distributors</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <select multiple id="dlg-distributors-list" name="dlg-distributors-list" class="distributors-list" title="Distributors" aria-describedby="dlg-distributors-list-HH">
        </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="btn-dlg-set-distributors">Set</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal modal-wide fade" id="dlg-finalize-result" tabindex="-1" role="dialog" aria-labelledby="dlg-finalize-label" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="dlg-finalize-label">Status</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="dlg-finalize-result-text" class="modal-body">
            
      </div>
	  <div id='preview'></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



<!--  /Modals -->



<script>


    function checkBoxFormatter(value, row, index) {
        return '<input type="checkbox" name="selected['+row.ID+']" class="row-selected" value=1 '+(row.status!=='finalized'?'checked':'')+' >';
    }

    function operateFormatter(value, row, index) {
        var view_type = $('#view_type').val();
        var view_state = $('#view_state').val();
        var discount_states = ['tfa_ma','tfa_mn','tfa_nj','tfa_nyr','tfa_nyw'];
        if (view_type=='Deleted Items' || view_state=='Deleted' ) {
            return [
                '<button type="button" style="min-width:90px" class="btn btn-outline-secondary btn-undelete btn-sm bg_arrow_lightblue with-item-'+row.itemID+'" data-id="'+row.ID+'">Undelete</button>',
            ].join('')
        } else if (discount_states.indexOf(view_state)>-1) {
            if (row.stock_uom=='TEST') { //no discounts for these items for example
                return [
                    '<button type="button" style="min-width:90px" class="btn btn-outline-secondary btn-delete btn-sm bg_arrow_lightblue with-item-'+row.itemID+'" data-id="'+row.ID+'">Delete</button>',
                ].join('')
            } else {
            return [
                    '<button type="button" style="min-width:90px" class="btn btn-outline-secondary btn-discounts btn-sm bg_arrow_white" data-id="'+row.ID+'">Discounts</button>',
                    '<button type="button" style="min-width:90px" class="btn btn-outline-secondary btn-delete btn-sm bg_arrow_lightblue with-item-'+row.itemID+'" data-id="'+row.ID+'">Delete</button>',
            ].join('')
            }
        } else {
            return [
                '<button type="button" style="min-width:90px" class="btn btn-outline-secondary btn-delete btn-sm bg_arrow_lightblue with-item-'+row.itemID+'" data-id="'+row.ID+'">Delete</button>',
            ].join('')
        }
    }

    //var itemsSource = [];

    var sourceStates = [
        {
            "text": "All States",
            "value": "All"
        },
        {
            "text": "Deleted records",
            "value": "Deleted"
        },
        {
            "text": "National",
            "value": "NL"
        },
        {
            "text": "Alabama",
            "value": "AL"
        },
        {
            "text": "Alaska",
            "value": "AK"
        },
        {
            "text": "American Samoa",
            "value": "AS"
        },
        {
            "text": "Arizona",
            "value": "AZ"
        },
        {
            "text": "Arkansas",
            "value": "AR"
        },
        {
            "text": "California",
            "value": "CA"
        },
        {
            "text": "Colorado",
            "value": "CO"
        },
        {
            "text": "Connecticut",
            "value": "CT"
        },
        {
            "text": "Delaware",
            "value": "DE"
        },
        {
            "text": "District Of Columbia",
            "value": "DC"
        },
        {
            "text": "Florida",
            "value": "FL"
        },
        {
            "text": "Georgia",
            "value": "GA"
        },
        {
            "text": "Guam",
            "value": "GU"
        },
        {
            "text": "Hawaii",
            "value": "HI"
        },
        {
            "text": "Idaho",
            "value": "ID"
        },
        {
            "text": "Illinois",
            "value": "IL"
        },
        {
            "text": "Indiana",
            "value": "IN"
        },
        {
            "text": "Iowa",
            "value": "IA"
        },
        {
            "text": "Kansas",
            "value": "KS"
        },
        {
            "text": "Kentucky",
            "value": "KY"
        },
        {
            "text": "Louisiana",
            "value": "LA"
        },
        {
            "text": "Maine",
            "value": "ME"
        },
        {
            "text": "Maryland",
            "value": "MD"
        },
        {
            "text": "Massachusetts",
            "value": "MA"
        },
        {
            "text": "Michigan",
            "value": "MI"
        },
        {
            "text": "Minnesota",
            "value": "MN"
        },
        {
            "text": "Mississippi",
            "value": "MS"
        },
        {
            "text": "Missouri",
            "value": "MO"
        },
        {
            "text": "Montana",
            "value": "MT"
        },
        {
            "text": "Nebraska",
            "value": "NE"
        },
        {
            "text": "Nevada",
            "value": "NV"
        },
        {
            "text": "New Hampshire",
            "value": "NH"
        },
        {
            "text": "New Jersey",
            "value": "NJ"
        },
        {
            "text": "New Mexico",
            "value": "NM"
        },
        {
            "text": "New York",
            "value": "NY"
        },
        {
            "text": "North Carolina",
            "value": "NC"
        },
        {
            "text": "North Dakota",
            "value": "ND"
        },
        {
            "text": "Ohio",
            "value": "OH"
        },
        {
            "text": "Oklahoma",
            "value": "OK"
        },
        {
            "text": "Oregon",
            "value": "OR"
        },
        {
            "text": "Palau",
            "value": "PW"
        },
        {
            "text": "Pennsylvania",
            "value": "PA"
        },
        {
            "text": "Puerto Rico",
            "value": "PR"
        },
        {
            "text": "Rhode Island",
            "value": "RI"
        },
        {
            "text": "South Carolina",
            "value": "SC"
        },
        {
            "text": "South Dakota",
            "value": "SD"
        },
        {
            "text": "Tennessee",
            "value": "TN"
        },
        {
            "text": "Texas",
            "value": "TX"
        },
        {
            "text": "Utah",
            "value": "UT"
        },
        {
            "text": "Vermont",
            "value": "VT"
        },
        {
            "text": "Virgin Islands",
            "value": "VI"
        },
        {
            "text": "Virginia",
            "value": "VA"
        },
        {
            "text": "Washington",
            "value": "WA"
        },
        {
            "text": "West Virginia",
            "value": "WV"
        },
        {
            "text": "Wisconsin",
            "value": "WI"
        },
        {
            "text": "Wyoming",
            "value": "WY"
        }
    ];

    var updatedValues = [];

    $(document).ready(function(){
        
        $.fn.editable.defaults.mode = 'inline';

            initMain();

        var selectedItemID = '';

        $('.tfa_item_desc').addClass('required');
    });

    var initMain = () => {

        //console.log(itemsSource);

        var view_states = [];

        view_states['Price Posting States'] = [];
        view_states['Price Posting States'].push({id:'tfa_ca',name:'CA - Beer Only'});
        view_states['Price Posting States'].push({id:'tfa_ct',name:'CT Wholesale'});
        view_states['Price Posting States'].push({id:'tfa_ma',name:'MA Wholesale'});
        view_states['Price Posting States'].push({id:'tfa_mn',name:'MN Wholesale'});
        view_states['Price Posting States'].push({id:'tfa_nj',name:'NJ Retail'});
        view_states['Price Posting States'].push({id:'tfa_nyw',name:'NY Wholesale'});
        view_states['Price Posting States'].push({id:'tfa_nyr',name:'NY Retail'});

        view_states['Open States'] = [];
        sourceStates.forEach((st)=>{
            view_states['Open States'].push({
                id: st.value, 
                name: st.text
            });
        });

        view_states['Deleted Items'] = [];
        view_states['Deleted Items'].push({id:'tfa_ca',name:'CA - Beer Only'});
        view_states['Deleted Items'].push({id:'tfa_ct',name:'CT Wholesale'});
        view_states['Deleted Items'].push({id:'tfa_ma',name:'MA Wholesale'});
        view_states['Deleted Items'].push({id:'tfa_mn',name:'MN Wholesale'});
        view_states['Deleted Items'].push({id:'tfa_nj',name:'NJ Retail'});
        view_states['Deleted Items'].push({id:'tfa_nyw',name:'NY Wholesale'});
        view_states['Deleted Items'].push({id:'tfa_nyr',name:'NY Retail'});
        view_states['Deleted Items'].push({id:'tfa_os',name:'Open States'});

        var fobdelSource = [
            {value: 'FOB', text: 'FOB'},
            {value: 'Delivered', text: 'Delivered'}
        ];


        // distributors list
        var distributors = [<?=join(",\n", array_map(function($d) {
            return "{State:'".$d['State']."',DistKey:'".$d['DistKey']."',Name:'".addslashes(utf8_encode($d['Name']))."'}";
        }, $distributorsArray))?>];


        var columns = [];
        columns['tfa_ca'] = [
            {
                formatter: checkBoxFormatter,
            },
            {
                field: 'ID',
                formatter: operateFormatter,
                events: window.operateEvents,
                //visible: false  
            },
            {
                field: 'item_description',
                title: 'Item Description',
                align: 'left',
                valign: 'middle',
                sortable: true  
                },
            {
                field: 'container_size',
                title: 'Container Size',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'bottles_per_case',
                title: 'Bottles per Case',
                align: 'center',
                valign: 'middle',
                sortable: true  
                },
            {
                field: 'vintage',
                title: 'Vintage',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'tfa_ca_change_type',
                title: 'CA Change Type',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'tfa_ca_package',
                title: 'CA Package',
                align: 'center',
                valign: 'middle',
                editable: true,
                sortable: true  
            },
            {
                field: 'tfa_ca_contents',
                title: 'CA Contents',
                align: 'center',
                valign: 'middle',
                editable: true,
                sortable: true  
            },
            {
                field: 'tfa_ca_cont_charge',
                title: 'Container Charge / Deposit',
                align: 'center',
                valign: 'middle',
                editable: true,
                sortable: true  
            },
            {
                field: 'tfa_ca_price_retail',
                title: 'CA Price to Retail',
                align: 'center',
                valign: 'middle',
                editable: true,
                sortable: true  
            },
            {
                field: 'tfa_ca_price_wholesale',
                title: 'CA Price to Wholesale',
                align: 'center',
                valign: 'middle',
                editable: true,
                sortable: true  
            },
            {
                field: 'tfa_ca_fob_del',
                title: 'Price FOB or Delivered?',
                align: 'center',
                valign: 'middle',
                editable: {
                    type: 'select',
                    source: fobdelSource,
                    title: 'Price FOB or Delivered?',
                },
                sortable: true  
            },
            {
                field: 'tfa_ca_foe',
                title: 'Freight on Empties',
                align: 'center',
                valign: 'middle',
                editable: true,
                sortable: true  
            },
            {
                field: 'tfa_ca_counties',
                title: 'Counties',
                align: 'center',
                valign: 'middle',
                editable: true,
                sortable: true  
            },
            {
                field: 'tfa_ca_dist',
                title: 'CA Distributors',
                align: 'center',
                valign: 'middle',
                editable: {
                    type: 'select2',
                    select2: {
                        tags: distributors.filter(d=>d.State==='CA').map(d=>d.Name.replace( /,/g,'.')),
                        //maximumResultsForSearch: 10,
                        tokenSeparators: [","]
                    },
                    title: 'Distributors',
                },
                sortable: true  
            },
            {
                field: 'status',
                title: 'Status',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'user',
                title: 'Added By ',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'tfa_effective_month',
                title: 'Effective Month',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'created',
                title: 'Created At',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },            
        ];

        columns['tfa_ct'] = [
            {
                formatter: checkBoxFormatter,
            },
            {
                field: 'ID',
                formatter: operateFormatter,
                events: window.operateEvents,
            },
            {
                field: 'item_description',
                title: 'Item Description',
                align: 'left',
                valign: 'middle',
                sortable: true  
                },
            {
                field: 'container_size',
                title: 'Container Size',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'bottles_per_case',
                title: 'Bottles per Case',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'vintage',
                title: 'Vintage',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'tfa_ct_change_type',
                title: 'CT Change Type',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'tfa_ct_price_bot',
                title: 'CT Price / Bottle',
                align: 'center',
                valign: 'middle',
                editable: true,
                sortable: true  
            },
            {
                field: 'tfa_ct_price_case',
                title: 'CT Price / Case',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'tfa_ct_fob',
                title: 'CT FOB Point',
                align: 'center',
                valign: 'middle',
                editable: true,
                sortable: true  
            },
            {
                field: 'status',
                title: 'Status',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'user',
                title: 'Added By ',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'tfa_effective_month',
                title: 'Effective Month',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'created',
                title: 'Created At',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },            
        ];

        columns['tfa_ma'] = [
            {
                formatter: checkBoxFormatter,
            },
            {
                field: 'ID',
                formatter: operateFormatter,
                events: window.operateEvents,
            },
            {
                field: 'item_description',
                title: 'Item Description',
                align: 'left',
                valign: 'middle',
                sortable: true  
                },
            {
                field: 'container_size',
                title: 'Container Size',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'bottles_per_case',
                title: 'Bottles per Case',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'vintage',
                title: 'Vintage',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'tfa_ma_change_type',
                title: 'MA Change Type',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'tfa_ma_price_bot',
                title: 'MA Price / Bottle',
                align: 'center',
                valign: 'middle',
                editable: true,
                sortable: true  
            },
            {
                field: 'tfa_ma_price_case',
                title: 'MA Price / Case',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'tfa_ma_fob',
                title: 'MA FOB Point',
                align: 'center',
                valign: 'middle',
                editable: true,
                sortable: true  
            },
            {
                field: 'tfa_ma_dist',
                title: 'MA Distibutors',
                align: 'center',
                valign: 'middle',
                editable: {
                    type: 'select2',
                    select2: {
                        tags: distributors.filter(d=>d.State==='MA').map(d=>d.Name.replace( /,/g,'.')),
                        tokenSeparators: [","]
                    },
                    title: 'Distributors',
                },
                sortable: true  
            },
            {
                field: 'status',
                title: 'Status',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'user',
                title: 'Added By ',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'tfa_effective_month',
                title: 'Effective Month',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'created',
                title: 'Created At',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },            
        ];

        columns['tfa_mn'] = [
            {
                formatter: checkBoxFormatter,
            },
            {
                field: 'ID',
                formatter: operateFormatter,
                events: window.operateEvents,
            },
            {
                field: 'item_description',
                title: 'Item Description',
                align: 'left',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'container_size',
                title: 'Container Size',
                align: 'center',
                valign: 'middle',
                sortable: true  
                },
            {
                field: 'bottles_per_case',
                title: 'Bottles per Case',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'vintage',
                title: 'Vintage',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'tfa_mn_change_type',
                title: 'MN Change Type',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'tfa_mn_price_bot',
                title: 'MN Price / Bottle',
                align: 'center',
                valign: 'middle',
                editable: true,
                sortable: true  
            },
            {
                field: 'tfa_mn_price_case',
                title: 'MN Price / Case',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'tfa_mn_fob',
                title: 'MN FOB Point',
                align: 'center',
                valign: 'middle',
                editable: true,
                sortable: true  
            },
            {
                field: 'status',
                title: 'Status',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'user',
                title: 'Added By ',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'tfa_effective_month',
                title: 'Effective Month',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'created',
                title: 'Created At',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },            
        ];

        columns['tfa_nj'] = [
            {
                formatter: checkBoxFormatter,
            },
            {
                field: 'ID',
                formatter: operateFormatter,
                events: window.operateEvents,
            },
            {
                field: 'item_description',
                title: 'Item Description',
                align: 'left',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'container_size',
                title: 'Container Size',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'bottles_per_case',
                title: 'Bottles per Case',
                align: 'center',
                valign: 'middle',
                sortable: true  
                },
            {
                field: 'vintage',
                title: 'Vintage',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'tfa_nj_change_type',
                title: 'NJ Change Type',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'tfa_nj_price_bot',
                title: 'NJ Price / Bottle',
                align: 'center',
                valign: 'middle',
                editable: true,
                sortable: true  
            },
            {
                field: 'tfa_nj_price_case',
                title: 'NJ Price / Case',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            /*{
                field: 'tfa_nj_fob',
                title: 'NJ FOB Point',
                align: 'center',
                valign: 'middle',
                editable: true,
                sortable: true  
            },*/
            {
                field: 'status',
                title: 'Status',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'user',
                title: 'Added By ',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'tfa_effective_month',
                title: 'Effective Month',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'created',
                title: 'Created At',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },            
        ];

        columns['tfa_nyr'] = [
            {
                formatter: checkBoxFormatter,
            },
            {
                field: 'ID',
                formatter: operateFormatter,
                events: window.operateEvents,
            },
            {
                field: 'item_description',
                title: 'Item Description',
                align: 'left',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'container_size',
                title: 'Container Size',
                align: 'center',
                valign: 'middle',
                sortable: true  
                },
            {
                field: 'bottles_per_case',
                title: 'Bottles per Case',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'vintage',
                title: 'Vintage',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'tfa_nyr_change_type',
                title: 'NYR Change Type',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'tfa_nyr_price_bot',
                title: 'NYR Price / Bottle',
                align: 'center',
                valign: 'middle',
                editable: true,
                sortable: true  
            },
            {
                field: 'tfa_nyr_price_case',
                title: 'NYR Price / Case',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            /*{
                field: 'tfa_nyr_fob',
                title: 'NYR FOB Point',
                align: 'center',
                valign: 'middle',
                editable: true,
                sortable: true  
            },*/
            {
                field: 'status',
                title: 'Status',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'user',
                title: 'Added By ',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'tfa_effective_month',
                title: 'Effective Month',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'created',
                title: 'Created At',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },            
        ];

        columns['tfa_nyw'] = [
            {
                formatter: checkBoxFormatter,
            },
            {
                field: 'ID',
                formatter: operateFormatter,
                events: window.operateEvents,
            },
            {
                field: 'item_description',
                title: 'Item Description',
                align: 'left',
                valign: 'middle',
                sortable: true  
                },
            {
                field: 'container_size',
                title: 'Container Size',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'bottles_per_case',
                title: 'Bottles per Case',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'vintage',
                title: 'Vintage',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'tfa_nyw_change_type',
                title: 'NYW Change Type',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'tfa_nyw_price_bot',
                title: 'NYW Price / Bottle',
                align: 'center',
                valign: 'middle',
                editable: true,
                sortable: true  
            },
            {
                field: 'tfa_nyw_price_case',
                title: 'NYW Price / Case',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'tfa_nyw_fob',
                title: 'NYW FOB Point',
                align: 'center',
                valign: 'middle',
                editable: true,
                sortable: true  
            },
            {
                field: 'tfa_nyw_dist',
                title: 'NYW Distibutor',
                align: 'center',
                valign: 'middle',
                editable: {
                    type: 'select2',
                    escape: true,
                    select2: {
                        tags: distributors.filter(d=>d.State==='NY').map(d=>d.Name.replace( /,/g,'.')),
                        tokenSeparators: [","]
                    },
                    title: 'Distributors',
                },
                sortable: true  
            },
            {
                field: 'status',
                title: 'Status',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'user',
                title: 'Added By ',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'tfa_effective_month',
                title: 'Effective Month',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'created',
                title: 'Created At',
                align: 'center',
                valign: 'middle',
                sortable: true  
            }
        ];

        columns['tfa_os'] = [
            {
                formatter: checkBoxFormatter,
            },
            {
                field: 'ID',
                formatter: operateFormatter,
                events: window.operateEvents,
            },
            {
                //field: 'itemID',
                field: 'item_description',
                title: 'Item Description',
                align: 'left',
                valign: 'middle',
                //editable: {
                //    type: 'select',
                //    source: itemsSource,
                //    title: 'Select the item',
                //},
                sortable: true  
            },
            {
                field: 'container_size',
                title: 'Container Size',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'bottles_per_case',
                title: 'Bottles per Case',
                align: 'center',
                valign: 'middle',
                sortable: true  
                },
            {
                field: 'vintage',
                title: 'Vintage',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'tfa_os_change_type',
                title: 'Change Type',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'tfa_os_price_bottle',
                title: 'Price/Bottle',
                align: 'center',
                valign: 'middle',
                editable: true,
                sortable: true  
            },
            {
                field: 'tfa_os_price_state',
                title: 'Other State',
                align: 'center',
                valign: 'middle',
                editable: {
                    type: 'select',
                    source: sourceStates,
                    title: 'Select state',
                },
                sortable: true  
            },
            {
                field: 'status',
                title: 'Status',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'user',
                title: 'Added By ',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'tfa_effective_month',
                title: 'Effective Month',
                align: 'center',
                valign: 'middle',
                sortable: true  
            },
            {
                field: 'created',
                title: 'Created At',
                align: 'center',
                valign: 'middle',
                sortable: true  
            }
        ];

        <?php 
            $feePrefix = "($300 fee) ";
            $curYear = date("Y");
            $curMonth = date("m");
            $curDay = date("d");
            $curDayOfWeek = date("w");
            $moveDay = $curDayOfWeek==0 ? 1 : ($curDayOfWeek==6 ? 2 : 0); //if cutoff falls on weekend, allow next business day
        ?>

        var feePrefix = '<?=$feePrefix?>';
        var defaultEffectiveMonth = '<?=date("F Y", mktime(0,0,0, $curMonth-1,1,$curYear))?>';
        var selectedEffectiveMonth = defaultEffectiveMonth;

        var setEffectiveMonthOptions = (view_state) => {
            
            $(".tfa_effective_month").text('');

            <?php 
                if ($_SESSION['mhwltdphp_usertype']=='MHW' || $_SESSION['mhwltdphp_usertype']=='ADMIN') { 
                    for($num = 0; $num < 10; $num++){
                        $option = date("F Y", mktime(0,0,0,$num+$curMonth-1,1,$curYear));
            ?>
                    $(".tfa_effective_month").append(`<option><?=$option?></option>`);
            <?php
                    }
                } else { ?>

                    if (view_state=='tfa_nj') {
                        <?php
                            //$shift = 2;
                            $shift = $curDay > 0 + $moveDay ? 2 : 1;
                            for($num = -$opt_effective_month_prior_count; $num < 0; $num++) {
                                $option = date("F Y", mktime(0,0,0,$num + $curMonth + $shift,1,$curYear));
                        ?>
                                $(".tfa_effective_month").append(`<option><?=$feePrefix.$option?></option>`);
                        <?php
                            }

                            $selected = "selected";
                            for($num = 0; $num < $opt_effective_month_forward_count; $num++) {
                                $option = date("F Y", mktime(0,0,0,$num + $curMonth + $shift,1,$curYear));
                        ?>
                                $(".tfa_effective_month").append(`<option <?=$selected?> ><?=$option?></option>`);
                        <?php
                                $selected = "";
                            }
                        ?>
                        defaultEffectiveMonth = '<?=date("F Y", mktime(0, 0, 0, $curMonth + $shift, 1, $curYear))?>';
                    } else if (view_state=='tfa_nyw') {
                        <?php
                            $shift = $curDay > 9 + $moveDay ? 2 : 1;

                            for($num = -$opt_effective_month_prior_count; $num < 0; $num++) {
                                $option = date("F Y", mktime(0,0,0,$num + $curMonth + $shift,1,$curYear));
                        ?>
                                $(".tfa_effective_month").append(`<option><?=$feePrefix.$option?></option>`);
                        <?php
                            }

                            $selected = "selected";
                            for($num = 0; $num < $opt_effective_month_forward_count; $num++) {
                                $option = date("F Y", mktime(0,0,0,$num + $curMonth + $shift,1,$curYear));
                        ?>
                                $(".tfa_effective_month").append(`<option <?=$selected?> ><?=$option?></option>`);
                        <?php
                                $selected = "";
                            }
                        ?>
                        defaultEffectiveMonth = '<?=date("F Y", mktime(0, 0, 0, $curMonth + $shift, 1, $curYear))?>';
                    } else {
                        <?php
                            $shift = $curDay > 19 + $moveDay ? 2 : 1;

                            for($num = -$opt_effective_month_prior_count; $num < 0; $num++) {
                                $option = date("F Y", mktime(0,0,0,$num + $curMonth + $shift,1,$curYear));
                        ?>
                                $(".tfa_effective_month").append(`<option><?=$feePrefix.$option?></option>`);
                        <?php
                            }

                            $selected = "selected";
                            for($num = 0; $num < $opt_effective_month_forward_count; $num++) {
                                $option = date("F Y", mktime(0,0,0,$num + $curMonth + $shift,1,$curYear));
                        ?>
                                $(".tfa_effective_month").append(`<option <?=$selected?> ><?=$option?></option>`);
                        <?php
                                $selected = "";
                            }
                        ?>
                        defaultEffectiveMonth = '<?=date("F Y", mktime(0, 0, 0, $curMonth + $shift, 1, $curYear))?>';
                    }
            <?php
                }
            ?>
        };

        var updateTable = () => {

            var view_type = $('#view_type').val();
            var view_state = $('#view_state').val();
            var client = $('#client_name').val();
            var qry_type = "pricing";


            if ($.inArray( view_state, ['tfa_ma','tfa_ca','tfa_nyw']) != -1 ) {

                const stateAbbr = $('#view_state').val().substr(4,2).toUpperCase();

                $('.distributors-list').text('');
                $('.distributors-list').append(
                    distributors.filter(d => d.State===stateAbbr )
                            .map(d => `<option>${d.Name.replace( /,/g,'.')}`).join('\n')
                );

                $('#btn-set-distributors').show();
            }else{
                $('#btn-set-distributors').hide();
            }

            if (view_state == 'tfa_nj') {
                $('#pp-calendar-m1').show();
                $('#pp-calendar-m10').hide();
                $('#pp-calendar-m20').hide();
                $('#pp-calendar').show();
            } else if (view_state == 'tfa_nyw') {
                $('#pp-calendar-m1').hide();
                $('#pp-calendar-m10').show();
                $('#pp-calendar-m20').hide();
                $('#pp-calendar').show();
            } else if ($.inArray( view_state, ['tfa_ma','tfa_mn','tfa_ca','tfa_ct','tfa_nyr']) != -1 ) {
                $('#pp-calendar-m1').hide();
                $('#pp-calendar-m10').hide();
                $('#pp-calendar-m20').show();
                $('#pp-calendar').show();
            } else {
                $('#pp-calendar').hide();
            }


			$('#priceTable').bootstrapTable('showLoading');
            $('#priceTable').data('editable-url', '/forms/update-value.php?view_state='+(view_type=="Open States" ? "tfa_os" : view_state));

			$.post('query-request.php', {
                qry_type:    qry_type,
                view_type:   view_type,
                view_state:   view_state,
				client:  client
            }, function(dataPF) { 
                //console.log(dataPF);
                var data = [];
                try {
                    var winH = $(window).height();
                    var navH = $(".navbar").height() + $(".before-table-header").height();
                    var trH = $("tr[data-index=0]").first().height()
                    data = JSON.parse(dataPF);
                    var tblH = navH+trH*data.length > (winH-navH)-20 ? (winH-navH)-20 : undefined;
                    //console.log(tblH, trH, data.length, trH * data.length, (winH-navH)-20);
                $('#priceTable').bootstrapTable('destroy').bootstrapTable('showLoading').bootstrapTable({
                    data: data,
                        height: tblH,
                    stickyHeader:true,
                    stickyHeaderOffsetY:60,
                    formatLoadingMessage: function () {
                        return  '<span class="mhwLoading"><img src="css/img/Logo_Chevron_Animated.gif"/> Loading</span>';
                    },
                    //fixedColumns:true,
                    //fixedNumber:7,
                    columns: columns[view_type=='Open States' ? 'tfa_os' : view_state]
                }); 

                } catch (error) {
                    location.reload();
                    //console.log(error);                    
                }

            });

            setEffectiveMonthOptions(view_state);
        };

        var updateViewStates = () => {
            var view_type = $('#view_type').val();
            //console.log(view_type, view_states[view_type]);
            $('#view_state').html("");
            view_states[view_type].forEach(el => {
                if (typeof el == 'object') {
                    $('#view_state').append("<option value='"+el.id+"'  >"+el.name+"</option>");
                } else {
                    $('#view_state').append("<option value='"+el+"'  >"+el+"</option>");
                }
            });
            /*
            view_states.filter((e)=>e.category_id==current_category_id).forEach((e)=>{
                $('#view_state').append("<option value='"+e.id+"'  >"+e.value+"</option>");
            });
            */
            updateTable();
        };

        $('#view_type').on('change',updateViewStates);
        $('#view_state').on('change',updateTable);
		$('#client_name').on('change',updateTable);



        /*======BEGIN comboboxes==========*/
        function cbb_formatter(){ 
            //$('#tfa_brand_name').addClass('combobox_restyle');
            //$('#tfa_brand_name').css( "border-radius", "0px" );
            $('ul.ac_results').css("margin-left","0em");
            $('ul.ac_results').css("margin","0em");
            $('.ac_button').css("height","28px");
            $('.ac_button').css("background","transparent");
            $('.ac_button').css("margin-left","-35px");
            $('.ac_button').css("border","0px");
            $('.ac_subinfo dl').css("margin-left","0em");
            $('.ac_subinfo dl').css("margin","0em");

            $('.tfa_container_size').val('');
            $('.tfa_bottles_per_case').val('');
            $('.tfa_vintage').val('');
            $('.validate-alphanum').val('');
            $('input[type=number]').val('0');
            $('.tfa_effective_month').val(defaultEffectiveMonth);

        }
        //load form fields - as a result of selecting item_desc combobox
        function prefill_item(itemID, effectMonth=null){

            var view_type = $('#view_type').val();
            var view_state = $('#view_state').val();

            var qrytype = "itemByID";
            var client = $('#client_name').val(); //get value of the client combobox

            //console.log(effectMonth, defaultEffectiveMonth);

            $('.tfa_container_size').val('');
            $('.tfa_bottles_per_case').val('');
            $('.tfa_vintage').val('');
            $('.validate-alphanum').val('');
            $('input[type=number]').val('0');
            
            if (!effectMonth) {
                $('.tfa_effective_month').val(defaultEffectiveMonth);
            }

            $.post('query-request.php', {qrytype: qrytype, itemID: itemID, client: client}, function(dataPx){ // ajax request query-request.php, send brand as post variable, return dataX variable, parse into JSON
                var itm = JSON.parse(dataPx);
                $.each( itm, function( key, value ) {
                    $('.tfa_container_size').val(value.container_size);
                    $('.tfa_bottles_per_case').val(value.bottles_per_case);
                    $('.tfa_vintage').val(value.vintage);
                    if (value.stock_uom=='TEST') {
                        $('.discounts-div').hide();
                    } else {
                        $('.discounts-div').show();
                    }
                });
            });
            qrytype = "pricing";

            var params = {
                qry_type: qrytype, 
                itemID: itemID, 
                client: client, 
                view_type: view_type, 
                view_state: view_state
            };

            if (effectMonth) {
                params.effective_month = effectMonth;
            }

            $.post('query-request.php', params, function(dataPx){ 
                var rec = JSON.parse(dataPx);
                if (rec.length == 0) return;
                $.each( rec[0], function( key, value ) {
                    if (key=='tfa_effective_month') {
                        // check tfa_effective_month options, and add if not exist, with ($300 fee)
                        var effectiveMonthOptions = $(".tfa_effective_month:first option").map(function(){
                            return this.value;
                        }).get();
                        //console.log(effectiveMonthOptions);
                        if (effectiveMonthOptions.indexOf(value)>-1) {
                            $('.'+key).val(value);
                        } else if (effectiveMonthOptions.indexOf(feePrefix + value)>-1) {
                            $('.'+key).val(feePrefix + value);
                        } else {
                            $(".tfa_effective_month").prepend('<option>'+ (value.indexOf(feePrefix)===-1 ? feePrefix : '') + value +'</option>');
                            $('.'+key).val((value.indexOf(feePrefix)===-1 ? feePrefix : '') + value);
                        }

                    } else if (key.substr(0,3)=='tfa') {
                        if ($('.'+key).length>0) {
                            $('.'+key).val(value);
        }
                        if ($('#'+key).length>0) {
                            if ($('#'+key).prop("multiple")) {
                                $('#'+key).val(value.split(","));
                            } else {
                                $('#'+key).val(value);
                            }
                        }
                    }
                });
            });
        }
        function isItemValidSelection(inputElement=null){

            var itemDescElement =  $('.tfa_item_desc');
            if (inputElement) {
                itemDescElement =  ($(inputElement).closest('.tfa_item_desc'));
            }

            var json = itemDescElement.attr('sub_info');
            if (json !== undefined) {
                //if additional sub_info elements are added, additional apostrophe replacements need to be added
                var json2 = json.replace( /\'/g, '"' ); 
                var jsonObj = JSON.parse(json2);
                //console.log(jsonObj);

                selectedItemID = jsonObj.id;

                prefill_item(jsonObj.id); //prefill form fields
                $(".item-not-found").remove();
            }
            else{
                $(".item-not-found").remove();
                if (inputElement && inputElement.value.length > 0) {
                    $(".tfa_item_desc").parent().parent().parent().prepend($("<div class='item-not-found'>The item is not found, you can <a href='/forms/productsetup.php'>add a new one here</a></div>" ));
                }
                //$("#itemID").val("");  //clear field for insert
            }
        }

    //initialize product combobox
        function initialize_item(){
            //$('.tfa_item_desc').unbind('change');
            $('.tfa_item_desc').val('');
            $('.tfa_item_desc').parent().find(".ac_button").remove();  //clear elements of previous instance
            $('.tfa_item_desc').parent().find(".ac_result_area").remove();  //clear elements of previous instance
            var view_state = $('#view_state').val();
            var client = $('#client_name').val(); //get value of the client combobox
            var cbbtype = 'item';
            $.post('select-request.php', {
                view_state: view_state,
                cbbtype: cbbtype, 
                client: client
            }, function(dataX){ // ajax request select-request.php, send brand as post variable, return dataX variable, parse into JSON
                //console.log(dataX);
                var data = JSON.parse(dataX);
                //console.log(data);
                $(function() {
                    $('.tfa_item_desc').ajaxComboBox(
                        data,
                        {
                            bind_to: 'tfa_item_desc',
                            sub_info: true,
                            sub_as: {
                            id: 'Item ID',
                            code: 'MHW Item Code',
                            code2: 'Client Item Code',
                            parent: 'Product Code',
                            parent2: 'Product Description',
                            brand: 'Brand'
                            },
                        }
                    ).unbind('tfa_item_desc').bind('tfa_item_desc', function() {
                        var json = $(this).attr('sub_info');
                        //if additional sub_info elements are added, additional apostrophe replacements need to be added
                        var json2 = json.replace( /\'/g, '"' ); 
                        var jsonObj = JSON.parse(json2);
                        //console.log(jsonObj);

                        if(parseInt(jsonObj.id) > 0){
                            //$("#itemID").val(jsonObj.id); //set field for processing update
                            selectedItemID = jsonObj.id;
                            //console.log(selectedItemID);
                            //prefill_item(jsonObj.id);   //prefill form fields, called in isItemValidSelection()
                        }
                        isItemValidSelection();
                    });
                    cbb_formatter(); //apply css and classname changes to combobox elements
                });
            });

            $('.tfa_item_desc').unbind("change").bind("change", function(){
                isItemValidSelection(this);
            });
        }
        initialize_item();

        //client combobox >> brand combobox
        $('#client_name').change(function(){ //if client value changes
            //replace combobox elements with original input
            $('.tfa_item_desc').parent().parent().html("<input type=\"text\" class=\"tfa_item_desc\" name=\"tfa_item_desc\" value=\"\" aria-required=\"true\" title=\"Item Description\" data-dataset-allow-free-responses=\"\" class=\"required validate-alphanum\">");
            
            //re-initialize item combobox
            initialize_item();
            //isItemValidSelection();
            //cbb_formatter(); //apply css and classname changes to combobox elements
        });
        /*======END comboboxes==========*/

        //clear form
        $("#clearform").click(function() {		
            //$('#itemID').val('');
            selectedItemID = '';
            $('#tfa_stock_uom').val('');
            $('#tfa_bottles_per_case').val('');
            $('#tfa_item_code').val('');

            $("#tfa_4-L").html("Item"); //display message for insert

            //replace combobox elements with original input
            $('.tfa_item_desc').parent().parent().html("<input type=\"text\" class=\"tfa_item_desc\" name=\"tfa_item_desc\" value=\"\" aria-required=\"true\" title=\"Item Description\" data-dataset-allow-free-responses=\"\" class=\"required validate-alphanum\">");

            initialize_item();
            //cbb_formatter();

        });
        
        $('#btn-dlg-delete').bind('click',()=>{
            var id = $("#dlg-delete").data('id');
            var view_type = $('#view_type').val();
            var view_state = $('#view_state').val();

			$.post('update-value.php', {
                view_state: (view_type=="Open States" ? "tfa_os" : view_state),
                pk: id,
                name: 'deleted',
                value: 1
            }, function(result) { 
                //console.log(result);
                $("#dlg-delete").modal('hide');    
                updateTable();
            });
        });

        $('#btn-dlg-undelete').bind('click',()=>{
            var id = $("#dlg-undelete").data('id');
            var view_type = $('#view_type').val();
            var view_state = $('#view_state').val();

			$.post('update-value.php', {
                view_state: (view_type=="Open States" ? "tfa_os" : view_state),
                pk: id,
                name: 'deleted',
                value: 0
            }, function(result) { 
                //console.log(result);
                $("#dlg-undelete").modal('hide');    
                updateTable();
            });
        });

        $('#btn-add-new').bind('click',()=>{
            var view_type = $('#view_type').val();
            var view_state = $('#view_state').val();
            //re-initialize item combobox
            initialize_item();
            //isItemValidSelection();
            //cbb_formatter(); //apply css and classname changes to combobox elements
            if ('Open States'==view_type) {
                $("#dlg-edit-tfa_os").modal();
            }else{
                $("#dlg-edit-"+view_state).modal();
            };
        });

        $('#btn-finalize').bind('click',()=>{
            var view_state = $('#view_state').val();
            $.post('/forms/finalize.php', $('#form-main').serialize()+"&view_state="+view_state, (res)=>{
                //console.log(res);
                updateTable();
                $('#dlg-finalize-result-text').html(""+res.result+" item(s) are finalized");
                $('#dlg-finalize-result').modal();
            });
        });

        $('.btn-save').bind('click',(e)=> {

            var elements = $(e.target).parent().parent().find('.required, validate-float, validate-alphanum');
            var validated = true;
            for (var idx=0; idx<elements.length; idx++) {
                if (elements[idx].value=='') {
                    validated = false;
                    elements[idx].classList.add("validation-error");
                } else {
                    elements[idx].classList.remove("validation-error");
                }
            }

            if (!validated) {

                $('.validation-error').bind('change',(e)=> {
                    e.target.classList.remove("validation-error");
                });

            } else {

            var view_type = $('#view_type').val();
            var view_state = $('#view_state').val();

            if ('Open States'==view_type) {
                    $.post('/forms/add-record.php', $('#form-tfa_os').serialize()+"&itemID="+selectedItemID+"&view_state="+view_state, (res)=>{
                    $('.modal').modal('hide');
                    updateTable();
                });
            } else {
                    $.post('/forms/add-record.php', $('#form-'+view_state).serialize()+"&itemID="+selectedItemID+"&view_state="+view_state, (res)=>{
                    $('.modal').modal('hide');
                    updateTable();
                });
            } 
            }
        });

        $('#btn-dlg-discounts-save').bind('click',()=>{
            var view_type = $('#view_type').val();
            var view_state = $('#view_state').val();
            $.post('/forms/update-record.php', $('#form-discounts').serialize()+
                                                    "&row_id="+$("#dlg-discounts").data('id')+
                                                    "&view_state="+view_state, (res)=>{
                $('.modal').modal('hide');
                updateTable();
            });
        });

        $(document).on('editable-save.bs.table', '#priceTable', function (e, field, row, old) {
            updatedValues.push({
                itemID: row.itemID,
                field: field
            });
            updateTable();
        });

        $(document).on('editable-init.bs.table', '#priceTable', function (e, field, row, old) {
            updatedValues.forEach(element => {
                $(".with-item-"+element.itemID).parent().parent().find("a[data-name='"+element.field+"']").parent().css("background-color", "#bfc5da");
           });
           $(".editable-click").each((idx, item)=>{
               var maxDistrLength = 50;
               if (item.text.length > maxDistrLength) {
                    item.text = item.text.substr(0, maxDistrLength)+"...";
               } 
           });
        });

        $(".select_qty_type").on("change", (e)=>{
            $(".select_qty_type").val(e.target.value);
        });

        $("#tfa_nj_mix_match").on("change", (e)=>{
            if ($("#tfa_nj_mix_match").val() != "") {
                $("#tfa_nj_fam_price").val("");
            }
        });
        
        $("#tfa_nj_fam_price").on("change", (e)=>{
            if ($("#tfa_nj_fam_price").val() != "") {
                $("#tfa_nj_mix_match").val("");
            }
        });

        ['ct','mn','ma','nj','nyw','nyr'].forEach(st => {
            $(`#tfa_${st}_price_bot`).change( e => {
                $(`#tfa_${st}_price_case`).val(parseFloat($(`#tfa_${st}_price_bot`).val() * $(`#tfa_${st}_bottles_per_case`).val()).toFixed(2));
            });
        });

        $( "input[type=number]" ).blur(function() {
            this.value = parseFloat(this.value).toFixed(2);
        });            

        $('#btn-set-distributors').click(()=>{
            $('#dlg-set-distributors').modal();
        });

        $('#btn-dlg-set-distributors').click(()=>{
            var view_state = $('#view_state').val();
            if ($('#dlg-distributors-list').val().length) {
                var selectedDistributors = $('#dlg-distributors-list').val().map(d=>d.replace( /,/g,'.')).join(',')
                $.post('/forms/set-distributors.php', $('#form-main').serialize()+"&view_state="+view_state+"&distributors="+selectedDistributors, (res)=>{
                    $('#dlg-set-distributors').modal('hide');
                    if (res.result) {
                        $('#dlg-finalize-result-text').html(""+res.result+" item(s) updated");
                    } else {
                        $('#dlg-finalize-result-text').html("Server error");
                    }
                    $('#dlg-finalize-result').modal();
                    updateTable();
                });
            }else{
                $('#dlg-finalize-result-text').html("Please select distributors!");
                $('#dlg-finalize-result').modal();
            }
        });

        $(".discount_quan").on("change", (e) => {
            var cur_order = Number($(e.target).data('order'));
            
            var min_value =  $(".discount_quan_"+(cur_order)).attr('min');
            var max_value =  $(".discount_quan_"+(cur_order)).attr('max');

            if (min_value && Number(e.target.value) && Number(e.target.value) < min_value) {
                $(".discount_quan_"+(cur_order)).val(min_value);
            }
            if (max_value && Number(e.target.value) && Number(e.target.value) > max_value) {
                $(".discount_quan_"+(cur_order)).val(max_value);
            }

            $(".discount_quan_"+(cur_order-1)).attr('max', Number(e.target.value)-1);
            $(".discount_quan_"+(cur_order+1)).attr('min', Number(e.target.value)+1);

            //$(".discount_quan_"+(cur_order+1)).prop('enabled',true);
        });

        $(".tfa_effective_month").on("change", (e)=>{
            //console.log(e);
            selectedEffectiveMonth = e.target.value;
            prefill_item(selectedItemID, selectedEffectiveMonth);
        });
        updateViewStates();

    };// main init

    window.operateEvents = {
        'click .btn-discounts': function (e, value, row, index) {
            //console.log(e, value, row, index);
            var qry_type = "pricing";
            var view_type = $('#view_type').val();
            var view_state = $('#view_state').val();
			var client = $('#client_name').val();

			$.post('query-request.php', {
                qry_type:    qry_type,
                view_type:   view_type,
                view_state:   view_state,
				client: client,
                row_id: value
            }, function(dataPF) { 
                var data = [];
                try {
                    data = JSON.parse(dataPF);
                    //console.log(data);
                } catch (error) {
                    //console.log(error);                    
                }
                for (var dis_id=1;dis_id<=10;dis_id++) {
                    var quan = data[0][""+view_state+"_qty"+dis_id+"_quan"];
                    quan = quan=='.00' ? '' : quan;
                    var amt = data[0][""+view_state+"_qty"+dis_id+"_amt"];
                    amt = amt=='.00' ? '' : amt;
                    $("#qty"+dis_id+"_quan").val(quan);
                    $("#qty"+dis_id+"_on").val(data[0][""+view_state+"_qty"+dis_id+"_on"]);
                    $("#qty"+dis_id+"_type").val(data[0][""+view_state+"_qty"+dis_id+"_type"]);
                    $("#qty"+dis_id+"_amt").val(amt);

                    if (quan) {
                        $(".discount_quan_"+(dis_id-1)).attr('max', Number(quan)-1);
                        $(".discount_quan_"+(dis_id+1)).attr('min', Number(quan)+1);
                    }
                };
                $("#dlg-discounts").data('id', value);
                $("#dlg-discounts").modal();
            });
        },
        'click .btn-delete': function (e, value, row, index) {
            $("#dlg-delete").data('id', value);
            $("#dlg-delete").modal();
        },
        'click .btn-undelete': function (e, value, row, index) {
            $("#dlg-undelete").data('id', value);
            $("#dlg-undelete").modal();
        }
    }

</script>

</body>
</html>