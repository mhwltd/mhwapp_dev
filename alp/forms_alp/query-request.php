<?php
include('settings.php');
include('functions.php');
error_reporting(E_ALL); //displays an error

session_start();
if(!isset($_SESSION['mhwltdphp_user'])){
	die( "Not authenticated !" );  
}
$isAdmin = in_array($_SESSION['mhwltdphp_usertype'], ['ADMIN','SUPERUSER']);
include("dbconnect.php");

//Establishes the connection
$conn = sqlsrv_connect($serverName, $connectionOptions);
if( $conn === false) {
		die( print_r( sqlsrv_errors(), true));
}

if($_SESSION['mhwltdphp_user']!='' && $_POST['qrytype']!=''){
	$trksql= "INSERT INTO [mhw_app_workflow] VALUES ('".$_POST['qrytype']."', 0, GETDATE(), GETDATE(), '".$_SESSION['mhwltdphp_user']."', 1, 0)";
	$trkResults= sqlsrv_query($conn, $trksql);
}

if($_POST['qrytype']=='prodByID')
{
	if(isset($_POST['prodID']) && $_POST['prodID']!=='' && isset($_POST['client']) && $_POST['client']!=='')
	{
		//$tsql= "SELECT p.[product_id],p.[client_name],p.[brand_name],p.[product_desc],p.[product_mhw_code],p.[product_mhw_code_search],p.[TTB_ID] as [TTB],p.[federal_type],p.[compliance_type],CASE WHEN p.[product_class] = 'Foreign' THEN 'Imported' ELSE p.[product_class] END AS [product_class],p.[mktg_prod_type],p.[bev_type],p.[fanciful],p.[country],p.[appellation],p.[lot_item],p.[bottle_material],p.[alcohol_pct],s.[supplier_id],s.[supplier_name],s.[supplier_contact],s.[supplier_fda_number],s.[tax_reduction_allocation],s.[supplier_address_1],s.[supplier_address_2],s.[supplier_address_3],s.[supplier_city],s.[supplier_state],s.[supplier_country],s.[supplier_zip],s.[supplier_phone],s.[supplier_email],s.[federal_basic_permit],(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[requirement_met] = 1 AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount' FROM [dbo].[mhw_app_prod] p left outer join [mhw_app_prod_supplier] s on s.[supplier_id] = p.[supplier_id] WHERE p.[product_id] = ".$_POST['prodID']." and p.[client_name] IN ('".$_POST['client']."') AND p.[active] = 1 and p.[deleted] = 0";
		
		$tsql= "EXEC [dbo].[usp_query_request_alp] @querytype = '".$_POST['qrytype']."', @client = '".$_POST['client']."', @prodID = ".$_POST['prodID'].", @suppID = 0";
		//echo $tsql;
	}
}
if($_POST['qrytype']=='suppByID')
{
	if(isset($_POST['suppID']) && $_POST['suppID']!=='')
	{
		$tsql= "SELECT s.[supplier_name],s.[supplier_contact],s.[supplier_fda_number],s.[tax_reduction_allocation],s.[supplier_address_1],s.[supplier_address_2],s.[supplier_address_3],s.[supplier_city],s.[supplier_state],s.[supplier_country],s.[supplier_zip],s.[supplier_phone],s.[supplier_email],s.[federal_basic_permit] FROM [mhw_app_prod_supplier] s WHERE s.[supplier_id] = ".$_POST['suppID']." AND s.[active] = 1 and s.[deleted] = 0";
		//echo $tsql;
	}
}
if($_POST['qrytype']=='VPdefault'){
	if(isset($_POST['clientlist']) && $_POST['clientlist']!=='')
	{
		$tsql= "EXEC [dbo].[usp_query_request_alp] @querytype = '".$_POST['qrytype']."', @client = ".$_POST['clientlist'].", @prodID = 0, @suppID = 0";

		//$tsql= "SELECT p.[product_id],p.[client_code],p.[client_name],p.[brand_name],p.[product_desc],p.[product_mhw_code],p.[product_mhw_code_search],p.[TTB_ID],p.[federal_type],p.[compliance_type],p.[product_class],p.[mktg_prod_type],p.[bev_type],p.[fanciful],p.[country],p.[appellation],p.[lot_item],p.[bottle_material],p.[alcohol_pct],p.[create_date],s.[supplier_id],s.[supplier_name],s.[supplier_contact],s.[supplier_fda_number],s.[tax_reduction_allocation],s.[supplier_address_1],s.[supplier_address_2],s.[supplier_address_3],s.[supplier_city],s.[supplier_state],s.[supplier_country]  ,s.[supplier_zip]  ,s.[supplier_phone] ,s.[supplier_email], s.[federal_basic_permit] ,s.[create_via] ,s.[create_date] ,s.[edit_date] ,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_item] i WHERE i.[product_id] = p.[product_id] AND i.[active] = 1 AND i.[deleted] = 0) as 'itemcount' ,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount' FROM [dbo].[mhw_app_prod] p WITH (NOLOCK) LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] s WITH (NOLOCK) on s.[supplier_id] = p.[supplier_id] WHERE p.[client_name] IN (".$_POST['clientlist'].") AND p.[active] = 1 AND p.[deleted] = 0 /* AND s.[active] = 1 AND s.[deleted] = 0 */ ORDER BY p.[create_date] desc";
	}
}
if($_POST['qrytype']=='VPitems'){
	if(isset($_POST['clientlist']) && $_POST['clientlist']!=='')
	{
		$tsql= "EXEC [dbo].[usp_query_request_alp] @querytype = '".$_POST['qrytype']."', @client = ".$_POST['clientlist'].", @prodID = 0, @suppID = 0";
		
		//$tsql= "SELECT i.[item_id],i.[item_client_code],i.[item_mhw_code],i.[item_description],i.[container_type],i.[container_size],i.[stock_uom],i.[bottles_per_case],i.[upc],i.[scc],i.[vintage],i.[various_vintages],i.[height],i.[length],i.[width],i.[weight],i.[item_status],i.[create_via],i.[create_date],i.[edit_date],p.[product_id],p.[client_code],p.[client_name],p.[brand_name],p.[product_desc],p.[product_mhw_code] FROM [dbo].[mhw_app_prod_item] i WITH (NOLOCK) left outer join [dbo].[mhw_app_prod] p WITH (NOLOCK) on p.[product_id] = i.[product_id] WHERE p.[client_name] IN (".$_POST['clientlist'].") AND p.[active] = 1 AND p.[deleted] = 0 AND i.[active] = 1 AND i.[deleted] = 0 ORDER BY p.[product_id], i.[create_date] desc";
	}
}
if($_POST['qrytype']=='VPproditems'){
	if(isset($_POST['prodID']) && $_POST['prodID']!=='')
	{
		$tsql= "EXEC [dbo].[usp_query_request_alp] @querytype = '".$_POST['qrytype']."', @client = '', @prodID = ".$_POST['prodID'].", @suppID = 0";

		//$tsql= "SELECT i.[item_id],i.[item_client_code],i.[item_mhw_code],i.[item_description]	      ,i.[container_type],i.[container_size],i.[stock_uom],i.[bottles_per_case],i.[upc],i.[scc],i.[vintage],i.[various_vintages],i.[height],i.[length],i.[width],i.[weight],i.[item_status],i.[create_via],i.[create_date],i.[edit_date],p.[product_id],p.[client_code],p.[client_name],p.[brand_name],p.[product_desc],p.[product_mhw_code] FROM [dbo].[mhw_app_prod_item] i WITH (NOLOCK) left outer join [dbo].[mhw_app_prod] p WITH (NOLOCK) on p.[product_id] = i.[product_id] WHERE i.[product_id] = '".$_POST['prodID']."' AND p.[active] = 1 AND p.[deleted] = 0 AND i.[active] = 1 AND i.[deleted] = 0 ORDER BY p.[product_id], i.[create_date] desc";
	}
}
if($_POST['qrytype']=='VPunfinalized'){
	if(isset($_POST['clientlist']) && $_POST['clientlist']!=='')
	{
		
		$tsql= "EXEC [dbo].[usp_query_request_alp] @querytype = '".$_POST['qrytype']."', @client = ".$_POST['clientlist'].", @prodID = 0, @suppID = 0";

		//$tsql= "SELECT p.[product_id],p.[client_code],p.[client_name],p.[brand_name],p.[product_desc],p.[product_mhw_code],p.[product_mhw_code_search],p.[TTB_ID],p.[federal_type],p.[compliance_type],p.[product_class],p.[mktg_prod_type],p.[bev_type],p.[fanciful],p.[country],p.[appellation],p.[lot_item],p.[bottle_material],p.[alcohol_pct],p.[create_date],s.[supplier_id],s.[supplier_name],s.[supplier_contact],s.[supplier_fda_number],s.[tax_reduction_allocation],s.[supplier_address_1],s.[supplier_address_2],s.[supplier_address_3],s.[supplier_city],s.[supplier_state],s.[supplier_country]  ,s.[supplier_zip]  ,s.[supplier_phone] ,s.[supplier_email] ,s.[federal_basic_permit] ,s.[create_via] ,s.[create_date] ,s.[edit_date] ,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_item] i WHERE i.[product_id] = p.[product_id] AND i.[active] = 1 AND i.[deleted] = 0) as 'itemcount' ,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount' FROM [dbo].[mhw_app_prod] p WITH (NOLOCK) LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] s WITH (NOLOCK) on s.[supplier_id] = p.[supplier_id] WHERE p.[client_name] IN (".$_POST['clientlist'].") AND p.[finalized] = 0 AND p.[processed] = 0 AND p.[active] = 1 AND p.[deleted] = 0 AND s.[active] = 1 AND s.[deleted] = 0 ORDER BY p.[create_date] desc";
	}
}
if($_POST['qrytype']=='VPpending'){
	if(isset($_POST['clientlist']) && $_POST['clientlist']!=='')
	{
		$tsql= "EXEC [dbo].[usp_query_request_alp] @querytype = '".$_POST['qrytype']."', @client = ".$_POST['clientlist'].", @prodID = 0, @suppID = 0";
		
		//$tsql= "SELECT p.[product_id],p.[client_code],p.[client_name],p.[brand_name],p.[product_desc],p.[product_mhw_code],p.[product_mhw_code_search],p.[TTB_ID],p.[federal_type],p.[compliance_type],p.[product_class],p.[mktg_prod_type],p.[bev_type],p.[fanciful],p.[country],p.[appellation],p.[lot_item],p.[bottle_material],p.[alcohol_pct],p.[create_date],s.[supplier_id],s.[supplier_name],s.[supplier_contact],s.[supplier_fda_number],s.[tax_reduction_allocation],s.[supplier_address_1],s.[supplier_address_2],s.[supplier_address_3],s.[supplier_city],s.[supplier_state],s.[supplier_country]  ,s.[supplier_zip]  ,s.[supplier_phone] ,s.[supplier_email] ,s.[federal_basic_permit] ,s.[create_via] ,s.[create_date] ,s.[edit_date] ,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_item] i WHERE i.[product_id] = p.[product_id] AND i.[active] = 1 AND i.[deleted] = 0) as 'itemcount' ,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount' FROM [dbo].[mhw_app_prod] p WITH (NOLOCK) LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] s WITH (NOLOCK) on s.[supplier_id] = p.[supplier_id] WHERE p.[client_name] IN (".$_POST['clientlist'].") AND p.[finalized] = 1 AND p.[processed] = 0 AND p.[active] = 1 AND p.[deleted] = 0 /* AND s.[active] = 1 AND s.[deleted] = 0 */ ORDER BY p.[create_date] desc";
	}
}
if($_POST['qrytype']=='VPfinalized'){
	if(isset($_POST['clientlist']) && $_POST['clientlist']!=='')
	{
		$tsql= "EXEC [dbo].[usp_query_request_alp] @querytype = '".$_POST['qrytype']."', @client = ".$_POST['clientlist'].", @prodID = 0, @suppID = 0";
		
		//$tsql= "SELECT p.[product_id],p.[client_code],p.[client_name],p.[brand_name],p.[product_desc],p.[product_mhw_code],p.[product_mhw_code_search],p.[TTB_ID],p.[federal_type],p.[compliance_type],p.[product_class],p.[mktg_prod_type],p.[bev_type],p.[fanciful],p.[country],p.[appellation],p.[lot_item],p.[bottle_material],p.[alcohol_pct],p.[create_date],s.[supplier_id],s.[supplier_name],s.[supplier_contact],s.[supplier_fda_number],s.[tax_reduction_allocation],s.[supplier_address_1],s.[supplier_address_2],s.[supplier_address_3],s.[supplier_city],s.[supplier_state],s.[supplier_country]  ,s.[supplier_zip]  ,s.[supplier_phone] ,s.[supplier_email] ,s.[federal_basic_permit] ,s.[create_via] ,s.[create_date] ,s.[edit_date] ,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_item] i WHERE i.[product_id] = p.[product_id] AND i.[active] = 1 AND i.[deleted] = 0) as 'itemcount' ,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount' FROM [dbo].[mhw_app_prod] p WITH (NOLOCK) LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] s WITH (NOLOCK) on s.[supplier_id] = p.[supplier_id] WHERE p.[client_name] IN (".$_POST['clientlist'].") AND p.[finalized] = 1 AND p.[processed] = 1 AND p.[active] = 1 AND p.[deleted] = 0 /* AND s.[active] = 1 AND s.[deleted] = 0 */ ORDER BY p.[create_date] desc";
	}
}
if($_POST['qrytype']=='STAT_COUNT_unfinalized'){
	if(isset($_POST['clientlist']) && $_POST['clientlist']!=='')
	{
		$tsql= "SELECT COUNT(*) as 'unfinalizedCount' FROM [dbo].[mhw_app_prod] p WITH (NOLOCK) LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] s WITH (NOLOCK) on s.[supplier_id] = p.[supplier_id] WHERE p.[client_code] IN ('".str_replace(";","','",$_POST['clientlist'])."') AND p.[finalized] = 0 AND p.[processed] = 0 AND p.[active] = 1 AND p.[deleted] = 0 /* AND s.[active] = 1 AND s.[deleted] = 0 */";
	}
}
if($_POST['qrytype']=='processFinalize'){
	
	if(isset($_POST['prodlist']) && $_POST['prodlist']!=='' && $_POST['prodlist']!=='[]')
	{
		$ProdIDString =  str_replace("]","",str_replace("[","",$_POST['prodlist']));		
		$tsql= "UPDATE [dbo].[mhw_app_prod] SET [finalized] = 1 WHERE [product_id] IN (".$ProdIDString.")";
		$getResults= sqlsrv_query($conn, $tsql);
	}
	
	$admin_confirm='';
	if(isset($_POST['des_chng_yes']))
	{
		$admin_confirm=$_POST['des_chng_yes'];
	}
	
	
	$UncheckedItemIDString="";
	
	if(isset($_POST['uncheckedItemIds']) && $_POST['uncheckedItemIds']!=='' && $_POST['uncheckedItemIds']!=='[]')
	{
		
		$UncheckedItemIDString =  str_replace("]","",str_replace("[","",$_POST['uncheckedItemIds']));
        $UncheckedItemIDString =  str_replace('"','',$UncheckedItemIDString);
        $UncheckedItemIDString="i.item_id not in (".$UncheckedItemIDString.") and ";		
    }
	
	$item_tsql=generate_draft_item_query($item_validation_matrix,$_SESSION['mhwltdphp_user_clients_codes'],$UncheckedItemIDString);
	
	$getResults_validItemList = sqlsrv_query($conn, $item_tsql);

	if ($getResults_validItemList == FALSE)
		echo (sqlsrv_errors());

    $all_finalized_ids="";
	while ($row_validItemList = sqlsrv_fetch_array($getResults_validItemList, SQLSRV_FETCH_ASSOC)) {
		$item_federal_type=$row_validItemList['federal_type'];
		$item_vintage=$row_validItemList['vintage'];
		$item_id=$row_validItemList['item_id'];
		$item_old_des=$row_validItemList['item_description'];
		$valid_item=0;
		if($item_federal_type=='Wine' && $item_vintage=='')
		{
		}
		else 
		{
			$valid_item=1;
			$all_finalized_ids=$all_finalized_ids.$item_id.",";
		}
		/* recalculating item description */
		if($valid_item==1 && $item_description_calculation_at_item_finalized_time==1 && $admin_confirm=='confirm_yes')
		{
			$new_item_des=generate_item_desc($row_validItemList['federal_type'],$row_validItemList['container_type'],$row_validItemList['container_size'],$row_validItemList['stock_uom'],$row_validItemList['outer_shipper'],$row_validItemList['product_desc'],$row_validItemList['vintage'],$row_validItemList['bottles_per_case']);
		    
			
			if($new_item_des!=$item_old_des)
			{
			$tsql ="UPDATE [dbo].[mhw_app_prod_item] SET [item_description] = ? WHERE [item_id] = ?; SELECT SCOPE_IDENTITY();";
			$stmt = sqlsrv_prepare($conn, $tsql, array($new_item_des, $item_id));
					    
				if( $stmt === false )  
				{  
					echo "Statement could not be prepared.\n";  
					die( print_r( sqlsrv_errors(), true));  
				}  
				
				if( sqlsrv_execute($stmt) === false )  
				{  
					echo "Statement could not be executed.\n";  
					die( print_r( sqlsrv_errors(), true));  
				}			
			}			
		}
	}

	if($all_finalized_ids!="")
	{
		$all_finalized_ids=substr($all_finalized_ids, 0, -1);
		$tsql= "UPDATE [dbo].[mhw_app_prod_item] SET [finalized] = 1 WHERE [item_id] IN (".$all_finalized_ids.")";
		$getResults= sqlsrv_query($conn, $tsql);
	}
		
}
if($_POST['qrytype']=='alertFinalize'){
	if(isset($_POST['prodlist']) && $_POST['prodlist']!=='')
	{
		$ProdIDString =  str_replace("]","",str_replace("[","",$_POST['prodlist']));
		$ProdCodeString =  str_replace("]","",str_replace("[","",$_POST['codelist']));
		
		$tsql= "EXEC [dbo].[usp_product_finalize_alp] '".$_POST['qrytype']."','".$ProdIDString."',".$_POST['clientlist'].",'".$ProdCodeString."','".$siteroot."','".$_SESSION['mhwltdphp_user']."','".$_SESSION['mhwltdphp_useremail']."'";
		//echo  $tsql;
	}
}
if($_POST['qrytype']=='itemByID')
	{
	if(isset($_POST['itemID']) && $_POST['itemID']!=='')
	{
		$itemID = intval($_POST['itemID']);
		$tsql= "
		SELECT TOP 1 [item_id]
			,[product_id]
			,[container_type]
			,[container_size]
			,[stock_uom]
			,[bottles_per_case]
			,[upc]
			,[scc]
			,[vintage]
			,[various_vintages]
			,[item_status]
			,[active]
			,[deleted]
			,[height]
			,[length]
			,[width]
			,[weight]
		FROM [dbo].[mhw_app_prod_item]
		WHERE [item_id] = $itemID";
	}
}
if ($_POST['qry_type']=='pricing'){
	if(isset($_POST['view_type']) && $_POST['view_type']!=='' && isset($_POST['view_state']) && $_POST['view_state']!=='' && isset($_POST['client']) && $_POST['client']!=='')
	{
		$limitRecords = 1000; // for every item
		//$whereItem = "";
		$whereArr = [];
		//$whereMonth = "";
		if(isset($_POST['row_id']) && $_POST['row_id']!=='') {
			$whereArr[] = "ID = ".intval($_POST['row_id']);
		}
		if(isset($_POST['itemID']) && $_POST['itemID']!=='') {
			$whereArr[] = "itemID = ".intval($_POST['itemID']);
			//$whereItem = "WHERE itemID = ".intval($_POST['itemID']);
			$limitRecords = 1;
		}
		if (isset($_POST['effective_month'])) {
			$effective_month = str_replace("($300 fee) ","",addslashes($_POST['effective_month']));
			$whereArr[] = "tfa_effective_month LIKE '$effective_month'";
			//$whereMonth = "WHERE tfa_effective_month LIKE '$effective_month' ";
		}
		if ($_POST['view_type']=='Open States' || $_POST['view_state']=='tfa_os') {
			$tableName = 'tfa_os';
			//$state = str_replace("'","\'",$_REQUEST['view_state']);
			$state = addslashes($_REQUEST['view_state']);
			if ($state == "Deleted" || $_POST['view_type']=='Deleted Items') {
				$whereArr[] = "l.deleted = 1";
			} else {
				$whereArr[] = "ISNULL(l.deleted, 0) <> 1";
			}
			if ($state != "All" && $state != "Deleted" && $state != "tfa_os") {
				$whereArr[] = "tfa_os_price_state = '".addslashes($state)."'";
			}
			$where = implode($whereArr, " AND ");
			$tsql = "SELECT l.*,  convert(varchar, l.created_at, 120) AS created,
							i.item_description, i.container_size, i.bottles_per_case, i.vintage
						FROM [dbo].[$tableName] AS l
						LEFT JOIN mhw_app_prod_item i
						ON i.item_id = l.itemID
						LEFT JOIN [dbo].[mhw_app_prod] p WITH (NOLOCK) 
						ON p.[product_id] = i.[product_id]
						WHERE l.ID IN (
							SELECT TOP $limitRecords ID FROM (
								SELECT itemID, tfa_effective_month, tfa_os_price_state, MAX(ID) AS ID
								FROM [dbo].[$tableName] AS l
								WHERE $where
								GROUP BY itemID, tfa_effective_month, tfa_os_price_state
							) subtbl
							ORDER BY ID DESC
						)
						AND p.[client_code] = '".addslashes($_POST['client'])."'
						AND $where
						ORDER BY i.item_description, tfa_os_price_state DESC
						";
		}else{

			$tableName = $_POST['view_state'];
			//$where = "AND l.".$tableName."_change_type <> 'Delete' ";
			if ($_POST['view_type']=='Deleted Items' ) {
				$whereArr[] = "l.deleted = 1";
			} else{
				$whereArr[] = "ISNULL(l.deleted, 0) <> 1 ";
	}
			$where = implode($whereArr, " AND ");
			$tsql = "SELECT *,  convert(varchar, created_at, 120) AS created, 
							i.item_description, i.container_size, i.bottles_per_case, i.vintage
						FROM [dbo].[$tableName] AS l
						LEFT JOIN mhw_app_prod_item i
						ON i.item_id = l.itemID
						LEFT JOIN [dbo].[mhw_app_prod] p WITH (NOLOCK) 
						ON p.[product_id] = i.[product_id]
						WHERE l.ID IN (
							SELECT TOP $limitRecords ID FROM (
								SELECT itemID, tfa_effective_month, MAX(ID) AS ID
								FROM [dbo].[$tableName] AS l
								WHERE $where
								GROUP BY itemID, tfa_effective_month
							) subtbl
							ORDER BY ID DESC
						)
						AND p.[client_code] = '".$_POST['client']."'
						AND $where
						ORDER BY i.item_description DESC
						";
		};
	}
	//echo( $tsql);exit;
}

/*
if($_GET['qrytype']=='prodByID')
{
	//if(isset($_POST['prodID']) && $_POST['prodID']!=='' && isset($_POST['client']) && $_POST['client']!=='')
	//{
	 $tsql= "SELECT [product_id],[client_name],[brand_name],[product_desc],[product_mhw_code],[TTB_ID],[federal_type],[compliance_type],[product_class],[mktg_prod_type],[bev_type],[fanciful],[country],[appellation],[lot_item],[bottle_material],[alcohol_pct] FROM [dbo].[mhw_app_prod] WHERE [product_id] = ".$_GET['prodID']." AND active = 1 and deleted = 0";

	//}
}
*/
if ($_POST['qry_type']=='distributors-list') {
	
	if (empty($_POST['state_abbrev'])) {
		echo("Wrong parameters !"); exit;
	}

	$state_abbrev = addslashes($_POST['state_abbrev']);

    $tsql = "SELECT [DistKey], [Name] FROM [dbo].[sc_distributor-list]
			WHERE [State] = '$state_abbrev'
			ORDER BY [Name]
	";
	//echo $tsql;exit;
	//$stmt = sqlsrv_prepare( $conn, $tsql, $params);
}

$product_id = intval($_POST['product_id']);
$item_id = intval($_POST['item_id']);
$supplier_id = intval($_POST['supplier_id']);

if ($_POST['qry_type']=='productArchive') {

	$where = "";
	$whereArr = array();
	
	if ($product_id > 0) {
		$whereArr[] = "a.product_id = ".$product_id;
	}

	if ($item_id > 0) {
		//$whereArr[] = "i.item_id = ".$item_id;
		$whereArr[] = "1=0";
	}

	if ($supplier_id > 0) {
		//$whereArr[] = "ps.supplier_id = ".$supplier_id;
		$whereArr[] = "1=0";
	} 

	if (count($whereArr)>0) {
		$where = "WHERE ".implode(" AND ", $whereArr);
	}

    $tsql = "SELECT DISTINCT a.[product_id]
				,p.*
				,a.[field]
				,a.[previous_value]
				,convert(varchar, DATEADD(HOUR,-4,a.[edit_date]), 120) as edit_date
				,a.[edit_user]
			FROM [dbo].[mhw_app_prod_archive] a
			LEFT JOIN [dbo].[mhw_app_prod] p
			ON p.product_id = a.product_id
			LEFT OUTER JOIN [dbo].[mhw_app_prod_item] i 
			ON i.product_id = p.product_id 
			LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] ps 
			ON ps.product_id = a.product_id 
			$where
			ORDER BY convert(varchar, DATEADD(HOUR,-4,a.[edit_date]), 120) DESC
	";
	//echo $tsql;exit;
}

if ($_POST['qry_type']=='itemArchive') {

	$where = "";
	$whereArr = array();
	
	if ($product_id > 0) {
		// $whereArr[] = "i.product_id = ".$product_id;
		// if calling item then show only items
		$whereArr[] = "1=0";
	}

	if ($item_id > 0) {
		$whereArr[] = "a.item_id = ".$item_id;
	}

	if ($supplier_id > 0) {
		//$whereArr[] = "ps.supplier_id = ".$supplier_id;
		$whereArr[] = "1=0";
	} 

	if (count($whereArr)>0) {
		$where = "WHERE ".implode(" AND ", $whereArr);
	}

    $tsql = "SELECT a.[item_id]
				,i.*
				,a.[field]
				,a.[previous_value]
				,convert(varchar, a.[edit_date], 120) as edit_date
				,a.[edit_user]
			FROM [dbo].[mhw_app_prod_item_archive] a
			LEFT JOIN [dbo].[mhw_app_prod_item] i
			ON i.item_id = a.item_id
			LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] ps
			ON ps.product_id = i.product_id
			$where
			ORDER BY a.[edit_date] DESC
	";
	//echo $tsql;exit;
}

if ($_POST['qry_type']=='supplierArchive') {

	$where = "";
	$whereArr = array();

	if ($supplier_id > 0) {
		$whereArr[] = "a.supplier_id = ".$supplier_id;
	} 

	if ($product_id > 0) {
		$whereArr[] = "ps.product_id = ".$product_id;
	}

	if ($item_id > 0) {
		$whereArr[] = "ip.item_id = ".$item_id;
	}

	if (count($whereArr)>0) {
		$where = "WHERE ".implode(" AND ", $whereArr);
	}

    $tsql = "SELECT a.[supplier_id]
				,s.[supplier_name]
				,a.[field]
				,a.[previous_value]
				,convert(varchar, a.[edit_date], 120) as edit_date
				,a.[edit_user]
			FROM [dbo].[mhw_app_prod_supplier_archive] a
			LEFT JOIN [dbo].[mhw_suppliers] s
			ON s.id = a.supplier_id
			LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] ps
			ON ps.supplier_id = a.supplier_id
			LEFT OUTER JOIN [dbo].[mhw_app_prod_item] ip
			ON ps.product_id = ip.product_id
			$where
			GROUP BY a.[edit_date]
				,a.[supplier_id]
				,s.[supplier_name]
				,a.[field]
				,a.[previous_value]
				,a.[edit_user]	
			ORDER BY a.[edit_date] DESC
	";
	//echo $tsql;exit;
}

if ($_POST['qry_type']=='workflow') {

	$whereArray = array();
	if ($product_id) {
		$whereArray[] = "workflow_type IN ('product_delete','product','product edit','product_image') AND record_id = $product_id";
	};
	if ($item_id) {
		$whereArray[] = "workflow_type IN ('product_item','product_item edit','item_delete') AND record_id = $item_id";
	};
	if ($supplier_id) {
		$whereArray[] = "workflow_type IN ('product_supplier','product_supplier edit','supplier_delete') AND record_id = $supplier_id";
	};

	$where = count($whereArray)>0 ? "WHERE ".implode(" AND ", $whereArray) : "";

    $tsql = "SELECT [workflow_type]
				,[record_id]
				,[created_date]
				,convert(varchar, [edit_date], 120) as edit_date
				,[username]
			FROM [dbo].[mhw_app_workflow]
			$where
			ORDER BY [edit_date] DESC
	";
}



/* ********************* added by Jobaidur :start ************************ */

if($_POST['qrytype']=='getItemDetails'){
	if(isset($_POST['prodID']) && $_POST['prodID']!=='')
	{		
		$tsql= "SELECT i.* FROM [dbo].[mhw_app_prod_item] i WITH (NOLOCK) left outer join [dbo].[mhw_app_prod] p WITH (NOLOCK) on p.[product_id] = i.[product_id]  WHERE i.[product_id] = '".$_POST['prodID']."' AND p.[active] = 1 AND p.[deleted] = 0  AND i.[active] = 1 AND i.[deleted] = 0 order by i.[create_date] desc";
	}
}

if($_POST['qrytype']=='getFileDetails'){
	if(isset($_POST['prodID']) && $_POST['prodID']!=='')
	{		
        $implode_arr="'".join("','",$limitedext)."'";	
		$tsql= "SELECT i4.*,(select count(*) as valid_file_count from [dbo].[mhw_app_prod_image] i WHERE i.[image_id] = i4.[image_id] and i.requirement_met=1 and reverse(left(reverse(i.image_name), charindex('.', reverse(i.image_name)))) in ($implode_arr)) as valid_file_ext_count FROM [dbo].[mhw_app_prod_image] i4 WITH (NOLOCK) left outer join [dbo].[mhw_app_prod] p WITH (NOLOCK) on p.[product_id] = i4.[product_id]  WHERE i4.[product_id] = '".$_POST['prodID']."' AND i4.[active] = 1 AND i4.[deleted] = 0";
	}
}

if($_POST['qrytype']=='getTtbIdInfo'){
	if(isset($_POST['prodID']) && $_POST['prodID']!=='')
	{		
		$tsql= "select p.[TTB_ID],p.[PRODUCT_ID],(select count(*) from [sc_cola_federal_sts] f where f.[cola] =SUBSTRING(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(p.TTB_ID,'-',''),'a',''),'b',''),'A',''),'B',''),0,15) and f.[status] = 'Approved') as is_valid_cola,case when (SELECT len(SUBSTRING(p2.[TTB_ID],1,14)) FROM mhw_app_prod p2 WHERE p2.[product_id]=p.[product_id] and SUBSTRING(p2.[TTB_ID],1,14) not like '%[^0-9]%' and (p2.TTB_ID != '' and p2.TTB_ID is not null))=14 then 1 else 0 end as is_valid_min_len from mhw_app_prod p where p.[product_id] = '".$_POST['prodID']."'";
	}
}


if ($_POST['qry_type']=='blr-status') {

	/*
	$tsql = "SELECT s.[blr_state_abbrev], s.[blr_state_name], s.[blr_state_note],
				p.[product_id], p.[product_desc],
				p.[brand_name], p.[product_mhw_code], p.[product_desc],
				p.[TTB_ID], p.[federal_type],
				b.[blr_id], b.[client_name],
				b.[blr_contact], b.[blr_email], b.[blr_phone],
				b.[blr_reg_type], b.[blr_reg_sub_type], b.[blr_phone],
				c.[Status]
			FROM [dbo].[mhw_app_blr] b
			LEFT JOIN [dbo].[mhw_app_blr_states] s
				ON s.[blr_id] = b.[blr_id]
			LEFT JOIN [dbo].[mhw_app_prod] p
				ON p.[product_id] = b.[product_id]
			LEFT JOIN [dbo].[sc_cola_state_sts] c
				ON c.[cola] = p.[TTB_ID]
				AND c.[client] = p.[client_code]
				AND c.[State] = s.[blr_state_name]
			WHERE p.[product_id] IS NOT NULL
				AND b.[client_name] = '".$_POST['client']."'
	";
	*/

	if (empty($_POST['view_type']) || empty($_POST['client']) ) {
		echo("Wrong parameters !"); exit;
	}

	if ($_POST['view_type'] === 'State Requests') {

		$tsql = "SELECT DISTINCT
				s.[blr_state_abbrev], s.[blr_state_name]
			FROM [dbo].[mhw_app_blr_states] s
			LEFT JOIN [dbo].[mhw_app_blr] b
				ON s.[blr_id] = b.[blr_id]
			LEFT JOIN [dbo].[mhw_app_prod] p
				ON p.[product_id] = b.[product_id]
			WHERE p.[product_id] IS NOT NULL
				AND b.[client_name] = '".addslashes($_POST['client'])."'
		";

	} else if ($_POST['view_type'] === 'Product Requests') {

		$tsql = "SELECT DISTINCT
				p.[product_id], p.[product_desc],
				p.[brand_name], p.[product_mhw_code],
				p.[TTB_ID], p.[federal_type], p.[client_code],
				f.[status] as federal_status
			FROM [dbo].[mhw_app_blr] b
			LEFT JOIN [dbo].[mhw_app_prod] p
				ON p.[product_id] = b.[product_id]
			LEFT JOIN [dbo].[sc_cola_federal_sts] f
				ON f.[cola] = p.[TTB_ID]
				AND f.[client] = b.[client_code]
			WHERE p.[product_id] IS NOT NULL
				AND b.[client_name] = '".addslashes($_POST['client'])."'
			ORDER BY p.[product_desc]
		";

	} else if ($_POST['view_type'] === 'All Products') {

		$tsql = "SELECT DISTINCT
				p.[product_id], p.[product_desc],
				p.[brand_name], p.[product_mhw_code],
				p.[TTB_ID], p.[federal_type], p.[client_code],
				f.[status] as federal_status
			FROM  [dbo].[mhw_app_prod] p
			LEFT JOIN [dbo].[sc_cola_federal_sts] f
				ON f.[cola] = p.[TTB_ID]
				AND f.[client] = p.[client_code]
			WHERE p.[product_id] IS NOT NULL
				AND p.[client_name] = '".addslashes($_POST['client'])."'
			ORDER BY p.[product_desc]
		";
	} else if ($_POST['view_type'] === 'All States') {

		$tsql = "SELECT DISTINCT
				[state_abbrev], [state_name]
			FROM [view_states]
			ORDER BY [state_name]
		";
	}
    else if ($_POST['view_type'] === 'Federal product compliance status') {
		
		$client_name_qry=" AND p.[client_code] = '".addslashes($_POST['client'])."'";
		
		$prod_ids_qry_part="";
		if($_POST['prodlistids']!==''&& $_POST['prodlistids']!=='[]')
		{
			$prodids =  str_replace("]","",str_replace("[","",$_POST['prodlistids']));
			$prod_ids_qry_part=" AND p.[product_id] IN (".$prodids.") ";			
		}
		
		$advance_searching_qry_part="";
		if($_POST['searching_data']!==''&& $_POST['searching_data']!=='{}')
		{
			$searching_data_val =  json_decode($_POST['searching_data']);
		    foreach($searching_data_val as $key=>$val)
			{
				if(trim($val)!='' && (substr($key, 0, 2)=='p.' || substr($key, 0, 2)=='f.'))
				{					
						$advance_searching_qry_part=$advance_searching_qry_part. " AND ". $key ." like '%".$val."%'";
				}
			}
			
			if(isset($searching_data_val->all_clients) && $searching_data_val->all_clients=='all_clients')
				{
					if($advance_searching_qry_part)
					{
					$client_name_qry="";
                    }					
				}
				
		}
		
		
		/* BLR STATUS FEDERAL QUERY - DISPLAY EACH PRODUCT WITH OR WITHOUT APPROVAL */

		$tsql = "SELECT DISTINCT
				p.[product_id],CASE
    WHEN ISNUMERIC(SUBSTRING(p.[TTB_ID],1,14))=1 AND len(SUBSTRING(p.[TTB_ID],1,14))=14 THEN SUBSTRING(p.[TTB_ID],1,14)
    ELSE ''
END AS prefix_ttb_id, p.[product_desc],
				p.[brand_name], p.[product_mhw_code],
				p.[TTB_ID], p.[federal_type], p.[client_code],
				f.[status] as federal_status,f.[status] as status,
				convert(varchar, f.[dateapproved], 120) as dateapproved,p.[product_class],f.[primaryfederalbasicpermitnumber],
				p.[country],f.[legalnameusedonlabel],f.[applicantname],convert(varchar, f.[datecompleted], 120) as datecompleted
				,'' as est_dt_approval,'' as user_notes,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount'
			FROM  [dbo].[mhw_app_prod] p
			LEFT JOIN [dbo].[sc_cola_federal_sts] f
				ON f.[cola] = p.[TTB_ID]
				AND f.[client] = p.[client_code]
				AND f.[status] is not NULL and f.[status]!=''
			WHERE p.[product_id] IS NOT NULL  
				AND p.active = 1
				  $client_name_qry  
				$prod_ids_qry_part $advance_searching_qry_part
			ORDER BY p.[product_desc]
		";
		// echo $tsql; exit;
	}
	else if ($_POST['view_type'] === 'State product compliance status') {
		
		 $client_name_qry=" AND p.[client_code] = '".addslashes($_POST['client'])."'";
		 $prod_ids_qry_part="";
		if($_POST['prodlistids']!==''&& $_POST['prodlistids']!=='[]')
		{
			$prodids =  str_replace("]","",str_replace("[","",$_POST['prodlistids']));
			$prod_ids_qry_part=" AND p.[product_id] IN (".$prodids.") ";			
		}
		
		$advance_searching_qry_part="";
		if($_POST['searching_data']!==''&& $_POST['searching_data']!=='{}')
		{
			$searching_data_val =  json_decode($_POST['searching_data']);
		    foreach($searching_data_val as $key=>$val)
			{ 
				if(trim($val)!='' && (substr($key, 0, 2)=='p.' || substr($key, 0, 2)=='s.' || substr($key, 0, 2)=='r.'))
				{					
						$advance_searching_qry_part=$advance_searching_qry_part. " AND ". $key ." like '%".$val."%'";
				}
			}
			
			if(isset($searching_data_val->all_clients) && $searching_data_val->all_clients=='all_clients')
				{
					if($advance_searching_qry_part)
					{
						$client_name_qry="";	
                    }						
				}
			
		}
	
		/* BLR STATUS STATE QUERY - DISPLAY EACH PRODUCT AND EACH STATE WITH OR WITHOUT REGISTRATION */
		
		$tsql = "SELECT DISTINCT
				p.[product_id], p.[product_desc],CASE
    WHEN ISNUMERIC(SUBSTRING(p.[TTB_ID],1,14))=1 AND len(SUBSTRING(p.[TTB_ID],1,14))=14 THEN SUBSTRING(p.[TTB_ID],1,14)
    ELSE ''
END AS prefix_ttb_id,
				p.[brand_name], p.[product_mhw_code],
				p.[TTB_ID], p.[federal_type], p.[client_code],
				s.StateApprovalNumber as StateApprovalNumber,s.Status as Status,s.EffectiveDate as EffectiveDate,r.state_name as State,s.Preparer as Preparer,'' as est_dt_approval,'' as mhw_user_notes,
				(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount'
			FROM  [dbo].[mhw_app_prod] p
			cross join [dbo].[__ref_states] r with (nolock)
            left outer join [__fact_state_reg_status] s 
           on s.[ProductKey] = p.[product_mhw_code] and s.client=p.client_code and s.State = r.state_name 			
            WHERE p.[product_id] IS NOT NULL
			AND p.active = 1
				  $client_name_qry  
			$prod_ids_qry_part  $advance_searching_qry_part
			ORDER BY p.[product_desc],r.state_name";	
		//	echo $tsql;exit;
	}
	else if($_POST['view_type'] == 'get_state_blr_rules')
	{		
		$tsql = "select blr_rule_franchise,blr_rule_at_rest,blr_rule_tax,blr_rule_state_reg,blr_rule_approvals,
        blr_rule_who,blr_rule_ctrl,blr_rule_key,blr_rule_additional_licensing,blr_rule_approval_time,
        blr_rule_renewal,blr_rule_renew_dates,blr_rule_wine_ff,blr_rule_spirit_ff,blr_rule_beer_ff
		from [dbo].[mhw_app_blr_rules] where blr_rule_state_name='".addslashes($_POST['rule_state_name'])."' 
		and blr_rule_federal_type='".addslashes($_POST['rule_federal_type'])."'";			
			//echo $tsql;exit;
	}
   else if ($_POST['view_type'] == 'blr_status_cmt') {
	        $product_cmt=$_POST['product_cmt'];
			$compliance_type=$_POST['compliance_type'];
			$cmt_product_id=$_POST['cmt_product_id']; 
			$user_name=$_SESSION['mhwltdphp_user'];
			
          if($compliance_type==='Federal product compliance status')
		  {
						$cmtInsql= "insert into [blr_status_cmt] (client_code,client_name,product_id,TTB_ID,brand_name,product_desc,
product_mhw_code,federal_type,status,dateapproved,product_class,
primaryfederalbasicpermitnumber,country,legalnameusedonlabel,applicantname,datecompleted,
active,username_name,create_dt,comment,compliance_type)
SELECT DISTINCT p.[client_code],p.[client_name],p.[product_id],p.[TTB_ID],p.[brand_name],p.[product_desc],
p.[product_mhw_code], p.[federal_type],f.[status] as status,convert(varchar, f.[dateapproved], 120) as dateapproved,p.[product_class],
 f.[primaryfederalbasicpermitnumber],p.[country],f.[legalnameusedonlabel],f.[applicantname],convert(varchar, f.[datecompleted], 120) as datecompleted,
0,'".$user_name."',GETDATE(),'".$product_cmt."','".$compliance_type."'
FROM  [dbo].[mhw_app_prod] p LEFT JOIN [dbo].[sc_cola_federal_sts] f ON f.[cola] = p.[TTB_ID] AND f.[client] = p.[client_code] 
AND f.[status] is not NULL and f.[status]!='' WHERE p.[product_id]=$cmt_product_id AND p.active = 1";
            $cmtResults= sqlsrv_query($conn, $cmtInsql);
		  }
		  else 
		  {
			  $cmtInsql= "insert into [blr_status_cmt] (client_code,client_name,product_id,TTB_ID,brand_name,product_desc,
product_mhw_code,federal_type,status,product_class,StateApprovalNumber,
state_name,active,username_name,create_dt,comment,compliance_type)
SELECT DISTINCT p.[client_code],p.[client_name],p.[product_id],p.[TTB_ID],p.[brand_name],p.[product_desc],
p.[product_mhw_code], p.[federal_type],s.Status as status,p.[product_class],s.StateApprovalNumber as StateApprovalNumber,
r.state_name as State,0,'".$user_name."',GETDATE(),'".$product_cmt."','".$compliance_type."'
FROM  [dbo].[mhw_app_prod] p cross join [dbo].[__ref_states] r with (nolock) left outer join [__fact_state_reg_status] s 
on s.[ProductKey] = p.[product_mhw_code] and s.client=p.client_code and s.State = r.state_name 
WHERE p.[product_id]=$cmt_product_id AND p.active = 1";
         $cmtResults= sqlsrv_query($conn, $cmtInsql);
		  }
          
   }
	else {
		echo("Wrong parameters !"); exit;
	}

}

if ($_POST['qry_type']=='blr-status-expand') {

	if (empty($_POST['view_type']) || empty($_POST['client']) || empty($_POST['key'])) {
		echo("Wrong parameters !"); exit;
	}

	if ($_POST['view_type'] === 'State Requests') {

		$tsql = "SELECT DISTINCT s.[blr_state_note],
				(STUFF((SELECT CAST(', ' + dl.Name AS VARCHAR(MAX)) 
					FROM [mhw_app_blr_state_dist] sd
					LEFT JOIN [dbo].[sc_distributor-list] dl
					ON dl.[DistKey] = sd.[blr_distributor_key]

					WHERE (s.[blr_state_id] = sd.[blr_state_id]) 
				FOR XML PATH ('')), 1, 2, '')) AS blr_distributors,
				s.[blr_state_id], s.[blr_state_abbrev],
				p.[product_id], p.[product_desc],
				p.[brand_name], p.[product_mhw_code],
				p.[TTB_ID], p.[federal_type], p.[client_code],
				b.[blr_id], b.[blr_confirmation], b.[client_name],
				b.[blr_contact], b.[blr_email], b.[blr_phone],
				b.[blr_reg_type], b.[blr_reg_sub_type],
				convert(varchar, b.[create_date], 120) as create_date,
				c.[Status] as state_status, f.[status] as federal_status,
				c.[ExpirationDate], c.[StateApprovalNumber], c.[State]
			FROM [dbo].[mhw_app_blr] b
			LEFT JOIN [dbo].[mhw_app_prod] p
				ON p.[product_id] = b.[product_id]
			LEFT JOIN [dbo].[mhw_app_blr_states] s
				ON s.[blr_id] = b.[blr_id]
			LEFT JOIN [dbo].[sc_distributor-list] d
				ON d.[DistKey] = s.[blr_distributor_key]
			LEFT JOIN [dbo].[sc_cola_state_sts] c
				ON c.[cola] = p.[TTB_ID]
				AND c.[client] = p.[client_code]
				AND c.[State] = s.[blr_state_name]
			LEFT JOIN [dbo].[sc_cola_federal_sts] f
				ON f.[cola] = p.[TTB_ID]
				AND f.[client] = b.[client_code]
			WHERE p.[product_id] IS NOT NULL
				AND s.[blr_state_abbrev] = '".addslashes($_POST['key'])."'
				AND b.[client_name] = '".addslashes($_POST['client'])."'
		";
		//echo( $tsql);exit;
	} else if ($_POST['view_type'] === 'Product Requests') {

		$tsql = "SELECT DISTINCT
				s.[blr_state_abbrev], s.[blr_state_name], s.[blr_state_note],
				(STUFF((SELECT CAST(', ' + dl.Name AS VARCHAR(MAX)) 
					FROM [mhw_app_blr_state_dist] sd
					LEFT JOIN [dbo].[sc_distributor-list] dl
					ON dl.[DistKey] = sd.[blr_distributor_key]

					WHERE (s.[blr_state_id] = sd.[blr_state_id]) 
				FOR XML PATH ('')), 1, 2, '')) AS blr_distributors,
				s.[blr_state_id],
				b.[blr_id], b.[blr_confirmation], b.[client_name],
				b.[blr_contact], b.[blr_email], b.[blr_phone],
				b.[blr_reg_type], b.[blr_reg_sub_type],
				convert(varchar, b.[create_date], 120) as create_date,
				c.[Status] as state_status, f.[status] as federal_status,
				c.[ExpirationDate], c.[StateApprovalNumber], s.[blr_state_name] as [State]
			FROM [dbo].[mhw_app_blr_states] s
			LEFT JOIN [dbo].[mhw_app_blr] b
				ON s.[blr_id] = b.[blr_id]
			INNER JOIN [dbo].[mhw_app_prod] p
				ON p.[product_id] = b.[product_id]
			LEFT JOIN [dbo].[sc_distributor-list] d
				ON d.[DistKey] = s.[blr_distributor_key]
			LEFT JOIN [dbo].[sc_cola_state_sts] c
				ON c.[cola] = p.[TTB_ID]
				AND c.[client] = b.[client_code]
				AND c.[State] = s.[blr_state_name]
			LEFT JOIN [dbo].[sc_cola_federal_sts] f
				ON f.[cola] = p.[TTB_ID]
				AND f.[client] = b.[client_code]
			WHERE b.[product_id] = '".addslashes($_POST['key'])."'
				AND b.[client_name] = '".addslashes($_POST['client'])."'
		";

	} else if ($_POST['view_type'] === 'All States') {

		$state_name = addslashes($_POST['key']);
		$client_name = addslashes($_POST['client']);

		$tsql = "SELECT DISTINCT
				p.[product_id]
				,p.[product_desc]
				,p.[brand_name]
				,p.[product_mhw_code]
				,p.[TTB_ID]
				,p.[federal_type]
				,p.[client_code]
				,s.[blr_state_abbrev]
				,(STUFF((SELECT CAST(', ' + dl.Name AS VARCHAR(MAX))
					FROM (
						SELECT DISTINCT
						  [Region]
						  ,[DistributorKey]
						FROM [sc_product_avail]
					) sd
					LEFT JOIN [dbo].[sc_distributor-list] dl
					ON dl.[DistKey] = sd.[DistributorKey]
					WHERE (s.[blr_state_abbrev] = sd.[Region])
				FOR XML PATH ('')), 1, 2, '')) AS blr_distributors
				,b.[blr_reg_type]
				,b.[blr_reg_sub_type]
				,b.[blr_confirmation]
				,convert(varchar, b.[create_date], 120) as create_date
				,c.[Status] as state_status
				,f.[status] as federal_status
				,c.[ExpirationDate]
				,c.[StateApprovalNumber]
				,c.[State]
			FROM [dbo].[mhw_app_prod] p
			LEFT OUTER JOIN [dbo].[sc_cola_state_sts] c
				ON c.[cola] = p.[TTB_ID]
				AND c.[client] = p.[client_code]
				AND c.[State] = '$state_name'
			LEFT OUTER JOIN [dbo].[sc_cola_federal_sts] f
				ON f.[cola] = p.[TTB_ID]
				AND f.[client] = p.[client_code]
			LEFT OUTER JOIN [dbo].[mhw_app_blr] b
				ON p.[product_id] = b.[product_id]
				AND b.[client_code] = p.[client_code]
			LEFT OUTER JOIN [dbo].[mhw_app_blr_states] s
				ON s.[blr_id] = b.[blr_id]
				AND s.[blr_state_name] = '$state_name'
			WHERE p.[product_id] IS NOT NULL
				AND ( c.[State] IS NOT NULL OR s.[blr_state_name] IS NOT NULL)
				AND ( 
					b.[blr_confirmation] IS NOT NULL 
					OR c.[Status]  IS NOT NULL 
					OR f.[Status]  IS NOT NULL 
				)
				AND p.[client_name] = '$client_name'
		";

	} else if ($_POST['view_type'] === 'All Products') {

		$tsql = "SELECT DISTINCT
				CASE WHEN c.[State] IS NOT NULL THEN c.[State] ELSE s.[blr_state_name] END AS [State]
				,b.[blr_reg_type]
				,b.[blr_reg_sub_type]
				,b.[blr_confirmation]
				,convert(varchar, b.[create_date], 120) as create_date
				,s.[blr_state_abbrev]
				,(STUFF((SELECT CAST(', ' + dl.Name AS VARCHAR(MAX))
					FROM (
						SELECT DISTINCT
						  [Region]
						  ,[DistributorKey]
						FROM [sc_product_avail] pa
						WHERE p.[TTB_ID] = pa.[Cola]
							AND pa.[client] = '".addslashes($_POST['client'])."'
					) sd
					LEFT JOIN [dbo].[sc_distributor-list] dl
					ON dl.[DistKey] = sd.[DistributorKey]
					WHERE (
						s.[blr_state_abbrev] = sd.[Region]
					)
				FOR XML PATH ('')), 1, 2, '')) AS blr_distributors
				,f.[status] as federal_status
				,c.[Status] as state_status
				,c.[ExpirationDate]
				,c.[StateApprovalNumber]
			FROM [dbo].[mhw_app_prod] p
			LEFT OUTER JOIN [dbo].[sc_cola_state_sts] c
				ON c.[cola] = p.[TTB_ID]
				AND c.[client] = p.[client_code]
			LEFT OUTER JOIN [dbo].[sc_cola_federal_sts] f
				ON f.[cola] = p.[TTB_ID]
				AND f.[client] = p.[client_code]
			LEFT OUTER JOIN [dbo].[mhw_app_blr] b
				ON p.[product_id] = b.[product_id]
				AND b.[client_code] = p.[client_code]
			LEFT OUTER JOIN [dbo].[mhw_app_blr_states] s
				ON s.[blr_id] = b.[blr_id]
			WHERE  ( c.[State] IS NOT NULL OR s.[blr_state_name] IS NOT NULL)
				AND ( 
					b.[blr_confirmation] IS NOT NULL 
					OR c.[Status]  IS NOT NULL 
					OR f.[Status]  IS NOT NULL 
				)
				AND p.[product_id] = '".addslashes($_POST['key'])."'
				AND p.[client_name] = '".addslashes($_POST['client'])."'
		";

	}
    else if ($_POST['view_type'] === 'State product compliance status') {

		$tsql = "SELECT DISTINCT
				p.[product_id], p.[product_desc],
				p.[brand_name], p.[product_mhw_code],
				p.[TTB_ID], p.[federal_type], p.[client_code],
				f.[status] as federal_status,f.[status] as status,
				convert(varchar, f.[dateapproved], 120) as dateapproved,p.[product_class],f.[primaryfederalbasicpermitnumber],
				p.[country],f.[legalnameusedonlabel],f.[applicantname],convert(varchar, f.[datecompleted], 120) as datecompleted
				,'' as user_notes
			FROM  [dbo].[mhw_app_prod] p
			LEFT JOIN [dbo].[sc_cola_federal_sts] f
				ON f.[cola] = p.[TTB_ID]
				AND f.[client] = p.[client_code]
			WHERE p.[product_id] IS NOT NULL
				AND p.active = 1
				AND p.[client_name] = '".addslashes($_POST['client'])."'
				AND p.[product_id] = '".addslashes($_POST['key'])."'
			ORDER BY p.[product_desc]
		";
		
	}
	else if ($_POST['view_type'] === 'Federal product compliance status') {
		$tsql = "SELECT DISTINCT
				p.[product_id], p.[product_desc],
				p.[brand_name], p.[product_mhw_code],
				p.[TTB_ID], p.[federal_type], p.[client_code],
				'' as StateApprovalNumber,'' as Status,'' as EffectiveDate,'' as State,'' as Preparer,'' as mhw_user_notes
			FROM  [dbo].[mhw_app_prod] p			
            WHERE p.[product_id] IS NOT NULL
			AND p.active = 1
			AND p.[client_name] = '".addslashes($_POST['client'])."'
			AND p.[product_id] = '".addslashes($_POST['key'])."'
			ORDER BY p.[product_desc]"; 	
	}
	else {
		echo("Wrong parameters !"); exit;
	}
}


/* ********************* added by Jobaidur :End ************************ */


function utf8ize($d) {
	if (is_array($d)) {
		foreach ($d as $k => $v) {
			$d[$k] = utf8ize($v);
		}
	} else if (is_string ($d)) {
		return utf8_encode($d);
	}
	return $d;
}

//echo( $tsql);exit;
$getResults= sqlsrv_query($conn, $tsql);
//echo ("Reading data from table" . PHP_EOL);
if ($getResults == FALSE)
	echo (sqlsrv_errors());

/* Processing query results */

/* Setup an empty array */
$json = array();
/* Iterate through the table rows populating the array */
do {
     while ($row = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
     $json[] =  utf8ize($row);
     }
} while ( sqlsrv_next_result($getResults) );
 

//$json = utf8ize($json);

/* Run the tabular results through json_encode() */
/* And ensure numbers don't get cast to strings */
echo json_encode($json);

/* Free statement and connection resources. */
sqlsrv_free_stmt( $getResults);
sqlsrv_free_stmt( $trkResults);
sqlsrv_close( $conn);




?>