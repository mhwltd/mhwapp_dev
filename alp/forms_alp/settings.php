<?php
error_reporting(1);
ini_set('display_errors', 1);

//$siteroot = "https://mhwdev-alp.azurewebsites.net/forms";
$siteroot = "https://mhwdev-alp.azurewebsites.net/forms_alp";

//CRD login
//$sitelogin = "https://mhwdev-alp.azurewebsites.net/forms/login.php";
$sitelogin = "https://mhwdev-alp.azurewebsites.net/forms_alp/login.php";

//file attachment 
$size_bytes_limit = 7680000; //ie. 51200 bytes = 50KB // 7680000 = 7500KB (7.5MB);
$extlimit = "yes"; //Limit the extensions of file uploads (yes/no)
//$limitedext = array(".jpg",".jpeg",".jpe",".tif",".tiff",".gif",".png",".bmp");
$limitedext = array(".jpg",".jpeg",".jpe",".tif",".tiff",".pdf");
$upload_dir = "fileuploads/";
$thumb_dir  = $upload_dir."thumbs/";

//Add TTB Number to end of url to check approved COLA
$ttblink="https://www.ttbonline.gov/colasonline/viewColaDetails.do?action=publicFormDisplay&ttbid=";
$opt_effective_month_prior_count = 3;
$opt_effective_month_forward_count = 4;

/* final item description calculation at item finalization time */
$item_description_calculation_at_item_finalized_time=1;

/* Validation for Non Finiliazed Product , set 1 for required and 0 for optional */
/* product-level fields*/
$product_validation_matrix=array(
'brand_name'=>1,
'product_desc'=>1,
'TTB_ID'=>1,
'federal_type'=>1,
'compliance_type'=>1,
'product_class'=>1,
'mktg_prod_type'=>1,
'bev_type'=>1,
'country'=>1,
'alcohol_pct'=>1
);

/* product-level dropdown fields are set to valid option */
$product_dropdown_valid_matrix=array(
'federal_type'=>1,
'compliance_type'=>1,
'product_class'=>1,
'mktg_prod_type'=>1,
'bev_type'=>1
);

/* checked Product is linked to Supplier */
$checked_product_is_linked_to_supplier=1;

/* supplier-level fields */
$supplier_validation_matrix=array(
'supplier_name'=>1,
'supplier_contact'=>1,
'supplier_fda_number'=>1,
'supplier_address_1'=>1,
'supplier_country'=>1,
'supplier_phone'=>1,
'supplier_email'=>1
);


/* File Requirements */
$checking_supported_file_extension=1;
$checking_inactive_image_override_record=1;

/* TTB_ID Requirements */
$checking_approved_federal_cola_record=0;
$checking_ttb_id_min_digit_length=1; 


/* Items-level fields */
$item_validation_matrix=array(
'container_type'=>1,
'container_size'=>1,
'bottle_material'=>1,
'stock_uom'=>1,
'bottles_per_case'=>1,
'chill_storage'=>1,
'outer_shipper'=>1,
'vintage'=>1
);

/* Image or TTB validation requirement */
$image_or_ttb_id_checking=1;

/* BLR State Rules Info Permitted for Client */
$client_permitted_rules_info=array(
'blr_rule_franchise'=>1,
'blr_rule_at_rest'=>1,
'blr_rule_tax'=>1,
'blr_rule_state_reg'=>1,
'blr_rule_approvals'=>1,
'blr_rule_who'=>1,
'blr_rule_ctrl'=>1,
'blr_rule_key'=>1,
'blr_rule_additional_licensing'=>1,
'blr_rule_approval_time'=>1,
'blr_rule_renewal'=>1,
'blr_rule_renew_dates'=>1,
'blr_rule_wine_ff'=>1,
'blr_rule_spirit_ff'=>1,
'blr_rule_beer_ff'=>1
);

/* BLR State Rules Info Permitted for admin & SuperAdmin */
$admin_permitted_rules_info=array(
'blr_rule_franchise'=>1,
'blr_rule_at_rest'=>1,
'blr_rule_tax'=>1,
'blr_rule_state_reg'=>1,
'blr_rule_approvals'=>1,
'blr_rule_who'=>1,
'blr_rule_ctrl'=>1,
'blr_rule_key'=>1,
'blr_rule_additional_licensing'=>1,
'blr_rule_approval_time'=>1,
'blr_rule_renewal'=>1,
'blr_rule_renew_dates'=>1,
'blr_rule_wine_ff'=>1,
'blr_rule_spirit_ff'=>1,
'blr_rule_beer_ff'=>1
);

/*BLR State Rules Modal Label Mapping*/
$blr_rules_info_level=array(
'blr_rule_franchise'=>'Franchise',
'blr_rule_at_rest'=>'At Rest',
'blr_rule_tax'=>'Tax',
'blr_rule_state_reg'=>'State Reg',
'blr_rule_approvals'=>'Approvals',
'blr_rule_who'=>'Who',
'blr_rule_ctrl'=>'Control',
'blr_rule_key'=>'Key',
'blr_rule_additional_licensing'=>'Additional Licensing',
'blr_rule_approval_time'=>'Approval Time',
'blr_rule_renewal'=>'Renewal',
'blr_rule_renew_dates'=>'Renew Dates',
'blr_rule_wine_ff'=>'Wine FF',
'blr_rule_spirit_ff'=>'Spirit FF',
'blr_rule_beer_ff'=>'Beer FF'
);



?>