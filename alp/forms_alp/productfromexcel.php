<!doctype html>
<html lang="ru">
 <head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <title>Import excel file</title>
 </head>
<body>
<div class="container">
<?php
//  Include PHPExcel_IOFactory
include 'Classes/PHPExcel.php';

$inputFileName = './bulk_uploaded_files/sample_template_with_data.xlsx';

//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch(Exception $e) {
    die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
}

//  Get worksheet dimensions
$sheet = $objPHPExcel->getSheet(0); 
$highestRow = $sheet->getHighestRow(); 
$highestColumn = $sheet->getHighestColumn();

// Header
$rowData = $sheet->rangeToArray('A2:' . $highestColumn . "2", NULL, TRUE, FALSE);
echo '<div class="row font-weight-bold">';
foreach( $rowData as $array )
 foreach( $array as $value )
  echo '<div class="col">'.$value.'</div>';
echo '</div>';

//  Loop through each row of the worksheet in turn
for ($row = 3; $row <= $highestRow; $row++){ 
    //  Read a row of data into an array
    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
    //  Insert row data array into your database of choice here
    $data = $rowData[0];

	// STEP 1
	// If Product ID is provided, confirm it exists for client, ignore any other product-level columns from import, do not make updates to product record.  
	// If Product ID is invalid, skip import row and display �Product ID not found� error with item details from skipped import row
	$client_code = $data[0];
	$product_id  = 0;
	$product_mhv_code = $data[3];

  
//    echo '<div class="row"><div class="col font-weight-bold">Row number '.$row.'</div></div>';
//    echo '<div class="row">';
//    foreach( $rowData as $array )
//     foreach( $array as $value )
//      echo '<div class="col">'.$value.'</div>';
//    echo '</div>';
}?>
 </div>
</body>