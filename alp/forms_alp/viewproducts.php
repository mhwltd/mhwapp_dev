<?php
//session_start();
include("sessionhandler.php");
include("prepend.php");
include("settings.php");
include("functions.php");
$nf_view_show=0;
include("viewImageUpload.php");

if(!isset($_SESSION['mhwltdphp_user'])){

	header("Location: login.php");
}
else{	
	
	include("dbconnect.php");
    include('head.php');
	
	$curr_client_name=$_SESSION['mhwltdphp_userclients'];
    $curr_client_code=$_SESSION['mhwltdphp_user_clients_codes'];

	$isAdmin = false;
	$editableField = "";
	$prompt_active=false;

	if($_SESSION['mhwltdphp_usertype'] == "SUPERUSER" || $_SESSION['mhwltdphp_usertype'] == "ADMIN" ){ 
		$isAdmin = true;
		$editableField = "editable: true,";
	}
	
	
	$editableField_v2="";
	if($editableField!="")
	{
	  $editableField_v2=$editableField;
	}
	else 
	{
      $editableField_v2 = "editable: {
	  type: 'text',
	  noeditFormatter: function(value, row) {
				if(row.finalized==0 || row.finalized=='0') {
				  return false
				}

				return value;
			  }
	   },";
	}

	 
	$country_opts = array();
	$conn = sqlsrv_connect($serverName, $connectionOptions);
	if( $conn === false) {
		die( print_r( sqlsrv_errors(), true));
	}
	$tsql_country = "select DISTINCT [Code], REPLACE([Desc],'\"','') as [Desc] from [dbo].[country-list] ORDER BY [Code]";
	$getResults_country = sqlsrv_query($conn, $tsql_country);

	if ($getResults_country == FALSE)
		echo (sqlsrv_errors());

	while ($row_country = sqlsrv_fetch_array($getResults_country, SQLSRV_FETCH_ASSOC)) {
		array_push($country_opts, $row_country);
	}
	sqlsrv_free_stmt($getResults_country);


	$container_size_opts = array();
	$tsql_container_size = "select DISTINCT ContainerSize + ' ' + ContainerSizeUOM as [size_opts] from [dbo].[mhw_app_container_size] ORDER BY ContainerSize + ' ' + ContainerSizeUOM ";
	$getResults_container_size = sqlsrv_query($conn, $tsql_container_size);
	if ($getResults_container_size == FALSE)
		echo (sqlsrv_errors());
	while ($row_container_size = sqlsrv_fetch_array($getResults_container_size, SQLSRV_FETCH_ASSOC)) {
		array_push($container_size_opts, $row_container_size);
	}
	sqlsrv_free_stmt($getResults_container_size);

	sqlsrv_close($conn);

	
	
	
	/* *********************** Validating requred fields for NonFinalized Screen : start * */

$federal_type = ['Ciders','Distilled Spirits','Malt Beverage','Non Alcohol','Wine'];
$compliance_type = ['Cider','Wine based Cider','Malt Based Cider','Spirits','Mixed Drinks','Malt Beverages','Non Alcohol','Wine','Wine Product','Low Alcohol Wine','Domestic Sake'];
$product_class = ['Imported', 'Domestic'];
$mktg_prod_type = ["Absinthe","Akvavit","Applejack","Arak","Arrack","Awamori","Baijiu","Beer","Bitters","Borovicka","Brandy","Brandy - Armagnac","Brandy - Cognac","Brandy - Eau-de-vie","Brandy - Fruit","Brandy - Grappa","Brandy - Palinka","Brandy - Pisco","Brandy - Rakia","Brandy - Singani","Brandy - Slivovitz","Brandy - Tuica","Cachaca","Cauim","Champagne","Chicha","Cider","Cider - Flavored","Coolers","Cordials and Liqueurs","Cream Liqueurs","Desi Daru","Flavored Malt Beverages/Coolers","Fortified","Fortified - Madeira","Fortified - Marsala","Fortified - Port","Fortified - Sherry","Fortified - Tonto","Fortified - Vermouth","Fruit Liqueur - Tepache","General","Gin","Horilka","Huangjiu","Icariine Liquor","Kaoliang","Kasiri","Kilju","Kumis","ManX Spirit","Maotai","Mead","Metaxa","Mezcal","Mixers","Neutral Grain Spirit","Nihamanchi","Non-Alcohol","Non-Grape Wine","Ogogoro","Other","Ouzo","Palm wine","Parakari","Raki","Rice Wine","Rose","RTD / Prepared Cocktails","Rum","Rum - Flavored","Rum - Mamajuana","Sake","Sakur�","Sangria","Schnapps","Shochu","Soju","Sonti","Sparkling","Sparkling - Cava","Specialty Spirit","Table Flavored","Table Flavored - Pulque","Table Red","Table White","Tequila","Tiswin","Vinsanto","Vodka","Vodka - Flavored","Water","Whiskey","Whiskey - American","Whiskey - Bourbon","Whiskey - Flavored","Whiskey - Foreign","Whiskey - Irish","Whiskey - Moonshine","Whiskey - Poit�n","Whiskey - Rye","Whiskey - Tenessee","Whisky - Canadian","Whisky - Japanese","Whisky - Scotch","Wine - Champagne","Wine - Coolers","Wine - Fortified","Wine - Fortified - Port","Wine - Fortified - Sherry","Wine - Fortified - Vermouth","Wine - Red","Wine - Rose","Wine - Sake","Wine - Sparkling","Wine - White","Wine Product"];
$bev_type = ["Carbonated Wine","Dessert Fruit Wine","Dessert Flavored Wine","Dessert Port/Sherry/(Cooking)","Wine Low Alcohol","Other Wine","Sparkling Wine/Champagne","Table Fruit Wine","Table Flavored Wine","Table Red and Rose Wine","Table White Wine","Vermouth","Cider","Beer","Brandy","Cordials &amp; Liqueurs","Cocktails &amp; Specialties","Gin","Imitations","Neutral Spirits","Other Liquor","Tequila","Rum","Whiskey"];
$checking_approved_federal_cola_record=0;
		
	
	$validProductList = array();
	$conn = sqlsrv_connect($serverName, $connectionOptions);
	if( $conn === false) {
		die( print_r( sqlsrv_errors(), true));
	}
	
	$part_query1="";
	$part_query2="";
	$part_query3="";
	$part_query4="";
	$part_query5="";
	$part_query51="";
	$part_query52="";
	$part_query61="";
	$part_query62="";
	$part_query1=generated_validation_query('p',$product_validation_matrix);
	$part_query2=generated_validation_query_v2('p',$product_dropdown_valid_matrix,$federal_type,$compliance_type,$product_class,$mktg_prod_type,$bev_type);
	$part_query3=generated_validation_query_v3('p',$checked_product_is_linked_to_supplier);
	$part_query4=generated_validation_query('s',$supplier_validation_matrix);
	
	if($checking_supported_file_extension)
	{
		$part_query51=generated_validation_query_v51('i4',$limitedext);
	}
	
	$part_query52="select count(*) from [sc_cola_federal_sts] f where f.[cola] =SUBSTRING(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(p.TTB_ID,'-',''),'a',''),'b',''),'A',''),'B',''),0,15) and f.[status] = 'Approved'";
	
	$part_query61=generated_validation_query('i3',$item_validation_matrix);	
	$part_query62=generated_validation_query_6('i3',$item_validation_matrix);
	
	$part_query61=($part_query61)?" and $part_query61":$part_query61;
	$part_query62=($part_query62)?" and $part_query62":$part_query62;
	
	$final_qry="";
	if($part_query1)
	{
		$final_qry=$part_query1;
	}
	
	if($part_query2)
	{
		if($final_qry)
		{
			$final_qry=$final_qry." and ".$part_query2;
		}
		else 
		{
			$final_qry=$part_query2;
		}
	}
	
	if($part_query3)
	{
		if($final_qry)
		{
			$final_qry=$final_qry." and ".$part_query3;
		}
		else 
		{
			$final_qry=$part_query3;
		}
	}
	
	
	if($part_query4)
	{
		if($final_qry)
		{
			$final_qry=$final_qry." and ".$part_query4;
		}
		else 
		{
			$final_qry=$part_query4;
		}
	}
	
	$tsql_validProductList= "SELECT p.[product_id],SUBSTRING(p.[TTB_ID],1,14) as prefix_ttb_id,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_item] i WHERE i.[product_id] = p.[product_id] AND i.[active] = 1 AND i.[deleted] = 0) as 'itemcount',CASE WHEN p.[federal_type]='Wine' THEN (SELECT COUNT(*) FROM [dbo].[mhw_app_prod_item] i3 WHERE i3.[product_id] = p.[product_id] AND i3.[active] = 1 AND i3.[deleted] = 0 $part_query61) ELSE (SELECT COUNT(*) FROM [dbo].[mhw_app_prod_item] i3 WHERE i3.[product_id] = p.[product_id] AND i3.[active] = 1 AND i3.[deleted] = 0  $part_query62) END AS 'itemcount2',(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount',(SELECT count(*) FROM [dbo].[mhw_app_prod_image] i4 WHERE i4.[product_id] = p.[product_id] AND i4.[active] = 1 AND i4.[deleted] = 0 $part_query51) as 'filecount2',($part_query52) as 'approved_cola_count' FROM [dbo].[mhw_app_prod] p WITH (NOLOCK) LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] s WITH (NOLOCK) on s.[supplier_id] = p.[supplier_id] WHERE p.[client_code] IN ('".$_SESSION['mhwltdphp_user_clients_codes']."') AND (p.[finalized] = 0 OR p.[product_id] in (select mhw_app_prod_item.product_id from mhw_app_prod_item where mhw_app_prod_item.finalized<>1 or mhw_app_prod_item.finalized is null))  AND p.[active] = 1 AND p.[deleted] = 0 and ".$final_qry;
   // echo $tsql_validProductList; exit; 
	$getResults_validProductList = sqlsrv_query($conn, $tsql_validProductList);

	if ($getResults_validProductList == FALSE)
		echo (sqlsrv_errors());

	while ($row_validProductList = sqlsrv_fetch_array($getResults_validProductList, SQLSRV_FETCH_ASSOC)) {
		
		if($image_or_ttb_id_checking==1)
		{
					if($row_validProductList['itemcount']==$row_validProductList['itemcount2'])
						{
							if($row_validProductList['filecount']==$row_validProductList['filecount2']  && $row_validProductList['filecount2']>0)
							{
								array_push($validProductList, $row_validProductList['product_id']);
							}
							else if($checking_approved_federal_cola_record==1 && ($checking_ttb_id_min_digit_length==1 && $row_validProductList['prefix_ttb_id']))
							{
								if($row_validProductList['approved_cola_count']>0 && (ctype_digit($row_validProductList['prefix_ttb_id']) && strlen($row_validProductList['prefix_ttb_id'])==14))
								{
									array_push($validProductList, $row_validProductList['product_id']);
								}
								
							}
							else if($checking_approved_federal_cola_record==1)
							{
								if($row_validProductList['approved_cola_count']>0)
								{
									array_push($validProductList, $row_validProductList['product_id']);
								}
								
							}
							else if($checking_ttb_id_min_digit_length==1 && $row_validProductList['prefix_ttb_id'])
							{
								if(ctype_digit($row_validProductList['prefix_ttb_id']) && strlen($row_validProductList['prefix_ttb_id'])==14)
								{
									array_push($validProductList, $row_validProductList['product_id']);
								}
								
							}
							else 
							{
								array_push($validProductList, $row_validProductList['product_id']);
							}						
						}
		}
		else 
		{ 
					if($row_validProductList['itemcount']==$row_validProductList['itemcount2'] && $row_validProductList['filecount']==$row_validProductList['filecount2'] && $row_validProductList['filecount2']>0)
						{
							if($checking_approved_federal_cola_record==1 && ($checking_ttb_id_min_digit_length==1 && $row_validProductList['prefix_ttb_id']))
							{
								if($row_validProductList['approved_cola_count']>0 && (ctype_digit($row_validProductList['prefix_ttb_id']) && strlen($row_validProductList['prefix_ttb_id'])==14))
								{
									array_push($validProductList, $row_validProductList['product_id']);
								}
								
							}
							else if($checking_approved_federal_cola_record==1)
							{
								if($row_validProductList['approved_cola_count']>0)
								{
									array_push($validProductList, $row_validProductList['product_id']);
								}
								
							}
							else if($checking_ttb_id_min_digit_length==1 && $row_validProductList['prefix_ttb_id'])
							{
								if(ctype_digit($row_validProductList['prefix_ttb_id']) && strlen($row_validProductList['prefix_ttb_id'])==14)
								{
									array_push($validProductList, $row_validProductList['product_id']);
								}
								
							}
							else 
							{
								array_push($validProductList, $row_validProductList['product_id']);
							}						
						}
		}
		

	}
	sqlsrv_free_stmt($getResults_validProductList);
	//echo "<pre>"; print_r($validProductList); exit;
	/* *********************** Validating requred fields for NonFinalized Screen : End * */
	
	/* *************************************** fetching supplier list : start *************  */
	$curr_client_name=$_SESSION['mhwltdphp_userclients'];
    $curr_client_code=$_SESSION['mhwltdphp_user_clients_codes'];

    $last_supplier_arr=array('supplier_name'=>'Add New Supplier');
	$supplier_opts = array();
	$conn = sqlsrv_connect($serverName, $connectionOptions);
	if( $conn === false) {
		die( print_r( sqlsrv_errors(), true));
	}
	
	$tsql_supplier_opts = "select DISTINCT supplier_name from mhw_app_prod_supplier where client_code='$curr_client_code' and active=1 and deleted=0 and supplier_name<>'' order by supplier_name";
	$getResults_supplier_opts = sqlsrv_query($conn, $tsql_supplier_opts);

	if ($getResults_supplier_opts == FALSE)
		echo (sqlsrv_errors());

	while ($row_supplier_opts = sqlsrv_fetch_array($getResults_supplier_opts, SQLSRV_FETCH_ASSOC)) {
		array_push($supplier_opts, $row_supplier_opts);
	}
	
	sqlsrv_free_stmt($getResults_supplier_opts);
	array_push($supplier_opts, $last_supplier_arr);
	
	$supplier_name_in="";
	foreach($supplier_opts as $sk=>$sv)
	{
		$s_name=$sv['supplier_name'];
		$supplier_name_in=$supplier_name_in."<option value='".$s_name."'>$s_name</option>";
	}
	//$supplier_name_all="<select class='form-select' name='supplier_name' id='supplier_name' class='supplier_name'>".$supplier_name_in."</select>";
	
	/* *************************************** fetching supplier list : end ********************** */


/* *************************************** Fetching Federal_type and MKTG_PROD_TYPE : start *************  */    
	$federal_mktg_prod_list = array();
	$federal_type_opts=array();
	$mktg_prod_type_opts=array();
	$conn = sqlsrv_connect($serverName, $connectionOptions);
	if( $conn === false) {
		die( print_r( sqlsrv_errors(), true));
	}
	
	$federal_mktg_map = "select federal_type,mktg_prod_type from mhw_app_federal_type_mktg_prod_map where active=1 order by id asc";
	$federal_mktg_opts = sqlsrv_query($conn, $federal_mktg_map);

	if ($federal_mktg_opts == FALSE)
		echo (sqlsrv_errors());

	while ($row_federal_mktg_opts = sqlsrv_fetch_array($federal_mktg_opts, SQLSRV_FETCH_ASSOC)) {
		$temp_federal_type=$row_federal_mktg_opts['federal_type'];
		$federal_mktg_prod_list[$temp_federal_type][]=$row_federal_mktg_opts['mktg_prod_type'];
		array_push($federal_type_opts, $row_federal_mktg_opts['federal_type']);
		array_push($mktg_prod_type_opts, $row_federal_mktg_opts['mktg_prod_type']);		
	}	
	sqlsrv_free_stmt($federal_mktg_opts);
	
	$federal_type_opts = array_values(array_unique($federal_type_opts));
	$mktg_prod_type_opts = array_values(array_unique($mktg_prod_type_opts));
	
	$federal_type_in="";
	foreach($federal_type_opts as $sk=>$sv)
	{
		$federal_type_in=$federal_type_in."<option value='".$sv."'>$sv</option>";
	}
	
	/* *************************************** Fetching Federal_type and MKTG_PROD_TYPE : end ********************** */	
	
   /* **************************************** Vintage dropdown options generating : Start ************************* */	
   
    $vintage_start_year=1898;
	$current_year=date("Y");
	$vintage_end_year=$current_year+5;
	$vintage_dropdown_options=array();
	
	$first_vintage_opt=array('vin_val'=>'0000','vin_name'=>'NV');
	array_push($vintage_dropdown_options, $first_vintage_opt);
	
	for($sy=$vintage_start_year;$sy<=$vintage_end_year;$sy++)
	{
		array_push($vintage_dropdown_options, array('vin_val'=>$sy,'vin_name'=>$sy));
	}

   /* **************************************** Vintage dropdown options generating : End ************************* */	
   	
?>

<style>
div.supplier_name_div
{
	display: none;
}	

div.federal_type_div
{
	display: none;
}

div.mktgProd_type_div
{
	display: none;
}

</style>
<script>
	var federalTypeOptions = ['Ciders','Distilled Spirits','Malt Beverage','Non Alcohol','Wine'];
	var complianceTypeOptions = ['Cider','Wine based Cider','Malt Based Cider','Spirits','Mixed Drinks','Malt Beverages','Non Alcohol','Wine','Wine Product','Low Alcohol Wine','Domestic Sake'];
	var productClassOptions = ['Imported', 'Domestic'];
	var marketingCategoryOptions = ["Absinthe","Akvavit","Applejack","Arak","Arrack","Awamori","Baijiu","Beer","Bitters","Borovicka","Brandy","Brandy - Armagnac","Brandy - Cognac","Brandy - Eau-de-vie","Brandy - Fruit","Brandy - Grappa","Brandy - Palinka","Brandy - Pisco","Brandy - Rakia","Brandy - Singani","Brandy - Slivovitz","Brandy - Tuica","Cachaca","Cauim","Champagne","Chicha","Cider","Cider - Flavored","Coolers","Cordials and Liqueurs","Cream Liqueurs","Desi Daru","Flavored Malt Beverages/Coolers","Fortified","Fortified - Madeira","Fortified - Marsala","Fortified - Port","Fortified - Sherry","Fortified - Tonto","Fortified - Vermouth","Fruit Liqueur - Tepache","General","Gin","Horilka","Huangjiu","Icariine Liquor","Kaoliang","Kasiri","Kilju","Kumis","ManX Spirit","Maotai","Mead","Metaxa","Mezcal","Mixers","Neutral Grain Spirit","Nihamanchi","Non-Alcohol","Non-Grape Wine","Ogogoro","Other","Ouzo","Palm wine","Parakari","Raki","Rice Wine","Rose","RTD / Prepared Cocktails","Rum","Rum - Flavored","Rum - Mamajuana","Sake","Sakur�","Sangria","Schnapps","Shochu","Soju","Sonti","Sparkling","Sparkling - Cava","Specialty Spirit","Table Flavored","Table Flavored - Pulque","Table Red","Table White","Tequila","Tiswin","Vinsanto","Vodka","Vodka - Flavored","Water","Whiskey","Whiskey - American","Whiskey - Bourbon","Whiskey - Flavored","Whiskey - Foreign","Whiskey - Irish","Whiskey - Moonshine","Whiskey - Poit�n","Whiskey - Rye","Whiskey - Tenessee","Whisky - Canadian","Whisky - Japanese","Whisky - Scotch","Wine - Champagne","Wine - Coolers","Wine - Fortified","Wine - Fortified - Port","Wine - Fortified - Sherry","Wine - Fortified - Vermouth","Wine - Red","Wine - Rose","Wine - Sake","Wine - Sparkling","Wine - White","Wine Product"];
	var beverageTypeOptions = ["Carbonated Wine","Dessert Fruit Wine","Dessert Flavored Wine","Dessert Port/Sherry/(Cooking)","Wine Low Alcohol","Other Wine","Sparkling Wine/Champagne","Table Fruit Wine","Table Flavored Wine","Table Red and Rose Wine","Table White Wine","Vermouth","Cider","Beer","Brandy","Cordials &amp; Liqueurs","Cocktails &amp; Specialties","Gin","Imitations","Neutral Spirits","Other Liquor","Tequila","Rum","Whiskey"];
	var countries = [<?=join(",\n", array_map(function($c) {
		return "{value:'".$c['Code']."',text:'".addslashes(utf8_encode($c['Desc']))."'}";
	}, $country_opts))?>];

	var containerTypeOptions = ['Bottle','Can','Keg-oneway','Keg-deposit','Other'];
	var containerSizeOptions = [<?=join(",\n", array_map(function($c) {
		return "{value:'".$c['size_opts']."',text:'".addslashes(utf8_encode($c['size_opts']))."'}";
	}, $container_size_opts))?>];
	var stockUomOptions = ['CASE','BOTTLE','KEG'];
	var unitDimensionsOptions= ['M','IN','FT','CM'];
	var weightUomOptions= ['KG','LB','G'];
	var validProductList = [<?php echo '"'.implode('","', $validProductList).'"' ?>];
	var validItemList = [<?php echo '"'.implode('","', $validProductList).'"' ?>];
	var required_product_fields=<?php echo json_encode($product_validation_matrix); ?>;
	var product_dropdown_valid_matrix=<?php echo json_encode($product_dropdown_valid_matrix); ?>;
	var required_supplier_fields=<?php echo json_encode($supplier_validation_matrix); ?>;
	var required_item_fields=<?php echo json_encode($item_validation_matrix); ?>;
	var required_file_ext=<?php echo $checking_supported_file_extension; ?>;
	var approved_federal_cola_record=<?php echo $checking_approved_federal_cola_record; ?>;
	var ttb_id_min_digit_length=<?php echo $checking_ttb_id_min_digit_length; ?>;
	var image_or_ttb_id_checking=<?php echo $image_or_ttb_id_checking; ?>;
	
    var supplier_names_all="<?php echo $supplier_name_in;?>";
	var federal_type_all="<?php echo $federal_type_in;?>";
	
	var vintageOptions = [<?=join(",\n", array_map(function($c) {
		return "{value:'".$c['vin_val']."',text:'".addslashes(utf8_encode($c['vin_name']))."'}";
	}, $vintage_dropdown_options))?>];
	
	var err_item_wise = {};
	var unchecked_item_wise = {};
 
	$( document ).ready(function() {

	   $.fn.editable.defaults.mode = 'inline';
	   $.fn.editable.defaults.emptytext = '...';
		
		$("#finalize").hide();

		$('#client_name').on('change', function () {
			var value = $(this).val().toLowerCase();
			var valorig = $(this).val();
			
			if(value!="all"){
				$('#prodTable').bootstrapTable('filterBy', {client_name: valorig});
			}
			else{
				$('#prodTable').bootstrapTable('filterBy', '');
			}

			$('.prodedit_btn').on("click", function() {
					var prod = $(this).data("target");
				$("#editprod_"+prod).submit();
			});
		});
		
		
		
		$(".prodedit_btn").on("click", function() {
			var prod = $(this).data("target");
			$("#editprod_"+prod).submit();
		});
		
		$(document).on("click", '.prod_upload', function(event) { 
          var prod = $(this).data('prod');				
		  window.location = 'imageupload.php?pid='+prod;
         });
		

		$(document).off('click', '.btn_itemtoggle').on('click', '.btn_itemtoggle',function(e) {
			var trparent = $(this).closest("tr");
			var prodindex = $(this).data('index');
			var exp = trparent.find(".detail-icon");
			$( exp ).trigger( "click" );
		}); 
		//function (to be called within viewproducts.php) to update alert area in nav (head.php)
		function refreshAlert(){
			var clientlistH = '<?php echo $_SESSION["mhwltdphp_user_clients_codes"]; ?>';
			var qrytypeH = 'STAT_COUNT_unfinalized';
			$.post('query-request.php', {qrytype:qrytypeH,clientlist:clientlistH}, function(dataTH){ 
				// ajax request query-request.php, send qrytype & client as post variable, return dataX variable, parse into JSON
				var resp = JSON.parse(dataTH);
				$.each( resp, function( key, value ) {
					if(parseInt(value.unfinalizedCount)>0){
						$("#userAlerts").html("<i class=\"fas fa-exclamation-triangle\"></i><a href=\"viewproducts.php?v=nf\"> Drafts <span class=\"badge badge-light\">"+value.unfinalizedCount+"</span></a>");
					}
					else{ 
						$("#userAlerts").html(""); 
					}

				});
			});
		}
		//function to initialize all of the in-modal functionality
		function modalStuff(){
			//from image details to image full preview
			$('.prodimg_thumb').bind('click',function(){
				var imageid = $(this).data('imageid');
				var productid = $(this).data('product');
				var imageurl = 'viewfiles.php?iid='+imageid+'&b='+productid;
				$('.modal-body').load(imageurl,function(){
					$('#fileModalx').modal({show:true});
					//modalStuff(); //recursive but necessary to initialize other states on each change
				});
			});
			//back link in modal from image full to image details
			$('.prodimg_back').bind('click',function(){
				var productid = $(this).data('product');
				var producturl = 'viewfiles.php?pid='+productid;
				$('.modal-body').load(producturl,function(){
					$('#fileModalx').modal({show:true});
					//modalStuff(); //recursive but necessary to initialize other states on each change
				});
			});
			//initialize prod image modal functionality again
			$('.prodimg_btn').bind('click',function(){
				var productid = $(this).data('product');
				var imageurl = 'viewfiles.php?pid='+productid;
				$('.modal-body').load(imageurl,function(){
					$('#fileModalx').modal({show:true});
					//modalStuff(); //recursive but necessary to initialize other states on each change
				});
			});
			
			$('.direct_image_upload').bind('click',function(){
				var file_productid = $(this).data('product');
				var modelurl = 'loadImageUpload.php?fileProdID='+file_productid;
				$('.modal-body').load(modelurl,function(){
					$('#UpDirectFileModalx').modal({show:true});
					//modalStuff(); //recursive but necessary to initialize other states on each change
							
				});
			});
						
			
			//download image button
			$('.dwnld').bind('click', function () {
				var imagefile = $(this).data('url');
				var imagename = $(this).data('imagename');
				$.ajax({
					url: imagefile,
					method: 'GET',
					xhrFields: {
						responseType: 'blob'
					},
					success: function (data) {
						var a = document.createElement('a');
						var url = window.URL.createObjectURL(data);
						a.href = url;
						a.download = imagename;
						a.click();
						window.URL.revokeObjectURL(url);
					}
				});
			});
			//go to upload page for product
			$('.prod_upload').bind('click', function () {
				var prod = $(this).data('prod');				
				window.location = 'imageupload.php?pid='+prod;
			});
			
			
			$(".prodedit_btn").bind("click", function() {
				var prod = $(this).data("target");
				$("#editprod_"+prod).submit();
			});
			
			/* onchange supplier name : start  */
			$(".supplier_cancel_btn").bind("click", function() {
				var prod = $(this).data("product");
				var curr_supplier_div="#supplier_name_div_"+prod;
				$(curr_supplier_div).hide();				
			});
			
			$(".supplier_edit_btn").bind("click", function() {
				var prod = $(this).data("product");
				var curr_supplier_div="#supplier_name_div_"+prod;				
				$(curr_supplier_div).hide();
				
				var curr_supplier_text="#supplier_name_"+prod+" :selected";
                var selected_supplier_name=$(curr_supplier_text).text();
				var selected_supplierID="#supplier_name_"+prod;
				var selected_supplier_id=$(selected_supplierID).val();

				
				var sn_data;
				var clientlist = $("#clientlist").val();
				$.post('update_ajax.php', 
				  {tn:'mhw_app_prod',clientlist:clientlist,name:'supplier_id',product_id:prod,value:selected_supplier_name}, function(res_data){
					//sn_data = JSON.parse(res_data);
					alert("Supplier Name has been updated successfully.");
				});

               				
			});
			
			/* onchange supplier name : end  */
			
			
			
			/* onchange Federal Type : start  */
			
			$(".ft_cancel_btn").bind("click", function() {
				var prod = $(this).data("product");
				var curr_ft_div="#federal_type_div_"+prod;
				$(curr_ft_div).hide();				
			});
			
			$(".ft_edit_btn").bind("click", function() {
				var prod = $(this).data("product");
				var curr_ft_div="#federal_type_div_"+prod;				
				$(curr_ft_div).hide();
				
				var curr_ft_text="#federal_type_"+prod+" :selected";
                var selected_federal_type=$(curr_ft_text).text();
				var selected_FTID="#federal_type_"+prod;
				var selected_ft_id=$(selected_FTID).val();

				
				var sn_data;
				var clientlist = $("#clientlist").val();
				
				  var is_admin='<?php echo ($isAdmin)?'Yes':'No';?>';
				  var prompt_active='<?php echo ($prompt_active)?'Yes':'No';?>';
				  var confirm_txt="confirmed_yes";
				  if(is_admin=="Yes" && prompt_active=="Yes")
				  {
					  if (confirm("Are you want to change Item Description?")) {
						confirm_txt = "confirmed_yes";
					  } else {
						confirm_txt="";
					  }
				  }
				  else if(is_admin=="Yes" && prompt_active=="No")
				  {
					  confirm_txt="";
				  }
						  
				$.post('update_ajax.php', 
				  {tn:'mhw_app_prod',clientlist:clientlist,name:'federal_type',product_id:prod,value:selected_federal_type,confirm_txt:confirm_txt}, function(res_data){	
					alert("Federal Type has been updated successfully.");					
				});

               				
			});
			
			/* onchange Federal Type : end  */
			
			
						
			
			/* onchange MKTG Product Type : start  */
			
			$(".mkpt_cancel_btn").bind("click", function() {
				var prod = $(this).data("product");
				var curr_mkpt_div="#mktgProd_type_div_"+prod;
				$(curr_mkpt_div).hide();				
			});
			
			$(".mkpt_edit_btn").bind("click", function() {
				var prod = $(this).data("product");
				var curr_mkpt_div="#mktgProd_type_div_"+prod;				
				$(curr_mkpt_div).hide();
				
				var curr_mkpt_text="#mktgProd_type_"+prod+" :selected";
                var selected_mkpt=$(curr_mkpt_text).text();
				var selected_mkpt_id="#mktgProd_type_"+prod;
				var selected_ft_id=$(selected_mkpt_id).val();
								
				var sn_data;
				var clientlist = $("#clientlist").val();
				$.post('update_ajax.php', 
				  {tn:'mhw_app_prod',clientlist:clientlist,name:'mktg_prod_type',product_id:prod,value:selected_mkpt}, function(res_data){	
					alert("MKTG Product Type has been updated successfully.");
				});

               				
			});
			
			/* onchange MKTG Product Type : end  */
			
			
		};
		function alertTray(qrytype,prodlist,codelist){
			var clientlist = '<?php echo $curr_client_name; ?>';  
			var trayURL = '<?php echo $trayFinalizedWorkflow; ?>';  //defined in settings.php

			//$.post(trayURL, {qrytype:qrytype,prodlist:prodlist,clientlist:clientlist,codelist:codelist}, function(dataT){ 
				// ajax request tray web hook, send qrytype, products & clients as post variables
			$.post('query-request.php', {qrytype:qrytype,prodlist:prodlist,clientlist:clientlist,codelist:codelist}, function(dataT){ 
				// ajax request to queue finalization, send qrytype, products & clients as post variables
			});
		}

		//Bootstrap table
		var $table = $('#prodTable')
		var $remove = $('#remove')
		var selections = []

		function getIdSelections() {
			return $.map($table.bootstrapTable('getSelections'), function (row) {
				var viewname_raw = $("#screenview_name").val();
				if(viewname_raw=="Items") { 
					return row.item_id
				}
				else{
					return row.product_id
					//return row.product_mhw_code
				}
			})
		}
		
		
		function getItemIdSelections() {
			var viewname_raw = $("#screenview_name").val();
			if(viewname_raw=="Drafts") { 
			return  $("input[name='btSelectItemDetails']:checked").map(function () {
				return this.value;
			}).get();
			}	
		}
		

		function getUncheckedItemList() {
			var viewname_raw = $("#screenview_name").val();
			var uncheckedItems = [];
			if(viewname_raw=="Drafts") { 			
				Object.keys(unchecked_item_wise).map(function(key, index) {
				if(unchecked_item_wise[key]==1)
					{
						uncheckedItems.push(key);
					}
					});
				
			}
           return uncheckedItems;			
		}
		
		function getCodeSelections() {
			return $.map($table.bootstrapTable('getSelections'), function (row) {
				var viewname_raw = $("#screenview_name").val();
				if(viewname_raw=="Items") { 
					return row.item_id
				}
				else{
					//return row.product_id
					return row.product_mhw_code
				}
			})
		}
		function expandTable($detail, row) {
			$detail.bootstrapTable('showLoading');
			subtableBuilder($detail.html('<table id="item_details_'+row.product_id+'" class="table-info item_details_st" data-id-field="item_id"></table>').find('table'), 'VPproditems', row.product_id)
		}
		function responseHandler(res) {
			$.each(res.rows, function (i, row) {
			  row.state = $.inArray(row.id, selections) !== -1
			})
			return res
		}

		function detailFormatter(index, row) {
			var html = []
			$.each(row, function (key, value) {
			  html.push('<p><b>' + key + ':</b> ' + value + '</p>')
			})
			return html.join('')
		}

		window.operateEvents = {
			'click .like': function (e, value, row, index) {
			  //alert('You click like action, row: ' + JSON.stringify(row))
			},
			'click .prodimg_btn': function (e, value, row, index) {
			  //alert('You click prodimg_btn action, row: ' + JSON.stringify(row))
			},
			'click .btn-delete-product': function (e, value, row, index) {
			 var is_admin='<?php echo ($isAdmin)?'Yes':'No';?>';	
			 if(is_admin=='Yes' || (row.finalized==0 || row.finalized=='0'))
				{
				var p_del_confirm = confirm("Confirm Delete?"); 
				if (p_del_confirm == true) { 
					$.post('update-inline.php', {
						action: 'delete',
						table: 'mhw_app_prod',
						pk: row.product_id,
					}, function(result) {
			  $table.bootstrapTable('remove', {
							field: 'product_id',
							values: [row.product_id]
			  })
					});
				}
			  }
			},
			'click .btn-delete-item': function (e, value, row, index) {
				var is_admin='<?php echo ($isAdmin)?'Yes':'No';?>';
				if(is_admin=='Yes' || (row.finalized==0 || row.finalized=='0'))
				{
				var i_del_confirm = confirm("Confirm Delete?"); 
				if (i_del_confirm == true) { 
					$.post('update-inline.php', {
						action: 'delete',
						table: 'mhw_app_prod_item',
						pk: row.item_id,
					}, function(result) {
						$(e.target).closest('tr').remove();
					});
				}
			  }
			},
			<?php if ($isAdmin) { ?>
			'click .btn-audit-product': function (e, value, row, index) {
				window.open('/forms/audit-trail-view.php?product_id='+row.product_id)
			},
			'click .btn-audit-item': function (e, value, row, index) {
				window.open('/forms/audit-trail-view.php?item_id='+row.item_id)
			},
			<?php } ?>
			}

			
			window.ShowingEmptyoperateEvents = {
			'click .btn-showing-empty-fields': function (e, value, row, index) {
				
				/* checking product level empty fields */
				var err_msg='';
				var x=0;
				
				$.each(required_product_fields, function (k, val) {
					var field_val=getJsonValue(row,k)

			     if(val==1 && !field_val)
				 {
					 err_msg=err_msg+' '+k+' ,';
					 
				 }
				 				 
			    });
				if(err_msg.length>1)
				{
					err_msg = err_msg.slice(0, -1);
					err_msg="Product Level form empty Fields are : "+err_msg;
				}
				
				/* checking valid product level dropdown options */
				
				var err_msg2='';
				$.each(product_dropdown_valid_matrix, function (k, val) {
					var field_val=getJsonValue(row,k);
					
					if(val==1)
					{
						if(k=="federal_type")
						{
							if(!field_val && !checkArryValue(federalTypeOptions,row.federal_type))
							{
								err_msg2=err_msg2+' Federal Type,';
							}
							
						}
						else if(k=="compliance_type")
						{
							if(!field_val && !checkArryValue(complianceTypeOptions,row.compliance_type))
							{
								err_msg2=err_msg2+' Compliance Type,';
							}
							
						}
						else if(k=="product_class")
						{
							if(!field_val && !checkArryValue(productClassOptions,row.product_class))
							{
								err_msg2=err_msg2+' Product Classification,';
							}
							
						}
						else if(k=="mktg_prod_type")
						{
							if(!field_val && !checkArryValue(marketingCategoryOptions,row.mktg_prod_type))
							{
								err_msg2=err_msg2+' Marketing Category,';
							}
							
						}
						else if(k=="bev_type")
						{
							if(!field_val && !checkArryValue(beverageTypeOptions,row.bev_type))
							{
								err_msg2=err_msg2+' Beverage Type,';
							}
							
						}
						
					}
			 
			    });
				if(err_msg2.length>1)
				{
					err_msg2 = err_msg2.slice(0, -1);
					err_msg2="Invalid Product Level dropdown Fields are : "+err_msg2;
				}
				
				/* checking valid product to supplier link */
				var err_msg3='';
				<?php if ($checked_product_is_linked_to_supplier) { ?>
				if(!row.supplier_id)
				{
					err_msg3="Product is not linked to supplier and supplier id is invalid";
				}
				<?php } ?>
				
				/* checking Supplier level empty fields */
				var err_msg4='';
				
				$.each(required_supplier_fields, function (k, val) {
					var field_val=getJsonValue(row,k)

			     if(val==1 && !field_val)
				 {
					 err_msg4=err_msg4+' '+k+' ,';
					 
				 }
				 				 
			    });
				if(err_msg4.length>1)
				{
					err_msg4 = err_msg4.slice(0, -1);
					err_msg4="Supplier Level form empty Fields are : "+err_msg4;
				}
				
				var err_msg5='';
				/* checking file extension : start */
				
			if(row.filecount>0 && (required_file_ext==1 || required_file_ext=='1'))
				{
					var file_qry_type='getFileDetails';
					var file_product_id=row.product_id;
					var i=1;
					$.ajaxSetup({async: false});
					$.post('query-request.php', {qrytype:file_qry_type,prodID:file_product_id}, function(dataFile){ 
						var file_details_info = JSON.parse(dataFile);
						$.each( file_details_info, function( file_key, file_value ) {
							   var valid_ext=getJsonValue(file_value,'valid_file_ext_count');
							   if(valid_ext==1 || valid_ext=='1')
							   {
								   // valid image extension 
							   }
							   else 
							   {
								   err_msg5=err_msg5+"For File Item "+i+" : image extension is invalid \n";
							   }
									
										
							 i++;
						});
						
					});
					$.ajaxSetup({async: true});

				}
				else if (row.filecount<1 && (required_file_ext==1 || required_file_ext=='1'))
				{
					err_msg5=err_msg5+"Upload Valid Image or File \n";
				}
				
				
				/* checking file extension : End */
				
				/* checking approved_cola and TTB_ID_MIN_LENGTH : start */
				
				   // approved_cola err msg 
				  var err_msg52='';
				  // valid ttb id error message
				  var err_msg53='';
				  
					if(approved_federal_cola_record==1 || approved_federal_cola_record=='1' || ttb_id_min_digit_length==1 || ttb_id_min_digit_length=='1')
						{
							var cola_qry_type='getTtbIdInfo';
							var cola_product_id=row.product_id;
							
							$.ajaxSetup({async: false});
							$.post('query-request.php', {qrytype:cola_qry_type,prodID:cola_product_id}, function(dataCola){ 
								var cola_details_info = JSON.parse(dataCola);
								$.each( cola_details_info, function( cola_key, cola_value ) {
									
									  
									   
									if(ttb_id_min_digit_length==1 || ttb_id_min_digit_length=='1')   
									{
									   var valid_len=getJsonValue(cola_value,'is_valid_min_len');
									   if(valid_len==1 || valid_len=='1')
									   {
										   // valid len TTB_ID
									   }
									   else 
									   {
										  err_msg52="TTB_ID does not meet the minimum length requirement."; 
					
									   }
									}

								if(approved_federal_cola_record==1 || approved_federal_cola_record=='1')
									 {
									   var valid_cola=getJsonValue(cola_value,'is_valid_cola');
									   if(valid_cola!=1 && valid_cola !='1') 
									   {
										   //err_msg53="Approved Federal Cola record does not exist.";
										   err_msg53="Upload valid image or enter valid TTB ID that satisfy requirement";
									   }
									 } 									
										
								});
								
							});
							$.ajaxSetup({async: true});

						}
				  //err_msg52=getJsonValue(ttb_id_msg,row.product_id);
				  
				/* checking approved_cola and TTB_ID_MIN_LENGTH : End */
				
				/* checking Item level empty fields */
				var err_msg6='';
				
				/* retirving item details data */				
				
				if(row.itemcount>0)
				{
					var item_qry_type='getItemDetails';
					var item_product_id=row.product_id;
					var item_federal_type=row.federal_type;
					var x=1;
					$.ajaxSetup({async: false});
					$.post('query-request.php', {qrytype:item_qry_type,prodID:item_product_id}, function(dataItem){ 
						var item_details_info = JSON.parse(dataItem);
						
						$.each( item_details_info, function( item_key, item_value ) {
							  var err_part6="";
							  
							  var curr_item_id=getJsonValue(item_value,'item_id');
							  var curr_item_code=getJsonValue(item_value,'item_client_code');
							  
							 // console.log('item_id:'+curr_item_id+' :::curr_item_code:'+curr_item_code);
							  
								$.each(required_item_fields, function (k, val) {
										var field_val=getJsonValue(item_value,k);
										
									if(val==1 && k=='vintage' && item_federal_type=='Wine' && !field_val)
									{
										err_part6=err_part6+' '+k+' ,';
									}
                                    else if(val==1 && k!='vintage' && !field_val)
									{
										err_part6=err_part6+' '+k+' ,';
									}
										 
									});
									
									
									
									if(err_part6.length>1)
										{
											err_part6 = err_part6.slice(0, -1);
											err_part6="Empty fields for Item id : "+curr_item_id+" & Item code : "+curr_item_code+" are : "+err_part6;
											if(x==1)
											{
												err_msg6=err_part6;
											}
											else 
											{
												err_msg6=err_msg6+'\n'+err_part6;
											}
											
										}
										
										x++;
						});
						
					});
					$.ajaxSetup({async: true});

				}
			
				
				var final_err_msg='';
				if(err_msg.length>1)
				{
					final_err_msg=err_msg;
				}
				
				if(err_msg2.length>1)
				{
					if(final_err_msg.length>1)
					{
						final_err_msg=final_err_msg+'\n'+err_msg2;
					}
					else 
					{
						final_err_msg=err_msg2;
					}
					
				}
				
				
				if(err_msg3.length>1)
				{
					if(final_err_msg.length>1)
					{
						final_err_msg=final_err_msg+'\n'+err_msg3;
					}
					else 
					{
						final_err_msg=err_msg3;
					}
					
				}
				
				
				if(err_msg4.length>1)
				{
					if(final_err_msg.length>1)
					{
						final_err_msg=final_err_msg+'\n'+err_msg4;
					}
					else 
					{
						final_err_msg=err_msg4;
					}
					
				}
				
				
				/* if image and TTB_ID both have error message  */
				
				if(image_or_ttb_id_checking==1 || image_or_ttb_id_checking=='1')
				{
					if(err_msg5.length>1 && err_msg52.length>1)
						{
							
									if(final_err_msg.length>1)
									{
										final_err_msg=final_err_msg+'\n'+'Upload valid image or enter valid TTB ID that satisfy requirement';
									}
									else 
									{
										final_err_msg='Upload valid image or enter valid TTB ID that satisfy requirement';
									}
						}
				
				}
				else 
				{
										
							if(err_msg5.length>1)
								{
									if(final_err_msg.length>1)
									{
										err_msg5 = err_msg5.slice(0, -1);
										final_err_msg=final_err_msg+'\n'+err_msg5;
									}
									else 
									{
										//final_err_msg=err_msg5;
										final_err_msg=err_msg5.slice(0, -1);
									}
									
								}
					
							if(err_msg52.length>1)
								{
									if(final_err_msg.length>1)
									{
										final_err_msg=final_err_msg+'\n'+err_msg52;
									}
									else 
									{
										final_err_msg=err_msg52;
									}
									
								}
				}
				
				
				if(err_msg53.length>1)
				{
					if(final_err_msg.length>1)
					{
						final_err_msg=final_err_msg+'\n'+err_msg53;
					}
					else 
					{
						final_err_msg=err_msg53;
					}
					
				}
				
				
				if(err_msg6.length>1)
				{
					if(final_err_msg.length>1)
					{
						final_err_msg=final_err_msg+'\n'+err_msg6;
					}
					else 
					{
						final_err_msg=err_msg6;
					}
				}
				
			  alert(final_err_msg);
			},
			}

			
	  function checkArryValue(validProduct,value)
	  {
		  var is_matched=false;
		 validProduct.forEach((product_data, p_index) => {
				 if(product_data==value && !is_matched)
				 {
					is_matched=true;
				 }
      });  
	  
	  return is_matched;
	  }	  
	  
	  
	  function getJsonValue(row,k)
	  {
		  var return_val='';
		  $.each(row, function (rk, rval) {
						if(rk==k)
						{
							return_val=rval;
						}
					});
			return return_val;	
	  }
	  
	  
	/* ******************** Image Upload button model: start ************************************  */  
	
		
		window.operateEventsImageUpload = {
			'click .direct_image_upload': function (e, value, row, index) {
				//var ttbVal = row.product_id;
				//var ttbInt = parseInt(ttbVal);
				//window.location = 'imageupload.php?pid='+ttbVal;
			},
			}

		
		function operateFormatterImageUpload(value, row, index) {	
		
		        var ttbVal = row.product_id;
				var ttbInt = parseInt(ttbVal);  //TTB value must be integer to be valid, but may begin with leading zeros so use text value for url
				if(ttbVal!="" && ttbInt){
				
				return [
						'<button type="button" class="direct_image_upload btn btn-primary bg_arrow_blue" data-product="'+row.product_id+'">',
					'<span class="oi oi-data-transfer-upload" title="data-transfer-upload" aria-hidden="true"></span>Upload Image</button>',
					].join('');
				}
				else 
				{
					return [
						'<button type="button" class="direct_image_upload btn btn-primary bg_arrow_blue" data-product="'+row.product_id+'">',
					'<span class="oi oi-data-transfer-upload" title="data-transfer-upload" aria-hidden="true"></span>Upload Image</button>',
					].join('');
				}
			
		}
		
		
	/* ******************** Image Upload button model: end ************************************  */  
	 

	  
	/* ******************** Supplier Name button model: start ************************************  */  
	
		
		window.operateEventsSupplierName = {
			'change .supplier_name_select': function (e, value, row, index) {
				var curr_product_id = row.product_id;
				var curr_supplier_name=row.supplier_name;
				var curr_supplier_id="#supplier_name_"+row.product_id+" :selected";
				var curr_supplier_div="#supplier_name_div_"+row.product_id;
				var selected_supplier_name=$(curr_supplier_id).text();
				if(selected_supplier_name=="Add New Supplier")
				{
					$("#editprod_"+curr_product_id).attr('action', 'productsetup.php#edit_supplier');
				    $("#editprod_"+curr_product_id).submit();
				}
				else 
				{
					$(curr_supplier_div).show();									
                }
			},
			}
		
		
		
		function operateFormatterSupplierName(value, row, index) {	
		
		        var curr_product_id = row.product_id;
				var curr_product_id = parseInt(curr_product_id); 
				var curr_supplier_name = row.supplier_name;
				var supplier_names="";
				
				if(curr_supplier_name && curr_supplier_name!= null && curr_supplier_name!="")
				{
					var selected_str="<option selected value='"+curr_supplier_name+"'>"+curr_supplier_name+"</option>";
					var replacement_str="<option value='"+curr_supplier_name+"'>"+curr_supplier_name+"</option>";
					var suppliers_with_selected=supplier_names_all.replace(replacement_str,selected_str);
					supplier_names='<select class="form-select supplier_name_select" name="supplier_name_'+row.product_id+'" id="supplier_name_'+row.product_id+'">'+suppliers_with_selected+'</select><div  class="supplier_name_div" id="supplier_name_div_'+row.product_id+'"><div class="editable-buttons" style="padding-top:3px"><button type="button" style="margin-right:5px" class="btn btn-primary btn-sm supplier_edit_btn" data-product="'+row.product_id+'"><i class="fa fa-check" aria-hidden="true"></i></button><button type="button" class="btn btn-primary btn-sm supplier_cancel_btn" data-product="'+row.product_id+'"><i class="fa fa-times" aria-hidden="true"></i></button></div></div>';
				}
				else 
				{
					supplier_names='<select class="form-select supplier_name_select" name="supplier_name_'+row.product_id+'" id="supplier_name_'+row.product_id+'"><option selected value="">--</option>'+supplier_names_all+'</select><div class="supplier_name_div" id="supplier_name_div_'+row.product_id+'"><div class="editable-buttons" style="padding-top:3px"><button type="button" style="margin-right:5px" class="btn btn-primary btn-sm supplier_edit_btn" data-product="'+row.product_id+'"><i class="fa fa-check" aria-hidden="true"></i></button><button type="button" class="btn btn-primary btn-sm supplier_cancel_btn" data-product="'+row.product_id+'"><i class="fa fa-times" aria-hidden="true"></i></button></div></div>';
				}
								
				if(curr_product_id!="" && curr_product_id){
				 return supplier_names;
				}
				else 
				{
					return supplier_names;
				}
			
		}
		
		
	/* ******************** Supplier Name button model: end ************************************  */  
	
	
	/* ******************** Federal Type model: start ************************************  */  
	
		
		window.operateEventsFederalType = {
			'change .federal_type_select': function (e, value, row, index) {
				var curr_product_id = row.product_id;
				var curr_federal_type=row.federal_type;
				var curr_ft_id="#federal_type_"+row.product_id+" :selected";
				var curr_ft_div="#federal_type_div_"+row.product_id;
				var selected_federal_type=$(curr_ft_id).text();
				var mktgProd_type_all="";
				
				if(!selected_federal_type || selected_federal_type== null && selected_federal_type=="")
				 {
					var output = [];
					output.push('<option value="">Please select...</option>');
					//$('#tfa_mktg_prod_type').html(output.join('')); 
				 }
				 else 
				 {
				    var federal_mktg_prod_list_js=<?php echo json_encode($federal_mktg_prod_list); ?>;
					var selected_mktg_list=federal_mktg_prod_list_js[selected_federal_type];
					var output = [];
					output.push('<option value="">Please select...</option>');
					
					$.each(selected_mktg_list, function(key, value)
					{
					  output.push('<option value="'+ value +'">'+ value +'</option>');
					});
				 }	
					
				
                    var mktgProd_type_all=output.join('');	
					var curr_mkpt_div_id="#mktgProd_type_"+row.product_id;
				
                    $(curr_mkpt_div_id).empty().append(mktgProd_type_all);					
					//mktgProd_type_names='<select class="form-select mktgProd_type_select" name="mktgProd_type_'+row.product_id+'" id="mktgProd_type_'+row.product_id+'"><option selected value="">--</option>'+mktgProd_type_all+'</select><div class="mktgProd_type_div" id="mktgProd_type_div_'+row.product_id+'"><div class="editable-buttons" style="padding-top:3px"><button type="button" style="margin-right:5px" class="btn btn-primary btn-sm mkpt_edit_btn" data-product="'+row.product_id+'"><i class="fa fa-check" aria-hidden="true"></i></button><button type="button" class="btn btn-primary btn-sm mkpt_cancel_btn" data-product="'+row.product_id+'"><i class="fa fa-times" aria-hidden="true"></i></button></div></div>';
				     
				   $(curr_ft_div).show();				
			},
			}
		
		
		
		function operateFormatterFederalType(value, row, index) {	
		
		        var is_admin='<?php echo ($isAdmin)?'Yes':'No';?>';
				if(is_admin=='Yes' || (row.finalized==0 || row.finalized=='0'))
				{									
					var curr_product_id = row.product_id;
					var curr_product_id = parseInt(curr_product_id); 
					var curr_federal_type = row.federal_type;
					var federal_type_names="";
					
					if(curr_federal_type && curr_federal_type!= null && curr_federal_type!="")
					{
						var selected_str="<option selected value='"+curr_federal_type+"'>"+curr_federal_type+"</option>";
						var replacement_str="<option value='"+curr_federal_type+"'>"+curr_federal_type+"</option>";
						var ft_with_selected=federal_type_all.replace(replacement_str,selected_str);
						if(ft_with_selected==federal_type_all)
						{
							ft_with_selected += selected_str;
						}
						
						federal_type_names='<select class="form-select federal_type_select" name="federal_type_'+row.product_id+'" id="federal_type_'+row.product_id+'">'+ft_with_selected+'</select><div  class="federal_type_div" id="federal_type_div_'+row.product_id+'"><div class="editable-buttons" style="padding-top:3px"><button type="button" style="margin-right:5px" class="btn btn-primary btn-sm ft_edit_btn" data-product="'+row.product_id+'"><i class="fa fa-check" aria-hidden="true"></i></button><button type="button" class="btn btn-primary btn-sm ft_cancel_btn" data-product="'+row.product_id+'"><i class="fa fa-times" aria-hidden="true"></i></button></div></div>';
						
					}
					else 
					{
						federal_type_names='<select class="form-select federal_type_select" name="federal_type_'+row.product_id+'" id="federal_type_'+row.product_id+'"><option selected value="">--</option>'+federal_type_all+'</select><div class="federal_type_div" id="federal_type_div_'+row.product_id+'"><div class="editable-buttons" style="padding-top:3px"><button type="button" style="margin-right:5px" class="btn btn-primary btn-sm ft_edit_btn" data-product="'+row.product_id+'"><i class="fa fa-check" aria-hidden="true"></i></button><button type="button" class="btn btn-primary btn-sm ft_cancel_btn" data-product="'+row.product_id+'"><i class="fa fa-times" aria-hidden="true"></i></button></div></div>';
					}
									
					if(curr_product_id!="" && curr_product_id){
					 return federal_type_names;
					}
					else 
					{
						return federal_type_names;
					}

				}
                else 
				{
					return value;
				}	
			
		}
		
		
	/* ******************** Federal Type button model: end ************************************  */  
	 
	 
	/* ******************** MKTG Prod Type model: start ************************************  */  
	
		
		window.operateEventsMktgProdType = {
			'change .mktgProd_type_select': function (e, value, row, index) {
				var curr_product_id = row.product_id;
				var curr_mktgProd_type=row.mktg_prod_type;
				var curr_mkpt_id="#mktgProd_type_"+row.product_id+" :selected";
				var curr_mkpt_div="#mktgProd_type_div_"+row.product_id;
				var selected_mkpt_name=$(curr_mkpt_id).text();
				$(curr_mkpt_div).show();				
			},
			}
		
		
		
		function operateFormatterMktgProdType(value, row, index) {	
		
		        var is_admin='<?php echo ($isAdmin)?'Yes':'No';?>';
				if(is_admin=='Yes' || (row.finalized==0 || row.finalized=='0'))
				{
				    var curr_product_id = row.product_id;
					var curr_product_id = parseInt(curr_product_id); 
					var curr_mktgProd_type = row.mktg_prod_type;
					var curr_federal_type = row.federal_type;
					
					var mktgProd_type_names="";
					
					if(curr_mktgProd_type && curr_mktgProd_type!= null && curr_mktgProd_type!="")
					{
						var federal_mktg_prod_list_js=<?php echo json_encode($federal_mktg_prod_list); ?>;
						var selected_mktg_list=federal_mktg_prod_list_js[curr_federal_type];
						var output = [];
						
						
						
						if(!curr_mktgProd_type || curr_mktgProd_type=="")
						{
							output.push("<option value=''>Please select...</option>");
						}
						else if($.inArray(curr_mktgProd_type, selected_mktg_list) == -1)
						{
						  // show the option which not existed
						  // make all array value in lower case and then check 
						}
						
						$.each(selected_mktg_list, function(key, value)
						{
						  output.push("<option value='"+ value +"'>"+ value +"</option>");
						});
						
						var mktgProd_type_all=output.join('');
						var selected_str="<option selected value='"+curr_mktgProd_type+"'>"+curr_mktgProd_type+"</option>";
						var replacement_str="<option value='"+curr_mktgProd_type+"'>"+curr_mktgProd_type+"</option>";
						var mkpt_with_selected=mktgProd_type_all.replace(replacement_str,selected_str);
						if(mkpt_with_selected==mktgProd_type_all)
						{
							mkpt_with_selected += selected_str;
						}
															
						mktgProd_type_names='<select class="form-select mktgProd_type_select" name="mktgProd_type_'+row.product_id+'" id="mktgProd_type_'+row.product_id+'">'+mkpt_with_selected+'</select><div  class="mktgProd_type_div" id="mktgProd_type_div_'+row.product_id+'"><div class="editable-buttons" style="padding-top:3px"><button type="button" style="margin-right:5px" class="btn btn-primary btn-sm mkpt_edit_btn" data-product="'+row.product_id+'"><i class="fa fa-check" aria-hidden="true"></i></button><button type="button" class="btn btn-primary btn-sm mkpt_cancel_btn" data-product="'+row.product_id+'"><i class="fa fa-times" aria-hidden="true"></i></button></div></div>';
					}
					else 
					{
						mktgProd_type_names='<select class="form-select mktgProd_type_select" name="mktgProd_type_'+row.product_id+'" id="mktgProd_type_'+row.product_id+'"><option selected value="">--</option>'+mktgProd_type_all+'</select><div class="mktgProd_type_div" id="mktgProd_type_div_'+row.product_id+'"><div class="editable-buttons" style="padding-top:3px"><button type="button" style="margin-right:5px" class="btn btn-primary btn-sm mkpt_edit_btn" data-product="'+row.product_id+'"><i class="fa fa-check" aria-hidden="true"></i></button><button type="button" class="btn btn-primary btn-sm mkpt_cancel_btn" data-product="'+row.product_id+'"><i class="fa fa-times" aria-hidden="true"></i></button></div></div>';
					}
									
					if(curr_product_id!="" && curr_product_id){
					 return mktgProd_type_names;
					}
					else 
					{
						return mktgProd_type_names;
					}

				}
				else 
				{
					return value;
				}
				
			
		}
		
		
	/* ******************** MKTG Prod Type button model: end ************************************  */  
	 
    /* ****** Vintage dropdown field showing : start ***** */ 
	
	function operateVintageItemFormatter(value, row, index) {
			var vin="";
			if(row.vintage=="0000")
				vin="NV";
			else 
				vin=row.vintage;
			
			return vin;
		}
		
   /* ****** Vintage dropdown field showing : end ***** */ 
   
   
	  function UnFinalizedFormatter(value, row, index) {
		  
		  if(checkArryValue(validProductList,value))
		  {
			  if(row.finalized==1)
		     {
				 return [	
                			
				'<button type="button" class="btn btn-sm btn-success">',
				'<i class="fa fa-check-circle-o" style="font-size:20px;color:black"></i></button>',
				
			  ].join('');
			 }
			 else 
			 {
				return ['<label><input data-index="'+index+'" name="btSelectItem" type="checkbox" value="'+row.product_id+'"><span></span></label>'].join(''); 
			 }
			  
		  }
		  else 
		  {
    return [	
                			
				'<button type="button" class="btn btn-showing-empty-fields btn-sm btn-warning" data-product="'+row.product_id+'">',
				'<i class=\"fa fa-exclamation-triangle\"></i>Empty Fields</button>',
				
			].join('')
		  }
		  
			
		}
			
  
	function UnFinalizedItemFormatter(value, row, index) {
		  
		  var c_item_id=row.item_id;
		  
          if(row.finalized==1)
		  {
			  return [	
                			
				'<button type="button" class="btn btn-sm btn-success">',
				'<i class="fa fa-check-circle-o" style="font-size:20px;color:black"></i></button>',
				
			  ].join('');
		  }			  
		  else if (err_item_wise[c_item_id])
			 {
              return [	
                			
				'<button type="button" class="btn btn-showing-empty-fields-items btn-sm btn-warning" data-item="'+row.item_id+'">',
				'<i class="fa fa-exclamation-triangle" style="font-size:20px;color:red"></i></button>',
				
			  ].join('');
		  }
		  else 
		  {
			  if(unchecked_item_wise[c_item_id] && unchecked_item_wise[c_item_id]==1)
			  {
				  return ['<label><input data-index="'+index+'" name="btSelectItemDetails" type="checkbox" value="'+row.item_id+'" ><span></span></label>'].join('');
			  }
			  else 
			  {
				  return ['<label><input data-index="'+index+'" name="btSelectItemDetails" type="checkbox" value="'+row.item_id+'" checked><span></span></label>'].join('');
			  } 
			  
		  }		  
		}
			
	window.ShowingEmptyItemoperateEvents = {
			'click .btn-showing-empty-fields-items': function (e, value, row, index) {	
					
			  var c_item_id=row.item_id;
			  if (err_item_wise[c_item_id])
			  {
				  alert(err_item_wise[c_item_id]);
			  }
			  
			},
			}		
			
		
		//item row buttons
		function operateItemFormatter(value, row, index) {
			var is_admin='<?php echo ($isAdmin)?'Yes':'No';?>';
			//console.log("item_details_info:"+row);	
			if(is_admin=='Yes')
			{
				return [
				'<button type="button" class="btn btn-delete-item btn-sm btn-danger" data-item="'+row.item_id+'">',
				'<i class=\"fa fa-trash\"></i>Delete</button>',
				'<button type="button" class="btn btn-dark btn-audit-item btn-sm bg_arrow_darkblue" data-item="'+row.item_id+'">',
				'<i class=\"fa fa-book\"></i>Audit</button>',
			     ].join('')
			}
			else if(row.finalized==0 || row.finalized=='0')
			{
				return [
				'<button type="button" class="btn btn-delete-item btn-sm btn-danger" data-item="'+row.item_id+'">',
				'<i class=\"fa fa-trash\"></i>Delete</button>',
			   ].join('')
			}
			else 
			{
				return [
				'<button type="button" class="btn btn-sm btn-danger" disabled>',
				'<i class=\"fa fa-trash\"></i>Delete</button>',
			   ].join('')
			}
						
		}
		
		
		
		
		
		//buttons
		function operateFormatter(value, row, index) {
				
			var delete_txt="";
			if(row.finalized==0 || row.finalized=='0')
						{
				delete_txt='<button type="button" class="btn btn-delete-product btn-sm btn-danger" data-product="'+row.product_id+'"><i class=\"fa fa-trash\"></i>Delete</button>';
						}
						
			return [
				'<button type="button" class="btn btn-outline-secondary prodimg_btn btn-sm bg_arrow_lightblue" data-product="'+row.product_id+'">',
				'Files <span class="badge badge-light">'+row.filecount+'</span></button>',
				'<button type="button" class="btn btn-primary btn-sm bg_arrow_blue btn_itemtoggle" data-toggle="collapse" data-index="'+index+'" data-target=".itemprod'+row.product_id+'" data-prod="'+row.product_id+'">',
				'Items <span class="badge badge-light">'+row.itemcount+'</span></button>',
				'<form id="editprod_'+row.product_id+'" method="POST" action="productsetup.php"><input type="hidden" id="product_id" name="product_id" value="'+row.product_id+'">',
				'<input type="hidden" id="product_desc" name="product_desc" value="'+row.product_desc+'">',
				'<input type="hidden"  name="product_finalized" value="'+row.finalized+'">',
				'<input type="hidden" id="product_mhw_code" name="product_mhw_code" value="'+row.product_mhw_code+'">',
				'<input type="hidden" id="brand_name" name="brand_name" value="'+row.brand_name+'">',
				'<input type=\"hidden\" name=\"edit\" value=\"1\">',
				'<input type="hidden" id="client_name" name="client_name" value="'+row.client_name+'">',
				'<input type="hidden" id="client_code" name="client_code" value="'+row.client_code+'">',
				'<button type="submit" class="btn btn-dark btn-sm prodedit_btn bg_arrow_darkblue" data-target="'+row.product_id+'">',
				'<i class=\"far fa-edit\"></i> Edit</button> </form>',
				<?php if ($isAdmin) { ?>
				'<button type="button" class="btn btn-delete-product btn-sm btn-danger" data-product="'+row.product_id+'">',
				'<i class=\"fa fa-trash\"></i>Delete</button>',
				'<button type="button" class="btn btn-dark btn-audit-product btn-sm bg_arrow_darkblue" data-product="'+row.product_id+'">',
				'<i class=\"fa fa-book\"></i>Audit</button>',
				<?php }
                      else 
					  {
						?>
						delete_txt
                      <?php 						
					  }
           				?>
				
				
			].join('')
		}
		
		
		 // this is for Drafts View 
		function operateFormatterNotFinalizedView(value, row, index) {
			return [
				'<button type="button" class="btn btn-outline-secondary prodimg_btn btn-sm bg_arrow_lightblue" data-product="'+row.product_id+'">',
				'Files <span class="badge badge-light">'+row.filecount+'</span></button>',
				'<button type="button" class="btn btn-primary btn-sm bg_arrow_blue btn_itemtoggle" data-toggle="collapse" data-index="'+index+'" data-target=".itemprod'+row.product_id+'" data-prod="'+row.product_id+'">',
				'Items <span class="badge badge-light">'+row.itemcount+'</span></button>',
				'<form id="editprod_'+row.product_id+'" method="POST" action="productsetup.php"><input type="hidden" id="product_id" name="product_id" value="'+row.product_id+'">',
				'<input type="hidden" id="product_desc" name="product_desc" value="'+row.product_desc+'">',
				'<input type="hidden"  name="product_finalized" value="'+row.finalized+'">',
				'<input type="hidden" id="product_mhw_code" name="product_mhw_code" value="'+row.product_mhw_code+'">',
				'<input type="hidden" id="brand_name" name="brand_name" value="'+row.brand_name+'">',
				'<input type="hidden" id="selector_position" name="selector_position" value="edit_supplier">',
				'<input type=\"hidden\" name=\"edit\" value=\"1\">',
				'<input type="hidden" id="client_name" name="client_name" value="'+row.client_name+'"> ',
				'<input type="hidden" id="client_code" name="client_code" value="'+row.client_code+'">',
				'<button type="submit" class="btn btn-dark btn-sm prodedit_btn bg_arrow_darkblue" data-target="'+row.product_id+'">',
				'<i class=\"far fa-edit\"></i> Edit</button></form>',
				'<button type="button" class="btn btn-delete-product btn-sm btn-danger" data-product="'+row.product_id+'">',
				'<i class=\"fa fa-trash\"></i>Delete</button>',
				<?php if ($isAdmin) { ?>
				
				'<button type="button" class="btn btn-dark btn-audit-product btn-sm bg_arrow_darkblue" data-product="'+row.product_id+'">',
				'<i class=\"fa fa-book\"></i>Audit</button>',
				<?php } ?>
			].join('')
		}
		
		function subtableBuilder ($el,qrytype, prodID) {
			 const tableSubColumns =
					[
					{
					  field: 'item_id',
						events: window.operateEvents,
						formatter: operateItemFormatter,
						visible: true
					},{
					  field: 'item_client_code',
					  title: 'Item Code (client)',
					  sortable: true,
					  align: 'left'
					},
					{
					  title: 'Item Code (MHW)',
					  field: 'item_mhw_code',
					  align: 'left',
					  valign: 'middle',
					  sortable: true  
					},
					{
					  title: 'Item Description',
					  field: 'item_description',
					  align: 'left',
					  valign: 'middle',
					 <?=$editableField?>
					  sortable: true  
					},
					{
					  title: 'Container Type',
					  field: 'container_type',
					  align: 'left',
					  valign: 'middle',
						<?php if ($isAdmin) { ?>
							editable: {
								type: 'select',
								source: containerTypeOptions,
								display: function(value, sourceData) {									     
										if (value) {
										  $(this).html(value);
										  }                                        
									},
								title: 'Select Container type',
							},
						<?php 
						}
						else 
						{
						 ?>
						editable: {
							type: 'select',
							source: containerTypeOptions,
							display: function(value, sourceData) {									     
										if (value) {
										  $(this).html(value);
										  }                                        
									},
							title: 'Select Container type',
							noeditFormatter: function(value, row) {
							if(row.finalized==0 || row.finalized=='0') {
							  return false
							}

							return value;
						  }
						},
						<?php  							  
						}
						?>
					  sortable: true  
					},
					{
					  title: 'Container Size',
					  field: 'container_size',
					  align: 'left',
					  valign: 'middle',
						<?php if ($isAdmin) { ?>
							editable: {
								type: 'select',
								source: containerSizeOptions,
								display: function(value, sourceData) {									     
										if (value) {
										  $(this).html(value);
										  }                                        
									},
								title: 'Select Container Size',
							},
						<?php 
						}
						else 
						{
						 ?>
						editable: {
							type: 'select',
							source: containerSizeOptions,
							display: function(value, sourceData) {									     
										if (value) {
										  $(this).html(value);
										  }                                        
									},
							title: 'Select Container Size',
							noeditFormatter: function(value, row) {
							if(row.finalized==0 || row.finalized=='0') {
							  return false
							}

							return value;
						  }
						},
						<?php  							  
						}
						?>
					  sortable: true  
					},
					{
					  title: 'Stock UOM',
					  field: 'stock_uom',
					  align: 'left',
					  valign: 'middle',
					  <?php if ($isAdmin) { ?>
						editable: {
							type: 'select',
							source: stockUomOptions,
							display: function(value, sourceData) {									     
										if (value) {
										  $(this).html(value);
										  }                                        
									},
							title: 'Select Stock UOM',
						},
					  <?php 
						}
						else 
						{
						 ?>
						editable: {
							type: 'select',
							source: stockUomOptions,
							display: function(value, sourceData) {									     
										if (value) {
										  $(this).html(value);
										  }                                        
									},
							title: 'Select Stock UOM',
							noeditFormatter: function(value, row) {
							if(row.finalized==0 || row.finalized=='0') {
							  return false
							}

							return value;
						  }
						},
						<?php  							  
						}
						?>
					  sortable: true  
					},
					{
					  title: 'Bottles Per Case',
					  field: 'bottles_per_case',
					  align: 'left',
					  valign: 'middle',
					  <?=$editableField_v2?>
					  sortable: true
					},
					{
					  title: 'UPC',
					  field: 'upc',
					  align: 'left',
					  valign: 'middle',
					  <?=$editableField_v2?>
					  sortable: true  
					},
					{
					  title: 'SCC',
					  field: 'scc',
					  align: 'left',
					  valign: 'middle',
					  <?=$editableField_v2?>
					  sortable: true  
					},
					{
					  title: 'Height',
					  field: 'height',
					  align: 'left',
					  valign: 'middle',
					  <?=$editableField_v2?>
					  sortable: true  
					},
					{
					  title: 'Length',
					  field: 'length',
					  align: 'left',
					  valign: 'middle',
					  <?=$editableField_v2?>
					  sortable: true  
					},
					{
					  title: 'Width',
					  field: 'width',
					  align: 'left',
					  valign: 'middle',
					  <?=$editableField_v2?>
					  sortable: true  
					},
					{
						  title: 'Case/Unit Dimensions',
						  field: 'unit_dimensions',
						  align: 'center',
						  valign: 'middle',
						  <?php if ($isAdmin) { ?>
						  editable: {
							type: 'select',
							source: unitDimensionsOptions,
							display: function(value, sourceData) {									     
										if (value) {
										  $(this).html(value);
										  }                                        
									},
							title: 'Select Case/Unit Dimensions',
						    },
						  <?php 
							}
							else 
							{
							 ?>
							editable: {
								type: 'select',
								source: unitDimensionsOptions,
								display: function(value, sourceData) {									     
										if (value) {
										  $(this).html(value);
										  }                                        
									},
								title: 'Select Case/Unit Dimensions',
								noeditFormatter: function(value, row) {
								if(row.finalized==0 || row.finalized=='0') {
								  return false
								}

								return value;
							  }
							},
							<?php  							  
							}
							?>
						  sortable: true  
					},
					{
					  title: 'Weight',
					  field: 'weight',
					  align: 'left',
					  valign: 'middle',
					  <?=$editableField_v2?>
					  sortable: true  
					},
											{
						  title: 'Weight UOM',
						  field: 'weight_uom',
						  align: 'center',
						  valign: 'middle',
						  <?php if ($isAdmin) { ?>
						  editable: {
							type: 'select',
							source: weightUomOptions,
							display: function(value, sourceData) {									     
										if (value) {
										  $(this).html(value);
										  }                                        
									},
							title: 'Select Weight UOM',
						    },
						  <?php 
							}
							else 
							{
							 ?>
							editable: {
								type: 'select',
								source: weightUomOptions,
								display: function(value, sourceData) {									     
										if (value) {
										  $(this).html(value);
										  }                                        
									},
								title: 'Select Weight UOM',
								noeditFormatter: function(value, row) {
								if(row.finalized==0 || row.finalized=='0') {
								  return false
								}

								return value;
							  }
							},
							<?php  							  
							}
							?>
						  sortable: true  
					},
					{
					  title: 'Vintage',
					  field: 'vintage',
					  align: 'left',
					  valign: 'middle',
                     <?php if ($isAdmin) { ?>
						editable: {
							type: 'select',
							source: vintageOptions,
							display: function(value, sourceData) {									     
										if (value) {
										  $(this).html(value);
										  }                                        
									},
							title: 'Select Vintage',
						},
						 
					  <?php 
					  }					  
					  else 
						{
						 ?>
						editable: {
							type: 'select',
							source: vintageOptions,
							display: function(value, sourceData) {									     
										if (value) {
										  $(this).html(value);
										  }                                        
									},
							title: 'Select Vintage',
							noeditFormatter: function(value, row) {
							if(row.finalized==0 || row.finalized=='0') {
							  return false
							}							
							return value;
						  }
						},
						<?php  							  
						}
						?>
					  sortable: true					  
					},
					{
					  title: 'Chill Storage',
					  field: 'chill_storage',
					  align: 'left',
					  valign: 'middle',
					  <?=$editableField_v2?>
					  sortable: true  
					},
					{
						  title: 'Item Class',
						  field: 'item_class',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField_v2?>
						  sortable: true  
					},						
					 {
					  title: 'Created Date',
					  field: 'item_creation_date',
					  align: 'left',
					  valign: 'middle',
					  sortable: true  
					},
					{
					  title: 'Created User',
					  field: 'item_created_by',
					  align: 'left',
					  valign: 'middle',
					  sortable: true  
					}
				];
				
			var data;
			$el.bootstrapTable('showLoading');
			
			$el.data('editable-url', '/forms/update-inline.php?table=mhw_app_prod_item&product_id='+prodID);
			$el.data('id-field', 'item_id');
			$.fn.editable.defaults.emptytext = '...';

			$.post('query-request.php', {
				qrytype: qrytype,
				prodID: prodID
			}, function(dataIT) {

				data = JSON.parse(dataIT);
				
	/* ********************************** Validating Item Data : Start ******************************* */	
	
			   var viewname_raw = $("#screenview_name").val();
				if(viewname_raw=="Drafts") { 
				
							tableSubColumns.unshift(
									{
									 field: 'item_id',
									 align: 'center',
									 valign: 'middle',
									 events: window.ShowingEmptyItemoperateEvents,
									 formatter: UnFinalizedItemFormatter,
									 visible: true
									}
								);
				
							   $.each( data, function( item_key, item_value ) {
							  var err_item="";
							  
							  var curr_item_id=getJsonValue(item_value,'item_id');
							  var curr_item_code=getJsonValue(item_value,'item_client_code');
							  var item_federal_typename=getJsonValue(item_value,'federal_type');
							  
							  
								$.each(required_item_fields, function (k, val) {
									var field_val=getJsonValue(item_value,k);
										
									if(val==1 && k=='vintage' && item_federal_typename=='Wine' && !field_val)
									{
										err_item=err_item+' '+k+' ,';
									}
                                    else if(val==1 && k!='vintage' && !field_val)
									{
										err_item=err_item+' '+k+' ,';
									}
										 
									});
									
									
									
									if(err_item.length>1)
										{
											err_item = err_item.slice(0, -1);
											err_item="Empty fields for Item id : "+curr_item_id+" & Item code : "+curr_item_code+" are : "+err_item;
											err_item_wise[curr_item_id]=err_item;
										}
										else 
										{
										  if (err_item_wise[curr_item_id])
										  {
											err_item_wise.splice(curr_item_id, 1);
										  }	
										}										
						});

				}
						
						
    /* ********************************** Validating Item Data : End ******************************* */		
				

				//item sub table 
				$el.bootstrapTable('destroy').bootstrapTable('showLoading').bootstrapTable({
					data: data,
					formatLoadingMessage: function () {
						return '<span class="mhwLoading"><img src="css/img/Logo_Chevron_Animated.gif"/> Loading</span>';
					},
					columns: [tableSubColumns]
				}); //sub table init
			}); //POST
		}
		function tableBuilder (qrytype) {
			var data;
			var clientlist = $("#clientlist").val();
			$('#prodTable').bootstrapTable('showLoading');
			$.post('query-request.php', {qrytype:qrytype,clientlist:clientlist}, function(dataPT){ // ajax request query-request.php, send qrytype & client as post variable, return dataX variable, parse into JSON

				data = JSON.parse(dataPT);

				var winH = $(window).height();
				var navH = $(".navbar").height();
				var tblH = (winH-navH)-50;

				if(qrytype=="VPitems"){
					//product/item views

					$('#prodTable').data('editable-url', '/forms/update-inline.php?table=mhw_app_prod_item');
					$('#prodTable').data('id-field', 'item_id');

					$('#prodTable').bootstrapTable('destroy').bootstrapTable('showLoading').bootstrapTable({
						data: data,
						height:tblH,
						stickyHeader:true,
						stickyHeaderOffsetY:60,
						formatLoadingMessage: function () {
							return  '<span class="mhwLoading"><img src="css/img/Logo_Chevron_Animated.gif"/> Loading</span>';
						},
						//fixedColumns:true,
						//fixedNumber:7,
						columns: [
						[{
						  field: 'selected',
						  checkbox: true,
						  rowspan: 2,
						  align: 'center',
						  valign: 'middle'
						},
						{
						  field: 'item_id',
						  rowspan: 2,
						  align: 'center',
						  events: window.operateEvents,
						  formatter: operateItemFormatter
						},
						{
						  field: 'client_name',
						  title: 'Client',
						  rowspan: 2,
						  align: 'center',
						  valign: 'middle',
						  visible: false
						},
						{
						  title: 'Product Detail',
						  colspan: 3,
						  align: 'center'
						},
						{
						  title: 'Item Detail',
						  colspan: 20,
						  align: 'center'
						}], 
						[{
						  field: 'brand_name',
						  title: 'Brand Name',
						  align: 'center',
						  valign: 'middle',
						  'id-field': 'product_id',
						  <?=$editableField_v2?>
						  sortable: true 
						}, 
						{
						  title: 'Product Code (MHW)',
						  field: 'product_mhw_code',
						  align: 'center',
						  valign: 'middle',
						  sortable: true  
						},
						{
						  field: 'product_desc',
						  title: 'Product Description',
						  'id-field': 'product_id',
						  <?=$editableField_v2?>
						  sortable: true,
						  align: 'center'
						},
						{
						  field: 'item_client_code',
						  title: 'Item Code (client)',
						  sortable: true,
						  align: 'center'
						},
						{
						  title: 'Item Code (MHW)',
						  field: 'item_mhw_code',
						  align: 'center',
						  valign: 'middle',
						  sortable: true  
						},
						{
						  title: 'Item Description',
						  field: 'item_description',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField?>
						  sortable: true  
						},
						{
						  title: 'Container Type',
						  field: 'container_type',
							align: 'left',
						  valign: 'middle',
							<?php if ($isAdmin) { ?>
								editable: {
									type: 'select',
									source: containerTypeOptions,
									display: function(value, sourceData) {									     
										if (value) {
										  $(this).html(value);
										  }                                        
									},
									title: 'Select Container type',
								},
							<?php 
							}
							else 
							{
							 ?>
							editable: {
								type: 'select',
								source: containerTypeOptions,
								display: function(value, sourceData) {									     
										if (value) {
										  $(this).html(value);
										  }                                        
									},
								title: 'Select Container type',
								noeditFormatter: function(value, row) {
								if(row.finalized==0 || row.finalized=='0') {
								  return false
								}

								return value;
							  }
							},
							<?php  							  
							}
							?>
						  sortable: true  
						},
						{
						  title: 'Container Size',
						  field: 'container_size',
							align: 'left',
						  valign: 'middle',
							<?php if ($isAdmin) { ?>
								editable: {
									type: 'select',
									source: containerSizeOptions,
									display: function(value, sourceData) {									     
										if (value) {
										  $(this).html(value);
										  }                                        
									},
									title: 'Select Container Size',
								},
							<?php 
							}
							else 
							{
							 ?>
							editable: {
								type: 'select',
								source: containerSizeOptions,
								display: function(value, sourceData) {									     
										if (value) {
										  $(this).html(value);
										  }                                        
									},
								title: 'Select Container Size',
								noeditFormatter: function(value, row) {
								if(row.finalized==0 || row.finalized=='0') {
								  return false
								}

								return value;
							  }
							},
							<?php  							  
							}
							?>
						  sortable: true  
						},
						{
						  title: 'Stock UOM',
						  field: 'stock_uom',
							align: 'left',
						  valign: 'middle',
							<?php if ($isAdmin) { ?>
								editable: {
									type: 'select',
									source: stockUomOptions,
									display: function(value, sourceData) {									     
										if (value) {
										  $(this).html(value);
										  }                                        
									},
									title: 'Select Stock UOM',
								},
							<?php 
							}
							else 
							{
							 ?>
							editable: {
								type: 'select',
								source: stockUomOptions,
								display: function(value, sourceData) {									     
										if (value) {
										  $(this).html(value);
										  }                                        
									},
								title: 'Select Stock UOM',
								noeditFormatter: function(value, row) {
								if(row.finalized==0 || row.finalized=='0') {
								  return false
								}

								return value;
							  }
							},
							<?php  							  
							}
							?>
						  sortable: true  
						},
						{
						  title: 'Bottles Per Case',
						  field: 'bottles_per_case',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField_v2?>
						  sortable: true
						},
						{
						  title: 'UPC',
						  field: 'upc',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField_v2?>
						  sortable: true  
						},
						{
						  title: 'SCC',
						  field: 'scc',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField_v2?>
						  sortable: true  
						},
						{
						  title: 'Height',
						  field: 'height',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField_v2?>
						  sortable: true  
						},
						{
						  title: 'Length',
						  field: 'length',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField_v2?>
						  sortable: true  
						},
						{
						  title: 'Width',
						  field: 'width',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField_v2?>
						  sortable: true  
						},
												{
						  title: 'Case/Unit Dimensions',
						  field: 'unit_dimensions',
						  align: 'center',
						  valign: 'middle',
						  <?php if ($isAdmin) { ?>
						  editable: {
							type: 'select',
							source: unitDimensionsOptions,
							display: function(value, sourceData) {									     
										if (value) {
										  $(this).html(value);
										  }                                        
									},
							title: 'Select Case/Unit Dimensions',
						    },
						  <?php 
							}
							else 
							{
							 ?>
							editable: {
								type: 'select',
								source: unitDimensionsOptions,
								display: function(value, sourceData) {									     
										if (value) {
										  $(this).html(value);
										  }                                        
									},
								title: 'Select Case/Unit Dimensions',
								noeditFormatter: function(value, row) {
								if(row.finalized==0 || row.finalized=='0') {
								  return false
								}

								return value;
							  }
							},
							<?php  							  
							}
							?>
						  sortable: true  
						},
						{
						  title: 'Weight',
						  field: 'weight',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField_v2?>
						  sortable: true  
						},
												{
						  title: 'Weight UOM',
						  field: 'weight_uom',
						  align: 'center',
						  valign: 'middle',
						  <?php if ($isAdmin) { ?>
						  editable: {
							type: 'select',
							source: weightUomOptions,
							display: function(value, sourceData) {									     
										if (value) {
										  $(this).html(value);
										  }                                        
									},
							title: 'Select Weight UOM',
						    },
						  <?php 
							}
							else 
							{
							 ?>
							editable: {
								type: 'select',
								source: weightUomOptions,
								display: function(value, sourceData) {									     
										if (value) {
										  $(this).html(value);
										  }                                        
									},
								title: 'Select Weight UOM',
								noeditFormatter: function(value, row) {
								if(row.finalized==0 || row.finalized=='0') {
								  return false
								}

								return value;
							  }
							},
							<?php  							  
							}
							?>
						  sortable: true  
						},
						{
						  title: 'Vintage',
						  field: 'vintage',
						  align: 'center',
						  valign: 'middle',
						  <?php if ($isAdmin) { ?>
						  editable: {
							type: 'select',
							source: vintageOptions,
							display: function(value, sourceData) {									     
										if (value) {
										  $(this).html(value);
										  }                                        
									},
							title: 'Select Vintage',
						    },
						  <?php 
							}
							else 
							{
							 ?>
							editable: {
								type: 'select',
								source: vintageOptions,
								display: function(value, sourceData) {									     
										if (value) {
										  $(this).html(value);
										  }                                        
									},
								title: 'Select Vintage',
								noeditFormatter: function(value, row) {
								if(row.finalized==0 || row.finalized=='0') {
								  return false
								}

								return value;
							  }
							},
							<?php  							  
							}
							?>
						  sortable: true  
						},
						{
						  title: 'Chill Storage',
						  field: 'chill_storage',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField_v2?>
						  sortable: true  
						},
					    {
						  title: 'Item Class',
						  field: 'item_class',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField_v2?>
						  sortable: true  
						},
						{
						  title: 'Created Date',
						  field: 'item_creation_date',
						  align: 'left',
						  valign: 'middle',
						  sortable: true  
						},
						{
						  title: 'Created User',
						  field: 'item_created_by',
						  align: 'left',
						  valign: 'middle',
						  sortable: true  
						}
						
						]
						
					]
					}); //table init
				} else {
					$('#prodTable').data('editable-url', '/forms/update-inline.php?table=mhw_app_prod');
					$('#prodTable').data('id-field', 'product_id');

					const productDetailsColumns = [
						{
						  field: 'brand_name',
						  title: 'Brand Name',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField_v2?>
						  sortable: true 
						},
						{
						  title: 'Product Code (MHW)',
						  field: 'product_mhw_code',
						  align: 'center',
						  valign: 'middle',
						  sortable: true  
						},
						{
						  field: 'product_desc',
						  title: 'Product Description',
						  <?=$editableField_v2?>
						  sortable: true,
						  align: 'center'
						},
						{
						  field: 'TTB_ID',
						  title: 'TTB',
						  <?=$editableField_v2?>
						  sortable: true,
						  align: 'center'
						},
						{
						  title: 'Federal Type',
						  field: 'federal_type',
						  align: 'center',
						  valign: 'middle',
						  events: window.operateEventsFederalType,
						  formatter: operateFormatterFederalType, 
						  sortable: true 						  
						},
						{
						  title: 'Compliance Type',
						  field: 'compliance_type',
						  align: 'center',
						  valign: 'middle',
						  <?php if ($isAdmin) { ?>
							editable: {
								type: 'select',
								source: complianceTypeOptions,
								display: function(value, sourceData) {									     
										if (value) {
										  $(this).html(value);
										  }                                        
									},	
								title: 'Select Compliance type'
							},
						  <?php 
						  }
                          else 
						  {
							 ?>
							editable: {
								type: 'select',
								source: complianceTypeOptions,
								title: 'Select Compliance type',
								display: function(value, sourceData) {									     
										if (value) {
										  $(this).html(value);
										  }                                        
									},
								noeditFormatter: function(value, row) {
								if(row.finalized==0 || row.finalized=='0') {
								  return false
								}

								return value;
							  }
							},
						  <?php  							  
						  }
						  ?>
						  sortable: true  
						},
						{
						  title: 'Product Class',
						  field: 'product_class',
						  align: 'center',
						  valign: 'middle',
						  <?php if ($isAdmin) { ?>
							editable: {
								type: 'select',
								source: productClassOptions,
								display: function(value, sourceData) {									     
										if (value) {
										  $(this).html(value);
										  }                                        
									},	
								title: 'Select Product Class',
							},
						 <?php 
						  }
                          else 
						  {
							 ?>
							editable: {
								type: 'select',
								source: productClassOptions,
								display: function(value, sourceData) {									     
										if (value) {
										  $(this).html(value);
										  }                                        
									},	
								title: 'Select Product Class',
								noeditFormatter: function(value, row) {
								if(row.finalized==0 || row.finalized=='0') {
								  return false
								}

								return value;
							  }
							},
						  <?php  							  
						  }
						  ?>
						  sortable: true  
						},
						{
						  title: 'Mktg Product Type',
						  field: 'mktg_prod_type',
						  align: 'center',
						  valign: 'middle',
						  events: window.operateEventsMktgProdType,
						  formatter: operateFormatterMktgProdType, 
						  sortable: true 
						},
						{
						  title: 'Beverage Type',
						  field: 'bev_type',
						  align: 'center',
						  valign: 'middle',
						  <?php if ($isAdmin) { ?>
							editable: {
								type: 'select',
								source: beverageTypeOptions,
								display: function(value, sourceData) {									     
										if (value) {
										  $(this).html(value);
										  }                                        
									},	
								title: 'Select Beverage Type',
							},
						  <?php 
						  } 
						   else 
						  {
							 ?>
							editable: {
								type: 'select',
								source: beverageTypeOptions,
								display: function(value, sourceData) {									     
										if (value) {
										  $(this).html(value);
										  }                                        
									},	
								title: 'Select Beverage Type',
								noeditFormatter: function(value, row) {
								if(row.finalized==0 || row.finalized=='0') {
								  return false
								}

								return value;
							  }
							},
						  <?php  							  
						  }
						  ?>

						  sortable: true  
						},
						{
						  title: 'Fanciful',
						  field: 'fanciful',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField_v2?>
						  sortable: true,
						  visible: false
						},
						{
						  title: 'Country',
						  field: 'country',
						  align: 'center',
						  valign: 'middle',
						  <?php if ($isAdmin) { ?>
							editable: {
								type: 'select',
								source: countries,	
								title: 'Select Country',
							},
						  <?php 
						  }
						   else 
						  {
							 ?>
							editable: {
								type: 'select',
								source: countries,	
								title: 'Select Country',
								noeditFormatter: function(value, row) {
								if(row.finalized==0 || row.finalized=='0') {
								  return false
								}

								return value;
							  }
							},
						  <?php  							  
						  }
						  ?>
						  sortable: true  
						},
						{
						  title: 'Appellation',
						  field: 'appellation',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField_v2?>
						  sortable: true  
						},
						{
						  title: 'Lot Item',
						  field: 'lot_item',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField_v2?>
						  sortable: true  
						},
						/*{
						  title: 'Bottle Material',
						  field: 'bottle_material',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField?>
						  sortable: true  
						},*/
						{
						  title: 'Alcohol %',
						  field: 'alcohol_pct',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField_v2?>
						  sortable: true  
						},
						{
						  title: 'Created Date',
						  field: 'product_creation_date',
						  align: 'center',
						  valign: 'middle',
						  sortable: true  
						},
						{
						  title: 'Created User',
						  field: 'product_created_by',
						  align: 'center',
						  valign: 'middle',
						  sortable: true  
						}
						
					];
					if (qrytype=="VPunfinalized" || qrytype=="VPfinalized") {
						productDetailsColumns.unshift(
							{
								field: 'rejremark',
								title: 'MHW Review',
								align: 'center',
								valign: 'middle',
						  sortable: true  
							}
						);
					}
					const supplierDetailsColumns = [
						{
						  title: 'Supplier Name',
						  field: 'supplier_name',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField_v2?>
						  sortable: true  
						},
						{
						  field: 'supplier_contact',
						  title: 'Contact',
						  <?=$editableField_v2?>
						  sortable: true,
						  align: 'center'
						},
						{
						  field: 'supplier_fda_number',
						  title: 'FDA #',
						  <?=$editableField_v2?>
						  sortable: true,
						  align: 'center'
						},
						{
						  field: 'tax_reduction_allocation',
						  title: 'Tax Reduction Allocation',
						  <?=$editableField_v2?>
						  sortable: true,
						  align: 'center'
						},
						{
						  field: 'supplier_address_1',
						  title: 'Address 1',
						  <?=$editableField_v2?>
						  sortable: true,
						  align: 'center',
						  visible:false
						},
						{
						  field: 'supplier_address_2',
						  title: 'Address 2',
						  <?=$editableField_v2?>
						  sortable: true,
						  align: 'center',
						  visible:false
						},
						{
						  field: 'supplier_address_3',
						  title: 'Address 3',
						  <?=$editableField_v2?>
						  sortable: true,
						  align: 'center',
						  visible:false
						},
						{
						  field: 'supplier_city',
						  title: 'City',
						  <?=$editableField_v2?>
						  sortable: true,
						  align: 'center',
						  visible:false
						},
						{
						  field: 'supplier_state',
						  title: 'State',
						  <?=$editableField_v2?>
						  sortable: true,
						  align: 'center',
						  visible:false
						},
						{
						  field: 'supplier_country',
						  title: 'Country',
						  <?php if ($isAdmin) { ?>
							editable: {
								type: 'select',
								source: countries,	
								title: 'Select Country',
							},
						  <?php 
						  } 
						  else 
						  {
							 ?>
							editable: {
								type: 'select',
								source: countries,	
								title: 'Select Country',
								noeditFormatter: function(value, row) {
								if(row.finalized==0 || row.finalized=='0') {
								  return false
								}

								return value;
							  }
							},
						  <?php  							  
						  }
						  ?>
						  sortable: true,
						  align: 'center',
						  visible:false
						},
						{
						  field: 'supplier_zip',
						  title: 'Zip',
						  <?=$editableField_v2?>
						  sortable: true,
						  align: 'center',
						  visible:false
						},
						{
						  field: 'supplier_phone',
						  title: 'Phone',
						  <?=$editableField_v2?>
						  sortable: true,
						  align: 'center',
						  visible:false
						},
						{
						  field: 'supplier_email',
						  title: 'Email',
						  <?=$editableField_v2?>
						  sortable: true,
						  align: 'center',
						  visible:false
						},
						{
						  field: 'federal_basic_permit',
						  title: 'Federal Basic Permit',
						  <?=$editableField_v2?>
						  sortable: true,
						  align: 'center',
						  visible:false
				}
					];
					
					
		if(qrytype=="VPunfinalized")
					{
						

                const unfinalized_supplierDetailsColumns = [
				        {
						  title: 'Supplier Name',
						  field: 'supplier_name',
						  align: 'center',
						  valign: 'middle',
						  events: window.operateEventsSupplierName,
						  formatter: operateFormatterSupplierName
						},
						{
						  field: 'supplier_contact',
						  title: 'Contact',
						  <?=$editableField_v2?>
						  sortable: true,
						  align: 'center'
						},
						{
						  field: 'supplier_fda_number',
						  title: 'FDA #',
						  <?=$editableField_v2?>
						  sortable: true,
						  align: 'center'
						},
						{
						  field: 'tax_reduction_allocation',
						  title: 'Tax Reduction Allocation',
						  <?=$editableField_v2?>
						  sortable: true,
						  align: 'center'
						},
						{
						  field: 'supplier_address_1',
						  title: 'Address 1',
						  <?=$editableField_v2?>
						  sortable: true,
						  align: 'center',
						  visible:false
						},
						{
						  field: 'supplier_address_2',
						  title: 'Address 2',
						  <?=$editableField_v2?>
						  sortable: true,
						  align: 'center',
						  visible:false
						},
						{
						  field: 'supplier_address_3',
						  title: 'Address 3',
						  <?=$editableField_v2?>
						  sortable: true,
						  align: 'center',
						  visible:false
						},
						{
						  field: 'supplier_city',
						  title: 'City',
						  <?=$editableField_v2?>
						  sortable: true,
						  align: 'center',
						  visible:false
						},
						{
						  field: 'supplier_state',
						  title: 'State',
						  <?=$editableField_v2?>
						  sortable: true,
						  align: 'center',
						  visible:false
						},
						{
						  field: 'supplier_country',
						  title: 'Country',
						  <?php if ($isAdmin) { ?>
							editable: {
								type: 'select',
								source: countries,	
								title: 'Select Country',
							},
						  <?php 
						  }
						  else 
						  {
							 ?>
							editable: {
								type: 'select',
								source: countries,	
								title: 'Select Country',
								noeditFormatter: function(value, row) {
								if(row.finalized==0 || row.finalized=='0') {
								  return false
								}

								return value;
							  }
							},
						  <?php  							  
						  }
						  ?>
						  sortable: true,
						  align: 'center',
						  visible:false
						},
						{
						  field: 'supplier_zip',
						  title: 'Zip',
						  <?=$editableField_v2?>
						  sortable: true,
						  align: 'center',
						  visible:false
						},
						{
						  field: 'supplier_phone',
						  title: 'Phone',
						  <?=$editableField_v2?>
						  sortable: true,
						  align: 'center',
						  visible:false
						},
						{
						  field: 'supplier_email',
						  title: 'Email',
						  <?=$editableField_v2?>
						  sortable: true,
						  align: 'center',
						  visible:false
						},
						{
						  field: 'federal_basic_permit',
						  title: 'Federal Basic Permit',
						  <?=$editableField_v2?>
						  sortable: true,
						  align: 'center',
						  visible:false
				}
					];			
						
               var tableColumns = [
						[{	  
						  field: 'product_id',
						  rowspan: 2,
						  align: 'center',
						  valign: 'middle',
						  events: window.ShowingEmptyoperateEvents,
						  formatter: UnFinalizedFormatter,
						  visible: true
						},
						{
						  field: 'product_id',
						  rowspan: 2,
						  align: 'center',
						  events: window.operateEvents,
						  formatter: operateFormatterNotFinalizedView
						}, 
						{
						  field: 'client_name',
						  title: 'Client',
						  rowspan: 2,
						  align: 'center',
						  valign: 'middle',
						  visible: false
						},
						{
						  field: 'product_id,',
						  rowspan: 2,
						  align: 'center',
						  valign: 'middle',
						  events: window.operateEventsImageUpload,
						  formatter: operateFormatterImageUpload
						},
						{
						  title: 'Product Detail',
						  colspan: productDetailsColumns.length-1,
						  align: 'center'
						},
						{
						  title: 'Supplier Detail',
						  colspan: unfinalized_supplierDetailsColumns.length,
						  align: 'center'
						}],
						[...productDetailsColumns, ...unfinalized_supplierDetailsColumns]
					];

						
					}
					else 
					{
					var tableColumns = [
						[{
						  field: 'selected',
						  checkbox: true,
						  rowspan: 2,
						  align: 'center',
						  valign: 'middle'
						},
						{
						  field: 'product_id',
						  rowspan: 2,
						  align: 'center',
						  events: window.operateEvents,
						  formatter: operateFormatter
						}, 
						{
						  field: 'client_name',
						  title: 'Client',
						  rowspan: 2,
						  align: 'center',
						  valign: 'middle',
						  visible: false
						},
						{
						  title: 'Product Detail',
						  colspan: productDetailsColumns.length-1,
						  align: 'center'
						},
						{
						  title: 'Supplier Detail',
						  colspan: supplierDetailsColumns.length,
						  align: 'center'
						}],
						[...productDetailsColumns, ...supplierDetailsColumns]
					];
					}
					
					//product/supplier views
					$('#prodTable').bootstrapTable('destroy').bootstrapTable('showLoading').bootstrapTable({
						data: data,
						height:tblH,
						stickyHeader:true,
						stickyHeaderOffsetY:60,
						formatLoadingMessage: function () {
							return '<span class="mhwLoading"><img src="css/img/Logo_Chevron_Animated.gif"/> Loading</span>';
						},
						detailView: true,
						onExpandRow: function (index, row, $detail) {
							expandTable($detail,row)
						},
						//fixedColumns:true,
						//fixedNumber:7,
						columns: tableColumns
					}); //table init
				}

				var viewname_raw = $("#screenview_name").val();
				if(viewname_raw=="Drafts") { 

					$table.bootstrapTable('checkAll');  //check all Drafts by default
				}

				modalStuff(); //bind modal & buttons

				$table.on('check.bs.table uncheck.bs.table ' +
				  'check-all.bs.table uncheck-all.bs.table',
				function () {
				  $remove.prop('disabled', !$table.bootstrapTable('getSelections').length)

				  // save your data, here just save the current page
				  selections = getIdSelections()
				  // push or splice the selections if you want to save all data selections

				})
				$table.on('all.bs.table', function (e, name, args) {
				  //console.log("developer email : jahid0604015@gmail.com");
				  //modalStuff(); //when anything happens (filter, click, etc) re-bind modal & buttons
				})
				$remove.click(function () {
				  var ids = getIdSelections()
				  $table.bootstrapTable('remove', {
					field: 'id',
					values: ids
				  })
				  $remove.prop('disabled', true)
				})
				
				
				
		
			}); //POST
			
			

		} //unnamed function wrapping table init

		var initialview = '<?php echo $v; ?>';
		var nf_view_show='<?php echo $nf_view_show; ?>';
		
		if(initialview=="nf" || (nf_view_show==999 || nf_view_show=="999")){
			tableBuilder("VPunfinalized"); //initialize table
			$("#screenview_name").val("Drafts");
			$("#finalize").show();
		}
		else{
			tableBuilder("VPdefault"); //initialize table
		}

		$("#screenview_name").on("change", function () {
			var viewname_raw =  $(this).val();
			$("#finalize").hide();
			if(viewname_raw=="Drafts") { 
				viewname = "VPunfinalized"; 
				$("#finalize").show();
			}
			else if(viewname_raw=="Processing") { viewname = "VPpending"; }
			else if(viewname_raw=="Finalized") { viewname = "VPfinalized"; }
			else if(viewname_raw=="Items") { viewname = "VPitems"; }
			else { viewname = "VPdefault"; }

			tableBuilder(viewname);
		});
		
		

		$("#finalize").on("click", function () {
			var ids = getIdSelections();
			var item_ids = getItemIdSelections();
			var codes = getCodeSelections();
			var qrytype = "processFinalize";
			var prodlist = JSON.stringify(ids);
			var selectedItemList = JSON.stringify(item_ids);
			var codelist = JSON.stringify(codes);
			var clientlist = $("#clientlist").val(); 
			var uncheckedItemList=getUncheckedItemList();
			var uncheckedItemIds = JSON.stringify(uncheckedItemList);
			
	   /* ************ showing prompt confirm for admin/super_admin : start  ************* */ 
		                  var is_admin='<?php echo ($isAdmin)?'Yes':'No';?>';
						  var confirm_txt="confirm_yes";
						  if(is_admin=="Yes")
						  {
							  confirm_txt="";
						  }
		
		/*  *********** showing prompt confirm for admin/super_admin : end  ************* */ 
			
			
			$.post('query-request.php', {
				qrytype: qrytype,prodlist: prodlist,selectedItemList:selectedItemList,uncheckedItemIds:uncheckedItemIds,des_chng_yes:confirm_txt}, function(dataPF){
				
				alertTray('alertFinalize',prodlist,codelist);

				$("#finalize").hide();
				$("#screenview_name").val("Processing");
				tableBuilder("VPpending");

				refreshAlert(); //viewproducts.php-specific function for updating nav (head.php) 
				
			});
		  	
		});
		
				

		
	});  //document ready
	</script>
	
  <div class="container-fluid">
	<div class="row justify-content-md-left float-left">
		<div class="col-md-auto oneField field-container-D" id="client_name-D">
			<label id="client_name-L" class="label preField " for="client_name"><b>Client Name</b></label>
			<div class="inputWrapper">
				<select id="client_name" name="client_name" title="Client Name" aria-required="true">
				<?php
				$clients = explode(";",$_SESSION['mhwltdphp_userclients']);
				if (count($clients)>1){  echo "<option value=\"all\" class=\"\">ALL</option>"; }
				foreach ($clients as &$clientvalue) {
					echo "<option value=\"".$clientvalue."\" class=\"\">".$clientvalue."</option>";
				}
				?>					
				</select>
			</div>
		</div>
		<div class="col-md-auto oneField field-container-D" id="screenview_name-D">
			<label id="screenview_name-L" class="label preField " for="screenview_name"><b>View</b></label>
			<div class="inputWrapper">
				<select id="screenview_name" name="screenview_name" title="Screen View" aria-required="true">
				<?php
				$screenviews = array("Products","Items","Drafts","Processing","Finalized");
				//if (count($screenviews)>1){  echo "<option value=\"all\" class=\"\">Default</option>"; }
				foreach ($screenviews as &$screenviewvalue) {
					echo "<option value=\"".$screenviewvalue."\" class=\"\">".$screenviewvalue."</option>";
				}
				?>					
				</select>
			</div>
		</div>
		<div class="col-md-auto align-self-center oneField field-container-D" id="finalize-D">
		<button id="finalize" class="btn btn-success"><i class="fas fa-lock"></i> Finalize</button>
		</div>
	</div>
    <table id="prodTable" class="table table-hover" 
		data-toggle="table" data-pagination="false" 
		data-show-pagination-switch="true"  data-show-columns="true" data-show-toggle="true" 
		data-search="true" data-show-export="true"
		data-id-field="" data-editable-emptytext="...." data-editable-url="#"
	>
	<!--<table
  id="table"
  data-toolbar="#toolbar"
  data-search="true"
  data-show-refresh="true"
  data-show-toggle="true"
  data-show-fullscreen="true"
  data-show-columns="true"
  data-detail-view="true"
  data-show-export="true"
  data-click-to-select="true"
  data-detail-formatter="detailFormatter"
  data-minimum-count-columns="2"
  data-show-pagination-switch="true"
  data-pagination="true"
  data-id-field="id"
  data-page-list="[10, 25, 50, 100, all]"
  data-show-footer="true"
  data-side-pagination="server"
  data-response-handler="responseHandler">-->

	  <?php
	  //	$clientlist =  "'".str_replace(";","','",$_SESSION['mhwltdphp_userclients'])."'";
		$clientlist =  "'".str_replace(";","','",$_SESSION['mhwltdphp_user_clients_codes'])."'";

	/*	echo "<thead><tr>";
		 echo "<th>&nbsp;<span style=\"display:none;\">".$clientlist."</span></th>";
		  echo ("<th>Client Name</th>
		  <th>Brand Name</th>
          <th>Product Code (MHW)</th>
          <th>Product Description</th>
		  <th>TTB ID</th>
		  <th>Federal Type</th>
		  <th>Compliance Type</th>
		  <th>Product Class</th>
		  <th>Marketing Product Type</th>
		  <th>Beverage Type</th>
		  <th>Fanciful</th>
		  <th>Country of Origin</th>
		  <th>Appellation</th>
		  <th>Lot Item</th>
		  <th>Bottle Material</th>
		  <th>Alcohol %</th>
		  
		  <th>Supplier</th>
		  <th>Contact</th>
          <th>FDA Number</th>
          <th>Tax Reduction Allocation</th>
          <th>Phone</th>
          <th>Email</th>
		  
        </tr></thead>
      <tbody>".PHP_EOL);
*/
/*
	  	//Establishes the connection
	    $conn = sqlsrv_connect($serverName, $connectionOptions);
	    $tsql= "SELECT p.[product_id]
		      ,p.[client_code]
		      ,p.[client_name]
		      ,p.[brand_name]
		      ,p.[product_desc]
		      ,p.[product_mhw_code]
		      ,p.[product_mhw_code_search]
		      ,p.[TTB_ID]
		      ,p.[federal_type]
		      ,p.[compliance_type]
		      ,p.[product_class]
		      ,p.[mktg_prod_type]
		      ,p.[bev_type]
		      ,p.[fanciful]
		      ,p.[country]
		      ,p.[appellation]
		      ,p.[lot_item]
		      ,p.[bottle_material]
		      ,p.[alcohol_pct]
		      ,p.[create_date]
			  ,s.[supplier_id]
		      ,s.[supplier_name]
		      ,s.[supplier_contact]
		      ,s.[supplier_fda_number]
		      ,s.[tax_reduction_allocation]
		      ,s.[supplier_address_1]
		      ,s.[supplier_address_2]
		      ,s.[supplier_address_3]
		      ,s.[supplier_city]
		      ,s.[supplier_state]
		      ,s.[supplier_country]
		      ,s.[supplier_zip]
		      ,s.[supplier_phone]
		      ,s.[supplier_email]
			  ,s.[federal_basic_permit]
		      ,s.[create_via]
		      ,s.[create_date]
		      ,s.[edit_date]
			  ,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_item] i WHERE i.[product_id] = p.[product_id] AND i.[active] = 1 AND i.[deleted] = 0) as 'itemcount'
			  ,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount'
	         FROM [dbo].[mhw_app_prod] p WITH (NOLOCK)
			 LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] s WITH (NOLOCK) on s.[supplier_id] = p.[supplier_id]
			 WHERE p.[client_name] IN (".$clientlist.") AND p.[active] = 1 AND p.[deleted] = 0 AND s.[active] = 1 AND s.[deleted] = 0 ORDER BY p.[create_date] desc";
			 
	    $getResults= sqlsrv_query($conn, $tsql);

	    if ($getResults == FALSE)
	        echo (sqlsrv_errors());
	*/		
	    while ($row = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
			$i_supplier=0; //reset supplier count at each product
			$i_item=0; //reset item count at each product
			
			echo "<tr>";
			//echo "<td><button type=\"button\" class=\"btn btn-primary prodimgbtn\" data-toggle=\"modal\" data-target=\"#fileModal\" data-product=\"".$row['product_id']."\">";
					echo "<td><button type=\"button\" class=\"btn btn-outline-secondary prodimg_btn btn-sm bg_arrow_lightblue\" data-product=\"".$row['product_id']."\">";
					echo ("Files <span class=\"badge badge-light\">".$row['filecount']."</span></button>
					<button type=\"button\" class=\"btn btn-primary btn-sm bg_arrow_blue btn_itemtoggle\" data-toggle=\"collapse\" data-target=\".itemprod".$row['product_id']."\" data-prod=\"".$row['product_id']."\">
					Items <span class=\"badge badge-light\">".$row['itemcount']."</span></button>");
					echo "<form id=\"editprod_".$row['product_id']."\" method=\"POST\" action=\"productsetup.php\"><input type=\"hidden\" id=\"product_id\" name=\"product_id\" value=\"".$row['product_id']."\"><input type=\"hidden\" id=\"product_desc\" name=\"product_desc\" value=\"".$row['product_desc']."\"><input type=\"hidden\" id=\"brand_name\" name=\"brand_name\" value=\"".$row['brand_name']."\"><input type=\"hidden\" id=\"client_name\" name=\"client_name\" value=\"".$row['client_name']."\"><input type=\"hidden\" name=\"edit\" value=\"1\"></form>";
					echo ("<button type=\"button\" class=\"btn btn-dark btn-sm prodedit_btn bg_arrow_darkblue\" data-target=\"".$row['product_id']."\">
					<i class=\"far fa-edit\"></i> Edit</button></td>");

	     	echo ("<td>".$row['client_name']."</td><td>".$row['brand_name']."</td><td>".$row['product_mhw_code']."</td><td>".$row['product_desc']."</td>
					<td>".$row['TTB_ID']."</td><td>".$row['federal_type']."</td><td>".$row['compliance_type']."</td><td>".$row['product_class']."</td>
					<td>".$row['mktg_prod_type']."</td><td>".$row['bev_type']."</td><td>".$row['fanciful']."</td><td>".$row['country']."</td><td>".$row['appellation']."</td>
					<td>".$row['lot_item']."</td><td>".$row['bottle_material']."</td><td>".$row['alcohol_pct']."</td>
					<td>".$row['supplier_name']."</td>
			        <td>".$row['supplier_contact']."</td>
			        <td>".$row['supplier_fda_number']."</td>
			        <td>".$row['tax_reduction_allocation']."</td>
			        <td>".$row['supplier_phone']."</td>
			        <td>".$row['supplier_email']."</td>
					<td>".$row['federal_basic_permit']."</td>");
			echo ("</tr>". PHP_EOL);		
		 
		 /*
		 //begin supplier sub-loop
		  $tsql_supplier= "SELECT [supplier_id]
		      ,[supplier_name]
		      ,[supplier_contact]
		      ,[supplier_fda_number]
		      ,[tax_reduction_allocation]
		      ,[supplier_address_1]
		      ,[supplier_address_2]
		      ,[supplier_address_3]
		      ,[supplier_city]
		      ,[supplier_state]
		      ,[supplier_country]
		      ,[supplier_zip]
		      ,[supplier_phone]
		      ,[supplier_email]
		      ,[create_via]
		      ,[create_date]
		      ,[edit_date]
	         FROM [dbo].[mhw_app_prod_supplier] WHERE [product_id] = '".$row['product_id']."' AND [active] = 1 AND [deleted] = 0 ORDER BY [create_date] desc";
	    $getResults_supplier= sqlsrv_query($conn, $tsql_supplier);

	    if ($getResults_supplier == FALSE)
	        echo (sqlsrv_errors());
			
	    		while ($row_supplier = sqlsrv_fetch_array($getResults_supplier, SQLSRV_FETCH_ASSOC)) {
				if($i_supplier==0){

						echo ("<tr class=\"table-secondary\"><td class=\"hiddenRow fakeTH\"><div class=\"collapse suppprod".$row['product_id']."\">&nbsp;</div></td>
						<td class=\"hiddenRow fakeTH\"><div class=\"collapse suppprod".$row['product_id']."\">Supplier</div></td>
			            <td class=\"hiddenRow fakeTH\"><div class=\"collapse suppprod".$row['product_id']."\">Contact</div></td>
			            <td class=\"hiddenRow fakeTH\"><div class=\"collapse suppprod".$row['product_id']."\">FDA Number</div></td>
			            <td class=\"hiddenRow fakeTH\"><div class=\"collapse suppprod".$row['product_id']."\">Tax Reduction Allocation</div></td>
			            <td class=\"hiddenRow fakeTH\"><div class=\"collapse suppprod".$row['product_id']."\">Phone</div></td>
			            <td class=\"hiddenRow fakeTH\"><div class=\"collapse suppprod".$row['product_id']."\">Email</div></td></tr>". PHP_EOL);
					
				}
					 echo ("<tr class=\"table-secondary\"><td class=\"hiddenRow\"><div class=\"collapse suppprod".$row['product_id']."\">&nbsp;</div></td>
					 	<td class=\"hiddenRow\"><div class=\"collapse suppprod".$row['product_id']."\">".$row_supplier['supplier_name']."</div></td>
			            <td class=\"hiddenRow\"><div class=\"collapse suppprod".$row['product_id']."\">".$row_supplier['supplier_contact']."</div></td>
			            <td class=\"hiddenRow\"><div class=\"collapse suppprod".$row['product_id']."\">".$row_supplier['supplier_fda_number']."</div></td>
			            <td class=\"hiddenRow\"><div class=\"collapse suppprod".$row['product_id']."\">".$row_supplier['tax_reduction_allocation']."</div></td>
			            <td class=\"hiddenRow\"><div class=\"collapse suppprod".$row['product_id']."\">".$row_supplier['supplier_phone']."</div></td>
			            <td class=\"hiddenRow\"><div class=\"collapse suppprod".$row['product_id']."\">".$row_supplier['supplier_email']."</div></td></tr>". PHP_EOL);
						

					   
					$i_supplier++;   
			} //end supplier sub-loop
//			echo ("</table></div></td></tr>");
			sqlsrv_free_stmt($getResults_supplier);
			*/
			
			//begin item sub-loop
		  $tsql_item = "SELECT [item_id]
		      ,[item_client_code]
		      ,[item_mhw_code]
		      ,[item_description]
		      ,[container_type]
		      ,[container_size]
		      ,[stock_uom]
		      ,[bottles_per_case]
		      ,[upc]
		      ,[scc]
			  ,[vintage]
			  ,[various_vintages]
		      ,[item_status]
		      ,[create_via]
		      ,[create_date]
		      ,[edit_date]
	         FROM [dbo].[mhw_app_prod_item] WHERE [product_id] = '".$row['product_id']."' AND [active] = 1 AND [deleted] = 0 ORDER BY [create_date] desc";
	    $getResults_item= sqlsrv_query($conn, $tsql_item);

	    if ($getResults_item == FALSE)
	        echo (sqlsrv_errors());
			
	    		while ($row_item = sqlsrv_fetch_array($getResults_item, SQLSRV_FETCH_ASSOC)) {
				if($i_item==0){
					/*
					echo ("<tr><td class=\"hiddenRow\" colspan=\"2\"><div class=\"collapse suppprod".$row['product_id']."\">&nbsp;</div></td>
						<td class=\"hiddenRow\" colspan=\"15\"><div class=\"collapse itemprod".$row['product_id']."\">
						<table><tr><th class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">&nbsp;</div></th>
						<th class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">Item Client Code</div></th>
			            <th class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">Item MHW Code</div></th>
			            <th class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">Item Description</div></th>
			            <th class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">Container Type</div></th>
			            <th class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">Container Size</div></th>
			            <th class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">Stock UOM</div></th>
						<th class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">Bottles Per Case</div></th>
						<th class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">UPC</div></th>
						<th class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">SCC</div></th>
						</tr>". PHP_EOL);
						*/
						echo ("<tr data-card-visibile=\"true\" class=\"table-info thitem_prod".$row['product_id']."\"><td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\"><span style=\"display:none;\">".$row['client_name']."</span></div></td>
						<td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">Item Client Code</div></td>
			            <td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">Item MHW Code</div></td>
			            <td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">Item Description</div></td>
			            <td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">Container Type</div></td>
			            <td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">Container Size</div></td>
			            <td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">Stock UOM</div></td>
						<td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">Bottles Per Case</div></td>
						<td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">UPC</div></td>
						<td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">SCC</div></td>
						<td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">Vintage</div></td>
						<td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">Various Vintages</div></td>
						</tr>". PHP_EOL);
					
				}
					 echo ("<tr data-card-visibile=\"true\" class=\"table-info tritem_prod".$row['product_id']."\"><td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\"><span style=\"display:none;\">".$row['client_name']."</span></div></td>
					 	<td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['item_client_code']."</div></td>
			            <td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['item_mhw_code']."</div></td>
			            <td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['item_description']."</div></td>
			            <td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['container_type']."</div></td>
			            <td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['container_size']."</div></td>
			            <td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['stock_uom']."</div></td>
						<td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['bottles_per_case']."</div></td>
						<td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['upc']."</div></td>
						<td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['scc']."</div></td>
						<td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['vintage']."</div></td>
						<td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['various_vintages']."</div></td>
						</tr>". PHP_EOL);
						  
					$i_item++;   
			} //end item sub-loop
//			echo ("</table></div></td></tr>");
			sqlsrv_free_stmt($getResults_item);
			
	    }
	sqlsrv_free_stmt($getResults);
		
	sqlsrv_close($conn);  
}

?>
      </tbody>
    </table>

<input type="hidden" id="clientlist" value="<?php echo  $clientlist; ?>">

<div class="modal modal-wide fade" id="fileModalx" tabindex="-1" role="dialog" aria-labelledby="fileModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="fileModalLabel">Files</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      </div>
	  <div id='preview'></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



<div class="modal modal-wide fade" id="UpDirectFileModalx" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center" style="padding:5px;">
        <h4 class="modal-title w-100 font-weight-bold">Upload Product Image</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
  <form method="post" enctype="multipart/form-data" action="viewproducts.php">
    <div class="modal-body mx-5" style="padding:0px;">
	 
   </div>
      <div class="modal-footer d-flex justify-content-center" style="padding:5px;">
	    <button type='submit' id="uploadform"  name='uploadform' class='btn btn-primary bg_arrow_blue'><span class='oi oi-data-transfer-upload' title='data-transfer-upload' aria-hidden='true'></span> Upload</button>
      </div>
	  </form>
    </div>
  </div>
</div>


<script>



$(document).ready(function(){
	
$(document).on("change", "input[name='btSelectItemDetails']", function () {
    if (this.checked) {
		
		var item_val=$(this).val();		
		if (unchecked_item_wise[item_val])
		  {
			unchecked_item_wise[item_val]=0;
		  }	
	}
	else 
	{
		var item_val=$(this).val();
		unchecked_item_wise[item_val]=1;
	}
	
});
		

/* ************************************************* Item Description Generation : start ******************************** */
	
	 function get_item_description(federal_type,container_type,container_size,stock_uom,outer_shipper,product_desc,vintage,bottles_per_case)
         {
				var mhwitmdesc = "";
				var mhwitmdesc1 = "";
				var mhwitmdesc2 = "";
				var mhwitemdesc2LEN = 0;
				var prodfedtype = federal_type;
				var itmcontainertype = container_type;
				var itmcontainersize = container_size;
				var itmstockuom = stock_uom;
				var itmoutershipper = outer_shipper;
				var lenNotTaken = 60;
				var itmclientdesc = product_desc;
				var itmvintage = vintage;
								
				//strip trailing zeros from size (covers last char of double zero and trailing zero from decimal values)
				/*
				itmcontainersize = itmcontainersize.replace('.00 Lit','L');
				itmcontainersize = itmcontainersize.replace('.00 Gal','G');
				itmcontainersize = itmcontainersize.replace('.00 NA','NA');
				itmcontainersize = itmcontainersize.replace('.00 ML','ML');
				itmcontainersize = itmcontainersize.replace('.00 OZ','OZ');
				*/
				
				itmcontainersize = itmcontainersize.replace('0 Lit','L');
				itmcontainersize = itmcontainersize.replace('0 Gal','G');
				itmcontainersize = itmcontainersize.replace('0 NA','NA');
				itmcontainersize = itmcontainersize.replace('0 ML','ML');
				itmcontainersize = itmcontainersize.replace('0 OZ','OZ');
				
				//strip spaces between size and units
				itmcontainersize = itmcontainersize.replace(' ','');
				//strip zero right after decimal (covers first char of double zero, while retaining decimals with zero before final non-zero character)
				itmcontainersize = itmcontainersize.replace('.0Lit','.L');
				itmcontainersize = itmcontainersize.replace('.0Gal','.G');
				itmcontainersize = itmcontainersize.replace('.0L','.L');
				itmcontainersize = itmcontainersize.replace('.0G','.G');
				itmcontainersize = itmcontainersize.replace('.0NA','.NA');
				itmcontainersize = itmcontainersize.replace('.0ML','.ML');
				itmcontainersize = itmcontainersize.replace('.0OZ','.OZ');
				//abbreviate units if not already done during zero handling
				itmcontainersize = itmcontainersize.replace('.Lit','L');
				itmcontainersize = itmcontainersize.replace('.Gal','G');
				itmcontainersize = itmcontainersize.replace('.L','L');
				itmcontainersize = itmcontainersize.replace('.G','G');
				itmcontainersize = itmcontainersize.replace('.NA','NA');
				itmcontainersize = itmcontainersize.replace('.ML','ML');
				itmcontainersize = itmcontainersize.replace('.OZ','OZ');
				//abbreviate units if not already done during zero handling
				itmcontainersize = itmcontainersize.replace('Lit','L');
				itmcontainersize = itmcontainersize.replace('Gal','G');

				//itmcontainersizeLAST = itmcontainersize.charAt(itmcontainersize.length-1);
				//alert(itmcontainersizeLAST);
				//if(itmcontainersizeLAST=="0"){ 
				//		itmcontainersize = itmcontainersize.substring(0,itmcontainersize.length-1); 
				//}

				//var itmstockuom = $('input[name="tfa_stock_uom['+itemseq3+']"]').val();
				if(itmstockuom && itmstockuom!=''){
				var itmstockuom4 = itmstockuom.substring(0,4);
				itmstockuom = itmstockuom.toUpperCase();
				itmstockuom4 = itmstockuom4.toUpperCase();
				}

				var itmbottlespercase = bottles_per_case;
				
				if(itmcontainertype.toUpperCase()=='BOTTLE'){
					if(itmstockuom.toUpperCase()=='CASE' || itmstockuom4.toUpperCase()=='CASE'){
						if(prodfedtype.toUpperCase()=="WINE"){
							mhwitmdesc2 = itmvintage + ' ' + itmcontainersize + '-' + itmbottlespercase;
						}
						else if(prodfedtype.toUpperCase()=="DISTILLED SPIRITS" && itmvintage!=''){
							mhwitmdesc2 = itmvintage + ' ' + itmcontainersize + '-' + itmbottlespercase;
						}
						else{
							mhwitmdesc2 = itmcontainersize+'-'+itmbottlespercase;
						}
					}
					else if(itmstockuom.toUpperCase()=='BOTTLE'){
						if(prodfedtype.toUpperCase()=="WINE"){
							mhwitmdesc2 = itmvintage + ' ' + itmcontainersize + '-BT';
						}
						else if(prodfedtype.toUpperCase()=="DISTILLED SPIRITS" && itmvintage!=''){
							mhwitmdesc2 = itmvintage + ' ' + itmcontainersize + '-BT';
						}
						else{
							mhwitmdesc2 = itmcontainersize+'-BT';
						}
					}

					if(itmoutershipper.toUpperCase()=='WOOD'){ mhwitmdesc2 = mhwitmdesc2 + ' WOOD'; }

					mhwitemdesc2LEN = mhwitmdesc2.length;
					mhwitmdesc1 = itmclientdesc.substring(0,59-mhwitemdesc2LEN); //59 because sequence starts at 0
					mhwitmdesc = mhwitmdesc1 + ' ' + mhwitmdesc2;
					lenNotTaken = 59-mhwitemdesc2LEN; //59 because format includes a space between substring item desc and suffixes
				}
				else if(itmcontainertype.toUpperCase()=='CAN'){
					if(itmstockuom.toUpperCase()=='CASE' || itmstockuom4.toUpperCase()=='CASE'){
						if(prodfedtype.toUpperCase()=="WINE"){
							mhwitmdesc2 = itmvintage + ' ' + itmcontainersize + ' CAN-' + itmbottlespercase;
						}
						else if(prodfedtype.toUpperCase()=="DISTILLED SPIRITS" && itmvintage!=''){
							mhwitmdesc2 = itmvintage + ' ' + itmcontainersize + ' CAN-' + itmbottlespercase;
						}
						else{
							mhwitmdesc2 = itmcontainersize+' CAN-'+itmbottlespercase;
						}
					}
					else if(itmstockuom.toUpperCase()=='BOTTLE'){
						if(prodfedtype.toUpperCase()=="WINE"){
							mhwitmdesc2 = itmvintage + ' ' + itmcontainersize + ' CAN';
						}
						else if(prodfedtype.toUpperCase()=="DISTILLED SPIRITS" && itmvintage!=''){
							mhwitmdesc2 = itmvintage + ' ' + itmcontainersize + ' CAN';
						}
						else{
							mhwitmdesc2 = itmcontainersize+' CAN';
						}
					}

					if(itmoutershipper.toUpperCase()=='WOOD'){ mhwitmdesc2 = mhwitmdesc2 + ' WOOD'; }

					mhwitemdesc2LEN = mhwitmdesc2.length;
					mhwitmdesc1 = itmclientdesc.substring(0,59-mhwitemdesc2LEN); //59 because sequence starts at 0
					mhwitmdesc = mhwitmdesc1 + ' ' + mhwitmdesc2;
					lenNotTaken = 59-mhwitemdesc2LEN; //59 because format includes a space between substring item desc and suffixes
				}
				else if(itmcontainertype.toUpperCase()=='KEG' || itmcontainertype.toUpperCase()=='KEG-ONEWAY' || itmcontainertype.toUpperCase()=='KEG-DEPOSIT'){
					mhwitmdesc2 = itmcontainersize+' KEG';
					mhwitemdesc2LEN = mhwitmdesc2.length;
					mhwitmdesc1 = itmclientdesc.substring(0,59-mhwitemdesc2LEN); //59 because sequence starts at 0
					mhwitmdesc = mhwitmdesc1 + ' ' + mhwitmdesc2;
					lenNotTaken = 59-mhwitemdesc2LEN; //59 because format includes a space between substring item desc and suffixes
				}
				else if(itmcontainertype.toUpperCase()=='OTHER'){
					mhwitmdesc2 = itmcontainersize+' '+itmcontainertype;
					mhwitemdesc2LEN = mhwitmdesc2.length;
					mhwitmdesc1 = itmclientdesc.substring(0,59-mhwitemdesc2LEN); //59 because sequence starts at 0
					mhwitmdesc = mhwitmdesc1 + ' ' + mhwitmdesc2;
					lenNotTaken = 59-mhwitemdesc2LEN; //59 because format includes a space between substring item desc and suffixes
				}
				else{
					mhwitmdesc = itmclientdesc;
					lenNotTaken = 60-mhwitemdesc2LEN;
				}
	

				return mhwitmdesc;

         }
		 
/* ************************************************* Item Description Generation : End ******************************** */
		 
	/* those fields(federal_type,product_desc,outer_shipper,) not used now */ 
		
	
	$('#prodTable').on('editable-save.bs.table', function(e, field, row, oldValue, $el){
		        
				var viewname_selected = $("#screenview_name").val();				 
				var special_fields=["product_desc","federal_type"]; 
				if(viewname_selected=="Items") 
				{ 
				var edit_trigger_fields = ["container_type", "container_size","stock_uom", "vintage","bottles_per_case","product_desc"];
				}
				else 
				{
				var edit_trigger_fields = ["container_type", "container_size","stock_uom", "vintage","bottles_per_case","product_desc","federal_type"];
				}
				
		
						if(jQuery.inArray(field, edit_trigger_fields) !== -1)
						{
        /* ************ showing prompt confirm for admin/super_admin : start  ************* */ 
		                  var is_admin='<?php echo ($isAdmin)?'Yes':'No';?>';
						  var prompt_active='<?php echo ($prompt_active)?'Yes':'No';?>';
						  var confirm_txt="confirmed_yes";
						  if(is_admin=="Yes")
						  {
							  if(field=="product_desc" && prompt_active=="No")
							  {
								  confirm_txt="";
							  }
							  else 
							  {
									if (confirm("Are you want to change Item Description?")) {
									confirm_txt = "confirmed_yes";
								  } else {
									confirm_txt="";
								  }
							  }
							  
						  }
		
		/*  *********** showing prompt confirm for admin/super_admin : end  ************* */ 
		
                      if(confirm_txt=="confirmed_yes")
					    {	
                            var item_desc="";					
					        if(field!="product_desc")
							{
							item_desc=get_item_description(row.federal_type,row.container_type,row.container_size,row.stock_uom,row.outer_shipper,row.product_desc,row.vintage,row.bottles_per_case);
							}
							
							var row_item_id=row.item_id;
							var row_finalized=row.finalized;
						
						    if(jQuery.inArray(field, special_fields) == -1  && (row_finalized==0 || row_finalized=="0"))
							{
								if(viewname_selected=="Items") { 
									$('#prodTable').bootstrapTable('updateCell', {
									index: oldValue,
									field: 'item_description',
									value: item_desc
								  });
								}
								else {
									var item_tbl_id="#item_details_"+row.product_id;
								  
								  $(item_tbl_id).bootstrapTable('updateCell', {
									index: oldValue,
									field: 'item_description',
									value: item_desc
								  });
								}
								
								$.post('update-inline.php', {
									table : 'mhw_app_prod_item',
									name:'item_description',
									value: item_desc,
									pk: row_item_id
								}, function(res_data){
									
								});
							}
                   /* *************** update item description : start *********************** */	
								
                               
                   /* *************** update item description : end *********************** */

                    					
					  }

                    /* refresh current page : start */	
					
					 if(jQuery.inArray(field, special_fields) !== -1)
					 {
						 $( "#screenview_name" ).trigger( "change");
						 
					 }
					 
                    /* refresh current page : end */
					
					}
						
					});
	//initialize prod image modal functionality
	
	/*
	$('.prodimg_btn').on('click',function(){
		var productid = $(this).data('product');
		var imageurl = 'viewfiles.php?pid='+productid;
		alert(imageurl);
	    $('.modal-body').load(imageurl,function(){			
	        $('#fileModalx').modal({show:true});
	    });
	});
	*/
	
	
	//initialize prod image download functionality for outside of modal
	$('.dwnld').on('click', function () {
		var imagefile = $(this).data('url');
		var imagename = $(this).data('imagename');
		$.ajax({
			url: imagefile,
			method: 'GET',
			xhrFields: {
				responseType: 'blob'
			},
			success: function (data) {
				var a = document.createElement('a');
				var url = window.URL.createObjectURL(data);
				a.href = url;
				a.download = imagename;
				a.click();
				window.URL.revokeObjectURL(url);
			}
		});
	});

	
	//make sure modal initializers are called when modal is shown
	//$('#fileModalx').on('shown.bs.modal', function (e) {
	//	modalStuff();
	//});
	
  var final_file_err='<?php echo $final_file_err;?>'	
  if(final_file_err!="")
		{			
			alert(final_file_err); 			 
        }
	
	
	
});

</script>

    </div>
<?php
    include('footer.php');
?>