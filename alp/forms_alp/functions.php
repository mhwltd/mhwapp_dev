<?php

function createThumbs( $pathToImages, $pathToThumbs, $fname, $thumbWidth ) 
{
  // open the directory
  //$dir = opendir( $pathToImages );

  // loop through it, looking for any/all JPG files:
   //while (false !== ($fname = readdir( $dir ))) {
    // parse path for the extension
    $info = pathinfo($pathToImages . $fname);
    // continue only if this is a JPEG image
    if ( strtolower($info['extension']) == 'jpg' || strtolower($info['extension']) == 'jpeg' || strtolower($info['extension']) == 'jpe' ) 
    {
      if($debug=="on"){ echo "Creating thumbnail for {$pathToImages}{$fname} in {$pathToThumbs}<br />"; }

      // load image and get image size

	  $img = imagecreatefromjpeg( "{$pathToImages}{$fname}" );
		//echo '1';
      $width = imagesx( $img );
      $height = imagesy( $img );

      // calculate thumbnail size
      $new_width = $thumbWidth;
      $new_height = floor( $height * ( $thumbWidth / $width ) );
		//echo '2';
      // create a new tempopary image
      $tmp_img = imagecreatetruecolor( $new_width, $new_height );
		//echo '3';
      // copy and resize old image into new image 
      imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
		//echo '4';
      // save thumbnail into a file
	  imagejpeg( $tmp_img, "{$pathToThumbs}{$fname}" );
		//echo '5';
    }
	elseif ( strtolower($info['extension']) == 'gif' ) 
    {
      if($debug=="on"){ echo "Creating thumbnail for {$fname} <br />"; }

      // load image and get image size

	  $img = imagecreatefromgif( "{$pathToImages}{$fname}" );

      $width = imagesx( $img );
      $height = imagesy( $img );

      // calculate thumbnail size
      $new_width = $thumbWidth;
      $new_height = floor( $height * ( $thumbWidth / $width ) );

      // create a new tempopary image
      $tmp_img = imagecreatetruecolor( $new_width, $new_height );

      // copy and resize old image into new image 
      imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );

      // save thumbnail into a file
	  imagegif( $tmp_img, "{$pathToThumbs}{$fname}" );

    }
	elseif ( strtolower($info['extension']) == 'png' ) 
    {
      if($debug=="on"){ echo "Creating thumbnail for {$fname} <br />"; }

      // load image and get image size

	  $img = imagecreatefrompng( "{$pathToImages}{$fname}" );

      $width = imagesx( $img );
      $height = imagesy( $img );

      // calculate thumbnail size
      $new_width = $thumbWidth;
      $new_height = floor( $height * ( $thumbWidth / $width ) );

      // create a new tempopary image
      $tmp_img = imagecreatetruecolor( $new_width, $new_height );

      // copy and resize old image into new image 
      imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );

      // save thumbnail into a file
	  imagepng( $tmp_img, "{$pathToThumbs}{$fname}" );

    }
	/* elseif ( strtolower($info['extension']) == 'tif' || strtolower($info['extension']) == 'tiff'  ) 
    {
      if($debug=="on"){ echo "Creating thumbnail for {$fname} <br />"; }

      // load image and get image size
	  $img = imagecreatefromstring( file_get_contents("{$pathToImages}{$fname}") );

      $width = imagesx( $img );
      $height = imagesy( $img );

      // calculate thumbnail size
      $new_width = $thumbWidth;
      $new_height = floor( $height * ( $thumbWidth / $width ) );

      // create a new tempopary image
      $tmp_img = imagecreatetruecolor( $new_width, $new_height );

      // copy and resize old image into new image 
      imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );

      // save thumbnail into a file
	  imagepng( $tmp_img, "{$pathToThumbs}{$fname}" );

    } */
  //}
  // close the directory
  //closedir( $dir );
}
function xmlObjToArr($obj) { 
	$namespace = $obj->getDocNamespaces(true); 
	$namespace[NULL] = NULL; 

	$children = array(); 
	$attributes = array(); 
	$name = strtolower((string)$obj->getName()); 
	
	$text = trim((string)$obj); 
	if( strlen($text) <= 0 ) { 
		$text = NULL; 
	} 
	
	// get info for all namespaces 
	if(is_object($obj)) { 
		foreach( $namespace as $ns=>$nsUrl ) { 
			// atributes 
			$objAttributes = $obj->attributes($ns, true); 
			foreach( $objAttributes as $attributeName => $attributeValue ) { 
				$attribName = strtolower(trim((string)$attributeName)); 
				$attribVal = trim((string)$attributeValue); 
				if (!empty($ns)) { 
					$attribName = $ns . ':' . $attribName; 
				} 
				$attributes[$attribName] = $attribVal; 
			} 
			
			// children 
			$objChildren = $obj->children($ns, true); 
			foreach( $objChildren as $childName=>$child ) { 
				$childName = strtolower((string)$childName); 
				if( !empty($ns) ) { 
					$childName = $ns.':'.$childName; 
				} 
				$children[$childName][] = xmlObjToArr($child); 
			} 
		} 
	} 
	
	return array( 
		'name'=>$name, 
		'text'=>$text, 
		'attributes'=>$attributes, 
		'children'=>$children 
	); 
} 
function hash_gen($start = null, $end = 0, $hash = FALSE, $prefix = FALSE){

    // start IS set NO hash
    if( isset($start, $end) && ($hash == FALSE) ){

        $md_hash = substr(md5(uniqid(rand(), true)), $start, $end);
        $new_hash = $md_hash;

    }else //start IS set WITH hash NOT prefixing
    if( isset($start, $end) && ($hash != FALSE) && ($prefix == FALSE) ){

        $md_hash = substr(md5(uniqid(rand(), true)), $start, $end);
        $new_hash = $md_hash.$hash;

    }else //start NOT set WITH hash NOT prefixing 
    if( !isset($start, $end) && ($hash != FALSE) && ($prefix == FALSE) ){

        $md_hash = md5(uniqid(rand(), true));
        $new_hash = $md_hash.$hash;

    }else //start IS set WITH hash IS prefixing 
    if( isset($start, $end) && ($hash != FALSE) && ($prefix == TRUE) ){

        $md_hash = substr(md5(uniqid(rand(), true)), $start, $end);
        $new_hash = $hash.$md_hash;

    }else //start NOT set WITH hash IS prefixing
    if( !isset($start, $end) && ($hash != FALSE) && ($prefix == TRUE) ){

        $md_hash = md5(uniqid(rand(), true));
        $new_hash = $hash.$md_hash;

    }else{

        $new_hash = md5(uniqid(rand(), true));

    }

    return $new_hash;
}
function mb_pathinfo($filepath) {
    preg_match('%^(.*?)[\\\\/]*(([^/\\\\]*?)(\.([^\.\\\\/]+?)|))[\\\\/\.]*$%im',$filepath,$m);
    if($m[1]) $ret['dirname']=$m[1];
    if($m[2]) $ret['basename']=$m[2];
    if($m[5]) $ret['extension']=$m[5];
    if($m[3]) $ret['filename']=$m[3];
    return $ret;
}
function convert_ascii($string) 
{ 
  // Replace Single Curly Quotes
  $search[]  = chr(226).chr(128).chr(152);
  $replace[] = "'";
  $search[]  = chr(226).chr(128).chr(153);
  $replace[] = "'";
  // Replace Smart Double Curly Quotes
  $search[]  = chr(226).chr(128).chr(156);
  $replace[] = '"';
  $search[]  = chr(226).chr(128).chr(157);
  $replace[] = '"';
  // Replace En Dash
  $search[]  = chr(226).chr(128).chr(147);
  $replace[] = '--';
  // Replace Em Dash
  $search[]  = chr(226).chr(128).chr(148);
  $replace[] = '---';
  // Replace Bullet
  $search[]  = chr(226).chr(128).chr(162);
  $replace[] = '*';
  // Replace Middle Dot
  $search[]  = chr(194).chr(183);
  $replace[] = '*';
  // Replace Ellipsis with three consecutive dots
  $search[]  = chr(226).chr(128).chr(166);
  $replace[] = '...';
  // Apply Replacements
  $string = str_replace($search, $replace, $string);
  // Remove any non-ASCII Characters
  $string = preg_replace("/[^\x01-\x7F]/","", $string);
  return $string; 
}


function generated_validation_query($table_name,$product_info)
{
	
$generated_str="";

	foreach($product_info as $each_product_name=>$val)
	{
		if($val==1)
		{
		  $generated_str=$generated_str.$table_name.".".$each_product_name. " is not null and ".$table_name.".".$each_product_name."!='' and ";
		}
		
	}
	
	return substr($generated_str, 0, -4);
}


function generated_validation_query_v2($table_name,$product_info,$federal_type,$compliance_type,$product_class,$mktg_prod_type,$bev_type)
{
	
$generated_str="";

	foreach($product_info as $each_product_name=>$val)
	{
		if($val==1)
		{	
		  $implode_arr=join("','",$$each_product_name); 
		  $generated_str=$generated_str.$table_name.".".$each_product_name." in  ('".$implode_arr."') and ";
		}
		
	}
	
	return substr($generated_str, 0, -4);
}


function generated_validation_query_v3($table_name,$checked_product_is_linked_to_supplier)
{
$generated_str="";
	if($checked_product_is_linked_to_supplier==1)
	{	
	$generated_str=" $table_name.supplier_id in (select mhw_app_prod_supplier.supplier_id from mhw_app_prod_supplier where mhw_app_prod_supplier.active=1 and mhw_app_prod_supplier.supplier_id>0) ";
	}
return $generated_str;	
}


function generated_validation_query_v51($table_name,$product_info)
{	
    $generated_str="";
    $implode_arr="'".join("','",$product_info)."'";	
	$generated_str=" and i4.requirement_met=1 and reverse(left(reverse(i4.image_name), charindex('.', reverse(i4.image_name)))) in ($implode_arr) ";

	return $generated_str;
}


function generated_validation_query_6($table_name,$product_info)
{
unset($product_info["vintage"]);
$generated_str="";

	foreach($product_info as $each_product_name=>$val)
	{
		if($val==1)
		{
		  $generated_str=$generated_str.$table_name.".".$each_product_name. " is not null and ".$table_name.".".$each_product_name."!='' and ";
		}
		
	}
	
	return substr($generated_str, 0, -4);
}


function get_values_for_keys($mapping, $keys) {
		foreach($keys as $key) {
			$output_arr[] = $mapping[$key];
		}
		return $output_arr;
	}

function generate_draft_item_query($item_validation_matrix,$mhwltdphp_userclients,$UncheckedItemIDString)
{
	$part_query62="";
	$part_query62=generated_validation_query_6('i',$item_validation_matrix);
	$part_query62=($part_query62)?" and $part_query62":$part_query62;
	$product_ids_qry= "select p1.product_id FROM [dbo].[mhw_app_prod] p1 WITH (NOLOCK) 
	 LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] s WITH (NOLOCK) on s.[supplier_id] = p1.[supplier_id] WHERE p1.[client_code] 
	 IN ('".$mhwltdphp_userclients."') AND (p1.[finalized] = 0 OR p1.[product_id] in (select mhw_app_prod_item.product_id 
	 from mhw_app_prod_item where mhw_app_prod_item.finalized<>1 or mhw_app_prod_item.finalized is null))  AND p1.[active] = 1 AND p1.[deleted] =0";
	 
  $final_qry="SELECT i.[item_id],i.[item_client_code],i.[item_mhw_code],i.[item_description]
    ,i.[container_type],i.[container_size],i.[stock_uom],i.[bottles_per_case],i.[upc],i.[scc],i.[vintage],i.[various_vintages],i.[height],i.[length],i.[width],i.[weight],i.[chill_storage],i.[item_status],i.[outer_shipper],i.[bottle_material],i.[create_via],i.[create_date],i.[edit_date],p.[product_id],p.[client_code],p.[client_name]
	,p.[brand_name],p.[product_desc]
	,p.[product_mhw_code]
	,p.[federal_type]
	,convert(varchar, i.create_date, 120) as item_creation_date
	,i.finalized			
	FROM [dbo].[mhw_app_prod_item] i WITH (NOLOCK) 
	left outer join [dbo].[mhw_app_prod] p WITH (NOLOCK) on p.[product_id] = i.[product_id] 
	WHERE $UncheckedItemIDString p.[client_code] IN ('".$mhwltdphp_userclients."') and i.finalized=0 and i.[product_id] in ($product_ids_qry) $part_query62 AND p.[active] = 1 AND p.[deleted] = 0 AND i.[active] = 1 AND i.[deleted] = 0  AND p.[product_desc] <> 'STAR'
	ORDER BY p.[product_id], i.[create_date] desc";
	
	return $final_qry;
}


		 /* Item description generation function : start   */
		 
		 
function generate_item_desc($federal_type,$container_type,$container_size,$stock_uom,$outer_shipper,$brand_product_desc,$vintage,$container_per_case)
        {
			
				$mhwitmdesc = "";
				$mhwitmdesc1 = "";
				$mhwitmdesc2 = "";
				$mhwitemdesc2LEN = 0;
				$prodfedtype = $federal_type;
				$itmcontainertype = $container_type;
				$itmcontainersize = $container_size;
				$itmstockuom = $stock_uom;
				$itmoutershipper = $outer_shipper;
				$lenNotTaken = 60;
				$itmclientdesc = $brand_product_desc;
				$itmvintage = $vintage;
				$itmbottlespercase = $container_per_case;
				
				
				/* strip trailing zeros from size (covers last char of double zero and trailing zero from decimal values) */
				
				$itmcontainersize = str_replace(".00 Lit","L",$itmcontainersize);
				$itmcontainersize = str_replace(".00 Gal","G",$itmcontainersize);
				$itmcontainersize = str_replace(".00 NA","NA",$itmcontainersize);
				$itmcontainersize = str_replace(".00 ML","ML",$itmcontainersize);
				$itmcontainersize = str_replace(".00 OZ","OZ",$itmcontainersize);
				
				$itmcontainersize = str_replace("0 Lit","L",$itmcontainersize);
				$itmcontainersize = str_replace("0 Gal","G",$itmcontainersize);
				$itmcontainersize = str_replace("0 NA","NA",$itmcontainersize);
				$itmcontainersize = str_replace("0 ML","ML",$itmcontainersize);
				$itmcontainersize = str_replace("0 OZ","OZ",$itmcontainersize);
				
				
				/* strip spaces between size and units */
				
				$itmcontainersize = str_replace(" ","",$itmcontainersize);
				
				/* strip zero right after decimal (covers first char of double zero, while retaining decimals with zero before final non-zero character) */
				
				$itmcontainersize = str_replace(".0Lit",".L",$itmcontainersize);
				$itmcontainersize = str_replace(".0Gal",".G",$itmcontainersize);
				$itmcontainersize = str_replace(".0L",".L",$itmcontainersize);
				$itmcontainersize = str_replace(".0G",".G",$itmcontainersize);
				$itmcontainersize = str_replace(".0NA",".NA",$itmcontainersize);
				$itmcontainersize = str_replace(".0ML",".ML",$itmcontainersize);
				$itmcontainersize = str_replace(".0OZ",".OZ",$itmcontainersize);
				
				/* abbreviate units if not already done during zero handling */
				
				$itmcontainersize = str_replace(".Lit","L",$itmcontainersize); 
				$itmcontainersize = str_replace(".Gal","G",$itmcontainersize);
				$itmcontainersize = str_replace(".L","L",$itmcontainersize);
				$itmcontainersize = str_replace(".G","G",$itmcontainersize);
				$itmcontainersize = str_replace(".NA","NA",$itmcontainersize);
				$itmcontainersize = str_replace(".ML","ML",$itmcontainersize);
				$itmcontainersize = str_replace(".OZ","OZ",$itmcontainersize);
				
				/* abbreviate units if not already done during zero handling */
				
				$itmcontainersize = str_replace("Lit","L",$itmcontainersize);
				$itmcontainersize = str_replace("Gal","G",$itmcontainersize);


				if($itmstockuom && $itmstockuom!='')
				{
				$itmstockuom4 =substr($itmstockuom,0,4);
				$itmstockuom = strtoupper($itmstockuom);
				$itmstockuom4 = strtoupper($itmstockuom4);
				}

				
				
				if(strtoupper($itmcontainertype)=='BOTTLE'){
					if($itmstockuom=='CASE' || $itmstockuom4=='CASE'){
						if(strtoupper($prodfedtype)=="WINE"){
							$mhwitmdesc2 = $itmvintage.' '.$itmcontainersize .'-'.$itmbottlespercase;
						}
						else if(strtoupper($prodfedtype)=="DISTILLED SPIRITS" && $itmvintage!=''){
							$mhwitmdesc2 = $itmvintage.' '.$itmcontainersize.'-'.$itmbottlespercase;
						}
						else{
							$mhwitmdesc2 = $itmcontainersize.'-'.$itmbottlespercase;
						}
					}
					else if(strtoupper($itmstockuom)=='BOTTLE'){
						if(strtoupper($prodfedtype)=="WINE"){
							$mhwitmdesc2 = $itmvintage.' '.$itmcontainersize .'-BT';
						}
						else if(strtoupper($prodfedtype)=="DISTILLED SPIRITS" && $itmvintage!=''){
							$mhwitmdesc2 = $itmvintage.' '.$itmcontainersize.'-BT';
						}
						else{
							$mhwitmdesc2 = $itmcontainersize.'-BT';
						}
					}

					if(strtoupper($itmoutershipper)=='WOOD'){ $mhwitmdesc2 = $mhwitmdesc2.' WOOD'; }

					$mhwitemdesc2LEN = strlen($mhwitmdesc2);
					$mhwitmdesc1 =substr($itmclientdesc,0,59-$mhwitemdesc2LEN);					
					$mhwitmdesc = $mhwitmdesc1.' '.$mhwitmdesc2;
					$lenNotTaken = 59-$mhwitemdesc2LEN; 
				}
				else if(strtoupper($itmcontainertype)=='CAN'){
					if($itmstockuom=='CASE' || $itmstockuom4=='CASE'){
						if(strtoupper($prodfedtype)=="WINE"){
							$mhwitmdesc2 = $itmvintage.' '.$itmcontainersize.' CAN-'.$itmbottlespercase;
						}
						else if(strtoupper($prodfedtype)=="DISTILLED SPIRITS" && $itmvintage!=''){
							$mhwitmdesc2 = $itmvintage.' '.$itmcontainersize.' CAN-'.$itmbottlespercase;
						}
						else{
							$mhwitmdesc2 = $itmcontainersize.' CAN-'.$itmbottlespercase;
						}
					}
					else if(strtoupper($itmstockuom)=='BOTTLE'){
						if(strtoupper($prodfedtype)=="WINE"){
							$mhwitmdesc2 = $itmvintage.' '.$itmcontainersize.' CAN';
						}
						else if(strtoupper($prodfedtype)=="DISTILLED SPIRITS" && $itmvintage!=''){
							$mhwitmdesc2 = $itmvintage.' '.$itmcontainersize.' CAN';
						}
						else{
							$mhwitmdesc2 = $itmcontainersize.' CAN';
						}
					}

					if(strtoupper($itmoutershipper)=='WOOD'){ $mhwitmdesc2 = $mhwitmdesc2.' WOOD'; }

					$mhwitemdesc2LEN = strlen($mhwitmdesc2);
					$mhwitmdesc1 = substr($itmclientdesc,0,59-$mhwitemdesc2LEN);
					$mhwitmdesc = $mhwitmdesc1.' '.$mhwitmdesc2;
					$lenNotTaken = 59-$mhwitemdesc2LEN; 
				}
				else if(strtoupper($itmcontainertype)=='KEG' || strtoupper($itmcontainertype)=='KEG-ONEWAY' || strtoupper($itmcontainertype)=='KEG-DEPOSIT'){
					$mhwitmdesc2 = $itmcontainersize.' KEG';
					$mhwitemdesc2LEN = strlen($mhwitmdesc2);
					$mhwitmdesc1 = substr($itmclientdesc,0,59-$mhwitemdesc2LEN);
					$mhwitmdesc = $mhwitmdesc1 .' '.$mhwitmdesc2;
					$lenNotTaken = 59-$mhwitemdesc2LEN; 
				}
				else if(strtoupper($itmcontainertype)=='OTHER'){
					$mhwitmdesc2 = $itmcontainersize.' '.$itmcontainertype;
					$mhwitemdesc2LEN = strlen($mhwitmdesc2);
					$mhwitmdesc1 = substr($itmclientdesc,0,59-$mhwitemdesc2LEN);
					$mhwitmdesc = $mhwitmdesc1.' '.$mhwitmdesc2;
					$lenNotTaken = 59-$mhwitemdesc2LEN;
				}
				else{
					$mhwitmdesc = $itmclientdesc;
					$lenNotTaken = 60-$mhwitemdesc2LEN;
				}
				
			return $mhwitmdesc;
    }			
	
		 
		 /* Item description generation function  : end */
		 
?>