<?php 
$pageref = str_replace(".php","",substr(__FILE__,strrpos(__FILE__,"\\")+1));
$final_file_err="";

if(isset($_POST['fileProdID'])){ $fileProdID = intval($_POST['fileProdID']); }

$clientlist =  "'".str_replace(";","','",$_SESSION['mhwltdphp_userclients'])."'";

if(!isset($_SESSION['mhwltdphp_user'])){

	header("Location: login.php");
}
else
{
	include("dbconnect.php");
	
	
	if(isset($_POST['uploadform'])){
		if(isset($fileProdID) && intval($fileProdID) > 0){
			$nf_view_show=999;
			/***BEGIN Product Code Lookup for Filename***/
			
			$conn = sqlsrv_connect($serverName, $connectionOptions);
			if( $conn === false) {
		    		die( print_r( sqlsrv_errors(), true));
			}
		
			$tsql_pcode= "SELECT product_mhw_code FROM [dbo].[mhw_app_prod] WHERE [product_id] = ".$fileProdID; 
			
			$prodCODE = '';
			
			$stmt_pcode = sqlsrv_query($conn, $tsql_pcode);
		
			while ($row_pcode = sqlsrv_fetch_array($stmt_pcode, SQLSRV_FETCH_ASSOC)) {
				$prodCODE = $row_pcode['product_mhw_code']; 
			}
			
			if( $stmt_pcode === false ) {
				if( ($errors = sqlsrv_errors() ) != null) {
					//log sql errors for writing to server later
					foreach( $errors as $error ) {
						$errorlog.= $stmt_pcode."\n";
						$errorlog.= "SQLSTATE: ".$error[ 'SQLSTATE']."\n";
						$errorlog.= "code: ".$error[ 'code']."\n";
						$errorlog.= "message: ".$error[ 'message']."\n";
					}
				}
				$errorFree=0;
			} //end error handling

			//release sql statement
	  		sqlsrv_free_stmt($stmt_img);
			sqlsrv_close($conn); 
			/***END Product Code Lookup for Filename***/
			
			 if(isset($_POST['img_type']) && $_POST['img_type'] !== ""){

				/* Getting file name */
					$filename = convert_ascii($_FILES['filetoupload']['name']);
				$filetype = str_replace(" ", "",$_POST['img_type']);

				/* Location */
				$location = $upload_dir.$filename;
				$uploadOk = 1;
				$imageFileType = pathinfo($location,PATHINFO_EXTENSION);

				copy($_FILES['filetoupload']['tmp_name'],$upload_dir."temp/".strtotime("now").$filename);
				//check if no file selected.
				if(!is_uploaded_file($_FILES['filetoupload']['tmp_name'])){
					$pageerr="Upload Failure: Please select a file to upload";
					$uploadOk=0;
				}
				if($pageerr==""){
					//check if the directory exists or not.
					if (!is_dir($upload_dir)) {
						$pageerr="Upload Failure: The upload directory does not exist";
						$uploadOk=0;
					}
				}
				if($pageerr==""){
					//check if the directory is writable.
					if (!is_writeable($upload_dir)){
						$pageerr="Upload Failure: The upload directory is not writable";
						$uploadOk=0;

					}
				}
				 if($pageerr==""){
					//Get the Size of the File
					if(isset($size_bytes_limit)){
						$size = $_FILES['filetoupload']['size'];
						//Make sure that file size is correct
						if($size > $size_bytes_limit){
							$kb = $size_bytes_limit / 1024;
							$pageerr="Upload Failure: File size is too large. File must be under ".$kb." KB";
								$uploadOk=0;
						}
					}
				} 
				if($pageerr==""){
					//check file for timeout
					$size = $_FILES['filetoupload']['size'];
					if($size==0){
						$pageerr="Upload Failure: Temporary file expired.  Please try again";
						$uploadOk=0;
					}
				}
				if($pageerr==""){
					//check file extension
					$ext = strrchr($_FILES['filetoupload'][name],'.');
					$ext = strtolower($ext);
					if(($extlimit == "yes") && (!in_array($ext,$limitedext))){
						$pageerr="Upload Failure: File uploads of this type ".$ext." are not allowed";
						$uploadOk=0;
					}
				}

				if($pageerr==""){
					//$filename will hold the value of the file name submitted from the form.
						$filename = 'p'.$fileProdID.'_'.$prodCODE.'_'.$filetype.'__'.convert_ascii($_FILES['filetoupload']['name']);
					$filename = str_replace(' ','',$filename);
					$filename = str_replace('%20','',$filename);
						$filename = str_replace('+','',$filename);
						$filename = str_replace("'","",$filename);
					// Check if file is already exists.
					if(file_exists($upload_dir.$filename)){
						$pageerr="Upload Failure: The file named $filename already exists";
						$uploadOk=0;
					}
				}
				if($pageerr==""){
					//echo $upload_dir.$filename;
					//Move the File to the Directory of your choice
					//move_uploaded_file('filename','destination') Moves file to a new location.
					if(move_uploaded_file($_FILES['filetoupload']['tmp_name'],$upload_dir.$filename)){
						createThumbs($upload_dir,$thumb_dir,$filename,200);
					}	
					else{
						$pageerr="Upload Failure: There was a problem moving your file";
						$uploadOk=0;
					}
				}
			}
			else{
				$pageerr="Upload Failure: Image Type is required";
				$uploadOk=0;
			}
			
			
			

			
			
		}
		else{
			$pageerr="Upload Failure: There was no product associated with the upload";
			$uploadOk=0;
			
		}
		
		if($uploadOk == 0){
		   $final_file_err=$pageerr;
		}
		else
		{				
		   /***BEGIN FILE DB LOGGING***/
			include("dbconnect.php");
			$conn = sqlsrv_connect($serverName, $connectionOptions);
			if( $conn === false) {
		    		die( print_r( sqlsrv_errors(), true));
			}

			 $tsql_img= "EXEC [dbo].[usp_product_image] 
			 @product_id =".$fileProdID."
			,@image_body = '".base64_encode($_FILES['filetoupload'])."'
			,@image_name = '".$filename."'
			,@image_dim = '".str_replace("'","",$_POST['img_dim'])."'
			,@image_type = '".$_POST['img_type']."'
			,@image_note = '".str_replace("'","''",$_POST['img_note'])."'
			,@create_via = 'View Product'
			,@username = '".$_SESSION['mhwltdphp_user']."'"; 
			
			$imgIDx = 0;			
			$stmt_img = sqlsrv_query($conn, $tsql_img);
			
			while ($row_img = sqlsrv_fetch_array($stmt_img, SQLSRV_FETCH_ASSOC)) {
				$imgIDx = $row_img['imgID']; 
			}
			
			$final_file_err="Image has been uploaded successfully.";
			
			if( $stmt_img === false ) {
				if( ($errors = sqlsrv_errors() ) != null) {
					//log sql errors for writing to server later
					foreach( $errors as $error ) {
						$errorlog.= $tsql_img."\n";
						$errorlog.= "SQLSTATE: ".$error[ 'SQLSTATE']."\n";
						$errorlog.= "code: ".$error[ 'code']."\n";
						$errorlog.= "message: ".$error[ 'message']."\n";
					}
				}
				$errorFree=0;
			} //end error handling
			
		//release sql statement
	  		sqlsrv_free_stmt($stmt_img);
			sqlsrv_close($conn); 
		/***END FILE DB LOGGING***/
			
		}
		
		
	}
}
