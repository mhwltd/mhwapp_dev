<?php

session_start();
if($_SESSION['mhwltdphp_usertype'] != "SUPERUSER" && $_SESSION['mhwltdphp_usertype'] != "ADMIN" ){ 
    echo "Access Denied"; exit(0);
}

//echo '<pre>'; print_r($_REQUEST); exit(0);

error_reporting(E_ALL); //displays an error
include("dbconnect.php");

//Establishes the connection
$conn = sqlsrv_connect($serverName, $connectionOptions);
if( $conn === false) {
	print_r( sqlsrv_errors()); exit(0);
}

$current_user = $_SESSION['mhwltdphp_user'];
$current_date = date("Y-m-d H:i:s");

$table = addslashes($_REQUEST['table']);
$product_id = intval($_REQUEST['product_id']);
$id = intval($_POST['pk']);
$field = addslashes($_POST['name']);
$value = $_POST['value'];

if ($_POST['action']=='delete') {

    if ($table == 'mhw_app_prod') {
        $key = 'product_id';
        $workflow_type = "product_delete";

    } else if ($table == 'mhw_app_prod_item') {
        $key = 'item_id';
        $workflow_type = "item_delete";

    } else {
        echo "Wrong parameters"; exit(0);
    }

    $tsql = "UPDATE [$table] SET active = 0, deleted = 1 WHERE [$key] = ?; SELECT SCOPE_IDENTITY();";

    //echo $tsql; exit;
    $stmt = sqlsrv_prepare($conn, $tsql, array($id));
    
    if( $stmt === false )  
    {  
        echo "Statement could not be prepared.\n";  
        die( print_r( sqlsrv_errors(), true));  
    }  
    
    if( sqlsrv_execute($stmt) === false )  
    {  
        echo "Statement could not be executed.\n";  
        die( print_r( sqlsrv_errors(), true));  
    }

    $tsql = "INSERT INTO [mhw_app_workflow] ( 
        [workflow_type]
        ,[record_id]
        ,[created_date]
        ,[edit_date]
        ,[username]
        ,[active]
        ,[deleted]
    ) VALUES (?, ?, ?, ?, ?, ?, ?); SELECT SCOPE_IDENTITY();";

    $stmt = sqlsrv_prepare($conn, $tsql, array( 
        $workflow_type, 
        $id, 
        $current_date, 
        $current_date, 
        $current_user, 
        1, 
        0
    ));  

    if( $stmt === false )  
    {  
        echo "Statement could not be prepared.\n";  
        die( print_r( sqlsrv_errors(), true));  
    }  
    
    if( sqlsrv_execute($stmt) === false )  
    {  
        echo "Statement could not be executed.\n";  
        die( print_r( sqlsrv_errors(), true));  
    }
    
} else {

    if ($table == 'mhw_app_prod') {
        $key = 'product_id';
        $dataTableName = 'mhw_app_prod';
        $logsTableName = 'mhw_app_prod_archive';

        $tsql = "SELECT TOP 1 $key, '$field' AS [field], [$field] AS [previous_value] FROM [$dataTableName] WHERE product_id = ?";

    } else {

        if (in_array($field, ['brand_name','product_desc'])) {
            
            $key = 'product_id';
            $dataTableName = 'mhw_app_prod';
            $logsTableName = 'mhw_app_prod_archive';

            $tsql = "SELECT TOP 1 $key, '$field' AS [field], [$field] AS [previous_value] FROM [$dataTableName] WHERE product_id IN
                (SELECT product_id FROM [mhw_app_prod_item] WHERE item_id = ?)
            ";

        } else {

            $key = 'item_id';
            $dataTableName = 'mhw_app_prod_item';
            $logsTableName = 'mhw_app_prod_item_archive';
    
            $tsql = "SELECT TOP 1 $key, '$field' AS [field], [$field] AS [previous_value] FROM [$dataTableName] WHERE item_id = ?";
        }

    }

    $stmt= sqlsrv_query($conn, $tsql, array($id));
    if (!$stmt) die (sqlsrv_errors());

    $row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);
    if (!$row) die ("Wrong ID...");

    $row['edit_user'] = $current_user;
    $row['edit_date'] = $current_date;

    $fields = "[".implode("],[", array_keys($row))."]";
    $params = array_values($row);
    $values = implode(",", array_fill(0, count($row), '?'));

    $tsql = "INSERT INTO [$logsTableName] ( $fields ) VALUES ( $values ); SELECT SCOPE_IDENTITY();";

    //echo $tsql; exit;
    $stmt = sqlsrv_prepare($conn, $tsql, $params);  

    if( $stmt === false )
    {  
        echo "Statement could not be prepared.\n";  
        die( print_r( sqlsrv_errors(), true));  
    }  

    if( sqlsrv_execute($stmt) === false )  
    {  
        echo "Statement could not be executed.\n";  
        die( print_r( sqlsrv_errors(), true));  
    }  



    $tsql = "UPDATE [$dataTableName] SET [$field] = ? WHERE [$key] = ?; SELECT SCOPE_IDENTITY();";

    //echo $tsql; exit;
    $stmt = sqlsrv_prepare($conn, $tsql, array($value, $row[$key]));

    if( $stmt === false )  
    {  
        echo "Statement could not be prepared.\n";  
        die( print_r( sqlsrv_errors(), true));  
    }  

    if( sqlsrv_execute($stmt) === false )  
    {  
        echo "Statement could not be executed.\n";  
        die( print_r( sqlsrv_errors(), true));  
    }  

}
//echo '<pre>'; print_r($row); exit(0);
//exit(0);


$json = array(
    //'sql' => $tsql,
    //'params' => array($value, $id),
    'result' => sqlsrv_rows_affected ( $stmt )
);
header('Content-Type: application/json');
echo json_encode($json);

/* Free statement and connection resources. */  
sqlsrv_free_stmt( $stmt);  
sqlsrv_close( $conn); 

?>