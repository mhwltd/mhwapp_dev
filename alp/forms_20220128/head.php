<?php
$whereami=str_replace("forms","",str_replace("formsdev","",str_replace("/","",str_replace(".php","",$_SERVER['PHP_SELF']))));
?>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-table.min.css">
	<link rel="stylesheet" href="css/bootstrap-table-fixed-columns.min.css">
	<link rel="stylesheet" href="css/bootstrap-table-sticky-header.min.css">
	<link rel="stylesheet" href="css/bootstrap-editable.css?v=4">
	<link rel="stylesheet" href="css/open-iconic-bootstrap.css">
	<link rel="stylesheet" href="css/fa-all.css">
	<link rel="stylesheet" href="css/jquery.ajax-combobox.css">
	<link rel="stylesheet" href="css/more.css">
	<link rel="stylesheet" href="css/font-awesome.min.css" />

	<style>
		.editable-click {
			color: black;
			text-decoration: none;
		}
	</style>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery-3.3.1.min.js"></script>
	<script src="js/popper.min.js"></script>
    <script src="js/tooltip.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="js/validator.js"></script>
	<script src="js/bootstrap-table.min.js"></script>
	<script type="text/javascript" src="js/bootstrap-table-fixed-columns.min.js"></script>
	<script type="text/javascript" src="js/bootstrap-table-sticky-header.js"></script>
	<script type="text/javascript" src="js/bootstrap-editable.js?v=5"></script>
	<script type="text/javascript" src="extensions/editable/bootstrap-table-editable.js"></script>
	<script type="text/javascript" src="js/FileSaver.min.js"></script>
	<script type="text/javascript" src="js/xlsx.core.min.js"></script>
	<script type="text/javascript" src="js/jspdf.min.js"></script>
	<script type="text/javascript" src="js/jspdf.plugin.autotable.js"></script>
	<!-- For IE support include es6-promise before html2canvas -->
	<script type="text/javascript" src="js/jquery.validate.js"></script>
	<script type="text/javascript" src="js/jquery.validate.unobtrusive.js"> </script>
	<script type="text/javascript" src="js/additional-methods.js"></script>
	<script type="text/javascript" src="js/es6-promise.auto.min.js"></script>
	<script type="text/javascript" src="js/html2canvas.min.js"></script>
	<script type="text/javascript" src="js/tableExport.min.js"></script>
	<script type="text/javascript" src="js/bootstrap-table-export.min.js"></script>
	<script type="text/javascript" src="js/jquery.ajax-combobox.min.js"></script>
	<script type="text/javascript" src="js/moment.min.js"></script>

	<!-- select2 --> 
	<link href="extensions/select2/css/select2.css" rel="stylesheet">
	<script src="extensions/select2/js/select2.js"></script>         

	<!-- select2 bootstrap -->
	<link href="extensions/select2/css/select2-bootstrap.css" rel="stylesheet">	

	<!--favicon-->
	<link rel="apple-touch-icon" sizes="180x180" href="../apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="../favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="../favicon-16x16.png">
	<link rel="manifest" href="../site.webmanifest">
	<link rel="mask-icon" href="../safari-pinned-tab.svg" color="#5bbad5">

	<link rel="prefetch" href="css/img/Logo_Chevron_Animated.gif">

	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="theme-color" content="#ffffff">

    <title>MHW Ltd.</title>

  </head>



  <body>
  
  <nav class="navbar sticky-top navbar-expand-lg navbar-dark bg_arrow_gradient">
    <a class="navbar-brand nav-pills bg_arrow_white_rnd" href="#">
    <img src="css/img/Logo_Full_SVG.svg" height="30" class="svgLogo" alt="mhw logo">
  	</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
     
	  
      <!--
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">Dropdown
        </a>
        <div class="dropdown-menu dropdown-secondary" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>-->
	  <?php 
	  
	if($whereami=='home') { 
		echo "<li class=\"nav-item active\"><a class=\"nav-link\" href=\"home.php\">Home<span class=\"sr-only\">(current)</span></a></li>";
	}
	else{ 
		echo "<li class=\"nav-item\"><a class=\"nav-link\" href=\"home.php\">Home</a></li>";	
	}
	
	if($whereami=='productsetup'
		|| $whereami=='viewproducts'
		|| $whereami=='imageupload') { 
		//echo "<li class=\"nav-item active\"><a class=\"nav-link\" href=\"viewproducts.php\">Products<span class=\"sr-only\">(current)</span></a></li>";
		echo "<li class=\"nav-item dropdown active\">";
        echo "<a class=\"nav-link dropdown-toggle\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">Products</a>";
        echo "<div class=\"dropdown-menu dropdown-secondary\" aria-labelledby=\"navbarDropdownMenuLink\">";
		  echo "<a class=\"dropdown-item\" href=\"productsetup.php\">Product &amp; Item Setup</a>";
          echo "<a class=\"dropdown-item\" href=\"viewproducts.php\">View Products &amp; Items</a>";
          echo "<a class=\"dropdown-item\" href=\"imageupload.php\">Attach Product Images</a>";
			if($_SESSION['mhwltdphp_usertype'] == "SUPERUSER" || $_SESSION['mhwltdphp_usertype'] == "ADMIN" ){ 
				echo "<a class=\"dropdown-item\" href=\"viewpricing.php\">Price Posting</a>";
			}
        echo "</div>";
      echo "</li>";
	}
	else{ 
		//echo "<li class=\"nav-item\"><a class=\"nav-link\" href=\"viewproducts.php\">Products</span></a></li>";	
		echo "<li class=\"nav-item dropdown\">";
        echo "<a class=\"nav-link dropdown-toggle\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">Products</a>";
        echo "<div class=\"dropdown-menu dropdown-secondary\" aria-labelledby=\"navbarDropdownMenuLink\">";
		  echo "<a class=\"dropdown-item\" href=\"productsetup.php\">Product &amp; Item Setup</a>";
          echo "<a class=\"dropdown-item\" href=\"viewproducts.php\">View Products & Items</a>";
          echo "<a class=\"dropdown-item\" href=\"imageupload.php\">Attach Product Images</a>";
          if($_SESSION['mhwltdphp_usertype'] == "SUPERUSER" || $_SESSION['mhwltdphp_usertype'] == "ADMIN" ){ 
				echo "<a class=\"dropdown-item\" href=\"viewpricing.php\">Price Posting</a>";
			}
        echo "</div>";
      echo "</li>";
	}

	if($_SESSION['mhwltdphp_usertype'] == "SUPERUSER" || $_SESSION['mhwltdphp_usertype'] == "ADMIN" ){ 
		if($whereami=='orderreview'
			|| $whereami=='productreview') { 
			echo "<li class=\"nav-item dropdown active\">";
			echo "<a class=\"nav-link dropdown-toggle\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">Compliance</a>";
			echo "<div class=\"dropdown-menu dropdown-secondary\" aria-labelledby=\"navbarDropdownMenuLink\">";
			  echo "<a class=\"dropdown-item\" href=\"orderreview.php\">Order Review</a>";
			  echo "<a class=\"dropdown-item\" href=\"productreview.php\">Product Review</a>";
				echo "<a class=\"dropdown-item\" href=\"product_state_status.php\">Distributor Registration Cancel</a>";
			  echo "<a class=\"dropdown-item\" href=\"blr-status.php\">BLR Status</a>";
			echo "</div>";
		  echo "</li>";
		}
		else{ 
			echo "<li class=\"nav-item dropdown\">";
			echo "<a class=\"nav-link dropdown-toggle\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">Compliance</a>";
			echo "<div class=\"dropdown-menu dropdown-secondary\" aria-labelledby=\"navbarDropdownMenuLink\">";
			  echo "<a class=\"dropdown-item\" href=\"orderreview.php\">Order Review</a>";
			  echo "<a class=\"dropdown-item\" href=\"productreview.php\">Product Review</a>";
				echo "<a class=\"dropdown-item\" href=\"product_state_status.php\">Distributor Registration Cancel</a>";
  echo "<a class=\"dropdown-item\" href=\"blr-status.php\">BLR Status</a>";
			echo "</div>";
		  echo "</li>";
		}
	} 

	/* Select Client Dropdown */
	if (in_array($_SESSION['mhwltdphp_usertype'], ["SUPERUSER", "ADMIN"])) {
		echo "<a class='nav-link' id='set-client-link' href='#'><span class='menu_item2'>Select Client</span></a>";
	}
echo "<li class=\"nav-item\" id=\"menudiv\">";
//echo "|".$whereami."|";
	if($_SESSION['mhwltdphp_user']){ 
		
		echo "<a class=\"nav-link\" href=\"../logout.php\"><span class=\"menu_item2\">Logout</span></a>"; 
	}
	else{ 
		if($whereami=='login') { 
			echo "<li class=\"nav-item active\"><a class=\"nav-link\" href=\"login.php\">Login<span class=\"sr-only\">(current)</span></a></li>";
		}
		else{ 
			echo "<li class=\"nav-item\"><a class=\"nav-link\" href=\"login.php\">Login</span></a></li>";	
		}
		//echo "<a class=\"nav-link\" href=\"login.php\"><span class=\"";
		//if($whereami=='login') { echo "selected_"; }
		//echo "menu_item\">Login</span></a>"; 
	}
echo "</li>";

if($_SESSION['mhwltdphp_user']){ 
	echo "<li class=\"nav-item  float-right\" id=\"loggedin\"><span class=\"loggedin_span\"><i class=\"fas fa-user-circle\"></i><!--Logged in as:--> ".strtoupper($_SESSION['mhwltdphp_user']);
	//if($_SESSION['mhwltdphp_admin']==1){ echo " (ADMIN)"; }
	echo "</span></li>";

	echo "<li class=\"nav-item  float-left\" id=\"userAlerts\"></li>";
}

echo "<div class=\"usertooltip\" style=\"display:none;\">Last Name:".$_SESSION['mhwltdphp_userlastname']."<br />Email:".$_SESSION['mhwltdphp_useremail']."<br />Clients:".$_SESSION['mhwltdphp_userclients']."<br />Type:".$_SESSION['mhwltdphp_usertype']."</div>";

?>
    </ul>
    <!--<form class="form-inline">
      <div class="md-form my-0">
        <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
      </div>
    </form>-->
  </div>
</nav>


<?php 
if (in_array($_SESSION['mhwltdphp_usertype'], ["SUPERUSER", "ADMIN"])) {
	include('settings.php');
	include('dbconnect.php');
	$conn = sqlsrv_connect($serverName, $connectionOptions);
	if( $conn === false) die( print_r( sqlsrv_errors(), true));
	$tsql= "SELECT * FROM [vw_Clients] order by client_name";
	$stmt = sqlsrv_query( $conn, $tsql);
	if ( $stmt === false ) die( print_r( sqlsrv_errors(), true));  
?>
<form method="POST">
<div class="modal modal-wide fade" id="set-client-modal" role="dialog" aria-labelledby="dlg-set-client-label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="dlg-set-client-label">Select Client</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body1 text-center">
        <select id="admin-client-code" name="admin-client-code" title="Select Client" style="width:90%">
		<?php while ( $row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC) ) { 
				if ($_POST['admin-client-code'] == $row['client_code']) {
					$_SESSION['mhwltdphp_user_clients_codes'] = $row['client_code'];
					$_SESSION['mhwltdphp_userclients'] = $row['client_name'];
					setcookie('UC_mhwphp', $_SESSION['mhwltdphp_userclients'], 3600);
					$clientSelected = "SELECTED";
				}
				$clientSelected = $_SESSION['mhwltdphp_user_clients_codes'] == $row['client_code'] ? "SELECTED" : "";
		?>
			<option value='<?=$row['client_code']?>' <?=$clientSelected?> ><?=$row['client_name']?></option>
		<?php } ?>
        </select>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary" id="btn-dlg-set-client">Set</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</form>
<?php 
	sqlsrv_free_stmt($stmt);
}
?>

<script>
$( document ).ready(function() {
	
	$("#set-client-link").click(() => {
		$("#set-client-modal").modal();
	});
	$("#admin-client-code").select2();

	//function (to be called within head.php as included in other pages) to update alert area in nav (head.php)
	function refreshAlertH(){
		var clientlistH = '<?php echo $_SESSION["mhwltdphp_userclients"]; ?>';
		var qrytypeH = 'STAT_COUNT_unfinalized';
			$.post('query-request.php', {qrytype:qrytypeH,clientlist:clientlistH}, function(dataTH){ 
				// ajax request query-request.php, send qrytype & client as post variable, return dataX variable, parse into JSON
				var resp = JSON.parse(dataTH);
				$.each( resp, function( key, value ) {
					if(parseInt(value.unfinalizedCount)>0){
						$("#userAlerts").html("<i class=\"fas fa-exclamation-triangle\"></i><a href=\"viewproducts.php?v=nf\"> Not Finalized <span class=\"badge badge-warning\">"+value.unfinalizedCount+"</span></a>");
					}
					else{ 
						$("#userAlerts").html(""); 
					}

				});
			});
	}
	refreshAlertH();
});
</script>
