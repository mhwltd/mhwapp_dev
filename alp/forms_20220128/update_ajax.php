<?php
include("sessionhandler.php");

//error_reporting(E_ALL);
//ini_set('display_errors', '1');

if($_SESSION['mhwltdphp_usertype'] != "SUPERUSER" && $_SESSION['mhwltdphp_usertype'] != "ADMIN" ){ 
    echo "Access Denied"; exit(0);
}

include("dbconnect.php");

//Establishes the connection
$conn = sqlsrv_connect($serverName, $connectionOptions);
if( $conn === false) {
	print_r( sqlsrv_errors()); exit(0);
}

$current_user = $_SESSION['mhwltdphp_user'];
$current_date = date("Y-m-d H:i:s");

$table = addslashes($_POST['tn']);
$product_id = intval($_POST['product_id']);
$id=0;
$field = addslashes($_POST['name']);
$value = addslashes($_POST['value']);
$clientlist=$_POST['clientlist'];

if($value!="")
 {

    if ($table == 'mhw_app_prod') {
        $key = 'product_id';
        $dataTableName = 'mhw_app_prod';
        $logsTableName = 'mhw_app_prod_archive';
		$id=$product_id;
		
		if($field=='supplier_id')
		{
			$tsql_supplier_opts = "select top 1 supplier_id  from mhw_app_prod_supplier where client_name in ($clientlist) and active=1 and deleted=0 and supplier_name='$value'";
			$getResults_supplier_opts = sqlsrv_query($conn, $tsql_supplier_opts);

			if ($getResults_supplier_opts == FALSE)
				echo (sqlsrv_errors());

			while ($row_supplier_opts = sqlsrv_fetch_array($getResults_supplier_opts, SQLSRV_FETCH_ASSOC)) {
				$value=$row_supplier_opts['supplier_id'];
			}
		}
				
        $tsql = "SELECT TOP 1 $key, '$field' AS [field], [$field] AS [previous_value] FROM [$dataTableName] WHERE product_id = ?";
        
    }
	
	
	$stmt= sqlsrv_query($conn, $tsql, array($id));
    if (!$stmt) die (sqlsrv_errors());

    $row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);
    if (!$row) die ("Wrong ID...");

    $row['edit_user'] = $current_user;
    $row['edit_date'] = $current_date;

    $fields = "[".implode("],[", array_keys($row))."]";
    $params = array_values($row);
    $values = implode(",", array_fill(0, count($row), '?'));

    $tsql = "INSERT INTO [$logsTableName] ( $fields ) VALUES ( $values ); SELECT SCOPE_IDENTITY();";
    //echo "<pre>"; print_r($params); exit;
    //echo $tsql; exit;
    $stmt = sqlsrv_prepare($conn, $tsql, $params);  

    if( $stmt === false )
    {  
        echo "Statement could not be prepared.\n";  
        die( print_r( sqlsrv_errors(), true));  
    }  

    if( sqlsrv_execute($stmt) === false )  
    {  
        echo "Statement could not be executed.\n";  
        die( print_r( sqlsrv_errors(), true));  
    }  



    $tsql = "UPDATE [$dataTableName] SET [$field] = ? WHERE [$key] = ?; SELECT SCOPE_IDENTITY();";

    //echo $tsql; exit;
    $stmt = sqlsrv_prepare($conn, $tsql, array($value, $id));

    if( $stmt === false )  
    {  
        echo "Statement could not be prepared.\n";  
        die( print_r( sqlsrv_errors(), true));  
    }  

    if( sqlsrv_execute($stmt) === false )  
    {  
        echo "Statement could not be executed.\n";  
        die( print_r( sqlsrv_errors(), true));  
    }  

	$json = array(
    'result' => sqlsrv_rows_affected ( $stmt )
		);
		header('Content-Type: application/json');
		echo json_encode($json);
		
		
 }
else 
{
	echo "operation not successfully";
}

/* Free statement and connection resources. */  
sqlsrv_free_stmt( $stmt);  
sqlsrv_close( $conn);  

?>

