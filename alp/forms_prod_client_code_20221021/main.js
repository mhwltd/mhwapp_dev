		$(document).ready(function(){
			/*======BEGIN INITIAL PAGE LOAD======*/
			
			//if product id is passed to page, preload with data
			var prodIDinit = $("#prodID").val();
			if(parseInt(prodIDinit) > 0){
				prefill_prod(prodIDinit);

				cbb_formatter();
			}

			/*======END INITIAL PAGE LOAD======*/

			/*======BEGIN comboboxes==========*/
			function cbb_formatter(){ 				
				$('ul.ac_results').css("margin-left","0em");
				$('ul.ac_results').css("margin","0em");
				$('.ac_button').css("height","28px");
				$('.ac_button').css("background","transparent");
				$('.ac_button').css("margin-left","-35px");
				$('.ac_button').css("border","0px");
				$('.ac_subinfo dl').css("margin-left","0em");
				$('.ac_subinfo dl').css("margin","0em");
			}
			//load form fields - as a result of selecting product_desc combobox
			function prefill_prod(prodID){
				var qrytype = "prodByID";
				var client = $('#tfa_client_name').val(); //get value of the client combobox
				var client_code = '<?php echo $_SESSION["mhwltdphp_userclientcodes"]; ?>';
				
				$.post('query-request.php', {qrytype:qrytype,prodID:prodID,client:client_code}, function(dataPx){ // ajax request query-request.php, send brand as post variable, return dataX variable, parse into JSON

					var t = JSON.parse(dataPx);

					$.each( t, function( key, value ) {
						//product

						$('#product_mhw_code').val(value.product_mhw_code);
						$('#product_mhw_code_search').val(value.product_mhw_code_search);	
	
					});
				});
			}
			function isProdValidSelection(){
				var isProdID = $('#tfa_product_desc')[0].hasAttribute("sub_info"); //free-type data will not have sub_info attribute
				if(isProdID){
					var json = $('#tfa_product_desc').attr('sub_info');
					//if additional sub_info elements are added, additional apostrophe replacements need to be added
					var json2 = json.replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"');
					var jsonObj = JSON.parse(json2);

					$("#prodID").val(jsonObj.id);  //set field for processing update
					//$("#tfa_399-L").html("Product: editing product_id "+jsonObj.id); //display message for update
					//prefill_prod(jsonObj.id); //prefill form fields					
					$("#productvalid").hide();
				}
				else{
					//$("#tfa_399-L").html("Product: new entry"); //display message for insert
					$("#prodID").val("");  //clear field for insert	
					$("#productvalid").show();					
				}
			}
			
			//initialize product combobox
			function initialize_product(){
				//$('#tfa_product_desc').parent().find(".ac_button").remove();  //clear elements of previous instance
				//$('#tfa_product_desc').parent().find(".ac_result_area").remove();  //clear elements of previous instance
				//var client = $('#tfa_client_name').val(); //get value of the client combobox
				var client_code = 'WEBDEMO';
				//var brand = $('#tfa_brand_name').val(); //get value of the brand combobox
				var brand = '';
				var cbbtype = 'product';
				$.post('select-request.php', {cbbtype:cbbtype,client:client_code,brand:brand}, function(dataX){ // ajax request select-request.php, send brand as post variable, return dataX variable, parse into JSON
					var data = JSON.parse(dataX);
					$(function() {
						$('#tfa_product_desc').ajaxComboBox(
							data,
							{
							  bind_to: 'tfa_product_desc',
							  sub_info: true,
							  sub_as: {
								id: 'Product ID',
								code: 'MHW Product Code',
								ttb: 'TTB ID'
							  },
							}
						).bind('tfa_product_desc', function() {
						  var json = $(this).attr('sub_info');
						  //if additional sub_info elements are added, additional apostrophe replacements need to be added
						  var json2 = json.replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"');
						  var jsonObj = JSON.parse(json2);

						  if(parseInt(jsonObj.id) > 0){
							$("#prodID").val(jsonObj.id); //set field for processing update
							$("#tfa_399-L").html("Product: editing product_id "+jsonObj.id); //display message for update
							prefill_prod(jsonObj.id); //prefill form fields
						  }
						  isProdValidSelection();

						});
						cbb_formatter(); //apply css and classname changes to combobox elements
					});
				});
				$('#tfa_product_desc').bind("change", function(){ 
					isProdValidSelection();
				});
			}
			initialize_product();

			/*======END comboboxes==========*/
			
			//Display/Hide Divs functionality
			
			CheckMMBoxes();
			
			$("form").change(function(){   				
				   CheckMMBoxes();
			});
						
		   function CheckMMBoxes() {
			   $(".hiddeninputs").each(function () {
					 var currdiv = $(this).attr('data-div');
									
					
				 if ($(this).is(":checked")) {	         
					   $("#" + currdiv).show();
					 } else {
							$("#" + currdiv).hide();
						 }
				});
		   } 
		   
		   //Initialize repeating boxes
		   $("#repeater").createRepeater();	 


			//Clear form
			$("#clearform").click(function() {
				$("4690986").trigger('reset');
			});		   

		});