<?php
session_start();
include("prepend.php");
include("settings.php");
//include('head.php');
session_cache_expire();
session_unset();
session_destroy();

if($_SESSION['mhwltdphp_user']){ unset($_SESSION['mhwltdphp_user']); }
if($_SESSION['mhwltdphp_userlastname']){ unset($_SESSION['mhwltdphp_userlastname']); }
if($_SESSION['mhwltdphp_useremail']){ unset($_SESSION['mhwltdphp_useremail']); }
if($_SESSION['mhwltdphp_userclients']){ unset($_SESSION['mhwltdphp_userclients']); }
if($_SESSION['mhwltdphp_usertype']){ unset($_SESSION['mhwltdphp_usertype']); }
if($_SESSION['mhwltdphp_usercompids']){ unset($_SESSION['mhwltdphp_usercompids']); }
if($_SESSION['mhwltdphp_userclientcodes']){ unset($_SESSION['mhwltdphp_userclientcodes']); }
if($_SESSION['mhwltdphp_redirect']){ unset($_SESSION['mhwltdphp_redirect']); }

$past = time() - 100;
//this makes the time in the past to destroy the cookie
setcookie(ID_mhwphp, gone, $past);
setcookie(UL_mhwphp, gone, $past);
setcookie(UE_mhwphp, gone, $past);
setcookie(UC_mhwphp, gone, $past);
setcookie(UT_mhwphp, gone, $past);
setcookie(UI_mhwphp, gone, $past);
setcookie(UD_mhwphp, gone, $past);
//setcookie(Key_mhwphp, gone, $past);


$MHW_AD_USER_1 = $_SERVER['AUTH_USER'];
$MHW_AD_USER_2 = $_SERVER['HTTP_X_MS_CLIENT_PRINCIPAL_NAME'];

if($MHW_AD_USER_1!='' || $MHW_AD_USER_2!=''){
	header("Location: ../.auth/logout?post_logout_redirect_uri=/forms/logout.php");
}
else{
	header("Location: ".$sitelogin);
}

?>