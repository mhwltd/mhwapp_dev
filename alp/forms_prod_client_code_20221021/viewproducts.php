<?php
//session_start();
include("sessionhandler.php");
include("prepend.php");
include("settings.php");
include("functions.php");

//echo "<hr><pre>"; print_r($_SESSION); echo "</pre><hr>";

if(!isset($_SESSION['mhwltdphp_user'])){

	header("Location: login.php");
}
else{	
	
	include("dbconnect.php");
    include('head.php');
//echo "<hr><pre>"; print_r($_SESSION); echo "</pre><hr>";
	$isAdmin = false;
	$editableField = "";

	if($_SESSION['mhwltdphp_usertype'] == "SUPERUSER" || $_SESSION['mhwltdphp_usertype'] == "ADMIN" ){ 
		$isAdmin = true;
		$editableField = "editable: true,";
	}

	$country_opts = array();
	$conn = sqlsrv_connect($serverName, $connectionOptions);
	if( $conn === false) {
		die( print_r( sqlsrv_errors(), true));
	}
	$tsql_country = "select DISTINCT [Code], REPLACE([Desc],'\"','') as [Desc] from [dbo].[country-list] ORDER BY [Code]";
	$getResults_country = sqlsrv_query($conn, $tsql_country);

	if ($getResults_country == FALSE)
		echo (sqlsrv_errors());

	while ($row_country = sqlsrv_fetch_array($getResults_country, SQLSRV_FETCH_ASSOC)) {
		array_push($country_opts, $row_country);
	}
	sqlsrv_free_stmt($getResults_country);


	$container_size_opts = array();
	$tsql_container_size = "select DISTINCT ContainerSize + ' ' + ContainerSizeUOM as [size_opts] from [dbo].[mhw_app_container_size] ORDER BY ContainerSize + ' ' + ContainerSizeUOM ";
	$getResults_container_size = sqlsrv_query($conn, $tsql_container_size);
	if ($getResults_container_size == FALSE)
		echo (sqlsrv_errors());
	while ($row_container_size = sqlsrv_fetch_array($getResults_container_size, SQLSRV_FETCH_ASSOC)) {
		array_push($container_size_opts, $row_container_size);
	}
	sqlsrv_free_stmt($getResults_container_size);

	sqlsrv_close($conn);

?>

<script>
	var federalTypeOptions = ['Ciders','Distilled Spirits','Malt Beverage','Non Alcohol','Wine'];
	var complianceTypeOptions = ['Cider','Wine based Cider','Malt Based Cider','Spirits','Mixed Drinks','Malt Beverages','Non Alcohol','Wine','Wine Product','Low Alcohol Wine','Domestic Sake'];
	var productClassOptions = ['Imported', 'Domestic'];
	var marketingCategoryOptions = ["Absinthe","Akvavit","Applejack","Arak","Arrack","Awamori","Baijiu","Beer","Bitters","Borovicka","Brandy","Brandy - Armagnac","Brandy - Cognac","Brandy - Eau-de-vie","Brandy - Fruit","Brandy - Grappa","Brandy - Palinka","Brandy - Pisco","Brandy - Rakia","Brandy - Singani","Brandy - Slivovitz","Brandy - Tuica","Cachaca","Cauim","Champagne","Chicha","Cider","Cider - Flavored","Coolers","Cordials and Liqueurs","Cream Liqueurs","Desi Daru","Flavored Malt Beverages/Coolers","Fortified","Fortified - Madeira","Fortified - Marsala","Fortified - Port","Fortified - Sherry","Fortified - Tonto","Fortified - Vermouth","Fruit Liqueur - Tepache","General","Gin","Horilka","Huangjiu","Icariine Liquor","Kaoliang","Kasiri","Kilju","Kumis","ManX Spirit","Maotai","Mead","Metaxa","Mezcal","Mixers","Neutral Grain Spirit","Nihamanchi","Non-Alcohol","Non-Grape Wine","Ogogoro","Other","Ouzo","Palm wine","Parakari","Raki","Rice Wine","Rose","RTD / Prepared Cocktails","Rum","Rum - Flavored","Rum - Mamajuana","Sake","Sakur�","Sangria","Schnapps","Shochu","Soju","Sonti","Sparkling","Sparkling - Cava","Specialty Spirit","Table Flavored","Table Flavored - Pulque","Table Red","Table White","Tequila","Tiswin","Vinsanto","Vodka","Vodka - Flavored","Water","Whiskey","Whiskey - American","Whiskey - Bourbon","Whiskey - Flavored","Whiskey - Foreign","Whiskey - Irish","Whiskey - Moonshine","Whiskey - Poit�n","Whiskey - Rye","Whiskey - Tennessee","Whisky - Canadian","Whisky - Japanese","Whisky - Scotch","Wine - Champagne","Wine - Coolers","Wine - Fortified","Wine - Fortified - Port","Wine - Fortified - Sherry","Wine - Fortified - Vermouth","Wine - Red","Wine - Rose","Wine - Sake","Wine - Sparkling","Wine - White","Wine Product"];
	var beverageTypeOptions = ["Carbonated Wine","Dessert Fruit Wine","Dessert Flavored Wine","Dessert Port/Sherry/(Cooking)","Wine Low Alcohol","Other Wine","Sparkling Wine/Champagne","Table Fruit Wine","Table Flavored Wine","Table Red and Rose Wine","Table White Wine","Vermouth","Cider","Beer","Brandy","Cordials &amp; Liqueurs","Cocktails &amp; Specialties","Gin","Imitations","Neutral Spirits","Other Liquor","Tequila","Rum","Whiskey"];
	var countries = [<?=join(",\n", array_map(function($c) {
		return "{value:'".$c['Code']."',text:'".addslashes(utf8_encode($c['Desc']))."'}";
	}, $country_opts))?>];

	var containerTypeOptions = ['Bottle','Can','Keg-oneway','Keg-deposit','Other'];
	var containerSizeOptions = [<?=join(",\n", array_map(function($c) {
		return "{value:'".$c['size_opts']."',text:'".addslashes(utf8_encode($c['size_opts']))."'}";
	}, $container_size_opts))?>];
	var stockUomOptions = ['CASE','BOTTLE','KEG'];


	$( document ).ready(function() {

        <?php if($isAdmin) { ?>
			$.fn.editable.defaults.mode = 'inline';
			$.fn.editable.defaults.emptytext = '...';
		<?php }?>

		$("#finalize").hide();

		$('#client_name').on('change', function () {
			var value = $(this).val().toLowerCase();
			var valorig = $(this).val();
			
			if(value!="all"){
				$('#prodTable').bootstrapTable('filterBy', {client_name: valorig});
			}
			else{
				$('#prodTable').bootstrapTable('filterBy', '');
			}

			$('.prodedit_btn').on("click", function() {
					var prod = $(this).data("target");
				$("#editprod_"+prod).submit();
			});
		});
		
		$(".prodedit_btn").on("click", function() {
			var prod = $(this).data("target");
			$("#editprod_"+prod).submit();
		});

		$(document).off('click', '.btn_itemtoggle').on('click', '.btn_itemtoggle',function(e) {
			var trparent = $(this).closest("tr");
			var prodindex = $(this).data('index');
			var exp = trparent.find(".detail-icon");
			$( exp ).trigger( "click" );
		}); 
		//function (to be called within viewproducts.php) to update alert area in nav (head.php)
		function refreshAlert(){
			var clientlistH = '<?php echo $_SESSION["mhwltdphp_userclientcodes"]; ?>';
			var qrytypeH = 'STAT_COUNT_unfinalized';
			$.post('query-request.php', {qrytype:qrytypeH,clientlist:clientlistH}, function(dataTH){ 
				// ajax request query-request.php, send qrytype & client as post variable, return dataX variable, parse into JSON
				var resp = JSON.parse(dataTH);
				$.each( resp, function( key, value ) {
					if(parseInt(value.unfinalizedCount)>0){
						$("#userAlerts").html("<i class=\"fas fa-exclamation-triangle\"></i><a href=\"viewproducts.php?v=nf\"> Not Finalized <span class=\"badge badge-light\">"+value.unfinalizedCount+"</span></a>");
					}
					else{ 
						$("#userAlerts").html(""); 
					}

				});
			});
		}
		//function to initialize all of the in-modal functionality
		function modalStuff(){
			//from image details to image full preview
			$('.prodimg_thumb').bind('click',function(){
				var imageid = $(this).data('imageid');
				var productid = $(this).data('product');
				var imageurl = 'viewfiles.php?iid='+imageid+'&b='+productid;
				$('.modal-body').load(imageurl,function(){
					$('#fileModalx').modal({show:true});
					modalStuff(); //recursive but necessary to initialize other states on each change
				});
			});
			//back link in modal from image full to image details
			$('.prodimg_back').bind('click',function(){
				var productid = $(this).data('product');
				var producturl = 'viewfiles.php?pid='+productid;
				$('.modal-body').load(producturl,function(){
					$('#fileModalx').modal({show:true});
					modalStuff(); //recursive but necessary to initialize other states on each change
				});
			});
			//initialize prod image modal functionality again
			$('.prodimg_btn').bind('click',function(){
				var productid = $(this).data('product');
				var imageurl = 'viewfiles.php?pid='+productid;
				$('.modal-body').load(imageurl,function(){
					$('#fileModalx').modal({show:true});
					modalStuff(); //recursive but necessary to initialize other states on each change
				});
			});
			//download image button
			$('.dwnld').bind('click', function () {
				var imagefile = $(this).data('url');
				var imagename = $(this).data('imagename');
				$.ajax({
					url: imagefile,
					method: 'GET',
					xhrFields: {
						responseType: 'blob'
					},
					success: function (data) {
						var a = document.createElement('a');
						var url = window.URL.createObjectURL(data);
						a.href = url;
						a.download = imagename;
						a.click();
						window.URL.revokeObjectURL(url);
					}
				});
			});
			//go to upload page for product
			$('.prod_upload').bind('click', function () {
				var prod = $(this).data('prod');
				window.location = 'imageupload.php?pid='+prod;
			});
			$(".prodedit_btn").bind("click", function() {
				var prod = $(this).data("target");
				$("#editprod_"+prod).submit();
			});
		};
		function alertTray(qrytype,prodlist,codelist){
			var clientlist = $("#clientlist").val(); 
			var trayURL = '<?php echo $trayFinalizedWorkflow; ?>';  //defined in settings.php

			//$.post(trayURL, {qrytype:qrytype,prodlist:prodlist,clientlist:clientlist,codelist:codelist}, function(dataT){ 
				// ajax request tray web hook, send qrytype, products & clients as post variables
			$.post('query-request.php', {qrytype:qrytype,prodlist:prodlist,clientlist:clientlist,codelist:codelist}, function(dataT){ 
				// ajax request to queue finalization, send qrytype, products & clients as post variables
			});
		}

		//Bootstrap table
		var $table = $('#prodTable')
		var $remove = $('#remove')
		var selections = []

		function getIdSelections() {
			return $.map($table.bootstrapTable('getSelections'), function (row) {
				var viewname_raw = $("#screenview_name").val();
				if(viewname_raw=="Items") { 
					return row.item_id
				}
				else{
					return row.product_id
					//return row.product_mhw_code
				}
			})
		}
		function getCodeSelections() {
			return $.map($table.bootstrapTable('getSelections'), function (row) {
				var viewname_raw = $("#screenview_name").val();
				if(viewname_raw=="Items") { 
					return row.item_id
				}
				else{
					//return row.product_id
					return row.product_mhw_code
				}
			})
		}
		function expandTable($detail, row) {
			$detail.bootstrapTable('showLoading');
			subtableBuilder($detail.html('<table id="item_details_'+row.product_id+'" class="table-info item_details_st" data-id-field="item_id"></table>').find('table'), 'VPproditems', row.product_id)
		}
		function responseHandler(res) {
			$.each(res.rows, function (i, row) {
			  row.state = $.inArray(row.id, selections) !== -1
			})
			return res
		}

		function detailFormatter(index, row) {
			var html = []
			$.each(row, function (key, value) {
			  html.push('<p><b>' + key + ':</b> ' + value + '</p>')
			})
			return html.join('')
		}

		window.operateEvents = {
			'click .like': function (e, value, row, index) {
			  //alert('You click like action, row: ' + JSON.stringify(row))
			},
			'click .prodimg_btn': function (e, value, row, index) {
			  //alert('You click prodimg_btn action, row: ' + JSON.stringify(row))
			},
			<?php if ($isAdmin) { ?>
			'click .btn-delete-product': function (e, value, row, index) {
				var p_del_confirm = confirm("Confirm Delete?"); 
				if (p_del_confirm == true) { 
					$.post('update-inline.php', {
						action: 'delete',
						table: 'mhw_app_prod',
						pk: row.product_id,
					}, function(result) {
			  $table.bootstrapTable('remove', {
							field: 'product_id',
							values: [row.product_id]
			  })
					});
				}

			},
			'click .btn-delete-item': function (e, value, row, index) {
				var i_del_confirm = confirm("Confirm Delete?"); 
				if (i_del_confirm == true) { 
					$.post('update-inline.php', {
						action: 'delete',
						table: 'mhw_app_prod_item',
						pk: row.item_id,
					}, function(result) {
						$(e.target).closest('tr').remove();
					});
				}
			},
			'click .btn-audit-product': function (e, value, row, index) {
				window.open('/forms/audit-trail-view.php?product_id='+row.product_id)
			},
			'click .btn-audit-item': function (e, value, row, index) {
				window.open('/forms/audit-trail-view.php?item_id='+row.item_id)
			},
			<?php } ?>
			}

		<?php if ($isAdmin) { ?>
		//item row buttons
		function operateItemFormatter(value, row, index) {
			return [
				'<button type="button" class="btn btn-delete-item btn-sm btn-danger" data-item="'+row.item_id+'">',
				'<i class=\"fa fa-trash\"></i>Delete</button>',
				'<button type="button" class="btn btn-dark btn-audit-item btn-sm bg_arrow_darkblue" data-item="'+row.item_id+'">',
				'<i class=\"fa fa-book\"></i>Audit</button>',
			].join('')
		}
		<?php }?>
		
		//buttons
		function operateFormatter(value, row, index) {
			return [
				'<button type="button" class="btn btn-outline-secondary prodimg_btn btn-sm bg_arrow_lightblue" data-product="'+row.product_id+'">',
				'Files <span class="badge badge-light">'+row.filecount+'</span></button>',
				'<button type="button" class="btn btn-primary btn-sm bg_arrow_blue btn_itemtoggle" data-toggle="collapse" data-index="'+index+'" data-target=".itemprod'+row.product_id+'" data-prod="'+row.product_id+'">',
				'Items <span class="badge badge-light">'+row.itemcount+'</span></button>',
				'<form id="editprod_'+row.product_id+'" method="POST" action="productsetup.php"><input type="hidden" id="product_id" name="product_id" value="'+row.product_id+'">',
				'<input type="hidden" id="product_desc" name="product_desc" value="'+row.product_desc+'">',
				'<input type="hidden" id="product_mhw_code" name="product_mhw_code" value="'+row.product_mhw_code+'">',
				'<input type="hidden" id="brand_name" name="brand_name" value="'+row.brand_name+'">',
				'<input type=\"hidden\" name=\"edit\" value=\"1\">',
				'<input type="hidden" id="client_code" name="client_code" value="'+row.client_code+'">',
				'<input type="hidden" id="client_name" name="client_name" value="'+row.client_name+'"> </form>',
				'<button type="button" class="btn btn-dark btn-sm prodedit_btn bg_arrow_darkblue" data-target="'+row.product_id+'">',
				'<i class=\"far fa-edit\"></i> Edit</button>',
				<?php if ($isAdmin) { ?>
				'<button type="button" class="btn btn-delete-product btn-sm btn-danger" data-product="'+row.product_id+'">',
				'<i class=\"fa fa-trash\"></i>Delete</button>',
				'<button type="button" class="btn btn-dark btn-audit-product btn-sm bg_arrow_darkblue" data-product="'+row.product_id+'">',
				'<i class=\"fa fa-book\"></i>Audit</button>',
				<?php } ?>
			].join('')
		}
		function subtableBuilder ($el,qrytype, prodID) {
			var data;
			$el.bootstrapTable('showLoading');
			
			$el.data('editable-url', '/forms/update-inline.php?table=mhw_app_prod_item&product_id='+prodID);
			$el.data('id-field', 'item_id');

			$.post('query-request.php', {
				qrytype: qrytype,
				prodID: prodID
			}, function(dataIT) {

				data = JSON.parse(dataIT);

				//item sub table 
				$el.bootstrapTable('destroy').bootstrapTable('showLoading').bootstrapTable({
					data: data,
					formatLoadingMessage: function () {
						return '<span class="mhwLoading"><img src="css/img/Logo_Chevron_Animated.gif"/> Loading</span>';
					},
					columns: [
					[{
					  field: 'item_id',
						events: window.operateEvents,
						<?php if ($isAdmin) { ?>
						formatter: operateItemFormatter,
						<?php } ?>
						visible: true
					},{
					  field: 'item_client_code',
					  title: 'Item Code (client)',
					  sortable: true,
					  align: 'left'
					},
					{
					  title: 'Item Code (MHW)',
					  field: 'item_mhw_code',
					  align: 'left',
					  valign: 'middle',
					  sortable: true  
					},
					{
					  title: 'Item Description',
					  field: 'item_description',
					  align: 'left',
					  valign: 'middle',
					  <?=$editableField?>
					  sortable: true  
					},
					{
					  title: 'Container Type',
					  field: 'container_type',
					  align: 'left',
					  valign: 'middle',
						<?php if ($isAdmin) { ?>
							editable: {
								type: 'select',
								source: containerTypeOptions,
								title: 'Select Container type',
							},
						<?php } ?>
					  sortable: true  
					},
					{
					  title: 'Container Size',
					  field: 'container_size',
					  align: 'left',
					  valign: 'middle',
						<?php if ($isAdmin) { ?>
							editable: {
								type: 'select',
								source: containerSizeOptions,
								title: 'Select Container Size',
							},
						<?php } ?>
					  sortable: true  
					},
					{
					  title: 'Stock UOM',
					  field: 'stock_uom',
					  align: 'left',
					  valign: 'middle',
					  <?php if ($isAdmin) { ?>
						editable: {
							type: 'select',
							source: stockUomOptions,
							title: 'Select Stock UOM',
						},
					  <?php } ?>
					  sortable: true  
					},
					{
					  title: 'Bottles Per Case',
					  field: 'bottles_per_case',
					  align: 'left',
					  valign: 'middle',
					  <?=$editableField?>
					  sortable: true
					},
					{
					  title: 'UPC',
					  field: 'upc',
					  align: 'left',
					  valign: 'middle',
					  <?=$editableField?>
					  sortable: true  
					},
					{
					  title: 'SCC',
					  field: 'scc',
					  align: 'left',
					  valign: 'middle',
					  <?=$editableField?>
					  sortable: true  
					},
					{
					  title: 'Height',
					  field: 'height',
					  align: 'left',
					  valign: 'middle',
					  <?=$editableField?>
					  sortable: true  
					},
					{
					  title: 'Length',
					  field: 'length',
					  align: 'left',
					  valign: 'middle',
					  <?=$editableField?>
					  sortable: true  
					},
					{
					  title: 'Width',
					  field: 'width',
					  align: 'left',
					  valign: 'middle',
					  <?=$editableField?>
					  sortable: true  
					},
					{
					  title: 'Weight',
					  field: 'weight',
					  align: 'left',
					  valign: 'middle',
					  <?=$editableField?>
					  sortable: true  
					},
					{
					  title: 'Vintage',
					  field: 'vintage',
					  align: 'left',
					  valign: 'middle',
					  <?=$editableField?>
					  sortable: true  
					},
					{
					  title: 'Chill Storage',
					  field: 'chill_storage',
					  align: 'left',
					  valign: 'middle',
					  <?=$editableField?>
					  sortable: true  
					}
					]
				]
				}); //sub table init
			}); //POST
		}
		function tableBuilder (qrytype) {
			var data;
			var clientlist = $("#clientlist").val();
			
			var curr_page_number=$('#page_number').val();
			var curr_per_page_size=$('#per_page_size').val();
			var curr_filter_txt=$('#filter_txt').val();
			var curr_is_export_data=$('#is_export_data').val();
				
			$('#prodTable').bootstrapTable('showLoading');
			$.post('query-request.php', 
			{
				qrytype:qrytype,
				clientlist:clientlist,
				page_number:curr_page_number,
				per_page_size:curr_per_page_size,
				filter_txt : curr_filter_txt,
				active_pagination : 'yes'
			}, function(dataPT){ // ajax request query-request.php, send qrytype & client as post variable, return dataX variable, parse into JSON

				data = JSON.parse(dataPT);
				$("#total_rows_num").val(data.total);

				var winH = $(window).height();
				var navH = $(".navbar").height();
				var tblH = (winH-navH)-50;

				if(qrytype=="VPitems"){
					//product/item views

					$('#prodTable').data('editable-url', '/forms/update-inline.php?table=mhw_app_prod_item');
					$('#prodTable').data('id-field', 'item_id');

					$('#prodTable').bootstrapTable('destroy').bootstrapTable('showLoading').bootstrapTable({
						data: data.rows,
						height:tblH,
						stickyHeader:true,
						stickyHeaderOffsetY:60,
						formatLoadingMessage: function () {
							return  '<span class="mhwLoading"><img src="css/img/Logo_Chevron_Animated.gif"/> Loading</span>';
						},
						//fixedColumns:true,
						//fixedNumber:7,
						search : true,
						searchOnEnterKey : true,
						pagination: true,
						sidePagination : 'server',
						pageSize: parseInt(curr_per_page_size), 
						pageNumber  : parseInt(curr_page_number),
						pageList: [10, 25, 50, 100],
						totalRows: data.total,
						searchText : curr_filter_txt,
						columns: [
						[{
						  field: 'selected',
						  checkbox: true,
						  rowspan: 2,
						  align: 'center',
						  valign: 'middle'
						},
						{
						  field: 'item_id',
						  rowspan: 2,
						  align: 'center',
						  events: window.operateEvents,
						  <?php if ($isAdmin) { ?>
						  formatter: operateItemFormatter
						  <?php } ?>
						},
						{
						  field: 'client_name',
						  title: 'Client',
						  rowspan: 2,
						  align: 'center',
						  valign: 'middle',
						  visible: false
						},
						{
						  title: 'Product Detail',
						  colspan: 3,
						  align: 'center'
						},
						{
						  title: 'Item Detail',
						  colspan: 15,
						  align: 'center'
						}], 
						[{
						  field: 'brand_name',
						  title: 'Brand Name',
						  align: 'center',
						  valign: 'middle',
						  'id-field': 'product_id',
						  <?=$editableField?>
						  sortable: true 
						}, 
						{
						  title: 'Product Code (MHW)',
						  field: 'product_mhw_code',
						  align: 'center',
						  valign: 'middle',
						  sortable: true  
						},
						{
						  field: 'product_desc',
						  title: 'Product Description',
						  'id-field': 'product_id',
						  <?=$editableField?>
						  sortable: true,
						  align: 'center'
						},
						{
						  field: 'item_client_code',
						  title: 'Item Code (client)',
						  sortable: true,
						  align: 'center'
						},
						{
						  title: 'Item Code (MHW)',
						  field: 'item_mhw_code',
						  align: 'center',
						  valign: 'middle',
						  sortable: true  
						},
						{
						  title: 'Item Description',
						  field: 'item_description',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField?>
						  sortable: true  
						},
						{
						  title: 'Container Type',
						  field: 'container_type',
							align: 'left',
						  valign: 'middle',
							<?php if ($isAdmin) { ?>
								editable: {
									type: 'select',
									source: containerTypeOptions,
									title: 'Select Container type',
								},
							<?php } ?>
						  sortable: true  
						},
						{
						  title: 'Container Size',
						  field: 'container_size',
							align: 'left',
						  valign: 'middle',
							<?php if ($isAdmin) { ?>
								editable: {
									type: 'select',
									source: containerSizeOptions,
									title: 'Select Container Size',
								},
							<?php } ?>
						  sortable: true  
						},
						{
						  title: 'Stock UOM',
						  field: 'stock_uom',
							align: 'left',
						  valign: 'middle',
							<?php if ($isAdmin) { ?>
								editable: {
									type: 'select',
									source: stockUomOptions,
									title: 'Select Stock UOM',
								},
							<?php } ?>
						  sortable: true  
						},
						{
						  title: 'Bottles Per Case',
						  field: 'bottles_per_case',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField?>
						  sortable: true
						},
						{
						  title: 'UPC',
						  field: 'upc',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField?>
						  sortable: true  
						},
						{
						  title: 'SCC',
						  field: 'scc',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField?>
						  sortable: true  
						},
						{
						  title: 'Height',
						  field: 'height',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField?>
						  sortable: true  
						},
						{
						  title: 'Length',
						  field: 'length',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField?>
						  sortable: true  
						},
						{
						  title: 'Width',
						  field: 'width',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField?>
						  sortable: true  
						},
						{
						  title: 'Weight',
						  field: 'weight',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField?>
						  sortable: true  
						},
						{
						  title: 'Vintage',
						  field: 'vintage',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField?>
						  sortable: true  
						},
						{
						  title: 'Chill Storage',
						  field: 'chill_storage',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField?>
						  sortable: true  
						}]
						
					]
					}); //table init
				} else {
					$('#prodTable').data('editable-url', '/forms/update-inline.php?table=mhw_app_prod');
					$('#prodTable').data('id-field', 'product_id');

					const productDetailsColumns = [
						{
						  field: 'brand_name',
						  title: 'Brand Name',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField?>
						  sortable: true 
						}, 
						{
						  title: 'Product Code (MHW)',
						  field: 'product_mhw_code',
						  align: 'center',
						  valign: 'middle',
						  sortable: true  
						},
						{
						  field: 'product_desc',
						  title: 'Product Description',
						  <?=$editableField?>
						  sortable: true,
						  align: 'center'
						},
						{
						  field: 'TTB_ID',
						  title: 'TTB',
						  <?=$editableField?>
						  sortable: true,
						  align: 'center'
						},
						{
						  title: 'Federal Type',
						  field: 'federal_type',
						  align: 'center',
						  valign: 'middle',
						  <?php if ($isAdmin) { ?>
							editable: {
								type: 'select',
								source: federalTypeOptions,
								title: 'Select Federal type',
							},
						  <?php } ?>
						  sortable: true  
						},
						{
						  title: 'Compliance Type',
						  field: 'compliance_type',
						  align: 'center',
						  valign: 'middle',
						  <?php if ($isAdmin) { ?>
							editable: {
								type: 'select',
								source: complianceTypeOptions,
								title: 'Select Compliance type',
							},
						  <?php } ?>
						  sortable: true  
						},
						{
						  title: 'Product Class',
						  field: 'product_class',
						  align: 'center',
						  valign: 'middle',
						  <?php if ($isAdmin) { ?>
							editable: {
								type: 'select',
								source: productClassOptions,
								title: 'Select Product Class',
							},
						  <?php } ?>
						  sortable: true  
						},
						{
						  title: 'Mktg Product Type',
						  field: 'mktg_prod_type',
						  align: 'center',
						  valign: 'middle',
						  <?php if ($isAdmin) { ?>
							editable: {
								type: 'select',
								source: marketingCategoryOptions,
								title: 'Select Marketing Category',
							},
						  <?php } ?>
						  sortable: true  
						},
						{
						  title: 'Beverage Type',
						  field: 'bev_type',
						  align: 'center',
						  valign: 'middle',
						  <?php if ($isAdmin) { ?>
							editable: {
								type: 'select',
								source: beverageTypeOptions,
								title: 'Select Beverage Type',
							},
						  <?php } ?>
						  sortable: true  
						},
						{
						  title: 'Fanciful',
						  field: 'fanciful',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField?>
						  sortable: true,
						  visible: false
						},
						{
						  title: 'Country',
						  field: 'country',
						  align: 'center',
						  valign: 'middle',
						  <?php if ($isAdmin) { ?>
							editable: {
								type: 'select',
								source: countries,
								title: 'Select Country',
							},
						  <?php } ?>
						  sortable: true  
						},
						{
						  title: 'Appellation',
						  field: 'appellation',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField?>
						  sortable: true  
						},
						{
						  title: 'Lot Item',
						  field: 'lot_item',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField?>
						  sortable: true  
						},
						/*{
						  title: 'Bottle Material',
						  field: 'bottle_material',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField?>
						  sortable: true  
						},*/
						{
						  title: 'Alcohol %',
						  field: 'alcohol_pct',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField?>
						  sortable: true  
						}
					];
					if (qrytype=="VPunfinalized" || qrytype=="VPfinalized") {
						productDetailsColumns.unshift(
							{
								field: 'rejremark',
								title: 'MHW Review',
								align: 'center',
								valign: 'middle',
						  sortable: true  
							}
						);
					}
					const supplierDetailsColumns = [
						{
						  title: 'Supplier Name',
						  field: 'supplier_name',
						  align: 'center',
						  valign: 'middle',
						  <?=$editableField?>
						  sortable: true  
						},
						{
						  field: 'supplier_contact',
						  title: 'Contact',
						  <?=$editableField?>
						  sortable: true,
						  align: 'center'
						},
						{
						  field: 'supplier_fda_number',
						  title: 'FDA #',
						  <?=$editableField?>
						  sortable: true,
						  align: 'center'
						},
						{
						  field: 'tax_reduction_allocation',
						  title: 'Tax Reduction Allocation',
						  <?=$editableField?>
						  sortable: true,
						  align: 'center'
						},
						{
						  field: 'supplier_address_1',
						  title: 'Address 1',
						  <?=$editableField?>
						  sortable: true,
						  align: 'center',
						  visible:false
						},
						{
						  field: 'supplier_address_2',
						  title: 'Address 2',
						  <?=$editableField?>
						  sortable: true,
						  align: 'center',
						  visible:false
						},
						{
						  field: 'supplier_address_3',
						  title: 'Address 3',
						  <?=$editableField?>
						  sortable: true,
						  align: 'center',
						  visible:false
						},
						{
						  field: 'supplier_city',
						  title: 'City',
						  <?=$editableField?>
						  sortable: true,
						  align: 'center',
						  visible:false
						},
						{
						  field: 'supplier_state',
						  title: 'State',
						  <?=$editableField?>
						  sortable: true,
						  align: 'center',
						  visible:false
						},
						{
						  field: 'supplier_country',
						  title: 'Country',
						  <?php if ($isAdmin) { ?>
							editable: {
								type: 'select',
								source: countries,
								title: 'Select Country',
							},
						  <?php } ?>
						  sortable: true,
						  align: 'center',
						  visible:false
						},
						{
						  field: 'supplier_zip',
						  title: 'Zip',
						  <?=$editableField?>
						  sortable: true,
						  align: 'center',
						  visible:false
						},
						{
						  field: 'supplier_phone',
						  title: 'Phone',
						  <?=$editableField?>
						  sortable: true,
						  align: 'center',
						  visible:false
						},
						{
						  field: 'supplier_email',
						  title: 'Email',
						  <?=$editableField?>
						  sortable: true,
						  align: 'center',
						  visible:false
						},
						{
						  field: 'federal_basic_permit',
						  title: 'Federal Basic Permit',
						  <?=$editableField?>
						  sortable: true,
						  align: 'center',
						  visible:false
				}
					];
					const tableColumns = [
						[{
						  field: 'selected',
						  checkbox: true,
						  rowspan: 2,
						  align: 'center',
						  valign: 'middle'
						},
						{
						  field: 'product_id',
						  rowspan: 2,
						  align: 'center',
						  events: window.operateEvents,
						  formatter: operateFormatter
						}, 
						{
						  field: 'client_name',
						  title: 'Client',
						  rowspan: 2,
						  align: 'center',
						  valign: 'middle',
						  visible: false
						},
						{
						  title: 'Product Detail',
						  colspan: productDetailsColumns.length-1,
						  align: 'center'
						},
						{
						  title: 'Supplier Detail',
						  colspan: supplierDetailsColumns.length,
						  align: 'center'
						}], 
						[...productDetailsColumns, ...supplierDetailsColumns]
					];
					//product/supplier views
					$('#prodTable').bootstrapTable('destroy').bootstrapTable('showLoading').bootstrapTable({
						data: data.rows,
						height:tblH,
						stickyHeader:true,
						stickyHeaderOffsetY:60,
						formatLoadingMessage: function () {
							return '<span class="mhwLoading"><img src="css/img/Logo_Chevron_Animated.gif"/> Loading</span>';
						},
						detailView: true,
						onExpandRow: function (index, row, $detail) {
							expandTable($detail,row)
						},
						//fixedColumns:true,
						//fixedNumber:7,
						search : true,
						searchOnEnterKey : true,
						pagination: true,
						sidePagination : 'server',
						pageSize: parseInt(curr_per_page_size), 
						pageNumber  : parseInt(curr_page_number),
						pageList: [10, 25, 50, 100],
						totalRows: data.total,
						searchText : curr_filter_txt,
						columns: tableColumns
					}); //table init
				}

				var viewname_raw = $("#screenview_name").val();
				if(viewname_raw=="Not Finalized") { 

					$table.bootstrapTable('checkAll');  //check all Not Finalized by default
				}

				modalStuff(); //bind modal & buttons

				$table.on('check.bs.table uncheck.bs.table ' +
				  'check-all.bs.table uncheck-all.bs.table',
				function () {
				  $remove.prop('disabled', !$table.bootstrapTable('getSelections').length)

				  // save your data, here just save the current page
				  selections = getIdSelections()
				  // push or splice the selections if you want to save all data selections

				})
				$table.on('all.bs.table', function (e, name, args) {
				  console.log(name, args)
				  modalStuff(); //when anything happens (filter, click, etc) re-bind modal & buttons
				})
				
							
			$table.on('page-change.bs.table', function (e, page_number, per_page_size) {
			  console.log("page_number::"+page_number);
			  console.log("per_page_size::"+per_page_size);
			  var curr_page_number=$('#page_number').val();
			  var curr_per_page_size=$('#per_page_size').val();
              if(curr_page_number!=page_number || curr_per_page_size!=per_page_size)
			  {
				  $("#page_number").val(page_number);
			      $("#per_page_size").val(per_page_size);
				  
				  var viewname_raw = $('#screenview_name').val();
				   console.log("viewname_raw::"+viewname_raw);
					if(viewname_raw=="Not Finalized") { 
						viewname = "VPunfinalized"; 
					}
					else if(viewname_raw=="Processing") { viewname = "VPpending"; }
					else if(viewname_raw=="Finalized") { viewname = "VPfinalized"; }
					else if(viewname_raw=="Items") { viewname = "VPitems"; }
					else { viewname = "VPdefault"; }
                     console.log("viewname::"+viewname);
					tableBuilder(viewname);

			  }			  
			});
			
			$table.on('search.bs.table', function (e, searching_text) {
			  console.log("searching_text::"+searching_text);	
              var curr_filter_txt=$('#filter_txt').val();
              if(curr_filter_txt!=searching_text)
			  {
				  $("#filter_txt").val(searching_text);
			      
				    var viewname_raw = $('#screenview_name').val();
					console.log("inside search viewname_raw::"+viewname_raw);
					if(viewname_raw=="Not Finalized") { 
						viewname = "VPunfinalized"; 
					}
					else if(viewname_raw=="Processing") { viewname = "VPpending"; }
					else if(viewname_raw=="Finalized") { viewname = "VPfinalized"; }
					else if(viewname_raw=="Items") { viewname = "VPitems"; }
					else { viewname = "VPdefault"; }
					
                    console.log("inside search viewname::"+viewname);
					tableBuilder(viewname);
			  }			  
			});
			

			$table.on('load-error.bs.table', function (e, status) {
				$('.mhwLoading').html(errorMessage);
			});
						
			$("#data_export").submit(function(e){
				
				var export_total_rows_num=$('#total_rows_num').val();
				export_total_rows_num=parseInt(export_total_rows_num);
				if(export_total_rows_num>35000)
				{
					e.preventDefault();
                    e.stopImmediatePropagation();				
					console.log(export_total_rows_num);
					alert("Error Exporting, dataset too large, please try filtering data with Advanced Search or use pre-filtered Reports screen");
					return false;
				}
				else 
				{
				   var viewname_raw = $('#screenview_name').val();
				   console.log("inside export viewname_raw::"+viewname_raw);
					if(viewname_raw=="Not Finalized") { 
						viewname = "VPunfinalized"; 
					}
					else if(viewname_raw=="Processing") { viewname = "VPpending"; }
					else if(viewname_raw=="Finalized") { viewname = "VPfinalized"; }
					else if(viewname_raw=="Items") { viewname = "VPitems"; }
					else { viewname = "VPdefault"; }

                 console.log("inside export viewname::"+viewname);
				 
				$("#export_qry_type").val(viewname);
				 
                var export_client = $('#client_name').val();
				$("#export_client").val(export_client);
				 
				var export_filter_txt=$('#filter_txt').val();
				$("#export_filter_txt").val(export_filter_txt);	
				
				return true;
				}
			});
			
				
				
				
				$remove.click(function () {
				  var ids = getIdSelections()
				  $table.bootstrapTable('remove', {
					field: 'id',
					values: ids
				  })
				  $remove.prop('disabled', true)
				})
		
				
				
		
			}); //POST
			
			

		} //unnamed function wrapping table init

		var initialview = '<?php echo $v; ?>';
		if(initialview=="nf"){
			tableBuilder("VPunfinalized"); //initialize table
			$("#screenview_name").val("Not Finalized");
			$("#finalize").show();
		}
		else{
			tableBuilder("VPdefault"); //initialize table
		}

		$("#screenview_name").on("change", function () {
			var viewname_raw =  $(this).val();
			$("#finalize").hide();
			if(viewname_raw=="Not Finalized") { 
				viewname = "VPunfinalized"; 
				$("#finalize").show();
			}
			else if(viewname_raw=="Processing") { viewname = "VPpending"; }
			else if(viewname_raw=="Finalized") { viewname = "VPfinalized"; }
			else if(viewname_raw=="Items") { viewname = "VPitems"; }
			else { viewname = "VPdefault"; }

			tableBuilder(viewname);
		});

		$("#finalize").on("click", function () {
			var ids = getIdSelections();
			var codes = getCodeSelections();
			var qrytype = "processFinalize";
			var prodlist = JSON.stringify(ids);
			var codelist = JSON.stringify(codes);
			var clientlist = $("#clientlist").val(); 
			//codes = codes.replace('[','');
			//codes = codes.replace(']','');
			//codes = codes.replace('"','');
			//var codelist=codes;

			$.post('query-request.php', {
				qrytype: qrytype,
				prodlist: prodlist
			}, function(dataPF){
				alertTray('alertFinalize',prodlist,codelist);

				$("#finalize").hide();
				$("#screenview_name").val("Processing");
				tableBuilder("VPpending");

				refreshAlert(); //viewproducts.php-specific function for updating nav (head.php) 
			});
		});
		
	});  //document ready
	</script>
	
  <div class="container-fluid">
	<div class="row justify-content-md-left float-left">
		<div class="col-md-auto oneField field-container-D" id="client_name-D">
			<label id="client_name-L" class="label preField " for="client_name"><b>Client Name</b></label>
			<div class="inputWrapper">
				<select id="client_name" name="client_name" title="Client Name" aria-required="true">
				<?php
				$clients = explode(";",$_SESSION['mhwltdphp_userclients']);
				if (count($clients)>1){  echo "<option value=\"all\" class=\"\">ALL</option>"; }
				foreach ($clients as &$clientvalue) {
					//$row_client_code=$client_info[$clientvalue];
					$row_client_code=$_SESSION['mhwltdphp_userclientcodes'];
					echo "<option value=\"".$row_client_code."\" class=\"\">".$clientvalue."</option>";
				}
				?>					
				</select>
			</div>
		</div>
		<div class="col-md-auto oneField field-container-D" id="screenview_name-D">
			<label id="screenview_name-L" class="label preField " for="screenview_name"><b>View</b></label>
			<div class="inputWrapper">
				<select id="screenview_name" name="screenview_name" title="Screen View" aria-required="true">
				<?php
				$screenviews = array("Products","Items","Not Finalized","Processing","Finalized");
				//if (count($screenviews)>1){  echo "<option value=\"all\" class=\"\">Default</option>"; }
				foreach ($screenviews as &$screenviewvalue) {
					echo "<option value=\"".$screenviewvalue."\" class=\"\">".$screenviewvalue."</option>";
				}
				?>					
				</select>
			</div>
		</div>
		
		<div class="col-md-auto align-self-center oneField field-container-D" id="add-new-D">
	        <form name="data_export" id="data_export" method="POST" action="export_data.php" target="_blank">
			<input type="hidden" name="export_view_type" id="export_view_type" value="viewproducts">
			<input type="hidden" name="export_qry_type" id="export_qry_type" value="VPitems">
			<input type="hidden" name="export_client" id="export_client" value="">
			<input type="hidden" name="export_prodlistids" id="export_prodlistids" value="">
			<input type="hidden" name="export_searching_data" id="export_searching_data" value="">
			<input type="hidden" name="export_filter_txt" id="export_filter_txt" value="">
    		<button type="submit" id="export_data_btn" class="btn btn-primary"><i class="fa fa-download"></i> <span id='export_data_sp'>Export Data</span></button>
			</form>
		</div>
		
		<div class="col-md-auto align-self-center oneField field-container-D" id="finalize-D">
		<button id="finalize" class="btn btn-success"><i class="fas fa-lock"></i> Finalize</button>
		</div>
		
		<input type="hidden" name="page_number" id="page_number" value="1">
		<input type="hidden" name="per_page_size" id="per_page_size" value="10">
		<input type="hidden" name="filter_txt" id="filter_txt" value="">
		<input type="hidden" name="is_export_data" id="is_export_data" value="no">
		<input type="hidden" name="total_rows_num" id="total_rows_num" value='0'>
				
	</div>
    <table id="prodTable" class="table table-hover" 
		data-toggle="table"  
		data-show-pagination-switch="true"  data-show-columns="true" data-show-toggle="true" 
		data-search="true" 
		data-id-field="" data-editable-emptytext="...." data-editable-url="#" data-pagination="true">
	
	<!--<table
  id="table"
  data-toolbar="#toolbar"
  data-search="true"
  data-show-refresh="true"
  data-show-toggle="true"
  data-show-fullscreen="true"
  data-show-columns="true"
  data-detail-view="true"
  data-show-export="true"
  data-click-to-select="true"
  data-detail-formatter="detailFormatter"
  data-minimum-count-columns="2"
  data-show-pagination-switch="true"
  data-pagination="true"
  data-id-field="id"
  data-page-list="[10, 25, 50, 100, all]"
  data-show-footer="true"
  data-side-pagination="server"
  data-response-handler="responseHandler">-->

	  <?php
	  	//$clientlist =  "'".str_replace(";","','",$_SESSION['mhwltdphp_userclients'])."'";
	    //$clientlist =  "'".str_replace(";",",",$_SESSION['mhwltdphp_userclients'])."'";
	    $clientlist =  "'".$_SESSION['mhwltdphp_userclientcodes']."'";

	/*	echo "<thead><tr>";
		 echo "<th>&nbsp;<span style=\"display:none;\">".$clientlist."</span></th>";
		  echo ("<th>Client Name</th>
		  <th>Brand Name</th>
          <th>Product Code (MHW)</th>
          <th>Product Description</th>
		  <th>TTB ID</th>
		  <th>Federal Type</th>
		  <th>Compliance Type</th>
		  <th>Product Class</th>
		  <th>Marketing Product Type</th>
		  <th>Beverage Type</th>
		  <th>Fanciful</th>
		  <th>Country of Origin</th>
		  <th>Appellation</th>
		  <th>Lot Item</th>
		  <th>Bottle Material</th>
		  <th>Alcohol %</th>
		  
		  <th>Supplier</th>
		  <th>Contact</th>
          <th>FDA Number</th>
          <th>Tax Reduction Allocation</th>
          <th>Phone</th>
          <th>Email</th>
		  
        </tr></thead>
      <tbody>".PHP_EOL);
*/
/*
	  	//Establishes the connection
	    $conn = sqlsrv_connect($serverName, $connectionOptions);
	    $tsql= "SELECT p.[product_id]
		      ,p.[client_code]
		      ,p.[client_name]
		      ,p.[brand_name]
		      ,p.[product_desc]
		      ,p.[product_mhw_code]
		      ,p.[product_mhw_code_search]
		      ,p.[TTB_ID]
		      ,p.[federal_type]
		      ,p.[compliance_type]
		      ,p.[product_class]
		      ,p.[mktg_prod_type]
		      ,p.[bev_type]
		      ,p.[fanciful]
		      ,p.[country]
		      ,p.[appellation]
		      ,p.[lot_item]
		      ,p.[bottle_material]
		      ,p.[alcohol_pct]
		      ,p.[create_date]
			  ,s.[supplier_id]
		      ,s.[supplier_name]
		      ,s.[supplier_contact]
		      ,s.[supplier_fda_number]
		      ,s.[tax_reduction_allocation]
		      ,s.[supplier_address_1]
		      ,s.[supplier_address_2]
		      ,s.[supplier_address_3]
		      ,s.[supplier_city]
		      ,s.[supplier_state]
		      ,s.[supplier_country]
		      ,s.[supplier_zip]
		      ,s.[supplier_phone]
		      ,s.[supplier_email]
			  ,s.[federal_basic_permit]
		      ,s.[create_via]
		      ,s.[create_date]
		      ,s.[edit_date]
			  ,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_item] i WHERE i.[product_id] = p.[product_id] AND i.[active] = 1 AND i.[deleted] = 0) as 'itemcount'
			  ,(SELECT COUNT(*) FROM [dbo].[mhw_app_prod_image] i2 WHERE i2.[product_id] = p.[product_id] AND i2.[active] = 1 AND i2.[deleted] = 0) as 'filecount'
	         FROM [dbo].[mhw_app_prod] p WITH (NOLOCK)
			 LEFT OUTER JOIN [dbo].[mhw_app_prod_supplier] s WITH (NOLOCK) on s.[supplier_id] = p.[supplier_id]
			 WHERE p.[client_name] IN (".$clientlist.") AND p.[active] = 1 AND p.[deleted] = 0 AND s.[active] = 1 AND s.[deleted] = 0 ORDER BY p.[create_date] desc";
			 
	    $getResults= sqlsrv_query($conn, $tsql);

	    if ($getResults == FALSE)
	        echo (sqlsrv_errors());
	*/		
	    while ($row = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
			$i_supplier=0; //reset supplier count at each product
			$i_item=0; //reset item count at each product
			
			echo "<tr>";
			//echo "<td><button type=\"button\" class=\"btn btn-primary prodimgbtn\" data-toggle=\"modal\" data-target=\"#fileModal\" data-product=\"".$row['product_id']."\">";
					echo "<td><button type=\"button\" class=\"btn btn-outline-secondary prodimg_btn btn-sm bg_arrow_lightblue\" data-product=\"".$row['product_id']."\">";
					echo ("Files <span class=\"badge badge-light\">".$row['filecount']."</span></button>
					<button type=\"button\" class=\"btn btn-primary btn-sm bg_arrow_blue btn_itemtoggle\" data-toggle=\"collapse\" data-target=\".itemprod".$row['product_id']."\" data-prod=\"".$row['product_id']."\">
					Items <span class=\"badge badge-light\">".$row['itemcount']."</span></button>");
					echo "<form id=\"editprod_".$row['product_id']."\" method=\"POST\" action=\"productsetup.php\"><input type=\"hidden\" id=\"product_id\" name=\"product_id\" value=\"".$row['product_id']."\"><input type=\"hidden\" id=\"product_desc\" name=\"product_desc\" value=\"".$row['product_desc']."\"><input type=\"hidden\" id=\"brand_name\" name=\"brand_name\" value=\"".$row['brand_name']."\"><input type=\"hidden\" id=\"client_name\" name=\"client_name\" value=\"".$row['client_name']."\"><input type=\"hidden\" name=\"edit\" value=\"1\"></form>";
					echo ("<button type=\"button\" class=\"btn btn-dark btn-sm prodedit_btn bg_arrow_darkblue\" data-target=\"".$row['product_id']."\">
					<i class=\"far fa-edit\"></i> Edit</button></td>");

	     	echo ("<td>".$row['client_name']."</td><td>".$row['brand_name']."</td><td>".$row['product_mhw_code']."</td><td>".$row['product_desc']."</td>
					<td>".$row['TTB_ID']."</td><td>".$row['federal_type']."</td><td>".$row['compliance_type']."</td><td>".$row['product_class']."</td>
					<td>".$row['mktg_prod_type']."</td><td>".$row['bev_type']."</td><td>".$row['fanciful']."</td><td>".$row['country']."</td><td>".$row['appellation']."</td>
					<td>".$row['lot_item']."</td><td>".$row['bottle_material']."</td><td>".$row['alcohol_pct']."</td>
					<td>".$row['supplier_name']."</td>
			        <td>".$row['supplier_contact']."</td>
			        <td>".$row['supplier_fda_number']."</td>
			        <td>".$row['tax_reduction_allocation']."</td>
			        <td>".$row['supplier_phone']."</td>
			        <td>".$row['supplier_email']."</td>
					<td>".$row['federal_basic_permit']."</td>");
			echo ("</tr>". PHP_EOL);		
		 
		 /*
		 //begin supplier sub-loop
		  $tsql_supplier= "SELECT [supplier_id]
		      ,[supplier_name]
		      ,[supplier_contact]
		      ,[supplier_fda_number]
		      ,[tax_reduction_allocation]
		      ,[supplier_address_1]
		      ,[supplier_address_2]
		      ,[supplier_address_3]
		      ,[supplier_city]
		      ,[supplier_state]
		      ,[supplier_country]
		      ,[supplier_zip]
		      ,[supplier_phone]
		      ,[supplier_email]
		      ,[create_via]
		      ,[create_date]
		      ,[edit_date]
	         FROM [dbo].[mhw_app_prod_supplier] WHERE [product_id] = '".$row['product_id']."' AND [active] = 1 AND [deleted] = 0 ORDER BY [create_date] desc";
	    $getResults_supplier= sqlsrv_query($conn, $tsql_supplier);

	    if ($getResults_supplier == FALSE)
	        echo (sqlsrv_errors());
			
	    		while ($row_supplier = sqlsrv_fetch_array($getResults_supplier, SQLSRV_FETCH_ASSOC)) {
				if($i_supplier==0){

						echo ("<tr class=\"table-secondary\"><td class=\"hiddenRow fakeTH\"><div class=\"collapse suppprod".$row['product_id']."\">&nbsp;</div></td>
						<td class=\"hiddenRow fakeTH\"><div class=\"collapse suppprod".$row['product_id']."\">Supplier</div></td>
			            <td class=\"hiddenRow fakeTH\"><div class=\"collapse suppprod".$row['product_id']."\">Contact</div></td>
			            <td class=\"hiddenRow fakeTH\"><div class=\"collapse suppprod".$row['product_id']."\">FDA Number</div></td>
			            <td class=\"hiddenRow fakeTH\"><div class=\"collapse suppprod".$row['product_id']."\">Tax Reduction Allocation</div></td>
			            <td class=\"hiddenRow fakeTH\"><div class=\"collapse suppprod".$row['product_id']."\">Phone</div></td>
			            <td class=\"hiddenRow fakeTH\"><div class=\"collapse suppprod".$row['product_id']."\">Email</div></td></tr>". PHP_EOL);
					
				}
					 echo ("<tr class=\"table-secondary\"><td class=\"hiddenRow\"><div class=\"collapse suppprod".$row['product_id']."\">&nbsp;</div></td>
					 	<td class=\"hiddenRow\"><div class=\"collapse suppprod".$row['product_id']."\">".$row_supplier['supplier_name']."</div></td>
			            <td class=\"hiddenRow\"><div class=\"collapse suppprod".$row['product_id']."\">".$row_supplier['supplier_contact']."</div></td>
			            <td class=\"hiddenRow\"><div class=\"collapse suppprod".$row['product_id']."\">".$row_supplier['supplier_fda_number']."</div></td>
			            <td class=\"hiddenRow\"><div class=\"collapse suppprod".$row['product_id']."\">".$row_supplier['tax_reduction_allocation']."</div></td>
			            <td class=\"hiddenRow\"><div class=\"collapse suppprod".$row['product_id']."\">".$row_supplier['supplier_phone']."</div></td>
			            <td class=\"hiddenRow\"><div class=\"collapse suppprod".$row['product_id']."\">".$row_supplier['supplier_email']."</div></td></tr>". PHP_EOL);
						

					   
					$i_supplier++;   
			} //end supplier sub-loop
//			echo ("</table></div></td></tr>");
			sqlsrv_free_stmt($getResults_supplier);
			*/
			
			//begin item sub-loop
		  $tsql_item = "SELECT [item_id]
		      ,[item_client_code]
		      ,[item_mhw_code]
		      ,[item_description]
		      ,[container_type]
		      ,[container_size]
		      ,[stock_uom]
		      ,[bottles_per_case]
		      ,[upc]
		      ,[scc]
			  ,[vintage]
			  ,[various_vintages]
		      ,[item_status]
		      ,[create_via]
		      ,[create_date]
		      ,[edit_date]
	         FROM [dbo].[mhw_app_prod_item] WHERE [product_id] = '".$row['product_id']."' AND [active] = 1 AND [deleted] = 0 ORDER BY [create_date] desc";
	    $getResults_item= sqlsrv_query($conn, $tsql_item);

	    if ($getResults_item == FALSE)
	        echo (sqlsrv_errors());
			
	    		while ($row_item = sqlsrv_fetch_array($getResults_item, SQLSRV_FETCH_ASSOC)) {
				if($i_item==0){
					/*
					echo ("<tr><td class=\"hiddenRow\" colspan=\"2\"><div class=\"collapse suppprod".$row['product_id']."\">&nbsp;</div></td>
						<td class=\"hiddenRow\" colspan=\"15\"><div class=\"collapse itemprod".$row['product_id']."\">
						<table><tr><th class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">&nbsp;</div></th>
						<th class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">Item Client Code</div></th>
			            <th class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">Item MHW Code</div></th>
			            <th class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">Item Description</div></th>
			            <th class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">Container Type</div></th>
			            <th class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">Container Size</div></th>
			            <th class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">Stock UOM</div></th>
						<th class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">Bottles Per Case</div></th>
						<th class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">UPC</div></th>
						<th class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">SCC</div></th>
						</tr>". PHP_EOL);
						*/
						echo ("<tr data-card-visibile=\"true\" class=\"table-info thitem_prod".$row['product_id']."\"><td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\"><span style=\"display:none;\">".$row['client_name']."</span></div></td>
						<td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">Item Client Code</div></td>
			            <td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">Item MHW Code</div></td>
			            <td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">Item Description</div></td>
			            <td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">Container Type</div></td>
			            <td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">Container Size</div></td>
			            <td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">Stock UOM</div></td>
						<td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">Bottles Per Case</div></td>
						<td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">UPC</div></td>
						<td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">SCC</div></td>
						<td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">Vintage</div></td>
						<td class=\"hiddenRow fakeTH\"><div class=\"collapse itemprod".$row['product_id']."\">Various Vintages</div></td>
						</tr>". PHP_EOL);
					
				}
					 echo ("<tr data-card-visibile=\"true\" class=\"table-info tritem_prod".$row['product_id']."\"><td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\"><span style=\"display:none;\">".$row['client_name']."</span></div></td>
					 	<td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['item_client_code']."</div></td>
			            <td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['item_mhw_code']."</div></td>
			            <td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['item_description']."</div></td>
			            <td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['container_type']."</div></td>
			            <td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['container_size']."</div></td>
			            <td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['stock_uom']."</div></td>
						<td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['bottles_per_case']."</div></td>
						<td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['upc']."</div></td>
						<td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['scc']."</div></td>
						<td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['vintage']."</div></td>
						<td class=\"hiddenRow\"><div class=\"collapse itemprod".$row['product_id']."\">".$row_item['various_vintages']."</div></td>
						</tr>". PHP_EOL);
						  
					$i_item++;   
			} //end item sub-loop
//			echo ("</table></div></td></tr>");
			sqlsrv_free_stmt($getResults_item);
			
	    }
	sqlsrv_free_stmt($getResults);
		
	sqlsrv_close($conn);  
}

?>
      </tbody>
    </table>

<input type="hidden" id="clientlist" value="<?php echo  $clientlist; ?>">

<div class="modal modal-wide fade" id="fileModalx" tabindex="-1" role="dialog" aria-labelledby="fileModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="fileModalLabel">Files</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      </div>
	  <div id='preview'></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script>

$(document).ready(function(){
	//initialize prod image modal functionality
	$('.prodimg_btn').on('click',function(){
		var productid = $(this).data('product');
		var imageurl = 'viewfiles.php?pid='+productid;
		//alert(imageurl);
	    $('.modal-body').load(imageurl,function(){
	        $('#fileModalx').modal({show:true});
	    });
	});
	//initialize prod image download functionality for outside of modal
	$('.dwnld').on('click', function () {
		var imagefile = $(this).data('url');
		var imagename = $(this).data('imagename');
		$.ajax({
			url: imagefile,
			method: 'GET',
			xhrFields: {
				responseType: 'blob'
			},
			success: function (data) {
				var a = document.createElement('a');
				var url = window.URL.createObjectURL(data);
				a.href = url;
				a.download = imagename;
				a.click();
				window.URL.revokeObjectURL(url);
			}
		});
	});

	
	//make sure modal initializers are called when modal is shown
	//$('#fileModalx').on('shown.bs.modal', function (e) {
	//	modalStuff();
	//});
	
});

</script>

    </div>
<?php
    include('footer.php');
?>