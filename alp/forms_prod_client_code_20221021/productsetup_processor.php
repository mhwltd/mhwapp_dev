<?php
//=============settings============
include("settings.php");
//=============settings============

//=============functions============
include("functions.php");
//=============functions============
session_start();
$isAdmin = in_array($_SESSION['mhwltdphp_usertype'], ["SUPERUSER", "ADMIN"]);

if ($_SESSION['productsetup_submit_hash'] === $_POST['productsetup_submit_hash']) {
	// page refreshed with post data
	echo "Submitted already!";
} else {
	$_SESSION['productsetup_submit_hash'] = $_POST['productsetup_submit_hash'];
?>

<!--Main layout-->
<main>
<div class="container-fluid">
<!--<span id="processor_include">PROCESSOR LISTENING...</span>-->

<?php
	//echo "<pre>";
	//var_dump($_REQUEST); 
	//echo "</post>";
	
	//echo "FILES:<br />";
	//print_r($_FILES);

    include("dbconnect.php");
   
    //Establishes the connection
    $conn = sqlsrv_connect($serverName, $connectionOptions);
	if( $conn === false) {
    		die( print_r( sqlsrv_errors(), true));
	}
	
	/* added client name & client code block : start  */
	
	$client_code_info=array();
	$tsql_cli= "SELECT * FROM [vw_Clients] order by client_name";
	$stmt_cli = sqlsrv_query( $conn, $tsql_cli);
	if ( $stmt_cli === false ) die( print_r( sqlsrv_errors(), true));  
	
	while ( $row_cli = sqlsrv_fetch_array($stmt_cli, SQLSRV_FETCH_ASSOC) ) {               			
				$row_code_name=$row_cli['client_code'];
				$client_code_info[$row_code_name]=$row_cli['client_name']
	}			
	
    /* added client name & client code block : end  */

	$errorlog="\n";
	$reqlog="\n";
	$errorFree=1;

	$i=0; 
	$rows=intval($_POST['tfa_399'][0]);
	if($rows<1){ $rows=0; }
	//echo "rows:".$rows;

	$client_code = '';
	$post_client_code=$_POST['tfa_client_name']
	//$client_name = $client_code_info[$post_client_code];
	$client_name ='CE DEV CLIENT';
	$brand_name = str_replace("'","''",$_POST['tfa_brand_name']);
	$product_desc = str_replace("'","''",$_POST['tfa_product_desc']);
	$product_mhw_code = $_POST['product_mhw_code'];
	$product_mhw_code_search = $_POST['product_mhw_code_search'];
	$product_id = intval($_POST['prodID']);
	$user_id = $_POST['userID'];
	$supplier_id = intval($_POST['suppID']);

	while ($i<=$rows){
		/* ============BEGIN PRODUCT ======================== */ 
		//$_POST['tfa_federal_type'][$i]
		//product insert stored procedure
	    $tsql= "EXEC [dbo].[usp_product] 
		 @product_id = ".$product_id."
		,@client_code = '".$client_code."'
		,@client_name = '".$client_name."'
		,@brand_name = '".$brand_name."'
		,@product_desc = '".$product_desc."'
		,@product_mhw_code = '".$product_mhw_code."'
		,@product_mhw_code_search = '".$product_mhw_code_search."'
		,@TTB_ID = '".str_replace("'","",$_POST['tfa_TTB'])."'
		,@federal_type = '".$_POST['tfa_federal_type']."'
		,@compliance_type = '".$_POST['tfa_compliance_type']."'
		,@product_class = '".$_POST['tfa_product_class']."'
		,@mktg_prod_type = '".$_POST['tfa_mktg_prod_type']."'
		,@bev_type = '".$_POST['tfa_bev_type']."'
		,@fanciful = '".str_replace("'","''",$_POST['tfa_fanciful'])."'
		,@country = '".$_POST['tfa_country']."'
		,@appellation = '".str_replace("'","''",$_POST['tfa_appellation'])."'
		,@lot_item = '".$_POST['tfa_lot_item']."'
		,@varietal = '".str_replace("'","''",$_POST['tfa_varietal'])."'
		,@alcohol_pct = '".str_replace("'","",str_replace(",",".",$_POST['tfa_alcohol_pct']))."'
		,@create_via  = 'productsetup'
		,@username  = '".$user_id."'";

		$prodID = 0;
		$prodCODE = $product_mhw_code;
		
		//prep variables for re-filling the fields for resubmitting
		$pf_client_code = $client_code;
		$pf_client_name = $client_name;
		$pf_brand_name = $brand_name;
		$pf_product_desc = $product_desc;
		$pf_product_mhw_code = $product_mhw_code;
		$pf_product_mhw_code_search = $product_mhw_code_search;
		$pf_TTB_ID = $_POST['tfa_TTB'];
		$pf_federal_type = $_POST['tfa_federal_type'];
		$pf_compliance_type = $_POST['tfa_compliance_type'];
		$pf_product_class = $_POST['tfa_product_class'];
		$pf_mktg_prod_type = $_POST['tfa_mktg_prod_type'];
		$pf_bev_type = $_POST['tfa_bev_type'];
		$pf_fanciful = $_POST['tfa_fanciful'];
		$pf_country = $_POST['tfa_country'];
		$pf_appellation = $_POST['tfa_appellation'];
		$pf_lot_item = $_POST['tfa_lot_item'];
		$pf_bottle_material = $_POST['tfa_bottle_material'];
		$pf_alcohol_pct = $_POST['tfa_alcohol_pct'];
		
		
		/*
		//IN/OUT parameterized method
		
		$brand_name = $_POST['tfa_brand_name'][$i];
		$product_desc = $_POST['tfa_product_desc'][$i];
		$product_mhw_code = 'test';
		$product_mhw_code_search = 'test';
		$TTB_ID = $_POST['tfa_TTB'][$i];
		$federal_type = $_POST['tfa_federal_type'][$i];
		$compliance_type = $_POST['tfa_compliance_type'][$i];
		$product_class = $_POST['tfa_product_class'][$i];
		$mktg_prod_type = $_POST['tfa_mktg_prod_type'][$i];
		$bev_type = $_POST['tfa_bev_type'][$i];
		$fanciful = $_POST['tfa_fanciful'][$i];
		$country = $_POST['tfa_country'][$i];
		$appellation = $_POST['tfa_appellation'][$i];
		$lot_item = $_POST['tfa_lot_item'][$i];
		$bottle_material = $_POST['tfa_bottle_material'][$i];
		$alcohol_pct = $_POST['tfa_alcohol_pct'][$i];
		$create_via  = 'productsetup';
		
		$tsql= "{EXEC [dbo].[usp_product] (@client_code = ?, @client_name = ?, @brand_name = ?, @product_desc = ?, @product_mhw_code = ?, @product_mhw_code_search = ?, @TTB_ID = ?, @federal_type = ?, @compliance_type = ?, @product_class = ?, @mktg_prod_type = ?, @bev_type = ?, @fanciful = ?, @country = ?, @appellation = ?, @lot_item = ?, @bottle_material = ?, @alcohol_pct = ?, @create_via = ?, @prodID = ?)}";
		
		$tsqlparams = array(
			array($client_code, SQLSRV_PARAM_IN),
			array($client_name, SQLSRV_PARAM_IN),
			array($brand_name, SQLSRV_PARAM_IN),
			array($product_desc, SQLSRV_PARAM_IN),
			array($product_mhw_code, SQLSRV_PARAM_IN),
			array($product_mhw_code_search, SQLSRV_PARAM_IN),
			array($TTB_ID, SQLSRV_PARAM_IN),
			array($federal_type, SQLSRV_PARAM_IN),
			array($compliance_type, SQLSRV_PARAM_IN),
			array($product_class, SQLSRV_PARAM_IN),
			array($mktg_prod_type, SQLSRV_PARAM_IN),
			array($bev_type, SQLSRV_PARAM_IN),
			array($fanciful, SQLSRV_PARAM_IN),
			array($country, SQLSRV_PARAM_IN),
			array($appellation, SQLSRV_PARAM_IN),
			array($lot_item, SQLSRV_PARAM_IN),
			array($bottle_material, SQLSRV_PARAM_IN),
			array($alcohol_pct, SQLSRV_PARAM_IN),
			array($create_via, SQLSRV_PARAM_IN),
			array(&$prodID, SQLSRV_PARAM_OUT)
		);
		
		//$stmt = sqlsrv_query($conn, $tsql, $tsqlparams);
		//$stmt = sqlsrv_prepare($conn, $tsql, $tsqlparams);
		//sqlsrv_execute($stmt);
		*/
		
		//echo  $tsql."<br />";
		   	  
		$stmt = sqlsrv_query($conn, $tsql);
		
		while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
			$prodID = $row['prodID']; 
			$prodCODE = $row['prodCODE']; 
		}
		//echo  "PRODID:".$prodID;
			 
		if( $stmt === false ) {
			if( ($errors = sqlsrv_errors() ) != null) {
				//log sql errors for writing to server later
				foreach( $errors as $error ) {
					$errorlog.= $tsql."\n";
					$errorlog.= "SQLSTATE: ".$error[ 'SQLSTATE']."\n";
					$errorlog.= "code: ".$error[ 'code']."\n";
					$errorlog.= "message: ".$error[ 'message']."\n";
				}
			}
			$errorFree=0;
		}
	  
		//release sql statement
	  	sqlsrv_free_stmt($stmt);
		/* ============END PRODUCT ======================== */ 
		
		//if($prodID>0){
			/* ============BEGIN SUPPLIER ======================== */ 
			 $tsql_supplier= "EXEC [dbo].[usp_product_supplier] 
			 @supplier_id =".$supplier_id."
			,@product_id =".$prodID."
			,@supplier_name = '".str_replace("'","''",$_POST['tfa_supplier_name'])."'
			,@supplier_contact = '".str_replace("'","''",$_POST['tfa_supplier_contact'])."'
			,@supplier_fda_number = '".str_replace("'","",$_POST['tfa_supplier_fda_number'])."'
			,@tax_reduction_allocation = '".str_replace(",",".",str_replace("'","",$_POST['tfa_tax_reduction_allocation']))."'
			,@supplier_address_1 = '".str_replace("'","''",$_POST['tfa_supplier_address_1'])."'
			,@supplier_address_2 = '".str_replace("'","''",$_POST['tfa_supplier_address_2'])."'
			,@supplier_address_3  = '".str_replace("'","''",$_POST['tfa_supplier_address_3'])."'
			,@supplier_city = '".str_replace("'","''",$_POST['tfa_supplier_city'])."'
			,@supplier_state = '".str_replace("'","''",$_POST['tfa_supplier_state'])."'
			,@supplier_country  = '".str_replace("'","''",$_POST['tfa_supplier_country'])."'
			,@supplier_zip = '".str_replace("'","",$_POST['tfa_supplier_zip'])."'
			,@supplier_phone = '".str_replace("'","",$_POST['tfa_supplier_phone'])."'
			,@supplier_email = '".str_replace("'","",$_POST['tfa_supplier_email'])."'
			,@create_via = 'productsetup'
			,@username = '".$user_id."'
			,@client_code = '".$client_code."'
			,@client_name = '".$client_name."'
			,@federal_permit = '".str_replace("'","",$_POST['tfa_supplier_fed_permit'])."'"; 
			
			//prep variables for re-filling the fields for resubmitting
			$pf_supplier_name = $_POST['tfa_supplier_name'];
			$pf_supplier_contact = $_POST['tfa_supplier_contact'];
			$pf_supplier_fda_number = $_POST['tfa_supplier_fda_number'];
			$pf_tax_reduction_allocation = $_POST['tfa_tax_reduction_allocation'];
			$pf_supplier_address_1 = $_POST['tfa_supplier_address_1'];
			$pf_supplier_address_2 = $_POST['tfa_supplier_address_2'];
			$pf_supplier_address_3  = $_POST['tfa_supplier_address_3'];
			$pf_supplier_city = $_POST['tfa_supplier_city'];
			$pf_supplier_state = $_POST['tfa_supplier_state'];
			$pf_supplier_country  = $_POST['tfa_supplier_country'];
			$pf_supplier_zip = $_POST['tfa_supplier_zip'];
			$pf_supplier_phone = $_POST['tfa_supplier_phone'];
			$pf_supplier_email = $_POST['tfa_supplier_email'];
			$pf_supplier_fed_permit = $_POST['tfa_supplier_fed_permit'];
		
			$suppID = 0;

			if($prodID>0){  //product_id set
				$stmt_supplier = sqlsrv_query($conn, $tsql_supplier);
			
				while ($row_supplier = sqlsrv_fetch_array($stmt_supplier, SQLSRV_FETCH_ASSOC)) {
					$suppID = $row_supplier['suppID']; 
				}
				//echo  "SUPPID:".$suppID;
				
				if( $stmt_supplier === false ) {
					if( ($errors = sqlsrv_errors() ) != null) {
						//log sql errors for writing to server later
						foreach( $errors as $error ) {
							$errorlog.= $tsql_supplier."\n";
							$errorlog.= "SQLSTATE: ".$error[ 'SQLSTATE']."\n";
							$errorlog.= "code: ".$error[ 'code']."\n";
							$errorlog.= "message: ".$error[ 'message']."\n";
						}
					}
					$errorFree=0;
				}
			  
				//release sql statement
				sqlsrv_free_stmt($stmt_supplier);
			}  //end product_id
			else{
				$errorlog.= $tsql_supplier."\n";
			}
			
			/* ============END SUPPLIER ======================== */ 
			
			
			/* ============BEGIN IMAGE ======================== */ 
			$iii=0;
			//$imgrows=intval(count($_FILES['tfa_image']['name'][$i]))-1;
			$imgrows=intval(count($_FILES['tfa_image']['name']))-1;
			if($imgrows<1){ $imgrows=0; }
			//echo "imgrows:".$imgrows;
					
			while ($iii<=$imgrows){
				/*BEGIN FILE UPLOAD*/
				/* Getting file name */
				//$filename = $_FILES['tfa_image']['name'][$i][$iii];
				$filename = convert_ascii($_FILES['tfa_image']['name'][$iii]);
				$filetype = str_replace(" ", "",$_POST['tfa_image_type'][$iii]);

				/* Location */
				$location = $upload_dir.$filename;
				$uploadOk = 1;
				$imageFileType = pathinfo($location,PATHINFO_EXTENSION);

				copy($_FILES['tfa_image']['tmp_name'],$upload_dir."temp/".strtotime("now").$filename);

				//check if no file selected.
				if(!is_uploaded_file($_FILES['tfa_image']['tmp_name'][$iii])){
					$pageerr="Upload Failure: Please select a file to upload.\n";
					$uploadOk=0;
				}
				if($pageerr==""){
					//check if the directory exists or not.
					if (!is_dir($upload_dir)) {
						$pageerr="Upload Failure: The upload directory does not exist.\n";
						$uploadOk=0;
					}
				}
				if($pageerr==""){
					//check if the directory is writable.
					if (!is_writeable($upload_dir)){
						$pageerr="Upload Failure: The upload directory is not writable.\n";
						$uploadOk=0;
					}
				}
				if($pageerr==""){
					//Get the Size of the File
					if(isset($size_bytes_limit)){
						$size = $_FILES['tfa_image']['size'][$iii];
						//Make sure that file size is correct
						if($size > $size_bytes_limit){
							$kb = $size_bytes_limit / 1024;
							$pageerr="Upload Failure: File size is too large. File must be under ".$kb." KB.\n";
							$uploadOk=0;
						}
					}
				} 
				if($pageerr==""){
					//Get the Size of the File
					$size = $_FILES['tfa_image']['size'][$iii];
					//Make sure that file size is correct
					if($size ==0){
						$pageerr="Upload Failure: Temporary file expired.  Please try again.\n";
					}
					
				} 
				if($pageerr==""){
					//check file extension
					$ext = strrchr($_FILES['tfa_image'][name][$iii],'.');
					$ext = strtolower($ext);
					if(($extlimit == "yes") && (!in_array($ext,$limitedext))){
						$pageerr="Upload Failure: File uploads of this type ".$ext." are not allowed.\n";
						$uploadOk=0;
					}
				}

				if($pageerr==""){
					//$filename will hold the value of the file name submitted from the form.
					$filename = 'p'.$prodID.'_'.$prodCODE.'_'.$filetype.'__'.convert_ascii($_FILES['tfa_image']['name'][$iii]);
					$filename = str_replace(' ','',$filename);
					$filename = str_replace('%20','',$filename);
					$filename = str_replace('+','',$filename);
					$filename = str_replace("'","",$filename);
					// Check if file is already exists.
					if(file_exists($upload_dir.$filename)){
						//$pageerr="Upload Failure: The file named $filename already exists.\n";
						//$uploadOk=0;
						$filename = 'p'.$prodID.'_'.$prodCODE.'_'.$filetype.'__'.strtotime("now").'_'.convert_ascii($_FILES['tfa_image']['name'][$iii]);
					   $filename = str_replace(' ','',$filename);
					   $filename = str_replace('%20','',$filename);
					   $filename = str_replace('+','',$filename);
					   $filename = str_replace("'","",$filename);
					}
				}
				
				if($pageerr==""){
					//echo $upload_dir.$filename;
					//Move the File to the Directory of your choice
					//move_uploaded_file('filename','destination') Moves file to a new location.
					if(move_uploaded_file($_FILES['tfa_image']['tmp_name'][$iii],$upload_dir.$filename)){
						createThumbs($upload_dir,$thumb_dir,$filename,200);
					}	
					else{
						$pageerr="Upload Failure: There was a problem moving your file.\n";
						$uploadOk=0;
					}
				}
				/***END FILE UPLOAD***/
				
				/***BEGIN FILE DB LOGGING***/
				 $tsql_img= "EXEC [dbo].[usp_product_image] 
					 @product_id =".$prodID."
					,@image_body = '".base64_encode($_FILES['filetoupload'])."'
					,@image_name = '".$filename."'
					,@image_dim = '".str_replace("'","",$_POST['tfa_image_dim'][$iii])."'
					,@image_type = '".$_POST['tfa_image_type'][$iii]."'
					,@image_note = ''
					,@create_via = 'productsetup'
					,@username = '".$user_id."'"; 

				if($uploadOk == 0){
					//upload validation/renaming failure
				   //echo $pageerr;
				   $IMGerrorFree=0;
				   $errorlog.= $tsql_img."\n";
				   $errorlog.= "file: ".$_FILES['tfa_image']['name'][$iii]."\n";
				   $errorlog.= "message: ".$pageerr."\n";
				}
				else{
					$imgID = 0;
					if($prodID>0){ //product_id set
						$stmt_img = sqlsrv_query($conn, $tsql_img);
					
						while ($row_img = sqlsrv_fetch_array($stmt_img, SQLSRV_FETCH_ASSOC)) {
							$imgID = $row_img['imgID']; 
						}
						//echo  "IMGID:".$imgID;
						
						if( $stmt_img === false ) {
							if( ($errors = sqlsrv_errors() ) != null) {
								//log sql errors for writing to server later
								foreach( $errors as $error ) {
									$errorlog.= $tsql_img."\n";
									$errorlog.= "SQLSTATE: ".$error[ 'SQLSTATE']."\n";
									$errorlog.= "code: ".$error[ 'code']."\n";
									$errorlog.= "message: ".$error[ 'message']."\n";
								}
							}
							$IMGerrorFree=0;
							$errorFree=0;
						} //end error handling
				  
						//release sql statement
						sqlsrv_free_stmt($stmt_img);
					} //end product_id
					else{
						$errorlog.= $tsql_img."\n";
					}
				} //end file upload success
				/***END FILE DB LOGGING***/
				//increment item counter
				$iii++;
			} //end item loop
			/* ============END IMAGE ======================== */ 
			
			
			
			/* ============BEGIN ITEM ======================== */ 
			$ii=0;
			//$itemrows=intval($_POST['tfa_825'][$i][0]);
			$itemrows=intval($_POST['tfa_825'][0]);
			if($itemrows<1){ $itemrows=0; }
			//echo "itemrows:".$itemrows;
					
			while ($ii<=$itemrows){
				if($_POST['clientcodeoverride'][$ii]=="1"){ $_POST['tfa_item_mhw_code'][$ii] = $_POST['tfa_client_code'][$ii]; }

				 $tsql_item= "EXEC [dbo].[usp_product_item] 
				 @product_id =".$prodID."
				,@item_client_code = '".str_replace("'","",$_POST['tfa_client_code'][$ii])."'
				,@item_mhw_code = '".str_replace("'","",$_POST['tfa_item_mhw_code'][$ii])."'
				,@item_description = '".str_replace("'","''",$_POST['tfa_mhw_item_description'][$ii])."'
				,@container_type = '".$_POST['tfa_container_type'][$ii]."'
				,@container_size = '".$_POST['tfa_container_size'][$ii]."'
				,@stock_uom = '".$_POST['tfa_stock_uom'][$ii]."'
				,@bottles_per_case = ".str_replace("'","",str_replace(",","",$_POST['tfa_bottles_per_case'][$ii]))."
				,@upc = '".str_replace("'","",str_replace(",","",$_POST['tfa_upc'][$ii]))."'
				,@scc = '".str_replace("'","",str_replace(",","",$_POST['tfa_scc'][$ii]))."'
				,@vintage  = '".str_replace("'","",str_replace(",","",$_POST['tfa_vintage'][$ii]))."'
				,@various_vintages = '".$_POST['tfa_various_vintages'][$ii]."'
				,@height = '".str_replace("'","",str_replace(",","",$_POST['tfa_height'][$ii]))."'
				,@length = '".str_replace("'","",str_replace(",","",$_POST['tfa_length'][$ii]))."'
				,@width = '".str_replace("'","",str_replace(",","",$_POST['tfa_width'][$ii]))."'
				,@weight = '".str_replace("'","",str_replace(",","",$_POST['tfa_weight'][$ii]))."'
				,@item_status = 'A'
				,@create_via = 'productsetup'
				,@username = '".$user_id."'
				,@client_description = '".convert_ascii(str_replace("'","''",$_POST['tfa_item_description'][$ii]))."'
				,@chill = '".$_POST['tfa_chill_storage'][$ii]."'
				,@bottle_material = '".$_POST['tfa_bottle_material'][$ii]."'
				,@outer_shipper = '".$_POST['tfa_outer_shipper'][$ii]."'
				,@pallet_cases = '".$_POST['tfa_pallet_cases'][$ii]."'
				,@pallet_layers_cases = '".$_POST['tfa_pallet_layer_cases'][$ii]."'
				,@pallet_layers = '".$_POST['tfa_pallet_layers'][$ii]."'"; 
				
				$itemID = 0;
				if($prodID>0){ //product_id set
					if($_POST['tfa_mhw_item_description'][$ii] && $_POST['tfa_mhw_item_description'][$ii]!='' && $_POST['tfa_mhw_item_description'][$ii]!=null){ //desc set
						$stmt_item = sqlsrv_query($conn, $tsql_item);
					
						while ($row_item = sqlsrv_fetch_array($stmt_item, SQLSRV_FETCH_ASSOC)) {
							$itemID = $row_item['itemID']; 
						}
						//echo  "ITEMID:".$itemID;
						
						if( $stmt_item === false ) {
							if( ($errors = sqlsrv_errors() ) != null) {
								//log sql errors for writing to server later
								foreach( $errors as $error ) {
									$errorlog.= $tsql_item."\n";
									$errorlog.= "SQLSTATE: ".$error[ 'SQLSTATE']."\n";
									$errorlog.= "code: ".$error[ 'code']."\n";
									$errorlog.= "message: ".$error[ 'message']."\n";
								}
							}
							$ITMerrorFree=0;
							$errorFree=0;
						} //end error handling
					  
						//release sql statement
						sqlsrv_free_stmt($stmt_item);
					} //end item_description
					else{
						$errorlog.= $tsql_item."\n";
					}
				} //end product_id
				else{
					$errorlog.= $tsql_item."\n";
				}
				//increment item counter
				$ii++;
			} //end item loop
			/* ============END ITEM ======================== */ 
		//} //end prodID dependent child sections

		
		//increment product counter
	  	$i++;
	} //end product loop

  	
	if ($isAdmin && $prodID>0) {
		//MHW user product auto-finalized

		//clear previous prod review
		$tsql_pre_1="UPDATE [dbo].[mhw_app_finalized_product_review] SET [active] = 0 WHERE [active] = 1 AND [processed] = 0 AND [prod_id] = ?";
		$stmt_pre_1 = sqlsrv_prepare($conn, $tsql_pre_1, array($product_id));
		if( $stmt_pre_1 === false )  
		{  
			echo "Statement could not be prepared.\n";  
			die( print_r( sqlsrv_errors(), true));  
		}  
		
		if( sqlsrv_execute($stmt_pre_1) === false )  
		{  
			echo "Statement could not be executed.\n";  
			die( print_r( sqlsrv_errors(), true));  
		}
		sqlsrv_free_stmt($stmt_pre_1);


		//add finalized product review record (set finalized queue to 0, not linked)
		$tsql_pre_2="INSERT INTO [dbo].[mhw_app_finalized_product_review] ([prod_id],[fq_id],[fq_username],[fq_useremail],[create_date],[active],[processed],[reviewer],[review_date],[remarks],[approvalstatus],[temp_status]) VALUES (?, 0, '".$user_id."', '".$_SESSION['mhwltdphp_useremail']."',GETDATE(),1,0,NULL,NULL,NULL,'Pending',(SELECT TOP 1 temp_status FROM [dbo].[mhw_app_finalized_product_review] pr WHERE pr.[prod_id] = ? ORDER by pr.active desc, pr.create_date desc));";
		$stmt_pre_2 = sqlsrv_prepare($conn, $tsql_pre_2, array($product_id,$product_id));
		if( $stmt_pre_2 === false )  
		{  
			echo "Statement could not be prepared.\n";  
			die( print_r( sqlsrv_errors(), true));  
		}  
		
		if( sqlsrv_execute($stmt_pre_2) === false )  
		{  
			echo "Statement could not be executed.\n";  
			die( print_r( sqlsrv_errors(), true));  
		}
		sqlsrv_free_stmt($stmt_pre_2);


		//finalized product
		$tsql= "UPDATE [dbo].[mhw_app_prod] SET [finalized] = 1 WHERE [product_id] = ?";

		//echo $tsql." ".$prodID; exit;

		$stmt = sqlsrv_prepare($conn, $tsql, array($product_id));
		if( $stmt === false )  
		{  
			echo "Statement could not be prepared.\n";  
			die( print_r( sqlsrv_errors(), true));  
		}  
		
		if( sqlsrv_execute($stmt) === false )  
		{  
			echo "Statement could not be executed.\n";  
			die( print_r( sqlsrv_errors(), true));  
		}
	}

	//release sql statement
	  sqlsrv_free_stmt($stmt);


  	
	sqlsrv_close($conn);  

	if($errorFree==1){
		//if($i>1){
		//	echo "<div class=\"row\"><div class=\"col-sm-8\"><div class=\"card\"><div class=\"card-body\"><h5 class=\"card-title\">Thank you</h5><p class=\"card-text\">Your Product &amp; Item Setup Forms have been submitted.  <br />You may edit the Product &amp; add Items below.  <br /><br /><img src=\"css/img/Logo_Chevron_Animated.gif\" class=\"inline_animated_chevron\" /> You must <a href=\"viewproducts.php?v=nf\" class=\"btn btn-outline-success\">Finalize</a> your Products &amp; Items to submit to MHW for processing. <br /><br /><a href=\"viewproducts.php\" class=\"btn btn-outline-primary\">View Products</a></p></div></div></div>";
		//}
		//else{
		//echo "<div class=\"jumbotron jumbotron-fluid\">";
		  //echo "<div class=\"container\">";
			//echo "<div class=\"row\"><div class=\"col-sm-12\">";
			echo "<div class=\"card\"><div class=\"card-body\">";
			echo "<h5 class=\"card-title\">Thank you</h5>";
			
			if($pageerr!=""){
				echo "<div class=\"alert alert-danger\" role=\"alert\">".$pageerr."</div>";
			}

			  if (!$isAdmin) {
			  echo "<p class=\"card-text\">Your Product &amp; Item Setup Form has been submitted. <br />You may continue to edit the Product, upload images &amp; add Items below.  <br /><br/ ><img src=\"css/img/Logo_Chevron_Animated.gif\" class=\"inline_animated_chevron\" /> After you are finished with your session of adding products & items, <a href=\"viewproducts.php?v=nf\" class=\"btn btn-outline-success btn-sm\">Finalize</a> your Products &amp; Items to submit to MHW for processing. <br /><br />  <a href=\"viewproducts.php\" class=\"btn btn-outline-primary btn-sm\">View Products &amp; Items</a> <a href=\"imageupload.php?pid=".$prodID."\" class=\"btn btn-outline-primary btn-sm\">View &amp; Attach Product Images</a></p>";
			} else {
				echo "Finalized  <a href=\"viewproducts.php\" class=\"btn btn-outline-primary btn-sm\">View Products &amp; Items</a> <a href=\"imageupload.php?pid=".$prodID."\" class=\"btn btn-outline-primary btn-sm\">View &amp; Attach Product Images</a>";
			  }

			echo "</div></div>";
			//echo "</div></div>";
		  //echo "</div>";
		//echo "</div>";
		//}  
		$fp1 = fopen('../logs/prodlog.txt', 'a');
		$reqlog="\n\r\n\r".date("Ymd H:i:s")."\t".$reqlog."\n\r";
		fwrite($fp1, $reqlog);
		fclose($fp1);
	}
	else{
		$errorstamp = date("Ymd H:i:s");
		echo "<div class=\"row\"><div class=\"col-sm-8\"><div class=\"card\"><div class=\"card-body\"><h5 class=\"card-title\">Error</h5><p class=\"card-text\">There was an error saving your Product &amp; Item Setup Form. ".$pageerr." Additional error details have been logged. (ERROR-".$errorstamp.")</p></div></div></div>";
		$fp2 = fopen('../logs/prod3rr0rlog.txt', 'a');
		$errorlog="\n\r\n\r".$errorstamp."\t".$errorlog."\n\r";
		fwrite($fp2, $errorlog);
		fclose($fp2);
		
	}
}
?>
</div>
</main>
<!--Main layout-->