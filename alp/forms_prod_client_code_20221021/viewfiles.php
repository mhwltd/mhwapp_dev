<?php
//session_start();
include("sessionhandler.php");
include("settings.php");
if(!isset($_SESSION['mhwltdphp_user'])){

	header("Location: login.php");
}
else{	
?>
<!--<!doctype html>
<html lang="en">
  <head>-->
    <!-- Required meta tags -->
    <!--<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>MHW Ltd.</title>
  </head>
  <body>-->
<?php
	include("dbconnect.php");
	$conn = sqlsrv_connect($serverName, $connectionOptions);
	if( $conn === false) {
    		die( print_r( sqlsrv_errors(), true));
	}
	
	
		
	/* added client name & client code block : start  */
	
	$client_code_info=array();
	$client_info=array();
	$tsql_cli= "SELECT * FROM [vw_Clients] order by client_name";
	$stmt_cli = sqlsrv_query( $conn, $tsql_cli);
	if ( $stmt_cli === false ) die( print_r( sqlsrv_errors(), true));  
	
	while ( $row_cli = sqlsrv_fetch_array($stmt_cli, SQLSRV_FETCH_ASSOC) ) {               			
				$row_code_name=$row_cli['client_code'];
				$row_client_name=$row_cli['client_name'];
				$client_code_info[$row_code_name]=$row_cli['client_name']
				$client_info[$row_client_name]=$row_cli['client_code']
	}			
	
    /* added client name & client code block : end  */

	$pf_client_name = urldecode($_GET['client_name']);
	$isAdmin = in_array($_SESSION['mhwltdphp_usertype'], ["SUPERUSER", "ADMIN"]);

	if ( ($isAdmin) && $pf_client_name!=='' ) { 
		$clientlist_ori =  "'".str_replace(";","','",$pf_client_name)."'";
		$clientlist=$client_info[$clientlist_ori];
	}
	else{
		$clientlist =  "'".str_replace(";","','",$_SESSION['mhwltdphp_userclientcodes'])."'";
		$clientlist_ori=$client_code_info[$clientlist];
	}

	if(intval($_GET['pid'])>0){ $prodID=$_GET['pid']; }
	
	if(intval($_GET['iid'])>0){ $imgID=$_GET['iid']; }

	if($_POST['whereami']!=""){ $whereami=$_POST['whereami']; }
	
	echo "<div class=\"container-fluid\">";
	
	
	if(isset($prodID) && intval($prodID)>0){
		if($whereami!="imageupload"){
			echo "<button type=\"button\" name=\"upload\" data-prod=\"".$prodID."\" class=\"prod_upload btn btn-primary bg_arrow_blue\">";
				echo "<span class=\"oi oi-data-transfer-upload\" title=\"data-transfer-upload\" aria-hidden=\"true\"></span>New Upload";
			echo "</button><br />";
		}

		echo ("<table class=\"table table-bordered table-striped table-hover\"><thead><tr>
		  <th>Client Name</th>
		  <th>Brand Name</th>
          <th>Product Code (MHW)</th>
          <th>Product Description</th>
		  	
		  <th>Preview</th>	  
		  <th>Image</th>
		  <th>Type</th>
		  <th>Dimensions</th>
		  <th>Note<span style=\"display:none;\">".$clientlist_ori."</span></th>
		  
        </tr></thead>
      	<tbody>".PHP_EOL);
	  
	  /*$tsql_img = "SELECT i.[image_id]
	      , i.[product_id]
		  , p.[product_desc]
		  , p.[product_mhw_code]
		  , p.[client_name]
		  , p.[brand_name]
	      , i.[image_name]
	      , i.[image_dim]
		  , i.[image_type]
		  , i.[image_note]
	      , i.[create_via]
	      , i.[create_date]
	      , i.[edit_date]
         FROM [dbo].[mhw_app_prod_image] i
		 left outer join [dbo].[mhw_app_prod] p on i.[product_id] = p.[product_id]
		 WHERE i.[product_id] = ".$prodID." AND p.[client_name] IN (".$clientlist.") AND i.[active] = 1 AND i.[deleted] = 0 ORDER BY i.[create_date] desc";
		 */
		$tsql_img = "EXEC [dbo].[usp_viewfiles_revamp] @querytype = 'viewfiles', @client = ".$clientlist.", @prodID = ".$prodID.", @imgID = 0";
		
	}
	else if(isset($imgID)){
		/*
		$tsql_img = "SELECT i.[image_id]
	      , i.[product_id]
		  , p.[product_desc]
		  , p.[product_mhw_code]
		  , p.[client_name]
		  , p.[brand_name]
	      , i.[image_name]
	      , i.[image_dim]
		  , i.[image_type]
		  , i.[image_note]
	      , i.[create_via]
	      , i.[create_date]
	      , i.[edit_date]
         FROM [dbo].[mhw_app_prod_image] i
		 left outer join [dbo].[mhw_app_prod] p on i.[product_id] = p.[product_id]
		 WHERE i.[image_id] = ".$imgID." AND p.[client_name] IN (".$clientlist.") AND i.[active] = 1 AND i.[deleted] = 0";
		 */

		 $tsql_img = "EXEC [dbo].[usp_viewfiles_revamp] @querytype = 'viewfiles', @client = ".$clientlist.", @prodID = 0, @imgID = ".$imgID;
	}
	else{
		echo ("<table class=\"table table-bordered table-striped table-hover\"><thead><tr>
		  <th>Client Name</th>
		  <th>Brand Name</th>
          <th>Product Code (MHW)</th>
          <th>Product Description</th>
		  	
		  <th>Preview</th>		  
		  <th>Image</th>
		  <th>Type</th>
		  <th>Dimensions</th>
		  <th>Note<span style=\"display:none;\">".$clientlist_ori."</span></th>
		  
        </tr></thead>
      	<tbody>".PHP_EOL);
	  
	  /*
		 $tsql_img = "SELECT i.[image_id]
	      , i.[product_id]
		  , p.[product_desc]
		  , p.[product_mhw_code]
		  , p.[client_name]
		  , p.[brand_name]
	      , i.[image_name]
	      , i.[image_dim]
		  , i.[image_type]
		  , i.[image_note]
	      , i.[create_via]
	      , i.[create_date]
	      , i.[edit_date]
         FROM [dbo].[mhw_app_prod_image] i 
		 left outer join [dbo].[mhw_app_prod] p on i.[product_id] = p.[product_id]
		 WHERE p.[client_name] IN (".$clientlist.") AND i.[active] = 1 AND i.[deleted] = 0 ORDER BY i.[create_date] desc";
*/
		 $tsql_img = "EXEC [dbo].[usp_viewfiles_revamp] @querytype = 'viewfiles', @client = ".$clientlist.", @prodID = 0, @imgID = 0";
		
	}
	
	//echo $tsql_img;
	    $getResults_img= sqlsrv_query($conn, $tsql_img);

	    if ($getResults_img == FALSE)
	        echo (sqlsrv_errors());
			
			if(isset($imgID)){
				while ($row_img = sqlsrv_fetch_array($getResults_img, SQLSRV_FETCH_ASSOC)) {
					if(isset($_GET['b'])){ echo "<a href\"#\" class=\"btn btn-default prodimg_back\" data-product=\"".$_GET['b']."\" data-clientname=\"".$pf_client_name."\"><img src=\"svg/arrow-circle-left.svg\" alt=\"arrow-circle-left\" width=\"16\"> Go Back</a><br />"; }
					
					$ext = strrchr($row_img['image_name'],'.');
					$ext = strtolower($ext);
					if($ext==".pdf"){
						echo "<i class=\"far fa-file-pdf fa-9x\"></i>";
					}
					else{
						echo "<img src=\"".$siteroot."/".$upload_dir.$row_img['image_name']."\" data-imageid=\"".$row_img['image_id']."\" />";
					}

					echo "<br /><button type=\"button\" class=\"dwnld\" aria-label=\"Download\" data-imagename=\"".$row_img['image_name']."\" data-url=\"".$siteroot."/".$upload_dir.$row_img['image_name']."\"><img src=\"svg/data-transfer-download.svg\" alt=\"data-transfer-download\" width=\"16\"> Download File</button>";
				} //end loop
			}
			else{
		    		while ($row_img = sqlsrv_fetch_array($getResults_img, SQLSRV_FETCH_ASSOC)) {
					
				  echo ("<tr>");
				  echo ("<td><div data-product=\"".$row_img['product_id']."\" data-image=\"".$row_img['image_id']."\">".$row_img['client_name']."</div></td>
				 	<td><div data-product=\"".$row_img['product_id']."\" data-image=\"".$row_img['image_id']."\">".$row_img['brand_name']."</div></td>
		            <td><div data-product=\"".$row_img['product_id']."\" data-image=\"".$row_img['image_id']."\">".$row_img['product_mhw_code']."</div></td>
		            <td><div data-product=\"".$row_img['product_id']."\" data-image=\"".$row_img['image_id']."\">".$row_img['product_desc']."</div></td>");
					
					//image preview
					echo ("<td><div data-product=\"".$row_img['product_id']."\" data-image=\"".$row_img['image_id']."\">");
					$ext = strrchr($row_img['image_name'],'.');
					$ext = strtolower($ext);

					if($ext==".tif" || $ext==".tiff" || $ext==".pdf"){
						echo "<div class=\"imagebox\"><a href=\"#\" class=\"prodimg_thumb\" data-imageid=\"".$row_img['image_id']."\" data-clientname=\"".$pf_client_name."\">";
	          			echo "<img src=\"".$siteroot."/css/img/1x1.png\" width=\"100\" class=\"category-banner img-responsive\" data-imageid=\"".$row_img['image_id']."\" data-product=\"".$row_img['product_id']."\" data-clientname=\"".$pf_client_name."\" />";
	                		echo "<span class=\"imagebox-desc\">No Preview</span></a></div>";	
					}
					else{
						echo "<img src=\"".$siteroot."/".$thumb_dir.$row_img['image_name']."\" class=\"prodimg_thumb\" data-imageid=\"".$row_img['image_id']."\" data-product=\"".$row_img['product_id']."\" data-clientname=\"".$pf_client_name."\" width=\"100\" />";
					}
					echo ("</div></td>");
					
		            echo ("<td><div data-product=\"".$row_img['product_id']."\" data-image=\"".$row_img['image_id']."\"><a href=\"".$siteroot."/".$upload_dir.$row_img['image_name']."\" id=\"".$row_img['image_id']."\" target=\"_blank\">".$row_img['image_name']." <img src=\"svg/external-link.svg\" alt=\"external-link\" width=\"16\"></a></div></td>
		            <td><div data-product=\"".$row_img['product_id']."\" data-image=\"".$row_img['image_id']."\">".$row_img['image_type']."</div></td>
					<td><div data-product=\"".$row_img['product_id']."\" data-image=\"".$row_img['image_id']."\">".$row_img['image_dim']."</div></td>
					<td><div data-product=\"".$row_img['product_id']."\" data-image=\"".$row_img['image_id']."\"><span class=\"hidden\">".$row_img['image_note']."</span></div></td>");
					
					echo ("</tr>". PHP_EOL); 
	  
					$i_img++;   
				} //end loop
				echo ("</tbody></table>");
			}
			echo  "</div>";
			sqlsrv_free_stmt($getResults_img);

sqlsrv_close( $conn); 
//echo "</body></html>";

}
?>

