<?php
//session_start();
include("sessionhandler.php");
include("prepend.php");
if(!isset($_SESSION['mhwltdphp_user'])){
	header("Location: login.php");
}
else{
	//echo "<hr><pre>"; print_r($_SESSION); echo "</pre></hr>";
	include('head.php');
?>
<div class="container">	

	<div class="row">
	
	  <div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Product &amp; Item Setup</h5>
	        <p class="card-text">product setup</p>
			<a href="productsetup.php" class="btn btn-primary bg_arrow_blue">Visit</a>
	      </div>
	    </div>
	  </div>
	  <div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">View Product &amp; Items</h5>
	        <p class="card-text">view products</p>
			<a href="viewproducts.php" class="btn btn-primary bg_arrow_blue">Visit</a>
	      </div>
	    </div>
	  </div>
	  <div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Attach Product Images</h5>
	        <p class="card-text">image upload</p>
			<a href="imageupload.php" class="btn btn-primary bg_arrow_blue">Visit</a>
	      </div>
	    </div>
	  </div>
	  <!-- !HARD-CODED!  ACOUNTID: TEST MHW -->
	  <!--<div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Product & Item Setup - Form Assembly SSI (TEST MHW)</h5>
	        <p class="card-text">productform</p>
			<a href="productform.php?acctid=0011N00001M8OzvQAF" class="btn btn-primary">Visit</a>
	      </div>
	    </div>
	  </div>
	  <div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Product & Item Setup Login - Form Assembly SSI</h5>
	        <p class="card-text">product_interstitial</p>
			<a href="product_interstitial.php?email=<?php echo urlencode($_SESSION['mhwltdphp_useremail']); ?>&lastname=<?php echo urlencode($_SESSION['mhwltdphp_userlastname']); ?>" class="btn btn-primary">Visit</a>
	      </div>
	    </div>
	  </div>-->
	  
	</div>
<?php if($_SESSION['mhwltdphp_usertype'] == "SUPERUSER" || $_SESSION['mhwltdphp_usertype'] == "ADMIN" ){ ?>
	<div class="row">
	  <div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Compliance Order Review</h5>
	        <p class="card-text">order review</p>
			<a href="orderreview.php" class="btn btn-primary bg_arrow_blue">Visit</a>
	      </div>
	    </div>
	  </div>
		<div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Compliance Product Review</h5>
	        <p class="card-text">product review</p>
			<a href="productreview.php" class="btn btn-primary bg_arrow_blue">Visit</a>
	      </div>
	    </div>
	  </div>
	  
  <div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Distributor Registration Cancel</h5>
	        <p class="card-text">product auto fails compliance</p>
			<a href="product_state_status.php" class="btn btn-primary bg_arrow_blue">Visit</a>
	      </div>
	    </div>
	  </div>
	</div>
	<div class="row">
	  <div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Product Compliance Status</h5>
	        <p class="card-text">product compliance status</p>
<a href="prod_comp_status.php" class="btn btn-primary bg_arrow_blue">Visit</a>
	      </div>
	    </div>
	  </div>
	  <!--EMPTY-->
	  <!--EMPTY-->
	</div>
	<?php 
	} 
	else{
		include("dbconnect.php");
		$beta_client_user=false;
		$clientsBETA = explode(";",$_SESSION['mhwltdphp_userclientcodes']);

		$conn = sqlsrv_connect($serverName, $connectionOptions);
		if( $conn === false) die( print_r( sqlsrv_errors(), true));

		foreach ($clientsBETA as &$clientBETAvalue) {
			
			//if($clientBETAvalue=='MHW Web Demo') { $beta_client_user=true; }

			$tsql= "SELECT * FROM [mhw_app_client_access] WHERE [user_application] = 'BLR_Status' and [active] = 1 and [client_code] = '".$clientBETAvalue."'";
			$stmt = sqlsrv_query( $conn, $tsql);
			if ( $stmt === false ) die( print_r( sqlsrv_errors(), true));
			while ( $row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC) ) { 
				$beta_client_user=true;
			}
		}

		sqlsrv_free_stmt($stmt);

		if($beta_client_user){
		?>
			<div class="row">
			  <div class="col-sm-4">
				<div class="card">
				  <div class="card-body">
					<h5 class="card-title">Product Compliance Status</h5>
					<p class="card-text">product compliance status</p>
			<a href="prod_comp_status.php" class="btn btn-primary bg_arrow_blue">Visit</a>
	      </div>
	    </div>
	  </div>
	  <!--EMPTY-->
	  <!--EMPTY-->
	</div>
<?php 
} 
}

if($_SESSION['mhwltdphp_usertype'] == "SUPERUSER" || $_SESSION['mhwltdphp_usertype'] == "ADMIN" ){ ?>
	<div class="row">

		<div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Price Posting</h5>
	        <p class="card-text">view pricing</p>
			<a href="viewpricing.php" class="btn btn-primary bg_arrow_blue">Visit</a>
	      </div>
	    </div>
	  </div>
	</div>
<?php } ?>

	<!--<div class="row">
	
	  <div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Price Posting - Native</h5>
	        <p class="card-text">priceposting</p>
			<a href="priceposting.php" class="btn btn-primary">Visit</a>
	      </div>
	    </div>
	  </div>-->
	  <!-- !HARD-CODED!  ACOUNTID: TEST MHW -->
	  <!--<div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Price Posting - Form Assembly SSI (TEST MHW)</h5>
	        <p class="card-text">pricingform</p>
			<a href="pricingform.php?acctid=0011N00001M8OzvQAF" class="btn btn-primary">Visit</a>
	      </div>
	    </div>
	  </div>
	  <div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Price Posting Login - Form Assembly SSI</h5>
	        <p class="card-text">pricing_interstitial</p>
			<a href="pricing_interstitial.php?email=<?php echo urlencode($_SESSION['mhwltdphp_useremail']); ?>&lastname=<?php echo urlencode($_SESSION['mhwltdphp_userlastname']); ?>" class="btn btn-primary">Visit</a>
	      </div>
	    </div>
	  </div>-->
	  
	<!--</div>-->

<!--
<div class="row">
  <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Product & Item Setup Login (cont) - Form Assembly SSI</h5>
        <p class="card-text">productlogin2</p>
		<a href="productlogin2.php?email=<?php echo urlencode($_SESSION['mhwltdphp_useremail']); ?>&lastname=<?php echo urlencode($_SESSION['mhwltdphp_userlastname']); ?>" class="btn btn-primary">Visit</a>
      </div>
    </div>
  </div>
</div>-->

<!--
<div class="row">
  <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Product & Item Setup Login - Form Assembly SSI</h5>
        <p class="card-text">product_interstitial</p>
		<a href="product_interstitial.php?email=<?php echo urlencode($_SESSION['mhwltdphp_useremail']); ?>&lastname=<?php echo urlencode($_SESSION['mhwltdphp_userlastname']); ?>" class="btn btn-primary">Visit</a>
      </div>
    </div>
  </div>

  <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Price Posting Login - Form Assembly SSI</h5>
        <p class="card-text">pricing_interstitial</p>
		<a href="pricing_interstitial.php?email=<?php echo urlencode($_SESSION['mhwltdphp_useremail']); ?>&lastname=<?php echo urlencode($_SESSION['mhwltdphp_userlastname']); ?>" class="btn btn-primary">Visit</a>
      </div>
    </div>
  </div>
  
</div>-->

	<!--<div class="row">-->
	
 	   <!--!HARD CODED!  ACCOUNT: SEEDLIP-->
	  <!--<div class="col-sm-6">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Web Access Form - Native</h5>
	        <p class="card-text">requestaccess</p>
			<a href="requestaccess.php?acctname=Seedlip" class="btn btn-primary">Visit</a>
	      </div>
	    </div>
	  </div>-->
	  
	  <!--!HARD CODED!  ACCOUNT: SEEDLIP-->
	 <!-- <div class="col-sm-6">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Web Access Form - Form Assembly SSI</h5>
	        <p class="card-text">webaccessform</p>
			<a href="webaccessform.php?acctname=Seedlip" class="btn btn-primary">Visit</a>
	      </div>
	    </div>
	  </div>-->
	 
	<!--</div>-->

</div>
	
<?php	
}
?>
