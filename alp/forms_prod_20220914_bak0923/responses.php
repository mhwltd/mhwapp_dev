<?php

error_reporting(E_ALL); //displays an error

if(empty($_POST['itemID']))
{
	echo "Error: the Item ID is not set";
}
$itemID	= intval($_POST['itemID']);

//echo '<pre>'; print_r($_POST); echo '</pre>';

include("dbconnect.php");

//Establishes the connection
$conn = sqlsrv_connect($serverName, $connectionOptions);
if( $conn === false) {
		die( print_r( sqlsrv_errors(), true));
}

$tsql = "
	SET NOCOUNT ON;
	INSERT INTO tfa_common (
		itemID,
		userID,
		tfa_client_name,
		tfa_item_code,
		tfa_stock_uom,
		tfa_bottles_per_case,
		tfa_nat_price,
		
		tfa_ca_state,
		tfa_ct_state,
		tfa_ma_state,
		tfa_mn_state,
		tfa_nj_state,
		tfa_nyw_state,
		tfa_nyr_state,
		tfa_other_state
	)
	VALUES (
		?,?,?,?,?,?,?,  ?,?,?,?,?,?,?,?
	);
	SELECT SCOPE_IDENTITY() as LAST_ID;
";

$stmt = sqlsrv_prepare( $conn, $tsql, array ( 
	$itemID,
	$_POST['userID'],
	$_POST['tfa_client_name'],
	$_POST['tfa_item_code'],
	$_POST['tfa_stock_uom'],
	intval($_POST['tfa_bottles_per_case']),
	floatval($_POST['tfa_nat_price']),
	
	$_POST['tfa_ca_state']=='Yes'?1:0,
	$_POST['tfa_ct_state']=='Yes'?1:0,
	$_POST['tfa_ma_state']=='Yes'?1:0,
	$_POST['tfa_mn_state']=='Yes'?1:0,
	$_POST['tfa_nj_state']=='Yes'?1:0,
	$_POST['tfa_nyw_state']=='Yes'?1:0,
	$_POST['tfa_nyr_state']=='Yes'?1:0,
	$_POST['tfa_other_state']=='Yes'?1:0
));  

if( $stmt === false )  
{  
     echo "Statement could not be prepared.\n";  
     die( print_r( sqlsrv_errors(), true));  
}
	
if( sqlsrv_execute($stmt) === false )  
{  
  echo "Statement could not be executed.\n";  
  die( print_r( sqlsrv_errors(), true));  
}

/* Verify that the row was successfully inserted. */  
//echo "Rows affected: ".sqlsrv_rows_affected( $stmt )."\n";  

//$res = sqlsrv_fetch_array($stmt);
//echo '<pre>'; print_r($res); echo '</pre>';

sqlsrv_fetch($stmt);
$rowID = sqlsrv_get_field($stmt, 0);
//echo "ID : ".$insert_id;

	 
if($_POST['tfa_ca_state']=='Yes') {

	$tsql = "
		INSERT INTO tfa_ca (
			rowID,
			itemID,
			tfa_ca_change_type,
			tfa_ca_price_post_active,
			tfa_ca_price_bottle, 
			tfa_ca_price_case,
			tfa_ca_stand_cost,
			tfa_ca_purch_cost,
			tfa_ca_fob,
			tfa_ca_brand,
			tfa_ca_package,
			tfa_ca_contents,
			tfa_ca_cont_charge,
			tfa_ca_price_retail,
			tfa_ca_price_wholesale,
			tfa_ca_fob_del,
			tfa_ca_foe,
			tfa_ca_counties,
			tfa_ca_dist,
			tfa_ca_disc
		)
		VALUES (
			?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?
		);
	";

	$stmt = sqlsrv_prepare( $conn, $tsql, array ( 
		$rowID,
		$itemID,
		$_POST['tfa_ca_change_type'],
		$_POST['tfa_ca_price_post_active'],
		floatval($_POST['tfa_ca_price_bottle']),
		floatval($_POST['tfa_ca_price_case']),
		floatval($_POST['tfa_ca_stand_cost']),
		floatval($_POST['tfa_ca_purch_cost']),
		$_POST['tfa_ca_fob'],
		$_POST['tfa_ca_brand'],
		$_POST['tfa_ca_package'],
		$_POST['tfa_ca_contents'],
		$_POST['tfa_ca_cont_charge'],
		floatval($_POST['tfa_ca_price_retail']),
		floatval($_POST['tfa_ca_price_wholesale']),
		$_POST['tfa_ca_fob_del'],
		$_POST['tfa_ca_foe'],
		$_POST['tfa_ca_counties'],
		$_POST['tfa_ca_dist'],
		$_POST['tfa_ca_disc']
	));  

	if( $stmt === false )  
	{  
		 echo "Statement could not be prepared.\n";  
		 die( print_r( sqlsrv_errors(), true));  
	}  
		
	if( sqlsrv_execute($stmt) === false )  
	{  
	  echo "Statement could not be executed.\n";  
	  die( print_r( sqlsrv_errors(), true));  
	}  

}





if($_POST['tfa_ct_state']=='Yes') {

	$tsql = "
		INSERT INTO tfa_ct (
			rowID,
			itemID,
			tfa_ct_change_type,
			tfa_ct_price_posting,
			tfa_ct_price_bot, 
			tfa_ct_price_case,
			tfa_ct_stand_cost,
			tfa_ct_purch_cost,
			tfa_ct_fob
		)
		VALUES (
			?,?,?,?,?,?,?,?,?
		);
	";

	$stmt = sqlsrv_prepare( $conn, $tsql, array ( 
		$rowID,
		$itemID,
		$_POST['tfa_ct_change_type'],
		$_POST['tfa_ct_price_posting'],
		floatval($_POST['tfa_ct_price_bot']),
		floatval($_POST['tfa_ct_price_case']),
		floatval($_POST['tfa_ct_stand_cost']),
		floatval($_POST['tfa_ct_purch_cost']),
		$_POST['tfa_ct_fob']
	));  

	if( $stmt === false )  
	{  
		 echo "Statement could not be prepared.\n";  
		 die( print_r( sqlsrv_errors(), true));  
	}  
		
	if( sqlsrv_execute($stmt) === false )  
	{  
	  echo "Statement could not be executed.\n";  
	  die( print_r( sqlsrv_errors(), true));  
	}  

}





if($_POST['tfa_ma_state']=='Yes') {

	$tsql = "
		INSERT INTO tfa_ma (
			rowID,
			itemID,
			tfa_ma_change_type,
			tfa_ma_price_posting,
			tfa_ma_price_bot, 
			tfa_ma_price_case,
			tfa_ma_stand_cost,
			tfa_ma_purch_cost,
			tfa_ma_fob,
			tfa_ma_dist,
			tfa_ma_disc,
			
			tfa_ma_qty1_quan,
			tfa_ma_qty1_on,
			tfa_ma_qty1_type,
			tfa_ma_qty1_amt,

			tfa_ma_qty2_quan,
			tfa_ma_qty2_on,
			tfa_ma_qty2_type,
			tfa_ma_qty2_amt,

			tfa_ma_qty3_quan,
			tfa_ma_qty3_on,
			tfa_ma_qty3_type,
			tfa_ma_qty3_amt,
			
			tfa_ma_qty4_quan,
			tfa_ma_qty4_on,
			tfa_ma_qty4_type,
			tfa_ma_qty4_amt,

			tfa_ma_qty5_quan,
			tfa_ma_qty5_on,
			tfa_ma_qty5_type,
			tfa_ma_qty5_amt,

			tfa_ma_qty6_quan,
			tfa_ma_qty6_on,
			tfa_ma_qty6_type,
			tfa_ma_qty6_amt,

			tfa_ma_qty7_quan,
			tfa_ma_qty7_on,
			tfa_ma_qty7_type,
			tfa_ma_qty7_amt,

			tfa_ma_qty8_quan,
			tfa_ma_qty8_on,
			tfa_ma_qty8_type,
			tfa_ma_qty8_amt,

			tfa_ma_qty9_quan,
			tfa_ma_qty9_on,
			tfa_ma_qty9_type,
			tfa_ma_qty9_amt,

			tfa_ma_qty10_quan,
			tfa_ma_qty10_on,
			tfa_ma_qty10_type,
			tfa_ma_qty10_amt
		)
		VALUES (
			?,?,?,?,?, ?,?,?,?,?,?, 
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?
		);
	";

	$stmt = sqlsrv_prepare( $conn, $tsql, array ( 
		$rowID,
		$itemID,
		$_POST['tfa_ma_change_type'],
		$_POST['tfa_ma_price_posting'],
		floatval($_POST['tfa_ma_price_bot']),
		floatval($_POST['tfa_ma_price_case']),
		floatval($_POST['tfa_ma_stand_cost']),
		floatval($_POST['tfa_ma_purch_cost']),
		$_POST['tfa_ma_fob'],
		$_POST['tfa_ma_dist'],
		$_POST['tfa_ma_disc'],
		
		floatval($_POST['tfa_ma_qty1_quan']),
		$_POST['tfa_ma_qty1_on'],
		$_POST['tfa_ma_qty1_type'],
		floatval($_POST['tfa_ma_qty1_amt']),
		
		floatval($_POST['tfa_ma_qty2_quan']),
		$_POST['tfa_ma_qty2_on'],
		$_POST['tfa_ma_qty2_type'],
		floatval($_POST['tfa_ma_qty2_amt']),

		floatval($_POST['tfa_ma_qty3_quan']),
		$_POST['tfa_ma_qty3_on'],
		$_POST['tfa_ma_qty3_type'],
		floatval($_POST['tfa_ma_qty3_amt']),

		floatval($_POST['tfa_ma_qty4_quan']),
		$_POST['tfa_ma_qty4_on'],
		$_POST['tfa_ma_qty4_type'],
		floatval($_POST['tfa_ma_qty4_amt']),

		floatval($_POST['tfa_ma_qty5_quan']),
		$_POST['tfa_ma_qty5_on'],
		$_POST['tfa_ma_qty5_type'],
		floatval($_POST['tfa_ma_qty5_amt']),

		floatval($_POST['tfa_ma_qty6_quan']),
		$_POST['tfa_ma_qty6_on'],
		$_POST['tfa_ma_qty6_type'],
		floatval($_POST['tfa_ma_qty6_amt']),

		floatval($_POST['tfa_ma_qty7_quan']),
		$_POST['tfa_ma_qty7_on'],
		$_POST['tfa_ma_qty7_type'],
		floatval($_POST['tfa_ma_qty7_amt']),

		floatval($_POST['tfa_ma_qty8_quan']),
		$_POST['tfa_ma_qty8_on'],
		$_POST['tfa_ma_qty8_type'],
		floatval($_POST['tfa_ma_qty8_amt']),

		floatval($_POST['tfa_ma_qty9_quan']),
		$_POST['tfa_ma_qty9_on'],
		$_POST['tfa_ma_qty9_type'],
		floatval($_POST['tfa_ma_qty9_amt']),

		floatval($_POST['tfa_ma_qty10_quan']),
		$_POST['tfa_ma_qty10_on'],
		$_POST['tfa_ma_qty10_type'],
		floatval($_POST['tfa_ma_qty10_amt']),
		
	));  

	if( $stmt === false )  
	{  
		 echo "Statement could not be prepared.\n";  
		 die( print_r( sqlsrv_errors(), true));  
	}  
		
	if( sqlsrv_execute($stmt) === false )  
	{  
	  echo "Statement could not be executed.\n";  
	  die( print_r( sqlsrv_errors(), true));  
	}  

}






if($_POST['tfa_mn_state']=='Yes') {

	$tsql = "
		INSERT INTO tfa_mn (
			rowID,
			itemID,
			tfa_mn_change_type,
			tfa_mn_price_posting,
			tfa_mn_price_bot, 
			tfa_mn_price_case,
			tfa_mn_stand_cost,
			tfa_mn_purch_cost,
			tfa_mn_fob,
			tfa_mn_dist,
			tfa_mn_disc,
			
			tfa_mn_qty1_quan,
			tfa_mn_qty1_on,
			tfa_mn_qty1_type,
			tfa_mn_qty1_amt,

			tfa_mn_qty2_quan,
			tfa_mn_qty2_on,
			tfa_mn_qty2_type,
			tfa_mn_qty2_amt,

			tfa_mn_qty3_quan,
			tfa_mn_qty3_on,
			tfa_mn_qty3_type,
			tfa_mn_qty3_amt,
			
			tfa_mn_qty4_quan,
			tfa_mn_qty4_on,
			tfa_mn_qty4_type,
			tfa_mn_qty4_amt,

			tfa_mn_qty5_quan,
			tfa_mn_qty5_on,
			tfa_mn_qty5_type,
			tfa_mn_qty5_amt,

			tfa_mn_qty6_quan,
			tfa_mn_qty6_on,
			tfa_mn_qty6_type,
			tfa_mn_qty6_amt,

			tfa_mn_qty7_quan,
			tfa_mn_qty7_on,
			tfa_mn_qty7_type,
			tfa_mn_qty7_amt,

			tfa_mn_qty8_quan,
			tfa_mn_qty8_on,
			tfa_mn_qty8_type,
			tfa_mn_qty8_amt,

			tfa_mn_qty9_quan,
			tfa_mn_qty9_on,
			tfa_mn_qty9_type,
			tfa_mn_qty9_amt,

			tfa_mn_qty10_quan,
			tfa_mn_qty10_on,
			tfa_mn_qty10_type,
			tfa_mn_qty10_amt
		)
		VALUES (
			?,?,?,?,?, ?,?,?,?,?,?, 
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?
		);
	";

	$stmt = sqlsrv_prepare( $conn, $tsql, array ( 
		$rowID,
		$itemID,
		$_POST['tfa_mn_change_type'],
		$_POST['tfa_mn_price_posting'],
		floatval($_POST['tfa_mn_price_bot']),
		floatval($_POST['tfa_mn_price_case']),
		floatval($_POST['tfa_mn_stand_cost']),
		floatval($_POST['tfa_mn_purch_cost']),
		$_POST['tfa_mn_fob'],
		$_POST['tfa_mn_dist'],
		$_POST['tfa_mn_disc'],
		
		floatval($_POST['tfa_mn_qty1_quan']),
		$_POST['tfa_mn_qty1_on'],
		$_POST['tfa_mn_qty1_type'],
		floatval($_POST['tfa_mn_qty1_amt']),
		
		floatval($_POST['tfa_mn_qty2_quan']),
		$_POST['tfa_mn_qty2_on'],
		$_POST['tfa_mn_qty2_type'],
		floatval($_POST['tfa_mn_qty2_amt']),

		floatval($_POST['tfa_mn_qty3_quan']),
		$_POST['tfa_mn_qty3_on'],
		$_POST['tfa_mn_qty3_type'],
		floatval($_POST['tfa_mn_qty3_amt']),

		floatval($_POST['tfa_mn_qty4_quan']),
		$_POST['tfa_mn_qty4_on'],
		$_POST['tfa_mn_qty4_type'],
		floatval($_POST['tfa_mn_qty4_amt']),

		floatval($_POST['tfa_mn_qty5_quan']),
		$_POST['tfa_mn_qty5_on'],
		$_POST['tfa_mn_qty5_type'],
		floatval($_POST['tfa_mn_qty5_amt']),

		floatval($_POST['tfa_mn_qty6_quan']),
		$_POST['tfa_mn_qty6_on'],
		$_POST['tfa_mn_qty6_type'],
		floatval($_POST['tfa_mn_qty6_amt']),

		floatval($_POST['tfa_mn_qty7_quan']),
		$_POST['tfa_mn_qty7_on'],
		$_POST['tfa_mn_qty7_type'],
		floatval($_POST['tfa_mn_qty7_amt']),

		floatval($_POST['tfa_mn_qty8_quan']),
		$_POST['tfa_mn_qty8_on'],
		$_POST['tfa_mn_qty8_type'],
		floatval($_POST['tfa_mn_qty8_amt']),

		floatval($_POST['tfa_mn_qty9_quan']),
		$_POST['tfa_mn_qty9_on'],
		$_POST['tfa_mn_qty9_type'],
		floatval($_POST['tfa_mn_qty9_amt']),

		floatval($_POST['tfa_mn_qty10_quan']),
		$_POST['tfa_mn_qty10_on'],
		$_POST['tfa_mn_qty10_type'],
		floatval($_POST['tfa_mn_qty10_amt']),
		
	));  

	if( $stmt === false )  
	{  
		 echo "Statement could not be prepared.\n";  
		 die( print_r( sqlsrv_errors(), true));  
	}  
		
	if( sqlsrv_execute($stmt) === false )  
	{  
	  echo "Statement could not be executed.\n";  
	  die( print_r( sqlsrv_errors(), true));  
	}  

}






if($_POST['tfa_nj_state']=='Yes') {

	$tsql = "
		INSERT INTO tfa_nj (
			rowID,
			itemID,
			tfa_nj_change_type,
			tfa_nj_price_posting,
			tfa_nj_price_bot, 
			tfa_nj_price_case,
			tfa_nj_stand_cost,
			tfa_nj_purch_cost,
			tfa_nj_fob,
			tfa_nj_dist,
			tfa_nj_disc,
			
			tfa_nj_qty1_quan,
			tfa_nj_qty1_on,
			tfa_nj_qty1_type,
			tfa_nj_qty1_amt,

			tfa_nj_qty2_quan,
			tfa_nj_qty2_on,
			tfa_nj_qty2_type,
			tfa_nj_qty2_amt,

			tfa_nj_qty3_quan,
			tfa_nj_qty3_on,
			tfa_nj_qty3_type,
			tfa_nj_qty3_amt,
			
			tfa_nj_qty4_quan,
			tfa_nj_qty4_on,
			tfa_nj_qty4_type,
			tfa_nj_qty4_amt,

			tfa_nj_qty5_quan,
			tfa_nj_qty5_on,
			tfa_nj_qty5_type,
			tfa_nj_qty5_amt,

			tfa_nj_qty6_quan,
			tfa_nj_qty6_on,
			tfa_nj_qty6_type,
			tfa_nj_qty6_amt,

			tfa_nj_qty7_quan,
			tfa_nj_qty7_on,
			tfa_nj_qty7_type,
			tfa_nj_qty7_amt,

			tfa_nj_qty8_quan,
			tfa_nj_qty8_on,
			tfa_nj_qty8_type,
			tfa_nj_qty8_amt,

			tfa_nj_qty9_quan,
			tfa_nj_qty9_on,
			tfa_nj_qty9_type,
			tfa_nj_qty9_amt,

			tfa_nj_qty10_quan,
			tfa_nj_qty10_on,
			tfa_nj_qty10_type,
			tfa_nj_qty10_amt
		)
		VALUES (
			?,?,?,?,?, ?,?,?,?,?,?, 
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?
		);
	";

	$stmt = sqlsrv_prepare( $conn, $tsql, array ( 
		$rowID,
		$itemID,
		$_POST['tfa_nj_change_type'],
		$_POST['tfa_nj_price_posting'],
		floatval($_POST['tfa_nj_price_bot']),
		floatval($_POST['tfa_nj_price_case']),
		floatval($_POST['tfa_nj_stand_cost']),
		floatval($_POST['tfa_nj_purch_cost']),
		$_POST['tfa_nj_fob'],
		$_POST['tfa_nj_dist'],
		$_POST['tfa_nj_disc'],
		
		floatval($_POST['tfa_nj_qty1_quan']),
		$_POST['tfa_nj_qty1_on'],
		$_POST['tfa_nj_qty1_type'],
		floatval($_POST['tfa_nj_qty1_amt']),
		
		floatval($_POST['tfa_nj_qty2_quan']),
		$_POST['tfa_nj_qty2_on'],
		$_POST['tfa_nj_qty2_type'],
		floatval($_POST['tfa_nj_qty2_amt']),

		floatval($_POST['tfa_nj_qty3_quan']),
		$_POST['tfa_nj_qty3_on'],
		$_POST['tfa_nj_qty3_type'],
		floatval($_POST['tfa_nj_qty3_amt']),

		floatval($_POST['tfa_nj_qty4_quan']),
		$_POST['tfa_nj_qty4_on'],
		$_POST['tfa_nj_qty4_type'],
		floatval($_POST['tfa_nj_qty4_amt']),

		floatval($_POST['tfa_nj_qty5_quan']),
		$_POST['tfa_nj_qty5_on'],
		$_POST['tfa_nj_qty5_type'],
		floatval($_POST['tfa_nj_qty5_amt']),

		floatval($_POST['tfa_nj_qty6_quan']),
		$_POST['tfa_nj_qty6_on'],
		$_POST['tfa_nj_qty6_type'],
		floatval($_POST['tfa_nj_qty6_amt']),

		floatval($_POST['tfa_nj_qty7_quan']),
		$_POST['tfa_nj_qty7_on'],
		$_POST['tfa_nj_qty7_type'],
		floatval($_POST['tfa_nj_qty7_amt']),

		floatval($_POST['tfa_nj_qty8_quan']),
		$_POST['tfa_nj_qty8_on'],
		$_POST['tfa_nj_qty8_type'],
		floatval($_POST['tfa_nj_qty8_amt']),

		floatval($_POST['tfa_nj_qty9_quan']),
		$_POST['tfa_nj_qty9_on'],
		$_POST['tfa_nj_qty9_type'],
		floatval($_POST['tfa_nj_qty9_amt']),

		floatval($_POST['tfa_nj_qty10_quan']),
		$_POST['tfa_nj_qty10_on'],
		$_POST['tfa_nj_qty10_type'],
		floatval($_POST['tfa_nj_qty10_amt']),
		
	));  

	if( $stmt === false )  
	{  
		 echo "Statement could not be prepared.\n";  
		 die( print_r( sqlsrv_errors(), true));  
	}  
		
	if( sqlsrv_execute($stmt) === false )  
	{  
	  echo "Statement could not be executed.\n";  
	  die( print_r( sqlsrv_errors(), true));  
	}  

}





if($_POST['tfa_nyr_state']=='Yes') {

	$tsql = "
		INSERT INTO tfa_nyr (
			rowID,
			itemID,
			tfa_nyr_change_type,
			tfa_nyr_price_posting,
			tfa_nyr_price_bot, 
			tfa_nyr_price_case,
			tfa_nyr_stand_cost,
			tfa_nyr_purch_cost,
			tfa_nyr_fob,
			tfa_nyr_dist,
			tfa_nyr_disc,
			
			tfa_nyr_qty1_quan,
			tfa_nyr_qty1_on,
			tfa_nyr_qty1_type,
			tfa_nyr_qty1_amt,

			tfa_nyr_qty2_quan,
			tfa_nyr_qty2_on,
			tfa_nyr_qty2_type,
			tfa_nyr_qty2_amt,

			tfa_nyr_qty3_quan,
			tfa_nyr_qty3_on,
			tfa_nyr_qty3_type,
			tfa_nyr_qty3_amt,
			
			tfa_nyr_qty4_quan,
			tfa_nyr_qty4_on,
			tfa_nyr_qty4_type,
			tfa_nyr_qty4_amt,

			tfa_nyr_qty5_quan,
			tfa_nyr_qty5_on,
			tfa_nyr_qty5_type,
			tfa_nyr_qty5_amt,

			tfa_nyr_qty6_quan,
			tfa_nyr_qty6_on,
			tfa_nyr_qty6_type,
			tfa_nyr_qty6_amt,

			tfa_nyr_qty7_quan,
			tfa_nyr_qty7_on,
			tfa_nyr_qty7_type,
			tfa_nyr_qty7_amt,

			tfa_nyr_qty8_quan,
			tfa_nyr_qty8_on,
			tfa_nyr_qty8_type,
			tfa_nyr_qty8_amt,

			tfa_nyr_qty9_quan,
			tfa_nyr_qty9_on,
			tfa_nyr_qty9_type,
			tfa_nyr_qty9_amt,

			tfa_nyr_qty10_quan,
			tfa_nyr_qty10_on,
			tfa_nyr_qty10_type,
			tfa_nyr_qty10_amt
		)
		VALUES (
			?,?,?,?,?, ?,?,?,?,?,?, 
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?
		);
	";

	$stmt = sqlsrv_prepare( $conn, $tsql, array ( 
		$rowID,
		$itemID,
		$_POST['tfa_nyr_change_type'],
		$_POST['tfa_nyr_price_posting'],
		floatval($_POST['tfa_nyr_price_bot']),
		floatval($_POST['tfa_nyr_price_case']),
		floatval($_POST['tfa_nyr_stand_cost']),
		floatval($_POST['tfa_nyr_purch_cost']),
		$_POST['tfa_nyr_fob'],
		$_POST['tfa_nyr_dist'],
		$_POST['tfa_nyr_disc'],
		
		floatval($_POST['tfa_nyr_qty1_quan']),
		$_POST['tfa_nyr_qty1_on'],
		$_POST['tfa_nyr_qty1_type'],
		floatval($_POST['tfa_nyr_qty1_amt']),
		
		floatval($_POST['tfa_nyr_qty2_quan']),
		$_POST['tfa_nyr_qty2_on'],
		$_POST['tfa_nyr_qty2_type'],
		floatval($_POST['tfa_nyr_qty2_amt']),

		floatval($_POST['tfa_nyr_qty3_quan']),
		$_POST['tfa_nyr_qty3_on'],
		$_POST['tfa_nyr_qty3_type'],
		floatval($_POST['tfa_nyr_qty3_amt']),

		floatval($_POST['tfa_nyr_qty4_quan']),
		$_POST['tfa_nyr_qty4_on'],
		$_POST['tfa_nyr_qty4_type'],
		floatval($_POST['tfa_nyr_qty4_amt']),

		floatval($_POST['tfa_nyr_qty5_quan']),
		$_POST['tfa_nyr_qty5_on'],
		$_POST['tfa_nyr_qty5_type'],
		floatval($_POST['tfa_nyr_qty5_amt']),

		floatval($_POST['tfa_nyr_qty6_quan']),
		$_POST['tfa_nyr_qty6_on'],
		$_POST['tfa_nyr_qty6_type'],
		floatval($_POST['tfa_nyr_qty6_amt']),

		floatval($_POST['tfa_nyr_qty7_quan']),
		$_POST['tfa_nyr_qty7_on'],
		$_POST['tfa_nyr_qty7_type'],
		floatval($_POST['tfa_nyr_qty7_amt']),

		floatval($_POST['tfa_nyr_qty8_quan']),
		$_POST['tfa_nyr_qty8_on'],
		$_POST['tfa_nyr_qty8_type'],
		floatval($_POST['tfa_nyr_qty8_amt']),

		floatval($_POST['tfa_nyr_qty9_quan']),
		$_POST['tfa_nyr_qty9_on'],
		$_POST['tfa_nyr_qty9_type'],
		floatval($_POST['tfa_nyr_qty9_amt']),

		floatval($_POST['tfa_nyr_qty10_quan']),
		$_POST['tfa_nyr_qty10_on'],
		$_POST['tfa_nyr_qty10_type'],
		floatval($_POST['tfa_nyr_qty10_amt']),
		
	));  

	if( $stmt === false )  
	{  
		 echo "Statement could not be prepared.\n";  
		 die( print_r( sqlsrv_errors(), true));  
	}  
		
	if( sqlsrv_execute($stmt) === false )  
	{  
	  echo "Statement could not be executed.\n";  
	  die( print_r( sqlsrv_errors(), true));  
	}  

}







if($_POST['tfa_nyw_state']=='Yes') {

	$tsql = "
		INSERT INTO tfa_nyw (
			rowID,
			itemID,
			tfa_nyw_change_type,
			tfa_nyw_price_posting,
			tfa_nyw_price_bot, 
			tfa_nyw_price_case,
			tfa_nyw_stand_cost,
			tfa_nyw_purch_cost,
			tfa_nyw_fob,
			tfa_nyw_dist,
			tfa_nyw_disc,
			
			tfa_nyw_qty1_quan,
			tfa_nyw_qty1_on,
			tfa_nyw_qty1_type,
			tfa_nyw_qty1_amt,

			tfa_nyw_qty2_quan,
			tfa_nyw_qty2_on,
			tfa_nyw_qty2_type,
			tfa_nyw_qty2_amt,

			tfa_nyw_qty3_quan,
			tfa_nyw_qty3_on,
			tfa_nyw_qty3_type,
			tfa_nyw_qty3_amt,
			
			tfa_nyw_qty4_quan,
			tfa_nyw_qty4_on,
			tfa_nyw_qty4_type,
			tfa_nyw_qty4_amt,

			tfa_nyw_qty5_quan,
			tfa_nyw_qty5_on,
			tfa_nyw_qty5_type,
			tfa_nyw_qty5_amt,

			tfa_nyw_qty6_quan,
			tfa_nyw_qty6_on,
			tfa_nyw_qty6_type,
			tfa_nyw_qty6_amt,

			tfa_nyw_qty7_quan,
			tfa_nyw_qty7_on,
			tfa_nyw_qty7_type,
			tfa_nyw_qty7_amt,

			tfa_nyw_qty8_quan,
			tfa_nyw_qty8_on,
			tfa_nyw_qty8_type,
			tfa_nyw_qty8_amt,

			tfa_nyw_qty9_quan,
			tfa_nyw_qty9_on,
			tfa_nyw_qty9_type,
			tfa_nyw_qty9_amt,

			tfa_nyw_qty10_quan,
			tfa_nyw_qty10_on,
			tfa_nyw_qty10_type,
			tfa_nyw_qty10_amt
		)
		VALUES (
			?,?,?,?, ?,?,?,?, ?,?,?, 
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?
		);
	";

	$stmt = sqlsrv_prepare( $conn, $tsql, array ( 
		$rowID, $itemID,
		$_POST['tfa_nyw_change_type'],
		$_POST['tfa_nyw_price_posting'],
		floatval($_POST['tfa_nyw_price_bot']),
		floatval($_POST['tfa_nyw_price_case']),
		floatval($_POST['tfa_nyw_stand_cost']),
		floatval($_POST['tfa_nyw_purch_cost']),
		$_POST['tfa_nyw_fob'],
		$_POST['tfa_nyw_dist'],
		$_POST['tfa_nyw_disc'],
		
		floatval($_POST['tfa_nyw_qty1_quan']),
		$_POST['tfa_nyw_qty1_on'],
		$_POST['tfa_nyw_qty1_type'],
		floatval($_POST['tfa_nyw_qty1_amt']),
		
		floatval($_POST['tfa_nyw_qty2_quan']),
		$_POST['tfa_nyw_qty2_on'],
		$_POST['tfa_nyw_qty2_type'],
		floatval($_POST['tfa_nyw_qty2_amt']),

		floatval($_POST['tfa_nyw_qty3_quan']),
		$_POST['tfa_nyw_qty3_on'],
		$_POST['tfa_nyw_qty3_type'],
		floatval($_POST['tfa_nyw_qty3_amt']),

		floatval($_POST['tfa_nyw_qty4_quan']),
		$_POST['tfa_nyw_qty4_on'],
		$_POST['tfa_nyw_qty4_type'],
		floatval($_POST['tfa_nyw_qty4_amt']),

		floatval($_POST['tfa_nyw_qty5_quan']),
		$_POST['tfa_nyw_qty5_on'],
		$_POST['tfa_nyw_qty5_type'],
		floatval($_POST['tfa_nyw_qty5_amt']),

		floatval($_POST['tfa_nyw_qty6_quan']),
		$_POST['tfa_nyw_qty6_on'],
		$_POST['tfa_nyw_qty6_type'],
		floatval($_POST['tfa_nyw_qty6_amt']),

		floatval($_POST['tfa_nyw_qty7_quan']),
		$_POST['tfa_nyw_qty7_on'],
		$_POST['tfa_nyw_qty7_type'],
		floatval($_POST['tfa_nyw_qty7_amt']),

		floatval($_POST['tfa_nyw_qty8_quan']),
		$_POST['tfa_nyw_qty8_on'],
		$_POST['tfa_nyw_qty8_type'],
		floatval($_POST['tfa_nyw_qty8_amt']),

		floatval($_POST['tfa_nyw_qty9_quan']),
		$_POST['tfa_nyw_qty9_on'],
		$_POST['tfa_nyw_qty9_type'],
		floatval($_POST['tfa_nyw_qty9_amt']),

		floatval($_POST['tfa_nyw_qty10_quan']),
		$_POST['tfa_nyw_qty10_on'],
		$_POST['tfa_nyw_qty10_type'],
		floatval($_POST['tfa_nyw_qty10_amt']),
		
	));  

	if( $stmt === false )  
	{  
		 echo "Statement could not be prepared.\n";  
		 die( print_r( sqlsrv_errors(), true));  
	}  
		
	if( sqlsrv_execute($stmt) === false )  
	{  
	  echo "Statement could not be executed.\n";  
	  die( print_r( sqlsrv_errors(), true));  
	}  

}






//if($_POST['tfa_os_state']=='Yes') {

	$tsql = "
		INSERT INTO tfa_os (
			rowID,
			itemID,
			tfa_os_price1_bottle,
			tfa_os_price1_states,
			tfa_os_price2_bottle,
			tfa_os_price2_states,
			tfa_os_price3_bottle,
			tfa_os_price3_states,
			tfa_os_price4_bottle,
			tfa_os_price4_states,
			tfa_os_price5_bottle,
			tfa_os_price5_states
		)
		VALUES (
			?,?,?,?,?,?, ?,?,?,?,?,?
		);
	";

	$stmt = sqlsrv_prepare( $conn, $tsql, array ( 
		$rowID, $itemID,
		floatval($_POST['tfa_os_price1_bottle']),
		implode(',', $_POST['tfa_os_price1_states']),
		floatval($_POST['tfa_os_price2_bottle']),
		implode(',', $_POST['tfa_os_price2_states']),
		floatval($_POST['tfa_os_price3_bottle']),
		implode(',', $_POST['tfa_os_price3_states']),
		floatval($_POST['tfa_os_price4_bottle']),
		implode(',', $_POST['tfa_os_price4_states']),
		floatval($_POST['tfa_os_price5_bottle']),
		implode(',', $_POST['tfa_os_price5_states'])
	));  

	if( $stmt === false )  
	{  
		 echo "Statement could not be prepared.\n";  
		 die( print_r( sqlsrv_errors(), true));  
	}  
		
	if( sqlsrv_execute($stmt) === false )  
	{  
	  echo "Statement could not be executed.\n";  
	  die( print_r( sqlsrv_errors(), true));  
	}  

//}






/* Free statement and connection resources. */  
sqlsrv_free_stmt( $stmt);  
sqlsrv_close( $conn); 

header("Location: result.php");

?>