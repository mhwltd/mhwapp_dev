<?php

error_reporting(E_ALL); //displays an error

session_start();
if(!isset($_SESSION['mhwltdphp_user'])){
	die( "Not authenticated !" );  
	header("Location: login.php");
}

//print_r($_POST);

if(empty($_REQUEST['row_id']))
{
	die("Error: the Row ID is not set");
}
$row_id	= intval($_REQUEST['row_id']);

if(empty($_REQUEST['view_state']))
{
	die("Error: the State is not set");
}
$view_state	= ($_REQUEST['view_state']);

include("dbconnect.php");

//Establishes the connection
$conn = sqlsrv_connect($serverName, $connectionOptions);
if( $conn === false) {
		die( print_r( sqlsrv_errors(), true));
}

	 
$tsql = "
	UPDATE $view_state SET
		".$view_state."_qty1_quan = ?,
		".$view_state."_qty1_on = ?,
		".$view_state."_qty1_type = ?,
		".$view_state."_qty1_amt = ?,

		".$view_state."_qty2_quan = ?,
		".$view_state."_qty2_on = ?,
		".$view_state."_qty2_type = ?,
		".$view_state."_qty2_amt = ?,

		".$view_state."_qty3_quan = ?,
		".$view_state."_qty3_on = ?,
		".$view_state."_qty3_type = ?,
		".$view_state."_qty3_amt = ?,

		".$view_state."_qty4_quan = ?,
		".$view_state."_qty4_on = ?,
		".$view_state."_qty4_type = ?,
		".$view_state."_qty4_amt = ?,

		".$view_state."_qty5_quan = ?,
		".$view_state."_qty5_on = ?,
		".$view_state."_qty5_type = ?,
		".$view_state."_qty5_amt = ?,

		".$view_state."_qty6_quan = ?,
		".$view_state."_qty6_on = ?,
		".$view_state."_qty6_type = ?,
		".$view_state."_qty6_amt = ?,

		".$view_state."_qty7_quan = ?,
		".$view_state."_qty7_on = ?,
		".$view_state."_qty7_type = ?,
		".$view_state."_qty7_amt = ?,

		".$view_state."_qty8_quan = ?,
		".$view_state."_qty8_on = ?,
		".$view_state."_qty8_type = ?,
		".$view_state."_qty8_amt = ?,

		".$view_state."_qty9_quan = ?,
		".$view_state."_qty9_on = ?,
		".$view_state."_qty9_type = ?,
		".$view_state."_qty9_amt = ?,

		".$view_state."_qty10_quan = ?,
		".$view_state."_qty10_on = ?,
		".$view_state."_qty10_type = ?,
		".$view_state."_qty10_amt = ?
	WHERE ID = ?
	";


$params = array ( 
		floatval($_REQUEST['qty1_quan']),
		$_REQUEST['qty1_on'],
		$_REQUEST['qty1_type'],
		floatval($_REQUEST['qty1_amt']),

		floatval($_REQUEST['qty2_quan']),
		$_REQUEST['qty2_on'],
		$_REQUEST['qty1_type'],
		floatval($_REQUEST['qty2_amt']),

		floatval($_REQUEST['qty3_quan']),
		$_REQUEST['qty3_on'],
		$_REQUEST['qty1_type'],
		floatval($_REQUEST['qty3_amt']),

		floatval($_REQUEST['qty4_quan']),
		$_REQUEST['qty4_on'],
		$_REQUEST['qty1_type'],
		floatval($_REQUEST['qty4_amt']),

		floatval($_REQUEST['qty5_quan']),
		$_REQUEST['qty5_on'],
		$_REQUEST['qty1_type'],
		floatval($_REQUEST['qty5_amt']),

		floatval($_REQUEST['qty6_quan']),
		$_REQUEST['qty6_on'],
		$_REQUEST['qty1_type'],
		floatval($_REQUEST['qty6_amt']),

		floatval($_REQUEST['qty7_quan']),
		$_REQUEST['qty7_on'],
		$_REQUEST['qty1_type'],
		floatval($_REQUEST['qty7_amt']),

		floatval($_REQUEST['qty8_quan']),
		$_REQUEST['qty8_on'],
		$_REQUEST['qty1_type'],
		floatval($_REQUEST['qty8_amt']),

		floatval($_REQUEST['qty9_quan']),
		$_REQUEST['qty9_on'],
		$_REQUEST['qty1_type'],
		floatval($_REQUEST['qty9_amt']),

		floatval($_REQUEST['qty10_quan']),
		$_REQUEST['qty10_on'],
		$_REQUEST['qty1_type'],
		floatval($_REQUEST['qty10_amt']),

		$row_id
	);

	$stmt = sqlsrv_prepare( $conn, $tsql, $params);  

if( $stmt !== false )  
{  
	//echo "Statement could not be prepared.\n";  
	//die( print_r( sqlsrv_errors(), true));  
	if( sqlsrv_execute($stmt) === false )  
	{  
		  echo "Statement could not be executed.\n";  
		  echo '<pre>';
		  echo print_r($params);
		  die( print_r( sqlsrv_errors(), true));  
	}  
}  


$json = array(
//	'query' => $tsql,
//	'params' => $params,
	'error' => print_r( sqlsrv_errors(), true),
    'result' => sqlsrv_rows_affected ( $stmt )
);

header('Content-Type: application/json');
echo json_encode($json);


/* Free statement and connection resources. */  
sqlsrv_free_stmt( $stmt);  
sqlsrv_close( $conn); 

//header("Location: result.php");

?>