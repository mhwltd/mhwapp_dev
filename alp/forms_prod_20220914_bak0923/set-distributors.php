<?php

session_start();
if(!isset($_SESSION['mhwltdphp_user'])){
	die( "Not authenticated !" );  
}

error_reporting(E_ALL); //displays an error
include("dbconnect.php");

//Establishes the connection
$conn = sqlsrv_connect($serverName, $connectionOptions);
if( $conn === false) {
	die( print_r( sqlsrv_errors(), true));
}
//echo '<pre>';print_r($_REQUEST);exit;
$tableName = $_REQUEST['view_state'];

if (!in_array($tableName, ['tfa_ma','tfa_ca','tfa_nyw'])) {
    die('Error!');
}

$ids = array_keys($_REQUEST['selected']);
$distributors = str_replace("'","''",$_REQUEST['distributors']);

$tsql = "UPDATE $tableName SET ".$tableName."_dist='$distributors' WHERE ID IN (".implode(",",$ids).");";

//echo $tsql;
$stmt = sqlsrv_prepare($conn, $tsql);  

if( $stmt === false )  
{  
    echo "Statement could not be prepared.\n";  
    die( print_r( sqlsrv_errors(), true));  
}  

if( sqlsrv_execute($stmt) === false )  
{  
    echo "Statement could not be executed.\n";  
    die( print_r( sqlsrv_errors(), true));  
}  

$json = array(
    'result' => sqlsrv_rows_affected ( $stmt )
);
header('Content-Type: application/json');
echo json_encode($json);

/* Free statement and connection resources. */  
sqlsrv_free_stmt( $stmt);  
sqlsrv_close( $conn); 

?>