<?php
    //ini_set('display_errors', 1);
    //ini_set('display_startup_errors', 1);
    //error_reporting(E_ALL);
	session_start();
    include("prepend.php");
    include("settings.php");

    if(!isset($_SESSION['mhwltdphp_user'])){
        header("Location: login.php");
    }

	if($_SESSION['mhwltdphp_usertype'] != "SUPERUSER" && $_SESSION['mhwltdphp_usertype'] != "ADMIN" ){ 
		echo "Access Denied";
		header("Location: home.php");
	} 

    include("dbconnect.php");
    include('head.php');

    $product_id = $_REQUEST['product_id'];
    $item_id = $_REQUEST['item_id'];
    $supplirer_id = $_REQUEST['supplirer_id'];

?>


<div class="container-fluid">

    <div id="productArchiveBlock" style="display:none">
        <h3>Product Change History</h3>
        <table id="productArchiveTable" class="table table-hover" data-toggle="table" data-pagination="true" data-show-pagination-switch="true"  data-show-columns="true" data-show-toggle="true" data-search="true" data-show-export="true" >
            <thead>

            </thead>
            <tbody>

            </tbody>
        </table>
    </div>

    <div id="itemArchiveBlock" style="display:none">
        <h3>Item Change History</h3>
        <table id="itemArchiveTable" class="table table-hover" data-toggle="table" data-pagination="true" data-show-pagination-switch="true"  data-show-columns="true" data-show-toggle="true" data-search="true" data-show-export="true">
            <thead>

            </thead>
            <tbody>

            </tbody>
        </table>
    </div>

    <div id="supplierArchiveBlock" style="display:none">
        <h3>Supplier Change History</h3>
        <table id="supplierArchiveTable" class="table table-hover" data-toggle="table" data-pagination="true" data-show-pagination-switch="true"  data-show-columns="true" data-show-toggle="true" data-search="true" data-show-export="true">
            <thead>

            </thead>
            <tbody>

            </tbody>
        </table>
    </div>

    <div id="workflowBlock" style="display:none">
        <h3>Workflow</h3>
        <table id="workflowTable" class="table table-hover" data-toggle="table" data-pagination="true" data-show-pagination-switch="true"  data-show-columns="true" data-show-toggle="true" data-search="true" data-show-export="true">
            <thead>

            </thead>
            <tbody>

            </tbody>
        </table>
    </div>


	<div id="nohistoryBlock" style="display:none">
        <h3>No History</h3>
    </div>

</div>

<script>

    function valueFormatter(value, item) {
        //console.log(value, item);
        if (item['field']) {
            return item[item['field']] || '';
        }
        return '';
    };

    var columns = [];
    columns['productArchive'] = [
        {
            field: 'product_id',
            title: 'ID',
            align: 'right',
            valign: 'middle',
            sortable: true
        },
        {
            field: 'product_desc',
            title: 'Product',
            align: 'left',
            valign: 'middle',
            sortable: true
        },
        {
            field: 'product_mhw_code',
            title: 'Product Code',
            align: 'left',
            valign: 'middle',
            sortable: true
        },
        {
            field: 'field',
            title: 'Field Name',
            align: 'left',
            valign: 'middle',
            sortable: true
        },
        {
            field: 'previous_value',
            title: 'Previous Value',
            align: 'left',
            valign: 'middle',
            sortable: true
        },
        {
            formatter: valueFormatter,
            title: 'Current Value',
            align: 'left',
            valign: 'middle',
            sortable: true
        },
        {
            field: 'edit_date',
            title: 'Date',
            align: 'left',
            valign: 'middle',
            sortable: true
        },
        {
            field: 'edit_user',
            title: 'User',
            align: 'left',
            valign: 'middle',
            sortable: true
        },
    ];

    columns['itemArchive'] = [
        {
            field: 'item_id',
            title: 'ID',
            align: 'right',
            valign: 'middle',
            sortable: true
        },
        {
            field: 'item_description',
            title: 'Item',
            align: 'left',
            valign: 'middle',
            sortable: true
        },
        {
            field: 'item_mhw_code',
            title: 'Item Code',
            align: 'left',
            valign: 'middle',
            sortable: true
        },
        {
            field: 'field',
            title: 'Field Name',
            align: 'left',
            valign: 'middle',
            sortable: true
        },
        {
            field: 'previous_value',
            title: 'Previous Value',
            align: 'left',
            valign: 'middle',
            sortable: true
        },
        {
            formatter: valueFormatter,
            title: 'Current Value',
            align: 'left',
            valign: 'middle',
            sortable: true
        },
        {
            field: 'edit_date',
            title: 'Date',
            align: 'left',
            valign: 'middle',
            sortable: true
        },
        {
            field: 'edit_user',
            title: 'User',
            align: 'left',
            valign: 'middle',
            sortable: true
        },
    ];

    columns['supplierArchive'] = [
        {
            field: 'supplier_id',
            title: 'ID',
            align: 'right',
            valign: 'middle',
            sortable: true
        },
        {
            field: 'supplier_name',
            title: 'Supplier',
            align: 'left',
            valign: 'middle',
            sortable: true
        },
        {
            field: 'field',
            title: 'Field Name',
            align: 'left',
            valign: 'middle',
            sortable: true
        },
        {
            field: 'previous_value',
            title: 'Previous Value',
            align: 'left',
            valign: 'middle',
            sortable: true
        },
        {
            formatter: valueFormatter,
            title: 'Current Value',
            align: 'left',
            valign: 'middle',
            sortable: true
        },
        {
            field: 'edit_date',
            title: 'Date',
            align: 'left',
            valign: 'middle',
            sortable: true
        },
        {
            field: 'edit_user',
            title: 'User',
            align: 'left',
            valign: 'middle',
            sortable: true
        },
    ];


    columns['workflow'] = [
        {
            field: 'record_id',
            title: 'ID',
            align: 'left',
            valign: 'middle',
            sortable: true
        },
        {
            field: 'workflow_type',
            title: 'Workflow Type',
            align: 'left',
            valign: 'middle',
            sortable: true
        },
        {
            field: 'edit_date',
            title: 'Date',
            align: 'left',
            valign: 'middle',
            sortable: true
        },
        {
            field: 'username',
            title: 'User',
            align: 'left',
            valign: 'middle',
            sortable: true
        },
    ];

    $(document).ready(()=>{
		var hist = false;
        var loadingMessage = '<span class="mhwLoading"><img src="css/img/Logo_Chevron_Animated.gif"/> Loading</span>';

        Object.keys(columns).forEach(function (qry_type) {

            //$('#'+qry_type+'Table').bootstrapTable('showLoading');

            $.post('query-request.php', {
                qry_type: qry_type,
                <?=$product_id>0 ? "product_id: $product_id," : ""?>
                <?=$item_id>0 ? "item_id: $item_id," : ""?>
                <?=$supplier_id>0 ? "supplier_id: $supplier_id," : ""?>
            }, function(rawData) { 
                var data = [];
                try {
                    data = JSON.parse(rawData);

                    if (data.length>0) {
                        $('#'+qry_type+'Block').show();
                        $('#'+qry_type+'Table').bootstrapTable('destroy').bootstrapTable('showLoading').bootstrapTable({
                            data: data,
                            formatLoadingMessage: () => loadingMessage,
                            detailView: false,
                            columns: columns[qry_type],
                        });
						hist=true;
						$('#nohistoryBlock').hide();
                    }

                } catch (error) {
                    //location.reload();
                    console.log(error, rawData);
                }
            });
        });

		if(hist===false){ $('#nohistoryBlock').show(); }
		else{ $('#nohistoryBlock').hide(); } 
    });

</script>

<?php
    include('footer.php');
?>
