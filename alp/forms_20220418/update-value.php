<?php

session_start();
if(!isset($_SESSION['mhwltdphp_user'])){
	die( "Not authenticated !" );  
}

error_reporting(E_ALL); //displays an error
include("dbconnect.php");

//Establishes the connection
$conn = sqlsrv_connect($serverName, $connectionOptions);
if( $conn === false) {
		die( print_r( sqlsrv_errors(), true));
}

$tableName = addslashes($_REQUEST['view_state']);
$id = intval($_POST['pk']);
$field = addslashes($_POST['name']);
$value = $_POST['value'];// can be an array

$tsql = "SELECT * FROM [$tableName] WHERE ID = ?";
$stmt= sqlsrv_query($conn, $tsql, array($id));
if (!$stmt) die (sqlsrv_errors());
$row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);
if (!$row) die ("Wrong ID...");

unset($row['ID']);
unset($row['created_at']);

$row['user'] = $_SESSION['mhwltdphp_user'];
$row[$tableName."_change_type"] = "Update";
$row['status'] = 'draft';

$row[$field] = is_array($value) ? implode($value, ',')  : addslashes($value);
//$row[$field] = is_array($value) ? array_reduce($value, function($carry, $item) {
//    return $carry.($carry==''?'':'|').addslashes($item);
//}, '') : addslashes($value);

if ($field=='deleted' && $value=='1') {
    $row[$tableName."_change_type"] = "Delete";
    $row['status'] = 'deleted';
} else if ($field=='deleted' && $value != '1') {
    $row[$tableName."_change_type"] = "Undelete";
    $row['status'] = 'restored';
} else if (substr($field, -10)=='_price_bot') {
    $tsql= "SELECT TOP 1 [bottles_per_case] FROM [dbo].[mhw_app_prod_item]
    WHERE [item_id] IN (
        SELECT itemID FROM [$tableName] WHERE ID = ?
    )";
    $stmt= sqlsrv_query($conn, $tsql, array($id));
    $item = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);
    $row[$tableName."_price_case"] = $value * $item["bottles_per_case"];
}

$fields = "[".implode(array_keys($row),"],[")."]";
//$values = str_replace("''","NULL","'".implode(array_values($row),"','")."'");
//$values = "'".implode(array_values($row),"','")."'";
$params = array_values($row);
$values = implode(array_fill(0, count($row), '?'),',');

$tsql = "INSERT INTO [$tableName] ( $fields ) VALUES ( $values ); SELECT SCOPE_IDENTITY();";

//echo $tsql;
$stmt = sqlsrv_prepare($conn, $tsql, $params);  

if( $stmt === false )  
{  
    echo "Statement could not be prepared.\n";  
    die( print_r( sqlsrv_errors(), true));  
}  

if( sqlsrv_execute($stmt) === false )  
{  
    echo "Statement could not be executed.\n";  
    die( print_r( sqlsrv_errors(), true));  
}  



$json = array(
    'result' => sqlsrv_rows_affected ( $stmt )
);
header('Content-Type: application/json');
echo json_encode($json);

/* Free statement and connection resources. */  
sqlsrv_free_stmt( $stmt);  
sqlsrv_close( $conn); 

?>