<?php
    //ini_set('display_errors', 1);
    //ini_set('display_startup_errors', 1);
    //error_reporting(E_ALL);
    
    include("sessionhandler.php");
    include("prepend.php");     
    include("settings.php");   

    if(!isset($_SESSION['mhwltdphp_user'])){
        header("Location: login.php");
    }
   
    include("dbconnect.php");
    include('head.php');

    $isAdmin = in_array($_SESSION['mhwltdphp_usertype'], ['ADMIN','SUPERUSER']);
    $showRefreshDistributorButton = $isAdmin || count($showDistributorsArray) > 0;
	
	$admin_type_user=false;
	$permitted_rules_info=$client_permitted_rules_info;
	if($_SESSION['mhwltdphp_usertype'] == "SUPERUSER" || $_SESSION['mhwltdphp_usertype'] == "ADMIN" ){ 
		$admin_type_user=true;
		$editableField = "editable: true,";
		$permitted_rules_info=$admin_permitted_rules_info;
	}

	if(!$admin_type_user){
		die( "MHW Admin access required" ); 
	}

?>

<link href="css/wforms-layout_v530-14.css" rel="stylesheet" type="text/css" />
<link href="css/theme-52661.css" rel="stylesheet" type="text/css" />
<link href="css/wforms-jsonly_v530-14.css" rel="alternate stylesheet" title="This stylesheet activated by javascript" type="text/css" />
<link href="css/wforms-inline.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/wforms_v530-14.js"></script>
<script type="text/javascript" src="js/localization-en_US_v530-14.js"></script> 


<style type="text/css">
     #searching_div{display:none;}
	 #state_form_data{display:none;}
    .item-not-found {color: #d21729 !important;}
    .validation-error {background-color: #ffc2ab !important;}
</style>


<div class="container-fluid">
    <div class="row justify-content-md-left float-left">
        <div class="col-md-auto oneField field-container-D" id="client_name-D">
			<label id="client_name-L" class="label preField " for="client_name"><b>Client Name</b></label>
			<div class="inputWrapper">
				<select id="client_name" name="client_name" title="Client Name" aria-required="true">
				<?php
				$clients = explode(";",$_SESSION['mhwltdphp_userclients']);
				if (count($clients)>1) {  
                    echo "<option value='all' class=''>ALL</option>"; 
                }
				foreach ($clients as &$clientvalue) {
					echo "<option value='$clientvalue' class=''>$clientvalue</option>";
				}
				?>					
				</select>
			</div>
		</div>
		<div class="col-md-auto oneField field-container-D">
			<label class="label preField " for="view_type"><b>Compliance Type</b></label>
			<div class="inputWrapper">
				<select id="view_type" name="view_type" title="View Type" aria-required="true">
				<?php
				foreach (array("Federal product compliance status", "State product compliance status") as $viewType) {
					echo "<option value='$viewType' >$viewType</option>";
				}
				?>					
				</select>
			</div>
		</div>
		
		<div class="col-md-auto align-self-center oneField field-container-D" id="add-new-D">
    		<button id="btn-add-new" class="btn btn-success"><i class="fa fa-search"></i> <span id='adv_searching_txt'>Show Advanced Search</span></button>
		</div>
		
		<div class="col-md-auto align-self-center oneField field-container-D" id="report_btn">
		        <a href="blr_reports.php" target="_blank" class="btn btn-success" role="button" aria-pressed="true"><i class="fas fa-check-square"></i><span>&nbsp; Reports</span></a>
        </div>
				
    </div>

<div class="row"><div class="col-sm-12">&nbsp;</div></div>
	  
<div class="wFormContainer" style="max-width: 100%; width:auto;" >
    <div class="row" id="searching_div">
        <div class="col-sm-12">		
			  <fieldset id="tfa_399" class="section">
				<legend>Advanced Search</legend>
				
<form id="searching_data">
  <div class="form-row">
    <div class="form-group col-md-3">
      <label for="s_product_id">Product ID</label>
      <input type="text" class="form-control" id="s_product_id" name="p.product_id" placeholder="Product ID">
    </div>
    <div class="form-group col-md-3">
      <label for="s_brand_name">Brand Name</label>
      <input type="text" class="form-control" id="s_brand_name" name="p.brand_name" placeholder="Brand Name">
    </div>
	<div class="form-group col-md-3">
      <label for="s_product_mhw_code">Product Code</label>
      <input type="text" class="form-control" id="s_product_mhw_code" name="p.product_mhw_code" placeholder="Product Code">
    </div>
	<div class="form-group col-md-3">
      <label for="s_product_desc">Product Description</label>
      <input type="text" class="form-control" id="s_product_desc" name="p.product_desc" placeholder="Product Description">
    </div>
  </div>

  <div id="federal_form_data">
  <div class="form-row">
    <div class="form-group col-md-3">
      <label for="p_status">Status</label>
      <input type="text" class="form-control" id="p_status" name="f.status" placeholder="Status">
    </div>
    <div class="form-group col-md-3">
      <label for="s_dateapproved">Dates of all statuses</label>
      <input type="text" class="form-control" id="s_dateapproved" name="f.dateapproved" placeholder="">
    </div>
	<div class="form-group col-md-3">
      <label for="p_federal_type">Federal Type</label>
      <input type="text" class="form-control" id="p_federal_type" name="p.federal_type" placeholder="Federal Type">
    </div>
	<div class="form-group col-md-3">
      <label for="s_product_class">Product Class</label>
      <input type="text" class="form-control" id="s_product_class" name="p.product_class" placeholder="Product Class">
    </div>
  </div>
  
  <div class="form-row">
    <div class="form-group col-md-3">
      <label for="s_ttb_id">TTB ID</label>
      <input type="text" class="form-control" id="s_ttb_id" name="p.ttb_id" placeholder="TTB ID">
    </div>
    <div class="form-group col-md-3">
      <label for="s_primaryfederalbasicpermitnumber">TTB Basic Importer#</label>
      <input type="text" class="form-control" id="s_primaryfederalbasicpermitnumber" name="f.primaryfederalbasicpermitnumber" placeholder="TTB Basic Importer">
    </div>
	<div class="form-group col-md-3">
      <label for="s_country">Country of Origin</label>
      <input type="text" class="form-control" id="s_country" name="p.country" placeholder="Country of Origin">
    </div>
	<div class="form-group col-md-3">
      <label for="s_legalnameusedonlabel">TTB Basic Importer Name</label>
      <input type="text" class="form-control" id="s_legalnameusedonlabel" name="f.legalnameusedonlabel" placeholder="TTB Basic Importer Name">
    </div>
  </div>
  
  
    <div class="form-row">
    <div class="form-group col-md-6">
      <label for="s_applicantname">MHW user that filed (If Applicable)</label>
      <input type="text" class="form-control" id="s_applicantname" name="f.applicantname" placeholder="MHW user that filed (If Applicable)">
    </div>
  </div>
  </div>
  
  
  
   <div id="state_form_data">
  <div class="form-row">
    <div class="form-group col-md-3">
      <label for="s_StateApprovalNumber">Brand label registration (BLR) # (if approved)</label>
      <input type="text" class="form-control" id="s_StateApprovalNumber" name="s.StateApprovalNumber" placeholder="">
    </div>
    <div class="form-group col-md-3">
      <label for="s_status">Status</label>
      <input type="text" class="form-control" id="s_status" name="s.status" placeholder="Status">
    </div>
	<div class="form-group col-md-3">
      <label for="s_effectiveDate">Dates of all statuses</label>
      <input type="text" class="form-control" id="s_effectiveDate" name="s.EffectiveDate" placeholder="">
    </div>
	
	<div class="form-group col-md-3">
      <label for="s_federal_type">Federal Type</label>
      <input type="text" class="form-control" id="s_federal_type" name="p.federal_type" placeholder="Federal Type">
    </div>
	
  </div>
  
  <div class="form-row">
  
    <div class="form-group col-md-3">
      <label for="s_ttb_id">State licensee (registration was filed under)</label>
      <input type="text" class="form-control" id="r_State" name="r.state_name" placeholder="State Name">
    </div>
    <div class="form-group col-md-3">
      <label for="s_Preparer">MHW user that filed (If Applicable)</label>
      <input type="text" class="form-control" id="s_Preparer" name="s.Preparer" placeholder="">
    </div>
  </div>
  
 
  </div>
  
 <?php 
 if($_SESSION['mhwltdphp_usertype'] == "SUPERUSER" || $_SESSION['mhwltdphp_usertype'] == "ADMIN" )
 { 
 ?> 
  <div class="form-check">
    <input type="checkbox" name="all_clients" value="all_clients" class="form-check-input" id="all_clients">
    <label class="form-check-label" for="all_clients">Include All Clients</label>
  </div>
  <br>
  <?php 
 }
 ?>
  <button type="button" id="s_sub_btn" class="btn btn-primary">Search &nbsp;&nbsp;&nbsp;<i class="fa fa-search"></i></button>
</form>
				
				
			  </fieldset>		
		</div>
	</div>
</div>	
	


    <table id="blrViewTable" class="table table-hover" data-toggle="table" data-pagination="false" data-show-pagination-switch="true"  data-show-columns="true" data-show-toggle="true" data-search="true" data-show-export="true" data-id-field="ID" data-editable-emptytext="...." data-editable-url="#">
        <thead>
        </thead>
        <tbody>
        </tbody>
    </table>

    <script>

	 function getJsonValue(row,k)
	  {
		  var return_val='';
		  $.each(row, function (rk, rval) {
						if(rk==k)
						{
							return_val=rval;
						}
					});
			return return_val;	
	  }
        //table button
		
		//function to initialize all of the in-modal functionality
		function INmodalStuff(init){
			$('.prodimg_thumb').off().on('click', function() {});
			//from image details to image full preview
			$('.prodimg_thumb').on('click',function(){
				var imageid = $(this).data('imageid');
				var productid = $(this).data('product');
				var imageurl = 'viewfiles.php?iid='+imageid+'&b='+productid;
				$('.modal-body').load(imageurl,function(){
					$('#fileModalx').modal({show:true});
					THUMBmodalStuff('prodimg_thumb'); 
				});
			});
			
			//go to upload page for product
			$('.prod_upload').bind('click', function () {
				var prod = $(this).data('prod');
				window.location = 'imageupload.php?pid='+prod;
			});
			
		};
		function THUMBmodalStuff(init){
			
			$('.prodimg_back').off().on('click', function() {});
			//back link in modal from image full to image details
			$('.prodimg_back').on('click',function(){
				var productid = $(this).data('product');
				var producturl = 'viewfiles.php?pid='+productid;
				$('.modal-body').load(producturl,function(){
					$('#fileModalx').modal({show:true});
					INmodalStuff('prodimg_thumb'); 
				});
			});
			
			$('.dwnld').off().on('click', function() {});
			//download image button
			$('.dwnld').on('click', function () {
				var imagefile = $(this).data('url');
				var imagename = $(this).data('imagename');
				$.ajax({
					url: imagefile,
					method: 'GET',
					xhrFields: {
						responseType: 'blob'
					},
					success: function (data) {
						var a = document.createElement('a');
						var url = window.URL.createObjectURL(data);
						a.href = url;
						a.download = imagename;
						a.click();
						window.URL.revokeObjectURL(url);
					}
				});
			});
			//go to upload page for product
			$('.prod_upload').bind('click', function () {
				var prod = $(this).data('prod');
				window.location = 'imageupload.php?pid='+prod;
			});
		
			
		};
		function OUTmodalStuff(init){
			$('.prodimg_btn').off().on('click', function() {});
			//initialize prod image modal functionality again
			$('.prodimg_btn').on('click',function(){
				var productid = $(this).data('product');
				var imageurl = 'viewfiles.php?pid='+productid;
				//alert('4');
				$('.modal-body').load(imageurl,function(){
					$('#fileModalx').modal({show:true});
					INmodalStuff('prodimg_btn'); 
				});
			});
			
			$('.insert_comments').off().on('click', function() {});
			//initialize insert comments modal functionality again
			$('.insert_comments').on('click',function(){
				var cmt_productid = $(this).data('product');
				$("#cmt_product_id").val(cmt_productid);
				$('#cmt_data').val('');
				$('#commentsModalx').modal({show:true});
				
			});
			
			$('.rules_info').off().on('click', function() {});
			
			    $('span#blr_rule_franchise').html('');
				$('span#blr_rule_at_rest').html('');
				$('span#blr_rule_tax').html('');
				$('span#blr_rule_state_reg').html('');
				$('span#blr_rule_approvals').html('');
				
			    $('span#blr_rule_who').html('');
				$('span#blr_rule_ctrl').html('');
				$('span#blr_rule_key').html('');
				$('span#blr_rule_additional_licensing').html('');
				$('span#blr_rule_approval_time').html('');
			    $('span#blr_rule_renewal').html('');
				$('span#blr_rule_renew_dates').html('');
				$('span#blr_rule_wine_ff').html('');
				$('span#blr_rule_spirit_ff').html('');
				$('span#blr_rule_beer_ff').html('');
				
			$('.rules_info').on('click',function(){
				var rule_state_name = $(this).data('state_name');
				var rule_federal_type = $(this).data('state_federal_type');
				var client = $('#client_name').val();
				
			  $.post('query-request.php', {
				qry_type:'blr-status',
				view_type:'get_state_blr_rules',
				rule_state_name:rule_state_name,
				rule_federal_type:rule_federal_type,
				client:client
				}, 
				function(blrRulesData){ 
				            var blr_rules_info = JSON.parse(blrRulesData);
							$.each( blr_rules_info, function( rule_k, rule_v ) {								
								$("span#blr_rule_franchise").html(rule_v.blr_rule_franchise);
								$("span#blr_rule_at_rest").html(rule_v.blr_rule_at_rest);
								$("span#blr_rule_tax").html(rule_v.blr_rule_tax);
								$("span#blr_rule_state_reg").html(rule_v.blr_rule_state_reg);
								$("span#blr_rule_approvals").html(rule_v.blr_rule_approvals);												
								$("span#blr_rule_who").html(rule_v.blr_rule_who);
								$("span#blr_rule_ctrl").html(rule_v.blr_rule_ctrl);
								$("span#blr_rule_key").html(rule_v.blr_rule_key);
								$("span#blr_rule_additional_licensing").html(rule_v.blr_rule_additional_licensing);
								$("span#blr_rule_approval_time").html(rule_v.blr_rule_approval_time);																
								$("span#blr_rule_renewal").html(rule_v.blr_rule_renewal);
								$("span#blr_rule_renew_dates").html(rule_v.blr_rule_renew_dates);
								$("span#blr_rule_wine_ff").html(rule_v.blr_rule_wine_ff);
								$("span#blr_rule_spirit_ff").html(rule_v.blr_rule_spirit_ff);
								$("span#blr_rule_beer_ff").html(rule_v.blr_rule_beer_ff);								
							});
							
				  });
		  
		  
				//$("#cmt_product_id").val(cmt_productid);
				//$('#cmt_data').val('');
				$('#rulesModalx').modal({show:true});
				
			});

			//go to upload page for product
			$('.prod_upload').bind('click', function () {
				var prod = $(this).data('prod');
				window.location = 'imageupload.php?pid='+prod;
			});
			
		};
		
		window.operateEvents = {
			'click .like': function (e, value, row, index) {
			  //alert('You click like action, row: ' + JSON.stringify(row))
			},
			'click .prodimg_btn': function (e, value, row, index) {
			 //alert("jahid");	
			  //alert('You click prodimg_btn action, row: ' + JSON.stringify(row))
			},
			}

		
		function operateFormatter(value, row, index) {
			return [
				'<button type="button" class="btn btn-outline-secondary prodimg_btn btn-sm bg_arrow_lightblue" data-product="'+row.product_id+'">',
				'Files  <span class="badge badge-light">'+row.filecount+'</span></button>',
			].join('')
		}
		
		
		
		window.operateEventsTTBID = {
			'click .view_cola_details': function (e, value, row, index) {
				var ttbUrl = '<?php echo $ttblink; ?>';  //defined in settings.php
				var ttbVal = row.prefix_ttb_id;
				var ttbInt = parseInt(ttbVal);  //TTB value must be integer to be valid, but may begin with leading zeros so use text value for url
				if(ttbVal!="" && ttbVal!="P" && ttbVal!="p" && ttbInt){
				window.open(ttbUrl+ttbVal);
				}
			},
			}

		
		function operateFormatterTTBID(value, row, index) {	
		
		        var ttbVal = row.prefix_ttb_id;
				var ttbInt = parseInt(ttbVal);  //TTB value must be integer to be valid, but may begin with leading zeros so use text value for url
				if(ttbVal!="" && ttbVal!="P" && ttbVal!="p" && ttbInt){
				return [
						'<button type="button" class="btn btn-dark btn-sm bg_arrow_darkblue view_cola_details" data-target="'+row.prefix_ttb_id+'">',
					'<i class="fas fa-file-contract"></i> TTB Site <i class="fas fa-external-link-square-alt"></i></button>',
					].join('');
				}
				else 
				{
					return [
						'<button type="button" class="btn btn-dark btn-sm bg_arrow_darkblue view_cola_details disabled" data-target="'+row.prefix_ttb_id+'">',
					'<i class="fas fa-file-contract"></i> TTB Site <i class="fas fa-external-link-square-alt"></i></button>',
					].join('');
				}
			
		}
		
		
				
		window.operateEventsFlag = {
			'click .insert_comments': function (e, value, row, index) {
			 //alert("jahid");	
			  //alert('You click prodimg_btn action, row: ' + JSON.stringify(row))
			},
			}

		
		function operateFormatterFlag(value, row, index) {	
		  return [
						'<button type="button" class="btn insert_comments" data-product="'+row.product_id+'">',
					'<i class="far fa-comment" style="font-size:36px"></i></button>',
					].join('');
		}
		
		
		window.operateEventsState = {
			'click .rules_info': function (e, value, row, index) {
			 //alert("jahid");	
			 // alert("Working for showing BLR Rules Model ");
			},
			}

		
		function operateFormatterState(value, row, index) {	
		  return [  
		            '<button type="button" class="btn rules_info" data-state_name="'+row.State+'" data-state_federal_type="'+row.federal_type+'">',
					'<i class="fa fa-info-circle" style="font-size:20px;"></i></button> &nbsp;&nbsp;'+row.State].join('');
		}

				
		function UnFinalizedFormatter(value, row, index) {
		  console.log("jahid:"+value);
		  if(value!='' && value>0)
		  {
			  return ['<label><input data-index="0" name="btSelectItem" type="checkbox" value="'+row.product_id+'"><span></span></label>'].join('')
		  }
		  
		}
		
		
		function getFormData($form){
			var unindexed_array = $form.serializeArray();
			var indexed_array = {};

			$.map(unindexed_array, function(n, i){
				indexed_array[n['name']] = n['value'];
			});

			return indexed_array;
		}


		
		
		
		
        
        var columns = [];
		
        columns['Federal product compliance status'] = [
			{
			  field: 'selected',
			  checkbox: true,
			  align: 'center',
			  valign: 'middle'
			},
			<?php if ($admin_type_user) 
            { ?>
			{
			  field: 'product_id,',
			  title: '',
			  align: 'center',
			  valign: 'middle',
			  events: window.operateEventsFlag,
			  formatter: operateFormatterFlag
			},
        <?php } ?>
			{
			  field: 'product_id',
			  title: 'Files',
			  align: 'center',
			  valign: 'middle',
			  events: window.operateEvents,
			  formatter: operateFormatter
			}, 
			{
			  field: 'prefix_ttb_id,',
			  title: 'View COLA Details',
			  align: 'center',
			  valign: 'middle',
			  events: window.operateEventsTTBID,
			  formatter: operateFormatterTTBID
			},
            {
                field: 'product_mhw_code',
                title: 'Product Code',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'brand_name',
                title: 'Brand name',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'product_desc',
                title: 'Product Description',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'status',
                title: 'Status',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'dateapproved',
                title: 'Dates of all statuses',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'product_id',
                title: 'Product ID',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'federal_type',
                title: 'Federal Type',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'product_class',
                title: 'Product Class (Domestic / Foreign)',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'TTB_ID',
                title: 'TTB ID',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'primaryfederalbasicpermitnumber',
                title: 'TTB Basic Importer #',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'country',
                title: 'Country of Origin',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'legalnameusedonlabel',
                title: 'TTB Basic Importer Name',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'datecompleted',
                title: 'Estimated Date of Approval',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'applicantname',
                title: 'MHW user that filed (If Applicable)',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'user_notes',
                title: 'MHW user notes',
                align: 'center',
                valign: 'middle',
                sortable: true
            }
            
        ];
        
        
        columns['State product compliance status'] = [
		    {
			  field: 'selected',
			  checkbox: true,
			  align: 'center',
			  valign: 'middle'
			},
			<?php if ($admin_type_user) 
            { ?>
			{
			  field: 'product_id,',
			  title: '',
			  align: 'center',
			  valign: 'middle',
			  events: window.operateEventsFlag,
			  formatter: operateFormatterFlag
			},
        <?php } ?>
			{
			  title: 'Files',	
			  field: 'product_id',
			  align: 'center',
			  valign: 'middle',
			  events: window.operateEvents,
			  formatter: operateFormatter
			}, 
			{
			  field: 'prefix_ttb_id',
			  title: 'View COLA Details',
			  align: 'center',
			  valign: 'middle',
			  events: window.operateEventsTTBID,
			  formatter: operateFormatterTTBID
			},
            {
                field: 'product_mhw_code',
                title: 'Product ID',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'brand_name',
                title: 'Brand name',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'product_desc',
                title: 'Product Description',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'StateApprovalNumber',
                title: 'Brand label registration (BLR) # (if approved)',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'Status',
                title: 'Status',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'EffectiveDate',
                title: 'Dates of all statuses',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'federal_type',
                title: 'Federal Type',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'State',
                title: 'State licensee (registration was filed under)',
                align: 'center',
                valign: 'middle',
                sortable: true,
				events: window.operateEventsState,
			    formatter: operateFormatterState
            },
            {
                field: 'est_dt_approval',
                title: 'Estimated Date of Approval (if pending)',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'Preparer',
                title: 'MHW user that filed (If Applicable)',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'user_notes',
                title: 'MHW user notes',
                align: 'center',
                valign: 'middle',
                sortable: true
            }
        ];
		
		
		
		var subColumns = [];
		
		subColumns['State product compliance status'] = [
            {
                field: 'product_mhw_code',
                title: 'Product ID',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'brand_name',
                title: 'Brand name',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'product_desc',
                title: 'Product Description',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'status',
                title: 'Status',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'dateapproved',
                title: 'Dates of all statuses',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'product_id',
                title: 'Product ID',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'federal_type',
                title: 'Federal Type',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'product_class',
                title: 'Product Class (Domestic / Foreign)',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'TTB_ID',
                title: 'TTB ID',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'primaryfederalbasicpermitnumber',
                title: 'TTB Basic Importer #',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'country',
                title: 'Country of Origin',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'legalnameusedonlabel',
                title: 'TTB Basic Importer Name',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'datecompleted',
                title: 'Estimated Date of Approval',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'applicantname',
                title: 'MHW user that filed (If Applicable)',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'user_notes',
                title: 'MHW user notes',
                align: 'center',
                valign: 'middle',
                sortable: true
            }
            
        ];
        
        
        subColumns['Federal product compliance status'] = [
            {
                field: 'product_mhw_code',
                title: 'Product ID',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'brand_name',
                title: 'Brand name',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'product_desc',
                title: 'Product Description',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'StateApprovalNumber',
                title: 'Brand label registration (BLR) # (if approved)',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'status',
                title: 'Status',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'EffectiveDate',
                title: 'Dates of all statuses',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'federal_type',
                title: 'Federal Type',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'State',
                title: 'State licensee (registration was filed under)',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'EffectiveDate',
                title: 'Estimated Date of Approval (if pending)',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'Preparer',
                title: 'MHW user that filed (If Applicable)',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'user_notes',
                title: 'MHW user notes',
                align: 'center',
                valign: 'middle',
                sortable: true
            }
            
        ];
		
		


        $(document).ready(()=>{

            var getHeight = () => {
                var winH = $(window).height();
                var navH = $(".navbar").height();
                return winH - navH - 100;
            }
			
			//Bootstrap table 
            var $table = $('#blrViewTable');
			var $remove = $('#remove')
			var selections = []
			
			function getIdSelections() {
			return $.map($table.bootstrapTable('getSelections'), function (row) {
				return row.product_id;
			})
		}

			
            var updateTable = () => {
                var qry_type = "blr-status";
                var view_type = $('#view_type').val();
                var client = $('#client_name').val();
				
				/* change form element as per type : start */
				
				if(view_type=="Federal product compliance status")
				{					
					$("#federal_form_data").show();
					$("#state_form_data").hide();
				}
				else 
				{
					$("#state_form_data").show();
					$("#federal_form_data").hide();
				}
				
				/* change form element as per type : end */
				
				/* get the checked Item List : start */
				var prodids = getIdSelections();
				var prodlistids = JSON.stringify(prodids);
				/* get the checked Item List : End */
				
				/* get advance searching data : start */
				var b_text=$("span#adv_searching_txt").text();
					if(b_text=="Hide Advanced Search")
					{
						var $form_id = $("#searching_data");
						var form_data = getFormData($form_id);
						console.log("form data :::"+JSON.stringify(form_data));
						var searching_data=JSON.stringify(form_data);
						
					}
					else 
					{
					var searching_data="";	
					}
				/* get advance searching data : end */
                
                var loadingMessage = '<span class="mhwLoading"><img src="css/img/Logo_Chevron_Animated.gif"/> Loading</span>';
				var errorMessage = '<span class="mhwError"><img src="css/img/Logo_Chevron.png" height="64" /> Error Loading</span>';

                $('#blrViewTable').bootstrapTable('showLoading');
                console.log(view_type, columns[view_type]);
                $.post('query-request.php', {
                    qry_type: qry_type,
                    view_type: view_type,
                    client: client,
					prodlistids:prodlistids,
					searching_data:searching_data,
                }, (dataPF) => { 
                    //console.log(dataPF);
                    var data = [];
					$('#blrViewTable').data('id-field', 'product_id');
                    try {
                        data = JSON.parse(dataPF);
                        $('#blrViewTable').bootstrapTable('destroy').bootstrapTable('showLoading').bootstrapTable({
                            data: data,
                            height: getHeight(),
                            stickyHeader:true,
                            stickyHeaderOffsetY:60,
                            formatLoadingMessage: () => loadingMessage,
                            detailView: false,
                            columns: columns[view_type],
                            //fixedColumns:true,
                            //fixedNumber:7,
							/*
                            onExpandRow: (idx, row, el) => {
                                var subTable = el.html('<table class="table-info"></table>').find('table');
                                subTable.bootstrapTable({formatLoadingMessage: () => loadingMessage}).bootstrapTable('showLoading');
                                var qry_type = "blr-status-expand";
                                var key = view_type == 'State Requests' ? row.blr_state_abbrev : view_type == 'All States' ? row.state_name : row.product_id;
                                $.post('query-request.php', {
                                    key: key,
                                    qry_type: qry_type,
                                    view_type: view_type,
                                    client: client,
                                }, (dataRaw) => {
                                    try {
                                        dataExpand = JSON.parse(dataRaw);
                                        //item sub table
                                        subTable.bootstrapTable('destroy').bootstrapTable('showLoading').bootstrapTable({
                                            data: dataExpand,
                                            formatLoadingMessage: () => loadingMessage,
                                            detailView: false,
                                            columns: subColumns[view_type]
                                        })

                                    } catch (error) {
                                        //location.reload();
                                        console.log(error, dataPF);
                                    }
                                })
                            }
							*/
							// on expand row
                        }); //show main table
                        
                    } catch (error) {
                        //location.reload();
                        console.log(error, dataPF);
						$('.mhwLoading').html(errorMessage);
                    }
					
				OUTmodalStuff('all'); //bind modal & buttons
					

                }).fail(function(jqXHR, textStatus, errorThrown){
					$('.mhwLoading').html(errorMessage);
					if(errorThrown=='Internal Server Error'){
						alert("Error Loading.  If error persists, please try filtering data with Advanced Search");
					}
				});
            }; // updateTable
				
				$table.on('all.bs.table', function (e, name, args) {
				  console.log(name, args);
				});
				
			$table.on('load-error.bs.table', function (e, status) {
				$('.mhwLoading').html(errorMessage);
                });

            $('#view_type').on('change',updateTable);			
            $('#client_name').on('change',updateTable);
			
			$("#s_sub_btn").on("click", updateTable);

            updateTable();
           
        });

    </script>
	
	<div class="modal modal-wide fade" id="fileModalx" tabindex="-1" role="dialog" aria-labelledby="fileModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="fileModalLabel">Files</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      </div>
	  <div id='preview'></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="commentsModalx" tabindex="-1" role="dialog" aria-labelledby="commentsModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Insert Comments</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-3">
	  <form id="cmt_form" method="post">
	  <div class="md-form mb-5 form-group">
	      <input type="hidden" value="0" name="cmt_product_id" id="cmt_product_id">
	      <textarea required placeholder="Enter Your Comments" class="form-control" id="cmt_data" name="cmt_data" rows="3"></textarea>
        </div>
		<div class="md-form mb-5 d-flex justify-content-center form-group">
         <button type="submit" id="cmt_sub_btn" class="btn btn-primary">Submit</button>
        </div>
	  
	  </form>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>



<div class="modal fade" id="rulesModalx" tabindex="-1" role="dialog" aria-labelledby="rulesModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">BLR State Rules </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
	  
      <div class="modal-body">
	  <table class="table table-striped table-bordered">
          <thead>
          </thead>
          <tbody >
              <?php 
			  $tr_levels="";
				foreach($permitted_rules_info as $rk=>$rv)
				{
					$lebel_txt=$blr_rules_info_level[$rk];
					if($permitted_rules_info[$rk]==1)
					{
						$tr_levels=$tr_levels."<tr><th width='40%' scope='row'>$lebel_txt</th><td width='60%'><span id='".$rk."'></span></td></tr>";		
					}
					
				}
              echo $tr_levels; 
			  ?>			  
          </tbody>
      </table>  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>




<script>
        $("span#adv_searching_txt").click(function () {
            $(this).text(function(i, v){				
			   if(v === 'Show Advanced Search')
			   {
				 $("#searching_div").show();					 
			   }
			   else 
			   {
				 $("#searching_div").hide();  
			   }
			   return v === 'Show Advanced Search' ? 'Hide Advanced Search' : 'Show Advanced Search';
            })
        });
		
$(function(){
    $('#cmt_sub_btn').click(function(e){
      e.preventDefault();
	  var cmt_product_id=$('#cmt_product_id').val();
	  var cmt_data=$('#cmt_data').val();
	  var compliance_type = $('#view_type').val();
	  var client = $('#client_name').val();
  				
	  $.post('query-request.php', {
				qry_type:'blr-status',
				view_type:'blr_status_cmt',
				compliance_type:compliance_type,
				cmt_product_id:cmt_product_id,
				product_cmt:cmt_data,
				client: client,
				}, 
				function(cmtSuccessData){ 
					//var cmtSuccess = JSON.parse(cmtSuccessData);
					$('#commentsModalx').modal('hide');
					alert("Comments has been submitted successfully");
		  });						
	  
    });
});


</script>


</div>


<?php
    include('footer.php');
?>
    