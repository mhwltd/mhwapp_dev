<?php
include('functions.php');
include("sessionhandler.php");

//error_reporting(E_ALL);
//ini_set('display_errors', '1');

if($_SESSION['mhwltdphp_usertype'] != "SUPERUSER" && $_SESSION['mhwltdphp_usertype'] != "ADMIN" ){ 
   // echo "Access Denied"; exit(0); // this line is commented as client can edit draft status products or items.
}

if(!($_SESSION['mhwltdphp_usertype']) || $_SESSION['mhwltdphp_usertype']=="" || !($_SESSION['mhwltdphp_user']) || $_SESSION['mhwltdphp_user']=="")
{
	echo "Access Denied"; exit(0);
}

include("dbconnect.php");

//Establishes the connection
$conn = sqlsrv_connect($serverName, $connectionOptions);
if( $conn === false) {
	print_r( sqlsrv_errors()); exit(0);
}

$current_user = $_SESSION['mhwltdphp_user'];
$current_date = date("Y-m-d H:i:s");

$table = addslashes($_POST['tn']);
$product_id = intval($_POST['product_id']);
$id=0;
$field = addslashes($_POST['name']);
$value = addslashes($_POST['value']);
$clientlist=$_POST['clientlist'];
$item_des_sql='';

$admin_confirm='';
if(isset($_POST['confirm_txt']))
{
	$admin_confirm=$_POST['confirm_txt'];
}
	

if($value!="")
 {

    if ($table == 'mhw_app_prod') {
        $key = 'product_id';
        $dataTableName = 'mhw_app_prod';
        $logsTableName = 'mhw_app_prod_archive';
		$id=$product_id;
		$item_des_sql="$id";
		
		if($field=='supplier_id')
		{
			$tsql_supplier_opts = "select top 1 supplier_id  from mhw_app_prod_supplier where client_name in ($clientlist) and active=1 and deleted=0 and supplier_name='$value'";
			$getResults_supplier_opts = sqlsrv_query($conn, $tsql_supplier_opts);

			if ($getResults_supplier_opts == FALSE)
				echo (sqlsrv_errors());

			while ($row_supplier_opts = sqlsrv_fetch_array($getResults_supplier_opts, SQLSRV_FETCH_ASSOC)) {
				$value=$row_supplier_opts['supplier_id'];
			}
		}
				
        $tsql = "SELECT TOP 1 $key, '$field' AS [field], [$field] AS [previous_value] FROM [$dataTableName] WHERE product_id = ?";
        
    }
	
	
	$stmt= sqlsrv_query($conn, $tsql, array($id));
    if (!$stmt) die (sqlsrv_errors());

    $row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);
    if (!$row) die ("Wrong ID...");

    $row['edit_user'] = $current_user;
    $row['edit_date'] = $current_date;

    $fields = "[".implode("],[", array_keys($row))."]";
    $params = array_values($row);
    $values = implode(",", array_fill(0, count($row), '?'));

    $tsql = "INSERT INTO [$logsTableName] ( $fields ) VALUES ( $values ); SELECT SCOPE_IDENTITY();";
    //echo "<pre>"; print_r($params); exit;
    //echo $tsql; exit;
    $stmt = sqlsrv_prepare($conn, $tsql, $params);  

    if( $stmt === false )
    {  
        echo "Statement could not be prepared.\n";  
        die( print_r( sqlsrv_errors(), true));  
    }  

    if( sqlsrv_execute($stmt) === false )  
    {  
        echo "Statement could not be executed.\n";  
        die( print_r( sqlsrv_errors(), true));  
    }  



    $tsql = "UPDATE [$dataTableName] SET [$field] = ? WHERE [$key] = ?; SELECT SCOPE_IDENTITY();";

    //echo $tsql; exit;
    $stmt = sqlsrv_prepare($conn, $tsql, array($value, $id));

    if( $stmt === false )  
    {  
        echo "Statement could not be prepared.\n";  
        die( print_r( sqlsrv_errors(), true));  
    }  

    if( sqlsrv_execute($stmt) === false )  
    {  
        echo "Statement could not be executed.\n";  
        die( print_r( sqlsrv_errors(), true));  
    }  
	
	
/* item description recalculation : start */ 

 if (in_array($field, ['federal_type','product_desc']) && $item_des_sql!='' && $admin_confirm=="confirmed_yes") {
	 
	 $mhwltdphp_userclients=$_SESSION['mhwltdphp_userclients'];
	 $item_tsql="SELECT i.[item_id],i.[item_client_code],i.[item_mhw_code],i.[item_description]
    ,i.[container_type],i.[container_size],i.[stock_uom],i.[bottles_per_case],i.[upc],i.[scc],i.[vintage],i.[various_vintages],i.[height],i.[length],i.[width],i.[weight],i.[chill_storage],i.[item_status],i.[outer_shipper],i.[bottle_material],i.[create_via],i.[create_date],i.[edit_date],p.[product_id],p.[client_code],p.[client_name]
	,p.[brand_name],p.[product_desc]
	,p.[product_mhw_code]
	,p.[federal_type]
	,convert(varchar, i.create_date, 120) as item_creation_date
	,i.finalized			
	FROM [dbo].[mhw_app_prod_item] i WITH (NOLOCK) 
	left outer join [dbo].[mhw_app_prod] p WITH (NOLOCK) on p.[product_id] = i.[product_id] 
	WHERE i.finalized=0 and p.[client_name] IN ('".$mhwltdphp_userclients."') and i.[product_id] in ($item_des_sql) AND p.[active] = 1 AND p.[deleted] = 0 AND i.[active] = 1 AND i.[deleted] = 0  AND p.[product_desc] <> 'STAR'
	ORDER BY p.[product_id], i.[create_date] desc";
	 
	$getResults_validItemList = sqlsrv_query($conn, $item_tsql);

	if ($getResults_validItemList == FALSE)
		echo (sqlsrv_errors());

    $all_finalized_ids="";
	while ($row_validItemList = sqlsrv_fetch_array($getResults_validItemList, SQLSRV_FETCH_ASSOC)) {
		$item_federal_type=$row_validItemList['federal_type'];
		$item_vintage=$row_validItemList['vintage'];
		$item_id=$row_validItemList['item_id'];
		$item_old_des=$row_validItemList['item_description'];
		
		
			$new_item_des=generate_item_desc($row_validItemList['federal_type'],$row_validItemList['container_type'],$row_validItemList['container_size'],$row_validItemList['stock_uom'],$row_validItemList['outer_shipper'],$row_validItemList['product_desc'],$row_validItemList['vintage'],$row_validItemList['bottles_per_case']);
		    
			if($new_item_des!=$item_old_des)
			{
			$tsql ="UPDATE [dbo].[mhw_app_prod_item] SET [item_description] = ? WHERE [item_id] = ?; SELECT SCOPE_IDENTITY();";
			$stmt = sqlsrv_prepare($conn, $tsql, array($new_item_des, $item_id));
					    
				if( $stmt === false )  
				{  
					echo "Statement could not be prepared.\n";  
					die( print_r( sqlsrv_errors(), true));  
				}  
				
				if( sqlsrv_execute($stmt) === false )  
				{  
					echo "Statement could not be executed.\n";  
					die( print_r( sqlsrv_errors(), true));  
				}
				
			 
			$tsql_arch = "INSERT INTO [mhw_app_prod_item_archive] (item_id,field,previous_value,edit_date,edit_user) VALUES (?, ?, ?, ?, ?); SELECT SCOPE_IDENTITY();";
			$stmt_arch = sqlsrv_prepare($conn, $tsql_arch, array($item_id, 'item_description', $item_old_des, $current_date, $current_user)); 
			if( $stmt_arch === false )  
				{  
					echo "Statement could not be prepared.\n";  
					die( print_r( sqlsrv_errors(), true));  
				}  
				
				if( sqlsrv_execute($stmt_arch) === false )  
				{  
					echo "Statement could not be executed.\n";  
					die( print_r( sqlsrv_errors(), true));  
				}
			
			$tsql_item_wf= "INSERT INTO [mhw_app_workflow] VALUES ('item edit',$item_id,'$current_date','$current_date','$current_user',1,0)";
		    $getResults_item_wf= sqlsrv_query($conn, $tsql_item_wf);			
			}
			
	}
 }
/* item description recalculation : end */  
	

	$json = array(
    'result' => sqlsrv_rows_affected ( $stmt )
		);
		header('Content-Type: application/json');
		echo json_encode($json);
		
		
 }
else 
{
	echo "operation not successfully";
}

/* Free statement and connection resources. */  
sqlsrv_free_stmt( $stmt);  
sqlsrv_close( $conn);  

?>

