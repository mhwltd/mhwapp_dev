<?php
include('head.php');

//no authentication required

?>

<!--Main layout-->
<main>
<div class="container-fluid">


<?php
	//echo "<pre>";
	//var_dump($_REQUEST); 
	//echo "</post>";

    $serverName = "mhwtestdbsvr.database.windows.net"; // update me
    $connectionOptions = array(
        "Database" => "mhwTestDB", // update me
        "Uid" => "mhwtest", // update me
        "PWD" => "mhwt3st3r01!" // update me
    );
    //Establishes the connection
    $conn = sqlsrv_connect($serverName, $connectionOptions);
	if( $conn === false) {
    		die( print_r( sqlsrv_errors(), true));
	}

	$i=0; 
	$errorlog="";
	$reqlog="";
	$rows=intval($_POST['tfa_1'][0]);
	//echo "rows".$rows;

	while ($i<=$rows){

	    $tsql1= "EXEC [dbo].[usp_user_request] 
		 @acct_name_salesforce = ''
	      ,@business_name = '".$_POST['business_name']."'
	      ,@first_name = '".$_POST['first_name'][$i]."'
	      ,@last_name = '".$_POST['last_name'][$i]."'
	      ,@user_id_requested  = '".$_POST['tfa_user_id'][$i]."'";
		  
	      $tsql2= ",@password_requested = '".$_POST['tfa_password'][$i]."'";
		  
	      $tsql3= ",@email  = '".$_POST['email'][$i]."'
	      ,@role  = '".$_POST['role'][$i]."'
	      ,@ics_compliance_reports = '".$_POST['ics'][$i]."'
	      ,@onboarding_emails  = '".$_POST['onboarding'][$i]."'
	      ,@request_via  = 'mwhltd.app/requestaccess.php'";
		  
		 
		  //prep sql
		  $tsql=$tsql1.$tsql2.$tsql3;
		  //echo  $tsql."<br />";
		   
		   
		  //replace password for log & append to log variable
		  $reqlog.=$tsql1.",@password_requested = 'hidden'".$tsql3."\n";
		  
		  $stmt = sqlsrv_query($conn, $tsql);
		  
		  if( $stmt === false ) {
		  	if( ($errors = sqlsrv_errors() ) != null) {
		        foreach( $errors as $error ) {
					$errorlog.= $tsql1.",@password_requested = 'hidden'".$tsql3."\n";
		            $errorlog.= "SQLSTATE: ".$error[ 'SQLSTATE']."\n";
		            $errorlog.= "code: ".$error[ 'code']."\n";
		            $errorlog.= "message: ".$error[ 'message']."\n";
		        }
		    }
			$errorFree=0;
		  }
		  else{
		  	$errorFree=1;
		  }
	  
	  $i++;
	}

    sqlsrv_free_stmt($stmt);

	if($errorFree==1){
		if($i>1){
			echo "<div class=\"row\"><div class=\"col-sm-8\"><div class=\"card\"><div class=\"card-body\"><h5 class=\"card-title\">Thank you</h5><p class=\"card-text\">Your Web Access Forms have been submitted.  Each user should receive an email response when their CRD login has been provisioned.</p></div></div></div>";
		}
		else{
			echo "<div class=\"row\"><div class=\"col-sm-8\"><div class=\"card\"><div class=\"card-body\"><h5 class=\"card-title\">Thank you</h5><p class=\"card-text\">Your Web Access Form has been submitted.  You should receive an email response when your CRD login has been provisioned.</p></div></div></div>";
		}  
		$fp1 = fopen('logs/reqlog.txt', 'a');
		$reqlog.="\n";
		fwrite($fp1, $reqlog);
		fclose($fp1);
	}
	else{
		echo "<div class=\"row\"><div class=\"col-sm-8\"><div class=\"card\"><div class=\"card-body\"><h5 class=\"card-title\">Error</h5><p class=\"card-text\">There was an error saving your Web Access Form.  Error details have been logged.</p></div></div></div>";
		$fp2 = fopen('logs/req3rr0rlog.txt', 'a');
		$errorlog.="\n";
		fwrite($fp2, $errorlog);
		fclose($fp2);
	}
	

	
	/*
	$procedure_params = array(
	array(&$myparams['Item_ID'], SQLSRV_PARAM_OUT),
	array(&$myparams['Item_Name'], SQLSRV_PARAM_OUT)
	);

	$sql = "EXEC stp_Create_Item @Item_ID = ?, @Item_Name = ?";
	$stmt = sqlsrv_prepare($conn, $sql, $procedure_params);
	*/
?>
</div>
</main>
<!--Main layout-->
  
</html>