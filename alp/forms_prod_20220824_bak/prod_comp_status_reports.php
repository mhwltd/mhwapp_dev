<?php
    //ini_set('display_errors', 1);
    //ini_set('display_startup_errors', 1);
    //error_reporting(E_ALL);
    
    include("sessionhandler.php");
    include("prepend.php");     
    include("settings.php");   

    if(!isset($_SESSION['mhwltdphp_user'])){
        header("Location: login.php");
    }
    
    include("dbconnect.php");
    include('head.php');

    $isAdmin = in_array($_SESSION['mhwltdphp_usertype'], ['ADMIN','SUPERUSER']);
    $showRefreshDistributorButton = $isAdmin || count($showDistributorsArray) > 0;
	
	$admin_type_user=false;
	if($_SESSION['mhwltdphp_usertype'] == "SUPERUSER" || $_SESSION['mhwltdphp_usertype'] == "ADMIN" ){ 
		$admin_type_user=true;
		$editableField = "editable: true,";
	}

	if(!$admin_type_user){
		$blrclients='';
		$beta_client_user=false;
		$clientsBETA = explode(";",$_SESSION['mhwltdphp_userclients']);

		$conn = sqlsrv_connect($serverName, $connectionOptions);
		if( $conn === false) die( print_r( sqlsrv_errors(), true));

		foreach ($clientsBETA as &$clientBETAvalue) {
			//if($clientBETAvalue=='MHW Web Demo') { $beta_client_user=true; }

			$tsql= "SELECT * FROM [mhw_app_client_access] WHERE [user_application] = 'BLR_Status' and [active] = 1 and [client_name] = '".$clientBETAvalue."'";
			$stmt = sqlsrv_query( $conn, $tsql);
			if ( $stmt === false ) die( print_r( sqlsrv_errors(), true));
			while ( $row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC) ) { 
				$blrclients.=$row['client_name'].';';
				$blrclientclass[$row['client_name']] = $row['user_class']; //20211029 DS
				$beta_client_user=true;
			}
		}
		$blrclients = substr($blrclients,0,strlen($blrclients)-1);
		$_SESSION['mhwltdphp_userclients'] = $blrclients;

		sqlsrv_free_stmt($stmt);

		if(!$beta_client_user){
			die( "MHW Admin access required" ); 
		}
	}
?>

<link href="css/wforms-layout_v530-14.css" rel="stylesheet" type="text/css" />
<link href="css/theme-52661.css" rel="stylesheet" type="text/css" />
<link href="css/wforms-jsonly_v530-14.css" rel="alternate stylesheet" title="This stylesheet activated by javascript" type="text/css" />
<link href="css/wforms-inline.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/wforms_v530-14.js"></script>
<script type="text/javascript" src="js/localization-en_US_v530-14.js"></script> 


<style type="text/css">
     #searching_div{display:none;}
	 #state_form_data{display:none;}
    .item-not-found {color: #d21729 !important;}
    .validation-error {background-color: #ffc2ab !important;}
</style>


<div class="container-fluid">
    <div class="row justify-content-md-left float-left">
        <div class="col-md-auto oneField field-container-D" id="client_name-D">
			<label id="client_name-L" class="label preField " for="client_name"><b>Client Name</b></label>
			<div class="inputWrapper">
				<select id="client_name" name="client_name" title="Client Name" aria-required="true">
				<?php
				$clients = explode(";",$_SESSION['mhwltdphp_userclients']);
				if (count($clients)>1) {  
                    echo "<option value='all' class=''>ALL</option>"; 
                }
				foreach ($clients as &$clientvalue) {
					echo "<option value='$clientvalue' class='".$blrclientclass[$clientvalue]."'>$clientvalue</option>"; //20211029 DS
				}
				?>					
				</select>
			</div>
		</div>
		<div class="col-md-auto oneField field-container-D">
			<label class="label preField " for="view_type"><b>Report Type</b></label>
			<div class="inputWrapper">
				<select id="view_type" name="view_type" title="View Type" aria-required="true">
				<?php
				foreach (array("Federal product approvals", "State product approvals") as $viewType) {
					echo "<option value='$viewType' >$viewType</option>";
				}
				?>					
				</select>
			</div>
		</div>
		
		<div class="col-md-auto align-self-center oneField field-container-D" id="add-new-D">
    		<button id="btn-add-new" class="btn btn-success"><i class="fa fa-search"></i> <span id='adv_searching_txt'>Show Advanced Search</span></button>
		</div>

		<div class="col-md-auto align-self-center oneField field-container-D" id="asanaform_btn">
		        <a href="https://form.asana.com/?k=Cl5-bJLuzWI1rmsLC-Pu7A&d=632673473024762" target="_blank" class="btn btn-info" role="button" aria-pressed="true"><i class="fas fa-envelope-open-text"></i><span>&nbsp; Feedback/Support</span></a>
				
    </div>

<div class="row"><div class="col-sm-12">&nbsp;</div></div>
	  
<div class="wFormContainer" style="max-width: 100%; width:auto;" >
    <div class="row" id="searching_div">
        <div class="col-sm-12">		
			  <fieldset id="tfa_399" class="section">
				<legend>Advanced Search</legend>
				
				<form id="searching_data">
				  <div class="form-row">

					<!--<div class="form-group col-md-3">
					  <label for="s_product_id">Product ID</label>
					  <input type="text" class="form-control" id="s_product_id" name="p.product_id" placeholder="Product ID">
					</div>-->
					<div class="form-group col-md-2">
					  <label for="s_brand_name">Brand Name</label>
					  <input type="text" class="form-control" id="s_brand_name" name="p.brand_name" placeholder="Brand Name">
					</div>
					<div class="form-group col-md-1">
					  <label for="s_product_mhw_code">Product Code</label>
					  <input type="text" class="form-control" id="s_product_mhw_code" name="p.product_mhw_code" placeholder="Product Code">
					</div>
					<div class="form-group col-md-2">
					  <label for="s_product_desc">Product Description</label>
					  <input type="text" class="form-control" id="s_product_desc" name="p.product_desc" placeholder="Product Description">
					</div>
					
				  <!--</div>
				  <div class="form-row">-->

					<div class="form-group col-md-1">
					  <label for="s_ttb_id">TTB ID</label>
					  <input type="text" class="form-control" id="s_ttb_id" name="p.ttb_id" placeholder="TTB ID">
					</div>
					<div class="form-group col-md-1">
					  <label for="p_federal_type">Federal Type</label>
					  <input type="text" class="form-control" id="p_federal_type" name="p.federal_type" placeholder="Federal Type">
					</div>
					<div class="form-group col-md-1">
					  <label for="s_country">Country of Origin</label>
					  <input type="text" class="form-control" id="s_country" name="p.country" placeholder="Country of Origin">
					</div>
					<div class="form-group col-md-1">
					  <label for="s_product_class">Product Class</label>
					  <input type="text" class="form-control" id="s_product_class" name="p.product_class" placeholder="Product Class">
					</div>
					<div class="form-group col-md-1">
					  <label for="p_mhw_team">MHW Team</label>
					  <input type="text" class="form-control" id="p_mhw_team" name="p.mhw_team" placeholder="MHW Team">
					</div>
					<div class="form-group col-md-1">
					  <label for="p_compid">CompID</label>
					  <input type="text" class="form-control" id="p_compid" name="p.compid" placeholder="CompID">
					</div>

				  </div>
				  
				  <div id="federal_form_data">
				  
					  <div class="col-sm-12">
						<fieldset id="tfa_399f" class="section">
						  <legend>Federal Filters</legend>
						  <div class="form-row">

							<!--<div class="form-group col-md-2">
							  <label for="p_status">Status</label>
							  <input type="text" class="form-control" id="p_status" name="f.status" placeholder="Status">
							</div>-->
							<div class="form-group col-md-1">
							  <label for="s_dateapproved">Date of Application</label>
							  <input type="text" class="form-control" id="s_dateofapplication" name="f.dateofapplication" placeholder="Date of Application">
							</div>
							<div class="form-group col-md-1">
							  <label for="s_dateapproved">Date Approved</label>
							  <input type="text" class="form-control" id="s_dateapproved" name="f.dateapproved" placeholder="Date Approved">
							</div>
							<div class="form-group col-md-2">
							  <label for="s_primaryfederalbasicpermitnumber">TTB Basic Importer #</label>
							  <input type="text" class="form-control" id="s_primaryfederalbasicpermitnumber" name="f.primaryfederalbasicpermitnumber" placeholder="TTB Basic Importer">
							</div>
							<div class="form-group col-md-2">
							  <label for="s_legalnameusedonlabel">TTB Basic Importer Name</label>
							  <input type="text" class="form-control" id="s_legalnameusedonlabel" name="f.legalnameusedonlabel" placeholder="TTB Basic Importer Name">
							</div>
							<div class="form-group col-md-1">
							  <label for="s_applicantname">MHW user that filed</label>
							  <input type="text" class="form-control" id="s_applicantname" name="f.applicantname" placeholder="Preparer">
							</div>
					  
						</div>
						</fieldset>
					  </div>
					  </div>
					  
					  
					  
					 <div id="state_form_data">
					   <div class="col-sm-12">
						<fieldset id="tfa_399s" class="section">
						  <legend>State Filters</legend>
						  <div class="form-row">

							<!--<div class="form-group col-md-2">
							  <label for="s_status">Status</label>
							  <input type="text" class="form-control" id="s_status" name="s.status" placeholder="Status">
							</div>-->
							<div class="form-group col-md-1">
							  <label for="s_ttb_id">State</label>
							  <input type="text" class="form-control" id="s_State" name="s.State" placeholder="State Name">
							</div>
							<div class="form-group col-md-1">
							  <label for="s_effectiveDate">Submitted Date</label>
							  <input type="text" class="form-control" id="s_DateSubmitted" name="s.DateSubmitted" placeholder="Submitted Date">
							</div>
							<div class="form-group col-md-1">
							  <label for="s_effectiveDate">Effective Date</label>
							  <input type="text" class="form-control" id="s_effectiveDate" name="s.EffectiveDate" placeholder="Effective Date">
							</div>
							<div class="form-group col-md-1">
							  <label for="s_effectiveDate">Expiration Date</label>
							  <input type="text" class="form-control" id="s_ExpirationDate" name="s.ExpirationDate" placeholder="Expiration Date">
							</div>
							<div class="form-group col-md-2">
							  <label for="s_StateApprovalNumber">Brand label registration (BLR) #</label>
							  <input type="text" class="form-control" id="s_StateApprovalNumber" name="s.StateApprovalNumber" placeholder="BLR Number">
							</div>
							<div class="form-group col-md-1">
							  <label for="s_Preparer">MHW user that filed</label>
							  <input type="text" class="form-control" id="s_Preparer" name="s.Preparer" placeholder="Preparer">
							</div>

						  </div>
					  </fieldset>
					  </div>
				 
				  </div>
					
				 <?php 
				 if($_SESSION['mhwltdphp_usertype'] == "SUPERUSER" || $_SESSION['mhwltdphp_usertype'] == "ADMIN" )
				 { 
				 ?> 
				  <div class="form-check">
					<input type="checkbox" name="all_clients" value="all_clients" class="form-check-input" id="all_clients">
					<label class="form-check-label" for="all_clients">Include All Clients</label>
				  </div>
				  <br>
				  <?php 
				 }
				 ?>  
				  <button type="button" id="s_sub_btn" class="btn btn-primary">Search &nbsp;&nbsp;&nbsp;<i class="fa fa-search"></i></button>
				</form>
				
				
			  </fieldset>		
		</div>
	</div>
</div>	
	


    <table id="blrViewTable" class="table table-hover" data-pagination="false" data-show-pagination-switch="true" data-id-field="ID" data-page-list="[10, 25, 50, all]"  data-show-columns="true" data-search="true" data-show-export="true"  data-editable-emptytext="...." data-editable-url="#">
        <thead>
        </thead>
        <tbody>
        </tbody>
    </table>

    <script>

        //table button
		/*
		//function to initialize all of the in-modal functionality
		function modalStuff(){
			//from image details to image full preview
			$('.prodimg_thumb').bind('click',function(){
				var imageid = $(this).data('imageid');
				var productid = $(this).data('product');
				var imageurl = 'viewfiles.php?iid='+imageid+'&b='+productid;
				$('.modal-body').load(imageurl,function(){
					$('#fileModalx').modal({show:true});
					modalStuff(); //recursive but necessary to initialize other states on each change
				});
			});
			//back link in modal from image full to image details
			$('.prodimg_back').bind('click',function(){
				var productid = $(this).data('product');
				var producturl = 'viewfiles.php?pid='+productid;
				$('.modal-body').load(producturl,function(){
					$('#fileModalx').modal({show:true});
					modalStuff(); //recursive but necessary to initialize other states on each change
				});
			});
			//initialize prod image modal functionality again
			$('.prodimg_btn').bind('click',function(){
				var productid = $(this).data('product');
				var imageurl = 'viewfiles.php?pid='+productid;
				$('.modal-body').load(imageurl,function(){
					$('#fileModalx').modal({show:true});
					modalStuff(); //recursive but necessary to initialize other states on each change
				});
			});
			//download image button
			$('.dwnld').bind('click', function () {
				var imagefile = $(this).data('url');
				var imagename = $(this).data('imagename');
				$.ajax({
					url: imagefile,
					method: 'GET',
					xhrFields: {
						responseType: 'blob'
					},
					success: function (data) {
						var a = document.createElement('a');
						var url = window.URL.createObjectURL(data);
						a.href = url;
						a.download = imagename;
						a.click();
						window.URL.revokeObjectURL(url);
					}
				});
			});
			//go to upload page for product
			$('.prod_upload').bind('click', function () {
				var prod = $(this).data('prod');
				window.location = 'imageupload.php?pid='+prod;
			});
			$(".prodedit_btn").bind("click", function() {
				var prod = $(this).data("target");
				$("#editprod_"+prod).submit();
			});
			
		};
		*/
		window.operateEvents = {
			'click .like': function (e, value, row, index) {
			  //alert('You click like action, row: ' + JSON.stringify(row))
			},
			'click .prodimg_btn': function (e, value, row, index) {
			 //alert("jahid");	
			  //alert('You click prodimg_btn action, row: ' + JSON.stringify(row))
			},
			}

		
		function operateFormatter(value, row, index) {
			return [
				'<button type="button" class="btn btn-outline-secondary prodimg_btn btn-sm bg_arrow_lightblue" data-product="'+row.product_id+'">',
				'Files  <span class="badge badge-light">'+row.filecount+'</span></button>',
			].join('')
		}
		
		
		
		window.operateEventsTTBID = {
			'click .view_cola_details': function (e, value, row, index) {
				var ttbUrl = '<?php echo $ttblink; ?>';  //defined in settings.php
				var ttbVal = row.prefix_ttb_id;
				var ttbInt = parseInt(ttbVal);  //TTB value must be integer to be valid, but may begin with leading zeros so use text value for url
				if(ttbVal!="" && ttbVal!="P" && ttbVal!="p" && ttbInt){
				window.open(ttbUrl+ttbVal);
				}
			},
			}

		
		function operateFormatterTTBID(value, row, index) {	
		
		        var ttbVal = row.prefix_ttb_id;
				var ttbInt = parseInt(ttbVal);  //TTB value must be integer to be valid, but may begin with leading zeros so use text value for url
				if(ttbVal!="" && ttbVal!="P" && ttbVal!="p" && ttbInt){
				return [
						'<button type="button" class="btn btn-dark btn-sm bg_arrow_darkblue view_cola_details" data-target="'+row.prefix_ttb_id+'">',
					'<i class="fas fa-file-contract"></i> TTB Site <i class="fas fa-external-link-square-alt"></i></button>',
					].join('');
				}
				else 
				{
					return [
						'<button type="button" class="btn btn-dark btn-sm bg_arrow_darkblue view_cola_details disabled" data-target="'+row.prefix_ttb_id+'">',
					'<i class="fas fa-file-contract"></i> TTB Site <i class="fas fa-external-link-square-alt"></i></button>',
					].join('');
				}
			
		}
				
		function UnFinalizedFormatter(value, row, index) {
		  console.log("jahid:"+value);
		  if(value!='' && value>0)
		  {
			  return ['<label><input data-index="0" name="btSelectItem" type="checkbox" value="'+row.product_id+'"><span></span></label>'].join('')
		  }
		  
		}
		
		
		function getFormData($form){
			var unindexed_array = $form.serializeArray();
			var indexed_array = {};

			$.map(unindexed_array, function(n, i){
				indexed_array[n['name']] = n['value'];
			});

			return indexed_array;
		}


		
		
		
		
        
        var columns = [];
        columns['Federal product approvals'] = [
            {
                field: 'product_mhw_code',
                title: 'Product Code',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'brand_name',
                title: 'Brand name',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'product_desc',
                title: 'Product Description',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'federal_type',
                title: 'Federal Type',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'product_class',
                title: 'Product Class',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'TTB_ID',
                title: 'TTB ID',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'status',
                title: 'Status',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
			{
                field: 'dateofapplication',
                title: 'Date of Application',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
			{
                field: 'est_dt_approval',
                title: 'Estimated Date of Approval',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'dateapproved',
                title: 'Date Approved',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'primaryfederalbasicpermitnumber',
                title: 'TTB Basic Importer #',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'country',
                title: 'Country of Origin',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'legalnameusedonlabel',
                title: 'TTB Basic Importer Name',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'applicantname',
                title: 'MHW user that filed',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
			{
                field: 'MHWTeam',
                title: 'MHW Team',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
			{
                field: 'CompID',
                title: 'CompID',
                align: 'center',
                valign: 'middle',
                sortable: true
            }
				/*,
            {
                field: 'user_notes',
                title: 'MHW user notes',
                align: 'center',
                valign: 'middle',
                sortable: true
            }
            */
        ];
        
        
        columns['State product approvals'] = [
            {
                field: 'product_mhw_code',
                title: 'Product ID',
                align: 'left',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'brand_name',
                title: 'Brand name',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'product_desc',
                title: 'Product Description',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'federal_type',
                title: 'Federal Type',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'State',
                title: 'State',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'Status',
                title: 'Status',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
			{
                field: 'DateSubmitted',
                title: 'Submitted Date',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
			{
                field: 'EffectiveDate',
                title: 'Effective Date',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'ExpirationDate',
                title: 'Expiration Date',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'est_dt_approval',
                title: 'Estimated Date of Approval',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'StateApprovalNumber',
                title: 'Brand label registration (BLR) #',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
            {
                field: 'Preparer',
                title: 'MHW user that filed',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
			{
                field: 'MHWTeam',
                title: 'MHW Team',
                align: 'center',
                valign: 'middle',
                sortable: true
            },
			{
                field: 'CompID',
                title: 'CompID',
                align: 'center',
                valign: 'middle',
                sortable: true
            }
				/*,
            {
                field: 'user_notes',
                title: 'MHW user notes',
                align: 'center',
                valign: 'middle',
                sortable: true
            }
			*/
        ];
		
		
		
		var subColumns = [];
		

        $(document).ready(()=>{

            var getHeight = () => {
                var winH = $(window).height();
                var navH = $(".navbar").height();
                return winH - navH - 100;
            }
			
			//Bootstrap table 
            var $table = $('#blrViewTable');
			var $remove = $('#remove')
			var selections = []
			
			function getIdSelections() {
			return $.map($table.bootstrapTable('getSelections'), function (row) {
				return row.product_id;
			})
		}

			
            var updateTable = () => {
                var qry_type = "blr-status";
                var view_type = $('#view_type').val();
                var client = $('#client_name').val();
				
				
				/* change form element as per type : start */
				
				if(view_type=="Federal product approvals")
				{					
					$("#federal_form_data").show();
					$("#state_form_data").hide();
				}
				else 
				{
					$("#state_form_data").show();
					$("#federal_form_data").hide();
				}
				
				/* change form element as per type : end */
				
				/* get the checked Item List : start */
				var prodids = getIdSelections();
				var prodlistids = JSON.stringify(prodids);
				/* get the checked Item List : End */
				
				/* get advance searching data : start */
				var b_text=$("span#adv_searching_txt").text();
					if(b_text=="Hide Advanced Search")
					{
						var $form_id = $("#searching_data");
						var form_data = getFormData($form_id);
						console.log("form data :::"+JSON.stringify(form_data));
						var searching_data=JSON.stringify(form_data);
						
					}
					else 
					{
					var searching_data="";	
					}
				/* get advance searching data : end */
                
                var loadingMessage = '<span class="mhwLoading"><img src="css/img/Logo_Chevron_Animated.gif"/> Loading</span>';
				var errorMessage = '<span class="mhwError"><img src="css/img/Logo_Chevron.png" height="64" /> Error Loading</span>';

                $('#blrViewTable').bootstrapTable('showLoading');
                console.log(view_type, columns[view_type]);
                $.post('query-report.php', {
                    qry_type: qry_type,
                    view_type: view_type,
                    client: client,
					prodlistids:prodlistids,
					searching_data:searching_data,
                }, (dataPF) => { 
                    //console.log(dataPF);
                    var data = [];
					$('#blrViewTable').data('id-field', 'product_id');
                    try {
                        data = JSON.parse(dataPF);
                        $('#blrViewTable').bootstrapTable('destroy').bootstrapTable('showLoading').bootstrapTable({
                            data: data,
                            height: getHeight(),
                            stickyHeader:true,
                            stickyHeaderOffsetY:60,
                            formatLoadingMessage: () => loadingMessage,
							exportDataType: 'all',
                            exportTypes: ['csv','excel'],
							exportOptions: {
								fileName:view_type
							 },
                            detailView: false,
                            columns: columns[view_type],

                            //fixedColumns:true,
                            //fixedNumber:7,
							// on expand row
                        }); //show main table
                        
                    } catch (error) {
                        //location.reload();
                        console.log(error, dataPF);
						$('.mhwLoading').html(errorMessage);
                    }
					
					//modalStuff(); //bind modal & buttons
				
                }).fail(function(jqXHR, textStatus, errorThrown){
					$('.mhwLoading').html(errorMessage);
					if(errorThrown=='Internal Server Error'){
						alert("Error Loading.  If error persists, please try filtering data with Advanced Search");
					}
				});
            }; // updateTable
            
			$table.on('all.bs.table', function (e, name, args) {
			  console.log(name, args);
			  //modalStuff(); //when anything happens (filter, click, etc) re-bind modal & buttons
			});

			$table.on('load-error.bs.table', function (e, status) {
				$('.mhwLoading').html(errorMessage);
			});

			function updateViewType() {
				var optionFED = $('<option></option>').attr("value", "Federal product approvals").text("Federal product approvals");
				var optionSTATE = $('<option></option>').attr("value", "State product approvals").text("State product approvals");
				var clientclass = $('select[name="client_name"] :selected').attr('class');
				//alert(clientclass);
				if(clientclass=='BLR_PROD') { 
					//alert('fed');
					$("#view_type").empty().append(optionFED);
				}
				else{
					//alert('state');
					$("#view_type").empty().append(optionFED).append(optionSTATE);
				}
				
			}; //20211029 DS
			updateViewType(); //20211029 DS

            $('#view_type').on('change',updateTable);

			$('#client_name').on('change',updateViewType); //20211029 DS
            $('#client_name').on('change',updateTable);
			
			$("#s_sub_btn").on("click", updateTable);

            updateTable();
           
        });

    </script>
	
	<div class="modal modal-wide fade" id="fileModalx" tabindex="-1" role="dialog" aria-labelledby="fileModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="fileModalLabel">Files</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      </div>
	  <div id='preview'></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script>

$(document).ready(function() {
	/*
	//initialize prod image modal functionality
	$('.prodimg_btn').on('click',function(){
		var productid = $(this).data('product');
		var imageurl = 'viewfiles.php?pid='+productid;
		//alert(imageurl);
	    $('.modal-body').load(imageurl,function(){
	        $('#fileModalx').modal({show:true});
	    });
	});
	//initialize prod image download functionality for outside of modal
	$('.dwnld').on('click', function () {
		var imagefile = $(this).data('url');
		var imagename = $(this).data('imagename');
		$.ajax({
			url: imagefile,
			method: 'GET',
			xhrFields: {
				responseType: 'blob'
			},
			success: function (data) {
				var a = document.createElement('a');
				var url = window.URL.createObjectURL(data);
				a.href = url;
				a.download = imagename;
				a.click();
				window.URL.revokeObjectURL(url);
			}
		});
	});
*/
	
	//make sure modal initializers are called when modal is shown
	//$('#fileModalx').on('shown.bs.modal', function (e) {
	//	modalStuff();
	//});
	
});

</script>



<script>
        $("span#adv_searching_txt").click(function () {
            $(this).text(function(i, v){
			   if(v === 'Show Advanced Search')
			   {
				 $("#searching_div").show();				 				   
			   }
			   else 
			   {
				 $("#searching_div").hide();  
			   }
			   return v === 'Show Advanced Search' ? 'Hide Advanced Search' : 'Show Advanced Search';
            })
        });
</script>


</div>


<?php
    include('footer.php');
?>
    