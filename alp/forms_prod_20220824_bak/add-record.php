<?php

error_reporting(E_ALL); //displays an error

session_start();
if(!isset($_SESSION['mhwltdphp_user'])){
	die( "Not authenticated !" );  
	//header("Location: login.php");
}

//print_r($_POST);

if(empty($_POST['itemID']))
{
	die("Error: the Item ID is not set");
}

if (isset($_POST['effective_month'])) {
	die("Error: Effective month is not set");
}

$itemID	= intval($_POST['itemID']);

$effective_month = str_replace("($300 fee) ","",addslashes($_POST['tfa_effective_month']));

include("dbconnect.php");

//Establishes the connection
$conn = sqlsrv_connect($serverName, $connectionOptions);
if( $conn === false) {
		die( print_r( sqlsrv_errors(), true));
}

if (in_array($_REQUEST['view_state'], ['tfa_ca','tfa_ct','tfa_ma','tfa_mn','tfa_nj','tfa_nyr','tfa_nyw'])) {
	$tableName = $_REQUEST['view_state'];
	$tsql = "
		SELECT TOP 1 ID FROM [dbo].[$tableName] WHERE itemID = $itemID AND tfa_effective_month = '".$effective_month."'
	";
} else {
	$tableName = "tfa_os";
	$state = str_replace("'","\'",$_REQUEST['view_state']);
	$tsql = "
		SELECT TOP 1 ID FROM [dbo].[$tableName] WHERE itemID = $itemID AND tfa_os_price_state = '".$state."' AND tfa_effective_month = '".$effective_month."'
	";
}

$stmt = sqlsrv_query( $conn, $tsql);
if ( $stmt === false )  
{  
	die( print_r( sqlsrv_errors(), true));  
}  

$status = "draft";
$change_type = "Add";
if ( sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC) ) {
	$change_type = "Update";
}
	 
if($_POST['tfa_ca_state']=='Yes') {

	$tsql = "
		INSERT INTO tfa_ca (
			itemID,
			tfa_ca_change_type,
			tfa_ca_price_bottle, 
			tfa_ca_price_case,
			tfa_ca_fob,
			tfa_ca_brand,
			tfa_ca_package,
			tfa_ca_contents,
			tfa_ca_cont_charge,
			tfa_ca_price_retail,
			tfa_ca_price_wholesale,
			tfa_ca_fob_del,
			tfa_ca_foe,
			tfa_ca_counties,
			tfa_ca_dist,
			tfa_effective_month,
			[status],
			[user]
		)
		VALUES (
			?,?,?,
			?,?,?,
			?,?,?,?,?,
			?,?,?,?,?,?,?
		);
	";

	$params = array ( 
		$itemID,
		$change_type,
		floatval($_POST['tfa_ca_price_bottle']),
		floatval($_POST['tfa_ca_price_case']),
		$_POST['tfa_ca_fob'],
		$_POST['tfa_ca_brand'],
		$_POST['tfa_ca_package'],
		$_POST['tfa_ca_contents'],
		$_POST['tfa_ca_cont_charge'],
		floatval($_POST['tfa_ca_price_retail']),
		floatval($_POST['tfa_ca_price_wholesale']),
		$_POST['tfa_ca_fob_del'],
		$_POST['tfa_ca_foe'],
		$_POST['tfa_ca_counties'],
		is_array($_POST['tfa_ca_dist']) ? implode($_POST['tfa_ca_dist'],",") : "",
		$effective_month,
		$status,
		$_SESSION['mhwltdphp_user']
	);

	$stmt = sqlsrv_prepare( $conn, $tsql, $params);  

	if( $stmt === false )  
	{  
		 echo "Statement could not be prepared.\n";  
		 die( print_r( sqlsrv_errors(), true));  
	}  

	if( sqlsrv_execute($stmt) === false )  
	{  
	  echo "Statement could not be executed.\n";  
	  die( print_r( sqlsrv_errors(), true));  
	}  

}





if($_POST['tfa_ct_state']=='Yes') {

	$tsql = "
		INSERT INTO tfa_ct (
			itemID,
			tfa_ct_change_type,
			tfa_ct_price_bot, 
			tfa_ct_price_case,
			tfa_ct_fob,
			tfa_effective_month,
			[status],
			[user]
		)
		VALUES (
			?,?,?,?,?,?,?,?
		);
	";

	$stmt = sqlsrv_prepare( $conn, $tsql, array ( 
		$itemID,
		$change_type,
		floatval($_POST['tfa_ct_price_bot']),
		floatval($_POST['tfa_ct_price_case']),
		$_POST['tfa_ct_fob'],
		$effective_month,
		$status,
		$_SESSION['mhwltdphp_user']
	));  

/*
	echo "[";
	print_r($stmt);
	echo "]";
	echo "[";
	print_r($conn);
	echo "]";
	echo "[";
	print_r($params);
	echo "]";
*/	

	if( $stmt === false )  
	{  
		 echo "Statement could not be prepared.\n";  
		 die( print_r( sqlsrv_errors(), true));  
	}  
		
	if( sqlsrv_execute($stmt) === false )  
	{  
	  echo "Statement could not be executed.\n";  
	  die( print_r( sqlsrv_errors(), true));  
	}  

}





if($_POST['tfa_ma_state']=='Yes') {

	$tsql = "
		INSERT INTO tfa_ma (
			itemID,
			tfa_ma_change_type,
			tfa_ma_price_bot, 
			tfa_ma_price_case,
			tfa_ma_fob,
			tfa_ma_dist,
			tfa_ma_disc,
			
			tfa_ma_qty1_quan,
			tfa_ma_qty1_on,
			tfa_ma_qty1_type,
			tfa_ma_qty1_amt,

			tfa_ma_qty2_quan,
			tfa_ma_qty2_on,
			tfa_ma_qty2_type,
			tfa_ma_qty2_amt,

			tfa_ma_qty3_quan,
			tfa_ma_qty3_on,
			tfa_ma_qty3_type,
			tfa_ma_qty3_amt,
			
			tfa_ma_qty4_quan,
			tfa_ma_qty4_on,
			tfa_ma_qty4_type,
			tfa_ma_qty4_amt,

			tfa_ma_qty5_quan,
			tfa_ma_qty5_on,
			tfa_ma_qty5_type,
			tfa_ma_qty5_amt,

			tfa_ma_qty6_quan,
			tfa_ma_qty6_on,
			tfa_ma_qty6_type,
			tfa_ma_qty6_amt,

			tfa_ma_qty7_quan,
			tfa_ma_qty7_on,
			tfa_ma_qty7_type,
			tfa_ma_qty7_amt,

			tfa_ma_qty8_quan,
			tfa_ma_qty8_on,
			tfa_ma_qty8_type,
			tfa_ma_qty8_amt,

			tfa_ma_qty9_quan,
			tfa_ma_qty9_on,
			tfa_ma_qty9_type,
			tfa_ma_qty9_amt,

			tfa_ma_qty10_quan,
			tfa_ma_qty10_on,
			tfa_ma_qty10_type,
			tfa_ma_qty10_amt,
			tfa_effective_month,
			[status],
			[user]
		)
		VALUES (
			?,?,?,?, ?,?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,?,?,?
		);
	";


	$values = array ( 
		$itemID,
		$change_type,
		floatval($_POST['tfa_ma_price_bot']),
		floatval($_POST['tfa_ma_price_case']),
		$_POST['tfa_ma_fob'],
		is_array($_POST['tfa_ma_dist']) ? implode($_POST['tfa_ma_dist'],",") : "",
		$_POST['tfa_ma_disc'],
		
		floatval($_POST['tfa_ma_qty1_quan']),
		trim($_POST['tfa_ma_qty1_on']),
		trim($_POST['tfa_ma_qty1_type']),
		floatval($_POST['tfa_ma_qty1_amt']),
		
		floatval($_POST['tfa_ma_qty2_quan']),
		trim($_POST['tfa_ma_qty2_on']),
		trim($_POST['tfa_ma_qty2_type']),
		floatval($_POST['tfa_ma_qty2_amt']),

		floatval($_POST['tfa_ma_qty3_quan']),
		trim($_POST['tfa_ma_qty3_on']),
		trim($_POST['tfa_ma_qty3_type']),
		floatval($_POST['tfa_ma_qty3_amt']),

		floatval($_POST['tfa_ma_qty4_quan']),
		trim($_POST['tfa_ma_qty4_on']),
		trim($_POST['tfa_ma_qty4_type']),
		floatval($_POST['tfa_ma_qty4_amt']),

		floatval($_POST['tfa_ma_qty5_quan']),
		trim($_POST['tfa_ma_qty5_on']),
		trim($_POST['tfa_ma_qty5_type']),
		floatval($_POST['tfa_ma_qty5_amt']),

		floatval($_POST['tfa_ma_qty6_quan']),
		trim($_POST['tfa_ma_qty6_on']),
		trim($_POST['tfa_ma_qty6_type']),
		floatval($_POST['tfa_ma_qty6_amt']),

		floatval($_POST['tfa_ma_qty7_quan']),
		trim($_POST['tfa_ma_qty7_on']),
		trim($_POST['tfa_ma_qty7_type']),
		floatval($_POST['tfa_ma_qty7_amt']),

		floatval($_POST['tfa_ma_qty8_quan']),
		trim($_POST['tfa_ma_qty8_on']),
		trim($_POST['tfa_ma_qty8_type']),
		floatval($_POST['tfa_ma_qty8_amt']),

		floatval($_POST['tfa_ma_qty9_quan']),
		trim($_POST['tfa_ma_qty9_on']),
		trim($_POST['tfa_ma_qty9_type']),
		floatval($_POST['tfa_ma_qty9_amt']),

		floatval($_POST['tfa_ma_qty10_quan']),
		trim($_POST['tfa_ma_qty10_on']),
		trim($_POST['tfa_ma_qty10_type']),
		floatval($_POST['tfa_ma_qty10_amt']),
		$effective_month,
		$status,
		$_SESSION['mhwltdphp_user']
	);

	$stmt = sqlsrv_prepare( $conn, $tsql, $values);  

	if( $stmt === false )  
	{  
		 echo "Statement could not be prepared.\n";  
		 die( print_r( sqlsrv_errors(), true));  
	}  
		
	if( sqlsrv_execute($stmt) === false )  
	{  
	  echo "Statement could not be executed.\n";  
	  die( print_r( sqlsrv_errors(), true));  
	}  

}


if($_POST['tfa_mn_state']=='Yes') {

	$tsql = "
		INSERT INTO tfa_mn (
			itemID,
			tfa_mn_change_type,
			tfa_mn_price_bot, 
			tfa_mn_price_case,
			tfa_mn_fob,
			
			tfa_mn_qty1_quan,
			tfa_mn_qty1_on,
			tfa_mn_qty1_type,
			tfa_mn_qty1_amt,

			tfa_mn_qty2_quan,
			tfa_mn_qty2_on,
			tfa_mn_qty2_type,
			tfa_mn_qty2_amt,

			tfa_mn_qty3_quan,
			tfa_mn_qty3_on,
			tfa_mn_qty3_type,
			tfa_mn_qty3_amt,
			
			tfa_mn_qty4_quan,
			tfa_mn_qty4_on,
			tfa_mn_qty4_type,
			tfa_mn_qty4_amt,

			tfa_mn_qty5_quan,
			tfa_mn_qty5_on,
			tfa_mn_qty5_type,
			tfa_mn_qty5_amt,

			tfa_mn_qty6_quan,
			tfa_mn_qty6_on,
			tfa_mn_qty6_type,
			tfa_mn_qty6_amt,

			tfa_mn_qty7_quan,
			tfa_mn_qty7_on,
			tfa_mn_qty7_type,
			tfa_mn_qty7_amt,

			tfa_mn_qty8_quan,
			tfa_mn_qty8_on,
			tfa_mn_qty8_type,
			tfa_mn_qty8_amt,

			tfa_mn_qty9_quan,
			tfa_mn_qty9_on,
			tfa_mn_qty9_type,
			tfa_mn_qty9_amt,

			tfa_mn_qty10_quan,
			tfa_mn_qty10_on,
			tfa_mn_qty10_type,
			tfa_mn_qty10_amt,
			tfa_effective_month,
			[status],
			[user]
		)
		VALUES (
			?,?,?,?, ?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,?,?
		);
	";

	$values = array ( 
		$itemID,
		$change_type,
		floatval($_POST['tfa_mn_price_bot']),
		floatval($_POST['tfa_mn_price_case']),
		$_POST['tfa_mn_fob'],
		
		floatval($_POST['tfa_mn_qty1_quan']),
		trim($_POST['tfa_mn_qty1_on']),
		trim($_POST['tfa_mn_qty1_type']),
		floatval($_POST['tfa_mn_qty1_amt']),
		
		floatval($_POST['tfa_mn_qty2_quan']),
		trim($_POST['tfa_mn_qty2_on']),
		trim($_POST['tfa_mn_qty2_type']),
		floatval($_POST['tfa_mn_qty2_amt']),

		floatval($_POST['tfa_mn_qty3_quan']),
		trim($_POST['tfa_mn_qty3_on']),
		trim($_POST['tfa_mn_qty3_type']),
		floatval($_POST['tfa_mn_qty3_amt']),

		floatval($_POST['tfa_mn_qty4_quan']),
		trim($_POST['tfa_mn_qty4_on']),
		trim($_POST['tfa_mn_qty4_type']),
		floatval($_POST['tfa_mn_qty4_amt']),

		floatval($_POST['tfa_mn_qty5_quan']),
		trim($_POST['tfa_mn_qty5_on']),
		trim($_POST['tfa_mn_qty5_type']),
		floatval($_POST['tfa_mn_qty5_amt']),

		floatval($_POST['tfa_mn_qty6_quan']),
		trim($_POST['tfa_mn_qty6_on']),
		trim($_POST['tfa_mn_qty6_type']),
		floatval($_POST['tfa_mn_qty6_amt']),

		floatval($_POST['tfa_mn_qty7_quan']),
		trim($_POST['tfa_mn_qty7_on']),
		trim($_POST['tfa_mn_qty7_type']),
		floatval($_POST['tfa_mn_qty7_amt']),

		floatval($_POST['tfa_mn_qty8_quan']),
		trim($_POST['tfa_mn_qty8_on']),
		trim($_POST['tfa_mn_qty8_type']),
		floatval($_POST['tfa_mn_qty8_amt']),

		floatval($_POST['tfa_mn_qty9_quan']),
		trim($_POST['tfa_mn_qty9_on']),
		trim($_POST['tfa_mn_qty9_type']),
		floatval($_POST['tfa_mn_qty9_amt']),

		floatval($_POST['tfa_mn_qty10_quan']),
		trim($_POST['tfa_mn_qty10_on']),
		trim($_POST['tfa_mn_qty10_type']),
		floatval($_POST['tfa_mn_qty10_amt']),

		$effective_month,
		$status,
		$_SESSION['mhwltdphp_user']
	);

	$stmt = sqlsrv_prepare( $conn, $tsql, $values);  

	if( $stmt === false )  
	{  
		 echo "Statement could not be prepared.\n";  
		 die( print_r( sqlsrv_errors(), true));  
	}  
		
	if( sqlsrv_execute($stmt) === false )  
	{  
	  echo "Statement could not be executed.\n";  
	  die( print_r( sqlsrv_errors(), true));  
	}  

}


if($_POST['tfa_nj_state']=='Yes') {

	$tsql = "
		INSERT INTO tfa_nj (
			itemID,
			tfa_nj_change_type,
			tfa_nj_price_bot, 
			tfa_nj_price_case,
			tfa_nj_fob,
			
			tfa_nj_qty1_quan,
			tfa_nj_qty1_on,
			tfa_nj_qty1_type,
			tfa_nj_qty1_amt,

			tfa_nj_qty2_quan,
			tfa_nj_qty2_on,
			tfa_nj_qty2_type,
			tfa_nj_qty2_amt,

			tfa_nj_qty3_quan,
			tfa_nj_qty3_on,
			tfa_nj_qty3_type,
			tfa_nj_qty3_amt,
			
			tfa_nj_qty4_quan,
			tfa_nj_qty4_on,
			tfa_nj_qty4_type,
			tfa_nj_qty4_amt,

			tfa_nj_qty5_quan,
			tfa_nj_qty5_on,
			tfa_nj_qty5_type,
			tfa_nj_qty5_amt,

			tfa_nj_qty6_quan,
			tfa_nj_qty6_on,
			tfa_nj_qty6_type,
			tfa_nj_qty6_amt,

			tfa_nj_qty7_quan,
			tfa_nj_qty7_on,
			tfa_nj_qty7_type,
			tfa_nj_qty7_amt,

			tfa_nj_qty8_quan,
			tfa_nj_qty8_on,
			tfa_nj_qty8_type,
			tfa_nj_qty8_amt,

			tfa_nj_qty9_quan,
			tfa_nj_qty9_on,
			tfa_nj_qty9_type,
			tfa_nj_qty9_amt,

			tfa_nj_qty10_quan,
			tfa_nj_qty10_on,
			tfa_nj_qty10_type,
			tfa_nj_qty10_amt,
			tfa_effective_month,
			[status],
			[user]
		)
		VALUES (
			?,?,?,?, ?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,?,?,?
		);
	";

	$stmt = sqlsrv_prepare( $conn, $tsql, array ( 
		$itemID,
		$change_type,
		floatval($_POST['tfa_nj_price_bot']),
		floatval($_POST['tfa_nj_price_case']),
		$_POST['tfa_nj_fob'],
		
		floatval($_POST['tfa_nj_qty1_quan']),
		trim($_POST['tfa_nj_qty1_on']),
		trim($_POST['tfa_nj_qty1_type']),
		floatval($_POST['tfa_nj_qty1_amt']),
		
		floatval($_POST['tfa_nj_qty2_quan']),
		trim($_POST['tfa_nj_qty2_on']),
		trim($_POST['tfa_nj_qty2_type']),
		floatval($_POST['tfa_nj_qty2_amt']),

		floatval($_POST['tfa_nj_qty3_quan']),
		trim($_POST['tfa_nj_qty3_on']),
		trim($_POST['tfa_nj_qty3_type']),
		floatval($_POST['tfa_nj_qty3_amt']),

		floatval($_POST['tfa_nj_qty4_quan']),
		trim($_POST['tfa_nj_qty4_on']),
		trim($_POST['tfa_nj_qty4_type']),
		floatval($_POST['tfa_nj_qty4_amt']),

		floatval($_POST['tfa_nj_qty5_quan']),
		trim($_POST['tfa_nj_qty5_on']),
		trim($_POST['tfa_nj_qty5_type']),
		floatval($_POST['tfa_nj_qty5_amt']),

		floatval($_POST['tfa_nj_qty6_quan']),
		trim($_POST['tfa_nj_qty6_on']),
		trim($_POST['tfa_nj_qty6_type']),
		floatval($_POST['tfa_nj_qty6_amt']),

		floatval($_POST['tfa_nj_qty7_quan']),
		trim($_POST['tfa_nj_qty7_on']),
		trim($_POST['tfa_nj_qty7_type']),
		floatval($_POST['tfa_nj_qty7_amt']),

		floatval($_POST['tfa_nj_qty8_quan']),
		trim($_POST['tfa_nj_qty8_on']),
		trim($_POST['tfa_nj_qty8_type']),
		floatval($_POST['tfa_nj_qty8_amt']),

		floatval($_POST['tfa_nj_qty9_quan']),
		trim($_POST['tfa_nj_qty9_on']),
		trim($_POST['tfa_nj_qty9_type']),
		floatval($_POST['tfa_nj_qty9_amt']),

		floatval($_POST['tfa_nj_qty10_quan']),
		trim($_POST['tfa_nj_qty10_on']),
		trim($_POST['tfa_nj_qty10_type']),
		floatval($_POST['tfa_nj_qty10_amt']),

		$effective_month,
		$status,
		$_SESSION['mhwltdphp_user']
	));  

	if( $stmt === false )  
	{  
		 echo "Statement could not be prepared.\n";  
		 die( print_r( sqlsrv_errors(), true));  
	}  
		
	if( sqlsrv_execute($stmt) === false )  
	{  
	  echo "Statement could not be executed.\n";  
	  die( print_r( sqlsrv_errors(), true));  
	}  

}





if($_POST['tfa_nyr_state']=='Yes') {

	$tsql = "
		INSERT INTO tfa_nyr (
			itemID,
			tfa_nyr_change_type,
			tfa_nyr_price_bot, 
			tfa_nyr_price_case,
			tfa_nyr_fob,
			
			tfa_nyr_qty1_quan,
			tfa_nyr_qty1_on,
			tfa_nyr_qty1_type,
			tfa_nyr_qty1_amt,

			tfa_nyr_qty2_quan,
			tfa_nyr_qty2_on,
			tfa_nyr_qty2_type,
			tfa_nyr_qty2_amt,

			tfa_nyr_qty3_quan,
			tfa_nyr_qty3_on,
			tfa_nyr_qty3_type,
			tfa_nyr_qty3_amt,
			
			tfa_nyr_qty4_quan,
			tfa_nyr_qty4_on,
			tfa_nyr_qty4_type,
			tfa_nyr_qty4_amt,

			tfa_nyr_qty5_quan,
			tfa_nyr_qty5_on,
			tfa_nyr_qty5_type,
			tfa_nyr_qty5_amt,

			tfa_nyr_qty6_quan,
			tfa_nyr_qty6_on,
			tfa_nyr_qty6_type,
			tfa_nyr_qty6_amt,

			tfa_nyr_qty7_quan,
			tfa_nyr_qty7_on,
			tfa_nyr_qty7_type,
			tfa_nyr_qty7_amt,

			tfa_nyr_qty8_quan,
			tfa_nyr_qty8_on,
			tfa_nyr_qty8_type,
			tfa_nyr_qty8_amt,

			tfa_nyr_qty9_quan,
			tfa_nyr_qty9_on,
			tfa_nyr_qty9_type,
			tfa_nyr_qty9_amt,

			tfa_nyr_qty10_quan,
			tfa_nyr_qty10_on,
			tfa_nyr_qty10_type,
			tfa_nyr_qty10_amt,
			tfa_effective_month,
			[status],
			[user]
		)
		VALUES (
			?,?,?,?, ?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,?,?,?
		);
	";

	$stmt = sqlsrv_prepare( $conn, $tsql, array ( 
		$itemID,
		$change_type,
		floatval($_POST['tfa_nyr_price_bot']),
		floatval($_POST['tfa_nyr_price_case']),
		$_POST['tfa_nyr_fob'],
		
		floatval($_POST['tfa_nyr_qty1_quan']),
		trim($_POST['tfa_nyr_qty1_on']),
		trim($_POST['tfa_nyr_qty1_type']),
		floatval($_POST['tfa_nyr_qty1_amt']),
		
		floatval($_POST['tfa_nyr_qty2_quan']),
		trim($_POST['tfa_nyr_qty2_on']),
		trim($_POST['tfa_nyr_qty2_type']),
		floatval($_POST['tfa_nyr_qty2_amt']),

		floatval($_POST['tfa_nyr_qty3_quan']),
		trim($_POST['tfa_nyr_qty3_on']),
		trim($_POST['tfa_nyr_qty3_type']),
		floatval($_POST['tfa_nyr_qty3_amt']),

		floatval($_POST['tfa_nyr_qty4_quan']),
		trim($_POST['tfa_nyr_qty4_on']),
		trim($_POST['tfa_nyr_qty4_type']),
		floatval($_POST['tfa_nyr_qty4_amt']),

		floatval($_POST['tfa_nyr_qty5_quan']),
		trim($_POST['tfa_nyr_qty5_on']),
		trim($_POST['tfa_nyr_qty5_type']),
		floatval($_POST['tfa_nyr_qty5_amt']),

		floatval($_POST['tfa_nyr_qty6_quan']),
		trim($_POST['tfa_nyr_qty6_on']),
		trim($_POST['tfa_nyr_qty6_type']),
		floatval($_POST['tfa_nyr_qty6_amt']),

		floatval($_POST['tfa_nyr_qty7_quan']),
		trim($_POST['tfa_nyr_qty7_on']),
		trim($_POST['tfa_nyr_qty7_type']),
		floatval($_POST['tfa_nyr_qty7_amt']),

		floatval($_POST['tfa_nyr_qty8_quan']),
		trim($_POST['tfa_nyr_qty8_on']),
		trim($_POST['tfa_nyr_qty8_type']),
		floatval($_POST['tfa_nyr_qty8_amt']),

		floatval($_POST['tfa_nyr_qty9_quan']),
		trim($_POST['tfa_nyr_qty9_on']),
		trim($_POST['tfa_nyr_qty9_type']),
		floatval($_POST['tfa_nyr_qty9_amt']),

		floatval($_POST['tfa_nyr_qty10_quan']),
		trim($_POST['tfa_nyr_qty10_on']),
		trim($_POST['tfa_nyr_qty10_type']),
		floatval($_POST['tfa_nyr_qty10_amt']),

		$effective_month,
		$status,
		$_SESSION['mhwltdphp_user']
	));  

	if( $stmt === false )  
	{  
		 echo "Statement could not be prepared.\n";  
		 die( print_r( sqlsrv_errors(), true));  
	}  
		
	if( sqlsrv_execute($stmt) === false )  
	{  
	  echo "Statement could not be executed.\n";  
	  die( print_r( sqlsrv_errors(), true));  
	}  

}




if($_POST['tfa_nyw_state']=='Yes') {

	$tsql = "
		INSERT INTO tfa_nyw (
			itemID,
			tfa_nyw_change_type,
			tfa_nyw_price_bot, 
			tfa_nyw_price_case,
			tfa_nyw_fob,
			tfa_nyw_dist,
			tfa_nyw_disc,
			
			tfa_nyw_qty1_quan,
			tfa_nyw_qty1_on,
			tfa_nyw_qty1_type,
			tfa_nyw_qty1_amt,

			tfa_nyw_qty2_quan,
			tfa_nyw_qty2_on,
			tfa_nyw_qty2_type,
			tfa_nyw_qty2_amt,

			tfa_nyw_qty3_quan,
			tfa_nyw_qty3_on,
			tfa_nyw_qty3_type,
			tfa_nyw_qty3_amt,
			
			tfa_nyw_qty4_quan,
			tfa_nyw_qty4_on,
			tfa_nyw_qty4_type,
			tfa_nyw_qty4_amt,

			tfa_nyw_qty5_quan,
			tfa_nyw_qty5_on,
			tfa_nyw_qty5_type,
			tfa_nyw_qty5_amt,

			tfa_nyw_qty6_quan,
			tfa_nyw_qty6_on,
			tfa_nyw_qty6_type,
			tfa_nyw_qty6_amt,

			tfa_nyw_qty7_quan,
			tfa_nyw_qty7_on,
			tfa_nyw_qty7_type,
			tfa_nyw_qty7_amt,

			tfa_nyw_qty8_quan,
			tfa_nyw_qty8_on,
			tfa_nyw_qty8_type,
			tfa_nyw_qty8_amt,

			tfa_nyw_qty9_quan,
			tfa_nyw_qty9_on,
			tfa_nyw_qty9_type,
			tfa_nyw_qty9_amt,

			tfa_nyw_qty10_quan,
			tfa_nyw_qty10_on,
			tfa_nyw_qty10_type,
			tfa_nyw_qty10_amt,
			tfa_effective_month,
			[status],
			[user]
		)
		VALUES (
			?,?,?, ?,?,?,?, 
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,
			?,?,?,?, ?,?,?,?,?,?,?
		);
	";

	$stmt = sqlsrv_prepare( $conn, $tsql, array ( 
		$itemID,
		$change_type,
		floatval($_POST['tfa_nyw_price_bot']),
		floatval($_POST['tfa_nyw_price_case']),
		$_POST['tfa_nyw_fob'],
		is_array($_POST['tfa_nyw_dist']) ? implode($_POST['tfa_nyw_dist'],",") : "",
		$_POST['tfa_nyw_disc'],
		
		floatval($_POST['tfa_nyw_qty1_quan']),
		trim($_POST['tfa_nyw_qty1_on']),
		trim($_POST['tfa_nyw_qty1_type']),
		floatval($_POST['tfa_nyw_qty1_amt']),
		
		floatval($_POST['tfa_nyw_qty2_quan']),
		trim($_POST['tfa_nyw_qty2_on']),
		trim($_POST['tfa_nyw_qty2_type']),
		floatval($_POST['tfa_nyw_qty2_amt']),

		floatval($_POST['tfa_nyw_qty3_quan']),
		trim($_POST['tfa_nyw_qty3_on']),
		trim($_POST['tfa_nyw_qty3_type']),
		floatval($_POST['tfa_nyw_qty3_amt']),

		floatval($_POST['tfa_nyw_qty4_quan']),
		trim($_POST['tfa_nyw_qty4_on']),
		trim($_POST['tfa_nyw_qty4_type']),
		floatval($_POST['tfa_nyw_qty4_amt']),

		floatval($_POST['tfa_nyw_qty5_quan']),
		trim($_POST['tfa_nyw_qty5_on']),
		trim($_POST['tfa_nyw_qty5_type']),
		floatval($_POST['tfa_nyw_qty5_amt']),

		floatval($_POST['tfa_nyw_qty6_quan']),
		trim($_POST['tfa_nyw_qty6_on']),
		trim($_POST['tfa_nyw_qty6_type']),
		floatval($_POST['tfa_nyw_qty6_amt']),

		floatval($_POST['tfa_nyw_qty7_quan']),
		trim($_POST['tfa_nyw_qty7_on']),
		trim($_POST['tfa_nyw_qty7_type']),
		floatval($_POST['tfa_nyw_qty7_amt']),

		floatval($_POST['tfa_nyw_qty8_quan']),
		trim($_POST['tfa_nyw_qty8_on']),
		trim($_POST['tfa_nyw_qty8_type']),
		floatval($_POST['tfa_nyw_qty8_amt']),

		floatval($_POST['tfa_nyw_qty9_quan']),
		trim($_POST['tfa_nyw_qty9_on']),
		trim($_POST['tfa_nyw_qty9_type']),
		floatval($_POST['tfa_nyw_qty9_amt']),

		floatval($_POST['tfa_nyw_qty10_quan']),
		trim($_POST['tfa_nyw_qty10_on']),
		trim($_POST['tfa_nyw_qty10_type']),
		floatval($_POST['tfa_nyw_qty10_amt']),

		$effective_month,
		$status,
		$_SESSION['mhwltdphp_user']
	));  

	if( $stmt === false )  
	{  
		 echo "Statement could not be prepared.\n";  
		 die( print_r( sqlsrv_errors(), true));  
	}  
		
	if( sqlsrv_execute($stmt) === false )  
	{  
	  echo "Statement could not be executed.\n";  
	  die( print_r( sqlsrv_errors(), true));  
	}  

}



if($_POST['tfa_os_state']=='Yes') {

	$tsql = "
		INSERT INTO tfa_os (
			itemID,
			tfa_os_change_type,
			tfa_os_price_bottle,
			tfa_os_price_state,
			tfa_effective_month,
			[status],
			[user]
		)
		VALUES (
			?,?,?,?,?,?,?
		);
	";

	$stmt = sqlsrv_prepare( $conn, $tsql, array ( 
		$itemID,
		$change_type,
		floatval($_POST['tfa_os_price_bottle']),
		$_POST['tfa_os_price_state'],
		$effective_month,
		$status,
		$_SESSION['mhwltdphp_user']
	));  

	if( $stmt === false )  
	{  
		 echo "Statement could not be prepared.\n";  
		 die( print_r( sqlsrv_errors(), true));  
	}  
		
	if( sqlsrv_execute($stmt) === false )  
	{  
	  echo "Statement could not be executed.\n";  
	  die( print_r( sqlsrv_errors(), true));  
	}  

}


$json = array(
    'result' => sqlsrv_rows_affected ( $stmt )
);
header('Content-Type: application/json');
echo json_encode($json);


/* Free statement and connection resources. */  
sqlsrv_free_stmt( $stmt);  
sqlsrv_close( $conn); 

//header("Location: result.php");

?>