<?php
session_start();
include("prepend.php");
//include('head.php');

//echo "<pre>";
//var_dump($_REQUEST); 
//echo "</post>";

$referrerDomain = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
//echo $referrerDomain;
//exit();
if(!isset($_SESSION['mhwltdphp_user'])){

	$page_err_msg = "You are not logged in.";

	if($referrerDomain!=="mhwltd.app" && $referrerDomain!=="mhwapp.azurewebsites.net" && $referrerDomain!=="mhwapp-dev.azurewebsites.net" && $referrerDomain!=="mhwdev-apprise.azurewebsites.net" && $referrerDomain!=="crd.mhwltdclients.com" && $referrerDomain!=="mhwltdclients.com" && $referrerDomain!=="www.mhwltdclients.com" && $referrerDomain!=="mhwwebdev.nstwebbeta.com" && $referrerDomain!=="nstwebbeta.com" && $referrerDomain!=="mhwapprise.nstwebbeta.com" && 
	$referrerDomain!=="apprisetest.nstwebbeta.com" && 
	$referrerDomain!=="apprisedev.nstwebbeta.com" && 
	$referrerDomain!=='mhwappriseprod.nstwebbeta.com' && 
	$referrerDomain!=='legacy.mhwltdclients.com'
		/* &&
	$referrerDomain!=='apprisebeta.mhwltdclients.com' */
	){
			$page_err_msg = "The referring domain (".$referrerDomain.") is not whitelisted.";
	}
	else{
		// check that login form was complete
		if(!$_POST['username'] /* | !$_POST['pass']*/) {
			$page_err_msg = "You did not fill in a required field.";
		}
		else{
			//$_POST['pass'] = stripslashes($_POST['pass']);
			//$_POST['pass'] = md5($_POST['pass']);

			// if login is ok, set session, add a cookie
			$_POST['username'] = stripslashes($_POST['username']);
			$_SESSION['mhwltdphp_user'] = $_POST['username'];
			
			$_POST['userlastname'] = stripslashes($_POST['userlastname']);
			$_SESSION['mhwltdphp_userlastname'] = $_POST['userlastname'];
			
			$_POST['useremail'] = stripslashes($_POST['useremail']);
			$_SESSION['mhwltdphp_useremail'] = $_POST['useremail'];

			$_POST['userrole'] = stripslashes($_POST['userrole']);
			$_SESSION['mhwltdphp_userrole'] = $_POST['userrole'];

			$_POST['userclients'] = stripslashes($_POST['userclients']);
/////////////////////////////////////////////////////////////////
			$arclntlookup = 1;
			//FIX CRD LOGIN ISSUES
			//$_POST['userclients'] = str_replace("'","''",$_POST['userclients']);
			
			if($_POST['userclients']=="MHW, Ltd./Latitude Beve -DO NOT USE"){
				$_POST['userclients'] = str_replace('MHW, Ltd./Latitude Beve -DO NOT USE','MHW, Ltd./Latitude Beverage',$_POST['userclients']);
			}
			if($_POST['userclients']=="Latitude Beve -DO NOT USE"){
				$_POST['userclients'] = str_replace('Latitude Beve -DO NOT USE','MHW, Ltd./Latitude Beverage',$_POST['userclients']);
			}
			if($_POST['userclients']=="MHW, Ltd./Grace O"){
				$_POST['userclients'] = str_replace("MHW, Ltd./Grace O","MHW, Ltd./Grace OMalley",$_POST['userclients']);
			}
			if($_POST['userclients']=="Grace O"){
				$_POST['userclients'] = str_replace("Grace O","MHW, Ltd./Grace OMalley",$_POST['userclients']);
			}
			if($_POST['userclients']=="MHW, Ltd./R"){
				$_POST['userclients'] = str_replace("MHW, Ltd./R","MHW, Ltd./R Brothers",$_POST['userclients']);
				$arclntlookup = 0;
			}
			if($_POST['userclients']=="R"){
				$_POST['userclients'] = str_replace("R","MHW, Ltd./R Brothers",$_POST['userclients']);
				$arclntlookup = 0;
			}
			if($_POST['userclients']=="MHW, Ltd./Bertrand"){
				$_POST['userclients'] = str_replace("MHW, Ltd./Bertrand","MHW, Ltd./Bertrands Wines",$_POST['userclients']);
			}
			if($_POST['userclients']=="Bertrand"){
				$_POST['userclients'] = str_replace("Bertrand","MHW, Ltd./Bertrands Wines",$_POST['userclients']);
			}
			if($_POST['userclients']=="Vintner"){
				$_POST['userclients'] = str_replace("Vintner","MHW, Ltd./Vintners Alliance",$_POST['userclients']);
			}
			if($_POST['userclients']=="" || $_POST['userclients']==" " || strlen($_POST['userclients']) <= 1){
				$arclntlookup = 0;
			}
			//$_POST['userclients'] = str_replace("MHW, Ltd./Carson","MHW, Ltd./Carson'z",$_POST['userclients']);
			//$_SESSION['mhwltdphp_userclients'] = $_POST['userclients'];
/////////////////////////////////////////////////////////////////
			if($arclntlookup!=0){
				$oldclient = '';
				include("dbconnect.php");
				$conn = sqlsrv_connect($serverName, $connectionOptions);
				if( $conn === false) {
					die( print_r( sqlsrv_errors(), true));
				}
				$clients = explode(";",$_POST['userclients']);

				foreach ($clients as &$clientvalue) {
					if(trim($clientvalue)!==''){
						$thisclient = $clientvalue;
						$tsql_clients = "select ISNULL((select TOP 1 REPLACE(REPLACE(a.[COMPANY],'\"',''),'''','') as [clientName] from [dbo].[ARCLNT] a WHERE a.[COMPANY] = 'MHW, Ltd./".$clientvalue."' ORDER BY [clientName]),(select TOP 1 REPLACE(REPLACE(a.[COMPANY],'\"',''),'''','') as [clientName] from [dbo].[ARCLNT] a WHERE a.[COMPANY] LIKE '%".$clientvalue."%' ORDER BY [clientName])) as clientName";
						$getResults_clients = sqlsrv_query($conn, $tsql_clients);

						if ($getResults_clients == FALSE)
							echo (sqlsrv_errors());
						
						while ($row_client = sqlsrv_fetch_array($getResults_clients, SQLSRV_FETCH_ASSOC)) {
							$thisclient = trim($row_client['clientName']);
						}

						$oldclient.= $thisclient.';';

						sqlsrv_free_stmt($getResults_clients);
					}
				}
				sqlsrv_close($conn);

				$oldclient = substr($oldclient,0,strlen($oldclient)-1);
			}
////////////////////////////////////////////////////////////////////
			if(isset($oldclient) && $oldclient!=''){
				$_SESSION['mhwltdphp_userclients'] = $oldclient;
			}
			else{
				$_SESSION['mhwltdphp_userclients'] = $_POST['userclients'];
			}
////////////////////////////////////////////////////////////////////
			

			if(strtoupper($_SESSION['mhwltdphp_user'])=='DSZAREJKO'){
				$_SESSION['mhwltdphp_usertype'] = "ADMIN";
			}
			else{
				/* MHW Admin (1); MHW Standard User (2); MHW Staff (7) */
				if(intval($_SESSION['mhwltdphp_userrole'])==1 || intval($_SESSION['mhwltdphp_userrole'])==2 || intval($_SESSION['mhwltdphp_userrole'])==7){
					$_SESSION['mhwltdphp_usertype'] = "SUPERUSER";
				}
				else{
					$_SESSION['mhwltdphp_usertype'] = "CRDforms";
				}
			}

			$hour = time() + 14400; //4 hrs
			setcookie(ID_mhwphp, $_POST['username'], $hour);
			setcookie(UL_mhwphp, $_POST['userlastname'], $hour);
			setcookie(UE_mhwphp, $_POST['useremail'], $hour);
			setcookie(UC_mhwphp, $_SESSION['mhwltdphp_userclients'], $hour);
			setcookie(UT_mhwphp, $_SESSION['mhwltdphp_usertype'], $hour);
			//setcookie(Key_mhwphp, $_POST['pass'], $hour);

			//log access
			$fp1 = fopen('logs/accesslog.txt', 'a');
			$accesslog="\n\r\n\r".date("Ymd H:i:s")."\t".$_POST['username']."\t".$_POST['userlastname']."\t".$_POST['useremail']."\t".$_POST['userclients']."\t".$_SESSION['mhwltdphp_usertype']."\n\r";
			fwrite($fp1, $accesslog);
			fclose($fp1);

			// redirect to content
		
			if($_GET["redirect"]=="productsetup"){  //FROM CRD
				$page_err_msg = "";
				if(($referrerDomain=="mhwwebdev.nstwebbeta.com" || $referrerDomain=="mhwapprise.nstwebbeta.com" || 
				$referrerDomain=="apprisetest.nstwebbeta.com" || 
				$referrerDomain=="apprisedev.nstwebbeta.com" ||
				$referrerDomain=="nstwebbeta.com") /*&& strtoupper($_SESSION['mhwltdphp_user'])=='DSZAREJKO' */){
					//header("Location: formsdev/productsetup.php"); //NATIVE FORM
					header("Location: send.php");
				}
				else{
					header("Location: forms/productsetup.php"); //NATIVE FORM
				}
				//header("Location: product_interstitial.php?email=".urlencode($_SESSION['mhwltdphp_useremail'])."&lastname=".urlencode($_SESSION['mhwltdphp_userlastname'])); //FA LOGIN 2
			}
			else if($_GET["redirect"]=="orderreview"){ //FROM CRD
				$page_err_msg = "";
				header("Location: forms/orderreview.php"); 
			}
			/*
			else if($_GET["redirect"]=="priceposting"){ //FROM CRD
				$page_err_msg = "";
				//header("Location: priceposting.php"); //NATIVE FORM
				header("Location: pricing_interstitial.php?email=".urlencode($_SESSION['mhwltdphp_useremail'])."&lastname=".urlencode($_SESSION['mhwltdphp_userlastname'])); //FA LOGIN 2
			}
			else if($_GET["redirect"]=="pricingform"){
				$page_err_msg = "";
				header("Location: pricingform.php");
			}
			else if($_GET["redirect"]=="productform"){
				$page_err_msg = "";
				header("Location: productform.php");
			}
			else if($_GET["redirect"]=="product_interstitial"){
				$page_err_msg = "";
				header("Location: product_interstitial.php");
			}
			else if($_GET["redirect"]=="pricing_interstitial"){
				$page_err_msg = "";
				header("Location: pricing_interstitial.php");
			}
			*/
			else{
				$page_err_msg = "";
				header("Location: home.php");
			}	
		
		}
	}

	//if not logged in
	echo "<span id=\"loginmsg\">".$page_err_msg."</span>";

}
else{  //existing session
	
	//reauthenticate without requiring logout (CRD impersonate user)	
	if($_POST['username'] && ($referrerDomain=="mhwltd.app" || $referrerDomain=="mhwltdclients.com" || $referrerDomain=="mhwwebdev.nstwebbeta.com"  || $referrerDomain=="mhwdev-apprise.azurewebsites.net" || $referrerDomain=="mhwapprise.nstwebbeta.com" || $referrerDomain=="nstwebbeta.com" || 
	$referrerDomain=="apprisetest.nstwebbeta.com" || 
	$referrerDomain=="apprisedev.nstwebbeta.com" || 
	$referrerDomain=='mhwappriseprod.nstwebbeta.com' || 
	$referrerDomain=='legacy.mhwltdclients.com' 
		/* || 
	$referrerDomain=='apprisebeta.mhwltdclients.com' */
	)){
		
		// if login is ok, set session, add a cookie
		$_POST['username'] = stripslashes($_POST['username']);
		$_SESSION['mhwltdphp_user'] = $_POST['username'];
		
		$_POST['userlastname'] = stripslashes($_POST['userlastname']);
		$_SESSION['mhwltdphp_userlastname'] = $_POST['userlastname'];
		
		$_POST['useremail'] = stripslashes($_POST['useremail']);
		$_SESSION['mhwltdphp_useremail'] = $_POST['useremail'];

		$_POST['userrole'] = stripslashes($_POST['userrole']);
		$_SESSION['mhwltdphp_userrole'] = $_POST['userrole'];
		
		$_POST['userclients'] = stripslashes($_POST['userclients']);
		/////////////////////////////////////////////////////////////////
			$arclntlookup = 1;
			//FIX CRD LOGIN ISSUES
			//$_POST['userclients'] = str_replace("'","''",$_POST['userclients']);
			
			if($_POST['userclients']=="MHW, Ltd./Latitude Beve -DO NOT USE"){
				$_POST['userclients'] = str_replace('MHW, Ltd./Latitude Beve -DO NOT USE','MHW, Ltd./Latitude Beverage',$_POST['userclients']);
			}
			if($_POST['userclients']=="Latitude Beve -DO NOT USE"){
				$_POST['userclients'] = str_replace('Latitude Beve -DO NOT USE','MHW, Ltd./Latitude Beverage',$_POST['userclients']);
			}
			if($_POST['userclients']=="MHW, Ltd./Grace O"){
				$_POST['userclients'] = str_replace("MHW, Ltd./Grace O","MHW, Ltd./Grace OMalley",$_POST['userclients']);
			}
			if($_POST['userclients']=="Grace O"){
				$_POST['userclients'] = str_replace("Grace O","MHW, Ltd./Grace OMalley",$_POST['userclients']);
			}
			if($_POST['userclients']=="MHW, Ltd./R"){
				$_POST['userclients'] = str_replace("MHW, Ltd./R","MHW, Ltd./R Brothers",$_POST['userclients']);
				$arclntlookup = 0;
			}
			if($_POST['userclients']=="R"){
				$_POST['userclients'] = str_replace("R","MHW, Ltd./R Brothers",$_POST['userclients']);
				$arclntlookup = 0;
			}
			if($_POST['userclients']=="MHW, Ltd./Bertrand"){
				$_POST['userclients'] = str_replace("MHW, Ltd./Bertrand","MHW, Ltd./Bertrands Wines",$_POST['userclients']);
			}
			if($_POST['userclients']=="Bertrand"){
				$_POST['userclients'] = str_replace("Bertrand","MHW, Ltd./Bertrands Wines",$_POST['userclients']);
			}
			if($_POST['userclients']=="Vintner"){
				$_POST['userclients'] = str_replace("Vintner","MHW, Ltd./Vintners Alliance",$_POST['userclients']);
			}
			if($_POST['userclients']=="" || $_POST['userclients']==" " || strlen($_POST['userclients']) <= 1){
				$arclntlookup = 0;
			}
			//$_POST['userclients'] = str_replace("MHW, Ltd./Carson","MHW, Ltd./Carson'z",$_POST['userclients']);
			//$_SESSION['mhwltdphp_userclients'] = $_POST['userclients'];
			/////////////////////////////////////////////////////////////////
			if($arclntlookup!=0){
				$oldclient = '';
				include("dbconnect.php");
				$conn = sqlsrv_connect($serverName, $connectionOptions);
				if( $conn === false) {
					die( print_r( sqlsrv_errors(), true));
				}
				$clients = explode(";",$_POST['userclients']);

				foreach ($clients as &$clientvalue) {
					if(trim($clientvalue)!==''){
						$thisclient = $clientvalue;
						$tsql_clients = "select ISNULL((select TOP 1 REPLACE(REPLACE(a.[COMPANY],'\"',''),'''','') as [clientName] from [dbo].[ARCLNT] a WHERE a.[COMPANY] = 'MHW, Ltd./".$clientvalue."' ORDER BY [clientName]),(select TOP 1 REPLACE(REPLACE(a.[COMPANY],'\"',''),'''','') as [clientName] from [dbo].[ARCLNT] a WHERE a.[COMPANY] LIKE '%".$clientvalue."%' ORDER BY [clientName])) as clientName";
						$getResults_clients = sqlsrv_query($conn, $tsql_clients);

						if ($getResults_clients == FALSE)
							echo (sqlsrv_errors());
						
						while ($row_client = sqlsrv_fetch_array($getResults_clients, SQLSRV_FETCH_ASSOC)) {
							$thisclient = trim($row_client['clientName']);
						}

						$oldclient.= $thisclient.';';

						sqlsrv_free_stmt($getResults_clients);
					}
				}
				sqlsrv_close($conn);

				$oldclient = substr($oldclient,0,strlen($oldclient)-1);
			}
			////////////////////////////////////////////////////////////////////
			if(isset($oldclient) && $oldclient!=''){
				$_SESSION['mhwltdphp_userclients'] = $oldclient;
			}
			else{
				$_SESSION['mhwltdphp_userclients'] = $_POST['userclients'];
			}
////////////////////////////////////////////////////////////////////





		if(strtoupper($_SESSION['mhwltdphp_user'])=='DSZAREJKO'){
			$_SESSION['mhwltdphp_usertype'] = "ADMIN";
		}
		else{
			/* MHW Admin (1); MHW Standard User (2); MHW Staff (7) */
			if(intval($_SESSION['mhwltdphp_userrole'])==1 || intval($_SESSION['mhwltdphp_userrole'])==2 || intval($_SESSION['mhwltdphp_userrole'])==7){
				$_SESSION['mhwltdphp_usertype'] = "SUPERUSER";
			}
			else{
				$_SESSION['mhwltdphp_usertype'] = "CRDforms";
			}
		}

		$hour = time() + 14400; //4 hrs
		setcookie(ID_mhwphp, $_POST['username'], $hour);
		setcookie(UL_mhwphp, $_POST['userlastname'], $hour);
		setcookie(UE_mhwphp, $_POST['useremail'], $hour);
		setcookie(UC_mhwphp, $_SESSION['mhwltdphp_userclients'], $hour);
		setcookie(UT_mhwphp, $_SESSION['mhwltdphp_usertype'], $hour);
		//setcookie(Key_mhwphp, $_POST['pass'], $hour);	

		//log access
		$fp1 = fopen('logs/accesslog.txt', 'a');
		$accesslog="\n\r\n\r".date("Ymd H:i:s")."\t".$_POST['username']."\t".$_POST['userlastname']."\t".$_POST['useremail']."\t".$_POST['userclients']."\t".$_SESSION['mhwltdphp_usertype']."\n\r";
		fwrite($fp1, $accesslog);
		fclose($fp1);
	}
	
	// redirect to content
	if($_GET["redirect"]=="productsetup"){  //FROM CRD
		$page_err_msg = "";
		if(($referrerDomain=="mhwwebdev.nstwebbeta.com" || $referrerDomain=="mhwapprise.nstwebbeta.com" || 
		$referrerDomain=="apprisetest.nstwebbeta.com" || 
		$referrerDomain=="apprisedev.nstwebbeta.com" || 
		$referrerDomain=="nstwebbeta.com") /*&& strtoupper($_SESSION['mhwltdphp_user'])=='DSZAREJKO' */){
			//header("Location: formsdev/productsetup.php"); //NATIVE FORM
			header("Location: send.php");
		}
		else{
			header("Location: forms/productsetup.php"); //NATIVE FORM
		}
		//header("Location: product_interstitial.php?email=".urlencode($_SESSION['mhwltdphp_useremail'])."&lastname=".urlencode($_SESSION['mhwltdphp_userlastname'])); //FA LOGIN 2
	}
	else if($_GET["redirect"]=="orderreview"){ //FROM CRD
		$page_err_msg = "";
		header("Location: forms/orderreview.php"); 
	}
	/*
	else if($_GET["redirect"]=="priceposting"){  //FROM CRD
		$page_err_msg = "";
		//header("Location: priceposting.php"); //NATIVE FORM
		header("Location: pricing_interstitial.php?email=".urlencode($_SESSION['mhwltdphp_useremail'])."&lastname=".urlencode($_SESSION['mhwltdphp_userlastname'])); //FA LOGIN 2
	}
	else if($_GET["redirect"]=="pricingform"){
		$page_err_msg = "";
		header("Location: pricingform.php");
	}
	else if($_GET["redirect"]=="productform"){
		$page_err_msg = "";
		header("Location: productform.php");
	}
	else if($_GET["redirect"]=="product_interstitial"){
		$page_err_msg = "";
		header("Location: product_interstitial.php");
	}
	else if($_GET["redirect"]=="pricing_interstitial"){
		$page_err_msg = "";
		header("Location: pricing_interstitial.php");
	}
	*/
	else{
		$page_err_msg = "";
		header("Location: home.php");
	}
}
?>


