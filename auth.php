<?php
session_start();
include("prepend.php");
//include('head.php');

//echo "<pre>";
//var_dump($_REQUEST); 
//echo "</post>";

$referrerDomain = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
//echo $referrerDomain;
//exit();
if(!isset($_SESSION['mhwltdphp_user'])){

	$page_err_msg = "You are not logged in.";

	if($referrerDomain!=="mhwltd.app" && $referrerDomain!=="mhwapp.azurewebsites.net" && $referrerDomain!=="mhwapp-dev.azurewebsites.net" && $referrerDomain!=="crd.mhwltdclients.com" && $referrerDomain!=="mhwltdclients.com" && $referrerDomain!=="www.mhwltdclients.com" && $referrerDomain!=="mhwwebdev.nstwebbeta.com" && $referrerDomain!=="nstwebbeta.com" && $referrerDomain!=="mhwapprise.nstwebbeta.com"){
			$page_err_msg = "The referring domain (".$referrerDomain.") is not whitelisted.";
	}
	else{
		// check that login form was complete
		if(!$_POST['username'] /* | !$_POST['pass']*/) {
			$page_err_msg = "You did not fill in a required field.";
		}
		else{
			//$_POST['pass'] = stripslashes($_POST['pass']);
			//$_POST['pass'] = md5($_POST['pass']);

			// if login is ok, set session, add a cookie
			$_POST['username'] = stripslashes($_POST['username']);
			$_SESSION['mhwltdphp_user'] = $_POST['username'];
			
			$_POST['userlastname'] = stripslashes($_POST['userlastname']);
			$_SESSION['mhwltdphp_userlastname'] = $_POST['userlastname'];
			
			$_POST['useremail'] = stripslashes($_POST['useremail']);
			$_SESSION['mhwltdphp_useremail'] = $_POST['useremail'];

			$_POST['userrole'] = stripslashes($_POST['userrole']);
			$_SESSION['mhwltdphp_userrole'] = $_POST['userrole'];
			
			$_POST['userclients'] = stripslashes($_POST['userclients']);
			//FIX CRD LOGIN ISSUES
			//$_POST['userclients'] = str_replace("'","''",$_POST['userclients']);
			$_POST['userclients'] = str_replace('MHW, Ltd./Latitude Beve -DO NOT USE','MHW, Ltd./Latitude Beverage',$_POST['userclients']);
			$_POST['userclients'] = str_replace("MHW, Ltd./Grace O","MHW, Ltd./Grace OMalley",$_POST['userclients']);
			if($_POST['userclients']=="MHW, Ltd./R"){
				$_POST['userclients'] = str_replace("MHW, Ltd./R","MHW, Ltd./R Brothers",$_POST['userclients']);
			}
			//$_POST['userclients'] = str_replace("MHW, Ltd./Carson","MHW, Ltd./Carson'z",$_POST['userclients']);
			
			$_SESSION['mhwltdphp_userclients'] = $_POST['userclients'];
			
			if(strtoupper($_SESSION['mhwltdphp_user'])=='DSZAREJKO'){
				$_SESSION['mhwltdphp_usertype'] = "ADMIN";
			}
			else{
				/* MHW Admin (1); MHW Standard User (2); MHW Staff (7) */
				if(intval($_SESSION['mhwltdphp_userrole'])==1 || intval($_SESSION['mhwltdphp_userrole'])==2 || intval($_SESSION['mhwltdphp_userrole'])==7){
					$_SESSION['mhwltdphp_usertype'] = "SUPERUSER";
				}
				else{
					$_SESSION['mhwltdphp_usertype'] = "CRDforms";
				}
			}

			//$hour = time() + 14400; //4 hrs
			//setcookie(ID_mhwphp, $_POST['username'], $hour);
			//setcookie(UL_mhwphp, $_POST['userlastname'], $hour);
			//setcookie(UE_mhwphp, $_POST['useremail'], $hour);
			//setcookie(UC_mhwphp, $_POST['userclients'], $hour);
			//setcookie(UT_mhwphp, $_SESSION['mhwltdphp_usertype'], $hour);
			////setcookie(Key_mhwphp, $_POST['pass'], $hour);

			//log access
			$fp1 = fopen('logs/accesslog.txt', 'a');
			$accesslog="\n\r\n\r".date("Ymd H:i:s")."\t".$_POST['username']."\t".$_POST['userlastname']."\t".$_POST['useremail']."\t".$_POST['userclients']."\t".$_SESSION['mhwltdphp_usertype']."\n\r";
			fwrite($fp1, $accesslog);
			fclose($fp1);

			// redirect to content
		
			if($_GET["redirect"]=="productsetup"){  //FROM CRD
				$page_err_msg = "";
				//if(($referrerDomain=="mhwwebdev.nstwebbeta.com" || $referrerDomain=="mhwapprise.nstwebbeta.com" || $referrerDomain=="nstwebbeta.com") && strtoupper($_SESSION['mhwltdphp_user'])=='DSZAREJKO'){
					//header("Location: formsdev/productsetup.php"); //NATIVE FORM
				//	header("Location: home.php");
				//}
				//else{
					header("Location: forms/productsetup.php"); //NATIVE FORM
				//}
				//header("Location: product_interstitial.php?email=".urlencode($_SESSION['mhwltdphp_useremail'])."&lastname=".urlencode($_SESSION['mhwltdphp_userlastname'])); //FA LOGIN 2
			}
			else if($_GET["redirect"]=="orderreview"){ //FROM CRD
				$page_err_msg = "";
				header("Location: forms/orderreview.php"); 
			}
			/*
			else if($_GET["redirect"]=="priceposting"){ //FROM CRD
				$page_err_msg = "";
				//header("Location: priceposting.php"); //NATIVE FORM
				header("Location: pricing_interstitial.php?email=".urlencode($_SESSION['mhwltdphp_useremail'])."&lastname=".urlencode($_SESSION['mhwltdphp_userlastname'])); //FA LOGIN 2
			}
			else if($_GET["redirect"]=="pricingform"){
				$page_err_msg = "";
				header("Location: pricingform.php");
			}
			else if($_GET["redirect"]=="productform"){
				$page_err_msg = "";
				header("Location: productform.php");
			}
			else if($_GET["redirect"]=="product_interstitial"){
				$page_err_msg = "";
				header("Location: product_interstitial.php");
			}
			else if($_GET["redirect"]=="pricing_interstitial"){
				$page_err_msg = "";
				header("Location: pricing_interstitial.php");
			}
			*/
			else{
				$page_err_msg = "";
				header("Location: home.php");
			}	
		
		}
	}

	//if not logged in
	echo "<span id=\"loginmsg\">".$page_err_msg."</span>";

}
else{  //existing session
	
	//reauthenticate without requiring logout (CRD impersonate user)	
	if($_POST['username'] && ($referrerDomain=="mhwltd.app" || $referrerDomain=="mhwltdclients.com" || $referrerDomain=="mhwwebdev.nstwebbeta.com" || $referrerDomain=="mhwapprise.nstwebbeta.com" || $referrerDomain=="nstwebbeta.com")){
		
		// if login is ok, set session, add a cookie
		$_POST['username'] = stripslashes($_POST['username']);
		$_SESSION['mhwltdphp_user'] = $_POST['username'];
		
		$_POST['userlastname'] = stripslashes($_POST['userlastname']);
		$_SESSION['mhwltdphp_userlastname'] = $_POST['userlastname'];
		
		$_POST['useremail'] = stripslashes($_POST['useremail']);
		$_SESSION['mhwltdphp_useremail'] = $_POST['useremail'];
		
		$_POST['userrole'] = stripslashes($_POST['userrole']);
		$_SESSION['mhwltdphp_userrole'] = $_POST['userrole'];
		
		$_POST['userclients'] = stripslashes($_POST['userclients']);
		//FIX CRD LOGIN ISSUES
		$_POST['userclients'] = str_replace('MHW, Ltd./Latitude Beve -DO NOT USE','MHW, Ltd./Latitude Beverage',$_POST['userclients']);
		$_POST['userclients'] = str_replace("MHW, Ltd./Grace O","MHW, Ltd./Grace OMalley",$_POST['userclients']);
		if($_POST['userclients']=="MHW, Ltd./R"){
			$_POST['userclients'] = str_replace("MHW, Ltd./R","MHW, Ltd./R Brothers",$_POST['userclients']);
		}
		//$_POST['userclients'] = str_replace("MHW, Ltd./Carson","MHW, Ltd./Carson'z",$_POST['userclients']);
		$_SESSION['mhwltdphp_userclients'] = $_POST['userclients'];
		
		if(strtoupper($_SESSION['mhwltdphp_user'])=='DSZAREJKO'){
			$_SESSION['mhwltdphp_usertype'] = "ADMIN";
		}
		else{
			/* MHW Admin (1); MHW Standard User (2); MHW Staff (7) */
			if(intval($_SESSION['mhwltdphp_userrole'])==1 || intval($_SESSION['mhwltdphp_userrole'])==2 || intval($_SESSION['mhwltdphp_userrole'])==7){
				$_SESSION['mhwltdphp_usertype'] = "SUPERUSER";
			}
			else{
				$_SESSION['mhwltdphp_usertype'] = "CRDforms";
			}
		}

		//$hour = time() + 14400; //4 hrs
		//setcookie(ID_mhwphp, $_POST['username'], $hour);
		//setcookie(UL_mhwphp, $_POST['userlastname'], $hour);
		//setcookie(UE_mhwphp, $_POST['useremail'], $hour);
		//setcookie(UC_mhwphp, $_POST['userclients'], $hour);
		//setcookie(UT_mhwphp, $_SESSION['mhwltdphp_usertype'], $hour);
		//setcookie(Key_mhwphp, $_POST['pass'], $hour);	

		//log access
		$fp1 = fopen('logs/accesslog.txt', 'a');
		$accesslog="\n\r\n\r".date("Ymd H:i:s")."\t".$_POST['username']."\t".$_POST['userlastname']."\t".$_POST['useremail']."\t".$_POST['userclients']."\t".$_SESSION['mhwltdphp_usertype']."\n\r";
		fwrite($fp1, $accesslog);
		fclose($fp1);
	}
	
	// redirect to content
	if($_GET["redirect"]=="productsetup"){  //FROM CRD
		$page_err_msg = "";
		header("Location: forms/productsetup.php"); //NATIVE FORM
		//header("Location: product_interstitial.php?email=".urlencode($_SESSION['mhwltdphp_useremail'])."&lastname=".urlencode($_SESSION['mhwltdphp_userlastname'])); //FA LOGIN 2
	}
	else if($_GET["redirect"]=="orderreview"){ //FROM CRD
		$page_err_msg = "";
		header("Location: forms/orderreview.php"); 
	}
	/*
	else if($_GET["redirect"]=="priceposting"){  //FROM CRD
		$page_err_msg = "";
		//header("Location: priceposting.php"); //NATIVE FORM
		header("Location: pricing_interstitial.php?email=".urlencode($_SESSION['mhwltdphp_useremail'])."&lastname=".urlencode($_SESSION['mhwltdphp_userlastname'])); //FA LOGIN 2
	}
	else if($_GET["redirect"]=="pricingform"){
		$page_err_msg = "";
		header("Location: pricingform.php");
	}
	else if($_GET["redirect"]=="productform"){
		$page_err_msg = "";
		header("Location: productform.php");
	}
	else if($_GET["redirect"]=="product_interstitial"){
		$page_err_msg = "";
		header("Location: product_interstitial.php");
	}
	else if($_GET["redirect"]=="pricing_interstitial"){
		$page_err_msg = "";
		header("Location: pricing_interstitial.php");
	}
	*/
	else{
		$page_err_msg = "";
		header("Location: home.php");
	}
}
?>


