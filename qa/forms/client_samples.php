<?php
//session_start();
include("sessionhandler.php");
include("prepend.php");
include("settings.php");
include("functions.php");

if(!isset($_SESSION['mhwltdphp_user'])){
	header("Location: login.php");
}
else{
	include("dbconnect.php");
	include('head.php');

	$admin_type_user=false;
	if($_SESSION['mhwltdphp_usertype'] == "SUPERUSER" || $_SESSION['mhwltdphp_usertype'] == "ADMIN" ){ 
		$admin_type_user=true;
	}
	
	if(!$admin_type_user){
		$smplclients='';
		$access_client_user=false;
		$clientsLIST = explode(";",$_SESSION['mhwltdphp_userclientcodes']);

		$conn = sqlsrv_connect($serverName, $connectionOptions);
		if( $conn === false) die( print_r( sqlsrv_errors(), true));

		foreach ($clientsLIST as &$clientLISTvalue) {

			$tsql= "SELECT TOP 1 * FROM [mhw_app_client_access] WHERE [user_application] = 'Event_Samples' and [active] = 1 and [client_code] = '".$clientLISTvalue."'";
			$stmt = sqlsrv_query( $conn, $tsql);
			if ( $stmt === false ) die( print_r( sqlsrv_errors(), true));
			while ( $row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC) ) { 
				$smplclients.=$row['client_code'].';';
				$smplclientclass[$row['client_code']] = $row['user_class']; 
				$access_client_user=true;
			}
		}
		$smplclients = substr($smplclients,0,strlen($smplclients)-1);
		
		sqlsrv_free_stmt($stmt);

		if(!$access_client_user){
			die( "Stay tuned! This feature will be available soon. You can return to CRD by clicking here <a href='".$sitelogin."'>".$sitelogin."</a>" ); 
		}

		$_SESSION['mhwltdphp_userclientcodes'] = $smplclients;
	}
	
	$param1 = $_GET["ok"];
?>

		<script>

		var debug = true;
		$( document ).ready(function() {
			$("#finalize").hide();
			$('#client_name').on('change', function () {
				var value = $(this).val().toLowerCase();
				var valorig = $(this).val();

				if(value!="all"){
					$('#clientSampleTable').bootstrapTable('filterBy', {client_name: valorig});
				}
				else{
					$('#clientSampleTable').bootstrapTable('filterBy', '');
				}

				$('.prodedit_btn').on("click", function() {
					var prod = $(this).data("target");
					$("#editprod_"+prod).submit();
				});
			});

			$(".prodedit_btn").on("click", function() {
				var prod = $(this).data("target");
				$("#editprod_"+prod).submit();
			});
			/////////////////////////////////////////////////////////////
			function processResponse(qryTYPE, qryID, qryMSG){
				var qryUSER = '<?php echo $_SESSION["mhwltdphp_user"]; ?>';
				$.post('query-request-samples.php', {qrytype:qryTYPE,qryid:qryID,qrymsg:qryMSG,qryuser:qryUSER}, function(dataTH){

				});
			}

			$(document).off('click', '.btna').on('click', '.btna',function(){
				//alert('approved');
				var thisid = $(this).data('id');
				var thismsg = $('#rr__'+thisid).val();

				processResponse('OrdRevApp',thisid,thismsg);

				var trparent = $(this).closest("tr");
				var trdetail = trparent.next();
				if(trdetail.hasClass("detail-view")){
					trdetail.hide();
				}
				trparent.css("color", "green");
				trparent.slideUp("slow");
				//}
			});

			$(document).off('click', '.btnr').on('click', '.btnr',function(){
				//alert('rejected');
				var thisid = $(this).data('id');
				var thismsg = $('#rr__'+thisid).val();

				//tbd-require lines to be auto-expanded?

				if(!thismsg || thismsg==''){
					alert('review remark is required');
					$('#rr__'+thisid).focus();
				}
				else{

					//tbd-ajax post
					processResponse('OrdRevRej',thisid,thismsg);

					var trparent = $(this).closest("tr");
					var trdetail = trparent.next();
					if(trdetail.hasClass("detail-view")){
						trdetail.hide();
					}
					trparent.css("color", "red");
					trparent.slideUp("slow");
				}
			});

			$(document).off('click', '.btnla').on('click', '.btnla',function(){
				//alert('approved');
				var thisid = $(this).data('id');
				var thisparentid = $(this).data('parentid');
				var thismsg = $('#rrl__'+thisid).val();


				//tbd-ajax post
				processResponse('LineRevApp',thisid,thismsg);

				//tbd-disable parent buttons
				$('#btnr__'+thisparentid).hide();
				$('#btna__'+thisparentid).hide();
				var trparent = $(this).closest("tr");
				var trdetail = trparent.next();
				if(trdetail.hasClass("detail-view")){
					trdetail.hide();
				}
				trparent.css("color", "green");
				trparent.slideUp("slow");
				//}
			});

			$(document).off('click', '.btnlr').on('click', '.btnlr',function(){
				//alert('rejected');
				var thisid = $(this).data('id');
				var thisparentid = $(this).data('parentid');
				var thismsg = $('#rrl__'+thisid).val();

				if(!thismsg || thismsg==''){
					alert('review remark is required');
					$('#rrl__'+thisid).focus();
				}
				else{

					//tbd-ajax post
					processResponse('LineRevRej',thisid,thismsg);

					//tbd-disable parent buttons
					$('#btnr__'+thisparentid).hide();
					$('#btna__'+thisparentid).hide();

					var trparent = $(this).closest("tr");
					var trdetail = trparent.next();
					if(trdetail.hasClass("detail-view")){
						trdetail.hide();
					}
					trparent.css("color", "red");
					trparent.slideUp("slow");
				}
			});

			$(document).off('click', '.btn_itemtoggle').on('click', '.btn_itemtoggle',function(e) {
				var trparent = $(this).closest("tr");
				var prodindex = $(this).data('index');
				var exp = trparent.find(".detail-icon");
				$( exp ).trigger( "click" );
			});
			//////////////////////////////////////////////////////////////////////////////////////
			//function (to be called within client_samples.php) to update alert area in nav (head.php)
			function refreshAlert(){
				var clientlistH = '<?php echo $_SESSION["mhwltdphp_userclients"]; ?>';
				var qrytypeH = 'STAT_COUNT_unfinalized';
				$.post('query-request-samples.php', {qrytype: qrytypeH, clientlist: clientlistH}, function(dataTH){
					// ajax request query-request.php, send qrytype & client as post variable, return dataX variable, parse into JSON
					var resp = JSON.parse(dataTH);
					$.each( resp, function( key, value ) {
						if(parseInt(value.unfinalizedCount)>0){
							$("#userAlerts").html("<i class=\"fas fa-exclamation-triangle\"></i><a href=\"client_samples.php?v=nf\"> Not Finalized <span class=\"badge badge-light\">"+value.unfinalizedCount+"</span></a>");
						}
						else{
							$("#userAlerts").html("");
						}

					});
				});
			}
			//function to initialize all of the in-modal functionality
			function modalStuff(){
				//from image details to image full preview
				$('.prodimg_thumb').bind('click',function(){
					var imageid = $(this).data('imageid');
					var productid = $(this).data('product');
					var imageurl = 'viewfiles.php?iid='+imageid+'&b='+productid;
					$('.modal-body').load(imageurl,function(){
						$('#fileModalx').modal({show:true});
						modalStuff(); //recursive but necessary to initialize other states on each change
					});
				});
				//back link in modal from image full to image details
				$('.prodimg_back').bind('click',function(){
					var productid = $(this).data('product');
					var producturl = 'viewfiles.php?pid='+productid;
					$('.modal-body').load(producturl,function(){
						$('#fileModalx').modal({show:true});
						modalStuff(); //recursive but necessary to initialize other states on each change
					});
				});
				//initialize prod image modal functionality again
				$('.prodimg_btn').bind('click',function(){
					var productid = $(this).data('product');
					var imageurl = 'viewfiles.php?pid='+productid;
					$('.modal-body').load(imageurl,function(){
						$('#fileModalx').modal({show:true});
						modalStuff(); //recursive but necessary to initialize other states on each change
					});
				});
				//download image button
				$('.dwnld').bind('click', function () {
					var imagefile = $(this).data('url');
					var imagename = $(this).data('imagename');
					$.ajax({
						url: imagefile,
						method: 'GET',
						xhrFields: {
							responseType: 'blob'
						},
						success: function (data) {
							var a = document.createElement('a');
							var url = window.URL.createObjectURL(data);
							a.href = url;
							a.download = imagename;
							a.click();
							window.URL.revokeObjectURL(url);
						}
					});
				});
				//go to upload page for product
				$('.prod_upload').bind('click', function () {
					var prod = $(this).data('prod');
					window.location = 'imageupload.php?pid='+prod;
				});
				$(".prodedit_btn").bind("click", function() {
					var prod = $(this).data("target");
					$("#editprod_"+prod).submit();
				});
			};


			//Bootstrap table
			var $table = $('#clientSampleTable')
			var $remove = $('#remove')
			var selections = []

			function getIdSelections() {
				return $.map($table.bootstrapTable('getSelections'), function (row) {
					var viewname_raw = $("#screenview_name").val();
					if(viewname_raw=="Items") {
						return row.item_id
					}
					else{
						return row.product_id
						//return row.product_mhw_code
					}
				})
			}
			function getCodeSelections() {
				return $.map($table.bootstrapTable('getSelections'), function (row) {
					var viewname_raw = $("#screenview_name").val();
					if(viewname_raw=="Items") {
						return row.item_id
					}
					else{
						//return row.product_id
						return row.product_mhw_code
					}
				})
			}
			function expandTable($detail, row) {
				$detail.bootstrapTable('showLoading');
				subtableBuilder($detail.html('<table class="table-info"></table>').find('table'), 'client_item_list', row.sampleOrderNo);
			}
			function responseHandler(res) {
				$.each(res.rows, function (i, row) {
					row.state = $.inArray(row.id, selections) !== -1
				})
				return res
			}

			function detailFormatter(index, row) {
				var html = []
				$.each(row, function (key, value) {
					html.push('<p><b>' + key + ':</b> ' + value + '</p>')
				})
				return html.join('')
			}

			window.operateEvents = {
				'click .like': function (e, value, row, index) {
					alert('You click like action, row: ' + JSON.stringify(row))
				},
				'click .prodimg_btn': function (e, value, row, index) {
					alert('You click prodimg_btn action, row: ' + JSON.stringify(row))
				},
				'click .remove': function (e, value, row, index) {
					$table.bootstrapTable('remove', {
						field: 'id',
						values: [row.id]
					})
				}
			}

			//buttons
			function operateFormatter(value, row, index) {
				return [
					'<!--<button type="button" class="btn btn-outline-secondary prodimg_btn btn-sm bg_arrow_lightblue" data-product="'+row.product_id+'">-->',
					'<!--Files <span class="badge badge-light">'+row.filecount+'</span></button>-->',
					'<button type="button" class="btn btn-primary btn-sm bg_arrow_blue btn_itemtoggle" data-toggle="collapse" data-index="'+index+'" data-target=".itemprod'+row.product_id+'" data-prod="'+row.product_id+'">',
					'Lines <span class="badge badge-light">'+row.itemcount+'</span></button>',
					'<!--<form id="editprod_'+row.product_id+'" method="POST" action="productsetup.php"><input type="hidden" id="product_id" name="product_id" value="'+row.product_id+'">-->',
					'<!--<input type="hidden" id="product_desc" name="product_desc" value="'+row.product_desc+'">-->',
					'<!--<input type="hidden" id="product_mhw_code" name="product_mhw_code" value="'+row.product_mhw_code+'">-->',
					'<!--<input type="hidden" id="brand_name" name="brand_name" value="'+row.brand_name+'">-->',
					'<!--<input type="hidden" id="client_name" name="client_name" value="'+row.client_name+'"> </form>-->',
					'<!--<button type="button" class="btn btn-dark btn-sm prodedit_btn bg_arrow_darkblue" data-target="'+row.product_id+'">-->',
					'<!--<i class=\"far fa-edit\"></i> Edit</button>-->'
				].join('')
			}
			function subtableBuilder ($el, qrytype, eventId) {
				var data;
				$el.bootstrapTable('showLoading');
				$.post('query-request-samples.php', { qrytype: qrytype, event_id: eventId }, function(dataIT){ // ajax request query-request.php, send qrytype & client as post variable, return dataX variable, parse into JSON
					data = JSON.parse(dataIT);
					//item sub table
					$el.bootstrapTable('destroy').bootstrapTable('showLoading').bootstrapTable({
						data: data,
						formatLoadingMessage: function () {
							return '<span class="mhwLoading"><img src="css/img/Logo_Chevron_Animated.gif"/> Loading</span>';
						},
						columns: [
							[{
								field: 'productName',
								title: 'MHW Item Code',
								sortable: true,
								align: 'left'
							},
							{
								title: 'Item Description',
								field: 'itemDescription',
								align: 'left',
								valign: 'middle',
								sortable: true
							},
							{
								title: '# of Bottles Requested',
								field: 'quantity',
								align: 'left',
								valign: 'middle',
								sortable: true
							},
							{
								title: 'Stock UOM',
								field: 'stockUom',
								align: 'left',
								valign: 'middle',
								sortable: true
							}]
						]
					}); //sub table init
				}); //POST
			}



			function tableBuilder (qrytype) {
				var data;
				var clientlist = $("#clientlist").val();
				if(debug) console.log(`tableBuilder: clientlist = ${clientlist} Query type -> ${qrytype}`);
				var val1 = '<?php echo $_GET["ok"]; ?>';
				$('#clientSampleTable').bootstrapTable('showLoading');
				$.post('query-request-samples.php', { qrytype: qrytype, clientlist: clientlist }, function(dataPT) { // ajax request query-request.php, send qrytype & client as post variable, return dataX variable, parse into JSON
					try {
						data = JSON.parse(dataPT);
					}
					catch(e) {
						console.error(`tableBuilder: Could not parse query-request-samples.php with query type -> ${qrytype} data -> ${dataPT}`);
						console.error(`tableBuilder: e -> ${e}`);
						return;
					}

					var winH = $(window).height();
					var navH = $(".navbar").height();
					var tblH = (winH-navH)-50;


					//product/supplier views
					$('#clientSampleTable').bootstrapTable('destroy').bootstrapTable('showLoading').bootstrapTable({
						data: data,
						height:tblH,
						stickyHeader:true,
						stickyHeaderOffsetY:60,
						formatLoadingMessage: function () {
							return '<span class="mhwLoading"><img src="css/img/Logo_Chevron_Animated.gif"/> Loading</span>';
						},
						detailView: true,
						onExpandRow: function (index, row, $detail) {
							expandTable($detail,row)
						},
						columns: [
							[
							{
								field: 'sampleOrderNo',
								title: 'Sample Order #',
								align: 'center',
								valign: 'middle',
								sortable: true
							},
							{
								field: 'clientName',
								title: 'Client Name',
								align: 'center',
								valign: 'middle',
								sortable: true
							},
							{
								field: 'submittedBy',
								title: 'Submitted By',
								align: 'center',
								valign: 'middle',
								sortable: true
							},
							{
								field: 'orderDate',
								title: 'Sample Delivery Date',
								align: 'center',
								valign: 'middle',
								sortable: true
							},
							{
								field: 'eventDate',
								title: 'Event Date',
								align: 'left',
								valign: 'middle',
								sortable: true
							},
							{
								field: 'createdDate',
								title: 'Created Date',
								align: 'left',
								valign: 'middle',
								sortable: true
							},
							{
								field: 'eventDetails',
								title: 'Event Details',
								align: 'center',
								valign: 'middle',
								sortable: true
							},
							{
								field: 'deliveryDetails',
								title: 'Delivery/Pickup',
								align: 'center',
								valign: 'middle',
								sortable: true
							},
							{
								field: 'location',
								title: 'Location',
								align: 'center',
								valign: 'middle',
								sortable: true
							},
							{
								field: 'useOfSamples',
								title: 'Use of Samples',
								align: 'center',
								valign: 'middle',
								sortable: true
							},
							{
								field: 'processed',
								title: 'Status',
								align: 'center',
								valign: 'middle',
								sortable: true
							}]
						]
					}); //table init


					var viewname_raw = $("#screenview_name").val();
					if(viewname_raw=="Not Finalized") {

						$table.bootstrapTable('checkAll');  //check all Not Finalized by default
					}

					var orderkey = '<?php echo $ok; ?>';
					if(orderkey && orderkey!=''){
						$table.bootstrapTable('expandAllRows');
					}
					//expand-row.bs.table

					modalStuff(); //bind modal & buttons

					$table.on('check.bs.table uncheck.bs.table ' + 'check-all.bs.table uncheck-all.bs.table', function () {
						$remove.prop('disabled', !$table.bootstrapTable('getSelections').length)

						// save your data, here just save the current page
						selections = getIdSelections()
						// push or splice the selections if you want to save all data selections

					})
					$table.on('all.bs.table', function (e, name, args) {
						console.log(name, args)
						modalStuff(); //when anything happens (filter, click, etc) re-bind modal & buttons
					})
					$remove.click(function () {
						var ids = getIdSelections()
						$table.bootstrapTable('remove', {
							field: 'id',
							values: ids
						})
						$remove.prop('disabled', true)
					})

				}); //POST

			} //unnamed function wrapping table init

			var initialview = '<?php echo $v; ?>';
			tableBuilder("client_sample_list"); //initialize table
			$("#screenview_name").on("change", function () {
				var viewname_raw =  $(this).val();
				$("#finalize").hide();
				tableBuilder(viewname);
			});

			$("#btn_toggle_sub1").on("click", function () {
				$table.bootstrapTable('expandAllRows');
			});
			$("#btn_toggle_sub2").on("click", function () {
				$table.bootstrapTable('collapseAllRows');
			});


		});  //document ready
		</script>
<div class="container-fluid">
<div class="row justify-content-md-left float-left">
		<div class="col-md-auto oneField field-container-D" id="client_name-D">
			<label id="client_name-L" class="label preField " for="client_name"><b>Client Name</b></label>
			<div class="inputWrapper">
				<select id="client_name" name="client_name" title="Client Name" aria-required="true">
				<?php
				$clients = explode(";",$_SESSION['mhwltdphp_userclients']);
				if (count($clients) > 1) {
					echo "<option value='all' class=''>ALL</option>";
				}
				foreach ($clients as &$clientvalue) {
					echo "<option value='$clientvalue' class=''>$clientvalue</option>";
			}
			?>
		</select>
	</div>
			</div>
			<div class="before-table-header">
				<div class="row justify-content-md-center">
					<div class="col-md-auto align-self-center oneField field-container-D" id="client_samples_link">
							<div style="font-size: 16.6px;padding:20px;"><b><u><a href="/forms/mhw_event_form.php">Create</u></b></a> new event notification and sample order.</div>
					</div>
				</div>
			</div>
		</div>
<?php
if($param1 && $param1!==''){
	?>
	<div class="col-md-auto oneField field-container-D" id="client_name-D">
		<label id="client_name-L" class="label preField " for="client_name"><b>Order Filter: <?php echo $param1; ?></b></label>
		<div class="inputWrapper">
			<a href='orderreview.php'>clear</a>
		</div>
	</div>
	<?php
}
else{
	?>
	<div class="toolbar btn-group">
		<button id="btn_toggle_sub1" class="btn btn-secondary"><i class="fas fa-expand"></i> Expand All</button>
		<button id="btn_toggle_sub2" class="btn btn-secondary"><i class="fas fa-compress"></i> Collapse All</button>
	</div>
	<?php
}
?>

</div>
</div>
<table id="clientSampleTable" class="table table-hover" data-toggle="table" data-pagination="true" data-show-pagination-switch="true" data-page-size="25" data-show-columns="true" data-show-toggle="true" data-search="true" data-show-export="true" data-toolbar=".toolbar" data-toolbar-align="right">


	<?php
	$clientlist =  "'".str_replace(";","','",trim($_SESSION['mhwltdphp_userclientcodes']))."'";


	sqlsrv_free_stmt($getResults);

	sqlsrv_close($conn);

	//20211215DS - removed to allow client access
	//}
}
?>
</tbody>
</table>

<input type="hidden" id="clientlist" value="<?php echo  $clientlist; ?>">

<div class="modal modal-wide fade" id="fileModalx" tabindex="-1" role="dialog" aria-labelledby="fileModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="fileModalLabel">Files</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

			</div>
			<div id='preview'></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<script>

$(document).ready(function(){


	//initialize prod image modal functionality
	$('.prodimg_btn').on('click',function(){
		var productid = $(this).data('product');
		var imageurl = 'viewfiles.php?pid='+productid;
		//alert(imageurl);
		$('.modal-body').load(imageurl,function(){
			$('#fileModalx').modal({show:true});
		});
	});
	//initialize prod image download functionality for outside of modal
	$('.dwnld').on('click', function () {
		var imagefile = $(this).data('url');
		var imagename = $(this).data('imagename');
		$.ajax({
			url: imagefile,
			method: 'GET',
			xhrFields: {
				responseType: 'blob'
			},
			success: function (data) {
				var a = document.createElement('a');
				var url = window.URL.createObjectURL(data);
				a.href = url;
				a.download = imagename;
				a.click();
				window.URL.revokeObjectURL(url);
			}
		});
	});
});

</script>

</div>
</div>
</body>
</html>
