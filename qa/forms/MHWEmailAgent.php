<?php

/**
 * This example shows settings to use when sending via Google's Gmail servers.
 * This uses traditional id & password authentication - look at the gmail_xoauth.phps
 * example to see how to use XOAUTH2.
 * The IMAP section shows how to save this message to the 'Sent Mail' folder using IMAP commands.
 */


include("settings.php");

//Import PHPMailer classes into the global namespace
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;



require_once("PHPMailer/PHPMailer/PHPMailer.php");
require_once("PHPMailer/PHPMailer/SMTP.php");
require_once("PHPMailer/PHPMailer/Exception.php");


//$fromAddress = "notifications@mhwltd.com";
//$fromPassword =  "e8!BG3hG"; 

function getEmailTemplate($text, $table) {
  return('<!doctype html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Simple Transactional Email</title>
    <style>
      /* -------------------------------------
          GLOBAL RESETS
      ------------------------------------- */

      /*All the styling goes here*/

      img {
        border: none;
        -ms-interpolation-mode: bicubic;
        max-width: 100%;
      }

      body {
        background-color: #f6f6f6;
        font-family: sans-serif;
        -webkit-font-smoothing: antialiased;
        font-size: 14px;
        line-height: 1.4;
        margin: 0;
        padding: 0;
        -ms-text-size-adjust: 100%;
        -webkit-text-size-adjust: 100%;
      }

      table {
        border-collapse: separate;
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
        width: 100%; }
        table td {
          font-family: sans-serif;
          font-size: 14px;
          vertical-align: top;
      }

      /* -------------------------------------
          BODY & CONTAINER
      ------------------------------------- */

      .body {
        background-color: #f6f6f6;
        width: 100%;
      }

      /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
      .container {
        display: block;
        margin: 0 auto !important;
        /* makes it centered */
        max-width: 580px;
        padding: 10px;
        width: 580px;
      }

      /* This should also be a block element, so that it will fill 100% of the .container */
      .content {
        box-sizing: border-box;
        display: block;
        margin: 0 auto;
        max-width: 580px;
        padding: 10px;
      }

      /* -------------------------------------
          HEADER, FOOTER, MAIN
      ------------------------------------- */
      .main {
        background: #ffffff;
        border-radius: 3px;
        width: 100%;
      }

      .wrapper {
        box-sizing: border-box;
        padding: 20px;
      }

      .content-block {
        padding-bottom: 10px;
        padding-top: 10px;
      }

      .footer {
        clear: both;
        margin-top: 10px;
        text-align: center;
        width: 100%;
      }
        .footer td,
        .footer p,
        .footer span,
        .footer a {
          color: #999999;
          font-size: 12px;
          text-align: center;
      }

      /* -------------------------------------
          TYPOGRAPHY
      ------------------------------------- */
      h1,
      h2,
      h3,
      h4 {
        color: #000000;
        font-family: sans-serif;
        font-weight: 400;
        line-height: 1.4;
        margin: 0;
        margin-bottom: 30px;
      }

      h1 {
        font-size: 35px;
        font-weight: 300;
        text-align: center;
        text-transform: capitalize;
      }

      p,
      ul,
      ol {
        font-family: sans-serif;
        font-size: 14px;
        font-weight: normal;
        margin: 0;
        margin-bottom: 15px;
      }
        p li,
        ul li,
        ol li {
          list-style-position: inside;
          margin-left: 5px;
      }

      a {
        color: #3498db;
        text-decoration: underline;
      }

      /* -------------------------------------
          BUTTONS
      ------------------------------------- */
      .btn {
        box-sizing: border-box;
        width: 100%; }
        .btn > tbody > tr > td {
          padding-bottom: 15px; }
        .btn table {
          width: auto;
      }
        .btn table td {
          background-color: #ffffff;
          border-radius: 5px;
          text-align: center;
      }
        .btn a {
          background-color: #ffffff;
          border: solid 1px #3498db;
          border-radius: 5px;
          box-sizing: border-box;
          color: #3498db;
          cursor: pointer;
          display: inline-block;
          font-size: 14px;
          font-weight: bold;
          margin: 0;
          padding: 12px 25px;
          text-decoration: none;
          text-transform: capitalize;
      }

      .btn-primary table td {
        background-color: #3498db;
      }

      .btn-primary a {
        background-color: #3498db;
        border-color: #3498db;
        color: #ffffff;
      }

      /* -------------------------------------
          OTHER STYLES THAT MIGHT BE USEFUL
      ------------------------------------- */
      .last {
        margin-bottom: 0;
      }

      .first {
        margin-top: 0;
      }

      .align-center {
        text-align: center;
      }

      .align-right {
        text-align: right;
      }

      .align-left {
        text-align: left;
      }

      .clear {
        clear: both;
      }

      .mt0 {
        margin-top: 0;
      }

      .mb0 {
        margin-bottom: 0;
      }

      .preheader {
        color: transparent;
        display: none;
        height: 0;
        max-height: 0;
        max-width: 0;
        opacity: 0;
        overflow: hidden;
        mso-hide: all;
        visibility: hidden;
        width: 0;
      }

      .powered-by a {
        text-decoration: none;
      }

      hr {
        border: 0;
        border-bottom: 1px solid #f6f6f6;
        margin: 20px 0;
      }

      /* -------------------------------------
          RESPONSIVE AND MOBILE FRIENDLY STYLES
      ------------------------------------- */
      @media only screen and (max-width: 620px) {
        table.body h1 {
          font-size: 28px !important;
          margin-bottom: 10px !important;
        }
        table.body p,
        table.body ul,
        table.body ol,
        table.body td,
        table.body span,
        table.body a {
          font-size: 16px !important;
        }
        table.body .wrapper,
        table.body .article {
          padding: 10px !important;
        }
        table.body .content {
          padding: 0 !important;
        }
        table.body .container {
          padding: 0 !important;
          width: 100% !important;
        }
        table.body .main {
          border-left-width: 0 !important;
          border-radius: 0 !important;
          border-right-width: 0 !important;
        }
        table.body .btn table {
          width: 100% !important;
        }
        table.body .btn a {
          width: 100% !important;
        }
        table.body .img-responsive {
          height: auto !important;
          max-width: 100% !important;
          width: auto !important;
        }
      }

      /* -------------------------------------
          PRESERVE THESE STYLES IN THE HEAD
      ------------------------------------- */
      @media all {
        .ExternalClass {
          width: 100%;
        }
        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
          line-height: 100%;
        }
        .apple-link a {
          color: inherit !important;
          font-family: inherit !important;
          font-size: inherit !important;
          font-weight: inherit !important;
          line-height: inherit !important;
          text-decoration: none !important;
        }
        #MessageViewBody a {
          color: inherit;
          text-decoration: none;
          font-size: inherit;
          font-family: inherit;
          font-weight: inherit;
          line-height: inherit;
        }
        .btn-primary table td:hover {
          background-color: #34495e !important;
        }
        .btn-primary a:hover {
          background-color: #34495e !important;
          border-color: #34495e !important;
        }
      }

    </style>
  </head>
  <body class="">
    <span class="preheader">This is preheader text. Some clients will show this text as a preview.</span>
    ' . $table . '
  </body>
</html>');
}


// Email formatting functions. Could be moved to a class

function generateComplianceEmailSubject($rushedOrder, $eventId, $sampleDate, $subject = null) {
  $emailSubject = null;
  if($rushedOrder) {
    $emailSubject = "*RUSHED* " . $subject;
  }
  $emailSubject = $subject . ": Event ID = " . $eventId . " Sample Date = " . $sampleDate;
  return($emailSubject);
}


function generateTableHTML($headings, $headingIndicies, $tableData) {
  $html = '<style> table, td, th, tr, tbody, th { border: 1px solid black; } table { width: 100%; border-collapse: collapse; } </style>';
  $html .= '<table align="left" border="1" cellspacing="0" cellpadding="0" width="1024" align="center" style="border:1px solid #cc;" class="MsoNormal">';
  $html .= '<th><tr>';
  for($i = 0; $i < sizeof($headings); $i++) {
    $html .= '<td class="MsoNormal"><strong>' . $headings[$i] . '</strong></td>';
  }
  $html .= '</tr></th><tbody>';
  for($i = 0; $i < sizeof($tableData); $i++) {
    $html .= '<tr class="MsoNormal">';
    for($k = 0; $k < sizeof($headingIndicies); $k++) {
      $html .= '<td class="MsoNormal">';
      for($l = 0; $l < sizeof($headingIndicies[$k]); $l++) {
        if(is_array($tableData[$i][$headingIndicies[$k][$l]])) {
          // echo("Found Items! -> " . print_r($tableData[$i][$headingIndicies[$k][$l]], true));
          for($m = 0; $m < sizeof($tableData[$i][$headingIndicies[$k][$l]]) ; $m++) {
            // echo("Found a row! -> " . print_r($tableData[$i][$headingIndicies[$k][$l]][$m], true));
            $html .= '<tr>';
            for($n = 0; $n < sizeof($tableData[$i][$headingIndicies[$k][$l]][$m]); $n++) {
              // echo("Found an Element! -> " . print_r($tableData[$i][$headingIndicies[$k][$l]][$m][$n], true) . "\n");
              if($m === 0) {
                $html .= '<td border=0 class="MsoNormal"><strong>' . $tableData[$i][$headingIndicies[$k][$l]][$m][$n] . '</strong></td>';
              }
              else {
                $html .= '<td class="MsoNormal">' . $tableData[$i][$headingIndicies[$k][$l]][$m][$n] . '</td>';
              }
            }
            $html .= '</tr>';
          }
        }
        else {
          $html .= $tableData[$i][$headingIndicies[$k][$l]];
        }
      }
      $html .= '</td>';
    }
    $html .= '</tr>';
  }

  $html .= '</tr></tbody></table><br />';
  return($html);
}




function generateComplianceEmailHTML($headings, $headingIndicies, $eventData, $itemData = []) {
  $message = '<table align="left" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #cc;">';
  $message .= '<th><tr>';
  for($i = 0; $i < sizeof($headings); $i++) {
    $message .= "<td><strong>" . $headings[$i] . "</strong></td>";
  }
  $message .= "</tr></th><tbody><tr>";
  for($i = 0; $i < sizeof($headingIndicies); $i++) {
    $message .= "<td>";
    for($j = 0; $j < sizeof($headingIndicies[$i]); $j++) {
      $messageLength = strlen($message);
      if($eventData[$headingIndicies[$i][$j]] != -1) {
        $message .= ($eventData[$headingIndicies[$i][$j]] === null) ? '' : $eventData[$headingIndicies[$i][$j]];
      }
      else {
        $message .= '-';
      }
      if(strlen($message) > $message && ($j < sizeof($headingIndicies[$i]))) {
        $message .= ' ';
        continue;
      }
      $message .= "</td>";
    }
  }
  $message .= "</tr>";
  for($k = 0; $k < sizeof($itemData); $k++) { // Item array row 1 should be the titles
    $message .= "<tr><td>+</td>"; // Indent by one column to match the web interface layout
    for($l = 0; $l < sizeof($itemData[$k]); $l++) {
      if($k === 0) {
        $message .= "<td><strong>" . $itemData[$k][$l] . "</strong></td>";
      }
      else {
        $message .= "<td>" . $itemData[$k][$l] . "</td>";
      }
    }
    $message .= "</tr>";
  }

  $message .= "</tr></tbody></table>";
  return($message);
}

function sendEmailReport($headings, $headingIndicies, $samples, $msgType, $send, $from, $to, $sbj, $siteroot, $fromPassword) {
  $emailAgent = new MHWEmailAgent();
  $emailAgent->setUsername();
  $emailAgent->setPassword($fromPassword);
  $emailAgent->setFrom($from);
  $msgSubject = "";
  $msgHeader  = "";
  $msgFooter  = "";

  switch($msgType) {
    case "SamplesOrdersReport":
	  $msgRecipient = $to;
      $msgSubject = $sbj;
	  $msgAlias = "Compliance Team";
      $msgHeader  = '<p class="MsoNormal">Following are the sample orders that have been approved by the Compliance team and are needed to be marked as Complete</p><br />';
      $msgFooter  = '<br /><br /><p class="MsoNormal">You can click on the following link to the CRD to view more details: <a href="' . $siteroot . '/client_samples_review.php">' . $siteroot . '/client_samples_review.php</a></p><br />';
      break;
    case "RushedOrdersReport":
	  $msgRecipient = $to;
      $msgSubject = $sbj;
	  $msgAlias = "Accounting Team";
      $msgHeader  = "";
      $msgFooter  = "";
      break;
    default:
      break;
  }
  $emailAgent->setSubject($msgSubject);
  $tableHTML =  generateTableHTML($headings, $headingIndicies, $samples);
  // print_r($tableHTML);
  //$from = "wilcoxdv@gmail.com";
  if($emailAgent->addRecipient($msgRecipient, $msgAlias)) {
    $emailAgent->setBody($msgHeader . $tableHTML . $msgFooter);
    $emailAgent->isHTML(true);
    if($emailAgent->sendEmail()) {
      echo($msgType . " Email Sent to " . $msgRecipient . ". Ok.\n");
    }
    else {
      error_log("Email failed to send with error " .  $emailAgent->getLastError());
    }
  }
  else {
    error_log("Could not set email recipient!");
  }
}


class MHWEmailAgent {
  private $mail = null;
  private $last_error = null;

  function __construct($host = 'smtp.gmail.com', $port = 465, $smtp_auth = true, $smtp_secure = PHPMailer::ENCRYPTION_SMTPS, $debug = 0) {

    $this->initPHPMailer($host, $port, $smtp_auth);

    //TODO: Remove these just for gmp_test
    if($this->mail->SMTPDebug == SMTP::DEBUG_SERVER) {
      $this->setUsername();
      $this->setPassword($fromPassword);
      $this->setFrom();
      $this->addRecipient();
      $this->setSubject();
      $this->setHTML();
      $this->setBody();
      $this->setAltBody();
    }
  }

  private function initPHPMailer($host = 'smtp.gmail.com', $port = 465, $smtp_auth = true, $smtp_secure = PHPMailer::ENCRYPTION_SMTPS, $debug = 0) {
    $this->mail = new PHPMailer();
    //Tell PHPMailer to use SMTP
    $this->mail->isSMTP();
    //Enable SMTP debugging
    //SMTP::DEBUG_OFF = off (for production use)
    //SMTP::DEBUG_CLIENT = client messages
    //SMTP::DEBUG_SERVER = client and server messages
    $this->mail->SMTPDebug = SMTP::DEBUG_OFF;
    //Set the hostname of the mail server
    $this->mail->Host = $host;
    //Use `$this->mail->Host = gethostbyname('smtp.gmail.com');`
    //if your network does not support SMTP over IPv6,
    //though this may cause issues with TLS

    //Set the SMTP port number:
    // - 465 for SMTP with implicit TLS, a.k.a. RFC8314 SMTPS or
    // - 587 for SMTP+STARTTLS
    $this->mail->Port = $port;

    //Set the encryption mechanism to use:
    // - SMTPS (implicit TLS on port 465) or
    // - STARTTLS (explicit TLS on port 587)
    $this->mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;

    //Whether to use SMTP authentication
    $this->mail->SMTPAuth = $smtp_auth;
    return(true);
  }

  public function setUsername($user_name = "notifications@mhwltd.com") {
    //Username to use for SMTP authentication - use full email address for gmail
    $this->mail->Username = $user_name;
    return(true);

  }
  //public function setPassword($password = "e8!BG3hG") {
  public function setPassword($password) {
    //Password to use for SMTP authentication
    $this->mail->Password = $password;
    return(true);
  }

  public function setFrom($from = "notifications@mhwltd.com") {
    //Set who the message is to be sent from
    //Note that with gmail you can only use your account address (same as `Username`)
    //or predefined aliases that you have configured within your account.
    //Do not use user-submitted addresses in here
    $this->mail->setFrom($from);
    return(true);
  }

  public function setReplyTo($reply_to = "notifications@mhwltd.com") {
    //Set an alternative reply-to address
    //This is a good place to put user-submitted addresses
    $this->mail->setFrom($reply_to);
    return(true);
  }

  public function addRecipient($to = null, $name = null) {
    if(isset($to) && isset($name)) {
      $this->mail->addAddress($to, $name);
      return(true);
    }
    return(false);
  }

  public function setSubject($subject = "WARNING: The email agent is in DEBUG TEST mode!") {
    $this->mail->Subject = $subject;
  }

  public function setBody($body = "WARNING: The email agent has not been configured correctly. And is in debug mode for testing.") {
    $this->mail->Body = $body;
  }

  public function setHTML($html = "<p><hr /><b>WARNING: The email agent has not been configured correctly. And is in debug mode for testing.</b><hr /></p>") {
    $this->mail->msgHTML = $html;
  }

  public function isHTML($html) {
    $this->mail->isHTML($html);
  }

  public function setAltBody($alt_body = "WARNING: The email agent has not been configured correctly. And is in debug mode for testing.") {
    $this->mail->AltBody = $alt_body;
  }

  public function sendEmail() {
    //TODO: Validate the email data is filled in here.
    if(!$this->mail->send()) {
      $this->last_error = $this->mail->ErrorInfo;
      return(false);
    }
    else {
      return(true);
    }

    $this->last_error = $e->getMessage();
    return(false);

  }

  public function getLastError() {
    return($this->last_error);
  }
}

// Test code if from CLI
// if(php_sapi_name() === 'cli') {
//   $mhwEmailAgent = new MHWEmailAgent();
//   if(!$mhwEmailAgent->sendEmail()) {
//     echo($mhwEmailAgent->getLastError());
//   }
//   else {
//     echo("Great Success!");
//     return(true);
//   }
// }
