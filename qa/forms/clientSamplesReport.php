<?php
  include("dbconnect.php");
  include("MHWEmailAgent.php");
  include("settings.php");


  $conn = sqlsrv_connect($serverName, $connectionOptions);
  if( $conn === false) {
	   die( print_r( sqlsrv_errors(), true));
  }


  $emailAgent = new MHWEmailAgent();
  // Compare this to the bottom query.
  $rushedOrdersQuery = "SELECT ef.event_id
         AS
         'Client Request Number For Backup',
         ef.clientname
         AS 'Client',
         '3.0000.01.4310.00.09'
         AS 'JE',
         Concat('3', '.', Trim(Cast(ci.clientdep AS NVARCHAR)), '.01.6310.00.00')
         AS
         'Client Account Number',
         CONVERT(varchar, DATEADD(HOUR, -4, ef.[created_date]), 120)
         AS 'Request Date',
         CONVERT(varchar, CONVERT(DATETIME, Getdate() at time zone (SELECT Current_timezone_id()) at time zone 'Eastern Standard Time'), 120)
         AS 'JE Date',
         '$150'
         AS 'Amount'
  FROM   (SELECT [event_id],
                 [clientname],
                 [rushedorder],
                 [processed],
                 [created_date],
				 [client_code]
          FROM   [dbo].[mhw_app_event_form]
                WHERE [rushedorder] = 1
                 AND Cast(CONVERT(DATETIME, [created_date] at time zone (SELECT Current_timezone_id()) at time zone 'Eastern Standard Time') AS
                                                                   DATETIME) > Dateadd(hour, 17, Cast(Cast(CONVERT(DATETIME, Getdate() at time zone (SELECT Current_timezone_id()) at time zone 'Eastern Standard Time') - 1
                                                                  AS
                                                                  DATE) AS
                                                             DATETIME))
                 AND Cast(CONVERT(DATETIME, [created_date] at time zone (SELECT Current_timezone_id()) at time zone 'Eastern Standard Time') AS
                                                                   DATETIME) <= Dateadd(hour, 17, Cast(Cast(CONVERT(DATETIME, Getdate() at time zone (SELECT Current_timezone_id()) at time zone 'Eastern Standard Time') AS
                                                                   DATE)
                                                              AS DATETIME)
                                       )) ef
         JOIN (SELECT [CLIENT],
                      [clientdep]
               FROM   [dbo].v_arclnt) ci
           ON ef.client_code = ci.[CLIENT]";


  $approvedOrderQuery = "SELECT
    e.[event_id] AS 'sampleOrderNo',
    e.[created_by] AS 'submittedBy',
    CONVERT(varchar(10), e.[eventdate], 20) AS 'eventDate',
    CONVERT(varchar(10), e.[samplerequestdate], 20) AS 'orderDate',
    CONVERT(varchar, DATEADD(HOUR, -4, e.[created_date]), 120) AS 'createdDate',
    e.[rushedorder] AS 'rushedOrder',
    e.[requesttype] + '<br />' + ISNULL(e.[eventname] + '<br />', '') + ISNULL(e.[eventdesc] + '<br />', '') + ISNULL(e.[address] + '<br />', '') + ISNULL(e.[address2] + '<br />', '') + ISNULL(e.[city] + '<br />', '') + ISNULL(e.[state] + '<br />', '') + ISNULL(e.[zip] + '<br />', '') + ISNULL(e.[country] + '<br />', '') +
      ISNULL(STUFF((SELECT N'<a href=".$siteroot."/".$upload_dir."temp/' + REPLACE(f.[fileName], ' ', '%20') + '>' + f.[filename] + '</a><br />' FROM [dbo].[mhw_event_file] f WHERE e.[event_id] = f.[event_id] FOR XMl PATH, TYPE).value(N'.[1]', N'nvarchar(max)'), 1, 0, '<br />'), '') + 
	 -- ISNULL(STUFF((SELECT N'<a href=".$siteroot."/query-request-samples.php?fileId=' + TRIM(STR(f.[id])) + '>' + f.[filename] + '</a><br />' FROM [dbo].[mhw_event_file] f WHERE e.[event_id] = f.[event_id] FOR XMl PATH, TYPE).value(N'.[1]', N'nvarchar(max)'), 1, 0, '<br />'), '') + 
	  CASE WHEN e.eventtype = 1 THEN 'Public' WHEN e.eventtype = 2 THEN 'Invitation Only' END  AS 'eventDetails',
    (ISNULL(e.[pickuptxt] + '<br />' , '') + ISNULL(e.[pickupdisttxt] + '<br />', '') + ISNULL(e.[pickupothertxt] + '<br />' , '') + ISNULL(e.[deliveryaddress]+ '<br />' , '') + ISNULL(e.[deliverycity] + '<br />', '') + ISNULL(e.[deliverystate] + '<br />', '') + ISNULL(e.[deliveryzip] + '<br />', '') + ISNULL(e.[deliverycountry] + '<br />', '')) AS 'location',
    e.[deliverytype] AS 'deliveryDetails',
    e.[delivery] AS 'Location',
    e.[sampleusage] AS 'useOfSamples',
    e.[eventstatesampleevent] AS 'eventStateSampleEvent',
    e.[eventdesc] AS 'eventDescription',
    e.[deliveryaddress] AS 'deliveryAddress',
    e.[deliveryaddress2] AS 'deliveryAddress2',
    e.[deliverycity] AS 'deliveryCity',
    e.[deliverystate] AS 'deliveryState',
    e.[deliveryzip] AS 'deliveryZip',
    e.[deliverycountry] AS 'deliveryCountry',
    e.[clientname] AS 'clientName',
    -- f.[id] AS 'attachmentId',
    e.[notes] AS 'notes',
    e.[processed] AS 'processed'
    -- fileName=STUFF((SELECT N'<a href=query-samples.php?fileId=' + TRIM(STR(f.[id])) + '>' + f.[filename] + '</a>' FROM [dbo].[mhw_event_file] f WHERE e.[event_id] = f.[event_id] FOR XMl PATH, TYPE).value(N'.[1]', N'nvarchar(max)'),	1, 0, '<br />')
  FROM
    [dbo].[mhw_app_event_form] e
  --LEFT JOIN
  --  [dbo].[mhw_event_file] f ON e.[event_id] = f.[event_id]
  WHERE
    e.[processed] = 'Approved'
  ORDER BY CASE WHEN e.[processed] = 'Approved' THEN 1 WHEN e.[processed] = 'Compliance Hold' THEN 2 WHEN e.[processed] = 'Completed' THEN 3 WHEN e.[processed] = 'Rejected' THEN 4 WHEN e.[processed] = 'Cancelled' THEN 5 END";

  $approvedOrderQueryByTeam = "SELECT
    e.[event_id] AS 'sampleOrderNo',
    e.[created_by] AS 'submittedBy',
    CONVERT(varchar(10), e.[eventdate], 20) AS 'eventDate',
    CONVERT(varchar(10), e.[samplerequestdate], 20) AS 'orderDate',
    CONVERT(varchar, DATEADD(HOUR, -4, e.[created_date]), 120) AS 'createdDate',
    e.[rushedorder] AS 'rushedOrder',
    e.[requesttype] + '<br />' + ISNULL(e.[eventname] + '<br />', '') + ISNULL(e.[eventdesc] + '<br />', '') + ISNULL(e.[address] + '<br />', '') + ISNULL(e.[address2] + '<br />', '') + ISNULL(e.[city] + '<br />', '') + ISNULL(e.[state] + '<br />', '') + ISNULL(e.[zip] + '<br />', '') + ISNULL(e.[country] + '<br />', '') +
      ISNULL(STUFF((SELECT N'<a href=".$siteroot."/".$upload_dir."temp/' +  REPLACE(f.[fileName], ' ', '%20') + '>' + f.[filename] + '</a><br />' FROM [dbo].[mhw_event_file] f WHERE e.[event_id] = f.[event_id] FOR XMl PATH, TYPE).value(N'.[1]', N'nvarchar(max)'), 1, 0, '<br />'), '') + 
	 -- ISNULL(STUFF((SELECT N'<a href=".$siteroot."/query-request-samples.php?fileId=' + TRIM(STR(f.[id])) + '>' + f.[filename] + '</a><br />' FROM [dbo].[mhw_event_file] f WHERE e.[event_id] = f.[event_id] FOR XMl PATH, TYPE).value(N'.[1]', N'nvarchar(max)'), 1, 0, '<br />'), '') + 
	  CASE WHEN e.eventtype = 1 THEN 'Public' WHEN e.eventtype = 2 THEN 'Invitation Only' END  AS 'eventDetails',
    (ISNULL(e.[pickuptxt] + '<br />' , '') + ISNULL(e.[pickupdisttxt] + '<br />', '') + ISNULL(e.[pickupothertxt] + '<br />' , '') + ISNULL(e.[deliveryaddress]+ '<br />' , '') + ISNULL(e.[deliverycity] + '<br />', '') + ISNULL(e.[deliverystate] + '<br />', '') + ISNULL(e.[deliveryzip] + '<br />', '') + ISNULL(e.[deliverycountry] + '<br />', '')) AS 'location',
    e.[deliverytype] AS 'deliveryDetails',
    e.[delivery] AS 'Location',
    e.[sampleusage] AS 'useOfSamples',
    e.[eventstatesampleevent] AS 'eventStateSampleEvent',
    e.[eventdesc] AS 'eventDescription',
    e.[deliveryaddress] AS 'deliveryAddress',
    e.[deliveryaddress2] AS 'deliveryAddress2',
    e.[deliverycity] AS 'deliveryCity',
    e.[deliverystate] AS 'deliveryState',
    e.[deliveryzip] AS 'deliveryZip',
    e.[deliverycountry] AS 'deliveryCountry',
    e.[clientname] AS 'clientName',
    e.[notes] AS 'notes',
    e.[processed] AS 'processed'
  FROM
    [dbo].[mhw_app_event_form] e
  WHERE
    e.[processed] = 'Approved'
	AND e.client_code IN (SELECT t.[Client ID] FROM [tray_email_routing] AS t WHERE ISNULL(t.[Compliance Ops Email],'TBD') = '{TEAM_EMAIL_REPLACE}' )
  ORDER BY CASE WHEN e.[processed] = 'Approved' THEN 1 WHEN e.[processed] = 'Compliance Hold' THEN 2 WHEN e.[processed] = 'Completed' THEN 3 WHEN e.[processed] = 'Rejected' THEN 4 WHEN e.[processed] = 'Cancelled' THEN 5 END";

  function getItemQuery($c, $eventId) {
    $query = "SELECT
       '-', i.[item_mhw_code] AS 'productName', i.[item_description] AS 'itemDescription', p.[quantity] AS 'quantity', i.[stock_uom] AS 'stockUom'
     FROM
       [dbo].[mhw_event_product] p
     LEFT JOIN
       [dbo].[mhw_app_prod_item] i
     ON
       i.[item_id] = p.[item_id]
     WHERE p.[event_id] = $eventId";

     $r = sqlsrv_query($c, $query);
     $items = array();
     do {
        try {
        while ($row = sqlsrv_fetch_array($r, SQLSRV_FETCH_ASSOC)) {
          $items[] = array_values(utf8ize($row));
        }
        }
        catch(Exception $e) {
          echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
     } while(sqlsrv_next_result($r));
     sqlsrv_free_stmt($r);
     array_unshift($items, array("+", "Item Code", "Item Description", "# of Bottles Requested", "Stock UOM"));
     return($items);
  }

   function getTeamToAlertQuery($c) {
    $query = "SELECT DISTINCT ISNULL(t.[Compliance Ops Email],'TBD') as [TeamInbox] FROM [dbo].[mhw_app_event_form] e
			LEFT OUTER JOIN [tray_email_routing] AS t on t.[Client ID] = e.[client_code]
			WHERE e.[processed] = 'Approved'";   

     $r = sqlsrv_query($c, $query);
     $teams = array();

     do {
        try {
          while($row = sqlsrv_fetch_array($r, SQLSRV_FETCH_ASSOC)) {
           // $teams[] = array_values(utf8ize($row));
		   $teams[] = utf8ize($row);
          }
        }
        catch(Exception $e) {
          echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
     } while(sqlsrv_next_result($r));
     sqlsrv_free_stmt($r);
     //array_unshift($teams, array("TeamInbox"));   //X
     return($teams);
  }

  $debug = true;

  function utf8ize($d) {
  	if (is_array($d)) {
  		foreach ($d as $k => $v) {
  			$d[$k] = utf8ize($v);
  		}
  	} else if (is_string ($d)) {
  		return utf8_encode($d);
  	}
  	return $d;
  }


  function getSamples($conn, $tsql, $doItemQuery) {
    $approvedOrderQuery = preg_replace('/\t/', '', $tsql);
    $orderResults = sqlsrv_query($conn, $tsql);

  $sqlErrors = "";
  if ($orderResults === false) {
  	$sqlErrors = sqlsrv_errors();
  	echo(print_r($sqlErrors, true));
    exit(-1);
  }
  $samples = array();
  do {
    $i = 0;
     while ($row = sqlsrv_fetch_array($orderResults, SQLSRV_FETCH_ASSOC)) {
         if($doItemQuery) {
           try {
       $row['items'] = getItemQuery($conn, $row['sampleOrderNo']);
           }
           catch(Exception $e) {
             echo 'Caught exception: ',  $e->getMessage(), "\n";
           }
         }
       $samples[] = array_values(utf8ize($row));
     }
  } while ( sqlsrv_next_result($orderResults) );
    sqlsrv_free_stmt($orderResults);
    return $samples;
  }

/*
  $samples = getSamples($conn, $approvedOrderQuery, true);

  $headings        = array("Sample Order #", "Client Name", "Submitted By", "Sample Date", "Event Date", "Created Date", "Event Details",  "Delivery/Pickup", "Location",  "Use of Samples", "Review Remarks", "Rushed Order", "Status");
  $headingIndicies = array(array(0),         array(19),       array(1),      array(3),   array(2), array(4), array(6),         array(8),          array(7),    array(10),        array(20),        array(5),       array(21));
  // print_r($samples);
  // $emailHTML = getEmailTemplate(null, $eventTable);
  $emailAgent->setUsername($fromAddress);
  $emailAgent->setPassword();
  $emailAgent->setFrom($fromAddress);
  $msgSubject = $ScheduledEmailSubject;
  $msgHeader  = '<p class="MsoNormal">Following are the sample orders that have been approved by the Compliance team and are needed to be marked as Complete</p><br />';
  $msgFooter  = '<br /><br /><p class="MsoNormal">You can click on the following link to the CRD to view more details: <a href="' . $siteroot . '/client_samples_review.php">' . $siteroot . '/client_samples_review.php</a></p><br />';
  $emailAgent->setSubject($msgSubject);
  $sendComplianceEmail = false;
  if($emailAgent->addRecipient($NewOrderComplianceEmailTo, "Compliance Team")) {
    $emailAgent->setBody($msgHeader . generateTableHTML($headings, $headingIndicies, $samples) . $msgFooter);
    $emailAgent->isHTML(true);
    if($emailAgent->sendEmail()) {
      echo("Samples Order Report to Email Sent to " . $NewOrderComplianceEmailTo . ". Ok.\n");
    }
    else {
      error_log("Email failed to send with error " .  $emailAgent->getLastError());
    }
  }
  else {
    error_log("Could not set email recipient!");
  }
*/
/**********/
  $samples = getSamples($conn, $approvedOrderQuery, true);
  $headings = array("Sample Order #", "Client Name", "Submitted By", "Sample Date", "Event Date", "Created Date", "Event Details",  "Delivery/Pickup", "Location",  "Use of Samples", "Review Remarks", "Rushed Order", "Status");
  $headingIndicies = array(array(0),         array(19),       array(1),      array(3),   array(2), array(4), array(6),         array(8),          array(7),    array(10),        array(20),        array(5),       array(21));
  sendEmailReport($headings, $headingIndicies, $samples, "SamplesOrdersReport", true, $fromAddress, $NewOrderComplianceEmailTo, $ScheduledEmailSubject, $siteroot, $fromPassword);

  $teamsMail = getTeamToAlertQuery($conn);
  for($k = 0; $k < sizeof($teamsMail); $k++) {
	$thisTeam = $teamsMail[$k]['TeamInbox']; 
	$samplesTeam = getSamples($conn, str_replace("{TEAM_EMAIL_REPLACE}",$thisTeam,$approvedOrderQueryByTeam), true);
	$headingsTeam = array("Sample Order #", "Client Name", "Submitted By", "Sample Date", "Event Date", "Created Date", "Event Details",  "Delivery/Pickup", "Location",  "Use of Samples", "Review Remarks", "Rushed Order", "Status");
	$headingIndiciesTeam = array(array(0),         array(19),       array(1),      array(3),   array(2), array(4), array(6),         array(8),          array(7),    array(10),        array(20),        array(5),       array(21));

	//after samples for blank/TBD team is collected, update to default email
	if($thisTeam === "TBD" ) {
		$thisTeam = $defaultOpsEmail; 
	}
	else if($thisTeam === "") {
		$thisTeam = $defaultOpsEmail;
	}
	sendEmailReport($headingsTeam, $headingIndiciesTeam, $samplesTeam, "SamplesOrdersReport", true, $fromAddress, $thisTeam, $ScheduledEmailSubject, $siteroot, $fromPassword);
  }
/*********/

  $samples = getSamples($conn, $rushedOrdersQuery, false);
  // print_r($samples);
  $headingsRushedOrders = array("Client Request Number For Backup", "Client", "JE", "Client Account Number", "Request Date", "JE Date", "Amount");
  $headingIndiciesRushedOrders = array(array(0),                     array(1),       array(2),                array(3),      array(4), array(5), array(6));
  // $approvedOrderResults = generateTableHTML($headingsApprovedOrders, $headingIndiciesApprovedOrders, $samples);

  sendEmailReport($headingsRushedOrders, $headingIndiciesRushedOrders, $samples, "RushedOrdersReport", true, $fromAddress, $RushedOrderEmailTo, $RushedOrderEmailSubject, $siteroot, $fromPassword);

  // sqlsrv_free_stmt($orderResults);
  sqlsrv_close($conn);





































?>
