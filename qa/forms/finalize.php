<?php

session_start();
if(!isset($_SESSION['mhwltdphp_user'])){
	die( "Not authenticated !" );  
}

error_reporting(E_ALL); //displays an error
include("dbconnect.php");

//Establishes the connection
$conn = sqlsrv_connect($serverName, $connectionOptions);
if( $conn === false) {
    die( print_r( sqlsrv_errors(), true));
}

$user = $_SESSION['mhwltdphp_user'];
$tableName = $_REQUEST['view_state'];
if (!in_array($tableName, ['tfa_ca','tfa_ct','tfa_ma','tfa_mn','tfa_nj','tfa_nyr','tfa_nyw'])) {
    $tableName = 'tfa_os';
}

if (!is_array($_REQUEST['selected'])) {
    die( "Wrong input data!" );
}

$rows_ids = str_replace("'","",implode(",", array_keys($_REQUEST['selected'])));
//echo '<pre>'; print_r($_REQUEST); exit;

$tsql = "UPDATE $tableName SET [status] = 'finalized' WHERE [user] = ? AND ID in ($rows_ids)";

$stmt = sqlsrv_prepare($conn, $tsql, array($user));  

if( $stmt === false )  
{  
    echo "Statement could not be prepared.\n";  
    die( print_r( sqlsrv_errors(), true));  
}  

if( sqlsrv_execute($stmt) === false )  
{  
    echo "Statement could not be executed.\n";  
    die( print_r( sqlsrv_errors(), true));  
}  

$json = array(
    'result' => sqlsrv_rows_affected ( $stmt ),
    'sql' => $tsql
);
header('Content-Type: application/json');
echo json_encode($json);

/* Free statement and connection resources. */  
sqlsrv_free_stmt( $stmt);  
sqlsrv_close( $conn); 

?>