$(document).ready(function(){1
  var debug = true;
    var allowedStates = ["NY", "NJ", "CA", "FL"];
  var allowsubmit = false;
  var focussed = document.activeElement;
  var itemsAdded = 0;
  tfaItemDescriptionHide();

  // function checkPageFocus() {
  //   if (document.hasFocus()) {
  //     enableSubmitButton(false);
  //   }
  //
  // }
  // setInterval(checkPageFocus, 300);
  // Hide the form elements and reveal as the flow occurs.

  function clearFormVariables() {
            $("[name='samplerequestdate']").val("");
            $("[name='samplesolicitordate']").val("");
            $("[name='eventdate']").val("");
            $("[name='eventdate2']").val("");
            $("[name='sampledate2']").val("");
            $("[name='eventstateeventonly']").val("");
            $("[name='eventstatesampleevent']").val("");
            $("[name='othertxt']").val("");
    $('#AddNewProduct').hide();
    $("#itemvalid").hide();

    $("[name='product']").val("");
    $("[name='ItemDescription']").val("");
    $("[name='ItemCode']").val("");
    $("[name='StockUOM']").val("");
    $("[name='quantity']").val("");
  }

  Section2Hide();
            Section3Hide();
  Section4Hide();
            Section5Hide();
  $("#otherwarning").hide();
  $("#submit_button").hide();
  $('#AddNewProduct').hide();
  $('input[name=requesttype]').change(function() {
    clearFormVariables();

    Section2Hide();
    Section3Hide();
    Section4Hide();
    Section5Hide();
    // RenumberRemoveSections();
            SameAddressSectionHide(); 
            SpecificAddressSectionShow();
  
    $("#otherwarning").hide();
    $("#submit_button").hide();
    // hack to remove items...
    for(let i = 0; i < 64; i++) {
      $("#remove-btn").click();
    }
    itemsAdded = 0;
    var value = $( 'input[name=requesttype]:checked' ).val();
            $("[name='samplerequestdate']").prop("required", false);
    $("[name='samplesolicitordate']").prop("required", false);
            $("[name='eventdate']").prop("required", false);
            $("[name='eventdate2']").prop("required", false);
            $("[name='sampledate2']").prop("required", false);
            $("[name='eventstateeventonly']").prop("required", false);
            $("[name='eventstatesampleevent']").prop("required", false);
            $("[name='othertxt']").prop("required", false); 

    ClearSection3Delivery();
    clearItemSelection();
   
    if (value === 'This is a sample request for a distributor') {
      clearFormVariables();
      ClearSection3Delivery();
      $("[name='samplerequestdate']").prop("required", true);
      $("#Section4").text("3.");
      $('#alerts').hide();
      jQuery('input[id=deliverytypelocation]:first').hide();
      jQuery('label[id=same-address-txt]').hide();
    } else if (value === 'This is a sample request for a licensed solicitor to use for account visits') {
      clearFormVariables();
      ClearSection3Delivery();
      $("[name='samplesolicitordate']").prop("required", true);
            $('#alerts').hide();
      $("#Section4").text("3.");
      jQuery('input[id=deliverytypelocation]:first').hide();
      jQuery('label[id=same-address-txt]').hide();
    } else if (value === 'This is a SAMPLE request and EVENT notification') {
            $("[name='eventdate2']").prop("required", true);
            $("[name='sampledate2']").prop("required", true);
            $("[name='eventstatesampleevent']").prop("required", true);

      $("#Section4").text("4.");
      $("#Section5").text("5.");
            $('#alerts').hide();

    } else if (value === "This is an EVENT only notification") {
      clearFormVariables();
      ClearSection3Delivery();
            $("[name='eventdate']").prop("required", true);
            $("[name='eventstateeventonly']").prop("required", true);
      $("#whosection").hide();
      $("#Section4").text("4.");
      $("#Section5").text("5.");
            RenumberAddSections(); 
    }

  });
            
  $('#ProductSetupLink').click(function(){
    $('#product_id').val($('#prodID').val());
    window.location.href = "productsetup.php?product_id=" + $('#prodID').val();
  });

  // To clear the delivery address was a pain, need to catch the radio event change for the delivery type and if its switched to
  // pickup clear the form. T
            
  $('input:radio[name="delivery"]').change(function() {
    console.info(`Radio button Changed! deliverytypelocation -> ${$(this).val() }`);
    if($(this).val() == 'Distributor') {
      $("pickupdistlocation").prop("required, true");
    }
    else if($(this).val() == 'Specific Address') {
      $("#deliveryaddress").prop("required", true);
      $("#deliveryaddress2").prop("required", true);
      $("#deliverycity").prop("required", true);
      $("#deliverystate").prop("required", true);
      $("#deliveryzip").prop("required", true);
      $("#deliverycountry").prop("required", true);
    }
  });

  $('input:radio[name="deliverytype"]').change(function() {
    console.info(`Radio button Changed! Delivery Type -> ${$(this).val() }`);
    if ($(this).val() == 'Pickup') {
      $('#deliveryaddress').val($('#address').val());
      $('#deliveryaddress2').val($('#address2').val());
      $('#deliverycity').val($('#city').val());
      $('#deliveryzip').val($('#zip').val());
      $('#deliverystate').val($('#state').val());
      $('#deliverycountry').val($('#country').val());
      $('#pickupdisttxt').val($('#address2').val());
      $("#deliveryaddress").prop("required", false);
      $("#deliveryaddress2").prop("required", false);
      $("#deliverycity").prop("required", false);
      $("#deliverystate").prop("required", false);
      $("#deliveryzip").prop("required", false);
      $("#deliverycountry").prop("required", false);
    }
  });

  // Reveal Section 2 whenAddNewProductSection 1 is complete.
  $("[name=samplerequestdate]").change(function() {
    $("#Section2").show();

  });
  $("[name=samplesolicitordate]").change(function() {
    $("#Section2").show();
    $("#Section4").text("3.");
  });
  $("[name='eventstatesampleevent']").change(function() {
    $("#Section2").show();
    $("#Section3").show();
  });
  $("[name='eventstateeventonly']").change(function () {
    $("#Section2").show();
    $("#Section3").show();
    });

    $('[name=country]').change(function(){
        var countryStr = $('[name=country]').val();
        if ((countryStr.toLowerCase() !== 'United States'.toLowerCase()) && (countryStr.toLowerCase() !== 'US'.toLowerCase())) { 
            $('[name=state]').val("XX");
        } 
      });

      $('[name=deliverycountry]').change(function(){
        var countryStr = $('[name=deliverycountry]').val();
        if ((countryStr.toLowerCase() !== 'United States'.toLowerCase()) && (countryStr.toLowerCase() !== 'US'.toLowerCase())) { 
            $('[name=deliverystate]').val("XX");
        } 
      });

      
    $('[name=state]').change(function(){
        var countryStr = $('[name=country]').val();
        if ((countryStr.toLowerCase() !== 'United States'.toLowerCase()) && (countryStr.toLowerCase() !== 'US'.toLowerCase()) && (countryStr !== '')) { 
            $('[name=state]').val("XX");
        } 
      });

      $('[name=deliverystate]').change(function(){
        var countryStr = $('[name=deliverycountry]').val();
        if ((countryStr.toLowerCase() !== 'United States'.toLowerCase()) && (countryStr.toLowerCase() !== 'US'.toLowerCase()) && (countryStr !== '')) { 
            $('[name=deliverystate]').val("XX");
        } 
      });

    $("#tfa_product_desc").change(function(){
    clearItemSelection();
        var eventNotificationType = $( 'input[name=requesttype]:checked' ).val();
        var prodID = $('#prodID').val();

           if ($(this).val() === "") {
                $("#AddNewProduct").prop('disabled', true);
    }
    else if($(this).val() !== "" &&  (typeof eventNotificationType == "undefined")) {
                $("#AddNewProduct").prop('disabled', true);
      tfaItemDescriptionHide();
           } else if (prodID == '') { 
                $("#AddNewProduct").prop('disabled', true);
      tfaItemDescriptionHide();
           } else {
             $("#AddNewProduct").prop('disabled', false);
      $('#AddNewProduct').hide();
      if(getProductItems($('#prodID').val())) {
        tfaItemDescriptionShow();
      }
    }
  });

  $("#tfa_item_desc").change(function(){
    var eventNotificationType = $( 'input[name=requesttype]:checked' ).val();

    if ($(this).val() === "") {
      $("#AddNewItem").prop('disabled', true);
      getProductItems($('#prodID').val());
    }
    else if($(this).val() !== "" &&  (typeof eventNotificationType == "undefined")) {
      $("#AddNewItem").prop('disabled', true);
      tfaItemDescriptionHide();
    }
    else if (itemID == '') {
      $("#AddNewItem").prop('disabled', true);
      tfaItemDescriptionHide();
    }
    else{
      $("#AddNewItem").prop('disabled', false);
        }
      });

  $("#eventform").on('click', '#remove-btn', function () {
    itemsAdded--;
    const itemArray = document.getElementsByName("item_id");
    console.info(`There are ${itemArray.length} items in the form...\n ${JSON.stringify($(this).parents('.items').children(), 0, 2)}`);
    $(this).parents('.items').remove();
    if(itemsAdded === 0) {
      Section4Hide();
      $("#submit_button").hide();
    }
    return(false);
  });

      $("#AddNewProduct").click(function(){
    itemsAdded++;
    $("#quantity").attr("value", 1);// Automatically set the value to one
        var eventNotificationType = $( 'input[name=requesttype]:checked' ).val();
    if (eventNotificationType === "This is an EVENT only notification") {
            $('#alerts').show();
            $('#alerts').hide();
        }

        var currentRow= $('.items').last();
    var quantity = currentRow.find('[id^=quantity]');
        if(eventNotificationType == "This is a SAMPLE request and EVENT notification" || eventNotificationType == "This is an EVENT only notification") {
          Section3Show();
          Section4Show();
          Section5Show();
          $("#submit_button").show();
        }
        else {
          Section4Show();
        }
    $("#submit_button").show();
    $("#ItemListing").hide();
    $("#ItemListing").show();
    $("#ItemValid").hide();
    $("#tfa_item_desc-D").hide();
    $("#tfa_item_desc").parent().find(".ac_button").remove();  //clear elements of previous instance
    // $("#tfa_item_desc").parent().find(".ac_result_area").remove();  //clear elements of previous instance\
    // Activate the submit button once a quanity has been seleted. This

    // quantity.change(function (event) {
    //   if ($(this).val() > 0 ) {
    //     console.log(`eventNotificationType -> ${eventNotificationType}`);
    //     if(eventNotificationType == "This is a SAMPLE request and EVENT notification" || eventNotificationType == "This is an EVENT only notification") {
    //       Section3Show();
    //       Section4Show();
    //       Section5Show();
    //       $("#submit_button").show();
    //     }
    //     else {
    //       Section4Show();
    //     }
    //     $(this).prop("required", true);
    //     $('#quantity').prop("required", false);
    //   }
    //   else {
    //     $(this).prop("required", true);
    //     $('#quantity').prop("required", false);
    //     $('#quantity').val("");
    //   }
    //
    // });
    Section4Show();
      });

      $('input[name=deliverytype]').change(function(){
        $('input[name="delivery"]').prop('checked', false);
        $('input[name="pickuptype"]').prop('checked', false);
    // ClearSection3Delivery();
    SpecificAddressSectionShow();
        var value = $( 'input[name=deliverytype]:checked' ).val();
        if (value === "delivery") {
            $("[name='delivery']").prop("required", true);
            $("[name='pickuptype']").prop("required", false);
        } else if (value === "pickup") {
            $("[name='delivery']").prop("required", false);
      $("[name='pickuptype']").prop("required", true);
        }
      });
  $('input[id=deliverytypelocation]').change(function() {
    $('input[id="deliverytypelocation"]').prop('required', false);
  });

    $('input[name=delivery]').change(function(){
        var value = $( 'input[name=delivery]:checked' ).val();
        if (value === 'distributor') {
            $("[name='pickupdisttxt']").prop("required", true);
            $("[name='deliveryaddress']").prop("required", false);
            $("[name='deliveryaddress2']").prop("required", false);
            $("[name='deliverycity']").prop("required", false);
            $("[name='deliverystate']").prop("required", false);
            $("[name='deliveryzip']").prop("required", false);
            $("[name='deliverycountry']").prop("required", false);

      $("[name='pickupdisttxt']").val("");
      $("[name='deliveryaddress']").val("");
      $("[name='deliveryaddress2']").val("");
      $("[name='deliverycity']").val("");
      $("[name='deliverystate']").val("");
      $("[name='deliveryzip']").val("");
      $("[name='deliverycountry']").val("");

        } else if (value === 'specificAddress') {
            $("[name='pickupdisttxt']").prop("required", false);
            $("[name='deliveryaddress']").prop("required", true);
            $("[name='deliveryaddress2']").prop("required", false);
            $("[name='deliverycity']").prop("required", true);
            $("[name='deliverystate']").prop("required", true);
            $("[name='deliveryzip']").prop("required", true);
            $("[name='deliverycountry']").prop("required", true);

            $("[name='pickupdisttxt']").val("");
            $("[name='deliveryaddress']").val("");
            $("[name='deliveryaddress2']").val("");
            $("[name='deliverycity']").val("");
            $("[name='deliverystate']").val("");
            $("[name='deliveryzip']").val("");
      $("[name='deliverycountry']").val("");
        } else if (value === 'sameAddress') {
            $("[name='pickupdisttxt']").prop("required", false);
            $("[name='deliveryaddress']").prop("required", false);
            $("[name='deliveryaddress2']").prop("required", false);
            $("[name='deliverycity']").prop("required", false);
            $("[name='deliverystate']").prop("required", false);
            $("[name='deliveryzip']").prop("required", false);
            $("[name='deliverycountry']").prop("required", false);

      $("[name='pickupdisttxt']").val($("[name='pickupdisttxt']").val());
      $("[name='deliveryaddress']").val($("[name='address']").val());
      $("[name='deliveryaddress2']").val($("[name='address2']").val());
      $("[name='deliverycity']").val($("[name='city']").val());
      $("[name='deliverystate']").val($("[name='state']").val());
      $("[name='deliveryzip']").val($("[name='zip']").val());
      $("[name='deliverycountry']").val($("[name='country']").val());
        }      
    });

  $('input[name=other]').change(function() {
    $("#otherwarning").show();
  });

    $('input[name=pickuptype]').change(function(){
        var value = $( 'input[name=pickuptype]:checked' ).val();
    // ClearSection3Delivery();
    if (value === 'Western Wine Services, CA') {
            $("[name='pickupothertxt']").prop("required", false);
            $("[name='pickupothertxt']").val("");
      $("#pickupothertxt").hide();
    } else if (value === 'Western Wine Carriers, NJ') {
            $("[name='pickupothertxt']").prop("required", false);
            $("[name='pickupothertxt']").val("");
      $("#pickupothertxt").hide();
    } else if (value === 'Convoy') {
            $("[name='pickupothertxt']").prop("required", false);
            $("[name='pickupothertxt']").val("");            
      $("#pickupothertxt").hide();
    } else if (value === 'Greystone') {
            $("[name='pickupothertxt']").prop("required", false);
            $("[name='pickupothertxt']").val("");
      $("#pickupothertxt").hide();
    } else if (value === 'Fond du lac (FDL), NJ') {
            $("[name='pickupothertxt']").prop("required", false);
            $("[name='pickupothertxt']").val("");
      $("#pickupothertxt").hide();
    } else if (value === 'Alba Wine and Spirits Warehousing') {
            $("[name='pickupothertxt']").prop("required", false);
            $("[name='pickupothertxt']").val("");            
      $("#pickupothertxt").hide();
    }  else if (value === "MHW's Office - Manhasset, NY") {
            $("[name='pickupothertxt']").prop("required", false);
            $("[name='pickupothertxt']").val("");
      $("#pickupothertxt").hide();
        }  else if (value === 'other') {
            $("[name='pickupothertxt']").prop("required", true);
            $("[name='pickupothertxt']").val("");
      $("#pickupothertxt").show();
        } 
    });


    $('input[name=sampleuser]').change(function(){
        var value = $( 'input[name=sampleuser]:checked' ).val();
        if (value === 'MHW') {
            $("[name='sampleusermhw']").prop("required", true);
            $("[name='sampleuserrep']").prop("required", false);
            $("[name='sampleuserbroker']").prop("required", false);

            $("[name='sampleuserbroker']").val("");
            $("[name='sampleuserrep']").val("");
        } else if (value === 'broker') {
            $("[name='sampleuserbroker']").prop("required", true);
            $("[name='sampleuserrep']").prop("required", false);
            $("[name='sampleusermhw']").prop("required", false);

            $("[name='sampleusermhw']").val("");
            $("[name='sampleuserrep']").val("");

        } else if (value === 'rep') {
            $("[name='sampleuserrep']").prop("required", true);
            $("[name='sampleuserbroker']").prop("required", false);
            $("[name='sampleusermhw']").prop("required", false);

            $("[name='sampleusermhw']").val("");
            $("[name='sampleuserbroker']").val("");
        } 
    });

    IsNotValidStateEventOnly();
    IsNotValidStateSampleReqEvent();

    function IsNotValidStateEventOnly() {   
    $("#eventstateeventonly").change(function () {
            DisplayErrorMsgIsNotValidStateEventOnly();
        });
    }

    function IsNotValidStateSampleReqEvent() {
    $("#eventstatesampleevent").change(function () {
            DisplayErrorMsgStateSampleReqEvent()
        });
    }

    function DisplayErrorMsgIsNotValidStateEventOnly() {
        if (!IsValidStateEventOnly()) {
            $('#eventTypeWarningMesg').modal('show');  
      $('#Section2').hide();
      return(false);
        } 
    return(true);
    }

    function DisplayErrorMsgStateSampleReqEvent() {
            if (!IsValidStateSampleReqEvent()) {  
                $('#eventTypeWarningMesg').modal('show'); 
      $('#Section2').hide();
      Section3Hide();
            } 
    return(false);
    }

    function IsValidStateEventOnly() {
    return true;
        var selValue = $("#eventstateeventonly").val();
        if (allowedStates.indexOf(selValue) >= 0) {
            return true; 
        } else {
            return false; 
        }
    }

    function IsValidStateSampleReqEvent() {
        var selValue = $("#eventstatesampleevent").val();
        if (allowedStates.indexOf(selValue) >= 0) {  
            return true; 
        } else {
            return false; 
        }
    }

  function toggleElementVisibility($id, $v = true) {
    if($(id).is(":visible")) {
      $(id).hide()
    }
    else {
      $(id).show()
    }
  }

  function tfaItemDescriptionShow() {
    $("#tfa_item_desc").show();
    $("#tfa_item_desc-D").show();
  }

  function tfaItemDescriptionHide() {
    $("#tfa_item_desc-D").hide();
    $("#tfa_item_desc").hide();
  }

  function hideSubmitButton() {
    $("#submit_button").hide();
  }

  function showSubmitButton() {
    $("#submit_button").show();
  }

  function Section2Hide() {
    $("#tfa_product_desc").val("");
    $("#tfa_item_desc").val("");
    $("#Section2").hide();


  }
  // Section 3 refers to something else
  function Section4Show() {
    $("#Section4").show();
    $("#deliveryDetails").show();
    $("[name='deliverytype']").prop("required", true);
    $("[id='deliverytypelocation']").prop("required", true);
    $("#submit_button").show();
  }

  function Section4Hide() {
    $("#Section4").hide();
    $("#deliveryDetails").hide();
  }

  function Section2Show() {
    $("#Section2").show();

  }

    function Section3Hide() {
        $("#eventDetails").hide();
        $("[name='eventdesc']").prop("required", false);
        $("[name='eventname']").prop("required", false);
        $("[name='eventtype']").prop("required", false);
        ClearSection3Delivery();
    }

    function Section3Show() {
        $("#eventDetails").show();
        $("[name='eventdesc']").prop("required", true);
        $("[name='eventname']").prop("required", true);
        $("[name='eventtype']").prop("required", true);
        $("[name='address']").prop("required", true);
        $("[name='address2']").prop("required", false);
        $("[name='city']").prop("required", true);
        $("[name='state']").prop("required", true);
        $("[name='zip']").prop("required", true);
        $("[name='country']").prop("required", true);
    }

    function ClearSection3Delivery() {     
        $("[name='pickupdisttxt']").prop("required", false);
        $("[name='address']").prop("required", false);
        $("[name='address2']").prop("required", false);
        $("[name='city']").prop("required", false);
        $("[name='state']").prop("required", false);
        $("[name='zip']").prop("required", false);
        $("[name='country']").prop("required", false);
        $("[name='pickupothertxt']").prop("required", false);

        $("[name='pickupdisttxt']").val("")
        $("[name='address']").val("");
        $("[name='address2']").val("");
        $("[name='city']").val("");
        $("[name='state']").val("");
        $("[name='zip']").val("");
        $("[name='country']").val("");
        $("[name='pickupothertxt']").val("");
    }

    function Section5Hide() {
        $("#whosection").hide();
        $("[name='sampleuser']").prop("required", false);

        $("[name='sampleusermhw']").prop("required", false);
        $("[name='sampleuserbroker']").prop("required", false);
        $("[name='sampleuserrep']").prop("required", false);
  
        $('input[name="sampleuser"]').prop('checked', false);
        $("[name='sampleusermhw']").val("");
        $("[name='sampleuserbroker']").val("");
        $("[name='sampleuserrep']").val("");
    }

    function Section5Show() {
        $("#whosection").show();

        $("input[name='sampleuser']").attr("checked", false);
        $("[name='sampleuser']").prop("required", true);

        $("[name='sampleusermhw']").prop("required", false);
        $("[name='sampleuserbroker']").prop("required", false);
        $("[name='sampleuserrep']").prop("required", false);

        $("[name='sampleusermhw']").val("");
        $("[name='sampleuserbroker']").val("");
        $("[name='sampleuserrep']").val("");

    }

    function SpecificAddressSectionShow() {
        $("#specificAddressSection").show();
        $("#showSpecificAddressFieldSection").removeClass('d-none');

        $("[name='sampleusermhw']").prop("required", false);
        $("[name='sampleuserbroker']").prop("required", false);
        $("[name='sampleuserrep']").prop("required", false);

        $("[name='sampleusermhw']").val("");
        $("[name='sampleuserbroker']").val("");
        $("[name='sampleuserrep']").val("");
    }

    function SameAddressSectionShow() {
        $("#sameAddressSection").show();

        $("[name='deliveryaddress']").prop("required", true);
        $("[name='deliveryaddress2']").prop("required", false);
        $("[name='deliverycity']").prop("required", true);
        $("[name='deliverystate']").prop("required", true);
        $("[name='deliveryzip']").prop("required", true);
        $("[name='deliverycountry']").prop("required", true);
    }

    function SpecificAddressSectionHide() {
        $("#specificAddressSection").hide();
        $("#showSpecificAddressFieldSection").addClass('d-none');
        
        $("[name='deliveryaddress']").prop("required", false);
        $("[name='deliveryaddress2']").prop("required", false);
        $("[name='deliverycity']").prop("required", false);
        $("[name='deliverystate']").prop("required", false);
        $("[name='deliveryzip']").prop("required", false);
        $("[name='deliverycountry']").prop("required", false);

        $("[name='deliveryaddress']").val("");
        $("[name='deliveryaddress2']").val("");
        $("[name='deliverycity']").val("");
        $("[name='deliverystate']").val("");
        $("[name='deliveryzip']").val("");
        $("[name='deliverycountry']").val("");
    }

    function SameAddressSectionHide() {
        $("#sameAddressSection").hide();
    }

  function clearItemSelection() {
    // $("#tfa_item_desc").val("");
    $("#tfa_item_desc").empty();
    $("#tfa_item_desc").hide();
    $("#tfa_item_desc-D").hide();
    $("#tfa_item_desc").parent().find(".ac_button").remove();  //clear elements of previous instance
    $("#tfa_item_desc").parent().find(".ac_result_area").remove();  //clear elements of previous instance\
  }

    function RenumberAddSections() 
    {
    $("#Section3").text("3.");
    $("#Section4").text("4.");
    $("#Section5").text("5.");
    }

    function RenumberRemoveSections() 
    {
    $("#Section4").text("3.");
    }
  var modalOverride = false;
  // $('form:first').submit(function(e) {
  $('#eventform').submit(function(e) {
    if(validForm() || modalOverride) {
      $(this).unbind('submit').submit();
      console.info(`Submitting form with request type ${$('input[name=requesttype]:checked').val()}`);
      return true;
    }
    e.preventDefault();
    if(allowsubmit) {
      allowsubmit = false;
      $(this).unbind('submit').submit();
            return true;
        } 
    return(false);
   }); 


  $("#okbutton").on("click", function(e) {
    modalOverride = true;
    $('#warningMesg').hide();
    $('#eventform').submit();
    return(true);
    });

  function enableSubmitButton(show = true) {
    $("#submit_button").prop('style', "");
    $("#submit_button").val("Submit");
    $("#submit_button").prop('disabled', false);
    $("#submit_button").hide();
    if(show)
    $("#submit_button").show();
  }

  $("#cancelButton").on("click", function(e) {
    $("#warningMesg").modal("hide");

    $(focussed).focus(); // returns focus to last active element
    enableSubmitButton();
    return(false);
    });
    
  $('#tfa_item_desc').keydown(function (e) {
      if (e.keyCode == 13) {
          e.preventDefault();
          if(!isItemValidSelection()) {
            $("#itemvalid").show();
            return false;
          }
          return(true);
      }
  });

      $(document.body).on('change', '.attachmentButton', function (e) {
        var docRowId = $(this).attr('data-doc-row');

        //hide the div we're in
        var theDiv = $('#' + docRowId).hide();

        //append a new <li> to the docsListing <ul>
        $('#docsListing').append('<div id="span-' + docRowId + '" class="pad-right-15">' + this.files[0].name + '<i class="fa fa-times-circle-o arm-font-16 text-danger pad-left-5 removeUpload arm-pointer" aria-label="Remove" title="Remove" data-doc-row="' + docRowId + '"></i></div>');

        //add another file and button to the page

        //in the first div of the attachmentTemplate add in a guid to uniquely identify the row (so we can delete if need be)
        var uniqueId = guid();
        $('#attachmentTemplate').find('.row').attr('id', uniqueId);
        $('#attachmentTemplate').find('.attachmentButton').attr('data-doc-row', uniqueId);

        $('#docsDiv').append($('#attachmentTemplate').html());
    });

    $(document.body).on('click', '.removeUpload', function (e) {
        //nuke it from the form
        var docRowId = $(this).attr('data-doc-row');
        $('#span-' + docRowId).remove();
        $('#' + docRowId).remove();
    });

function validForm() {


    // if (value === 'This is a sample request for a distributor') {
    //   var today = moment();
    //   var samplerequestdateValue = $('input[name=samplerequestdate]').val().replace(/-/, '/').replace(/-/, '/');
    //   var samplerequestdateValueDT = moment(samplerequestdateValue);
    //
      // console.log(`DeliveryType -> ${$("[id='Delivery']").val()} pickupdistlocation -> ${$("[id='pickupdistlocation']").val()}`);
      // if($("[id='deliverytype']").val() === "Delivery" && $.trim($("[id='pickupdistlocation']").val()) === "") {
      //   allowsubmit = false;
      //   enableSubmitButton();
      // }
      // if (samplerequestdateValueDT.isSame(today, 'day')) {
      //   $('#warningMesg #ModalText').text("For immediate pickup fees may apply, contact MHW directly, or select tomorrow or a future date");
      //   $('#warningMesg').modal('show');
      //   allowsubmit = false;
      // }
      // else if (samplerequestdateValueDT.isBefore(todaydateValue10DaysDT, 'day')) {
      //   $('#warningMesg #ModalText').text(firstSentence + '\n\n' + secondSentence);
      //   $('#warningMesg').modal('show');
      //   allowsubmit = false;
      // }
      // else if (samplerequestdateValueDT.isBefore(todaydateValue20DaysDT, 'day')) {
      //   $('#warningMesg #ModalText').text(firstSentence);
      //   $('#warningMesg').modal('show');
      //   allowsubmit = false;
      // }

    // } else
    // if (value === 'This is a sample request for a licensed solicitor to use for account visits') {
    //   var today = moment();
    //   var samplesolicitordateValue = $('input[name=samplesolicitordate]').val().replace(/-/, '/').replace(/-/, '/');
    //   var samplesolicitordateDT = moment(samplesolicitordateValue);
    //
    //   // if($("[id='deliverytype']").val() === "Delivery" && $("[id='pickupdistlocation']").val() === "") {
    //   //   allowsubmit = false;
    //   //   enableSubmitButton();
    //   // }
    //   if (samplesolicitordateDT.isSame(today, 'day')) {
    //     $('#warningMesg #ModalText').text("For immediate pickup fees may apply, contact MHW directly, or select tomorrow or a future date");
    //     $('#warningMesg').modal('show');
    //     allowsubmit = false;
    //   }
    //   else if (samplesolicitordateDT.isBefore(todaydateValue10DaysDT, 'day')) {
    //     $('#warningMesg #ModalText').text(firstSentence + '\n\n' + secondSentence);
    //     $('#warningMesg').modal('show');
    //     allowsubmit = false;
    //   }
    //   else if (samplesolicitordateDT.isBefore(todaydateValue20DaysDT, 'day')) {
    //     $('#warningMesg #ModalText').text(firstSentence);
    //     $('#warningMesg').modal('show');
      //   allowsubmit = false;
      // }
    //
    // }
    allowsubmit = true;

    var firstSentence = "Less than 20 days' notice for events in CA, FL, NJ, & NY are subject to a rushed vetting fee of $150.00 per request.";
    var secondSentence = "Less than 10 days notice is subject to rejection."

    var value = $('input[name=requesttype]:checked').val();

    var todaydateValue10DaysDT = moment(moment(), "DD-MM-YYYY").add(10, 'days');
    var todaydateValue20DaysDT = moment(moment(), "DD-MM-YYYY").add(20, 'days');

    if (value === 'This is a SAMPLE request and EVENT notification') {
        if (!IsValidStateSampleReqEvent()) {
           DisplayErrorMsgStateSampleReqEvent();
        $("#clearform").click();
           return false;
        } 
        var eventdateValue = $('input[name=eventdate2]').val().replace(/-/, '/').replace(/-/, '/');
        var sampledate2Value = $('input[name=sampledate2]').val().replace(/-/, '/').replace(/-/, '/');
        var eventdateValueDT = moment(eventdateValue);
        var sampledate2ValueDT = moment(sampledate2Value);
        var today = moment();
      if (sampledate2ValueDT.isSame(today, 'day') || eventdateValueDT.isSame(today, 'day')) {
            $('#warningMesg #ModalText').text("For immediate pickup fees may apply, contact MHW directly, or select tomorrow or a future date"); 
            $('#warningMesg').modal('show'); 
        allowsubmit = false;
      } else if (sampledate2ValueDT.isBefore(todaydateValue10DaysDT, 'day')) {
            $('#warningMesg #ModalText').text(firstSentence + '\n\n' + secondSentence); 
            $('#warningMesg').modal('show');  
        allowsubmit = false;
      }
      else if(eventdateValueDT.isBefore(todaydateValue10DaysDT, 'day')) {
        $('#warningMesg #ModalText').text(firstSentence + '\n\n' + secondSentence);
        $('#warningMesg').modal('show');
        allowsubmit = false;
      }
      else if(eventdateValueDT.isBefore(todaydateValue20DaysDT, 'day')) {
        $('#warningMesg #ModalText').text(firstSentence);
        $('#warningMesg').modal('show');
        allowsubmit = false;
      } else if(sampledate2ValueDT.isBefore(todaydateValue20DaysDT, 'day')) {
            $('#warningMesg #ModalText').text(firstSentence); 
            $('#warningMesg').modal('show');  
        allowsubmit = false;
        }
    } else if (value === "This is an EVENT only notification") {
        if (!IsValidStateEventOnly()) {
           DisplayErrorMsgIsNotValidStateEventOnly();
        $("#clearform").click();
        allowsubmit = false;
        }

        var eventdateValue = $('input[name=eventdate]').val().replace(/-/, '/').replace(/-/, '/');
        var eventdateValueDT = moment(eventdateValue);
        var today = moment();
        
      if (eventdateValueDT.isSame(today, 'day')) {
        $('#warningMesg #ModalText').text(firstSentence + '\n\n' + secondSentence);
        $('#warningMesg').modal('show');
        allowsubmit = false;
        }
      else if (eventdateValueDT.isBefore(todaydateValue10DaysDT, 'day')) {
            $('#warningMesg #ModalText').text(firstSentence + '\n\n' + secondSentence); 
            $('#warningMesg').modal('show');  
        allowsubmit = false;
        }
      else if (eventdateValueDT.isBefore(todaydateValue20DaysDT, 'day')) {
            $('#warningMesg #ModalText').text(firstSentence); 
            $('#warningMesg').modal('show');  
        allowsubmit = false;
      }
    }

    return(allowsubmit);
  }

  function cbb_formatter(){
    $('ul.ac_results').css("margin-left","0em");
    $('ul.ac_results').css("margin","0em");
    $('.ac_button').css("height","28px");
    $('.ac_button').css("background","transparent");
    $('.ac_button').css("margin-left","-35px");
    $('.ac_button').css("border","0px");
    $('.ac_subinfo dl').css("margin-left","0em");
    $('.ac_subinfo dl').css("margin","0em");
  }


  function isItemValidSelection() {
    if($("#tfa_item_desc")[0].hasAttribute("sub_info")) {
      var rawData = $("#tfa_item_desc").attr('sub_info');
      //if additional sub_info elements are added, additional apostrophe replacements need to be added
      var jsonString = rawData.replace(/'/g,'"');
      var jsonObj = JSON.parse(jsonString);
      console.log(JSON.stringify(jsonObj));
      $("#itemID").val(jsonObj.id);  //set field for processing update
      $("#itemID2").val(jsonObj.id);
      $("#tfa_item_id").val(jsonObj.id);
      $("#mhw_item_code").val(jsonObj.mhw_item_code);
      $("#itemvalid").hide();
      $('#AddNewProduct').show();
      return(true);
    }
    else {
      //$("#tfa_399-L").html("Product: new entry"); //display message for insert
      // $("#itemID").val("");  //clear field for insert
      $("#itemvalid").show();

      $('#AddNewProduct').hide();
      return(false);
    }
    return(false);
  }

  function getProductItems(prodID) {
    //$('#tfa_product_desc').parent().find(".ac_button").remove();  //clear elements of previous instance
    //$('#tfa_product_desc').parent().find(".ac_result_area").remove();  //clear elements of previous instance
    // TODO: Revert this to the variable client.
    //var client = "MHW Web Demo";
	var client = $('#client_name').val();
    var cbbtype = 'product_items';
    // Ajax callback to fetch data from the database.
    console.info(`select-request-samples.php -> ${JSON.stringify({ cbbtype: cbbtype, client: client, product_id: prodID })}`);
    $.post('select-request-samples.php', { cbbtype: cbbtype, client: client, product_id: prodID }, function(dataX) { // ajax request select-request-samples.php, send brand as post variable, return dataX variable, parse into JSON
      var data = ([]);
      try {
        data = JSON.parse(dataX);
        // console.info(`Result of Ajax call to the select request PHP code -> ${JSON.stringify(data, 0, 2)}`);
        if(data.length == 0) {
          $("#itemvalid").show();
          return(false);
        }
      }
      catch(e) {
        console.log(`JSON Data -> ${dataX}`);
        console.error(e);
        return(false);
      }
      // Build the item submenu
      $(function () {
        $("#tfa_item_desc").ajaxComboBox(
          data,
          {
            bind_to: 'tfa_item_desc',
            sub_info: true,
            sub_as: {
              id: 'Item ID',
              item_id: 'ID of the Item',
              mhw_item_code: "MHW Item Code",
              item_description: 'Item Description',
              code: 'MHW Code',
              stock_uom: 'Stock UOM',
              container_type: 'Container',
              container_size: 'Container Size'
            },
          }
        ).bind("tfa_item_desc", function() {
          var json = $(this).attr('sub_info');
          //if additional sub_info elements are added, additional apostrophe replacements need to be added
          // var json2 = json.replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"').replace("'",'"');

          // Replaced above with a regex to avoid being hard coded to the number of fields.
          var jsonObj = JSON.parse(json.replace(/'/g,'"'));

          if(parseInt(jsonObj.id) > 0) {
            $("#itemID").val(jsonObj.id); //set field for processing update
            $("#tfa_item_desc").attr("value", jsonObj.item_description);
            $("#tfa_stock_desc").attr("value", jsonObj.stock_uom);
            $("#tfa_code_desc").attr("value", jsonObj.code);
            $("#tfa_product_id").attr("value", prodID);
            $("#tfa_item_id").attr("value", jsonObj.id);
            $("#tfa_mhw_item_code").attr("value", jsonObj.code);
            console.log("MHW Item Code --> " + $("input[class=tfa_mhw_item_code]").val());
            $("#tfa_399-L").html("Product: editing item_id  -> " + jsonObj.id); //display message for update
            $("#itemvalid").hide();
          }
          else if(isItemValidSelection()) {
            $("#tfa_item_desc-D").show();
            $("#tfa_item_desc").show();
          }
        });
        cbb_formatter(); //apply css and classname changes to combobox elements
      });
    });
    $("#tfa_item_desc").bind("change", function() {
      if(isItemValidSelection()) {
        $("#tfa_item_desc-D").show();
        $("#tfa_item_desc").show();
        }       
    });
    return(true);
    }

    /**
     * Generates a GUID string.
     * @returns {String} The generated GUID.
     * @example af8a8416-6e18-a307-bd9c-f2c947bbb3aa
     * @author Slavik Meltser (slavik@meltser.info).
     * @link http://slavik.meltser.info/?p=142
     */
    function guid() {
        function _p8(s) {
            var p = (Math.random().toString(16)+"000000000").substr(2,8);
            return s ? "-" + p.substr(0,4) + "-" + p.substr(4,4) : p ;
        }
        return _p8() + _p8(true) + _p8(true) + _p8();
    }
});
