<?php //SUPPLIER SETUP
//=============settings============
include("settings.php");
//=============settings============

//=============functions============
include("functions.php");
//=============functions============

session_start();
if(!isset($_SESSION['mhwltdphp_user'])){
	die( "Not authenticated !" );  
}

$isAdmin = in_array($_SESSION['mhwltdphp_usertype'], ["SUPERUSER", "ADMIN"]);

//TODO: alter?
// if ($_SESSION['productsetup_submit_hash'] === $_POST['productsetup_submit_hash']) {
// 	// page refreshed with post data
// 	echo "Submitted already!";
// } else {
// 	$_SESSION['productsetup_submit_hash'] = $_POST['productsetup_submit_hash'];
?>

<!--Main layout-->
<main>
<div class="container-fluid">
<!--<span id="processor_include">PROCESSOR LISTENING...</span>-->

<?php
	//echo "<pre>";
	//var_dump($_REQUEST); 
	//echo "</post>";
	
	//echo "FILES:<br />";
	//print_r($_FILES);

    include("dbconnect.php");
   
    //Establishes the connection
    $conn = sqlsrv_connect($serverName, $connectionOptions);
	if( $conn === false) {
    		die( print_r( sqlsrv_errors(), true));
	}

	$errorlog="\n";
	$reqlog="\n";
	$errorFree=1;

	$client_code = '';
	$client_name = $_POST['tfa_client_name'];
	$user_id = $_POST['userID'];
	$supplier_id = 0;
	$origin_code = $_POST['origin_code'];

			//TODO: must use this in inline update too so that origin codes cannot be updated to same value
			/* ============BEGIN ORIGIN CODE VALIDATE ======================== */ 
		// 	$tsql_origin_code = "SELECT COUNT(*) FROM [mhw_app_prod_supplier] WHERE active=1 AND deleted=0 AND origin_code = ?";


		// 	$stmt_origin_code = sqlsrv_query($conn, $tsql_origin_code, array($origin_code));
		// 	if (!$stmt_origin_code) die (sqlsrv_errors());

		// 	if(sqlsrv_fetch( $stmt_origin_code ) === false) {
		// 		die( print_r( sqlsrv_errors(), true));
		//    }

		//    if ($stmt_origin_code) {
		// 	$rows = sqlsrv_has_rows( $stmt_origin_code );
		// 	if ($rows === true)
		// 	   //echo "There are rows. <br />";
		// 	else 
		// 	   //echo "There are no rows. <br />";
		// 	   exit;
		//  }
			/* ============END ORIGIN CODE VALIDATE ======================== */ 

		//if($prodID>0){//TODO:remove productID fields. rename post variables.
            $prodID = 0; 
			/* ============BEGIN SUPPLIER ======================== */ 
			 $tsql_supplier= "EXEC [dbo].[usp_product_supplier] 
			 @supplier_id =".$supplier_id."
			,@product_id =".$prodID." 
			,@supplier_name = '".str_replace("'","''",$_POST['tfa_supplier_name'])."'
			,@supplier_contact = '".str_replace("'","''",$_POST['tfa_supplier_contact'])."'
			,@supplier_fda_number = '".str_replace("'","",$_POST['tfa_supplier_fda_number'])."'
			,@tax_reduction_allocation = '".str_replace(",",".",str_replace("'","",$_POST['tfa_tax_reduction_allocation']))."'
			,@supplier_address_1 = '".str_replace("'","''",$_POST['tfa_supplier_address_1'])."'
			,@supplier_address_2 = '".str_replace("'","''",$_POST['tfa_supplier_address_2'])."'
			,@supplier_address_3  = '".str_replace("'","''",$_POST['tfa_supplier_address_3'])."'
			,@supplier_city = '".str_replace("'","''",$_POST['tfa_supplier_city'])."'
			,@supplier_state = '".str_replace("'","''",$_POST['tfa_supplier_state'])."'
			,@supplier_country  = '".str_replace("'","''",$_POST['tfa_supplier_country'])."'
			,@supplier_zip = '".str_replace("'","",$_POST['tfa_supplier_zip'])."'
			,@supplier_phone = '".str_replace("'","",$_POST['tfa_supplier_phone'])."'
			,@supplier_email = '".str_replace("'","",$_POST['tfa_supplier_email'])."'
			,@create_via = 'productsetup'
			,@username = '".$user_id."'
			,@client_code = '".$client_code."'
			,@client_name = '".$client_name."'
			,@federal_permit = '".str_replace("'","",$_POST['tfa_supplier_fed_permit'])."'
			,@origin_code = '".str_replace("'","''",$_POST['origin_code'])."'"; 

            //echo $tsql_supplier; exit;
			
            //TODO:Remove this?
			//prep variables for re-filling the fields for resubmitting
			$pf_supplier_name = $_POST['tfa_supplier_name'];
			$pf_supplier_contact = $_POST['tfa_supplier_contact'];
			$pf_supplier_fda_number = $_POST['tfa_supplier_fda_number'];
			$pf_tax_reduction_allocation = $_POST['tfa_tax_reduction_allocation'];
			$pf_supplier_address_1 = $_POST['tfa_supplier_address_1'];
			$pf_supplier_address_2 = $_POST['tfa_supplier_address_2'];
			$pf_supplier_address_3  = $_POST['tfa_supplier_address_3'];
			$pf_supplier_city = $_POST['tfa_supplier_city'];
			$pf_supplier_state = $_POST['tfa_supplier_state'];
			$pf_supplier_country  = $_POST['tfa_supplier_country'];
			$pf_supplier_zip = $_POST['tfa_supplier_zip'];
			$pf_supplier_phone = $_POST['tfa_supplier_phone'];
			$pf_supplier_email = $_POST['tfa_supplier_email'];
			$pf_supplier_fed_permit = $_POST['tfa_supplier_fed_permit'];
			//$pf_origin_code = $_POST['origin_code'];

			//TODO:eliminate or rewrite this. remove control flow statements
		//	if($prodID>0){  //product_id set 
			$stmt_supplier = sqlsrv_query($conn, $tsql_supplier);
		
			while ($row_supplier = sqlsrv_fetch_array($stmt_supplier, SQLSRV_FETCH_ASSOC)) {
				$suppID = $row_supplier['suppID']; 
			}
			//echo  "SUPPID:".$suppID;
			
			if( $stmt_supplier === false ) {
				if( ($errors = sqlsrv_errors() ) != null) {
					//log sql errors for writing to server later
					foreach( $errors as $error ) {
						$errorlog.= $tsql_supplier."\n";
						$errorlog.= "SQLSTATE: ".$error[ 'SQLSTATE']."\n";
						$errorlog.= "code: ".$error[ 'code']."\n";
						$errorlog.= "message: ".$error[ 'message']."\n";
					}
				}
				$errorFree=0;
			}
		  
			//release sql statement
		  	sqlsrv_free_stmt($stmt_supplier);
		//	}  //end product_id
		//	else{
				$errorlog.= $tsql_supplier."\n";
		//	}
			/* ============END SUPPLIER ======================== */ 
		
	

    //TODO: need to rewrite or eliminate as no products are updated
	if ($isAdmin && $prodID>0) {
		$tsql= "UPDATE [dbo].[mhw_app_prod] SET [finalized] = 1 WHERE [product_id] = ?";

		//echo $tsql." ".$prodID; exit;

		$stmt = sqlsrv_prepare($conn, $tsql, array($product_id));
		if( $stmt === false )  
		{  
			echo "Statement could not be prepared.\n";  
			die( print_r( sqlsrv_errors(), true));  
		}  
		
		if( sqlsrv_execute($stmt) === false )  
		{  
			echo "Statement could not be executed.\n";  
			die( print_r( sqlsrv_errors(), true));  
		}
	}

	//release sql statement
	  sqlsrv_free_stmt($stmt);


  	
	sqlsrv_close($conn);  

	if($errorFree==1){
		//if($i>1){
		//	echo "<div class=\"row\"><div class=\"col-sm-8\"><div class=\"card\"><div class=\"card-body\"><h5 class=\"card-title\">Thank you</h5><p class=\"card-text\">Your Product &amp; Item Setup Forms have been submitted.  <br />You may edit the Product &amp; add Items below.  <br /><br /><img src=\"css/img/Logo_Chevron_Animated.gif\" class=\"inline_animated_chevron\" /> You must <a href=\"viewproducts.php?v=nf\" class=\"btn btn-outline-success\">Finalize</a> your Products &amp; Items to submit to MHW for processing. <br /><br /><a href=\"viewproducts.php\" class=\"btn btn-outline-primary\">View Products</a></p></div></div></div>";
		//}
		//else{
		//echo "<div class=\"jumbotron jumbotron-fluid\">";
		  //echo "<div class=\"container\">";
			//echo "<div class=\"row\"><div class=\"col-sm-12\">";
			echo "<div class=\"card\"><div class=\"card-body\">";
			  echo "<h5 class=\"card-title\">Thank you</h5>";

			  if (!$isAdmin) {
			  echo "<p class=\"card-text\">Your Product &amp; Item Setup Form has been submitted. <br />You may continue to edit the Product, upload images &amp; add Items below.  <br /><br/ ><img src=\"css/img/Logo_Chevron_Animated.gif\" class=\"inline_animated_chevron\" /> After you are finished with your session of adding products & items, <a href=\"viewproducts.php?v=nf\" class=\"btn btn-outline-success btn-sm\">Finalize</a> your Products &amp; Items to submit to MHW for processing. <br /><br />  <a href=\"viewproducts.php\" class=\"btn btn-outline-primary btn-sm\">View Products &amp; Items</a> <a href=\"imageupload.php?pid=".$prodID."\" class=\"btn btn-outline-primary btn-sm\">View &amp; Attach Product Images</a></p>";
			} else {
				echo "Finalized  <a href=\"viewproducts.php\" class=\"btn btn-outline-primary btn-sm\">View Products &amp; Items</a> <a href=\"imageupload.php?pid=".$prodID."\" class=\"btn btn-outline-primary btn-sm\">View &amp; Attach Product Images</a>";
			  }

			echo "</div></div>";
			//echo "</div></div>";
		  //echo "</div>";
		//echo "</div>";
		//}  
		$fp1 = fopen('../logs/prodlog.txt', 'a');
		$reqlog="\n\r\n\r".date("Ymd H:i:s")."\t".$reqlog."\n\r";
		fwrite($fp1, $reqlog);
		fclose($fp1);
	}
	else{
		$errorstamp = date("Ymd H:i:s");
		echo "<div class=\"row\"><div class=\"col-sm-8\"><div class=\"card\"><div class=\"card-body\"><h5 class=\"card-title\">Error</h5><p class=\"card-text\">There was an error saving your Product &amp; Item Setup Form. ".$pageerr." Additional error details have been logged. (ERROR-".$errorstamp.")</p></div></div></div>";
		$fp2 = fopen('../logs/prod3rr0rlog.txt', 'a');
		$errorlog="\n\r\n\r".$errorstamp."\t".$errorlog."\n\r";
		fwrite($fp2, $errorlog);
		fclose($fp2);
		
	}

?>
</div>
</main>
<!--Main layout-->