<?php
//session_start();
include("sessionhandler.php");
include("prepend.php");
include("settings.php");
include("functions.php");

include("MHWEmailAgent.php");

$debug = 1;


// function generateComplianceEmailSubject($rushedOrder, $eventId, $sampleDate) {
//   $emailSubject = null;
//   if($rushedOrder) {
//     $emailSubject = "Compliance Team Accepted rushed order:";
//   }
//   else {
//     $emailSubject = "Compliance Team Accepted:";
//   }
//   $emailSubject .= " Event ID = " . $eventId . " Sample Date = " . $sampleDate;
//   return($emailSubject);
// }


$headings =        array("Sample Order #", "Client Name", "Submitted By", "Sample Date", "Event Date", "Created Date", "Event Details", "Delivery/Pickup", "Location", "Use of Samples", "Review Remarks", "Rushed Order", "Status");
$headingIndicies = array(array(0),           array(1),        array(2),     array(3),     array(4),      array(5),      array(6),         array(7),         array(8),      array(9),      array(10), array(12), array(12));

function generateAcceptedEmailHTML($headings, $headingIndicies, $eventData, $itemData = []) {
  $message = '<style> table, td, th, tr, tbody, th  { border: 1px solid black; } table { width: 100%; border-collapse: collapse; } </style>';
  $message .= '<table align="left" border="1" cellspacing="0" cellpadding="0" style="border:1px solid #cc;">';
  $message .= '<th><tr>';
  for($i = 0; $i < sizeof($headings); $i++) {
    $message .= "<td><strong>" . $headings[$i] . "</strong></td>";
  }
  $message .= "</tr></th><tbody><tr>";
  for($i = 0; $i < sizeof($headingIndicies); $i++) {
    $message .= "<td>";
    for($j = 0; $j < sizeof($headingIndicies[$i]); $j++) {
      $messageLength = strlen($message);
      if($eventData[$headingIndicies[$i][$j]] != -1) {
        $message .= ($eventData[$headingIndicies[$i][$j]] === null) ? '' : $eventData[$headingIndicies[$i][$j]];
      }
      else {
        $message .= '-';
      }
      // if(strlen(message) > $message && ($j < sizeof($headingIndicies[$i]))) {
      //   $message .= ' ';
      //   continue;
      // }
      $message .= "</td>";
    }
  }
  $message .= "</tr>";
  for($k = 0; $k < sizeof($itemData); $k++) { // Item array row 1 should be the titles
    $message .= "<tr><td>+</td>"; // Indent by one column to match the web interface layout
    for($l = 0; $l < sizeof($itemData[$k]); $l++) {
    	switch($k) {
				case 0:
        	$message .= "<td><strong>" . $itemData[$k][$l]  . "</strong></td>";
      		break;
				default:
	        $message .= "<td>" . $itemData[$k]["productName"] . "</td>";
	        $message .= "<td>" . $itemData[$k]["quantity"] . "</td>";
	        $message .= "<td>" . $itemData[$k]["itemDescription"] . "</td>";
	        $message .= "<td>" . $itemData[$k]["stockUom"] . "</td>";
					$l = sizeof($itemData[$k]);
					break;
      }
    }
    $message .= "</tr>";
		$l++;
  }

  $message .= "</tr></tbody></table>";
  return($message);
}


if(!isset($_SESSION['mhwltdphp_user'])){
	header("Location: login.php");
}
else{
	if($_SESSION['mhwltdphp_usertype'] != "SUPERUSER" && $_SESSION['mhwltdphp_usertype'] != "ADMIN" ){
		echo "Access Denied";
		//header("Location: ../mhwlogin.php");
		header("Location: home.php");
	}
	else {
		include("dbconnect.php");
		include('head.php');
		$param1 = $_GET["ok"];
		$linkedEventId = 0;
		if($_GET['event_id'] > 0) {
			$linkedEventId = $_GET['event_id'];
	}
	//Establishes the connection
	$conn = sqlsrv_connect($serverName, $connectionOptions);
	if( $conn === false) {
			die( print_r( sqlsrv_errors(), true));
	}

	$postParams = array_values($_POST);
	if($debug) {
		ob_implicit_flush(true);
		error_log("Post Parameters...");
		error_log(print_r($postParams, true));
		ob_implicit_flush(false);
	}
	if(sizeof($postParams) > 0) {
		if(debug) {
			error_log("Creating data objects for email");
		}
		$sample_order_number = (empty($postParams[0]["sample_order_number"])) ? null : $postParams[0]["sample_order_number"];
		$client_name = (empty($postParams[0]["client_name"])) ? null : $postParams[0]["client_name"];
		$client_code = (empty($postParams[0]["client_code"])) ? null : $postParams[0]["client_code"];
		$submitted_by = (empty($postParams[0]["submitted_by"])) ? null : $postParams[0]["submitted_by"];
		$order_date = (empty($postParams[0]["order_date"])) ? null : $postParams[0]["order_date"];
		$event_details = (empty($postParams[0]["event_details"])) ? null : $postParams[0]["event_details"];
		$delivery_pickup = (empty($postParams[0]["delivery_pickup"])) ? null : $postParams[0]["delivery_pickup"];
		$location = (empty($postParams[0]["location"])) ? null : $postParams[0]["location"];
		$use_of_samples = (empty($postParams[0]["use_of_samples"])) ? null : $postParams[0]["use_of_samples"];
		$review_remarks = (empty($postParams[0]["review_remarks"])) ? null : $postParams[0]["review_remarks"];
		$rushed_order = (empty($postParams[0]["rushed_order"])) ? 0 : $postParams[0]["rushed_order"];
		$status = (empty($postParams[0]["status"])) ? null : $postParams[0]["status"];

		$event_date = (empty($postParams[0]["event_date"])) ? null : $postParams[0]["event_date"];
		$create_date = (empty($postParams[0]["created_date"])) ? null : $postParams[0]["created_date"];

		$emailArray = [ $sample_order_number, $client_name, $submitted_by, $order_date, $event_date, $create_date, $event_details, $delivery_pickup, $location, $use_of_samples, $review_remarks, $rushed_order, $status ];

		// Fill the item array
		$itemArray = [];
		array_push($itemArray, array("MHW Item Code", "# of Bottles Requested", "Item Description", "Stock UOM"));
		error_log("postParams[1] -> " . print_r(postParams[1], true));
		error_log("postParams[1] -> Elements in array" .sizeof($postParams[1]));
		for($i = 0; $i < sizeof($postParams[1]); $i++) {
			array_push($itemArray, \array_splice($postParams[1][$i], 2, 6)); // Remove the item and product id
		}
		error_log("postParams[1] -> Elements in array" . print_r($itemArray, true));


		$complianceEmailQuery = "SELECT DISTINCT e.[Compliance Ops Email] FROM [tray_email_routing] AS e WHERE e.[Client ID] = '$client_code'";

		$operationsEmailStmnt = sqlsrv_query($conn, $complianceEmailQuery);
		if(sqlsrv_fetch($operationsEmailStmnt) === false) {
			die(print_r(sqlsrv_errors(), true));
		}
		$operationsEmailTo = sqlsrv_get_field($operationsEmailStmnt, 0);
		if($operationsEmailTo === "TBD" ) {
			$operationsEmailTo = $defaultOpsEmail;
		}
		else if($operationsEmailTo === "") {
			$operationsEmailTo = $defaultOpsEmail;
			//error_log("Error the compliance email in the database is not set for the query -> " . $tsql);
			//return;
		}
		error_log(`email to -> ` . $operationsEmailTo);
		$emailAgent = new MHWEmailAgent();
		$emailAgent->setUsername();
		$emailAgent->setPassword($fromPassword);
		$emailAgent->setFrom();
		$emailAgent->setBody();
		$emailAgent->setAltBody();
		if($emailAgent->addRecipient($operationsEmailTo, "Operations Team")) {

			$NewOrderOpsEmailSubject = str_replace("{enter_order_number_here}",$sample_order_number,$NewOrderOperationsEmailSubject);
			$emailAgent->setSubject($NewOrderOpsEmailSubject);
      $emailHeader = '<p class="MsoNormal">Sample Order #' . $sample_order_number . ' has been approved by Compliance team. Please refer to the following table for details.</p><br />';
      $emailFooter = '<br /><br /><p class="MsoNormal">You can click on the following link to the CRD to view more details: <a href="' . $siteroot . '/client_samples_review.php">' . $siteroot . '/client_samples_review.php</a></p>';
      $emailAgent->isHTML(true);
      $emailAgent->setBody($emailHeader . generateAcceptedEmailHTML($headings, $headingIndicies, $emailArray, $itemArray) . $emailFooter);
			if($emailAgent->sendEmail()) {
				error_log("Operations team email sent to -> " . $operationsEmailTo);
			}
			else {
				error_log("Operations team email sent to -> " . $operationsEmailTo . ". Email send failed");
			}
		}

	}

?>

<script>



function updateReviewRemarks(event_id, qyrmsg, qrymsg) {
	// console.info(`TextArea: #rr__${event_id} blur event. Event ID -> ${event_id} Data to post -> ${qrymsg}` );
	$.post('query-request-samples.php', { qrytype: "updatereviewremarks", event_id: event_id, qrymsg: qrymsg }, function(r) {
		var json = null;
		try {
			json = JSON.parse(r);
		}
		catch(e) {
			console.error(`processResponse: Exception -> ${e} for r -> ${r}`);
			return;
		}
	});
}

function setTextAreaOnBlur(qrytype, data, message) {
	var qryUSER = '<?php echo $_SESSION["mhwltdphp_user"]; ?>';
	if(qrytype === "compliance_sample_list") {
		for(let i = 0; i < data.length; i++) {
			$(`#rr__${data[i].sampleOrderNo}`).on('blur', function(e) {
				updateReviewRemarks(data[i].sampleOrderNo, qryUSER, $(this).val());
			});
		}
	}
	else if(qrytype === 'operations_sample_list') {
		for(let i = 0; i < data.length; i++) {
			$(`#rr__${data[i].sampleOrderNo}`).on('blur', function(e) {
				updateReviewRemarks(data[i].sampleOrderNo, qryUSER, $(this).val());
			});
		}
	}
}

function hideButtons(qrytype, data) {
	if(qrytype === 'compliance_sample_list') {
		for(let i = 0; i < data.length; i++) {
			$(`#btnca__${data[i].sampleOrderNo}`).show();
			$(`#btncr__${data[i].sampleOrderNo}`).show();
			$(`#btncc__${data[i].sampleOrderNo}`).show();
			if(data[i].processed === 'Approved' || data[i].processed === 'Completed' || data[i].processed === 'Rejected' || data[i].processed === 'Cancelled') {
				$(`#btnca__${data[i].sampleOrderNo}`).hide();
				$(`#btncr__${data[i].sampleOrderNo}`).hide();
				$(`#btncc__${data[i].sampleOrderNo}`).hide();
			}
		}
	}
	else if(qrytype === 'operations_sample_list') {

		for(let i = 0; i < data.length; i++) {
			$(`#btnob__${data[i].sampleOrderNo}`).show();
			$(`#btnor__${data[i].sampleOrderNo}`).show();
			$(`#btnoc__${data[i].sampleOrderNo}`).show();
			if(data[i].processed === "Compliance Hold" || data[i].processed === "Rejected" || data[i].processed === "Cancelled"|| data[i].processed === "Completed"  ) {
				$(`#btnob__${data[i].sampleOrderNo}`).hide();
				$(`#btnor__${data[i].sampleOrderNo}`).hide();
				$(`#btnoc__${data[i].sampleOrderNo}`).hide();
			}
		}
	}
}

var linkedEventId = <?php echo($linkedEventId); ?>;
var debug = true;
$( document ).ready(function() {
	$("#finalize").hide();
	$('#client_name').on('change', function () {
		var value = $(this).val().toLowerCase();
		var valorig = $(this).val();

		if(value!="all"){
			$('#clientSampleTable').bootstrapTable('filterBy', {client_name: valorig});
		}
		else{
			$('#clientSampleTable').bootstrapTable('filterBy', '');
		}

		$('.prodedit_btn').on("click", function() {
			var prod = $(this).data("target");
			$("#editprod_"+prod).submit();
		});
	});

	$(".prodedit_btn").on("click", function() {
		var prod = $(this).data("target");
		$("#editprod_"+prod).submit();
	});
	/////////////////////////////////////////////////////////////




	function processResponse(qryTYPE, qryID, qryMSG, emailRawData = null){
		var qryUSER = '<?php echo $_SESSION["mhwltdphp_user"]; ?>';
		// Knock the email out first async then the update to the status which is sync
		if(emailRawData !== null) {
			const emailData = getEmailData(emailRawData.eventString);
			$.ajax({
				type: "POST",
				url: "query-request-samples.php",
				data: { qrytype: 'client_item_list', event_id: qryID },
				success: 	function(items) {
					const itemData = JSON.parse(items);
					$.ajax({
						type: "POST",
						url: "client_samples_review.php?email_operations=1",
						data: { emailArray: emailData, itemArray: itemData },
						async: true
					});
				},
				async: true
			});
		}
		$.ajax({
	  	type: "POST",
		  url: "query-request-samples.php",
		  data: { qrytype: qryTYPE, event_id: qryID, qrymsg: qryMSG, qryuser: qryUSER },
		  success: null,
		  async:false
		});
	}

	function getEmailData(emailDataString) {
		const emailDataArray = emailDataString.split('~');
		var json = ({});
		for(let i = 0; i < emailDataArray.length; i++) {
			switch(i) {
				case 1:
					json.sample_order_number = emailDataArray[1];
					break;
				case 2:
					json.client_name = emailDataArray[2];
					break;
				case 3:
					json.submitted_by = emailDataArray[3];
					break;
				case 4:
					json.order_date = emailDataArray[4];
					break;
				case 5:
					json.event_date = emailDataArray[5];
					break;
				case 6:
					json.created_date = emailDataArray[6];
					break;
				case 7:
					json.event_details = emailDataArray[7];
					break;
				case 8:
					json.delivery_pickup = emailDataArray[8];
					break;
				case 9:
					json.location = emailDataArray[9];
					break;
				case 10:
					json.use_of_samples = emailDataArray[10];
					break;
				case 11:
					json.rushed_order = emailDataArray[11];
					break;
				case 12:
					json.status = emailDataArray[12];
					break;
				case 13:
					json.review_remarks = emailDataArray[13];
					break;
				default:
					break;
			}
		}
		return(json);
	}
	// 0 Sample Order Number 	328
	// 1 Client Name 					MHW Web Demo
	// 2 Submitted By 				DWILCOX client_samples_review.php:291:15
	// 3 Order Dates					2021-12-09 20:00:00
	// 4 Event Details 				This is a sample request for a distributor
	// 5 Delivery/pickup			delivery
	// 6 location							1234 1234 XX 1234 1234
	// 7 Use of samples
	// 8 Review Remarks
	// 9 Rushed Order					0
	// 10 Compliance Hold
	// 11 Approve							buttons
	// 12 Reject							  ...
	// 13 Cancel								...

	$(document).off('click', '.btnca').on('click', '.btnca',function(){
		//alert('approved');
		var thisid = $(this).data('id');
		var thismsg = $('#rr__'+thisid).val();
		var $row = $(this).closest("tr"),       // Finds the closest row <tr>
    $tds = $row.find("td");             // Finds all children <td> elements
		var eventString = "";
		var skip = 4;
		var i  = 0;
		$.each($tds, function() {               // Visits every single <td> element
			console.log($(this).text());        // Prints out the text within the <td>
			eventString += $(this).text() + '~';

		});
		const emailRawData = ({ eventString: eventString });

		processResponse('Approve', thisid, thismsg, emailRawData);

		var trparent = $(this).closest("tr");
		var trdetail = trparent.next();
		if(trdetail.hasClass("detail-view")){
			trdetail.hide();
		}
		trparent.css("color", "green");
		tableBuilder("compliance_sample_list");
		// trparent.slideUp("slow");
		//}
	});


	$(document).off('click', '.btnob').on('click', '.btnob',function(){
		//alert('approved');
		var thisid = $(this).data('id');
		var thismsg = $('#rr__'+thisid).val();

		processResponse('Complete', thisid, thismsg);

		var trparent = $(this).closest("tr");
		var trdetail = trparent.next();
		if(trdetail.hasClass("detail-view")){
			trdetail.hide();
		}
		trparent.css("color", "blue");
		tableBuilder("operations_sample_list");
		// trparent.slideUp("slow");
		//}
	});

	$(document).off('click', '.btnor').on('click', '.btnor',function(r){
		//alert('rejected');
		var thisid = $(this).data('id');
		var thismsg = $('#rr__' + thisid).val();

		//tbd-require lines to be auto-expanded?


		//tbd-ajax post
		processResponse('Reject', thisid, thismsg);

		var trparent = $(this).closest("tr");
		var trdetail = trparent.next();
		if(trdetail.hasClass("detail-view")){
			trdetail.hide();
		}
		trparent.css("color", "red");
		tableBuilder("operations_sample_list");
		// trparent.slideUp("slow");

	});
	$(document).off('click', '.btnoc').on('click', '.btnoc',function(r){
		//alert('rejected');
		var thisid = $(this).data('id');
		var thismsg = $('#rr__' + thisid).val();

		//tbd-ajax post
		processResponse('Cancel', thisid, thismsg);

		var trparent = $(this).closest("tr");
		var trdetail = trparent.next();
		if(trdetail.hasClass("detail-view")){
			trdetail.hide();
		}
		trparent.css("color", "yellow");
		tableBuilder("operations_sample_list");
		// trparent.slideUp("slow");

	});

	$(document).off('click', '.btncr').on('click', '.btncr',function(r){
		//alert('rejected');
		var thisid = $(this).data('id');
		var thismsg = $('#rr__' + thisid).val();

		//tbd-require lines to be auto-expanded?


		//tbd-ajax post
		processResponse('Reject', thisid, thismsg);

		var trparent = $(this).closest("tr");
		var trdetail = trparent.next();
		if(trdetail.hasClass("detail-view")){
			trdetail.hide();
		}
		trparent.css("color", "red");
		tableBuilder("compliance_sample_list");
		// trparent.slideUp("slow");

	});
	$(document).off('Cancel', '.btncc').on('click', '.btncc',function(){
		//alert('rejected');
		var thisid = $(this).data('id');
		var thismsg = $('#rr__' + thisid).val();

		processResponse('Cancel', thisid, thismsg);

		var trparent = $(this).closest("tr");
		var trdetail = trparent.next();
		if(trdetail.hasClass("detail-view")){
			trdetail.hide();
		}
		trparent.css("color", "yellow");
		tableBuilder("compliance_sample_list");
		// trparent.slideUp("slow");

	});


	$(document).off('click', '.btnla').on('click', '.btnla',function(){
		//alert('approved');
		var thisid = $(this).data('id');
		var thisparentid = $(this).data('parentid');
		var thismsg = $('#rrl__'+thisid).val();


		// //tbd-ajax post
		// processResponse('LineRevApp',thisid,thismsg);

		//tbd-disable parent buttons
		$('#btnr__'+thisparentid).hide();
		$('#btna__'+thisparentid).hide();
		var trparent = $(this).closest("tr");
		var trdetail = trparent.next();
		if(trdetail.hasClass("detail-view")){
			trdetail.hide();
		}
		trparent.css("color", "green");
		// trparent.slideUp("slow");
		//}
	});

	$(document).off('click', '.btnlr').on('click', '.btnlr',function(){
		//alert('rejected');
		var thisid = $(this).data('id');
		var thisparentid = $(this).data('parentid');
		var thismsg = $('#rrl__'+thisid).val();

		if(!thismsg || thismsg==''){
			alert('review remark is required');
			$('#rrl__'+thisid).focus();
		}
		else{

			//tbd-ajax post
			processResponse('LineRevRej',thisid,thismsg);

			//tbd-disable parent buttons
			$('#btnr__'+thisparentid).hide();
			$('#btna__'+thisparentid).hide();

			var trparent = $(this).closest("tr");
			var trdetail = trparent.next();
			if(trdetail.hasClass("detail-view")){
				trdetail.hide();
			}
			trparent.css("color", "red");
			// trparent.slideUp("slow");
		}
	});

	$(document).off('click', '.btn_itemtoggle').on('click', '.btn_itemtoggle',function(e) {
		var trparent = $(this).closest("tr");
		var prodindex = $(this).data('index');
		var exp = trparent.find(".detail-icon");
		$( exp ).trigger( "click" );
	});
	//////////////////////////////////////////////////////////////////////////////////////
	//function (to be called within client_samples.php) to update alert area in nav (head.php)
	function refreshAlert(){
		var clientlistH = '<?php echo $_SESSION["mhwltdphp_userclients"]; ?>';
		var qrytypeH = 'STAT_COUNT_unfinalized';
		$.post('query-request-samples.php', {qrytype: qrytypeH, clientlist: clientlistH}, function(dataTH){
			// ajax request query-request.php, send qrytype & client as post variable, return dataX variable, parse into JSON
			var resp = JSON.parse(dataTH);
			$.each( resp, function( key, value ) {
				if(parseInt(value.unfinalizedCount)>0){
					$("#userAlerts").html("<i class=\"fas fa-exclamation-triangle\"></i><a href=\"client_samples.php?v=nf\"> Not Finalized <span class=\"badge badge-light\">"+value.unfinalizedCount+"</span></a>");
				}
				else{
					$("#userAlerts").html("");
				}

			});
		});
	}
	//function to initialize all of the in-modal functionality
	function modalStuff(){
		//from image details to image full preview
		$('.prodimg_thumb').bind('click',function(){
			var imageid = $(this).data('imageid');
			var productid = $(this).data('product');
			var imageurl = 'viewfiles.php?iid='+imageid+'&b='+productid;
			$('.modal-body').load(imageurl,function(){
				$('#fileModalx').modal({show:true});
				modalStuff(); //recursive but necessary to initialize other states on each change
			});
		});
		//back link in modal from image full to image details
		$('.prodimg_back').bind('click',function(){
			var productid = $(this).data('product');
			var producturl = 'viewfiles.php?pid='+productid;
			$('.modal-body').load(producturl,function(){
				$('#fileModalx').modal({show:true});
				modalStuff(); //recursive but necessary to initialize other states on each change
			});
		});
		//initialize prod image modal functionality again
		$('.prodimg_btn').bind('click',function(){
			var productid = $(this).data('product');
			var imageurl = 'viewfiles.php?pid='+productid;
			$('.modal-body').load(imageurl,function(){
				$('#fileModalx').modal({show:true});
				modalStuff(); //recursive but necessary to initialize other states on each change
			});
		});
		//download image button
		$('.dwnld').bind('click', function () {
			var imagefile = $(this).data('url');
			var imagename = $(this).data('imagename');
			$.ajax({
				url: imagefile,
				method: 'GET',
				xhrFields: {
					responseType: 'blob'
				},
				success: function (data) {
					var a = document.createElement('a');
					var url = window.URL.createObjectURL(data);
					a.href = url;
					a.download = imagename;
					a.click();
					window.URL.revokeObjectURL(url);
				}
			});
		});
		//go to upload page for product
		$('.prod_upload').bind('click', function () {
			var prod = $(this).data('prod');
			window.location = 'imageupload.php?pid='+prod;
		});
		$(".prodedit_btn").bind("click", function() {
			var prod = $(this).data("target");
			$("#editprod_"+prod).submit();
		});
	};


	//Bootstrap table
	var $table = $('#clientSampleTable')
	var $remove = $('#remove')
	var selections = []

	function getIdSelections() {
		return $.map($table.bootstrapTable('getSelections'), function (row) {
			var viewname_raw = $("#screenview_name").val();
			if(viewname_raw=="Items") {
				return row.item_id
			}
			else{
				return row.event_id
				//return row.product_mhw_code
			}
		})
	}
	function getCodeSelections() {
		return $.map($table.bootstrapTable('getSelections'), function (row) {
			var viewname_raw = $("#screenview_name").val();
			if(viewname_raw=="Items") {
				return row.item_id
			}
			else{
				//return row.product_id
				return row.product_mhw_code
			}
		})
	}
	function expandTable($detail, row) {
		$detail.bootstrapTable('showLoading');
		subtableBuilder($detail.html('<table class="table-info"></table>').find('table'), 'client_item_list', row.sampleOrderNo);
	}
	function responseHandler(res) {
		$.each(res.rows, function (i, row) {
			row.state = $.inArray(row.id, selections) !== -1
		})
		return res
	}

	function detailFormatter(index, row) {
		var html = []
		$.each(row, function (key, value) {
			html.push('<p><b>' + key + ':</b> ' + value + '</p>')
		})
		return html.join('')
	}

	window.operateEvents = {
		'click .like': function (e, value, row, index) {
			alert('You click like action, row: ' + JSON.stringify(row))
		},
		'click .prodimg_btn': function (e, value, row, index) {
			alert('You click prodimg_btn action, row: ' + JSON.stringify(row))
		},
		'click .remove': function (e, value, row, index) {
			$table.bootstrapTable('remove', {
				field: 'id',
				values: [row.id]
			})
		}
	}

	//buttons
	function operateFormatter(value, row, index) {
		return [
			'<!--<button type="button" class="btn btn-outline-secondary prodimg_btn btn-sm bg_arrow_lightblue" data-product="'+row.product_id+'">-->',
			'<!--Files <span class="badge badge-light">'+row.filecount+'</span></button>-->',
			'<button type="button" class="btn btn-primary btn-sm bg_arrow_blue btn_itemtoggle" data-toggle="collapse" data-index="'+index+'" data-target=".itemprod'+row.product_id+'" data-prod="'+row.product_id+'">',
			'Lines <span class="badge badge-light">'+row.itemcount+'</span></button>',
			'<!--<form id="editprod_'+row.product_id+'" method="POST" action="productsetup.php"><input type="hidden" id="product_id" name="product_id" value="'+row.product_id+'">-->',
			'<!--<input type="hidden" id="product_desc" name="product_desc" value="'+row.product_desc+'">-->',
			'<!--<input type="hidden" id="product_mhw_code" name="product_mhw_code" value="'+row.product_mhw_code+'">-->',
			'<!--<input type="hidden" id="brand_name" name="brand_name" value="'+row.brand_name+'">-->',
			'<!--<input type="hidden" id="client_name" name="client_name" value="'+row.client_name+'"> </form>-->',
			'<!--<button type="button" class="btn btn-dark btn-sm prodedit_btn bg_arrow_darkblue" data-target="'+row.product_id+'">-->',
			'<!--<i class=\"far fa-edit\"></i> Edit</button>-->'
		].join('')
	}
	function subtableBuilder ($el, qrytype, eventId) {
		var data;
		$el.bootstrapTable('showLoading');
		$.post('query-request-samples.php', { qrytype: qrytype, event_id: eventId }, function(d){ // ajax request query-request.php, send qrytype & client as post variable, return dataX variable, parse into JSON
			try {
				data = JSON.parse(d);
			}
			catch(e) {
        console.log(`JSON Data -> ${JSON.stringify(d)}`);
				console.error(`subtableBuilder: Error fetching data -> ${e}`);
				return;
			}
			//item sub table
			$el.bootstrapTable('destroy').bootstrapTable('showLoading').bootstrapTable({
				data: data,
				formatLoadingMessage: function () {
					return '<span class="mhwLoading"><img src="css/img/Logo_Chevron_Animated.gif"/> Loading</span>';
				},
				columns: [
					[{
						field: 'productName',
						title: 'MHW Item Code',
						sortable: true,
						align: 'left'
					},
					{
						title: 'Item Description',
						field: 'itemDescription',
						align: 'left',
						valign: 'middle',
						sortable: true
					},
					{
						title: '# of Bottles Requested',
						field: 'quantity',
						align: 'left',
						valign: 'middle',
						sortable: true
					},
					{
						title: 'Stock UOM',
						field: 'stockUom',
						align: 'left',
						valign: 'middle',
						sortable: true
					}]
				]
			}); //sub table init
		}); //POST
	}



	function tableBuilder(qrytype) {
		var data;
		var clientlist = $("#clientlist").val();
		var val1 = '<?php echo $_GET["ok"]; ?>';
		$('#clientSampleTable').bootstrapTable('showLoading');
		if(qrytype == 'Compliance') {
			qrytype = "compliance_sample_list";
		}
		else if(qrytype == 'Operations') {
			qrytype = "operations_sample_list";
		}
		$.post('query-request-samples.php', { qrytype: qrytype, clientlist: clientlist }, function (dataPT) { // ajax request query-request.php, send qrytype & client as post variable, return dataX variable, parse into JSON
			try {
				data = JSON.parse(dataPT);
				delete data ["button"];
				delete data["query"];
			}
			catch(e) {
				console.error(`tableBuilder: exception -> ${e}`);
				console.error(`tableBuilder: String causing exception -> ${dataPT}`);
				return(false);
			}

			var winH = $(window).height();
			var navH = $(".navbar").height();
			var tblH = (winH-navH)-50;


			//product/supplier views
			$('#clientSampleTable').bootstrapTable('destroy').bootstrapTable('showLoading').bootstrapTable({
				data: data,
				height:tblH,
				stickyHeader:true,
				stickyHeaderOffsetY:60,
				formatLoadingMessage: function () {
					return '<span class="mhwLoading"><img src="css/img/Logo_Chevron_Animated.gif"/> Loading</span>';
				},
				detailView: true,
				onExpandRow: function (index, row, $detail) {
					expandTable($detail,row)
				},
				columns: [
					[
						{
							field: 'sampleOrderNo',
							title: 'Sample Order #',
							align: 'left',
							valign: 'middle',
							sortable: true
						},
						{
							field: 'clientName',
							title: 'Client Name',
							align: 'left',
							valign: 'middle',
							sortable: true
						},
						{
							field: 'submittedBy',
							title: 'Submitted By',
							align: 'left',
							valign: 'middle',
							sortable: true
						},
						{
							field: 'orderDate',
							title: 'Sample Delivery Date',
							align: 'left',
							valign: 'middle',
							sortable: true
						},
						{
							field: 'eventDate',
							title: 'Event Date',
							align: 'left',
							valign: 'middle',
							sortable: true
						},
						{
							field: 'createdDate',
							title: 'Created Date',
							align: 'left',
							valign: 'middle',
							sortable: true
						},
						{
							field: 'eventDetails',
							title: 'Event Details',
							align: 'left',
							valign: 'middle',
							sortable: true
						},
						{
							field: 'deliveryDetails',
							title: 'Delivery/Pickup',
							align: 'left',
							valign: 'middle',
							sortable: true
						},
						{
							field: 'location',
							title: 'Location',
							align: 'left',
							valign: 'middle',
							sortable: true
						},
						{
							field: 'useOfSamples',
							title: 'Use of Samples',
							align: 'left',
							valign: 'middle',
							sortable: true
						},
						{
							field: 'Value8',
							title: 'Review Remarks',
							align: 'left',
							valign: 'middle',
							sortable: true
						},
						{
							field: 'rushedOrder',
							title: 'Rushed Order',
							align: 'left',
							valign: 'middle',
							sortable: true
						},
						{
							field: 'processed',
							title: 'Status',
							align: 'left',
							valign: 'middle',
							sortable: true
						},
						{
							field: 'Value9',
							title: 'Action',
							align: 'left',
							sortable: false,
							visible: true
						}]

					]
				}); //table init


				var viewname_raw = $("#screenview_name").val();
				if(viewname_raw == "Not Finalized") {

					$table.bootstrapTable('checkAll');  //check all Not Finalized by default
				}

				var orderkey = '<?php echo $ok; ?>';
				if(orderkey && orderkey!=''){
					$table.bootstrapTable('expandAllRows');
				}
				//expand-row.bs.table

				modalStuff(); //bind modal & buttons

				$table.on('check.bs.table uncheck.bs.table ' + 'check-all.bs.table uncheck-all.bs.table', function () {
					$remove.prop('disabled', !$table.bootstrapTable('getSelections').length)

					// save your data, here just save the current page
					selections = getIdSelections()
					// push or splice the selections if you want to save all data selections

				})
				$table.on('all.bs.table', function (e, name, args) {
					modalStuff(); //when anything happens (filter, click, etc) re-bind modal & buttons
					hideButtons(qrytype, data);
					setTextAreaOnBlur(qrytype, data);
				})
				$remove.click(function () {
					var ids = getIdSelections()
					$table.bootstrapTable('remove', {
						field: 'id',
						values: ids
					})
					$remove.prop('disabled', true)
				})
				//Hide the buttons if this has been approved already as it is now for operations.

				hideButtons(qrytype, data);
				setTextAreaOnBlur(qrytype, data);

			}); //POST
		} //unnamed function wrapping table init

		var initialview = '<?php echo $v; ?>';
		tableBuilder("compliance_sample_list"); //initialize table
		$("#screenview_name").on("change", function () {
			var v =  $(this).val();
			$("#finalize").hide();
			tableBuilder(v);
		});

		$("#btn_toggle_sub1").on("click", function () {
			$table.bootstrapTable('expandAllRows');
		});
		$("#btn_toggle_sub2").on("click", function () {
			$table.bootstrapTable('collapseAllRows');
		});


	});  //document ready

</script>
<!-- prepend.php -->
<div class="container-fluid">
<div class="row justify-content-md-left float-left">
<div class="col-md-auto oneField field-container-D" id="client_name-D">
<label id="client_name-L" class="label preField " for="client_name"><b>Client Name</b></label>
<div class="inputWrapper">
  <select id="client_name" name="client_name" title="Client Name" aria-required="true">
			<?php
			$clients = explode(";",$_SESSION['mhwltdphp_userclients']);
  if (count($clients)>1) {
              echo "<option value='all' class=''>ALL</option>";
          }
			foreach ($clients as &$clientvalue) {
    echo "<option value='$clientvalue' class=''>$clientvalue</option>";
			}
			?>
		</select>
	</div>
</div>
<div class="col-md-auto oneField field-container-D" id="screenview_name-D">
	<label id="screenview_name-L" class="label preField " for="screenview_name"><b>View</b></label>
	<div class="inputWrapper">
		<select id="screenview_name" name="screenview_name" title="Screen View" aria-required="true">
			<?php
			$screenviews = array("Compliance","Operations");
			foreach ($screenviews as &$screenviewvalue) {
				echo "<option value=\"".$screenviewvalue."\" class=\"\">".$screenviewvalue."</option>";
			}
			?>
		</select>
	</div>
</div>
<?php
if($param1 && $param1!==''){
	?>
	<div class="col-md-auto oneField field-container-D" id="client_name-D">
		<label id="client_name-L" class="label preField " for="client_name"><b>Order Filter: <?php echo $param1; ?></b></label>
		<div class="inputWrapper">
			<a href='orderreview.php'>clear</a>
		</div>
	</div>
	<?php
}
else{
	?>
	<div class="toolbar btn-group">
		<button id="btn_toggle_sub1" class="btn btn-secondary"><i class="fas fa-expand"></i> Expand All</button>
		<button id="btn_toggle_sub2" class="btn btn-secondary"><i class="fas fa-compress"></i> Collapse All</button>
	</div>
	<?php
}
?>

</div>
</div>
<table id="clientSampleTable" class="table table-hover" data-toggle="table" data-pagination="true" data-show-pagination-switch="true" data-page-size="25" data-show-columns="true" data-show-toggle="true" data-search="true" data-show-export="true" data-toolbar=".toolbar" data-toolbar-align="right">


	<?php
	$clientlist =  "'".str_replace(";","','",$_SESSION['mhwltdphp_userclients'])."'";

}
}
?>
</tbody>
</table>

<input type="hidden" id="clientlist" value="<?php echo  $clientlist; ?>">

<div class="modal modal-wide fade" id="fileModalx" tabindex="-1" role="dialog" aria-labelledby="fileModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="fileModalLabel">Files</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

			</div>
			<div id='preview'></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<script>

$(document).ready(function(){


	//initialize prod image modal functionality
	$('.prodimg_btn').on('click',function(){
		var productid = $(this).data('product');
		var imageurl = 'viewfiles.php?pid='+productid;
		//alert(imageurl);
		$('.modal-body').load(imageurl,function(){
			$('#fileModalx').modal({show:true});
		});
	});
	//initialize prod image download functionality for outside of modal
	$('.dwnld').on('click', function () {
		var imagefile = $(this).data('url');
		var imagename = $(this).data('imagename');
		$.ajax({
			url: imagefile,
			method: 'GET',
			xhrFields: {
				responseType: 'blob'
			},
			success: function (data) {
				var a = document.createElement('a');
				var url = window.URL.createObjectURL(data);
				a.href = url;
				a.download = imagename;
				a.click();
				window.URL.revokeObjectURL(url);
			}
		});
	});
});

</script>

</div>
</div>
</body>
</html>
