<?php
include('settings.php');
include('functions.php');
header('Content-Type: application/json');

//$upload_dirX = $_SERVER['DOCUMENT_ROOT'] . "\\forms\\fileuploads\\ajax\\";
//echo $upload_dir ;
//if (is_dir($upload_dirX) && is_writable($upload_dirX)) {
if($_GET['from']=='productsetup'){
    if ($_FILES["tfa_image"]["error"] == UPLOAD_ERR_OK)
	{
		$file = $_FILES["tfa_image"]["tmp_name"];
		$file0 = convert_ascii($_FILES["tfa_image"]["tmp_name"][0]);
		$filename =  convert_ascii($_FILES['tfa_image']['name']);
		$filename = str_replace(' ','',$filename);
		$filename = str_replace('%20','',$filename);
		$filename = str_replace('+','',$filename);
		$filename = str_replace("'","",$filename);
		$imageFileType = pathinfo($filename,PATHINFO_EXTENSION);
		$ext = strtolower('.'.$imageFileType);
		if(($extlimit == "yes") && (!in_array($ext, $limitedext))){
			echo json_encode([
				'result' => 'error',
				'message' => "Upload Failure: File uploads of this type ".$ext." are not allowed.\n"
			]);
			exit;
		}
		// now you have access to the file being uploaded
		//perform the upload operation.
		copy($_FILES['tfa_image']['tmp_name'],$upload_dir."ajax/temp_".strtotime("now")."_".$file0.".".$imageFileType);
		
		$moved = move_uploaded_file( $file, $upload_dir."ajax/".strtotime("now")."_".$filename);
		if($moved)
		{
			echo json_encode([
				'result' => 'success',
				'name' => $filename,
				'path' => $upload_dir."ajax/temp_".strtotime("now")."_".$file0.".".$imageFileType
			]);
			exit;
		}
	}
	echo json_encode([
		'result' => 'error',
		'message' => error_get_last()
	]);
}
elseif($_GET['from']=='imageupload'){
    if ($_FILES["filetoupload"]["error"] == UPLOAD_ERR_OK)
	{
		$file = $_FILES["filetoupload"]["tmp_name"];
		$file0 = convert_ascii($_FILES["filetoupload"]["tmp_name"][0]);
		$filename =  convert_ascii($_FILES['filetoupload']['name']);
		$filename = str_replace(' ','',$filename);
		$filename = str_replace('%20','',$filename);
		$filename = str_replace('+','',$filename);
		$filename = str_replace("'","",$filename);
		$imageFileType = pathinfo($filename,PATHINFO_EXTENSION);
		$ext = strtolower('.'.$imageFileType);
		if(($extlimit == "yes") && (!in_array($ext, $limitedext))){
			echo json_encode([
				'result' => 'error',
				'message' => "Upload Failure: File uploads of this type ".$ext." are not allowed.\n"
			]);
			exit;
		}
		// now you have access to the file being uploaded
		//perform the upload operation.
		copy($_FILES['filetoupload']['tmp_name'],$upload_dir."ajax/temp_".strtotime("now")."_".$file0.".".$imageFileType);
		
		$moved = move_uploaded_file( $file, $upload_dir."ajax/".strtotime("now")."_".$filename);
		if($moved)
		{
			echo json_encode([
				'result' => 'success',
				'name' => $filename,
				'path' => $upload_dir."ajax/temp_".strtotime("now")."_".$file0.".".$imageFileType
			]);
			exit;
		}
		}
	echo json_encode([
		'result' => 'error',
		'message' => error_get_last()
	]);
	}


?>