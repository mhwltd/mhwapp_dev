<?php
$siteroot = "https://mhwdev-apprise.azurewebsites.net/forms";

//CRD login
//$sitelogin = "https://mhwdev-apprise.azurewebsites.net/forms/login.php";
//$sitelogin = "http://mhwwebdev.nstwebbeta.com/RamcoCurrent/";
//$sitelogin = "http://mhwapprise.nstwebbeta.com/";


$sitelogin = "http://apprisetest.nstwebbeta.com/";

/**** EVENTS & SAMPLES ****/
// Compliance Email
//$complianceEmailTo = "ngupta@mhwltd.com";
$fromAddress = "notifications@mhwltd.com";
$fromPassword = "e8!BG3hG"; //"dgerM>4d";

//Compliance Email - New Order Submission
$NewOrderComplianceEmailTo = "samplesandevents@mhwltd.com";  
//$NewOrderComplianceEmailTo = "samplesandevents@mhwltd.com"; // previously compliance@mhwltd.com
$NewOrderComplianceEmailSubject = "TEST: A new Sample/Event Order #{enter_order_number_here} has been submitted";

//Operations Email - New Order Approval
$NewOrderOperationsEmailSubject = "TEST: Sample/Event Order #{enter_order_number_here} has been approved";

//Order Email - Scheduled Report
$ScheduledEmailSubject = "TEST: Sample/Events to be Completed";

//Rushed Order Email - Scheduled Report
$RushedOrderEmailTo = "ngupta@mhwltd.com"; 
//$RushedOrderEmailTo = "posting@mhwltd.com"; 
$RushedOrderEmailSubject = "TEST: ".date("YFj")."_JE_REGISTRATION - RUSHED VET FEE_AutomatedNotification";
//Default email when Ops Team email is not found
$defaultOpsEmail = "ngupta@mhwltd.com"; 
//$defaultOpsEmail = "operations@mhwltd.com";  
/**** EVENTS & SAMPLES ****/

//$trayFinalizedWorkflow = "https://1d04e95c-56e9-4aa3-8e3c-59e18a0f1da4.trayapp.io"; //Forms - Product Finalized - SQL sp new
/*"https://28690257-c3bc-4df3-a612-e3b0b1a033b9.trayapp.io"; */ //Forms - Product Finalized - SQL sp 
//$trayFormSubmitWorkflow = "https://40271a86-c54b-4618-ad56-81d1728233e2.trayapp.io"; //Forms - Product Submitted  - SQL (same in Prod & Dev - FormsDev ignored by Tray)

//file attachment 
$size_bytes_limit = 7680000; //ie. 51200 bytes = 50KB // 7680000 = 7500KB (7.5MB);
$extlimit = "yes"; //Limit the extensions of file uploads (yes/no)
//$limitedext = array(".jpg",".jpeg",".jpe",".tif",".tiff",".gif",".png",".bmp");
$limitedext = array(".jpg",".jpeg",".jpe",".tif",".tiff",".pdf");
$upload_dir = "fileuploads/";
$thumb_dir  = $upload_dir."thumbs/";

//Add TTB Number to end of url to check approved COLA
$ttblink="https://www.ttbonline.gov/colasonline/viewColaDetails.do?action=publicFormDisplay&ttbid=";
$opt_effective_month_prior_count = 3;
$opt_effective_month_forward_count = 4;
$showDistributorsArray = [
    
];

$allowToAddNewDistributor = false;

/* Validation for Non Finiliazed Product , set 1 for required and 0 for optional */
/* product-level fields*/
$product_validation_matrix=array(
'brand_name'=>1,
'product_desc'=>1,
'TTB_ID'=>1,
'federal_type'=>1,
'compliance_type'=>1,
'product_class'=>1,
'mktg_prod_type'=>1,
'bev_type'=>1,
'country'=>1,
'alcohol_pct'=>1
);

/* product-level dropdown fields are set to valid option */
$product_dropdown_valid_matrix=array(
'federal_type'=>1,
'compliance_type'=>1,
'product_class'=>1,
'mktg_prod_type'=>1,
'bev_type'=>1
);

/* checked Product is linked to Supplier */
$checked_product_is_linked_to_supplier=1;

/* supplier-level fields */
$supplier_validation_matrix=array(
'supplier_name'=>1,
'supplier_contact'=>1,
'supplier_fda_number'=>1,
'supplier_address_1'=>1,
'supplier_country'=>1,
'supplier_phone'=>1,
'supplier_email'=>1
);


/* File Requirements */
$checking_supported_file_extension=1;
$checking_inactive_image_override_record=1;

/* TTB_ID Requirements */
$checking_approved_federal_cola_record=1;
$checking_ttb_id_min_digit_length=1; 


/* Items-level fields */
$item_validation_matrix=array(
'container_type'=>1,
'container_size'=>1,
'bottle_material'=>1,
'stock_uom'=>1,
'bottles_per_case'=>1,
'chill_storage'=>1,
'outer_shipper'=>1,
'vintage'=>1
);

/* BLR State Rules Info Permitted for Client */
$client_permitted_rules_info=array(
'blr_rule_franchise'=>1,
'blr_rule_at_rest'=>1,
'blr_rule_tax'=>1,
'blr_rule_state_reg'=>1,
'blr_rule_approvals'=>1,
'blr_rule_who'=>1,
'blr_rule_ctrl'=>1,
'blr_rule_key'=>1,
'blr_rule_additional_licensing'=>1,
'blr_rule_approval_time'=>1,
'blr_rule_renewal'=>1,
'blr_rule_renew_dates'=>1,
'blr_rule_wine_ff'=>1,
'blr_rule_spirit_ff'=>1,
'blr_rule_beer_ff'=>1
);

/* BLR State Rules Info Permitted for admin & SuperAdmin */
$admin_permitted_rules_info=array(
'blr_rule_franchise'=>1,
'blr_rule_at_rest'=>1,
'blr_rule_tax'=>1,
'blr_rule_state_reg'=>1,
'blr_rule_approvals'=>1,
'blr_rule_who'=>1,
'blr_rule_ctrl'=>1,
'blr_rule_key'=>1,
'blr_rule_additional_licensing'=>1,
'blr_rule_approval_time'=>1,
'blr_rule_renewal'=>1,
'blr_rule_renew_dates'=>1,
'blr_rule_wine_ff'=>1,
'blr_rule_spirit_ff'=>1,
'blr_rule_beer_ff'=>1
);

/*BLR State Rules Modal Label Mapping*/
$blr_rules_info_level=array(
'blr_rule_franchise'=>'Franchise',
'blr_rule_at_rest'=>'At Rest',
'blr_rule_tax'=>'Tax',
'blr_rule_state_reg'=>'State Reg',
'blr_rule_approvals'=>'Approvals',
'blr_rule_who'=>'Who',
'blr_rule_ctrl'=>'Control',
'blr_rule_key'=>'Key',
'blr_rule_additional_licensing'=>'Additional Licensing',
'blr_rule_approval_time'=>'Approval Time',
'blr_rule_renewal'=>'Renewal',
'blr_rule_renew_dates'=>'Renew Dates',
'blr_rule_wine_ff'=>'Wine FF',
'blr_rule_spirit_ff'=>'Spirit FF',
'blr_rule_beer_ff'=>'Beer FF'
);

?>