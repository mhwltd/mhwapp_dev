<?php
//session_start();
include("sessionhandler.php");
include("prepend.php");
if(!isset($_SESSION['mhwltdphp_user'])){
	header("Location: login.php");
}
else{
	include('head.php');
?>
<div class="container">	

	<div class="row">
	
	  <div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Product &amp; Item Setup - QA</h5>
	        <p class="card-text">product setup - apprise test</p>
			<a href="productsetup.php" class="btn btn-primary bg_arrow_blue">Visit</a>
	      </div>
	    </div>
	  </div>

	  <div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">View Products &amp; Items - QA</h5>
	        <p class="card-text">view products - apprise test</p>
			<a href="viewproducts.php" class="btn btn-primary bg_arrow_blue">Visit</a>
	      </div>
	    </div>
	  </div>

	  <div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Attach Product Images - QA</h5>
	        <p class="card-text">image upload - apprise test</p>
			<a href="imageupload.php" class="btn btn-primary bg_arrow_blue">Visit</a>
	      </div>
	    </div>
	  </div>
	  <!-- !HARD-CODED!  ACOUNTID: TEST MHW -->
	  <!--<div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Product & Item Setup - Form Assembly SSI (TEST MHW)</h5>
	        <p class="card-text">productform</p>
			<a href="productform.php?acctid=0011N00001M8OzvQAF" class="btn btn-primary">Visit</a>
	      </div>
	    </div>
	      </div>
	  <div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Product & Item Setup Login - Form Assembly SSI</h5>
	        <p class="card-text">product_interstitial</p>
			<a href="product_interstitial.php?email=<?php echo urlencode($_SESSION['mhwltdphp_useremail']); ?>&lastname=<?php echo urlencode($_SESSION['mhwltdphp_userlastname']); ?>" class="btn btn-primary">Visit</a>
	    </div>
	  </div>
	  </div>-->
	  
	</div>

	<?php if($_SESSION['mhwltdphp_usertype'] == "SUPERUSER" || $_SESSION['mhwltdphp_usertype'] == "ADMIN" ){ ?>
	<div class="row">
	  <div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Compliance Order Review - QA</h5>
	        <p class="card-text">order review - apprise test</p>
			<a href="orderreview.php" class="btn btn-primary bg_arrow_blue">Visit</a>
	      </div>
	    </div>
	  </div>

	  <div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Compliance Product Review - QA</h5>
	        <p class="card-text">product review - apprise test</p>
			<a href="productreview.php" class="btn btn-primary bg_arrow_blue">Visit</a>
	      </div>
	    </div>
	  </div>

	  <div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Distributor Registration Cancel - QA</h5>
	        <p class="card-text">product auto fails compliance - apprise test</p>
			<a href="product_state_status.php" class="btn btn-primary bg_arrow_blue">Visit</a>
	      </div>
	    </div>
	  </div>
	</div>
	<div class="row">
	  <div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Product Compliance Status - QA</h5>
	        <p class="card-text">product compliance status - apprise test</p>
			<a href="prod_comp_status.php" class="btn btn-primary bg_arrow_blue">Visit</a>
	      </div>
	    </div>
	  </div>

	  <div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Event Notifications &amp; Samples - QA</h5>
	        <p class="card-text">client samples review - apprise test</p>
			<a href="client_samples_review.php" class="btn btn-primary bg_arrow_blue">Visit</a>
	      </div>
	    </div>
	  </div>

	  <div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">View Suppliers</h5>
	        <p class="card-text">view suppliers - apprise test</p>
			<a href="viewsuppliers.php" class="btn btn-primary bg_arrow_blue">Visit</a>
	      </div>
	    </div>
	  </div>

	  <!--EMPTY-->
	  <!--EMPTY-->
	</div>
	<?php 
	} 
	else{
		include("dbconnect.php");
		$beta_client_user=false;
		$clientsBETA = explode(";",$_SESSION['mhwltdphp_userclients']);

		$conn = sqlsrv_connect($serverName, $connectionOptions);
		if( $conn === false) die( print_r( sqlsrv_errors(), true));

		foreach ($clientsBETA as &$clientBETAvalue) {
			
			//if($clientBETAvalue=='MHW Web Demo') { $beta_client_user=true; }

			$tsql= "SELECT * FROM [mhw_app_client_access] WHERE [user_application] = 'BLR_Status' and [active] = 1 and [client_name] = '".$clientBETAvalue."'";
			$stmt = sqlsrv_query( $conn, $tsql);
			if ( $stmt === false ) die( print_r( sqlsrv_errors(), true));
			while ( $row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC) ) { 
				$beta_client_user=true;
			}
		}

		sqlsrv_free_stmt($stmt);

		if($beta_client_user){
		?>
			<div class="row">
			  <div class="col-sm-4">
				<div class="card">
				  <div class="card-body">
					<h5 class="card-title">Product Compliance Status - QA</h5>
					<p class="card-text">product compliance status - apprise test</p>
					<a href="prod_comp_status.php" class="btn btn-primary bg_arrow_blue">Visit</a>
				  </div>
				</div>
			  </div>
			  <!--EMPTY-->
			  <!--EMPTY-->
			</div>
		<?php
		}

		$smpl_client_user=false;
		$clientsSMPL = explode(";",$_SESSION['mhwltdphp_userclientcodes']);

		$conn = sqlsrv_connect($serverName, $connectionOptions);
		if( $conn === false) die( print_r( sqlsrv_errors(), true));

		foreach ($clientsSMPL as &$clientSMPLvalue) {

			$tsql= "SELECT * FROM [mhw_app_client_access] WHERE [user_application] = 'Event_Samples' and [active] = 1 and [client_code] = '".$clientSMPLvalue."'";
			$stmt = sqlsrv_query( $conn, $tsql);
			if ( $stmt === false ) die( print_r( sqlsrv_errors(), true));
			while ( $row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC) ) { 
				$smpl_client_user=true;
			}
		}

		sqlsrv_free_stmt($stmt);

		if($smpl_client_user){
		?>

	   <div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Event Notifications &amp; Sample Request Form - QA</h5>
	        <p class="card-text">client samples - apprise test</p>
			<a href="client_samples.php" class="btn btn-primary bg_arrow_blue">Visit</a>
	      </div>
	    </div>
	  </div>

	<?php
		}
	}
	?>

	<div class="row">

		<div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Price Posting - QA</h5>
	        <p class="card-text">view pricing - apprise test</p>
			<a href="viewpricing.php" class="btn btn-primary bg_arrow_blue">Visit</a>
	      </div>
	    </div>
	  </div>

	   <div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">International Purchase Order Request Form - QA</h5>
	        <p class="card-text">international po request - apprise test</p>
			<a href="mhw_po_request.php" class="btn btn-primary bg_arrow_blue">Visit</a>
	      </div>
	    </div>
	  </div>


	</div>

	<div class="row">

	  	<div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Wholesale Customer Set-up Form - QA</h5>
	        <p class="card-text">wholesale customer - apprise test</p>
			<a href="mhw_wholesale_form.php" class="btn btn-primary bg_arrow_blue">Visit</a>
	      </div>
	    </div>
	  </div>

	   <div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Retail Customer Set-up Form - QA</h5>
	        <p class="card-text">retail customer - apprise test</p>
			<a href="mhw_retail_form.php" class="btn btn-primary bg_arrow_blue">Visit</a>
	      </div>
	    </div>
	  </div>

	  <!--<div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Brand Label Registration - QA</h5>
	        <p class="card-text">blr entry & blr view status - apprise test</p>
			<a href="blr.php" class="btn btn-primary bg_arrow_blue">BLR</a>
			<a href="blr-view.php" class="btn btn-primary bg_arrow_blue">BLR Status</a>
	      </div>
	    </div>
	  </div>-->
	  <!-- !HARD-CODED!  ACOUNTID: TEST MHW -->
	  <!--<div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Price Posting - Form Assembly SSI (TEST MHW)</h5>
	        <p class="card-text">pricingform</p>
			<a href="pricingform.php?acctid=0011N00001M8OzvQAF" class="btn btn-primary">Visit</a>
	      </div>
	    </div>
	  </div>

	  <div class="col-sm-4">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Price Posting Login - Form Assembly SSI</h5>
	        <p class="card-text">pricing_interstitial</p>
			<a href="pricing_interstitial.php?email=<?php echo urlencode($_SESSION['mhwltdphp_useremail']); ?>&lastname=<?php echo urlencode($_SESSION['mhwltdphp_userlastname']); ?>" class="btn btn-primary">Visit</a>
	      </div>
	    </div>
	  </div>-->
	  
	</div>

<!--
<div class="row">
  <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Product & Item Setup Login (cont) - Form Assembly SSI</h5>
        <p class="card-text">productlogin2</p>
		<a href="productlogin2.php?email=<?php echo urlencode($_SESSION['mhwltdphp_useremail']); ?>&lastname=<?php echo urlencode($_SESSION['mhwltdphp_userlastname']); ?>" class="btn btn-primary">Visit</a>
      </div>
	  </div>

	</div>
</div>-->

<!--
	<div class="row">
  <div class="col-sm-6">
	    <div class="card">
	      <div class="card-body">
        <h5 class="card-title">Product & Item Setup Login - Form Assembly SSI</h5>
        <p class="card-text">product_interstitial</p>
		<a href="product_interstitial.php?email=<?php echo urlencode($_SESSION['mhwltdphp_useremail']); ?>&lastname=<?php echo urlencode($_SESSION['mhwltdphp_userlastname']); ?>" class="btn btn-primary">Visit</a>
	      </div>
	    </div>
	  </div>

  <div class="col-sm-6">
	    <div class="card">
	      <div class="card-body">
        <h5 class="card-title">Price Posting Login - Form Assembly SSI</h5>
        <p class="card-text">pricing_interstitial</p>
		<a href="pricing_interstitial.php?email=<?php echo urlencode($_SESSION['mhwltdphp_useremail']); ?>&lastname=<?php echo urlencode($_SESSION['mhwltdphp_userlastname']); ?>" class="btn btn-primary">Visit</a>
	      </div>
	    </div>
	  </div>

</div>-->

	<!--<div class="row">-->
	
 	   <!--!HARD CODED!  ACCOUNT: SEEDLIP-->
	  <!--<div class="col-sm-6">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Web Access Form - Native</h5>
	        <p class="card-text">requestaccess</p>
			<a href="requestaccess.php?acctname=Seedlip" class="btn btn-primary">Visit</a>
	      </div>
	      </div>
	  </div>-->
	  
	  <!--!HARD CODED!  ACCOUNT: SEEDLIP-->
	 <!-- <div class="col-sm-6">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Web Access Form - Form Assembly SSI</h5>
	        <p class="card-text">webaccessform</p>
			<a href="webaccessform.php?acctname=Seedlip" class="btn btn-primary">Visit</a>
	    </div>
	  </div>
	  </div>-->
	  
	<!--</div>-->

</div>
	
<?php	
}
?>
