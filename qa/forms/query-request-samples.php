<?php
include('settings.php');
//include("sessionhandler.php");
include('functions.php');
include('settings.php');
error_reporting(E_ALL); //displays an error

session_start();
if(!isset($_SESSION['mhwltdphp_user'])) {
	die( "Not authenticated !" );
}
$isAdmin = in_array($_SESSION['mhwltdphp_usertype'], ['ADMIN','SUPERUSER']);
include("dbconnect.php");

//Establishes the connection
$conn = sqlsrv_connect($serverName, $connectionOptions);
if( $conn === false) {
		die( print_r( sqlsrv_errors(), true));
}

if($_SESSION['mhwltdphp_user']!='' && $_POST['qrytype']!='') {
	$trksql= "INSERT INTO [mhw_app_workflow] VALUES ('".$_POST['qrytype']."', 0, GETDATE(), GETDATE(), '".$_SESSION['mhwltdphp_user']."', 1, 0)";
	$trkResults= sqlsrv_query($conn, $trksql);
}

$thisUser = $_SESSION['mhwltdphp_user'];
$processed = "unset";
$fid = null;
if(isset($_GET['fileId'])) {
	$fid = $_GET['fileId'];
}

$eventId = $_POST['event_id'];
$btnString = "unset";
$itemWhereString = "";
if($_POST['qrytype'] == 'compliance_sample_list' ) {
	$btnString = ', \'<button id=\'+char(39)+\'btnca__\'+cast(e.[event_id] as varchar)+char(39)+\' data-id="\'+cast(e.[event_id] as varchar)+\'" class="btnca btn btn-success btn-lg"><i class="fas fa-check-double"></i>Approve</button>
	<button id=\'+char(39)+\'btncr__\'+cast(e.[event_id] as varchar)+char(39)+\' data-id="\'+cast(e.[event_id] as varchar)+\'" class="btncr btn btn-danger btn-lg"><i class="fas fa-ban"></i>Reject</button>
	<button id=\'+char(39)+\'btncc__\'+cast(e.[event_id] as varchar)+char(39)+\' data-id="\'+cast(e.[event_id] as varchar)+\'" class="btncc btn btn-warning btn-lg"><i class="fas fa-check-double"></i>Cancel</button>\' as [Value9]';
}
else if($_POST['qrytype'] == 'operations_sample_list') {
	$btnString = ', \'<button id=\'+char(39)+\'btnob__\'+cast(e.[event_id] as varchar)+char(39)+\' data-id="\'+cast(e.[event_id] as varchar)+\'" class="btnob btn btn-primary btn-lg"><i class="fas fa-check-double"></i>Complete</button>
	<button id=\'+char(39)+\'btnoc__\'+cast(e.[event_id] as varchar)+char(39)+\' data-id="\'+cast(e.[event_id] as varchar)+\'" class="btnoc btn btn-warning btn-lg"><i class="fas fa-check-double"></i>Cancel</button>\'  as [Value9]';
}
else {
	// If its just a client then they don't have any options yet.
	$itemWhereString + "WHERE p.[event_id] = $eventId";
}

$thisUser = $_SESSION['mhwltdphp_user'];
$tsql = "SELECT --------- WAT! ---->" . $_POST['qrytype'] ."-----" .$_POST['clientlist'] . " --------- " . $thisUser;
 
if($_POST['qrytype'] === 'client_sample_list')
{
	if(isset($_POST['clientlist']) && $_POST['clientlist'] !== '') {
		$tsql= "SELECT
        e.[event_id] AS 'sampleOrderNo',
				e.[created_by] AS 'submittedBy',
				e.[clientname] AS 'clientName',
				CONVERT(varchar(10), e.[eventdate], 20) AS 'eventDate',
				CONVERT(varchar(10), e.[samplerequestdate], 20) AS 'orderDate',
				CONVERT(varchar, DATEADD(HOUR, -4, e.[created_date]), 120) AS 'createdDate',
	     	e.[requesttype] + '<br />' + ISNULL(e.[eventname] + '<br />', '') + ISNULL(e.[eventdesc] + '<br />', '') + ISNULL(e.[address] + '<br />', '') + ISNULL(e.[address2] + '<br />', '') + ISNULL(e.[city] + '<br />', '') + ISNULL(e.[state] + '<br />', '') + ISNULL(e.[zip] + '<br />' , '') + ISNULL(e.[country] + '<br />', '')  +
					ISNULL(STUFF((SELECT N'<a href=".$siteroot."/".$upload_dir."temp/' + REPLACE(f.[fileName], ' ', '%20') + ' download=' + REPLACE(f.[fileName], ' ', '%20') +'>' + f.[filename] + '</a><br />' FROM [dbo].[mhw_event_file] f WHERE e.[event_id] = f.[event_id] FOR XMl PATH, TYPE).value(N'.[1]', N'nvarchar(max)'), 1, 0, '<br />'), '') + 
					--ISNULL(STUFF((SELECT N'<a href=".$siteroot."/query-request-samples.php?fileId=' + TRIM(STR(f.[id])) + ' download=' + REPLACE(f.[fileName], ' ', '%20') +'>' + f.[filename] + '</a><br />' FROM [dbo].[mhw_event_file] f WHERE e.[event_id] = f.[event_id] FOR XMl PATH, TYPE).value(N'.[1]', N'nvarchar(max)'), 1, 0, '<br />'), '') + 
					CASE WHEN e.eventtype = 1 THEN 'Public Event' WHEN e.eventtype = 2 THEN 'Invitation Only Event' END  AS 'eventDetails',
        e.[deliverytype] AS 'deliveryDetails',
				e.[delivery],
				(ISNULL(e.[pickuptxt] + '<br />' , '') + ISNULL(e.[pickupdisttxt] + '<br />' , '') + ISNULL(e.[pickupothertxt] + '<br />' , '') + ISNULL(e.[deliveryaddress], '') + ' ' + ISNULL(e.[deliverycity], '') + ' ' + ISNULL(e.[deliverystate], '') + ' ' + ISNULL(e.[deliveryzip], '') + ' ' + ISNULL(e.[deliverycountry], '')) AS 'location',
				e.[deleted],
				e.[sampleusage] AS 'useOfSamples',
				e.[processed]
				-- fileName=STUFF((SELECT N'<a href=query-samples.php?fileId=' + TRIM(STR(f.[id])) + '>' + f.[filename] + '</a>' FROM [dbo].[mhw_event_file] f WHERE e.[event_id] = f.[event_id] FOR XMl PATH, TYPE).value(N'.[1]', N'nvarchar(max)'),	1, 0, '<br />')
      FROM
        [dbo].[mhw_app_event_form] e
			-- LEFT JOIN
			-- 	[dbo].[mhw_event_file] f ON e.[event_id] = f.[event_id]
      WHERE
       --e.[created_by] = '$thisUser'
	   e.[client_code] = ".trim($_POST['clientlist'])."
      ORDER BY CASE WHEN e.[processed] = 'Compliance Hold' THEN 1 WHEN e.[processed] = 'Approved' THEN 2 WHEN e.[processed] = 'Completed' THEN 3 WHEN e.[processed] = 'Rejected' THEN 4 WHEN e.[processed] = 'Cancelled' THEN 5 END,
				e.[created_date] DESC";
	}
}
else if($_POST['qrytype'] === 'compliance_sample_list' || $_POST['qrytype'] === 'operations_sample_list') {
	if(isset($_POST['clientlist']) && $_POST['clientlist'] !== '') {
		$txtArea = '\'<textarea class="ReviewRemarksClass" cols=30 rows=4 maxlength=250 id=\'+char(39)+\'rr__\' + cast(e.[event_id] as varchar) + char(39) + \'>\'+ ISNULL(e.[notes], \'\') +\'</textarea>\' as [Value8]';
		$caseOrder = 'CASE WHEN e.[processed] = \'Compliance Hold\' THEN 1 WHEN e.[processed] = \'Approved\' THEN 2 WHEN e.[processed] = \'Completed\' THEN 3 WHEN e.[processed] = \'Rejected\' THEN 4 WHEN e.[processed] = \'Cancelled\' THEN 5 END';
		if($_POST['qrytype'] === 'operations_sample_list') {
			$caseOrder = 'CASE WHEN e.[processed] = \'Approved\' THEN 1 WHEN e.[processed] = \'Compliance Hold\' THEN 2 WHEN e.[processed] = \'Completed\' THEN 3 WHEN e.[processed] = \'Rejected\' THEN 4 WHEN e.[processed] = \'Cancelled\' THEN 5 END';
		}
		$tsql= "SELECT
	      e.[event_id] AS 'sampleOrderNo',
	      e.[created_by] AS 'submittedBy',
				CONVERT(varchar(10), e.[eventdate], 20) AS 'eventDate',
				CONVERT(varchar(10), e.[samplerequestdate], 20) AS 'orderDate',
				CONVERT(varchar, DATEADD(HOUR, -4, e.[created_date]), 120) AS 'createdDate',
	      e.[rushedorder] AS 'rushedOrder',
				e.[requesttype] + '<br />' + ISNULL(e.[eventname] + '<br />', '') + ISNULL(e.[eventdesc] + '<br />', '') + ISNULL(e.[address] + '<br />', '') + ISNULL(e.[address2] + '<br />', '') + ISNULL(e.[city] + '<br />', '') + ISNULL(e.[state] + '<br />', '') + ISNULL(e.[zip] + '<br />' , '') + ISNULL(e.[country] + '<br />', '')  +
					ISNULL(STUFF((SELECT N'<a href=".$siteroot."/".$upload_dir."temp/' + REPLACE(f.[fileName], ' ', '%20') + ' download=' + REPLACE(f.[fileName], ' ', '%20') +'>' + f.[filename] + '</a><br />' FROM [dbo].[mhw_event_file] f WHERE e.[event_id] = f.[event_id] FOR XMl PATH, TYPE).value(N'.[1]', N'nvarchar(max)'), 1, 0, '<br />'), '') + 
					--ISNULL(STUFF((SELECT N'<a href=".$siteroot."/query-request-samples.php?fileId=' + TRIM(STR(f.[id])) + ' download=' + REPLACE(f.[fileName], ' ', '%20') +'>' + f.[filename] + '</a><br />' FROM [dbo].[mhw_event_file] f WHERE e.[event_id] = f.[event_id] FOR XMl PATH, TYPE).value(N'.[1]', N'nvarchar(max)'), 1, 0, '<br />'), '') + 
					CASE WHEN e.eventtype = 1 THEN 'Public Event' WHEN e.eventtype = 2 THEN 'Invitation Only Event' END  AS 'eventDetails',
			  (ISNULL(e.[pickuptxt] + '<br />' , '') + ISNULL(e.[pickupdisttxt] + '<br />' , '') + ISNULL(e.[pickupothertxt] + '<br />' , '') + ISNULL(e.[deliveryaddress] + '<br />' , '') + ISNULL(e.[deliverycity] + '<br />' , '') + ISNULL(e.[deliverystate] + '<br />' , '') + ISNULL(e.[deliveryzip] + '<br />' , '') + ISNULL(e.[deliverycountry], '')) AS 'location',
	      e.[deliverytype] AS 'deliveryDetails',
	      e.[delivery] AS 'Location',
				e.[sampleusage] AS 'useOfSamples',
				e.[eventstatesampleevent] AS 'eventStateSampleEvent',
				e.[eventdesc] AS 'eventDescription',
				e.[deliveryaddress] AS 'deliveryAddress',
				e.[deliveryaddress2] AS 'deliveryAddress2',
				e.[deliverycity] AS 'deliveryCity',
				e.[deliverystate] AS 'deliveryState',
				e.[deliveryzip] AS 'deliveryZip',
				e.[deliverycountry] AS 'deliveryCountry',
				e.[deleted],
	      e.[processed],
				e.[clientname] AS 'clientName',
				-- f.[id] AS 'attachmentId',
				-- '<a href=query-samples.php?fileId=' + TRIM(STR(f.[id])) + '>' + f.[filename] + '</a>' AS 'fileName',
				-- fileName=STUFF((SELECT N'<a href=query-samples.php?fileId=' + TRIM(STR(f.[id])) + '>' + f.[filename] + '</a>' FROM [dbo].[mhw_event_file] f WHERE e.[event_id] = f.[event_id] FOR XMl PATH, TYPE).value(N'.[1]', N'nvarchar(max)'),	1, 0, '<br />')
				$txtArea
				$btnString
			FROM
				[dbo].[mhw_app_event_form] e
			-- LEFT JOIN
			--	[dbo].[mhw_event_file] f ON e.[event_id] = f.[event_id]
			ORDER BY $caseOrder,
			e.[created_date] DESC";
	}
}
// Sub items for the event list elements
else if($_POST['qrytype'] == 'client_item_list') {
  if(isset($_POST['event_id'])) {

    $tsql = "SELECT
               p.[product_id], p.[item_id] AS 'itemId', i.[item_mhw_code] AS 'productName', p.[product_name], p.[quantity] AS 'quantity',
               i.[item_description] AS 'itemDescription', i.[stock_uom] AS 'stockUom', p.[processed] AS 'status'
             FROM
               [dbo].[mhw_event_product] p
             LEFT JOIN
               [dbo].[mhw_app_prod_item] i
             ON
               i.[item_id] = p.[item_id]
          	 WHERE p.[event_id] = $eventId";
  }
}
else if($_POST['qrytype'] == 'Approve' || $_POST['qrytype']=='Reject' || $_POST['qrytype'] == 'Complete' || $_POST['qrytype']=='Cancel') {
  if(isset($_POST['event_id']) && intval($_POST['event_id'] > 0)) {
		$processed = "";
		switch($_POST['qrytype']) {
			case "Approve":
				$processed = "Approved";
				break;
			case "Reject":
				$processed = "Rejected";
				break;
			case "Complete":
				$processed = "Completed";
				break;
			case "Cancel":
				$processed = "Cancelled";
				break;
			default:
				$processed = "Compliance Hold";
				break;
		}
	  $thisUser = $_SESSION['mhwltdphp_user'];
		$queryMessage = null;
		if(isset($_POST['qrymsg'])) {
			$queryMessage = $_POST['qrymsg'];
		}
		$tsql = "UPDATE dbo.mhw_app_event_form
		SET modified_by = '$thisUser', modified_date = GETDATE(), deleted = 0, processed = '$processed', notes = '$queryMessage' WHERE event_id = $eventId";
	 }
}
else if($_POST["qrytype"] === "updatereviewremarks") {
	if(isset($_POST['qrymsg'])) {
		$queryMessage = $_POST['qrymsg'];
	}
	$tsql = "UPDATE dbo.[mhw_app_event_form] SET modified_by = '$thisUser', modified_date = GETDATE(), notes = '$queryMessage' WHERE event_id = $eventId";
}
else if(isset($fid)){
	error_log("Get file for ID -> " . $_GET['fileId']);
	$tsql = "SELECT
						f.[filename],
						f.[filetype],
						f.[attachment]
					FROM
						[dbo].[mhw_event_file] f
					WHERE
						f.[id] = $fid";
}
else {
	echo("Samples request error request type not defined -> " . $_POST['qrytype'] . "!");
	exit;
}

/* ********************* added by Jobaidur :End ************************ */

function utf8ize($d) {
	if (is_array($d)) {
		foreach ($d as $k => $v) {
			$d[$k] = utf8ize($v);
		}
	} else if (is_string ($d)) {
		return utf8_encode($d);
	}
	return $d;
}

$tsql = preg_replace('/\t/', '', $tsql);
$getResults= sqlsrv_query($conn, $tsql);
$sqlErrors = "";
if ($getResults == false) {
	$sqlErrors = sqlsrv_errors();
	echo($sqlErrors);
}


if(!isset($fid)) {
	$json = array();
	/* Iterate through the table rows populating the array */
	do {
	   while ($row = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
	     $json[] = utf8ize($row);
	   }
	} while ( sqlsrv_next_result($getResults) );

	if(empty($json)) {
		error_log("No results from query " . $tsql);
		$json = array("query_type" => $_POST['qrytype'], "query_id" => $_POST['qryid'], "processed" => $processed, "problematic_query" => preg_replace("/\r|\n/", "", $query), "sql_error" => $sqlErrors, "raw_query" => $tsql);
	}

	echo json_encode($json);
}
else {
	if (sqlsrv_fetch($getResults))  {
  	$name = sqlsrv_get_field($getResults, 0);
  	$type = sqlsrv_get_field($getResults, 1);
 	  $content = sqlsrv_get_field($getResults, 2, SQLSRV_PHPTYPE_STREAM(SQLSRV_ENC_BINARY));
	 	error_log("Got file to download " . $name . " Content-Type: " . $type . " Size " . sizeof($content));
		header("Content-Type: " . $type);
		fpassthru($content);
	}
	else {
		error_log("Error getting the event file!");
	}
}

/* Free statement and connection resources. */
sqlsrv_free_stmt( $getResults);
sqlsrv_free_stmt( $trkResults);
sqlsrv_close($conn);

?>
