<?php
error_reporting(E_ALL); //displays an error

	//echo '<pre>'; print_r($_POST); echo '</pre>';
include("dbconnect.php");

//Establishes the connection
$conn = sqlsrv_connect($serverName, $connectionOptions);
if( $conn === false) {
		die( print_r( sqlsrv_errors(), true));
}
//$TESTclient="MHW Web Demo";
$client = str_replace("'","''",$_POST['client']);
$brand = str_replace("'","''",$_POST['brand']);
if($_POST['cbbtype']=='brand'
	|| $_POST['cbbtype']=='product'
	|| $_POST['cbbtype']=='supplier')
{
	if(isset($client) && $client!=='')
	{
		$tsql= "EXEC [dbo].[usp_select_request] @querytype = '".$_POST['cbbtype']."', @client = '$client', @brand = '$brand'";
	}
}
/*
if($_POST['cbbtype']=='brand')
{
	if(isset($_POST['client']) && $_POST['client']!=='')
	{
	 $tsql= "SELECT DISTINCT brand_name as id, brand_name as name, brand_name as code
				 FROM [dbo].[mhw_app_prod] WHERE client_name = '$client' AND active = 1 and deleted = 0";

	}
}

if($_POST['cbbtype']=='product')
{
	if(isset($_POST['client']) && $_POST['client']!=='')
	{
		if(isset($_POST['brand']) && $_POST['brand']!=='')
		{
		 $tsql= "SELECT product_id as id, product_desc as name, product_mhw_code as code, TTB_ID as ttb FROM [dbo].[mhw_app_prod] WHERE client_name = '$client' AND brand_name = '$brand' AND active = 1 and deleted = 0";
		}
		else
		{
		 $tsql= "SELECT product_id as id, product_desc as name, product_mhw_code as code, TTB_ID as ttb FROM [dbo].[mhw_app_prod] WHERE client_name = '$client' AND active = 1 and deleted = 0";

		}
	} else {
		// for the  product_state_status screen
		$tsql= "SELECT product_id as id, product_mhw_code as name, TTB_ID as ttb FROM [dbo].[mhw_app_prod] WHERE active = 1 and deleted = 0";
	}
}

if($_POST['cbbtype']=='supplier')
{
	if(isset($_POST['client']) && $_POST['client']!=='')
	{
	 $tsql= "SELECT DISTINCT s.supplier_id as id, s.supplier_name as name, s.supplier_name as code
				 FROM [dbo].[mhw_app_prod_supplier] s
				 left outer join [dbo].[mhw_app_prod] p on p.[supplier_id] = s.[supplier_id]
				 WHERE p.client_name = '$client' AND s.active = 1 and s.deleted = 0";

	}
}
*/
if($_POST['cbbtype']=='po-suppliers')
{
	 $tsql= "SELECT * FROM [dbo].[mhw_suppliers] s
				WHERE s.deleted <> 1";

}
else if($_POST['cbbtype']=='product2')
{
	// for the  product_state_status screen
	$tsql= "SELECT product_id as id, product_mhw_code as name, TTB_ID as ttb FROM [dbo].[mhw_app_prod] WHERE active = 1 and deleted = 0";
}
else if($_POST['cbbtype']=='item')
{
	if(isset($_POST['client']) && $_POST['client']!=='')
	{
		$stateFilter = "";
		if (isset($_POST["view_state"])) {
			if ($_POST['view_state']=='tfa_ca') {
				$stateFilter = " AND p.federal_type = 'Malt Beverages'"; 
			}else if ($_POST['view_state']=='tfa_mn') {
				$stateFilter = " AND p.federal_type = 'Distilled Spirits'"; 
			}else if ($_POST['view_state']=='tfa_nyr' || $_POST['view_state']=='tfa_nyw') {
				$stateFilter = " AND p.federal_type IN ('Distilled Spirits','Wine')"; 
			}
		}
	 	$tsql= "SELECT DISTINCT i.item_id as id, 
		 			REPLACE(i.item_description,'''','') as name, 
					i.item_mhw_code as code, 
					i.item_client_code as code2, 
					p.product_mhw_code as parent, 
					REPLACE(p.product_desc,'''','') as parent2, 
					REPLACE(p.brand_name,'''','') as brand, 
					p.federal_type as federal_type
				 FROM [dbo].[mhw_app_prod_item] i
				 left outer join [dbo].[mhw_app_prod] p on p.[product_id] = i.[product_id]
				 WHERE p.client_name = '$client' 
				 	 AND p.active = 1 
					 AND p.deleted = 0 
					 AND i.active = 1 
					 AND i.deleted = 0
					 AND i.stock_uom <> 'BOTTLE'
					 $stateFilter";
//echo $tsql;
	}
}
else if($_POST['cbbtype'] == 'product_items')
{
	if(isset($_POST['product_id']) && $_POST['product_id'] !== '') {
		$product_id = $_POST['product_id'];
	 	$tsql= "SELECT DISTINCT item_id as id, item_id AS item_id,
		 			i.item_description as name,
		 			i.item_description as item_description,
					i.item_mhw_code as code,
					i.stock_uom as stock_uom,
					i.container_type AS container_type,
					i.container_size AS container_size
				 FROM [dbo].[mhw_app_prod_item] i
				 WHERE i.product_id = $product_id
					 AND i.active = 1
					 AND i.deleted = 0";
	}
	// echo($tsql);
}


$getResults= sqlsrv_query($conn, $tsql);
//echo ("Reading data from table" . PHP_EOL);
if ($getResults == FALSE)
	echo (sqlsrv_errors());

 /* Processing query results 
 */

/* Setup an empty array */
$json = array();
/* Iterate through the table rows populating the array */
do {
     while ($row = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)) {
     $json[] = $row;
     }
} while ( sqlsrv_next_result($getResults) );
 
function utf8ize($d) {
	if (is_array($d)) {
		foreach ($d as $k => $v) {
			$d[$k] = utf8ize($v);
		}
	} else if (is_string ($d)) {
		return utf8_encode($d);
	}
	return $d;
}
$json = utf8ize($json);

/* Run the tabular results through json_encode() */
/* And ensure numbers don't get cast to trings */
echo json_encode($json);

/* Free statement and connection resources. */
sqlsrv_free_stmt( $getResults);
sqlsrv_close( $conn);


?>
