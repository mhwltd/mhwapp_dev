<?php //SUPPLIER FUNCTIONS

// include("sessionhandler.php");
// include("prepend.php");
include("settings.php");
include("functions.php");

session_start();
if($_SESSION['mhwltdphp_usertype'] != "SUPERUSER" && $_SESSION['mhwltdphp_usertype'] != "ADMIN" ){ 
    echo "Access Denied"; exit(0);
}

//echo '<pre>'; print_r($_REQUEST); exit(0);

error_reporting(E_ALL); //displays an error
include("dbconnect.php");

//Establishes the connection
$conn = sqlsrv_connect($serverName, $connectionOptions);
if($conn === false) {
	print_r( sqlsrv_errors()); exit(0);
}


if($_SESSION['mhwltdphp_user']!='' && $_POST['qrytype']!=''){
	$trksql= "INSERT INTO [mhw_app_workflow] VALUES ('".$_POST['qrytype']."', 0, GETDATE(), GETDATE(), '".$_SESSION['mhwltdphp_user']."', 1, 0)";
	$trkResults= sqlsrv_query($conn, $trksql);
}

$current_user = $_SESSION['mhwltdphp_user'];
$current_date = date("Y-m-d H:i:s");

$origin_code = $_POST['origin_code'];


if ($_POST['action']=='origin code validate') {

    $tsql = "SELECT COUNT(*) AS 'supplier_count' FROM [mhw_app_prod_supplier] WHERE [origin_code] = ? AND active = 1 AND deleted = 0";

    $stmt = sqlsrv_prepare($conn,$tsql,array($origin_code));

    if($stmt === false ){  
        echo "Statement could not be prepared.\n";  
        die( print_r( sqlsrv_errors(), true));  
    }  
    
    if( sqlsrv_execute($stmt) === false ){  
        echo "Statement could not be executed.\n";  
        die( print_r( sqlsrv_errors(), true));  
    }

    $getResults = sqlsrv_execute($stmt);

    function utf8ize($d) {
	    if (is_array($d)) {
		    foreach ($d as $k => $v) {
			    $d[$k] = utf8ize($v);
		    }
	    } else if (is_string ($d)) {
		    return utf8_encode($d);
	    }
	    return $d;
    }
  
    /* Setup an empty array */
    $json = array();
    /* Iterate through the table rows populating the array */
    do {
        while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)){
            $json[] = utf8ize($row); 
        }
    } while (sqlsrv_next_result($stmt));
 

    //$json = utf8ize($json);

    /* Run the tabular results through json_encode() */
    /* And ensure numbers don't get cast to strings */
    echo json_encode($json);

    /* Free statement and connection resources. */
    sqlsrv_free_stmt($stmt);
    //sqlsrv_free_stmt($trkResults); ADD LATER TO TRACK ACTIVITY
    sqlsrv_close($conn);

}

?>