<?php
session_start();
include("prepend.php");
include("settings.php");
include("MHWEmailAgent.php");

function getEventDate($eventdate, $eventdate2) {
  if($eventdate !== null) {
    return($eventdate);
  }
  else if($eventdate2 !== null) {
    return($eventdate2);
  }
  return;
}

function getSampleDate($samplerequestdate, $samplesolicitordate, $sampledate, $sampledate2, $eventdate, $eventdate2) {
  if($samplerequestdate !== null) {
    return($samplerequestdate);
  }
  else if($samplesolicitordate !== null) {
    return($samplesolicitordate);
  }
  else if($sampledate  !== null) {
    return($sampledate);
  }
  else if($sampledate2 !== null) {
    return($sampledate2);
  }
  else if($eventdate !== null) {
    return($eventdate);
  }
  else if($eventdate2 !== null) {
    return($eventdate2);
  }
  return;
}

function getSampleUsage($sampleusermhw, $sampleuserbroker, $sampleuserrep) {
  if($sampleusermhw !== null) {
    return($sampleusermhw);
  }
  else if($sampleuserbroker !== null) {
    return($sampleuserbroker);
  }
  else if($sampleuserrep !== null) {
    return($sampleuserrep);
  }
  return;
}

if (!isset($_SESSION['mhwltdphp_user'])) {
    header("Location: login.php");
} else {
    include('head.php');
    include('dbconnect.php');

	$admin_type_user=false;
	if($_SESSION['mhwltdphp_usertype'] == "SUPERUSER" || $_SESSION['mhwltdphp_usertype'] == "ADMIN" ){ 
		$admin_type_user=true;
	}
	
	if(!$admin_type_user){
		$smplclients='';
		$access_client_user=false;
		$clientsLIST = explode(";",$_SESSION['mhwltdphp_userclientcodes']);

		$conn = sqlsrv_connect($serverName, $connectionOptions);
		if( $conn === false) die( print_r( sqlsrv_errors(), true));

		foreach ($clientsLIST as &$clientLISTvalue) {

			$tsql= "SELECT TOP 1 * FROM [mhw_app_client_access] WHERE [user_application] = 'Event_Samples' and [active] = 1 and [client_code] = '".$clientLISTvalue."'";
			$stmt = sqlsrv_query( $conn, $tsql);
			if ( $stmt === false ) die( print_r( sqlsrv_errors(), true));
			while ( $row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC) ) { 
				$smplclients.=$row['client_code'].';';
				$smplclientclass[$row['client_code']] = $row['user_class']; 
				$access_client_user=true;
			}
		}
		$smplclients = substr($smplclients,0,strlen($smplclients)-1);
//echo '<!--STOP|'.$smplclients.'|STOP-->';
		sqlsrv_free_stmt($stmt);

		if(!$access_client_user){
			die( "Stay tuned! This feature will be available soon. You can return to CRD by clicking here <a href='".$sitelogin."'>".$sitelogin."</a>" ); 
		}

		$_SESSION['mhwltdphp_userclientcodes'] = $smplclients;
	}

    $uniqueId = uniqid();
    
    //if(isset($_POST["submit"])) {

  // POST/GET ENV and Ajax functions
  error_log("------------------------------------- POST VARS -------------------------------------" . print_r($_POST, true));
    if (isset($_POST["requesttype"]) !== false) {
        //var_dump($_FILES["docs"]);

        $requesttype = (empty($_POST['requesttype'])) ? null : $_POST['requesttype'];
    $product_id = (empty($_POST['product_id'])) ? null : $_POST['product_id'];
    $item_id = (empty($_POST['item_id'])) ? null : $_POST['item_id'];
    $mhw_item_code = (empty($_POST['ItemCode'])) ? null : $_POST['ItemCode'];
    error_log("mhw_item_code -----> " . print_r($mhw_item_code, true));

        $samplerequestdate = (empty($_POST['samplerequestdate'])) ? null : $_POST['samplerequestdate'];
        $samplesolicitordate = (empty($_POST['samplesolicitordate'])) ? null : $_POST['samplesolicitordate'];
        $eventdate = (empty($_POST['eventdate'])) ? null : $_POST['eventdate'];
        $sampledate = (empty($_POST['sampledate'])) ? null : $_POST['sampledate'];
        $eventdate2 = (empty($_POST['eventdate2'])) ? null : $_POST['eventdate2'];
        $sampledate2 = (empty($_POST['sampledate2'])) ? null : $_POST['sampledate2'];

        $eventstatesampleevent = (empty($_POST['eventstatesampleevent'])) ? null : $_POST['eventstatesampleevent'];
        $eventstateeventonly = (empty($_POST['eventstateeventonly'])) ? null : $_POST['eventstateeventonly'];
        $othertxt = (empty($_POST['othertxt'])) ? null : $_POST['othertxt'];
        
        $deliverytype = (empty($_POST['deliverytype'])) ? null : $_POST['deliverytype'];
        $delivery = (empty($_POST['delivery'])) ? null : $_POST['delivery'];
        
        $pickuptype = (empty($_POST['pickuptype'])) ? null : $_POST['pickuptype'];
        $pickupdisttxt = (empty($_POST['pickupdisttxt'])) ? null : $_POST['pickupdisttxt'];
        $deliveryaddress = (empty($_POST['deliveryaddress'])) ? null : $_POST['deliveryaddress'];
        $deliveryaddress2 = (empty($_POST['deliveryaddress2'])) ? null : $_POST['deliveryaddress2'];
        $deliverycity = (empty($_POST['deliverycity'])) ? null : $_POST['deliverycity'];
        $deliveryzip = (empty($_POST['deliveryzip'])) ? null : $_POST['deliveryzip'];
        $deliverycountry = (empty($_POST['deliverycountry'])) ? null : $_POST['deliverycountry'];
        $pickupothertxt = (empty($_POST['pickupothertxt'])) ? null : $_POST['pickupothertxt'];

    // If the same address is selected replicate the data into the delivery data.
    if($delivery === "sameAddress") {
      $deliveryaddress = (empty($_POST['address'])) ? null : $_POST['address'];
      $deliveryaddress2 = (empty($_POST['address2'])) ? null : $_POST['address2'];
      $deliverycity = (empty($_POST['city'])) ? null : $_POST['city'];
      $deliverystate = (empty($_POST['state'])) ? null : $_POST['state'];
      $deliveryzip = (empty($_POST['zip'])) ? null : $_POST['zip'];
      $deliverycountry = (empty($_POST['country'])) ? null : $_POST['country'];
    }

        $eventdesc = (empty($_POST['eventdesc'])) ? null : $_POST['eventdesc'];
        $eventname = (empty($_POST['eventname'])) ? null : $_POST['eventname'];
        $eventtype = (empty($_POST['eventtype'])) ? null : $_POST['eventtype'];

        $address = (empty($_POST['address'])) ? null : $_POST['address'];
        $address2 = (empty($_POST['address2'])) ? null : $_POST['address2'];
        $city = (empty($_POST['city'])) ? null : $_POST['city'];
        $state = (empty($_POST['state'])) ? null : $_POST['state'];
        $zip = (empty($_POST['zip'])) ? null : $_POST['zip'];
        $country = (empty($_POST['country'])) ? null : $_POST['country'];
      
        $sampleuser = (empty($_POST['sampleuser'])) ? null : $_POST['sampleuser'];
        $sampleusermhw = (empty($_POST['sampleusermhw'])) ? null : $_POST['sampleusermhw'];
        $sampleuserbroker = (empty($_POST['sampleuserbroker'])) ? null : $_POST['sampleuserbroker'];
        $sampleuserrep = (empty($_POST['sampleuserrep'])) ? null : $_POST['sampleuserrep'];

    $clientname = str_replace(";","','",$_SESSION['mhwltdphp_userclients']);
	$client_code = str_replace(";","','",$_SESSION['mhwltdphp_userclientcodes']);
    $sampleUsage = getSampleUsage($sampleusermhw, $sampleuserbroker, $sampleuserrep);

        $userID = $_POST['userID'];
        
        //echo "<pre>";
        //var_dump($_REQUEST);
        //echo "</pre>";

        //Establishes the connection
        $conn = sqlsrv_connect($serverName, $connectionOptions);

        if ($conn === false) {
            //echo "Connection could not be established.<br />";
            die(print_r(sqlsrv_errors(), true));
        }

    $sampleDate = getSampleDate($samplerequestdate, $samplesolicitordate, $sampledate, $sampledate2, $eventdate, $eventdate2);
    $eventDate = getEventDate($eventdate, $eventdate2);
    if($requesttype == "" || $requesttype == "This is a sample request for a distributor" || $requesttype == "This is a sample request for a licensed solicitor to use for account visits") {
      $rushedOrder = 0;
    }
    else {
      $sampleFlag = (strtotime($sampleDate) < strtotime("+20 days")) ? 1 : 0;
      $eventFlag = (strtotime($eventDate) < strtotime("+20 days")) ? 1 : 0;
      if($sampleFlag || $eventFlag) {
        $rushedOrder = 1;
      }
    }
    if($rushedOrder == null) {
      $rushedOrder = 0;
    }
    error_log("rushedOrder = " . $rushedOrder . " is " . strtotime($sampleDate) . " < " . strtotime("+20 days"));
    $mainsql = "INSERT INTO mhw_app_event_form (requesttype, samplerequestdate, eventdate,
		                        eventstatesampleevent, eventstateeventonly, othertxt, deliverytype, delivery, pickupdisttxt,
								eventdesc, eventname, eventtype, address, address2, city, state, zip, country,
								deliveryaddress, deliveryaddress2, deliverycity, deliverystate, deliveryzip, deliverycountry,
      pickupothertxt, sampleuser, sampleusage, created_by, processed, rushedorder, pickuptxt, clientname, client_code)
      VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?); SELECT SCOPE_IDENTITY() AS IDENTITY_COLUMN_NAME";
      // prepare and bind
      $mainparams =
      array($requesttype, $sampleDate, $eventDate,
      $eventstatesampleevent, $eventstateeventonly,
      $othertxt, $deliverytype, $delivery, $pickupdisttxt,
      $eventdesc, $eventname, $eventtype,
      $address, $address2, $city,
      $state, $zip, $country,
      $deliveryaddress, $deliveryaddress2,
      $deliverycity,  $deliverystate, $deliveryzip,
      $deliverycountry,
      $pickupothertxt, $sampleuser, $sampleUsage,
      $userID, 'Compliance Hold', $rushedOrder,
      $pickuptype, $clientname, trim($client_code));

        
        $stmt = sqlsrv_query($conn, $mainsql, $mainparams);
		error_log("mhw_event_form: Main Query -> " . $mainsql);
		//error_log("mhw_event_form: Prepped Query -> " . $stmt);

        if ($stmt === false) {
            die(print_r(sqlsrv_errors(), true));
        }

        sqlsrv_next_result($stmt);  //note this line!!
        sqlsrv_fetch($stmt);
        $eventId = sqlsrv_get_field($stmt, 0);

    array_unshift($mainparams, $userID);
    array_unshift($mainparams, $clientname);
    array_unshift($mainparams, $eventId); // Prepend the event ID to the data submitted to the database.

    $eventproductsql = "INSERT INTO mhw_event_product (event_id, product_name, quantity, created_by, product_id, item_id)
    VALUES (?, ?, ?, ?, ?, ?)";

    $productnamearray = (empty($_POST['product'])) ? null : $_POST['product'];
    $itemCodeArray = (empty($_POST['ItemCode'])) ? null : $_POST['ItemCode'];
    $productarray = (empty($_POST['product_id'])) ? null : $_POST['product_id'];
    $itemarray = (empty($_POST['item_id'])) ? null : $_POST['item_id'];
    $quantity = (empty($_POST['quantity'])) ? null : $_POST['quantity'];
    $stockuom = (empty($_POST['StockUOM'])) ? null : $_POST['StockUOM'];
    $itemdesription = (empty($_POST["ItemDescription"])) ? null : $_POST["ItemDescription"];
    error_log("MHW Item Code Array -----------> " . print_r($mhwItemCodeArray, true));
    $itemData = [];
    for ($x = 0; $x < sizeof($productnamearray); $x++) {
      $prod = (empty($productnamearray[$x])) ? null : $productnamearray[$x];
      $prodId = (empty($productarray[$x])) ? null : $productarray[$x];
      $itemId = (empty($itemarray[$x])) ? null : $itemarray[$x];
      $itemCode = (empty($itemCodeArray[$x])) ? null : $itemCodeArray[$x];
      $quant = (empty($quantity[$x])) ? null : $quantity[$x];
      $stock = (empty($stockuom[$x])) ? null : $stockuom[$x];
      $itemDesc = (empty($itemdesription[$x])) ? null : $itemdesription[$x];
      $eventproductparams = array($eventId, $prod, $quant, $userID, $prodId, $itemId);
      array_push($itemData, array($itemCode, $prod, $itemDesc, $quant, $stock));
            $eventproductstmt = sqlsrv_query($conn, $eventproductsql, $eventproductparams);

            if ($eventproductstmt === false) {
                die(print_r(sqlsrv_errors(), true));
            }
        }
        


        //****** Upload File ***********
      $countfiles = count($_FILES['file']['name']) - 1;
      if($countfiles > 0) {
        $uploadFileSql = "INSERT INTO mhw_event_file (event_id, filename, filetype, attachment, created_by) VALUES (?, ?, ?, cast (? as varbinary(max)), ?)";
        // Looping all files
        for ($i=0;$i<$countfiles;$i++) {
            $tempName = trim($_FILES["file"]["name"][$i]);
            $baseFileName = trim(basename($_FILES["file"]["name"][$i]));

            if (!empty($baseFileName)) {
                $file_name = trim($_FILES["file"]["name"][$i]);
                $file_type = trim($_FILES["file"]["type"][$i]);
                $fileSize  = trim($_FILES["file"]["size"][$i]);
                $tmp_name  = trim($_FILES["file"]["tmp_name"][$i]);

				$newfilename = "e_".$eventId."_".strtotime("now")."_".$file_name;
				copy($_FILES['file']['tmp_name'][$i],$upload_dir."temp/".$newfilename);

                // Convert to base64
                //$image_base64 = base64_encode(file_get_contents($_FILES['file']['tmp_name']) );
                $filePointer = fopen($tmp_name, "rb");
                $file_content = fread($filePointer, filesize(trim($_FILES["file"]["tmp_name"][$i])));
                fclose($filePointer);

                //$uploadFileparams = array($eventId, $file_name, $file_type, $file_content, $userID);
				$uploadFileparams = array($eventId, $newfilename, $file_type, base64_encode($file_content), $userID);
                $uploadFileStmt = sqlsrv_query($conn, $uploadFileSql, $uploadFileparams);
    
                if ($uploadFileStmt === false) {
                    die(print_r(sqlsrv_errors(), true));
                }
            }
		}
        error_log("mhw_event_form: Upload File Query -> " . $uploadFileSql);
      }
      //So far so good now fire an email off to the relevant user based on the client.

      $clients = explode(";",$_SESSION['mhwltdphp_userclientcodes']);
      if (count($clients) > 1) {
        // This is obviously wrong, need to find out what to do in the event there is more than one user selected.
        $client = $clients[0];
      }
      else {
        $client = $clients[0];
      }

      // error_log("Compliance Query -> " . $complianceEmailQuery . "complianceEmailTo -> " . print_r($complianceEmailTo));
      $emailAgent = new MHWEmailAgent();
      $done = 0;
      $emailAgent->setUsername();
      $emailAgent->setPassword($fromPassword);
      $emailAgent->setFrom();
      $emailAgent->setBody();
      $emailAgent->setAltBody();
      // Add the header information to the item Array
      array_unshift($itemData, array("Item Code", "Product Description", "Item Description", "# of Bottles Requested", "Stock UOM"));
      error_log("itemData -> " . print_r($itemData, true));
      if($emailAgent->addRecipient($NewOrderComplianceEmailTo, "Compliance Team")) {
        // Set the subject for the compliance team
		$NewOrderEmailSubject = str_replace("{enter_order_number_here}",$eventId,$NewOrderComplianceEmailSubject);
        $emailAgent->setSubject($NewOrderEmailSubject);
        $headings        = array("Sample Order #", "Client Name", "Submitted By", "Order Date", "Event Details",                              "Delivery/Pickup",               "Location",                       "Use of Samples", "Review Remarks", "Rushed Order", "Status");
        $headingIndicies = array(array(0),            array(1),      array(2),      array(4),   array(3, 13, 12, 16, 17, 18, 19, 20),            array(9),             array(33, 11, 27, 21, 22, 23, 24, 25, 26), array(29),        array(-1),     array(32),        array(31));
        $messageHeader = '<p class="MsoNormal">A new Sample/Event Order #' . $eventId . ' has been submitted. Please refer to the following table for details.</p><br />';
        $messageFooter = '<br /><br /><p class="MsoNormal">You can click on the following link to the CRD to view more details: <a href="'  .$siteroot . '/client_samples_review.php">here ' . $siteroot . '/client_samples_review.php</a></p>';
        $emailAgent->setBody($messageHeader . generateComplianceEmailHTML($headings, $headingIndicies, $mainparams, $itemData) . $messageFooter);
        error_log(generateComplianceEmailHTML($headings, $headingIndicies, $mainparams, $itemData));
        $emailAgent->isHTML(true);
        if($emailAgent->sendEmail()) {
          echo '<script>document.location="mhw_sample_event_form_submitted.php?event_id=' . $eventId . '&email_sent=true&email_to=' . $NewOrderComplianceEmailTo . '&query=\"' . $complianceEmailQuery . '\""</script>';
        }
        else {
          echo '<script>document.location="mhw_sample_event_form_submitted.php?event_id=' . $eventId . '&sendmailerror=' . $emailAgent->getLastError() . '&query=\"' . $complianceEmailQuery .'"</script>';
        }
      }
      else {
        echo '<script>document.location="mhw_sample_event_form_submitted.php?event_id=' . $eventId . '&error=' . $emailAgent->getLastError() . '&query=\"' . $complianceEmailQuery .'"</script>';
      }
      // echo '<script>document.location="mhw_sample_event_form_submitted.php"</script>';
    }
    else {
      error_log('mhw_event_form: The variable $_POST[\"requesttype\"] is not set. If this is the result of a form post then something is off. Dump the request.');
    }

  }
  ?>
<!-- Form Layout -->
<!-- Global Javascript for site -->
<script src="main.js"></script>	
<!-- Javascript/JQuery specific to this page -->
<script src="eventform.js"></script>	
<!-- Table layout and building functionality -->
<script src="js/repeater.js"></script>


<style type="text/css">
	.warning {display:block; margin:15px 0;}
	.warning span {color:red;}
	input[type='number'] {width:18rem; display:inline;}
	#productvalid {display:none;}

	a.errorlink:link {color:red;text-decoration:underline;}
	a.errorlink:visited {color:red;text-decoration:underline;}
	a.errorlink:hover {color:red;text-decoration:underline;}
</style>	

    <link href="css/wforms-layout_v530-14.css" rel="stylesheet" type="text/css" />
    <link href="css/theme-52661.css" rel="stylesheet" type="text/css" />
    <link href="css/wforms-jsonly_v530-14.css" rel="alternate stylesheet" title="This stylesheet activated by javascript" type="text/css" />
	<link href="css/wforms-inline.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/wforms_v530-14.js"></script>
    <script type="text/javascript" src="js/localization-en_US_v530-14.js"></script>         

</head>
<body class="default wFormWebPage">
<div class="container-fluid">
	<div id="tfaContent">
      <form method="post" action="mhw_event_form.php" class="hintsBelow labelsAbove" id="eventform" role="form" enctype="multipart/form-data" >

		<div class="wFormContainer" style="max-width: 85%; width:auto;" >
			<div class="row">
				<div class="col-sm-12">

    				<div class="wFormHeader"></div>
					<h3 class="wFormTitle" id="4690986-T">Event Notification & Sample Request Form</h3>
					<p> Please answer the following questions regarding your event notification and/or sample request. Your responses will then be forwarded to our compliance department for review. Answering each question in its entirety will prevent delays in the compliance vetting process. </p>
					
					<div class="col-md-auto oneField field-container-D" id="client_name-D">
					<label id="client_name-L" class="label preField " for="client_name"><b>Client Name</b></label>
						<div class="inputWrapper">
							<select id="client_name" name="client_name" title="Client Name" aria-required="true">
							<?php
							$clients = explode(";",$_SESSION['mhwltdphp_userclients']);
							if (count($clients) > 1) {
								echo "<option value='all' class=''>ALL</option>";
							}
							foreach ($clients as &$clientvalue) {
								echo "<option value='$clientvalue' class=''>$clientvalue</option>";
						}
						?>
						</select>
						</div>
					
						<div class="float-right"><button type="reset" value="Reset" id="clearform" class="btn btn-sm btn-outline-danger" onClick="window.location.reload()">Clear Form</button></div>					
					</div>

						<div class="form-group">
							<h5>1. Select the appropriate option:</h5>

							<div class="form-check">
                  <input class="form-check-input hiddeninputs" data-div="showsrequestdate" type="radio" name="requesttype"  value="This is a sample request for a distributor" required>
							  <label class="form-check-label">
							    This is a sample request for a distributor
							  </label>						  
							</div>
							<div class="form-check" id="showsrequestdate">
								Provide requested date of sample delivery: <input type="date" name="samplerequestdate" min="<?php echo date("Y-m-d"); ?>" value="">
							</div>	

							<div class="form-check">
                  <input class="form-check-input hiddeninputs" data-div="showssolicitordate" type="radio" name="requesttype"  value="This is a sample request for a licensed solicitor to use for account visits" required>
							  <label class="form-check-label">
							  This is a sample request for a licensed solicitor to use for account visits
							  </label>						  
							</div>	
							<div class="form-check" id="showssolicitordate">
								Provide requested date of sample delivery: <input type="date" name="samplesolicitordate" min="<?php echo date("Y-m-d"); ?>" value="">
							</div>

							<div class="form-check">
                  <input class="form-check-input hiddeninputs" data-div="showrequesteventdate" type="radio" name="requesttype" value="This is a SAMPLE request and EVENT notification" required>
							  <label class="form-check-label">
								This is a SAMPLE request and EVENT notification
							  </label>
							</div>
							
							<div class="form-check" id="showrequesteventdate">
                  Provide requested date of sample delivery: <input type="date" name="sampledate2" min="<?php echo date("Y-m-d"); ?>" value="">
								Provide date of event: <input type="date" name="eventdate2" min="<?php echo date("Y-m-d"); ?>">
								Event State: <select id="eventstatesampleevent" style="width: 200px;" name="eventstatesampleevent" class="form-control custom-select">
								        <option value=""></option>
										<option value="AL">Alabama</option>
										<option value="AK">Alaska</option>
										<option value="AZ">Arizona</option>
										<option value="AR">Arkansas</option>
										<option value="CA">California</option>
										<option value="CO">Colorado</option>
										<option value="CT">Connecticut</option>
										<option value="DE">Delaware</option>
										<option value="DC">District Of Columbia</option>
										<option value="FL">Florida</option>
										<option value="GA">Georgia</option>
										<option value="HI">Hawaii</option>
										<option value="ID">Idaho</option>
										<option value="IL">Illinois</option>
										<option value="IN">Indiana</option>
										<option value="IA">Iowa</option>
										<option value="KS">Kansas</option>
										<option value="KY">Kentucky</option>
										<option value="LA">Louisiana</option>
										<option value="ME">Maine</option>
										<option value="MD">Maryland</option>
										<option value="MA">Massachusetts</option>
										<option value="MI">Michigan</option>
										<option value="MN">Minnesota</option>
										<option value="MS">Mississippi</option>
										<option value="MO">Missouri</option>
										<option value="MT">Montana</option>
										<option value="NE">Nebraska</option>
										<option value="NV">Nevada</option>
										<option value="NH">New Hampshire</option>
										<option value="NJ">New Jersey</option>
										<option value="NM">New Mexico</option>
										<option value="NY">New York</option>
										<option value="NC">North Carolina</option>
										<option value="ND">North Dakota</option>
										<option value="OH">Ohio</option>
										<option value="OK">Oklahoma</option>
										<option value="OR">Oregon</option>
										<option value="PA">Pennsylvania</option>
										<option value="RI">Rhode Island</option>
										<option value="SC">South Carolina</option>
										<option value="SD">South Dakota</option>
										<option value="TN">Tennessee</option>
										<option value="TX">Texas</option>
										<option value="UT">Utah</option>
										<option value="VT">Vermont</option>
										<option value="VA">Virginia</option>
										<option value="WA">Washington</option>
										<option value="WV">West Virginia</option>
										<option value="WI">Wisconsin</option>
										<option value="WY">Wyoming</option>
									</select>
							</div>
							
						
							<div class="form-check">
                  <input class="form-check-input hiddeninputs" data-div="showeventdate" type="radio" name="requesttype"  value="This is an EVENT only notification" required>
							  <label class="form-check-label">
								This is an EVENT notification only
							  </label>						  
							</div>						
							<div class="form-check" id="showeventdate">
								Provide date of event: <input type="date" name="eventdate" min="<?php echo date("Y-m-d"); ?>" value="">
                  Event State:
                <select id="eventstateeventonly" style="width: 200px;" name="eventstateeventonly" class="form-control custom-select">
								        <option value=""></option>
										<option value="AL">Alabama</option>
										<option value="AK">Alaska</option>
										<option value="AZ">Arizona</option>
										<option value="AR">Arkansas</option>
										<option value="CA">California</option>
										<option value="CO">Colorado</option>
										<option value="CT">Connecticut</option>
										<option value="DE">Delaware</option>
										<option value="DC">District Of Columbia</option>
										<option value="FL">Florida</option>
										<option value="GA">Georgia</option>
										<option value="HI">Hawaii</option>
										<option value="ID">Idaho</option>
										<option value="IL">Illinois</option>
										<option value="IN">Indiana</option>
										<option value="IA">Iowa</option>
										<option value="KS">Kansas</option>
										<option value="KY">Kentucky</option>
										<option value="LA">Louisiana</option>
										<option value="ME">Maine</option>
										<option value="MD">Maryland</option>
										<option value="MA">Massachusetts</option>
										<option value="MI">Michigan</option>
										<option value="MN">Minnesota</option>
										<option value="MS">Mississippi</option>
										<option value="MO">Missouri</option>
										<option value="MT">Montana</option>
										<option value="NE">Nebraska</option>
										<option value="NV">Nevada</option>
										<option value="NH">New Hampshire</option>
										<option value="NJ">New Jersey</option>
										<option value="NM">New Mexico</option>
										<option value="NY">New York</option>
										<option value="NC">North Carolina</option>
										<option value="ND">North Dakota</option>
										<option value="OH">Ohio</option>
										<option value="OK">Oklahoma</option>
										<option value="OR">Oregon</option>
										<option value="PA">Pennsylvania</option>
										<option value="RI">Rhode Island</option>
										<option value="SC">South Carolina</option>
										<option value="SD">South Dakota</option>
										<option value="TN">Tennessee</option>
										<option value="TX">Texas</option>
										<option value="UT">Utah</option>
										<option value="VT">Vermont</option>
										<option value="VA">Virginia</option>
										<option value="WA">Washington</option>
										<option value="WV">West Virginia</option>
										<option value="WI">Wisconsin</option>
										<option value="WY">Wyoming</option>
									</select>
							</div>
							
                <div class="warning pb-2"><span>Important Note:</span> <u>Less than 20 days' notice for events in CA, FL, NJ, & NY are subject to a rushed vetting fee of $150.00 per request. Less than 10 days notice is subject to rejection.</u>
							</div>												
						</div>
              <div class="form-group pb-3" id="Section2">
                <h5>2. QUANTITY: How many sample bottles are you requesting? (List quantity per item)</h5>
							<div id="alerts" class="alert alert-warning collapse" role="alert">
								<button type="button" class="close" data-toggle="collapse" href="#my-alert-box">
									<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
								</button>
								<p>This is for informational purposes only and a sample order will not be processed</p>
							</div>

							<div class="repeattest">
								<div class="oneField field-container-D" id="tfa_product_desc-D">
                    <label id="tfa_product_desc-L" class="label preField reqMark" for="tfa_product_desc">Product Description</label><br>
                    <div class="inputWrapper">
                      <input type="text" id="tfa_product_desc" name="tfa_product_desc" value="" aria-required="false" title="Product Name" data-dataset-allow-free-responses="" class="tfa_product_desc">
                      <small id="productvalid" class="warning">
                        <span>No product match found, you can <a class="errorlink" href="productsetup.php">add a new one</a> </span>
                      </small>
                    </div>
                    <!-- Replicate above but for items. Should remain hidden until an item is selected -->
                  </div>
                </div>
                <div class="repeattest">
                  <div class="oneField field-container-D" id="tfa_item_desc-D">
                    <label id="tfa_item_desc-L" class="label preField reqMark" for="tfa_item_desc">Item Description</label><br>
                    <div class="inputWrapper">
                      <input type="text" id="tfa_item_desc" name="tfa_item_desc" value="" aria-required="false" data-dataset-allow-free-responses="" class="tfa_item_desc">
                      <input type="hidden" id="tfa_code_desc" name="tfa_code_desc" value="" aria-required="false" data-dataset-allow-free-responses="" class="tfa_code_desc">
                      <input type="hidden" id="tfa_stock_desc" name="tfa_stock_desc" value="" aria-required="false" data-dataset-allow-free-responses="" class="tfa_stock_desc">
                      <small id="itemvalid" class="warning">
                        <span>No item exists for this product, you can add a new item<a id="ProductSetupLink" class="errorlink" href="#"> here.</a> </span>
                      </small>
                    </div>
								</div>													
							</div>
													
							<div id="repeater">
							  <!-- Repeater Heading -->
							  <div class="repeater-heading">
								  <button class="btn btn-primary repeater-add-btn" style="margin:10px 0;" id="AddNewProduct" disabled>
                      Add Item
								  </button>
							  </div>
							  
							  <!-- Repeater Items -->
                  <div class="items" data-group="test" id="ItemListing">
								<!-- Repeater Items Here -->		
								<div class="oneField field-container-D" >
								    <div class="row">
										<div class="col-3">
											<label class="label preField reqMark">Product Name</label>
										</div>
										<div class="col-3">
                          <label class="label preField reqMark">Item Description</label>
                        </div>
                        <div class="col-2">
                          <label class="label preField reqMark">Item Code</label>
										</div>
                        <div class="col-2">
                          <label class="label preField reqMark">Stock UOM</label>
                        </div>
                        <div class="col-1">
                          <label class="label preField reqMark"># of Bottles Requested</label>
										</div>
									</div>
									<div class="row" id="products">
									    <div class="col-3">
                          <input id="RepeaterProductName" type="text" style="width: 20em" name="product[]" value="" aria-required="true" title="Product Name" data-dataset-allow-free-responses="" data-skip-name="true" class="tfa_product_desc" readonly>
										</div>
										<div class="col-3">
                          <input id="RepeaterItemDescription" type="text" style="width: 20em" name="ItemDescription[]" value="" aria-required="true" title="Item Description?" data-dataset-allow-free-responses="" data-skip-name="true" class="tfa_item_desc" readonly>
                        </div>
                        <div class="col-2">
                          <input id="RepeaterItemCode" type="text" style="width: 20em" name="ItemCode[]" value="" aria-required="true" title="Item Code" data-dataset-allow-free-responses="" data-skip-name="true" class="tfa_code_desc" readonly>
                        </div>
                        <div class="col-2">
                          <input id="RepeaterStockUOM" type="text" style="width: 20em" name="StockUOM[]" value="" aria-required="true" title="Stock UOM" data-dataset-allow-free-responses="" data-skip-name="true" class="tfa_stock_desc" readonly>
										</div>
                        <div class="col-1">
                          <input type="number" class="form-control" style="width: 20em" data-skip-name="true" id="quantity" name="quantity[]" class="tfa_quantity" placeholder="1" min=1>
                          <input id="product_id" type="hidden" name="product_id[]" class="tfa_product_id" value="" data-dataset-allow-free-responses="" data-skip-name="true">
                          <input id="item_id" type="hidden" name="item_id[]" class="tfa_item_id" value="" data-dataset-allow-free-responses="" data-skip-name="true">
                          <input id="mhw_item_code" type="hidden" name="mhw_item_code[]" class="tfa_mhw_item_code" value="" data-dataset-allow-free-responses="" data-skip-name="true">
									</div>
								</div>							
								<!-- Repeater Remove Btn -->
								<div class="repeater-remove-btn">
                        <button name="remove-btn" id="remove-btn" type="button" class="btn btn-danger"  style="margin:10px 0;">Remove</button>
                      </div>
                    </div>
                  </div>
								</div>		
							  </div>															
						 
						  <div id="eventDetails" class="form-group collapse pt-5">
                <h5><span id="Section3">3.</span> PROVIDE EVENT DETAILS:</h5>
								<div class="form-group">						  
									<label>Describe the Event:(Common events: Consumer tasting, tradeshow, sponsorship, charitable donation, concert/festival/fairs, media, parties)</label>							
									<textarea class="form-control" name="eventdesc" placeholder="Event Description" maxlength="250"></textarea>							
								</div>
					
								<div class="form-group">
									<label> Name of Event:</label>
									<input type="text" class="form-control" name="eventname" placeholder="Event Name" maxlength="250">
								</div>
					
								<div class="form-group">
									<label> Is the event Public or Invitation Only:</label>
										<select class="custom-select" name="eventtype" required>								  
										<option value="1">Public</option>
										<option value="2">Invitation Only</option>								  
										</select>
								</div>
					
								<div class="form-group">
									<label>Address</label>
									<input type="text" class="form-control" name="address" placeholder="1234 Main St" maxlength="150">
								</div>
								<div class="form-group">
									<label>Address 2</label>
									<input type="text" class="form-control" name="address2" placeholder="Apartment, studio, or floor" maxlength="150">
								</div>
								<div class="form-row">
									<div class="form-group col-md-5">
										<label>City</label>
										<input type="text" class="form-control" name="city" maxlength="50">
									</div>
									<div class="form-group col-md-3">
										<label>State</label>
										<input type="text" class="form-control" name="state" maxlength="50">
									</div>
										<div class="form-group col-md-2">
										<label>Zip</label>
										<input type="text" class="form-control" name="zip" maxlength="20">
									</div>
									<div class="form-group col-md-2">
										<label>Country</label>
										<input type="text" class="form-control" name="country" maxlength="100">
									</div>
								</div>

								<div id="docsDiv">
									<div class="row col-lg-8 form-group" id="1">
										<label class="btn btn-default">
											<i class="fa fa-plus" aria-hidden="true"></i> Attachment
											<input id="docs" name="file[]" class="attachmentButton" data-doc-row="1" type="file" style="display: none" accept="image/*,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation" />
										</label>
									</div>
								</div>
								<div class="row pb-3">
									<div class="col-10">
										<div class="panel panel-default">
											<div class="panel-body pl-4" id="docsListing">

											</div>
										</div>
									</div>
								</div>

							</div>
						
              <div class="form-group pt-3" id="deliveryDetails">
                <h5><span id="Section4">4.</span> SAMPLE DELIVERY/PICK-UP PREFERENCE: Select the appropriate option: </h5>
									<div class="form-check">
                  <input class="form-check-input hiddeninputs" type="radio" data-div="showdelivery" id="deliverytype" name="deliverytype" value="Delivery" required>
										<label class="form-check-label">
											Delivery
										</label>
									</div>

									<div class="form-check" id="showdelivery">
										<div class="form-check">
                    <input class="form-check-input hiddeninputs" type="radio" id="deliverytypelocation" data-div="showdistributor" name="delivery" value="sameAddress" checked>
                    <label class="form-check-label" id="same-address-txt">
                      Same address as event, if listed above:
                    </label>
                  </div>
                  <div class="form-check">
                    <input class="form-check-input hiddeninputs" type="radio" id="deliverytypelocation" data-div="showdistributor" name="delivery" value="Distributor" checked>
											<label class="form-check-label">
											Licensed distributor (Provide name and address):
											</label>
										</div>

										<div id="showdistributor" class="form-check" >
                    <textarea class="form-control" id="pickupdistlocation" name="pickupdisttxt" placeholder="Provide name and address" maxlength="250"></textarea>
										</div>

										<div id="specificAddressSection" class="form-check">
                    <input class="form-check-input hiddeninputs" type="radio" id="deliverytypelocation" data-div="showSpecificAddressFieldSection" name="delivery" value="Specific Address">
											<label class="form-check-label">
                      Specific Address (samples to licensed retailers are prohibited)
											</label>
										</div>
										<div id="showSpecificAddressFieldSection" class="form-check collapse">
										    <div class="form-group">
												<label>Address</label>
                      <input type="text" class="form-control" id="deliveryaddress" name="deliveryaddress" placeholder="1234 Main St" maxlength="150">
											</div>
											<div class="form-group">
												<label>Address 2</label>
                      <input type="text" class="form-control" id="deliveryaddress2" name="deliveryaddress2" placeholder="Apartment, studio, or floor" maxlength="150">
											</div>
											<div class="form-row">
												<div class="form-group col-md-5">
													<label>City</label>
                        <input type="text" class="form-control" id="deliverycity" name="deliverycity" maxlength="50">
												</div>
												<div class="form-group col-md-3">
													<label>State</label>
                        <input type="text" class="form-control" id="deliverystate" name="deliverystate" maxlength="50">
												</div>
												<div class="form-group col-md-2">
													<label>Zip</label>
                        <input type="text" class="form-control" id="deliveryzip" name="deliveryzip" maxlength="20">
												</div>
												<div class="form-group col-md-2">
													<label>Country</label>
                        <input type="text" class="form-control" id="deliverycountry" name="deliverycountry" maxlength="100">
											</div>
											</div>
								       </div>
										</div>

										<div class="form-check">
                  <input class="form-check-input hiddeninputs" type="radio"  data-div="showpickup" id="deliverytype" name="deliverytype" value="Pickup" required>
                  <label class="form-check-label"> Pickup </label>
									<div class="form-check" id="showpickup">
										<div class="form-check">
                      <input class="form-check-input hiddeninputs" id="deliverytypelocation" type="radio" name="pickuptype" value="Western Wine Services, CA">
                      <label class="form-check-label"> Western Wine Services, CA </label>
										</div>
										<div class="form-check">
                      <input class="form-check-input hiddeninputs" id="deliverytypelocation" data-div="pickupwtxt" type="radio" name="pickuptype" value="Western Wine Carriers, NJ">
                      <label class="form-check-label"> Western Wine Carriers, NJ </label>
										</div>

										<div id="pickupwtxt" class="form-check-label">
                      <small class="warning"><span> **Pick-up at Western Wine Carriers, NJ require (1) NJ solicitor's permit or the equivalent for out-of-state companies. (2) NJ Transit Insignia for your vehicle or a temporary
											(2) NJ Transit Insignia for your  vehicle or a temporary permit for your rental vehicle. </span></small>
										</div>

										<div class="form-check">
                      <input class="form-check-input hiddeninputs" id="deliverytypelocation" type="radio" name="pickuptype" value="Convoy">
											<label class="form-check-label">
                        Convoy
											</label>	
										</div>

										<div class="form-check">
                      <input class="form-check-input hiddeninputs" id="deliverytypelocation" type="radio" name="pickuptype" value="MHWFL">
											<label class="form-check-label">
                        MHWFL
											</label>	
										</div>
                    <div class="form-check">
                      <input class="form-check-input hiddeninputs" id="deliverytypelocation" type="radio" name="pickuptype" value="Fond du lac (FDL), NJ">
                      <label class="form-check-label">
                        Fond du lac (FDL), NJ
                      </label>
                    </div>
                    <div class="form-check">
                      <input class="form-check-input hiddeninputs" id="deliverytypelocation" type="radio" name="pickuptype" value="Alba Wine and Spirits Warehousing">
                      <label class="form-check-label">
                        Alba Wine and Spirits Warehousing
                      </label>
                    </div>
                    <div class="form-check">
                      <input class="form-check-input hiddeninputs" id="deliverytypelocation" type="radio" name="pickuptype" value="MHW's Office - Manhasset, NY">
                      <label class="form-check-label">
                        MHW's Office - Manhasset, NY
                      </label>
                    </div>
										<div class="form-check">
                      <input class="form-check-input hiddeninputs" id="deliverytypelocation" data-div="pickupothertxt" id="other" type="radio" name="pickuptype" value="other">
											<label class="form-check-label">
                        Other - explain below
											</label>
                      <div id="otherwarning" class="warning pb-2">
                        <label><span>Retail sample delivery is prohibited.</label>
                      </div>
                    </div>
                  </div>
										</div>
										<div id="pickupothertxt">
											<textarea class="form-control" name="pickupothertxt" maxlength="250"></textarea>
										</div>	
									</div>
						<div id="whosection" class="form-group collapse pb-3">
                <h5><span id="Section5">5.</span> WHO is using the samples or participating in the event? Select the appropriate option: </h5>
							
							 <div class="form-check">
							  <input class="form-check-input hiddeninputs" data-div="sampleusermhw" type="radio" name="sampleuser" value="MHW">
							  <label class="form-check-label">
							  MHW licensed solicitor/rep (provide name and license number):
							  </label>
							</div>
							<div id="sampleusermhw">
								<textarea class="form-control" name="sampleusermhw" maxlength="150"></textarea>
							</div>
							<div class="form-check">
							  <input class="form-check-input hiddeninputs" data-div="sampleuserbroker" type="radio" name="sampleuser" value="broker">
							  <label class="form-check-label">
								Broker, PR/Marketing Firm (provide name and licenses/permits, if any):
							  </label>
							</div>
							<div id="sampleuserbroker">
								<textarea class="form-control" name="sampleuserbroker" maxlength="150"></textarea>
							</div>
							<div class="form-check">
							  <input class="form-check-input hiddeninputs" data-div="sampleuserrep" type="radio" name="sampleuser" value="rep">
							  <label class="form-check-label">
								An authorized representative (provide name and licenses/permits, if any):
							  </label>
							</div>
							<div id="sampleuserrep">
								<textarea class="form-control" name="sampleuserrep" maxlength="150"></textarea>
							</div>							
						</div>	
              <input type="hidden" name="productId" id="prodID" value="">
              <input type="hidden" name="itemId" id="itemID" value="">
              <input type="hidden" name="itemId2" id="itemID2" value="">
              <input type="hidden" name="product_mhw_code" id="product_mhw_code" value="">
              <input type="hidden" name="product_mhw_code_search" id="product_mhw_code_search" value="">
              <input type="hidden" name="userID" id="userID" value="<?php echo $_SESSION['mhwltdphp_user']; ?>">
					<div class="actions" id="4690986-A">
                <input type="submit" data-label="Submit" class="primaryAction btn btn-primary" id="submit_button" name="submit_button" value="Submit">
              </div>
            </div>
				</div>
			</div>
		</div>    
    </form>
	</div>

	<script src='js/iframe_resize_helper_internal.js'></script>


<!-- Modal -->
<div class="modal fade" id="eventTypeWarningMesg" tabindex="-1" role="dialog" aria-labelledby="eventTypeWarningLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="eventTypeWarningLabel">Warning</h5>
      </div>
          <!-- You are required to select 'This is a sample request for a distributor' option.  -->
          <div id="ModalText" class="modal-body">Allowed states for this option are limited to California, Florida, New Jersey and New York..</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="warningMesg" tabindex="-1" role="dialog" aria-labelledby="warningLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="warningLabel">Warning</h5>
      </div>
      <div id="ModalText" class="modal-body"> </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="okbutton">Ok</button>
		<button type="button" class="btn btn-primary" id="cancelButton" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="okMesg" tabindex="-1" role="dialog" aria-labelledby="warningLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="warningLabel"></h5>
      </div>
      <div id="ModalText" class="modal-body"> </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="okbutton" data-dismiss="modal">Ok</button>
      </div>
    </div>
  </div>
</div>

<div id="attachmentTemplate" class="d-none">
    <div class="row">
        <div class="col-lg-5">
            <div class="form-group">
                <label class="btn btn-default">
                    <input id="docs" name="file[]" class="attachmentButton" type="file" style="display:none" accept="image/*,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation" />
                    <i class="fa fa-plus" aria-hidden="true"></i> Attachment
                </label>
            </div>
        </div>
    </div>
</div>

</body>
</html>
