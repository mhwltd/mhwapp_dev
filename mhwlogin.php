<?php
session_start();
include("prepend.php");
//include('head.php');

//echo "<pre>";
//var_dump($_REQUEST); 
//echo "</post>";

//echo "<pre>";
//var_dump($_SERVER); 
//echo "</post>";

$referrerDomain = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
$MHW_AD_USER_1 = $_SERVER['AUTH_USER'];
$MHW_AD_USER_2 = $_SERVER['HTTP_X_MS_CLIENT_PRINCIPAL_NAME'];
//echo $referrerDomain;
echo $MHW_AD_USER_1."<br />";
echo $MHW_AD_USER_2."<br />";
//exit();
if(!isset($_SESSION['mhwltdphp_user'])){

	$page_err_msg = "You are not logged in.";

	if($referrerDomain!=="mhwltd.app" && $referrerDomain!=="mhwapp.azurewebsites.net" && $referrerDomain!=="mhwapp-dev.azurewebsites.net" && $referrerDomain!=="crd.mhwltdclients.com" && $referrerDomain!=="mhwltdclients.com" && $referrerDomain!=="www.mhwltdclients.com" && $referrerDomain!=="mhwwebdev.nstwebbeta.com" && $referrerDomain!=="nstwebbeta.com" && $referrerDomain!=="mhwapprise.nstwebbeta.com"){
			$page_err_msg = "The referring domain (".$referrerDomain.") is not whitelisted.";

			/* AD */
			$mhw=0;
			$pos1 = strrpos($MHW_AD_USER_1, "@mhwltd.com");
			$pos2 = strrpos($MHW_AD_USER_2, "@mhwltd.com");
			if ($pos1 === false) {
				if ($pos2 === false) {
					header("Location: .auth/login/aad?post_login_redirect_url=/mhwlogin.php");
				}
				else{
					$_SESSION['mhwltdphp_user'] = str_replace("@mhwltd.com","",$MHW_AD_USER_2); 
					$_SESSION['mhwltdphp_useremail'] =$MHW_AD_USER_2;
					$_SESSION['mhwltdphp_userlastname'] = substr($_SESSION['mhwltdphp_user'],1);
					$mhw=1;
				}
			}
			else{
				$_SESSION['mhwltdphp_user'] = str_replace("@mhwltd.com","",$MHW_AD_USER_1);
				$_SESSION['mhwltdphp_useremail'] = $MHW_AD_USER_1;
				$_SESSION['mhwltdphp_userlastname'] = substr($_SESSION['mhwltdphp_user'],1);
				$mhw=1;
			}

			if($mhw==1){
				if(strtoupper($_SESSION['mhwltdphp_user'])=='DSZAREJKO'){
					$_SESSION['mhwltdphp_usertype'] = "ADMIN";
				}
				else{
					$_SESSION['mhwltdphp_usertype'] = "SUPERUSER";
				}
				$_SESSION['mhwltdphp_userrole'] = "MHW AD";
				$_SESSION['mhwltdphp_userclients'] = stripslashes("MHW Web Demo");

				$hour = time() + 14400; //4 hrs
				setcookie(ID_mhwphp, $_SESSION['mhwltdphp_user'], $hour);
				setcookie(UL_mhwphp, $_SESSION['mhwltdphp_userlastname'], $hour);
				setcookie(UE_mhwphp, $_SESSION['mhwltdphp_useremail'], $hour);
				setcookie(UC_mhwphp, $_SESSION['mhwltdphp_userclients'], $hour);
				setcookie(UT_mhwphp, $_SESSION['mhwltdphp_usertype'], $hour);
				//setcookie(Key_mhwphp, $_POST['pass'], $hour);

				//log access
				$fp1 = fopen('logs/accesslog.txt', 'a');
				$accesslog="\n\r\n\r".date("Ymd H:i:s")."\t".$_SESSION['mhwltdphp_user']."\t".$_SESSION['mhwltdphp_userlastname']."\t".$_SESSION['mhwltdphp_useremail']."\t".$_SESSION['mhwltdphp_userclients']."\t".$_SESSION['mhwltdphp_usertype']."\n\r";
				fwrite($fp1, $accesslog);
				fclose($fp1);

				$page_err_msg = "";
				header("Location: home.php");
			}

			/* AD */
	}
	else{

		// check that login form was complete
		//if(!$_POST['username'] /* | !$_POST['pass']*/) {
		//	$page_err_msg = "You did not fill in a required field.";
		//}
		//else{
			//$_POST['pass'] = stripslashes($_POST['pass']);
			//$_POST['pass'] = md5($_POST['pass']);

			// if login is ok, set session, add a cookie
			$_POST['username'] = stripslashes($_POST['username']);
			$_SESSION['mhwltdphp_user'] = $_POST['username'];
			
			$_POST['userlastname'] = stripslashes($_POST['userlastname']);
			$_SESSION['mhwltdphp_userlastname'] = $_POST['userlastname'];
			
			$_POST['useremail'] = stripslashes($_POST['useremail']);
			$_SESSION['mhwltdphp_useremail'] = $_POST['useremail'];

			$_POST['userrole'] = stripslashes($_POST['userrole']);
			$_SESSION['mhwltdphp_userrole'] = $_POST['userrole'];
			
			$_POST['userclients'] = stripslashes($_POST['userclients']);
			//FIX CRD LOGIN ISSUES
			//$_POST['userclients'] = str_replace("'","''",$_POST['userclients']);
			$_POST['userclients'] = str_replace('MHW, Ltd./Latitude Beve -DO NOT USE','MHW, Ltd./Latitude Beverage',$_POST['userclients']);
			$_POST['userclients'] = str_replace("MHW, Ltd./Grace O","MHW, Ltd./Grace OMalley",$_POST['userclients']);
			//$_POST['userclients'] = str_replace("MHW, Ltd./Carson","MHW, Ltd./Carson'z",$_POST['userclients']);
			$_SESSION['mhwltdphp_userclients'] = $_POST['userclients'];
			
			if(strtoupper($_SESSION['mhwltdphp_user'])=='DSZAREJKO'){
				$_SESSION['mhwltdphp_usertype'] = "ADMIN";
			}
			else{
				$_SESSION['mhwltdphp_usertype'] = "CRDforms";
			}

			$hour = time() + 14400; //4 hrs
			setcookie(ID_mhwphp, $_POST['username'], $hour);
			setcookie(UL_mhwphp, $_POST['userlastname'], $hour);
			setcookie(UE_mhwphp, $_POST['useremail'], $hour);
			setcookie(UC_mhwphp, $_POST['userclients'], $hour);
			setcookie(UT_mhwphp, $_SESSION['mhwltdphp_usertype'], $hour);
			//setcookie(Key_mhwphp, $_POST['pass'], $hour);

			//log access
			$fp1 = fopen('logs/accesslog.txt', 'a');
			$accesslog="\n\r\n\r".date("Ymd H:i:s")."\t".$_POST['username']."\t".$_POST['userlastname']."\t".$_POST['useremail']."\t".$_POST['userclients']."\t".$_SESSION['mhwltdphp_usertype']."\n\r";
			fwrite($fp1, $accesslog);
			fclose($fp1);

			// redirect to content
		
			if($_GET["redirect"]=="productsetup"){  //FROM CRD
				$page_err_msg = "";
				if(($referrerDomain=="mhwwebdev.nstwebbeta.com" || $referrerDomain=="nstwebbeta.com") /*&& strtoupper($_SESSION['mhwltdphp_user'])=='DSZAREJKO' */){
					//header("Location: formsdev/productsetup.php"); //NATIVE FORM
					header("Location: send.php");
				}
				else{
					header("Location: forms/productsetup.php"); //NATIVE FORM
				}
				//header("Location: product_interstitial.php?email=".urlencode($_SESSION['mhwltdphp_useremail'])."&lastname=".urlencode($_SESSION['mhwltdphp_userlastname'])); //FA LOGIN 2
			}
			else if($_GET["redirect"]=="orderreview"){ //FROM CRD
				$page_err_msg = "";
				header("Location: forms/orderreview.php"); 
			}
			else{
				$page_err_msg = "";
				header("Location: home.php");
			}	
		
		//}
	}

	//if not logged in
	echo "<span id=\"loginmsg\">".$page_err_msg."</span>";

}
else{  //existing session
	
	//reauthenticate without requiring logout (CRD impersonate user)	
	if($_POST['username'] && ($referrerDomain=="mhwltd.app" || $referrerDomain=="mhwltdclients.com" || $referrerDomain=="mhwwebdev.nstwebbeta.com" || $referrerDomain=="nstwebbeta.com")){
		
		// if login is ok, set session, add a cookie
		$_POST['username'] = stripslashes($_POST['username']);
		$_SESSION['mhwltdphp_user'] = $_POST['username'];
		
		$_POST['userlastname'] = stripslashes($_POST['userlastname']);
		$_SESSION['mhwltdphp_userlastname'] = $_POST['userlastname'];
		
		$_POST['useremail'] = stripslashes($_POST['useremail']);
		$_SESSION['mhwltdphp_useremail'] = $_POST['useremail'];

		$_POST['userrole'] = stripslashes($_POST['userrole']);
		$_SESSION['mhwltdphp_userrole'] = $_POST['userrole'];
		
		$_POST['userclients'] = stripslashes($_POST['userclients']);
		//FIX CRD LOGIN ISSUES
		$_POST['userclients'] = str_replace('MHW, Ltd./Latitude Beve -DO NOT USE','MHW, Ltd./Latitude Beverage',$_POST['userclients']);
		//$_POST['userclients'] = str_replace("MHW, Ltd./Carson","MHW, Ltd./Carson'z",$_POST['userclients']);
		$_SESSION['mhwltdphp_userclients'] = $_POST['userclients'];
		
		if(strtoupper($_SESSION['mhwltdphp_user'])=='DSZAREJKO'){
			$_SESSION['mhwltdphp_usertype'] = "ADMIN";
		}
		else{
			$_SESSION['mhwltdphp_usertype'] = "CRDforms";
		}

		$hour = time() + 14400; //4 hrs
		setcookie(ID_mhwphp, $_POST['username'], $hour);
		setcookie(UL_mhwphp, $_POST['userlastname'], $hour);
		setcookie(UE_mhwphp, $_POST['useremail'], $hour);
		setcookie(UC_mhwphp, $_POST['userclients'], $hour);
		setcookie(UT_mhwphp, $_SESSION['mhwltdphp_usertype'], $hour);
		//setcookie(Key_mhwphp, $_POST['pass'], $hour);	

		//log access
		$fp1 = fopen('logs/accesslog.txt', 'a');
		$accesslog="\n\r\n\r".date("Ymd H:i:s")."\t".$_POST['username']."\t".$_POST['userlastname']."\t".$_POST['useremail']."\t".$_POST['userclients']."\t".$_SESSION['mhwltdphp_usertype']."\n\r";
		fwrite($fp1, $accesslog);
		fclose($fp1);
	}
	
	// redirect to content
	if($_GET["redirect"]=="productsetup"){  //FROM CRD
		$page_err_msg = "";
		header("Location: forms/productsetup.php"); //NATIVE FORM
		//header("Location: product_interstitial.php?email=".urlencode($_SESSION['mhwltdphp_useremail'])."&lastname=".urlencode($_SESSION['mhwltdphp_userlastname'])); //FA LOGIN 2
	}
	else if($_GET["redirect"]=="orderreview"){ //FROM CRD
		$page_err_msg = "";
		header("Location: forms/orderreview.php"); 
	}
	else{
		$page_err_msg = "";
		header("Location: home.php");
	}
}
?>


